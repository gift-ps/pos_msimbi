<!-- TOP 5 SELLERS -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <?php
                if (isset($_POST['display'])) {
                    $_SESSION['sales_date'] = $_POST['date'];
                } else {
                    unset($_SESSION['sales_date']);
                }
                ?>
                <div class="box-body">
                    <br></br>
                    <?php
                    if (isset($_POST['display'])) {

                        $date = $_POST['date'];
                        $date = explode('-', $date);
                        $branch = $_SESSION['branch'];
                        $start = date("Y-m-d", strtotime($date[0]));
                        $startDate = $start . " 00:00:00";
                        $end = date("Y-m-d", strtotime($date[1]));
                        $endDate = $end . " 00:00:00";
                        $stop_date = date('Y-m-d H:i:s', strtotime($endDate . ' +1 day'));
                        //$supplier_id = $_POST['supplier_id'];
                        $date_filter = "AND sales.date_added BETWEEN '$startDate' AND '$stop_date' ";


                        echo ' <a href="escpos-php-development/example/interface/print-sales.php?fromdate=' . $startDate . '&enddate=' . $stop_date . '   ">Print Report</a>';
                    } else {
                        //echo ' <a href="escpos-php-development/example/interface/print-sales.php">Print Report</a>';
                    }
                    ?>
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Item</th>
                                <th>Quantity Sold</th>
                                <th>Amount Sold</th>
                                <th>Contribution to Sales (%)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php


                            // echo ' <center><h3 class="box-title" style=" color: black"><b><u> Top 5 Best Sellers </u></b></h3></center><br>';
                            $top5sales = $DAO->best_selling_products($con, $date_filter);
                            while ($row = mysqli_fetch_array($top5sales)) {
                                $product = $row['prod_name'];
                                $qty = $row['qty'];
                                $total_qty = $row['total_qty'];
                                $total_price = $row['total_price'];
                                $total_sales = $row['total_sales'];
                                $percent_to_sales = $row['percentage'];
                            ?>
                                <tr>
                                    <td><?php echo $product ?></td>
                                    <td><?php echo $qty ?></td>
                                    <td><?php echo $total_price ?></td>
                                    <td><?php echo $percent_to_sales ?></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th><?php echo "Total of all sales" ?></th>
                                <th><?php if (isset($total_qty)) {
                                        echo $total_qty;
                                    } ?></th>
                                <th><?php if (isset($total_sales)) {
                                        echo $total_sales;
                                    } ?></th>
                                <th><?php echo "100%" ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
</section><!-- /.content -->

<!-- BOTTOM 5 SELLERS -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <?php
                if (isset($_POST['display'])) {
                    $_SESSION['sales_date'] = $_POST['date'];
                } else {
                    unset($_SESSION['sales_date']);
                }
                ?>
                <div class="box-body">
                    <br></br>
                    <?php
                    if (isset($_POST['display'])) {

                        $date = $_POST['date'];
                        $date = explode('-', $date);
                        $branch = $_SESSION['branch'];
                        $start = date("Y-m-d", strtotime($date[0]));
                        $startDate = $start . " 00:00:00";
                        $end = date("Y-m-d", strtotime($date[1]));
                        $endDate = $end . " 00:00:00";
                        $stop_date = date('Y-m-d H:i:s', strtotime($endDate . ' +1 day'));
                        //$supplier_id = $_POST['supplier_id'];
                        $date_filter = "AND sales.date_added BETWEEN '$startDate' AND '$stop_date' ";


                        echo ' <a href="escpos-php-development/example/interface/print-sales.php?fromdate=' . $startDate . '&enddate=' . $stop_date . '   ">Print Report</a>';
                    } else {
                        //echo ' <a href="escpos-php-development/example/interface/print-sales.php">Print Report</a>';
                    }
                    ?>
                    <table id="example3" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Item</th>
                                <th>Quantity Sold</th>
                                <th>Amount Sold</th>
                                <th>Contribution to Sales (%)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php


                            // echo ' <center><h3 class="box-title" style=" color: black"><b><u> Top 5 Best Sellers </u></b></h3></center><br>';
                            $top5sales = $DAO->worst_selling_products($con, $date_filter);
                            while ($row = mysqli_fetch_array($top5sales)) {
                                $product = $row['prod_name'];
                                $qty = $row['qty'];
                                $total_qty = $row['total_qty'];
                                $total_price = $row['total_price'];
                                $total_sales = $row['total_sales'];
                                $percent_to_sales = $row['percentage'];
                            ?>
                                <tr>
                                    <td><?php echo $product ?></td>
                                    <td><?php echo $qty ?></td>
                                    <td><?php echo $total_price ?></td>
                                    <td><?php echo $percent_to_sales ?></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th><?php echo "Total of all sales" ?></th>
                                <th><?php if (isset($total_qty)) {
                                        echo $total_qty;
                                    } ?></th>
                                <th><?php if (isset($total_sales)) {
                                        echo $total_sales;
                                    } ?></th>
                                <th><?php echo "100%" ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
</section><!-- /.content -->


<!-- SALES BY PAYMENT METHOD -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <?php
                if (isset($_POST['display'])) {
                    $_SESSION['sales_date'] = $_POST['date'];
                } else {
                    unset($_SESSION['sales_date']);
                }
                ?>
                <div class="box-body">
                    <br></br>
                    <?php
                    if (isset($_POST['display'])) {

                        $date = $_POST['date'];
                        $date = explode('-', $date);
                        $branch = $_SESSION['branch'];
                        $start = date("Y-m-d", strtotime($date[0]));
                        $startDate = $start . " 00:00:00";
                        $end = date("Y-m-d", strtotime($date[1]));
                        $endDate = $end . " 00:00:00";
                        $stop_date = date('Y-m-d H:i:s', strtotime($endDate . ' +1 day'));
                        //$supplier_id = $_POST['supplier_id'];
                        $date_filter = "AND sales.date_added BETWEEN '$startDate' AND '$stop_date' ";


                        echo ' <a href="escpos-php-development/example/interface/print-sales.php?fromdate=' . $startDate . '&enddate=' . $stop_date . '   ">Print Report</a>';
                    } else {
                        //echo ' <a href="escpos-php-development/example/interface/print-sales.php">Print Report</a>';
                    }
                    ?>
                    <table id="example4" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Payment Method</th>
                                <th>Total Sales</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php


                            // echo ' <center><h3 class="box-title" style=" color: black"><b><u> Top 5 Best Sellers </u></b></h3></center><br>';
                            $payment_method = $DAO->sales_by_payment_method($con, $date_filter);
                            while ($row = mysqli_fetch_array($payment_method)) {
                                $modeofpayment = $row['modepayment'];
                                $total_sales = $row['total_sales'];
                                // $percent_to_sales = $row['percentage'];
                            ?>
                                <tr>
                                    <td><?php echo $modeofpayment ?></td>
                                    <td><?php echo $total_sales ?></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th><?php echo "Total of all sales" ?></th>
                                <th><?php if (isset($total_qty)) {
                                        echo $total_qty;
                                    } ?></th>

                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
</section><!-- /.content -->


<!-- SALES BY SALESPERSON -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <?php
                if (isset($_POST['display'])) {
                    $_SESSION['sales_date'] = $_POST['date'];
                } else {
                    unset($_SESSION['sales_date']);
                }
                ?>
                <div class="box-body">
                    <br></br>
                    <?php
                    if (isset($_POST['display'])) {

                        $date = $_POST['date'];
                        $date = explode('-', $date);
                        $branch = $_SESSION['branch'];
                        $start = date("Y-m-d", strtotime($date[0]));
                        $startDate = $start . " 00:00:00";
                        $end = date("Y-m-d", strtotime($date[1]));
                        $endDate = $end . " 00:00:00";
                        $stop_date = date('Y-m-d H:i:s', strtotime($endDate . ' +1 day'));
                        //$supplier_id = $_POST['supplier_id'];
                        $date_filter = "AND sales.date_added BETWEEN '$startDate' AND '$stop_date' ";


                        echo ' <a href="escpos-php-development/example/interface/print-sales.php?fromdate=' . $startDate . '&enddate=' . $stop_date . '   ">Print Report</a>';
                    } else {
                        //echo ' <a href="escpos-php-development/example/interface/print-sales.php">Print Report</a>';
                    }
                    ?>
                    <table id="example5" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Sales Persom</th>
                                <th>Items Sold</th>
                                <th>Total Sales</th>
                                <th>Contribution to Sales (%)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php


                            $salesperson_query = $DAO->sales_by_salesperson($con, $date_filter);
                            while ($row = mysqli_fetch_array($salesperson_query)) {
                                $salesperson = $row['name'];
                                $qty = $row['qty'];
                                $total_sales = $row['total'];
                                $contribution = $row['contribution'];
                            ?>
                                <tr>
                                    <td><?php echo $salesperson ?></td>
                                    <td><?php echo $qty ?></td>
                                    <td><?php echo $total_sales ?></td>
                                    <td><?php echo $contribution ?></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
</section><!-- /.content -->



<!-- NET SALES  -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <?php
                if (isset($_POST['display'])) {
                    $_SESSION['sales_date'] = $_POST['date'];
                } else {
                    unset($_SESSION['sales_date']);
                }
                ?>
                <div class="box-body">
                    <br></br>
                    <?php
                    if (isset($_POST['display'])) {

                        $date = $_POST['date'];
                        $date = explode('-', $date);
                        $branch = $_SESSION['branch'];
                        $start = date("Y-m-d", strtotime($date[0]));
                        $startDate = $start . " 00:00:00";
                        $end = date("Y-m-d", strtotime($date[1]));
                        $endDate = $end . " 00:00:00";
                        $stop_date = date('Y-m-d H:i:s', strtotime($endDate . ' +1 day'));
                        //$supplier_id = $_POST['supplier_id'];
                        $date_filter = "AND sales.date_added BETWEEN '$startDate' AND '$stop_date' ";
                        $prev_week = "WHERE WEEK (date_added) = WEEK( current_date ) - 1 AND YEAR( date_added) = YEAR( current_date )";


                        echo ' <a href="escpos-php-development/example/interface/print-sales.php?fromdate=' . $startDate . '&enddate=' . $stop_date . '   ">Print Report</a>';
                    } else {
                        //echo ' <a href="escpos-php-development/example/interface/print-sales.php">Print Report</a>';
                    }
                    ?>
                    <table id="example7" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Sales (ZWK)</th>
                                <th>Items Sold</th>
                                <th>Growth From Previous Week (%)</th>
                                <th>Growth From Previous Month (%)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php


                            $salesperson_query = $DAO->net_sales($con, $date_filter);
                            while ($row = mysqli_fetch_array($salesperson_query)) {
                                $net_sales = $row['cur_total_sales'];
                                $qty = $row['current_gross_qty'];
                                $total_sales = $row['prev_week_growth'];
                                $contribution = $row['prev_month_growth'];

                                $total_sales_vat = ($net_sales / 1.16);
                                $total_sales_vatfinal = $net_sales - $total_sales_vat;

                                $prev_week_sales = $DAO->getSalesPreviousWeek($con);
                                $prev_week_sales_vat = ($prev_week_sales / 1.16);
                                $prev_week_sales_total = $prev_week_sales - $prev_week_sales_vat;
                                $prev_week_sales_total_growth = ($total_sales_vat - $prev_week_sales_total)/100;
                                
                                $prev_month_sales = $DAO->getSalesPreviousMonth($con);
                                $prev_month_sales_vat = ($prev_month_sales / 1.16);
                                $prev_month_sales_vat_final = $prev_month_sales - $prev_month_sales_vat;
                                $prev_month_sales_total_growth = ($prev_month_sales - $prev_month_sales_vat_final)/100;

                            }
                            ?>
                                <tr>
                                    <td><?php echo number_format($total_sales_vatfinal,2) ?></td>
                                    <td><?php echo $qty ?></td>
                                    <td><?php echo number_format($prev_week_sales_total_growth,2) ?></td>
                                    <td><?php echo number_format($prev_month_sales_total_growth,2) ?></td>
                                </tr>
                            <?php
                            
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section><!-- /.content -->

<!-- GROSS SALES -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <?php
                if (isset($_POST['display'])) {
                    $_SESSION['sales_date'] = $_POST['date'];
                } else {
                    unset($_SESSION['sales_date']);
                }
                ?>
                <div class="box-body">
                    <br></br>
                    <?php
                    if (isset($_POST['display'])) {

                        $date = $_POST['date'];
                        $date = explode('-', $date);
                        $branch = $_SESSION['branch'];
                        $start = date("Y-m-d", strtotime($date[0]));
                        $startDate = $start . " 00:00:00";
                        $end = date("Y-m-d", strtotime($date[1]));
                        $endDate = $end . " 00:00:00";
                        $stop_date = date('Y-m-d H:i:s', strtotime($endDate . ' +1 day'));
                        //$supplier_id = $_POST['supplier_id'];
                        $date_filter = "AND sales.date_added BETWEEN '$startDate' AND '$stop_date' ";


                        echo ' <a href="escpos-php-development/example/interface/print-sales.php?fromdate=' . $startDate . '&enddate=' . $stop_date . '   ">Print Report</a>';
                    } else {
                        //echo ' <a href="escpos-php-development/example/interface/print-sales.php">Print Report</a>';
                    }
                    ?>
                    <table id="example8" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Sales (ZWK)</th>
                                <th>Items Sold</th>
                                <th>Growth From Previous Week (%)</th>
                                <th>Growth From Previous Month (%)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php


                            $salesperson_query = $DAO->gross_sales($con, $date_filter);
                            while ($row = mysqli_fetch_array($salesperson_query)) {
                                $salesperson = $row['cur_total_sales'];
                                $qty = $row['current_gross_qty'];
                                $total_sales = $row['prev_week_growth'];
                                $contribution = $row['prev_month_growth'];
                            ?>
                                <tr>
                                    <td><?php echo $salesperson ?></td>
                                    <td><?php echo $qty ?></td>
                                    <td><?php echo $total_sales ?></td>
                                    <td><?php echo $contribution ?></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section><!-- /.content -->


<!-- SALES BY PRODUCT -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <?php
                if (isset($_POST['display'])) {
                    $_SESSION['sales_date'] = $_POST['date'];
                } else {
                    unset($_SESSION['sales_date']);
                }
                ?>
                <div class="box-body">
                    <br></br>
                    <?php
                    if (isset($_POST['display'])) {

                        $date = $_POST['date'];
                        $date = explode('-', $date);
                        $branch = $_SESSION['branch'];
                        $start = date("Y-m-d", strtotime($date[0]));
                        $startDate = $start . " 00:00:00";
                        $end = date("Y-m-d", strtotime($date[1]));
                        $endDate = $end . " 00:00:00";
                        $stop_date = date('Y-m-d H:i:s', strtotime($endDate . ' +1 day'));
                        //$supplier_id = $_POST['supplier_id'];
                        $date_filter = "AND sales.date_added BETWEEN '$startDate' AND '$stop_date' ";


                        echo ' <a href="escpos-php-development/example/interface/print-sales.php?fromdate=' . $startDate . '&enddate=' . $stop_date . '   ">Print Report</a>';
                    } else {
                        //echo ' <a href="escpos-php-development/example/interface/print-sales.php">Print Report</a>';
                    }
                    ?>
                    <table id="example9" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Sales (ZWK)</th>
                                <th>Items Sold</th>
                                <th>Contributions (%)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            // Put The ID Of The Product In Question Down Here...
                            $prod_id = '22';

                            $salesperson_query = $DAO->sales_by_earpircing($con, $date_filter);
                            while ($row = mysqli_fetch_array($salesperson_query)) {
                                $total_sales = $row['total_sales'];
                                $qty = $row['qty'];
                                $contribution = $row['percentage'];
                            ?>
                                <tr>
                                    <td><?php echo $total_sales ?></td>
                                    <td><?php echo $qty ?></td>
                                    <td><?php echo $contribution ?></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section><!-- /.content -->


<!-- PROFIT & LOSS -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <?php
                if (isset($_POST['display'])) {
                    $_SESSION['sales_date'] = $_POST['date'];
                } else {
                    unset($_SESSION['sales_date']);
                }
                ?>
                <div class="box-body">
                    <br></br>
                    <?php
                    if (isset($_POST['display'])) {

                        $date = $_POST['date'];
                        $date = explode('-', $date);
                        $branch = $_SESSION['branch'];
                        $start = date("Y-m-d", strtotime($date[0]));
                        $startDate = $start . " 00:00:00";
                        $end = date("Y-m-d", strtotime($date[1]));
                        $endDate = $end . " 00:00:00";
                        $stop_date = date('Y-m-d H:i:s', strtotime($endDate . ' +1 day'));
                        //$supplier_id = $_POST['supplier_id'];
                        $where = "WHERE sales.date_added BETWEEN '$startDate' AND '$stop_date' ";
                        $date_filter_part_pay = "WHERE date_added BETWEEN '$startDate' AND '$stop_date' ";


                        echo ' <a href="escpos-php-development/example/interface/print-sales.php?fromdate=' . $startDate . '&enddate=' . $stop_date . '   ">Print Report</a>';
                    } else {
                        //echo ' <a href="escpos-php-development/example/interface/print-sales.php">Print Report</a>';
                    }
                    ?>
                    <table id="examples" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Expense (ZWK)</th>
                                <th>Total Sales (ZWK) </th>
                                <th>Net Sales (ZWK)
                                <th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                            $expenseTotal = $DAO->getTotalExpenses($con, $date_filter);
                            $salesTotal = $DAO->getTotalSales($con, $where, $date_filter_part_pay);
                            $net_sales = $salesTotal - $expenseTotal;
                            $amountLessVat = ($salesTotal / 1.16);
                            $vatFinalTotal = $salesTotal - $amountLessVat;

                            $salesperson_query = $DAO->profit_loss($con);
                            // while ($row = mysqli_fetch_array($salesperson_query)) {
                            //     $expense = $row['expense'];
                            //     $total_sales = $row['total_sales'];
                            //     $net_sales = $row['net_sales'];
                            ?>
                                <tr>
                                    <td><?php echo $expenseTotal ?></td>
                                    <td><?php echo $salesTotal ?></td>
                                    <td><?php echo $net_sales ?></td>
                                </tr>
                            <?php
                            
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</section><!-- /.content -->