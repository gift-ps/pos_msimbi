<?php 

if (isset($_POST['add_composite'])) {
	$name = $_POST['name'];
    $discount = $_POST['discount'];
    // Array
    $prod_ids = $_POST['prod_id'];
	
	$query2=mysqli_query($con,"SELECT * FROM product_composites WHERE composite_name = '$name'")or die(mysqli_error($con));
    $count=mysqli_num_rows($query2);

    if ($count > 0){
        echo "<script type='text/javascript'>alert('Composite already exist!');</script>";
        echo "<script>document.location='product_composites.php'</script>";  
    }else{	

        // return var_dump($prod_ids."<br>".$discount.$name);
        
        array_map(function($prod_id){
            global $con, $name, $discount;
            mysqli_query($con, "INSERT INTO product_composites (composite_name, prod_id, discount) VALUES('$name', '$prod_id', '$discount') ")or die("Saving error: ".mysqli_error($con));
        },$prod_ids);


        echo "<script type='text/javascript'>alert('Successfully added new composite!');</script>";
        echo "<script>document.location='product_composites.php'</script>";  
    }

}

if (isset($_POST['delete_composite'])) {
    $composite_name = $_POST['name'];
    mysqli_query($con, "DELETE FROM product_composites WHERE composite_name = '$composite_name'")or die(mysqli_error($con));
    
    echo "<script type='text/javascript'>alert('Successfully Deleted Composite !!');</script>";
    echo "<script>document.location='product_composites.php'</script>";
}

if (isset($_POST['update_composite'])) {

	$name = $_POST['name'];
    $discount = $_POST['discount'];
    // Array
    $prod_ids = $_POST['prod_id'];
    // return var_dump($prod_ids);

    //First Delete all record with same name.
    $delete = mysqli_query($con, "DELETE FROM product_composites WHERE composite_name ='$name' ")or die('An error occured : '.mysqli_error($con));

    // Then add new data
    array_map(function($prod_id){
        global $con, $name, $discount;
        mysqli_query($con, "INSERT INTO product_composites (composite_name, prod_id, discount) VALUES('$name', '$prod_id', '$discount') ")or die("Saving error: ".mysqli_error($con));
    },$prod_ids);
	
	echo "<script type='text/javascript'>alert('Successfully Updated Composite!');</script>";
	echo "<script>document.location='product_composites.php'</script>";  

}



/**
 * WAREHOUSE COMPOSITES SERVER...
 */

if (isset($_POST['add_composite_warehouse'])) {
	$name = $_POST['name'];
    $discount = $_POST['discount'];
    // Array
    $prod_ids = $_POST['prod_id'];
	
	$query2=mysqli_query($con,"SELECT * FROM product_composites_warehouse WHERE composite_name = '$name'")or die(mysqli_error($con));
    $count=mysqli_num_rows($query2);

    if ($count > 0){
        echo "<script type='text/javascript'>alert('Composite already exist!');</script>";
        echo "<script>document.location='product_composites_warehouse.php'</script>";  
    }else{	

        // return var_dump($prod_ids."<br>".$discount.$name);
        
        array_map(function($prod_id){
            global $con, $name, $discount;
            mysqli_query($con, "INSERT INTO product_composites_warehouse (composite_name, prod_id, discount) VALUES('$name', '$prod_id', '$discount') ")or die("Saving error: ".mysqli_error($con));
        },$prod_ids);


        echo "<script type='text/javascript'>alert('Successfully added new warehouse composite!');</script>";
        echo "<script>document.location='product_composites_warehouse.php'</script>";  
    }

}

if (isset($_POST['delete_composite_warehouse'])) {
    $composite_name = $_POST['name'];
    mysqli_query($con, "DELETE FROM product_composites_warehouse WHERE composite_name = '$composite_name'")or die(mysqli_error($con));
    
    echo "<script type='text/javascript'>alert('Successfully Deleted Warehouse Composite !!');</script>";
    echo "<script>document.location='product_composites_warehouse.php'</script>";
}

if (isset($_POST['update_composite_warehouse'])) {

	$name = $_POST['name'];
    $discount = $_POST['discount'];
    // Array
    $prod_ids = $_POST['prod_id'];
    // return var_dump($prod_ids);

    //First Delete all record with same name.
    $delete = mysqli_query($con, "DELETE FROM product_composites_warehouse WHERE composite_name ='$name' ")or die('An error occured : '.mysqli_error($con));

    // Then add new data
    array_map(function($prod_id){
        global $con, $name, $discount;
        mysqli_query($con, "INSERT INTO product_composites_warehouse (composite_name, prod_id, discount) VALUES('$name', '$prod_id', '$discount') ")or die("Saving error: ".mysqli_error($con));
    },$prod_ids);
	
	echo "<script type='text/javascript'>alert('Successfully Updated Warehouse Composite!');</script>";
	echo "<script>document.location='product_composites_warehouse.php'</script>";  

}
