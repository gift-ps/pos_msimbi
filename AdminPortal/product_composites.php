<?php
session_start();
if (empty($_SESSION['id'])):
    header('Location:../index.php');
endif;

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Product Composites | <?php include('../dist/includes/title.php'); ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
                <!-- Select2 -->
                <link rel="stylesheet" href="../plugins/select2/select2.min.css">
        
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
        <style>
            select {
                display: block;
                width: 100%;
                height: 35px;
            }
        </style>

  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

    </head>
    <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
    <body class="hold-transition skin-<?php echo $_SESSION['skin']; ?> layout-top-nav">
        <div class="wrapper">
            <?php
            include('../dist/includes/header_admin.php');
            include('../dist/includes/dbcon.php');
            ?>
            <!-- Full Width Column -->
            <div class="content-wrapper">
                <div class="container">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            <a class="btn btn-lg btn-warning" href="home.php">Back</a>

                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">Product Composites</li>
                        </ol>
                    </section>

                    <!-- Main content -->
                    <section class="content">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title" style=" color: black"><b>Add New Composite</b></h3>
                                    </div>
                                    <div class="box-body">
                                        <?php include('product_composite_server.php') ?>
                                        <form autocomplete="off" method="post" action="#" enctype="multipart/form-data">
                                        
                                            <div class="form-group">
                                                <label for="date" style=" color: black"><b> Composite Name </b> </label>
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control pull-right" id="date" name="name" placeholder="Composite Name" required>
                                                </div><!-- /.input group -->
                                            </div><!-- /.form group -->
                                            <?php
                                                function products_options($con){
                                                    $output = '';
                                                    $query = mysqli_query($con, "SELECT prod_id, prod_name FROM product ORDER BY prod_name ASC ")or die(mysqli_error($con));
                                                    while ($row = mysqli_fetch_array($query)) {
                                                        $output .= '<option value="'.$row["prod_id"].'">'.$row["prod_name"].'</option>';
                                                    }
                                                    return $output;
                                                }
                                            ?>
                                            <!-- Add necessary scripts for adding products to composite -->

                                            <div class="form-group">
                                                <label for="date" style=" color: black"><b> Products </b> </label>
                                                <div class="input-group col-md-12">
                                                    <table class="table table-bordered" id="item_table">
                                                        <tr>
                                                            <th>Click to add product </th>
                                                            <th><button type="button" name="add" class="btn btn-success btn-sm add"><span class="glyphicon glyphicon-plus"></span></button></th>
                                                        </tr>
                                                    </table> 
                                                </div><!-- /.input group -->
                                            </div><!-- /.form group -->

                                            <div class="form-group">
                                                <label style="color: black"><b> Discount </b> <small> (in percentage) </small> </label>
                                                <div class="input-group col-md-12">
                                                    <input type="number" min="0" max="100" class="form-control pull-right" name="discount" placeholder="Discount (%)" required>
                                                </div><!-- /.input group -->
                                            </div><!-- /.form group -->

                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="submit" value="Add" name="add_composite" class="form-control btn btn-primary btn-sm" />
                                                </div>
                                            </div><!-- /.form group -->
                                        </form>	
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                            </div><!-- /.col (right) -->

                            <div class="col-xs-8">
                                <div class="box box-primary">

                                    <div class="box-header">
                                        <h3 class="box-title" style=" color: black"><b>Product Composites List</b></h3>
                                    </div><!-- /.box-header -->
                                    <div class="box-body">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Composite Name</th>
                                                    <th>Discount (%)</th>
                                                    <th>Products</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                // Get all individual composites and list then seperately, but as one entity each
                                                $query = mysqli_query($con, "SELECT * FROM product_composites GROUP BY composite_name ")or die(mysqli_error($con));
                                                while ($row = mysqli_fetch_array($query)) {
                                                    $composite_name = $row['composite_name'];
                                                    $discount = $row['discount'];
                                                    $no_space_comp_name = preg_replace('/\s+/', '', $composite_name);
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $composite_name ?></td>
                                                        <td><?php echo $discount; ?></td>
                                                        <td><?php 
                                                            $query_pro1 = mysqli_query($con, "SELECT * FROM product_composites WHERE composite_name = '$composite_name' ")or die(mysqli_error($con));
                                                            while ($row_pro1 = mysqli_fetch_array($query_pro1)) {
                                                                $prod_id = $row_pro1['prod_id'];
                                                                $query_pro = mysqli_query($con, "SELECT prod_name 
                                                                                FROM product WHERE prod_id = $prod_id ")or die(mysqli_error($con));
                                                                while ($row_pro = mysqli_fetch_array($query_pro)) {
                                                                    $prod_name = $row_pro['prod_name'];
                                                                    echo " - ".$prod_name." <br>";
                                                                }
                                                            }
                                                        ?></td>

                                                        <td>
                                                            <a href="product_composite_update.php?name=<?php echo $composite_name; ?>" > <i class="glyphicon glyphicon-edit text-blue"></i> </a>
                                                            <a href="#delete<?php echo $no_space_comp_name; ?>" data-target="#delete<?php echo $no_space_comp_name; ?>" data-toggle="modal" style="color:#fff;" class="small-box-footer"><i class="glyphicon glyphicon-trash text-blue"></i></a>

                                                        </td>
                                                    </tr>

                                                <div id="delete<?php echo $no_space_comp_name; ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content" style="height:auto">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title">Are you sure you want to delete this??
                                                                    <br style=" color: red">
                                                                    Deleting this item may affect other data related to it.!!!!
                                                                    </br>
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body" hidden="">
                                                                <form autocomplete="off" class="form-horizontal" method="post" action="#" enctype='multipart/form-data'>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-lg-3" for="name">Category</label>
                                                                        <div class="col-lg-9">
                                                                            <input type="hidden" class="form-control" id="id" name="name" value="<?php echo $composite_name; ?>" required>  
                                                                            <input type="text" class="form-control" id="name" name="id" value="<?php echo $row['id']; ?>" required>  
                                                                        </div>
                                                                    </div> 
                                                            </div><hr>
                                                            <div class="modal-footer">
                                                                <input type="submit" value="Delete" name="delete_composite" class="btn btn-danger" />
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            </div>
                                                            </form>
                                                        </div>

                                                    </div><!--end of modal-dialog-->
                                                </div>
                                                <!--end of modal-->                    
                                            <?php } ?>					  
                                            </tbody>

                                        </table>
                                    </div><!-- /.box-body -->

                                </div><!-- /.col -->

                            </div> <!-- box box-primary -->
                        </div><!-- /.row -->

                                
                    </section><!-- /.content -->
                </div><!-- /.container -->
            </div><!-- /.content-wrapper -->
            <?php include('../dist/includes/footer.php'); ?>
        </div><!-- ./wrapper -->

        <!-- jQuery 2.1.4 -->
        <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../plugins/fastclick/fastclick.min.js"></script>
        <!-- AdminLTE App -->
        <script src="../dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../dist/js/demo.js"></script>
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>

        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });
        </script>


    <script src="../plugins/jQuery/jQuery-2.2.0.min.js"></script>       
    <script src="../bootstrap/js/bootstrap.min.js"></script>      
    <script src="../plugins/select2/select2.full.min.js"></script>       
    <script src="../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="../plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>





    </body>
</html>

<script>
  $(document).ready(function(){
      $(document).on('click', '.add', function(){
        var html = '';
        html += '<tr>';
        html += '<td><select name="prod_id[]" class="form-control item_unit select2"><option></option><?php echo products_options($con); ?></select></td>';
        html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><span class="glyphicon glyphicon-minus"></span></button></td></tr>';
        $('#item_table').append(html);
      });
  
      $(document).on('click', '.remove', function(){
        $(this).closest('tr').remove();
      });
  
  });

    //Initialize Select2 Elements
    $(".select2").select2();
</script>
<script src="../plugins/select2/select2.full.min.js"></script>       
