<?php

session_start();
$branch = $_SESSION['branch'];
include('../dist/includes/dbcon.php');

include('../dist/includes/variant_validator.php');


$name = $_POST['prod_name'];
$price = $_POST['prod_price'];
$desc = $_POST['prod_desc'];
$supplier = $_POST['supplier'];
$reorder = "Non";
$category = $_POST['category'];
$serial = "Non";
$prod_qty = $_POST['prod_qty'];
$belongs_to = $_POST['belongs_to'];
$prod_sell_price = $_POST['prod_sell_price'];
$stock_branch_id = $_POST['stock_branch_id'];
$ware_id = $_POST['ware_id'];
$userid = $_SESSION['id'];
$barcode = $_POST['barcode'];
$currency_id = $_POST['currency'];
$vat_status = $_POST['vat_status'];

$query2 = mysqli_query($con, "select * from product where prod_name='$name' and branch_id='$branch' AND stock_branch_id='$stock_branch_id'")or die(mysqli_error($con));
$count = mysqli_num_rows($query2);

if ($count > 0) {
    // update the inventory stock..
    mysqli_query($con, "UPDATE product SET prod_qty=prod_qty + '$prod_qty' where prod_name='$name' AND stock_branch_id='$stock_branch_id' ")or die(mysqli_error($con));

    // Update the warehouse stock..
    mysqli_query($con, "UPDATE ware_house_tb SET prod_qty=prod_qty-'$prod_qty' where prod_id='$ware_id' ")or die(mysqli_error($con));

    // update the stock transfers log.. 
    mysqli_query($con, "INSERT INTO stock_trasfers_tb(prod_id,qty,user_id,moved_to)
            VALUES('$ware_id','$prod_qty','$userid','$stock_branch_id')")or die(mysqli_error($con));
    $transfer_id = mysqli_insert_id($con);

    // Get newly created product ID and create or update it's variants
    $vari_q = mysqli_query($con, "SELECT prod_id FROM product WHERE prod_name = '$name' ")or die(mysqli_error($con));
    $row_vari = mysqli_fetch_array($vari_q);
    $prod_id = $row_vari['prod_id'];

    // return var_dump($prod_id);

    add_variant_warehouse($prod_id);

    echo "<script type='text/javascript'>alert('Product Updated Successfully!');</script>";
    echo "<script>document.location='ware-house-stock.php'</script>";
} else {

    $pic = $_FILES["image"]["name"];
    if ($pic == "") {
        $pic = "default.gif";
    } else {
        $pic = $_FILES["image"]["name"];
        $type = $_FILES["image"]["type"];
        $size = $_FILES["image"]["size"];
        $temp = $_FILES["image"]["tmp_name"];
        $error = $_FILES["image"]["error"];

        if ($error > 0) {
            die("Error uploading file! Code $error.");
        } else {
            if ($size > 100000000000) { //conditions for the file
                die("Format is not allowed or file size is too big!");
            } else {
                move_uploaded_file($temp, "../dist/uploads/" . $pic);
            }
        }
    }

    // insert stock to the stock counts..

    mysqli_query($con, "INSERT INTO product(barcode,prod_name,prod_price,prod_desc,prod_pic,cat_id,reorder,supplier_id,branch_id,serial,prod_qty,belongs_to,prod_sell_price,stock_branch_id,vat_status,currency_id)
            VALUES('$barcode','$name','$price','$desc','$pic','$category','$reorder','$supplier','$branch','$serial','$prod_qty','$belongs_to','$prod_sell_price','$stock_branch_id','$vat_status','$currency_id')")or die(mysqli_error($con));
    $prod_id = mysqli_insert_id($con);

    // update the warehouse with the correct stock count.. 
    mysqli_query($con, "UPDATE ware_house_tb SET prod_qty=prod_qty-'$prod_qty' where prod_id='$ware_id' ")or die(mysqli_error($con));

    mysqli_query($con, "INSERT INTO stock_trasfers_tb(prod_id,qty,user_id,moved_to)
            VALUES('$ware_id','$prod_qty','$userid','$stock_branch_id')")or die(mysqli_error($con));
    
    $transfer_id = mysqli_insert_id($con);
    // Get newly created product ID and create or update it's variants
   
    add_variant_warehouse($prod_id);
    
    mysqli_query($con, "INSERT INTO stock_audit_tb(prod_id,count,added_to,action)
            VALUES('$name','$prod_qty','Warehouse','Transfer')")or die(mysqli_error($con));

    echo "<script type='text/javascript'>alert('Successfully Transfered Product !!');</script>";
    echo "<script>document.location='ware-house-stock.php'</script>";
}





function add_variant_warehouse($prod_id){

    // Variant Variables
    if(isset($_POST['variant_id'])){

        $variant_ids = $_POST['variant_id'];
        $variant_qtys = $_POST['variant_qty'];
        $variant_colors = $_POST['variant_color'];
        $variant_sizes = $_POST['variant_size'];

        // return var_dump($variant_sizes);

        $prod_id = $prod_id;
        array_map(function($variant_qty, $variant_color, $variant_size, $variant_id) {
            global $con, $prod_id, $transfer_id;
            // First check if variant exists...
            $queryck = mysqli_query($con, "SELECT * FROM product_variants WHERE prod_id='$prod_id' AND color = '$variant_color' AND size = '$variant_size' ")or die("Error 2 ".mysqli_error($con));
            $countck = mysqli_num_rows($queryck);
            if ($countck > 0) {
                // update variants in store products
                mysqli_query($con, "UPDATE product_variants SET qty=qty + '$variant_qty' WHERE prod_id='$prod_id' AND color = '$variant_color' AND size = '$variant_size' ")or die(mysqli_error($con));
                // update variants in warehouse products
                mysqli_query($con, "UPDATE product_variants_warehouse SET qty=qty-'$variant_qty' WHERE id='$variant_id' ")or die(mysqli_error($con));
            }else{
                mysqli_query($con, "INSERT INTO `product_variants`(`prod_id`, `color`, `size`, `qty`) VALUES ('$prod_id', '$variant_color', '$variant_size', '$variant_qty' ) ")or die(mysqli_error($con));
                // update variants in warehouse products
                mysqli_query($con, "UPDATE product_variants_warehouse SET qty=qty-'$variant_qty' WHERE id='$variant_id' ")or die(mysqli_error($con));
            }

            // Save variants to transfer table
            add_to_transfer_variants($con, $prod_id, $variant_id, $variant_qty, $transfer_id);

        }, $variant_qtys, $variant_colors, $variant_sizes, $variant_ids);
    }
    
}