<?php
session_start();
if (empty($_SESSION['id'])) :
    header('Location:../index.php');
endif;
if (empty($_SESSION['branch'])) :
    header('Location:../index.php');
endif;

include('../dist/includes/variant_validator.php');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sales Report | <?php include('../dist/includes/title.php'); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="../plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="../plugins/iCheck/all.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="../plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="../plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="../plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- Export to PDF DATATables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
                folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <script type="text/javascript" src="../dist/js/jquery.min.js"></script>
    <script type="text/javascript" src="../dist/js/moment.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../dist/css/bootstrap.css" />

    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="../plugins/daterangepicker/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../plugins/daterangepicker/daterangepicker.css" />

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="../plugins/datatables/table-exporter.js"></script>
    <script type="text/javascript" src="js_chart/jquery.min.js"></script>
    <script type="text/javascript" src="js_chart/Chart.min.js"></script>

    <?php
    if (isset($_POST['display'])) {

        $date = $_POST['date'];
        $date = explode('-', $date);
        $branch = $_SESSION['branch'];
        $start = date("Y-m-d", strtotime($date[0]));
        $startDate = $start . " 00:00:00";
        $end = date("Y-m-d", strtotime($date[1]));
        $endDate = $end . " 00:00:00";
        $stop_date = date('Y-m-d H:i:s', strtotime($endDate . ' +1 day'));


        $_SESSION['startDate'] = $startDate;
        $_SESSION['stop_date'] = $stop_date;
    } else {
        $_SESSION['startDate'] = "today";
    }
    ?>

    <script>
        $(document).ready(function() {
            showGraph();
        });

        function showGraph() {
            {
                $.post("data.php",
                    function(data) {
                        console.log(data);
                        var name = [];
                        var marks = [];

                        for (var i in data) {
                            name.push(data[i].prod_name);
                            marks.push(data[i].Total);
                        }

                        var chartdata = {
                            labels: name,
                            datasets: [{
                                label: 'Sales By Product Insight',
                                backgroundColor: '#3c8dbc',
                                borderColor: '#yellow',
                                hoverBackgroundColor: '#3c8dbc',
                                hoverBorderColor: '#3c8dbc',
                                data: marks
                            }]
                        };

                        var graphTarget = $("#graphCanvas");

                        var barGraph = new Chart(graphTarget, {
                            type: 'line',
                            data: chartdata
                        });
                    });
            }
        }
    </script>

    <style type="text/css">
        h5,
        h6 {
            text-align: center;
        }

        @media print {
            .btn-print {
                display: none !important;
            }

            .main-footer {
                display: none !important;
            }

            .box.box-primary {
                border-top: none !important;
            }

            .angel {
                display: none !important;
            }
        }
    </style>
</head>

<body class="hold-transition skin-<?php echo $_SESSION['skin']; ?> layout-top-nav">
    <div class="wrapper">
        <?php
        include('../dist/includes/header_admin.php');
        include('../Objects/Objects.php');
        $Objects = new InvObjects();

        include('DAO.php');
        $DAO = new DAO();
        ?>
        <div class="content-wrapper">
            <div class="container">
                <section class="content">
                    <div class="col-md-20">
                        <div class="box box-primary angel">
                            <div class="box-header">
                                <h3 class="box-title">Filter Report By Date Period</h3>
                            </div>
                            <div class="box-body">
                                <form autocomplete="off" method="post">
                                    <div class="form-group col-md-2">
                                        <label>Select Report Dates</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="date" class="form-control pull-right active" id="reservation" required autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <label>Select Branch</label>
                                        <select class="form-control select2" style="width: 100%;" name="branch_id" required>
                                            <option value="all_branches">All Branches</option>
                                            <?php
                                            $queryc = mysqli_query($con, "select * from stores_branch order by branch_name") or die("Error 11 " . mysqli_error($con));
                                            while ($rowc = mysqli_fetch_array($queryc)) {
                                            ?>
                                                <option value="<?php echo $rowc['id']; ?>"><?php echo $rowc['branch_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-2" hidden="">
                                        <label>Select Category</label>
                                        <select class="form-control select2" style="width: 100%;" name="cat_id" required>
                                            <option value="all">All Categories</option>
                                            <?php
                                            $queryc = mysqli_query($con, "select * from category order by cat_name") or die("Error 13 " . mysqli_error($con));
                                            while ($rowc = mysqli_fetch_array($queryc)) {
                                            ?>
                                                <option value="<?php echo $rowc['cat_id']; ?>"><?php echo $rowc['cat_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-2" hidden="">
                                        <label>Mode Payment</label>
                                        <select class="form-control select2" style="width: 100%;" name="payment_mode_id" required>
                                            <option value="All">All Modes Of Payment</option>
                                            <?php
                                            $queryc = mysqli_query($con, "select * from modes_of_payment_tb order by name") or die("Error 12 " . mysqli_error($con));
                                            while ($rowc = mysqli_fetch_array($queryc)) {
                                            ?>
                                                <option value="<?php echo $rowc['payment_mode_id']; ?>"><?php echo $rowc['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <button type="submit" class="btn btn-primary" name="display"> Generate Report </button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <?php 
                        $date_filter = "AND DATE(sales.date_added) BETWEEN DATE(NOW()) AND DATE(NOW())";
                        $where = "WHERE DATE(sales.date_added) BETWEEN DATE(NOW()) AND DATE(NOW())";
                        $date_filter_part_pay = "WHERE date_added BETWEEN DATE(NOW()) AND DATE(NOW()) ";
                        $prev_week = "WHERE WEEK (date_added) = WEEK( current_date ) - 1 AND YEAR( date_added) = YEAR( current_date )";
                    ?>

                    <?php include('sales_dashboard.php'); ?>

                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title" style=" color: black"><b>Sales Report</b></h3>
                                    </div>
                                    <div id="chart-container" style="align-content: center;height: 400px ">
                                        <center><canvas id="graphCanvas"></canvas>
                                            <center>
                                    </div>
                                    <br><br>
                                    <br><br><br><br><br><br>

                                    <?php
                                    if (isset($_POST['display'])) {
                                        $_SESSION['sales_date'] = $_POST['date'];
                                    } else {
                                        unset($_SESSION['sales_date']);
                                    }
                                    ?>
                                    <div class="box-body">
                                        <br></br>
                                        <?php
                                        if (isset($_POST['display'])) {

                                            $date = $_POST['date'];
                                            $date = explode('-', $date);
                                            $branch = $_SESSION['branch'];
                                            $start = date("Y-m-d", strtotime($date[0]));
                                            $startDate = $start . " 00:00:00";
                                            $end = date("Y-m-d", strtotime($date[1]));
                                            $endDate = $end . " 00:00:00";
                                            $stop_date = date('Y-m-d H:i:s', strtotime($endDate . ' +1 day'));
                                            //$supplier_id = $_POST['supplier_id'];

                                            echo ' <a href="escpos-php-development/example/interface/print-sales.php?fromdate=' . $startDate . '&enddate=' . $stop_date . '   ">Print Report</a>';
                                        } else {
                                            //echo ' <a href="escpos-php-development/example/interface/print-sales.php">Print Report</a>';
                                        }
                                        ?>
                                        <table id="example2" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Product Sold</th>


                                                    <th>Sold From</th>
                                                    <th>Date Sold</th>
                                                    <th>Sold By</th>
                                                    <th>Mode Of Payment</th>
                                                    <th>Quantity Sold</th>
                                                    <th>Variants</th>
                                                    <th>Cost Price</th>
                                                    <th>Sell Price</th>
                                                    <th>VAT</th>
                                                    <th>NONE VAT</th>
                                                    <th>Profit Loss Margin</th>
                                                    <th>Total Collected</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $totalSold = 0;
                                                $totalAmountCollected = 0;
                                                $vatFinalTotal = 0;
                                                $vatFreeTotal = 0;
                                                $vatFree = 0;
                                                $Vat = 0;
                                                $discountTotal = 0;
                                                $discountTotal3 = 0;
                                                $totalProfit = 0;

                                                if (isset($_POST['display'])) {

                                                    $date = $_POST['date'];
                                                    $date = explode('-', $date);
                                                    $branch = $_SESSION['branch'];
                                                    $start = date("Y-m-d", strtotime($date[0]));
                                                    $startDate = $start . " 00:00:00";
                                                    $end = date("Y-m-d", strtotime($date[1]));
                                                    $endDate = $end . " 00:00:00";
                                                    $stop_date = date('Y-m-d H:i:s', strtotime($endDate . ' +1 day'));
                                                    $cat_id = $_POST['cat_id'];
                                                    $branch_id = $_POST['branch_id'];
                                                    $payment_mode_id = $_POST['payment_mode_id'];

                                                    if ($cat_id != 'all' && $branch_id != 'all_branches') {
                                                        echo '2';
                                                        $query = mysqli_query($con, "SELECT product.prod_id, sales_details.cost_price,prod_price,sales.sales_id,vat_status,modes_of_payment_tb.name AS modepayment,sales_details.branch_id AS stock_branch_id,SUM(qty) AS qty,
                                                                prod_name,prod_desc,user.name,sales_details.price AS prod_sell_price,sales.date_added  FROM sales_details INNER JOIN sales
                                                                ON sales.sales_id=sales_details.sales_id 
                                                                INNER JOIN user ON user.user_id = sales.user_id
                                                                INNER JOIN product ON product.prod_id = sales_details.prod_id
                                                                inner join stores_branch on stores_branch.id=sales_details.branch_id
                                                                INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment AND 
                                                                sales.date_added BETWEEN '$startDate' AND '$stop_date' AND sales_details.branch_id='$branch_id'
                                                                AND product.cat_id='$cat_id' GROUP BY prod_name,sales_details.branch_id,modes_of_payment_tb.payment_mode_id,sales_details.price,sales_details.cost_price") or die("Err 1 " . mysqli_error($con));



                                                        $queryPayments = mysqli_query($con, "SELECT modes_of_payment_tb.name AS modepayment,SUM(sales.amount_due) AS total  FROM sales   
                                                            INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment 
                                                            INNER JOIN user on user.user_id=sales.user_id
                                                            WHERE  sales.date_added BETWEEN '$startDate' AND '$stop_date' AND sales.branch_id='$branch_id'
                                                            AND sales_id NOT IN (SELECT sales_id from split_payments WHERE DATE(sales.date_added)=DATE(NOW()))        
                                                            GROUP BY modes_of_payment_tb.payment_mode_id") or die("Err 6 " . mysqli_error($con));

                                                        $salesTotals = mysqli_query($con, "SELECT SUM(amount_due) as amount_due FROM `sales` WHERE date_added BETWEEN '$startDate' AND '$stop_date' AND sales.branch_id='$branch_id'  ") or die("Err 9 " . mysqli_error($con));
                                                    } else if ($cat_id == 'all' && $branch_id != 'all_branches') {
                                                        // return var_dump($branch_id);
                                                        echo '3 ' . $branch_id;
                                                        /*
                                                                          $query = mysqli_query($con, "SELECT product.prod_id, sales_details.cost_price,prod_price,sales.sales_id,vat_status,modes_of_payment_tb.name AS modepayment,sales_details.branch_id AS stock_branch_id,SUM(qty)
                                                                          AS qty,prod_name,prod_desc,user.name,sales_details.price AS prod_sell_price,sales.date_added
                                                                          FROM sales_details INNER JOIN sales ON sales.sales_id=sales_details.sales_id
                                                                          INNER JOIN user ON user.user_id = sales.user_id
                                                                          INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment
                                                                          INNER JOIN product ON product.prod_id = sales_details.prod_id
                                                                          inner join stores_branch on stores_branch.id=sales_details.branch_id
                                                                          AND sales.date_added BETWEEN '$startDate' AND '$stop_date' AND sales_details.branch_id='$branch_id'
                                                                          GROUP BY prod_name,sales_details.branch_id,modes_of_payment_tb.payment_mode_id,sales_details.price,sales_details.cost_price ")or die("Err 2 " . mysqli_error($con));
                                                                         */


                                                        $query = mysqli_query($con, "SELECT  sales_details.cost_price,sales.sales_id,modes_of_payment_tb.name AS modepayment,sales_details.branch_id AS stock_branch_id,SUM(qty)
                                                                AS qty,user.name,sales_details.price AS prod_sell_price,sales.date_added, sales_details.prod_id
                                                                FROM sales_details INNER JOIN sales ON sales.sales_id=sales_details.sales_id
                                                                INNER JOIN user ON user.user_id = sales.user_id
                                                                INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment
                                                               
                                                                inner join stores_branch on stores_branch.id=sales_details.branch_id
                                                                AND sales.date_added BETWEEN '$startDate' AND '$stop_date'   AND sales_details.branch_id='$branch_id'
                                                                
                                                                
                                                                 GROUP BY sales_details.branch_id,modes_of_payment_tb.payment_mode_id,sales_details.price,sales_details.cost_price ") or die("Err 2 " . mysqli_error($con));


                                                        $queryPayments = mysqli_query($con, "SELECT modes_of_payment_tb.name AS modepayment,SUM(sales.amount_due) AS total  FROM sales   
                                                            INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment 
                                                            INNER JOIN user on user.user_id=sales.user_id
                                                            WHERE  sales.date_added BETWEEN '$startDate' AND '$stop_date' AND sales.branch_id='$branch_id'
                                                            AND sales_id NOT IN (SELECT sales_id from split_payments WHERE DATE(sales.date_added)=DATE(NOW()))        
                                                            GROUP BY modes_of_payment_tb.payment_mode_id") or die("Err 6 " . mysqli_error($con));

                                                        //  echo '6 '.$branch_id;

                                                        $querySplitPayments = mysqli_query($con, "SELECT split_payments.date_added,split_payments.amount AS total,modes_of_payment_tb.name AS modepayment FROM `split_payments`
                                                    INNER JOIN sales on sales.sales_id=split_payments.sales_id
                                                    INNER JOIN user on user.user_id=sales.user_id
                                                    INNER JOiN modes_of_payment_tb on modes_of_payment_tb.payment_mode_id=split_payments.payment_id
                                                    WHERE sales.date_added BETWEEN '$startDate' AND '$stop_date' AND sales.branch_id='$branch_id'  ") or die("Err 7 " . mysqli_error($con));

                                                        $salesTotals = mysqli_query($con, "SELECT SUM(amount_due) as amount_due FROM `sales` WHERE date_added BETWEEN '$startDate' AND '$stop_date' AND sales.branch_id='$branch_id'  ") or die("Err 9 " . mysqli_error($con));
                                                    } else if ($cat_id != 'all' && $branch_id == 'all_branches') {
                                                        echo '4';
                                                        $query = mysqli_query($con, " SELECT product.prod_id, sales_details.cost_price,prod_price,sales.sales_id,vat_status,modes_of_payment_tb.name AS modepayment,sales_details.branch_id AS stock_branch_id,SUM(qty) AS qty,prod_name,
                                                                    prod_desc,user.name,sales_details.price 
                                                                    AS prod_sell_price,sales.date_added  FROM sales_details INNER JOIN sales ON sales.sales_id=sales_details.sales_id
                                                                    INNER JOIN user ON user.user_id = sales.user_id INNER JOIN product ON product.prod_id = sales_details.prod_id
                                                                    INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment
                                                                    inner join stores_branch on stores_branch.id=sales_details.branch_id
                                                                    AND sales.date_added BETWEEN '$startDate' AND '$stop_date' AND product.cat_id='$cat_id' 
                                                                    GROUP BY prod_name ,sales_details.branch_id,modes_of_payment_tb.payment_mode_id,sales_details.price,sales_details.cost_price ") or die("Err 3 " . mysqli_error($con));

                                                        $queryPayments = mysqli_query($con, "SELECT modes_of_payment_tb.name AS modepayment,SUM(sales.amount_due) AS total  FROM sales   
                                                            INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment 
                                                            INNER JOIN user on user.user_id=sales.user_id
                                                            WHERE  sales.date_added BETWEEN '$startDate' AND '$stop_date'   
                                                            AND sales_id NOT IN (SELECT sales_id from split_payments WHERE DATE(sales.date_added)=DATE(NOW()))        
                                                            GROUP BY modes_of_payment_tb.payment_mode_id") or die("Err 6 " . mysqli_error($con));

                                                        $salesTotals = mysqli_query($con, "SELECT SUM(amount_due) as amount_due FROM `sales` WHERE date_added BETWEEN '$startDate' AND '$stop_date' AND sales.modeofpayment='$payment_mode_id'  ") or die("Err 9 " . mysqli_error($con));
                                                    } else if ($payment_mode_id != 'All') {
                                                        echo '5';
                                                        $query = mysqli_query($con, " SELECT product.prod_id, sales_details.cost_price,prod_price,sales.sales_id,vat_status,modes_of_payment_tb.name AS modepayment,sales_details.branch_id AS stock_branch_id,SUM(qty) "
                                                            . "AS qty,prod_name,prod_desc,user.name,sales_details.price AS prod_sell_price,sales.date_added "
                                                            . " FROM sales_details INNER JOIN sales ON sales.sales_id=sales_details.sales_id "
                                                            . "INNER JOIN user ON user.user_id = sales.user_id "
                                                            . " INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment "
                                                            . "INNER JOIN product ON product.prod_id = sales_details.prod_id "
                                                            . "inner join stores_branch on stores_branch.id=sales_details.branch_id"
                                                            . "AND sales.date_added BETWEEN '$startDate' AND '$stop_date' AND sales.modeofpayment='$payment_mode_id' "
                                                            . " GROUP BY prod_name,sales_details.branch_id,modes_of_payment_tb.payment_mode_id,sales_details.price,sales_details.cost_price ") or die("Err 4 " . mysqli_error($con));
                                                        $queryPayments = mysqli_query($con, "SELECT modes_of_payment_tb.name AS modepayment,SUM(sales.amount_due) AS total  FROM sales   
                                                            INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment 
                                                            INNER JOIN user on user.user_id=sales.user_id
                                                            WHERE  sales.date_added BETWEEN '$startDate' AND '$stop_date'   AND sales.modeofpayment='$payment_mode_id'
                                                            AND sales_id NOT IN (SELECT sales_id from split_payments WHERE DATE(sales.date_added)=DATE(NOW()))        
                                                            GROUP BY modes_of_payment_tb.payment_mode_id") or die("Err 6 " . mysqli_error($con));

                                                        $salesTotals = mysqli_query($con, "SELECT SUM(amount_due) as amount_due FROM `sales` WHERE date_added BETWEEN '$startDate' AND '$stop_date' AND sales.modeofpayment='$payment_mode_id'  ") or die("Err 9 " . mysqli_error($con));
                                                    } else {

                                                        echo '6';



                                                        $query = mysqli_query($con, "SELECT  sales_details.cost_price,sales.sales_id,modes_of_payment_tb.name AS modepayment,sales_details.branch_id AS stock_branch_id,SUM(qty)
                                                                AS qty,user.name,sales_details.price AS prod_sell_price,sales.date_added, sales_details.prod_id
                                                                FROM sales_details INNER JOIN sales ON sales.sales_id=sales_details.sales_id
                                                                INNER JOIN user ON user.user_id = sales.user_id
                                                                INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment
                                                               
                                                                inner join stores_branch on stores_branch.id=sales_details.branch_id
                                                                AND sales.date_added BETWEEN '$startDate' AND '$stop_date'   
                                                                
                                                                
                                                                 GROUP BY sales_details.branch_id,modes_of_payment_tb.payment_mode_id,sales_details.price,sales_details.cost_price ") or die("Err 2 " . mysqli_error($con));


                                                        $queryPayments = mysqli_query($con, "SELECT modes_of_payment_tb.name AS modepayment,SUM(sales.amount_due) AS total  FROM sales   
                                                            INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment 
                                                            INNER JOIN user on user.user_id=sales.user_id
                                                            WHERE  sales.date_added BETWEEN '$startDate' AND '$stop_date' 
                                                            AND sales_id NOT IN (SELECT sales_id from split_payments WHERE DATE(sales.date_added)=DATE(NOW()))        
                                                            GROUP BY modes_of_payment_tb.payment_mode_id") or die("Err 6 " . mysqli_error($con));

                                                        //  echo '6 '.$branch_id;

                                                        $querySplitPayments = mysqli_query($con, "SELECT split_payments.date_added,split_payments.amount AS total,modes_of_payment_tb.name AS modepayment FROM `split_payments`
                                                    INNER JOIN sales on sales.sales_id=split_payments.sales_id
                                                    INNER JOIN user on user.user_id=sales.user_id
                                                    INNER JOiN modes_of_payment_tb on modes_of_payment_tb.payment_mode_id=split_payments.payment_id
                                                    WHERE sales.date_added BETWEEN '$startDate' AND '$stop_date'  ") or die("Err 7 " . mysqli_error($con));

                                                        $salesTotals = mysqli_query($con, "SELECT SUM(amount_due) as amount_due FROM `sales` WHERE date_added BETWEEN '$startDate' AND '$stop_date' ") or die("Err 9 " . mysqli_error($con));
                                                    }

                                                    // $DiscountQuery = mysqli_query($con, "SELECT  sales_details.discount, sales_details.discount_type  FROM sales_details INNER JOIN sales on sales.sales_id=sales_details.sales_id AND  sales_details.discount_type!='' AND sales.date_added BETWEEN '$startDate' AND '$stop_date'")or die("Error 10 " . mysqli_error($con));

                                                    $partPayments = mysqli_query($con, "SELECT SUM(amount) AS amount FROM `part_payments_tb` WHERE date_added BETWEEN '$startDate' AND '$stop_date'  ") or die("Err 10 " . mysqli_error($con));

                                                    $partPaymentsByModes = mysqli_query($con, "SELECT SUM(amount) AS amount,name FROM `part_payments_tb`
                                                    LEFT JOIn modes_of_payment_tb on modes_of_payment_tb.payment_mode_id=part_payments_tb.payment_mode_id
                                                    WHERE part_payments_tb.date_added BETWEEN '$startDate' AND '$stop_date' 
                                                    GROUP BY part_payments_tb.payment_mode_id") or die("Err 8 " . mysqli_error($con));

                                                    // get the total amount tendered..

                                                    echo ' <center><h3 class="box-title" style=" color: black"><b><u>Sales Report from ' . $start . ' to ' . $end . '</u></b></h3></center>';
                                                } else {
                                                    $query = mysqli_query($con, "SELECT product.prod_id, sales_details.cost_price,prod_price,sales.sales_id,vat_status,modes_of_payment_tb.name AS modepayment,
                                                     sales_details.branch_id AS stock_branch_id,SUM(qty) AS qty,prod_name,prod_desc,user.name,sales_details.price AS prod_sell_price,sales.date_added  
                                                    FROM sales_details INNER JOIN sales ON sales.sales_id=sales_details.sales_id                                                       
                                                    INNER JOIN user ON user.user_id = sales.user_id INNER JOIN product ON product.prod_id = sales_details.prod_id
                                                    INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment 
                                                    inner join stores_branch on stores_branch.id=sales_details.branch_id
                                                    AND DATE(sales.date_added) = DATE(NOW()) 
                                                    GROUP BY prod_name,stock_branch_id,modes_of_payment_tb.payment_mode_id,sales_details.price,sales_details.cost_price") or die("Err 11 " . mysqli_error($con));

                                                    $queryPayments = mysqli_query($con, "SELECT modes_of_payment_tb.name AS modepayment,SUM(sales.amount_due) AS total  FROM sales   
                                                    INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment 
                                                    INNER JOIN user on user.user_id=sales.user_id
                                                    WHERE DATE(sales.date_added)=DATE(NOW())     
                                                    AND sales_id NOT IN (SELECT sales_id from split_payments WHERE DATE(sales.date_added)=DATE(NOW()))
                                                    GROUP BY modes_of_payment_tb.payment_mode_id") or die("Err 12 " . mysqli_error($con));

                                                    $partPayments = mysqli_query($con, "SELECT SUM(amount) AS amount FROM `part_payments_tb` WHERE DATE(date_added)= DATE(NOW())") or die("Err 13 " . mysqli_error($con));

                                                    /*
                                                                      $DiscountQuery = mysqli_query($con, "SELECT  sales_details.discount, sales_details.discount_type"
                                                                      . "  FROM sales_details INNER JOIN sales on sales.sales_id=sales_details.sales_id AND"
                                                                      . "  sales_details.discount_type!='' AND DATE(sales.date_added) = DATE(NOW())")or die("Err 17 " . mysqli_error($con));
                                                                     */

                                                    $salesTotals = mysqli_query($con, "SELECT SUM(amount_due) as amount_due FROM `sales` WHERE DATE(date_added) = DATE(NOW())") or die("Err 14 " . mysqli_error($con));

                                                    $partPaymentsByModes = mysqli_query($con, "SELECT SUM(amount) AS amount,name FROM `part_payments_tb`
                                                    LEFT JOIn modes_of_payment_tb on modes_of_payment_tb.payment_mode_id=part_payments_tb.payment_mode_id
                                                    WHERE DATE(part_payments_tb.date_added)=DATE(NOW())
                                                    GROUP BY part_payments_tb.payment_mode_id") or die("Err 15 " . mysqli_error($con));

                                                    $querySplitPayments = mysqli_query($con, "SELECT split_payments.date_added,split_payments.amount AS total,modes_of_payment_tb.name AS modepayment FROM `split_payments`
                                                    INNER JOIN sales on sales.sales_id=split_payments.sales_id
                                                    INNER JOIN user on user.user_id=sales.user_id
                                                    INNER JOin modes_of_payment_tb on modes_of_payment_tb.payment_mode_id=split_payments.payment_id
                                                    WHERE DATE(sales.date_added)=DATE(NOW())  ") or die("Err 16 " . mysqli_error($con));

                                                    echo ' <center><h3 class="box-title" style=" color: black"><b><u>Todays Sales Report</u></b></h3></center><br>';
                                                }
                                                while ($row = mysqli_fetch_array($query)) {

                                                    $product_id = $row['prod_id'];
                                                    $stock_branch_id = $row['stock_branch_id'];
                                                    $sales_id = $row['sales_id'];
                                                    // return var_dump(]);

                                                    $DiscountQuery = mysqli_query($con, "SELECT sales_details.branch_id,sales_details.discount, sales_details.discount_type,SUM(qty) AS qty, sales_details.price AS prod_sell_price 
                                                                        FROM sales_details WHERE 
                                                                        sales_details.sales_id='$sales_id' AND sales_details.branch_id='$stock_branch_id'"
                                                        . " AND sales_details.discount_type!=''  GROUP BY sales_details.branch_id,sales_details.prod_id") or die(mysqli_error($con));

                                                    $totalAmountCollected += $totalSold;
                                                    //$sales_id = $row['sales_id'];

                                                    $costPrice = $row['prod_price'];

                                                    $queryProducts = mysqli_query($con, "SELECT * FROM `product` WHERE prod_id='$product_id' AND stock_branch_id='$stock_branch_id' ") or die("Err 16 " . mysqli_error($con));
                                                    $productRow = mysqli_fetch_array($queryProducts);
                                                    $totalSold = $row['qty'] * $productRow['prod_sell_price'];

                                                    while ($discountsRow = mysqli_fetch_array($DiscountQuery)) {

                                                        if ($discountsRow['discount_type'] != "") {
                                                            if ($discountsRow['discount_type'] == "Percentage") {
                                                                $discountTotal = ($discountsRow['discount'] / 100) * $productRow['prod_sell_price'];
                                                                //$newPrice = $price - $computedPrice;
                                                                $discountTotal3 += ($discountsRow['discount'] / 100) * $productRow['prod_sell_price'];
                                                            } else {
                                                                $discountTotal = $discountsRow['discount'];
                                                                $discountTotal3 += $discountsRow['discount'];
                                                            }
                                                        }
                                                    }

                                                    if ($row['vat_status'] == "inclusive") {
                                                        $vatFinalTotal += (($row['qty'] * $productRow['prod_sell_price']) - ($row['qty'] * $productRow['prod_sell_price']) / 1.16);
                                                        //  $Vat = ($row['qty'] * $row['prod_sell_price']) - ($row['qty'] * $row['prod_sell_price']) / 1.16;
                                                    } elseif ($row['vat_status'] == "free") {
                                                        $vatFreeTotal += ($row['qty'] * $productRow['prod_sell_price']);
                                                        // $vatFree = ($row['qty'] * $row['prod_sell_price']);
                                                    }
                                                ?>
                                                    <tr>
                                                        <td><?php echo $productRow['prod_name']; ?></td>
                                                        <td><?php
                                                            $branchId = $row['stock_branch_id'];
                                                            $damagesQuery = mysqli_query($con, "select * from stores_branch "
                                                                . " WHERE id='$branchId' ") or die("Error A " . mysqli_error($con));

                                                            $Rows = mysqli_fetch_array($damagesQuery);

                                                            echo "<span class='label label-success'>" . $Rows['branch_name'];
                                                            "</span>";
                                                            ?></td>
                                                        <td><?php echo $row['date_added']; ?></td>
                                                        <td><?php echo $row['name']; ?></td>

                                                        <td><?php echo $row['modepayment']; ?></td>

                                                        <td><?php echo $row['qty']; ?></td>
                                                        <td> <?php display_sales_variants($con, $product_id, $sales_id); ?> </td>
                                                        <td><?php
                                                            echo $productRow['prod_price'];
                                                            ?></td>
                                                        <td><?php echo '' . $productRow['prod_sell_price']; ?></td>

                                                        <td><?php
                                                            if ($productRow['vat_status'] == "inclusive") {

                                                                $Vat = ($row['qty'] * $productRow['prod_sell_price']) - ($row['qty'] * $productRow['prod_sell_price']) / 1.16;

                                                                //$vatFinalTotal += ($row['qty'] * $row['prod_sell_price']) - ($row['qty'] * $row['prod_sell_price']) / 1.16;;
                                                                echo number_format($Vat, 2);
                                                            } else {
                                                                echo "0.00";
                                                            }
                                                            ?></td>
                                                        <td><?php
                                                            if ($productRow['vat_status'] == "free") {

                                                                $vatFree = ($row['qty'] * $productRow['prod_sell_price']);

                                                                echo number_format($vatFree, 2);
                                                            } else {
                                                                echo "0.00";
                                                            }
                                                            ?></td>

                                                        <td><?php
                                                            $buy_pr = $productRow['prod_price'];

                                                            $buyTotal = $buy_pr * $row['qty'];
                                                            $profit = $totalSold - $buyTotal;
                                                            $totalProfit += $totalSold - $buyTotal;

                                                            if ($totalSold > $buyTotal) {
                                                                echo "<span class='label label-success'>" . ' ' . $profit . '  ' . "</span>";
                                                            } else {
                                                                echo "<span class='label label-danger'>" . '  ' . $profit . "</span>";
                                                            }
                                                            ?></td>
                                                        <td><?php echo $totalSold; ?></td>
                                                    </tr>
                                                <?php
                                                }

                                                $vat = $vatFinalTotal - ($vatFinalTotal / 1.16);
                                                ?>

                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>VAT @16 %.</td>
                                                    <td><?php echo 'K ' . number_format($vatFinalTotal, 2); ?></td>
                                                </tr>

                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>VAT Free.</td>
                                                    <td><?php echo 'K ' . number_format($vatFreeTotal, 2); ?></td>
                                                </tr>

                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>Discounts.</td>
                                                    <td><?php
                                                        echo $discountTotal3;
                                                        ?></td>
                                                </tr>

                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>Collections By Mode.</td>
                                                    <td><?php
                                                        while ($row1 = mysqli_fetch_array($queryPayments)) {
                                                            $name = $row1['modepayment'];
                                                            $total = $row1['total'];
                                                            echo "<span class='label label-success'>" . ' ' . $name . ' K  ' . number_format($total, 2) . '<br>' . "</span>";
                                                        }

                                                        while ($row1 = mysqli_fetch_array($querySplitPayments)) {
                                                            $name = $row1['modepayment'];
                                                            $total = $row1['total'];
                                                            echo "<span class='label label-success'>" . ' ' . $name . ' K  ' . number_format($total, 2) . '<br>' . "</span>";
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>


                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>Part Payments.</td>
                                                    <td>
                                                        <?php
                                                        while ($row1 = mysqli_fetch_array($partPaymentsByModes)) {
                                                            $name = $row1['name'];
                                                            $total = $row1['amount'];
                                                            echo "<span class='label label-success'>" . ' ' . $name . ' K  ' . number_format($total, 2) . '<br>' . "</span>";
                                                        }
                                                        ?>

                                                        <?php
                                                        $partpaymentsRow = mysqli_fetch_array($partPayments);
                                                        $partpaymentAmount = $partpaymentsRow['amount'];
                                                        echo 'K ' . number_format($partpaymentAmount, 2);
                                                        ?></td>
                                                </tr>

                                                <tr>
                                                    <td></td>
                                                    <td></td>

                                                    <td></td>
                                                    <td></td>
                                                    <td></td>

                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>Total Amount Tendered.</td>
                                                    <td><?php
                                                        $salesRow = mysqli_fetch_array($salesTotals);
                                                        $tenderedAmount = $salesRow['amount_due'] + $partpaymentAmount;
                                                        $amountLessDiscounts = $tenderedAmount - $discountTotal3;
                                                        echo 'K ' . $tenderedAmount; //number_format($amountLessDiscounts + $partpaymentAmount, 2);
                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>


                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>Total Profit.</td>
                                                    <td><?php
                                                        echo 'K ' . number_format($tenderedAmount - $buyTotal, 2);
                                                        ?></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div>
                            </div>
                    </section><!-- /.content -->


                </section><!-- /.content -->
            </div>
        </div>
        <?php include('../dist/includes/footer.php'); ?>
    </div>
    <script src="../plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="../plugins/select2/select2.full.min.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>






    <!-- Export to PDF - Datatable -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script> -->
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example2').DataTable({
                dom: 'Bfrtip',
                "ordering": false,
                "paging": false,
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
        });

        $(document).ready(function() {
            $('#example3').DataTable({
                dom: 'Bfrtip',
                "ordering": false,
                "paging": false,
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });
        });
    </script>



    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="../plugins/daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap datepicker -->
    <script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- bootstrap color picker -->
    <script src="../plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="../plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $(function() {
            $("#example1").DataTable();

            //Initialize Select2 Elements
            $(".select2").select2();
            $("#datemask").inputmask("dd/mm/yyyy", {
                "placeholder": "dd/mm/yyyy"
            });
            //Datemask2 mm/dd/yyyy
            $("#datemask2").inputmask("mm/dd/yyyy", {
                "placeholder": "mm/dd/yyyy"
            });
            //Money Euro
            $("[data-mask]").inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                format: 'MM/DD/YYYY h:mm A'
            });
            //Date range as a button
            $('#daterange-btn').daterangepicker({
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function(start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
            );

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();

            //Timepicker
            $(".timepicker").timepicker({
                showInputs: false
            });
        });
    </script>
</body>

</html>