

CREATE TABLE `advance_payments_tb` (
  `advace_id` int(12) NOT NULL AUTO_INCREMENT,
  `amount` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(12) NOT NULL,
  `prod_id` int(12) NOT NULL,
  `qty` int(12) NOT NULL,
  `branch_id` int(12) NOT NULL,
  `order_no` int(12) NOT NULL,
  `customer_name` text NOT NULL,
  `price` int(12) NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`advace_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO advance_payments_tb VALUES("1","10","2019-10-09 12:40:03","8","371","1","1","2747","USER","60","Sold");
INSERT INTO advance_payments_tb VALUES("2","10","2019-10-09 12:40:03","8","172","1","1","2747","USER","11","Sold");
INSERT INTO advance_payments_tb VALUES("3","60","2019-10-09 12:46:19","8","371","1","1","4679","Choolwe","60","Sold");



CREATE TABLE `branch` (
  `branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(50) NOT NULL,
  `branch_address` varchar(100) NOT NULL,
  `branch_contact` varchar(50) NOT NULL,
  `reciept_footer_text` text NOT NULL,
  `notification_count` int(12) NOT NULL,
  `skin` varchar(15) NOT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO branch VALUES("1","Healthy Leaving Pharmacy ","Lusaka, Zambia.","0977754735","Thank you for shopping with us. ","5","red");



CREATE TABLE `cashout_limits_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `cashoutlimit` text NOT NULL,
  `status` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO cashout_limits_tb VALUES("1","20000","Not Active","2019-10-05 13:03:19");



CREATE TABLE `category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(30) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

INSERT INTO category VALUES("63","BEEF");
INSERT INTO category VALUES("65","CHICKENS");
INSERT INTO category VALUES("66","FISH");
INSERT INTO category VALUES("67","GOAT");
INSERT INTO category VALUES("68","GROCERY");
INSERT INTO category VALUES("69","PORK");
INSERT INTO category VALUES("70","SAUSAGES");
INSERT INTO category VALUES("71","SHEEP");



CREATE TABLE `customer` (
  `cust_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_first` varchar(50) NOT NULL,
  `cust_last` varchar(30) NOT NULL,
  `cust_address` varchar(100) NOT NULL,
  `cust_contact` varchar(30) NOT NULL,
  `balance` decimal(10,2) NOT NULL,
  `cust_pic` varchar(300) NOT NULL,
  `bday` date NOT NULL,
  `nickname` varchar(30) NOT NULL,
  `house_status` varchar(30) NOT NULL,
  `years` varchar(20) NOT NULL,
  `rent` varchar(10) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `emp_no` varchar(30) NOT NULL,
  `emp_address` varchar(100) NOT NULL,
  `emp_year` varchar(10) NOT NULL,
  `occupation` varchar(30) NOT NULL,
  `salary` varchar(30) NOT NULL,
  `spouse` varchar(30) NOT NULL,
  `spouse_no` varchar(30) NOT NULL,
  `spouse_emp` varchar(50) NOT NULL,
  `spouse_details` varchar(100) NOT NULL,
  `spouse_income` decimal(10,2) NOT NULL,
  `comaker` varchar(30) NOT NULL,
  `comaker_details` varchar(100) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `credit_status` varchar(10) NOT NULL,
  `ci_remarks` varchar(1000) NOT NULL,
  `ci_name` varchar(50) NOT NULL,
  `ci_date` date NOT NULL,
  `payslip` int(11) NOT NULL,
  `valid_id` int(11) NOT NULL,
  `cert` int(11) NOT NULL,
  `cedula` int(11) NOT NULL,
  `income` int(11) NOT NULL,
  `email` text NOT NULL,
  PRIMARY KEY (`cust_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `damages_log_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(12) NOT NULL,
  `qty_damage` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `demo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(100) NOT NULL,
  `state` varchar(40) NOT NULL,
  `city` varchar(40) NOT NULL,
  `pin` varchar(40) NOT NULL,
  `submission_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO demo VALUES("1","Zambia","21111","Chilenje","212121","2019-10-02");
INSERT INTO demo VALUES("2","Congo","21111","Congo","212121","2019-10-02");
INSERT INTO demo VALUES("3","South Africa","21111","Congo","212121","2019-10-02");



CREATE TABLE `discount_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `discount_price` int(12) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `discount_from` text NOT NULL,
  `discount_to` text NOT NULL,
  `status` text NOT NULL,
  `price_before_disc` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO discount_tb VALUES("1","898","50","2019-11-29 09:57:48","2019-11-28 00:00:00","2019-11-30 00:00:00","notactive","100");



CREATE TABLE `draft_sales_tb` (
  `sales_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `order_no` int(12) NOT NULL,
  `client_name` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sales_details_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `draft_temp_trans` (
  `temp_trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `order_no` int(12) NOT NULL,
  `customer_name` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(12) NOT NULL,
  `is_printed` int(11) NOT NULL,
  `special_remarks` text NOT NULL,
  PRIMARY KEY (`temp_trans_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

INSERT INTO draft_temp_trans VALUES("13","900","77.00","1","1","4160","choolwe","2019-12-10 09:27:32","8","0","");
INSERT INTO draft_temp_trans VALUES("14","899","31.00","1","1","4160","choolwe","2019-12-10 09:27:32","8","0","");
INSERT INTO draft_temp_trans VALUES("15","900","77.00","1","1","3482","TEST","2019-12-11 15:43:58","8","0","");
INSERT INTO draft_temp_trans VALUES("16","900","77.00","1","1","3482","","2019-12-11 15:57:21","8","0","");
INSERT INTO draft_temp_trans VALUES("17","900","77.00","1","1","3482","","2019-12-11 15:57:34","8","0","");
INSERT INTO draft_temp_trans VALUES("18","899","31.00","1","1","3482","","2019-12-11 15:57:51","8","1","");
INSERT INTO draft_temp_trans VALUES("19","682","4.00","2","1","2882","Table 1","2019-12-16 16:54:49","8","1","Mild with Chilli");
INSERT INTO draft_temp_trans VALUES("20","682","2.00","1","1","137","Table 3","2019-12-16 16:56:50","8","1","");
INSERT INTO draft_temp_trans VALUES("21","230","5.00","1","1","3933","TEST","2019-12-19 16:04:02","8","1","");
INSERT INTO draft_temp_trans VALUES("22","682","2.00","1","1","1137","Choolwe","2019-12-19 16:16:43","8","1","Alo");



CREATE TABLE `expenses_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `amount` int(12) NOT NULL,
  `date` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO expenses_tb VALUES("1"," August Container Delivery","64761","2019-08-14 10:00:07");



CREATE TABLE `history_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=318 DEFAULT CHARSET=latin1;

INSERT INTO history_log VALUES("1","4","has logged out the system at ","2019-10-05 19:25:34");
INSERT INTO history_log VALUES("2","8","has logged in the system at ","2019-10-05 19:25:47");
INSERT INTO history_log VALUES("3","8","has logged out the system at ","2019-10-05 19:25:57");
INSERT INTO history_log VALUES("4","8","has logged in the system at ","2019-10-05 19:54:42");
INSERT INTO history_log VALUES("5","8","has logged out the system at ","2019-10-05 23:37:02");
INSERT INTO history_log VALUES("6","8","has logged in the system at ","2019-10-05 23:37:07");
INSERT INTO history_log VALUES("7","8","has logged out the system at ","2019-10-05 23:44:36");
INSERT INTO history_log VALUES("8","8","has logged in the system at ","2019-10-05 23:44:42");
INSERT INTO history_log VALUES("9","8","has logged out the system at ","2019-10-05 23:46:28");
INSERT INTO history_log VALUES("10","4","has logged in the system at ","2019-10-05 23:46:33");
INSERT INTO history_log VALUES("11","4","has logged out the system at ","2019-10-05 23:48:41");
INSERT INTO history_log VALUES("12","8","has logged in the system at ","2019-10-05 23:48:46");
INSERT INTO history_log VALUES("13","8","has logged out the system at ","2019-10-05 23:49:37");
INSERT INTO history_log VALUES("14","8","has logged in the system at ","2019-10-05 23:49:46");
INSERT INTO history_log VALUES("15","8","has logged out the system at ","2019-10-05 23:51:12");
INSERT INTO history_log VALUES("16","4","has logged in the system at ","2019-10-05 23:51:23");
INSERT INTO history_log VALUES("17","4","has logged out the system at ","2019-10-05 23:51:32");
INSERT INTO history_log VALUES("18","8","has logged in the system at ","2019-10-05 23:51:36");
INSERT INTO history_log VALUES("19","8","has logged in the system at ","2019-10-06 13:04:47");
INSERT INTO history_log VALUES("20","8","has logged in the system at ","2019-10-06 14:47:17");
INSERT INTO history_log VALUES("21","8","has logged in the system at ","2019-10-07 10:46:48");
INSERT INTO history_log VALUES("22","8","has logged out the system at ","2019-10-07 10:48:26");
INSERT INTO history_log VALUES("23","8","has logged in the system at ","2019-10-08 08:12:56");
INSERT INTO history_log VALUES("24","8","has logged out the system at ","2019-10-08 08:44:17");
INSERT INTO history_log VALUES("25","8","has logged in the system at ","2019-10-08 23:45:50");
INSERT INTO history_log VALUES("26","8","has logged in the system at ","2019-10-08 23:47:16");
INSERT INTO history_log VALUES("27","8","has logged in the system at ","2019-10-09 00:12:34");
INSERT INTO history_log VALUES("28","4","has logged in the system at ","2019-10-09 10:07:12");
INSERT INTO history_log VALUES("29","8","has logged in the system at ","2019-10-09 12:06:05");
INSERT INTO history_log VALUES("30","4","has logged in the system at ","2019-10-10 11:11:38");
INSERT INTO history_log VALUES("31","4","has logged in the system at ","2019-10-10 13:25:39");
INSERT INTO history_log VALUES("32","8","has logged in the system at ","2019-10-10 14:19:27");
INSERT INTO history_log VALUES("33","8","has logged out the system at ","2019-10-10 14:21:42");
INSERT INTO history_log VALUES("34","4","has logged in the system at ","2019-10-10 14:21:48");
INSERT INTO history_log VALUES("35","4","has logged out the system at ","2019-10-10 14:21:58");
INSERT INTO history_log VALUES("36","4","has logged in the system at ","2019-10-11 18:39:06");
INSERT INTO history_log VALUES("37","4","has logged in the system at ","2019-10-12 08:33:07");
INSERT INTO history_log VALUES("38","4","has logged in the system at ","2019-10-12 19:51:12");
INSERT INTO history_log VALUES("39","4","has logged out the system at ","2019-10-12 20:53:22");
INSERT INTO history_log VALUES("40","4","has logged in the system at ","2019-10-12 20:58:50");
INSERT INTO history_log VALUES("41","8","has logged in the system at ","2019-10-13 16:03:33");
INSERT INTO history_log VALUES("42","8","has logged out the system at ","2019-10-13 18:29:16");
INSERT INTO history_log VALUES("43","4","has logged in the system at ","2019-10-14 17:49:42");
INSERT INTO history_log VALUES("44","4","has logged in the system at ","2019-10-17 08:27:17");
INSERT INTO history_log VALUES("45","4","has logged in the system at ","2019-10-17 10:53:49");
INSERT INTO history_log VALUES("46","8","has logged in the system at ","2019-10-18 08:10:01");
INSERT INTO history_log VALUES("47","8","has logged out the system at ","2019-10-18 08:10:18");
INSERT INTO history_log VALUES("48","4","has logged in the system at ","2019-10-18 08:10:23");
INSERT INTO history_log VALUES("49","4","has logged out the system at ","2019-10-18 08:12:30");
INSERT INTO history_log VALUES("50","8","has logged in the system at ","2019-10-18 08:12:36");
INSERT INTO history_log VALUES("51","8","has logged out the system at ","2019-10-18 08:15:10");
INSERT INTO history_log VALUES("52","4","has logged in the system at ","2019-10-18 18:49:01");
INSERT INTO history_log VALUES("53","4","has logged in the system at ","2019-10-24 09:23:36");
INSERT INTO history_log VALUES("54","4","has logged in the system at ","2019-10-24 09:29:25");
INSERT INTO history_log VALUES("55","4","has logged in the system at ","2019-10-25 11:02:02");
INSERT INTO history_log VALUES("56","8","has logged in the system at ","2019-10-25 19:20:03");
INSERT INTO history_log VALUES("57","8","has logged out the system at ","2019-10-25 20:48:14");
INSERT INTO history_log VALUES("58","4","has logged in the system at ","2019-10-25 20:48:21");
INSERT INTO history_log VALUES("59","4","has logged in the system at ","2019-10-27 08:13:32");
INSERT INTO history_log VALUES("60","8","has logged in the system at ","2019-10-27 20:02:02");
INSERT INTO history_log VALUES("61","8","has logged in the system at ","2019-10-28 08:05:02");
INSERT INTO history_log VALUES("62","8","has logged out the system at ","2019-10-28 08:09:22");
INSERT INTO history_log VALUES("63","4","has logged in the system at ","2019-10-28 11:12:17");
INSERT INTO history_log VALUES("64","8","has logged in the system at ","2019-10-28 13:54:12");
INSERT INTO history_log VALUES("65","8","has logged out the system at ","2019-10-28 13:54:28");
INSERT INTO history_log VALUES("66","4","has logged in the system at ","2019-10-28 13:54:33");
INSERT INTO history_log VALUES("67","4","has logged out the system at ","2019-10-28 13:55:09");
INSERT INTO history_log VALUES("68","8","has logged in the system at ","2019-10-28 13:55:13");
INSERT INTO history_log VALUES("69","8","has logged out the system at ","2019-10-28 13:57:27");
INSERT INTO history_log VALUES("70","4","has logged in the system at ","2019-10-28 13:57:46");
INSERT INTO history_log VALUES("71","4","has logged out the system at ","2019-10-28 14:00:09");
INSERT INTO history_log VALUES("72","8","has logged in the system at ","2019-10-28 14:00:19");
INSERT INTO history_log VALUES("73","8","has logged out the system at ","2019-10-28 14:15:38");
INSERT INTO history_log VALUES("74","4","has logged in the system at ","2019-10-28 14:15:43");
INSERT INTO history_log VALUES("75","4","has logged out the system at ","2019-10-28 14:33:12");
INSERT INTO history_log VALUES("76","4","has logged in the system at ","2019-10-30 11:05:47");
INSERT INTO history_log VALUES("77","4","has logged in the system at ","2019-10-31 10:36:39");
INSERT INTO history_log VALUES("78","4","has logged out the system at ","2019-10-31 10:39:05");
INSERT INTO history_log VALUES("79","4","has logged in the system at ","2019-10-31 10:39:30");
INSERT INTO history_log VALUES("80","8","has logged in the system at ","2019-11-03 16:32:48");
INSERT INTO history_log VALUES("81","4","has logged in the system at ","2019-11-03 18:00:38");
INSERT INTO history_log VALUES("82","4","has logged out the system at ","2019-11-03 18:41:07");
INSERT INTO history_log VALUES("83","8","has logged in the system at ","2019-11-03 18:41:33");
INSERT INTO history_log VALUES("84","8","has logged out the system at ","2019-11-03 19:00:25");
INSERT INTO history_log VALUES("85","4","has logged in the system at ","2019-11-03 19:00:55");
INSERT INTO history_log VALUES("86","4","has logged in the system at ","2019-11-04 11:45:28");
INSERT INTO history_log VALUES("87","4","has logged out the system at ","2019-11-04 11:53:12");
INSERT INTO history_log VALUES("88","4","has logged in the system at ","2019-11-04 11:53:43");
INSERT INTO history_log VALUES("89","4","has logged out the system at ","2019-11-04 11:56:47");
INSERT INTO history_log VALUES("90","4","has logged in the system at ","2019-11-04 20:31:47");
INSERT INTO history_log VALUES("91","8","has logged in the system at ","2019-11-07 19:04:05");
INSERT INTO history_log VALUES("92","8","has logged out the system at ","2019-11-07 19:07:17");
INSERT INTO history_log VALUES("93","4","has logged in the system at ","2019-11-07 19:07:23");
INSERT INTO history_log VALUES("94","4","has logged out the system at ","2019-11-07 19:14:24");
INSERT INTO history_log VALUES("95","8","has logged in the system at ","2019-11-07 19:19:12");
INSERT INTO history_log VALUES("96","8","has logged in the system at ","2019-11-08 17:07:06");
INSERT INTO history_log VALUES("97","4","has logged in the system at ","2019-11-09 09:30:43");
INSERT INTO history_log VALUES("98","8","has logged in the system at ","2019-11-10 17:46:26");
INSERT INTO history_log VALUES("99","8","has logged out the system at ","2019-11-10 17:48:45");
INSERT INTO history_log VALUES("100","4","has logged in the system at ","2019-11-11 08:33:20");
INSERT INTO history_log VALUES("101","4","has logged out the system at ","2019-11-11 09:19:53");
INSERT INTO history_log VALUES("102","8","has logged in the system at ","2019-11-11 09:19:58");
INSERT INTO history_log VALUES("103","8","has logged out the system at ","2019-11-11 09:30:25");
INSERT INTO history_log VALUES("104","4","has logged in the system at ","2019-11-11 09:30:29");
INSERT INTO history_log VALUES("105","4","has logged out the system at ","2019-11-11 09:30:42");
INSERT INTO history_log VALUES("106","10","has logged in the system at ","2019-11-11 09:30:47");
INSERT INTO history_log VALUES("107","10","has logged out the system at ","2019-11-11 09:31:04");
INSERT INTO history_log VALUES("108","8","has logged in the system at ","2019-11-11 09:31:09");
INSERT INTO history_log VALUES("109","8","has logged out the system at ","2019-11-11 10:19:25");
INSERT INTO history_log VALUES("110","4","has logged in the system at ","2019-11-11 15:49:44");
INSERT INTO history_log VALUES("111","4","has logged out the system at ","2019-11-11 16:11:22");
INSERT INTO history_log VALUES("112","8","has logged in the system at ","2019-11-11 16:11:27");
INSERT INTO history_log VALUES("113","8","has logged out the system at ","2019-11-11 16:25:21");
INSERT INTO history_log VALUES("114","4","has logged in the system at ","2019-11-11 16:25:25");
INSERT INTO history_log VALUES("115","4","has logged out the system at ","2019-11-11 16:32:35");
INSERT INTO history_log VALUES("116","8","has logged in the system at ","2019-11-11 16:32:44");
INSERT INTO history_log VALUES("117","8","has logged out the system at ","2019-11-11 16:45:30");
INSERT INTO history_log VALUES("118","4","has logged in the system at ","2019-11-11 16:45:44");
INSERT INTO history_log VALUES("119","4","has logged out the system at ","2019-11-11 16:46:49");
INSERT INTO history_log VALUES("120","8","has logged in the system at ","2019-11-11 16:46:56");
INSERT INTO history_log VALUES("121","4","has logged in the system at ","2019-11-12 15:02:39");
INSERT INTO history_log VALUES("122","8","has logged in the system at ","2019-11-12 15:54:40");
INSERT INTO history_log VALUES("123","8","has logged out the system at ","2019-11-12 15:55:24");
INSERT INTO history_log VALUES("124","4","has logged in the system at ","2019-11-12 15:55:29");
INSERT INTO history_log VALUES("125","4","has logged in the system at ","2019-11-13 15:13:35");
INSERT INTO history_log VALUES("126","4","has logged out the system at ","2019-11-13 15:24:18");
INSERT INTO history_log VALUES("127","4","has logged in the system at ","2019-11-15 10:38:02");
INSERT INTO history_log VALUES("128","4","has logged out the system at ","2019-11-15 10:38:19");
INSERT INTO history_log VALUES("129","4","has logged in the system at ","2019-11-16 15:06:02");
INSERT INTO history_log VALUES("130","4","has logged out the system at ","2019-11-16 15:06:41");
INSERT INTO history_log VALUES("131","8","has logged in the system at ","2019-11-16 15:06:46");
INSERT INTO history_log VALUES("132","8","has logged in the system at ","2019-11-16 15:10:32");
INSERT INTO history_log VALUES("133","8","has logged in the system at ","2019-11-16 15:15:16");
INSERT INTO history_log VALUES("134","4","has logged in the system at ","2019-11-16 15:36:46");
INSERT INTO history_log VALUES("135","4","has logged in the system at ","2019-11-18 08:51:04");
INSERT INTO history_log VALUES("136","8","has logged in the system at ","2019-11-19 12:07:12");
INSERT INTO history_log VALUES("137","8","has logged out the system at ","2019-11-19 12:07:17");
INSERT INTO history_log VALUES("138","4","has logged in the system at ","2019-11-19 12:07:24");
INSERT INTO history_log VALUES("139","4","has logged out the system at ","2019-11-19 12:26:35");
INSERT INTO history_log VALUES("140","8","has logged in the system at ","2019-11-19 12:26:49");
INSERT INTO history_log VALUES("141","8","has logged out the system at ","2019-11-19 12:32:14");
INSERT INTO history_log VALUES("142","4","has logged in the system at ","2019-11-19 12:32:19");
INSERT INTO history_log VALUES("143","4","has logged out the system at ","2019-11-19 12:33:04");
INSERT INTO history_log VALUES("144","8","has logged in the system at ","2019-11-19 18:02:31");
INSERT INTO history_log VALUES("145","4","has logged in the system at ","2019-11-21 13:48:19");
INSERT INTO history_log VALUES("146","4","has logged in the system at ","2019-11-21 19:10:28");
INSERT INTO history_log VALUES("147","4","has logged out the system at ","2019-11-21 19:10:59");
INSERT INTO history_log VALUES("148","8","has logged in the system at ","2019-11-21 19:11:05");
INSERT INTO history_log VALUES("149","8","has logged out the system at ","2019-11-21 19:11:22");
INSERT INTO history_log VALUES("150","4","has logged in the system at ","2019-11-21 19:11:28");
INSERT INTO history_log VALUES("151","4","has logged out the system at ","2019-11-21 19:13:40");
INSERT INTO history_log VALUES("152","8","has logged in the system at ","2019-11-21 19:13:47");
INSERT INTO history_log VALUES("153","4","has logged in the system at ","2019-11-24 21:50:51");
INSERT INTO history_log VALUES("154","4","has logged in the system at ","2019-11-25 13:35:14");
INSERT INTO history_log VALUES("155","4","has logged in the system at ","2019-11-25 20:17:26");
INSERT INTO history_log VALUES("156","4","has logged in the system at ","2019-11-26 15:50:57");
INSERT INTO history_log VALUES("157","4","has logged out the system at ","2019-11-26 16:06:04");
INSERT INTO history_log VALUES("158","8","has logged in the system at ","2019-11-26 16:06:11");
INSERT INTO history_log VALUES("159","8","has logged out the system at ","2019-11-26 16:09:13");
INSERT INTO history_log VALUES("160","4","has logged in the system at ","2019-11-26 16:09:29");
INSERT INTO history_log VALUES("161","4","has logged in the system at ","2019-11-27 12:46:50");
INSERT INTO history_log VALUES("162","8","has logged in the system at ","2019-11-27 16:54:49");
INSERT INTO history_log VALUES("163","4","has logged in the system at ","2019-11-28 14:59:01");
INSERT INTO history_log VALUES("164","4","has logged in the system at ","2019-11-28 21:31:52");
INSERT INTO history_log VALUES("165","4","has logged out the system at ","2019-11-28 21:34:38");
INSERT INTO history_log VALUES("166","4","has logged in the system at ","2019-11-29 09:57:48");
INSERT INTO history_log VALUES("167","4","has logged in the system at ","2019-11-29 19:01:36");
INSERT INTO history_log VALUES("168","4","has logged out the system at ","2019-11-29 19:14:49");
INSERT INTO history_log VALUES("169","8","has logged in the system at ","2019-11-29 19:15:00");
INSERT INTO history_log VALUES("170","4","has logged in the system at ","2019-11-29 19:16:02");
INSERT INTO history_log VALUES("171","4","has logged out the system at ","2019-11-29 22:31:10");
INSERT INTO history_log VALUES("172","8","has logged in the system at ","2019-11-29 22:31:15");
INSERT INTO history_log VALUES("173","8","has logged out the system at ","2019-11-29 22:31:17");
INSERT INTO history_log VALUES("174","4","has logged in the system at ","2019-11-29 22:31:22");
INSERT INTO history_log VALUES("175","4","has logged out the system at ","2019-11-29 22:32:03");
INSERT INTO history_log VALUES("176","11","has logged in the system at ","2019-11-29 22:32:16");
INSERT INTO history_log VALUES("177","4","has logged in the system at ","2019-11-29 22:32:37");
INSERT INTO history_log VALUES("178","8","has logged in the system at ","2019-11-29 23:17:55");
INSERT INTO history_log VALUES("179","4","has logged in the system at ","2019-11-29 23:18:50");
INSERT INTO history_log VALUES("180","4","has logged in the system at ","2019-11-30 09:44:57");
INSERT INTO history_log VALUES("181","4","has logged out the system at ","2019-11-30 09:47:18");
INSERT INTO history_log VALUES("182","4","has logged in the system at ","2019-11-30 09:47:29");
INSERT INTO history_log VALUES("183","4","has logged out the system at ","2019-11-30 10:41:01");
INSERT INTO history_log VALUES("184","4","has logged in the system at ","2019-11-30 12:19:17");
INSERT INTO history_log VALUES("185","4","has logged in the system at ","2019-11-30 13:03:11");
INSERT INTO history_log VALUES("186","4","has logged in the system at ","2019-11-30 16:01:24");
INSERT INTO history_log VALUES("187","4","has logged in the system at ","2019-11-30 17:04:26");
INSERT INTO history_log VALUES("188","4","has logged out the system at ","2019-11-30 19:31:42");
INSERT INTO history_log VALUES("189","4","has logged in the system at ","2019-12-01 13:42:08");
INSERT INTO history_log VALUES("190","4","has logged in the system at ","2019-12-04 11:07:55");
INSERT INTO history_log VALUES("191","4","has logged in the system at ","2019-12-06 16:08:43");
INSERT INTO history_log VALUES("192","4","has logged in the system at ","2019-12-08 10:57:25");
INSERT INTO history_log VALUES("193","4","has logged in the system at ","2019-12-09 13:23:22");
INSERT INTO history_log VALUES("194","8","has logged in the system at ","2019-12-09 17:10:26");
INSERT INTO history_log VALUES("195","4","has logged in the system at ","2019-12-10 09:19:08");
INSERT INTO history_log VALUES("196","4","has logged out the system at ","2019-12-10 09:26:57");
INSERT INTO history_log VALUES("197","8","has logged in the system at ","2019-12-10 09:27:02");
INSERT INTO history_log VALUES("198","4","has logged in the system at ","2019-12-10 10:55:21");
INSERT INTO history_log VALUES("199","4","has logged out the system at ","2019-12-10 10:55:37");
INSERT INTO history_log VALUES("200","8","has logged in the system at ","2019-12-10 10:55:44");
INSERT INTO history_log VALUES("201","4","has logged in the system at ","2019-12-10 17:50:38");
INSERT INTO history_log VALUES("202","8","has logged in the system at ","2019-12-11 14:31:31");
INSERT INTO history_log VALUES("203","8","has logged in the system at ","2019-12-12 11:05:30");
INSERT INTO history_log VALUES("204","8","has logged in the system at ","2019-12-12 11:22:30");
INSERT INTO history_log VALUES("205","8","has logged out the system at ","2019-12-12 11:30:23");
INSERT INTO history_log VALUES("206","4","has logged in the system at ","2019-12-12 11:30:28");
INSERT INTO history_log VALUES("207","4","has logged in the system at ","2019-12-12 17:38:25");
INSERT INTO history_log VALUES("208","4","has logged out the system at ","2019-12-12 17:38:29");
INSERT INTO history_log VALUES("209","8","has logged in the system at ","2019-12-12 17:38:34");
INSERT INTO history_log VALUES("210","4","has logged in the system at ","2019-12-14 17:13:03");
INSERT INTO history_log VALUES("211","4","has logged out the system at ","2019-12-14 17:13:08");
INSERT INTO history_log VALUES("212","8","has logged in the system at ","2019-12-14 17:13:13");
INSERT INTO history_log VALUES("213","8","has logged out the system at ","2019-12-14 17:13:24");
INSERT INTO history_log VALUES("214","4","has logged in the system at ","2019-12-14 17:13:29");
INSERT INTO history_log VALUES("215","4","has logged out the system at ","2019-12-14 17:13:52");
INSERT INTO history_log VALUES("216","8","has logged in the system at ","2019-12-14 17:13:57");
INSERT INTO history_log VALUES("217","8","has logged out the system at ","2019-12-14 17:25:56");
INSERT INTO history_log VALUES("218","8","has logged in the system at ","2019-12-14 17:26:01");
INSERT INTO history_log VALUES("219","8","has logged out the system at ","2019-12-14 17:26:33");
INSERT INTO history_log VALUES("220","4","has logged in the system at ","2019-12-14 17:26:38");
INSERT INTO history_log VALUES("221","8","has logged in the system at ","2019-12-15 16:11:25");
INSERT INTO history_log VALUES("222","8","has logged out the system at ","2019-12-15 16:11:35");
INSERT INTO history_log VALUES("223","4","has logged in the system at ","2019-12-15 16:11:46");
INSERT INTO history_log VALUES("224","4","has logged out the system at ","2019-12-15 16:12:16");
INSERT INTO history_log VALUES("225","8","has logged in the system at ","2019-12-15 16:14:24");
INSERT INTO history_log VALUES("226","8","has logged out the system at ","2019-12-15 21:18:23");
INSERT INTO history_log VALUES("227","4","has logged in the system at ","2019-12-15 21:18:28");
INSERT INTO history_log VALUES("228","4","has logged out the system at ","2019-12-15 21:20:48");
INSERT INTO history_log VALUES("229","8","has logged in the system at ","2019-12-15 21:20:53");
INSERT INTO history_log VALUES("230","8","has logged out the system at ","2019-12-15 21:24:58");
INSERT INTO history_log VALUES("231","4","has logged in the system at ","2019-12-15 21:25:02");
INSERT INTO history_log VALUES("232","8","has logged in the system at ","2019-12-16 15:52:22");
INSERT INTO history_log VALUES("233","4","has logged in the system at ","2019-12-16 19:51:29");
INSERT INTO history_log VALUES("234","4","has logged in the system at ","2019-12-17 10:00:03");
INSERT INTO history_log VALUES("235","8","has logged in the system at ","2019-12-17 14:36:01");
INSERT INTO history_log VALUES("236","8","has logged out the system at ","2019-12-17 14:36:09");
INSERT INTO history_log VALUES("237","8","has logged in the system at ","2019-12-17 14:36:32");
INSERT INTO history_log VALUES("238","8","has logged out the system at ","2019-12-17 14:36:54");
INSERT INTO history_log VALUES("239","4","has logged in the system at ","2019-12-17 14:37:04");
INSERT INTO history_log VALUES("240","4","has logged out the system at ","2019-12-17 15:31:27");
INSERT INTO history_log VALUES("241","4","has logged in the system at ","2019-12-17 15:32:01");
INSERT INTO history_log VALUES("242","4","has logged out the system at ","2019-12-17 15:45:52");
INSERT INTO history_log VALUES("243","8","has logged in the system at ","2019-12-17 15:45:56");
INSERT INTO history_log VALUES("244","8","has logged out the system at ","2019-12-17 15:46:23");
INSERT INTO history_log VALUES("245","4","has logged in the system at ","2019-12-17 15:46:39");
INSERT INTO history_log VALUES("246","4","has logged out the system at ","2019-12-17 15:47:08");
INSERT INTO history_log VALUES("247","8","has logged in the system at ","2019-12-17 15:47:12");
INSERT INTO history_log VALUES("248","8","has logged out the system at ","2019-12-17 15:53:13");
INSERT INTO history_log VALUES("249","4","has logged in the system at ","2019-12-17 15:55:35");
INSERT INTO history_log VALUES("250","4","has logged out the system at ","2019-12-17 16:42:20");
INSERT INTO history_log VALUES("251","8","has logged in the system at ","2019-12-17 16:42:26");
INSERT INTO history_log VALUES("252","8","has logged in the system at ","2019-12-17 16:47:40");
INSERT INTO history_log VALUES("253","4","has logged in the system at ","2019-12-19 08:33:20");
INSERT INTO history_log VALUES("254","4","has logged out the system at ","2019-12-19 08:47:34");
INSERT INTO history_log VALUES("255","8","has logged in the system at ","2019-12-19 08:47:47");
INSERT INTO history_log VALUES("256","8","has logged out the system at ","2019-12-19 09:45:02");
INSERT INTO history_log VALUES("257","8","has logged in the system at ","2019-12-19 09:45:14");
INSERT INTO history_log VALUES("258","8","has logged out the system at ","2019-12-19 09:49:05");
INSERT INTO history_log VALUES("259","4","has logged in the system at ","2019-12-19 09:49:10");
INSERT INTO history_log VALUES("260","4","has logged out the system at ","2019-12-19 10:22:00");
INSERT INTO history_log VALUES("261","8","has logged in the system at ","2019-12-19 10:22:05");
INSERT INTO history_log VALUES("262","8","has logged out the system at ","2019-12-19 10:22:18");
INSERT INTO history_log VALUES("263","4","has logged in the system at ","2019-12-19 10:22:23");
INSERT INTO history_log VALUES("264","4","has logged out the system at ","2019-12-19 10:30:09");
INSERT INTO history_log VALUES("265","8","has logged in the system at ","2019-12-19 10:30:14");
INSERT INTO history_log VALUES("266","8","has logged out the system at ","2019-12-19 10:42:08");
INSERT INTO history_log VALUES("267","4","has logged in the system at ","2019-12-19 10:42:13");
INSERT INTO history_log VALUES("268","4","has logged out the system at ","2019-12-19 10:48:43");
INSERT INTO history_log VALUES("269","8","has logged in the system at ","2019-12-19 10:50:07");
INSERT INTO history_log VALUES("270","8","has logged out the system at ","2019-12-19 10:50:38");
INSERT INTO history_log VALUES("271","4","has logged in the system at ","2019-12-19 10:50:42");
INSERT INTO history_log VALUES("272","8","has logged in the system at ","2019-12-19 14:56:32");
INSERT INTO history_log VALUES("273","8","has logged in the system at ","2019-12-20 11:35:47");
INSERT INTO history_log VALUES("274","8","has logged out the system at ","2019-12-20 11:53:28");
INSERT INTO history_log VALUES("275","4","has logged in the system at ","2019-12-20 11:53:33");
INSERT INTO history_log VALUES("276","4","has logged out the system at ","2019-12-20 11:53:47");
INSERT INTO history_log VALUES("277","11","has logged in the system at ","2019-12-20 11:53:51");
INSERT INTO history_log VALUES("278","4","has logged in the system at ","2019-12-21 17:39:37");
INSERT INTO history_log VALUES("279","8","has logged in the system at ","2019-12-21 18:05:30");
INSERT INTO history_log VALUES("280","0","has logged out the system at ","2019-12-21 20:57:00");
INSERT INTO history_log VALUES("281","4","has logged in the system at ","2019-12-22 16:29:11");
INSERT INTO history_log VALUES("282","4","has logged out the system at ","2019-12-22 16:45:22");
INSERT INTO history_log VALUES("283","8","has logged in the system at ","2019-12-22 16:46:11");
INSERT INTO history_log VALUES("284","8","has logged out the system at ","2019-12-22 16:46:39");
INSERT INTO history_log VALUES("285","4","has logged in the system at ","2019-12-22 16:46:44");
INSERT INTO history_log VALUES("286","4","has logged out the system at ","2019-12-22 16:47:17");
INSERT INTO history_log VALUES("287","8","has logged in the system at ","2019-12-22 16:47:30");
INSERT INTO history_log VALUES("288","8","has logged out the system at ","2019-12-22 16:48:06");
INSERT INTO history_log VALUES("289","4","has logged in the system at ","2019-12-22 16:48:13");
INSERT INTO history_log VALUES("290","8","has logged in the system at ","2019-12-23 10:44:19");
INSERT INTO history_log VALUES("291","4","has logged in the system at ","2019-12-24 09:28:37");
INSERT INTO history_log VALUES("292","1","has logged out the system at ","2019-12-24 09:47:03");
INSERT INTO history_log VALUES("293","4","has logged in the system at ","2019-12-24 09:47:08");
INSERT INTO history_log VALUES("294","8","has logged in the system at ","2019-12-25 12:04:34");
INSERT INTO history_log VALUES("295","8","has logged out the system at ","2019-12-25 12:06:19");
INSERT INTO history_log VALUES("296","4","has logged in the system at ","2019-12-25 12:06:24");
INSERT INTO history_log VALUES("297","4","has logged out the system at ","2019-12-25 12:07:01");
INSERT INTO history_log VALUES("298","4","has logged in the system at ","2019-12-27 15:35:50");
INSERT INTO history_log VALUES("299","4","has logged in the system at ","2019-12-27 19:43:09");
INSERT INTO history_log VALUES("300","8","has logged in the system at ","2019-12-28 18:26:00");
INSERT INTO history_log VALUES("301","8","has logged in the system at ","2019-12-28 18:28:52");
INSERT INTO history_log VALUES("302","4","has logged in the system at ","2019-12-30 17:59:47");
INSERT INTO history_log VALUES("303","4","has logged out the system at ","2019-12-30 18:00:06");
INSERT INTO history_log VALUES("304","8","has logged in the system at ","2019-12-30 18:00:11");
INSERT INTO history_log VALUES("305","8","has logged out the system at ","2019-12-30 18:05:58");
INSERT INTO history_log VALUES("306","4","has logged in the system at ","2019-12-30 18:06:03");
INSERT INTO history_log VALUES("307","8","has logged in the system at ","2020-01-01 13:04:21");
INSERT INTO history_log VALUES("308","8","has logged out the system at ","2020-01-01 13:04:35");
INSERT INTO history_log VALUES("309","4","has logged in the system at ","2020-01-01 13:04:59");
INSERT INTO history_log VALUES("310","8","has logged in the system at ","2020-01-01 13:05:38");
INSERT INTO history_log VALUES("311","4","has logged in the system at ","2020-01-01 13:15:02");
INSERT INTO history_log VALUES("312","4","has logged in the system at ","2020-01-01 13:15:37");
INSERT INTO history_log VALUES("313","4","has logged in the system at ","2020-01-03 10:07:21");
INSERT INTO history_log VALUES("314","4","has logged out the system at ","2020-01-03 10:16:18");
INSERT INTO history_log VALUES("315","8","has logged in the system at ","2020-01-03 10:16:24");
INSERT INTO history_log VALUES("316","8","has logged out the system at ","2020-01-03 10:20:22");
INSERT INTO history_log VALUES("317","4","has logged in the system at ","2020-01-03 10:20:27");



CREATE TABLE `inv_damages_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `inv_id` int(12) NOT NULL,
  `quantity` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `inventory_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `quantity` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `invoices_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `order_no` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1271 DEFAULT CHARSET=latin1;

INSERT INTO invoices_tb VALUES("91","2383");
INSERT INTO invoices_tb VALUES("92","2383");
INSERT INTO invoices_tb VALUES("93","2383");
INSERT INTO invoices_tb VALUES("94","2383");
INSERT INTO invoices_tb VALUES("95","2383");
INSERT INTO invoices_tb VALUES("96","2307");
INSERT INTO invoices_tb VALUES("97","1256");
INSERT INTO invoices_tb VALUES("98","4565");
INSERT INTO invoices_tb VALUES("99","1165");
INSERT INTO invoices_tb VALUES("100","207");
INSERT INTO invoices_tb VALUES("101","1070");
INSERT INTO invoices_tb VALUES("102","2062");
INSERT INTO invoices_tb VALUES("103","0");
INSERT INTO invoices_tb VALUES("104","0");
INSERT INTO invoices_tb VALUES("105","0");
INSERT INTO invoices_tb VALUES("106","0");
INSERT INTO invoices_tb VALUES("107","0");
INSERT INTO invoices_tb VALUES("108","0");
INSERT INTO invoices_tb VALUES("109","0");
INSERT INTO invoices_tb VALUES("110","0");
INSERT INTO invoices_tb VALUES("111","0");
INSERT INTO invoices_tb VALUES("112","0");
INSERT INTO invoices_tb VALUES("113","0");
INSERT INTO invoices_tb VALUES("114","752");
INSERT INTO invoices_tb VALUES("115","752");
INSERT INTO invoices_tb VALUES("116","1393");
INSERT INTO invoices_tb VALUES("117","1393");
INSERT INTO invoices_tb VALUES("118","1393");
INSERT INTO invoices_tb VALUES("119","1393");
INSERT INTO invoices_tb VALUES("120","1393");
INSERT INTO invoices_tb VALUES("121","1393");
INSERT INTO invoices_tb VALUES("122","1393");
INSERT INTO invoices_tb VALUES("123","4611");
INSERT INTO invoices_tb VALUES("124","4611");
INSERT INTO invoices_tb VALUES("125","4611");
INSERT INTO invoices_tb VALUES("126","4611");
INSERT INTO invoices_tb VALUES("127","4611");
INSERT INTO invoices_tb VALUES("128","4611");
INSERT INTO invoices_tb VALUES("129","4611");
INSERT INTO invoices_tb VALUES("130","4611");
INSERT INTO invoices_tb VALUES("131","4611");
INSERT INTO invoices_tb VALUES("132","4611");
INSERT INTO invoices_tb VALUES("133","4611");
INSERT INTO invoices_tb VALUES("134","3197");
INSERT INTO invoices_tb VALUES("135","3197");
INSERT INTO invoices_tb VALUES("136","3197");
INSERT INTO invoices_tb VALUES("137","3197");
INSERT INTO invoices_tb VALUES("138","3197");
INSERT INTO invoices_tb VALUES("139","3197");
INSERT INTO invoices_tb VALUES("140","3197");
INSERT INTO invoices_tb VALUES("141","3197");
INSERT INTO invoices_tb VALUES("142","3197");
INSERT INTO invoices_tb VALUES("143","3197");
INSERT INTO invoices_tb VALUES("144","1865");
INSERT INTO invoices_tb VALUES("145","1865");
INSERT INTO invoices_tb VALUES("146","1865");
INSERT INTO invoices_tb VALUES("147","1865");
INSERT INTO invoices_tb VALUES("148","2692");
INSERT INTO invoices_tb VALUES("149","1737");
INSERT INTO invoices_tb VALUES("150","1737");
INSERT INTO invoices_tb VALUES("151","4089");
INSERT INTO invoices_tb VALUES("152","1082");
INSERT INTO invoices_tb VALUES("153","1082");
INSERT INTO invoices_tb VALUES("154","807");
INSERT INTO invoices_tb VALUES("155","2203");
INSERT INTO invoices_tb VALUES("156","3341");
INSERT INTO invoices_tb VALUES("157","4087");
INSERT INTO invoices_tb VALUES("158","1719");
INSERT INTO invoices_tb VALUES("159","348");
INSERT INTO invoices_tb VALUES("160","3039");
INSERT INTO invoices_tb VALUES("161","3039");
INSERT INTO invoices_tb VALUES("162","3136");
INSERT INTO invoices_tb VALUES("163","4598");
INSERT INTO invoices_tb VALUES("164","4893");
INSERT INTO invoices_tb VALUES("165","1609");
INSERT INTO invoices_tb VALUES("166","212");
INSERT INTO invoices_tb VALUES("167","812");
INSERT INTO invoices_tb VALUES("168","812");
INSERT INTO invoices_tb VALUES("169","3692");
INSERT INTO invoices_tb VALUES("170","4518");
INSERT INTO invoices_tb VALUES("171","0");
INSERT INTO invoices_tb VALUES("172","0");
INSERT INTO invoices_tb VALUES("173","1641");
INSERT INTO invoices_tb VALUES("174","1805");
INSERT INTO invoices_tb VALUES("175","1805");
INSERT INTO invoices_tb VALUES("176","3393");
INSERT INTO invoices_tb VALUES("177","4879");
INSERT INTO invoices_tb VALUES("178","1827");
INSERT INTO invoices_tb VALUES("179","159");
INSERT INTO invoices_tb VALUES("180","4752");
INSERT INTO invoices_tb VALUES("181","2982");
INSERT INTO invoices_tb VALUES("182","3302");
INSERT INTO invoices_tb VALUES("183","1402");
INSERT INTO invoices_tb VALUES("184","1881");
INSERT INTO invoices_tb VALUES("185","2618");
INSERT INTO invoices_tb VALUES("186","1377");
INSERT INTO invoices_tb VALUES("187","1878");
INSERT INTO invoices_tb VALUES("188","383");
INSERT INTO invoices_tb VALUES("189","84");
INSERT INTO invoices_tb VALUES("190","749");
INSERT INTO invoices_tb VALUES("191","4322");
INSERT INTO invoices_tb VALUES("192","1170");
INSERT INTO invoices_tb VALUES("193","978");
INSERT INTO invoices_tb VALUES("194","4821");
INSERT INTO invoices_tb VALUES("195","3640");
INSERT INTO invoices_tb VALUES("196","4218");
INSERT INTO invoices_tb VALUES("197","1540");
INSERT INTO invoices_tb VALUES("198","2757");
INSERT INTO invoices_tb VALUES("199","2757");
INSERT INTO invoices_tb VALUES("200","2757");
INSERT INTO invoices_tb VALUES("201","1075");
INSERT INTO invoices_tb VALUES("202","1075");
INSERT INTO invoices_tb VALUES("203","3476");
INSERT INTO invoices_tb VALUES("204","2018");
INSERT INTO invoices_tb VALUES("205","879");
INSERT INTO invoices_tb VALUES("206","2446");
INSERT INTO invoices_tb VALUES("207","4443");
INSERT INTO invoices_tb VALUES("208","2307");
INSERT INTO invoices_tb VALUES("209","1344");
INSERT INTO invoices_tb VALUES("210","3631");
INSERT INTO invoices_tb VALUES("211","3631");
INSERT INTO invoices_tb VALUES("212","1708");
INSERT INTO invoices_tb VALUES("213","3431");
INSERT INTO invoices_tb VALUES("214","2182");
INSERT INTO invoices_tb VALUES("215","2700");
INSERT INTO invoices_tb VALUES("216","1498");
INSERT INTO invoices_tb VALUES("217","1085");
INSERT INTO invoices_tb VALUES("218","1178");
INSERT INTO invoices_tb VALUES("219","353");
INSERT INTO invoices_tb VALUES("220","4819");
INSERT INTO invoices_tb VALUES("221","4819");
INSERT INTO invoices_tb VALUES("222","4323");
INSERT INTO invoices_tb VALUES("223","511");
INSERT INTO invoices_tb VALUES("224","511");
INSERT INTO invoices_tb VALUES("225","4531");
INSERT INTO invoices_tb VALUES("226","812");
INSERT INTO invoices_tb VALUES("227","1422");
INSERT INTO invoices_tb VALUES("228","442");
INSERT INTO invoices_tb VALUES("229","4438");
INSERT INTO invoices_tb VALUES("230","4126");
INSERT INTO invoices_tb VALUES("231","3095");
INSERT INTO invoices_tb VALUES("232","2277");
INSERT INTO invoices_tb VALUES("233","2784");
INSERT INTO invoices_tb VALUES("234","1218");
INSERT INTO invoices_tb VALUES("235","1617");
INSERT INTO invoices_tb VALUES("236","1617");
INSERT INTO invoices_tb VALUES("237","3399");
INSERT INTO invoices_tb VALUES("238","4617");
INSERT INTO invoices_tb VALUES("239","4181");
INSERT INTO invoices_tb VALUES("240","4181");
INSERT INTO invoices_tb VALUES("241","4181");
INSERT INTO invoices_tb VALUES("242","637");
INSERT INTO invoices_tb VALUES("243","637");
INSERT INTO invoices_tb VALUES("244","637");
INSERT INTO invoices_tb VALUES("245","1940");
INSERT INTO invoices_tb VALUES("246","1940");
INSERT INTO invoices_tb VALUES("247","1389");
INSERT INTO invoices_tb VALUES("248","1389");
INSERT INTO invoices_tb VALUES("249","391");
INSERT INTO invoices_tb VALUES("250","391");
INSERT INTO invoices_tb VALUES("251","391");
INSERT INTO invoices_tb VALUES("252","2486");
INSERT INTO invoices_tb VALUES("253","2486");
INSERT INTO invoices_tb VALUES("254","2486");
INSERT INTO invoices_tb VALUES("255","131");
INSERT INTO invoices_tb VALUES("256","2984");
INSERT INTO invoices_tb VALUES("257","2984");
INSERT INTO invoices_tb VALUES("258","892");
INSERT INTO invoices_tb VALUES("259","892");
INSERT INTO invoices_tb VALUES("260","4069");
INSERT INTO invoices_tb VALUES("261","4069");
INSERT INTO invoices_tb VALUES("262","2752");
INSERT INTO invoices_tb VALUES("263","2440");
INSERT INTO invoices_tb VALUES("264","2446");
INSERT INTO invoices_tb VALUES("265","2688");
INSERT INTO invoices_tb VALUES("266","2485");
INSERT INTO invoices_tb VALUES("267","1222");
INSERT INTO invoices_tb VALUES("268","588");
INSERT INTO invoices_tb VALUES("269","915");
INSERT INTO invoices_tb VALUES("270","3974");
INSERT INTO invoices_tb VALUES("271","3974");
INSERT INTO invoices_tb VALUES("272","3974");
INSERT INTO invoices_tb VALUES("273","3974");
INSERT INTO invoices_tb VALUES("274","4300");
INSERT INTO invoices_tb VALUES("275","699");
INSERT INTO invoices_tb VALUES("276","4703");
INSERT INTO invoices_tb VALUES("277","4494");
INSERT INTO invoices_tb VALUES("278","2254");
INSERT INTO invoices_tb VALUES("279","2911");
INSERT INTO invoices_tb VALUES("280","3146");
INSERT INTO invoices_tb VALUES("281","3608");
INSERT INTO invoices_tb VALUES("282","0");
INSERT INTO invoices_tb VALUES("283","3836");
INSERT INTO invoices_tb VALUES("284","2680");
INSERT INTO invoices_tb VALUES("285","3657");
INSERT INTO invoices_tb VALUES("286","1072");
INSERT INTO invoices_tb VALUES("287","4464");
INSERT INTO invoices_tb VALUES("288","3391");
INSERT INTO invoices_tb VALUES("289","3951");
INSERT INTO invoices_tb VALUES("290","3398");
INSERT INTO invoices_tb VALUES("291","3398");
INSERT INTO invoices_tb VALUES("292","3398");
INSERT INTO invoices_tb VALUES("293","653");
INSERT INTO invoices_tb VALUES("294","653");
INSERT INTO invoices_tb VALUES("295","2413");
INSERT INTO invoices_tb VALUES("296","286");
INSERT INTO invoices_tb VALUES("297","4499");
INSERT INTO invoices_tb VALUES("298","1036");
INSERT INTO invoices_tb VALUES("299","3394");
INSERT INTO invoices_tb VALUES("300","3519");
INSERT INTO invoices_tb VALUES("301","3519");
INSERT INTO invoices_tb VALUES("302","3519");
INSERT INTO invoices_tb VALUES("303","2595");
INSERT INTO invoices_tb VALUES("304","2595");
INSERT INTO invoices_tb VALUES("305","3808");
INSERT INTO invoices_tb VALUES("306","4275");
INSERT INTO invoices_tb VALUES("307","4275");
INSERT INTO invoices_tb VALUES("308","0");
INSERT INTO invoices_tb VALUES("309","0");
INSERT INTO invoices_tb VALUES("310","1816");
INSERT INTO invoices_tb VALUES("311","1816");
INSERT INTO invoices_tb VALUES("312","1899");
INSERT INTO invoices_tb VALUES("313","1899");
INSERT INTO invoices_tb VALUES("314","3778");
INSERT INTO invoices_tb VALUES("315","693");
INSERT INTO invoices_tb VALUES("316","3647");
INSERT INTO invoices_tb VALUES("317","3549");
INSERT INTO invoices_tb VALUES("318","3100");
INSERT INTO invoices_tb VALUES("319","168");
INSERT INTO invoices_tb VALUES("320","168");
INSERT INTO invoices_tb VALUES("321","4731");
INSERT INTO invoices_tb VALUES("322","4731");
INSERT INTO invoices_tb VALUES("323","497");
INSERT INTO invoices_tb VALUES("324","497");
INSERT INTO invoices_tb VALUES("325","220");
INSERT INTO invoices_tb VALUES("326","220");
INSERT INTO invoices_tb VALUES("327","2995");
INSERT INTO invoices_tb VALUES("328","2995");
INSERT INTO invoices_tb VALUES("329","1760");
INSERT INTO invoices_tb VALUES("330","1760");
INSERT INTO invoices_tb VALUES("331","1486");
INSERT INTO invoices_tb VALUES("332","642");
INSERT INTO invoices_tb VALUES("333","2133");
INSERT INTO invoices_tb VALUES("334","2133");
INSERT INTO invoices_tb VALUES("335","3488");
INSERT INTO invoices_tb VALUES("336","3091");
INSERT INTO invoices_tb VALUES("337","4196");
INSERT INTO invoices_tb VALUES("338","4196");
INSERT INTO invoices_tb VALUES("339","544");
INSERT INTO invoices_tb VALUES("340","3713");
INSERT INTO invoices_tb VALUES("341","3713");
INSERT INTO invoices_tb VALUES("342","4074");
INSERT INTO invoices_tb VALUES("343","184");
INSERT INTO invoices_tb VALUES("344","420");
INSERT INTO invoices_tb VALUES("345","3102");
INSERT INTO invoices_tb VALUES("346","3102");
INSERT INTO invoices_tb VALUES("347","3770");
INSERT INTO invoices_tb VALUES("348","1047");
INSERT INTO invoices_tb VALUES("349","1149");
INSERT INTO invoices_tb VALUES("350","138");
INSERT INTO invoices_tb VALUES("351","138");
INSERT INTO invoices_tb VALUES("352","2261");
INSERT INTO invoices_tb VALUES("353","2261");
INSERT INTO invoices_tb VALUES("354","2261");
INSERT INTO invoices_tb VALUES("355","2526");
INSERT INTO invoices_tb VALUES("356","2526");
INSERT INTO invoices_tb VALUES("357","1967");
INSERT INTO invoices_tb VALUES("358","1967");
INSERT INTO invoices_tb VALUES("359","2081");
INSERT INTO invoices_tb VALUES("360","1554");
INSERT INTO invoices_tb VALUES("361","2411");
INSERT INTO invoices_tb VALUES("362","1624");
INSERT INTO invoices_tb VALUES("363","2567");
INSERT INTO invoices_tb VALUES("364","2567");
INSERT INTO invoices_tb VALUES("365","1487");
INSERT INTO invoices_tb VALUES("366","233");
INSERT INTO invoices_tb VALUES("367","4357");
INSERT INTO invoices_tb VALUES("368","4862");
INSERT INTO invoices_tb VALUES("369","4862");
INSERT INTO invoices_tb VALUES("370","306");
INSERT INTO invoices_tb VALUES("371","3632");
INSERT INTO invoices_tb VALUES("372","3470");
INSERT INTO invoices_tb VALUES("373","3470");
INSERT INTO invoices_tb VALUES("374","3470");
INSERT INTO invoices_tb VALUES("375","3470");
INSERT INTO invoices_tb VALUES("376","3470");
INSERT INTO invoices_tb VALUES("377","3938");
INSERT INTO invoices_tb VALUES("378","3938");
INSERT INTO invoices_tb VALUES("379","4685");
INSERT INTO invoices_tb VALUES("380","3715");
INSERT INTO invoices_tb VALUES("381","1023");
INSERT INTO invoices_tb VALUES("382","1005");
INSERT INTO invoices_tb VALUES("383","4867");
INSERT INTO invoices_tb VALUES("384","1613");
INSERT INTO invoices_tb VALUES("385","2917");
INSERT INTO invoices_tb VALUES("386","4087");
INSERT INTO invoices_tb VALUES("387","4952");
INSERT INTO invoices_tb VALUES("388","4534");
INSERT INTO invoices_tb VALUES("389","1937");
INSERT INTO invoices_tb VALUES("390","1937");
INSERT INTO invoices_tb VALUES("391","660");
INSERT INTO invoices_tb VALUES("392","3285");
INSERT INTO invoices_tb VALUES("393","3285");
INSERT INTO invoices_tb VALUES("394","743");
INSERT INTO invoices_tb VALUES("395","743");
INSERT INTO invoices_tb VALUES("396","1316");
INSERT INTO invoices_tb VALUES("397","658");
INSERT INTO invoices_tb VALUES("398","912");
INSERT INTO invoices_tb VALUES("399","789");
INSERT INTO invoices_tb VALUES("400","4114");
INSERT INTO invoices_tb VALUES("401","3777");
INSERT INTO invoices_tb VALUES("402","3777");
INSERT INTO invoices_tb VALUES("403","2616");
INSERT INTO invoices_tb VALUES("404","4904");
INSERT INTO invoices_tb VALUES("405","1963");
INSERT INTO invoices_tb VALUES("406","1126");
INSERT INTO invoices_tb VALUES("407","1835");
INSERT INTO invoices_tb VALUES("408","2012");
INSERT INTO invoices_tb VALUES("409","2012");
INSERT INTO invoices_tb VALUES("410","2012");
INSERT INTO invoices_tb VALUES("411","2012");
INSERT INTO invoices_tb VALUES("412","4392");
INSERT INTO invoices_tb VALUES("413","2529");
INSERT INTO invoices_tb VALUES("414","2965");
INSERT INTO invoices_tb VALUES("415","2965");
INSERT INTO invoices_tb VALUES("416","3495");
INSERT INTO invoices_tb VALUES("417","3495");
INSERT INTO invoices_tb VALUES("418","2121");
INSERT INTO invoices_tb VALUES("419","3664");
INSERT INTO invoices_tb VALUES("420","3664");
INSERT INTO invoices_tb VALUES("421","4271");
INSERT INTO invoices_tb VALUES("422","1955");
INSERT INTO invoices_tb VALUES("423","1955");
INSERT INTO invoices_tb VALUES("424","4708");
INSERT INTO invoices_tb VALUES("425","4708");
INSERT INTO invoices_tb VALUES("426","553");
INSERT INTO invoices_tb VALUES("427","553");
INSERT INTO invoices_tb VALUES("428","629");
INSERT INTO invoices_tb VALUES("429","4166");
INSERT INTO invoices_tb VALUES("430","4326");
INSERT INTO invoices_tb VALUES("431","976");
INSERT INTO invoices_tb VALUES("432","2912");
INSERT INTO invoices_tb VALUES("433","4255");
INSERT INTO invoices_tb VALUES("434","4255");
INSERT INTO invoices_tb VALUES("435","662");
INSERT INTO invoices_tb VALUES("436","1090");
INSERT INTO invoices_tb VALUES("437","106");
INSERT INTO invoices_tb VALUES("438","2661");
INSERT INTO invoices_tb VALUES("439","813");
INSERT INTO invoices_tb VALUES("440","2042");
INSERT INTO invoices_tb VALUES("441","2042");
INSERT INTO invoices_tb VALUES("442","1195");
INSERT INTO invoices_tb VALUES("443","2126");
INSERT INTO invoices_tb VALUES("444","3624");
INSERT INTO invoices_tb VALUES("445","4427");
INSERT INTO invoices_tb VALUES("446","4427");
INSERT INTO invoices_tb VALUES("447","4427");
INSERT INTO invoices_tb VALUES("448","2890");
INSERT INTO invoices_tb VALUES("449","619");
INSERT INTO invoices_tb VALUES("450","3800");
INSERT INTO invoices_tb VALUES("451","3800");
INSERT INTO invoices_tb VALUES("452","4375");
INSERT INTO invoices_tb VALUES("453","2080");
INSERT INTO invoices_tb VALUES("454","2080");
INSERT INTO invoices_tb VALUES("455","2080");
INSERT INTO invoices_tb VALUES("456","1964");
INSERT INTO invoices_tb VALUES("457","3032");
INSERT INTO invoices_tb VALUES("458","1038");
INSERT INTO invoices_tb VALUES("459","1736");
INSERT INTO invoices_tb VALUES("460","1736");
INSERT INTO invoices_tb VALUES("461","2884");
INSERT INTO invoices_tb VALUES("462","980");
INSERT INTO invoices_tb VALUES("463","3142");
INSERT INTO invoices_tb VALUES("464","1737");
INSERT INTO invoices_tb VALUES("465","3570");
INSERT INTO invoices_tb VALUES("466","175");
INSERT INTO invoices_tb VALUES("467","4139");
INSERT INTO invoices_tb VALUES("468","2000");
INSERT INTO invoices_tb VALUES("469","2085");
INSERT INTO invoices_tb VALUES("470","2240");
INSERT INTO invoices_tb VALUES("471","1786");
INSERT INTO invoices_tb VALUES("472","1786");
INSERT INTO invoices_tb VALUES("473","1576");
INSERT INTO invoices_tb VALUES("474","1576");
INSERT INTO invoices_tb VALUES("475","402");
INSERT INTO invoices_tb VALUES("476","3397");
INSERT INTO invoices_tb VALUES("477","1318");
INSERT INTO invoices_tb VALUES("478","1411");
INSERT INTO invoices_tb VALUES("479","3078");
INSERT INTO invoices_tb VALUES("480","975");
INSERT INTO invoices_tb VALUES("481","1337");
INSERT INTO invoices_tb VALUES("482","1072");
INSERT INTO invoices_tb VALUES("483","1339");
INSERT INTO invoices_tb VALUES("484","1912");
INSERT INTO invoices_tb VALUES("485","2166");
INSERT INTO invoices_tb VALUES("486","370");
INSERT INTO invoices_tb VALUES("487","3931");
INSERT INTO invoices_tb VALUES("488","3340");
INSERT INTO invoices_tb VALUES("489","2272");
INSERT INTO invoices_tb VALUES("490","1747");
INSERT INTO invoices_tb VALUES("491","4076");
INSERT INTO invoices_tb VALUES("492","3523");
INSERT INTO invoices_tb VALUES("493","3617");
INSERT INTO invoices_tb VALUES("494","2526");
INSERT INTO invoices_tb VALUES("495","2067");
INSERT INTO invoices_tb VALUES("496","2440");
INSERT INTO invoices_tb VALUES("497","3468");
INSERT INTO invoices_tb VALUES("498","4434");
INSERT INTO invoices_tb VALUES("499","4434");
INSERT INTO invoices_tb VALUES("500","4434");
INSERT INTO invoices_tb VALUES("501","4434");
INSERT INTO invoices_tb VALUES("502","4434");
INSERT INTO invoices_tb VALUES("503","4649");
INSERT INTO invoices_tb VALUES("504","4711");
INSERT INTO invoices_tb VALUES("505","4272");
INSERT INTO invoices_tb VALUES("506","4347");
INSERT INTO invoices_tb VALUES("507","1139");
INSERT INTO invoices_tb VALUES("508","1252");
INSERT INTO invoices_tb VALUES("509","474");
INSERT INTO invoices_tb VALUES("510","4486");
INSERT INTO invoices_tb VALUES("511","4486");
INSERT INTO invoices_tb VALUES("512","1716");
INSERT INTO invoices_tb VALUES("513","1877");
INSERT INTO invoices_tb VALUES("514","431");
INSERT INTO invoices_tb VALUES("515","247");
INSERT INTO invoices_tb VALUES("516","1732");
INSERT INTO invoices_tb VALUES("517","1732");
INSERT INTO invoices_tb VALUES("518","472");
INSERT INTO invoices_tb VALUES("519","472");
INSERT INTO invoices_tb VALUES("520","2283");
INSERT INTO invoices_tb VALUES("521","1875");
INSERT INTO invoices_tb VALUES("522","4680");
INSERT INTO invoices_tb VALUES("523","3444");
INSERT INTO invoices_tb VALUES("524","3950");
INSERT INTO invoices_tb VALUES("525","4586");
INSERT INTO invoices_tb VALUES("526","1446");
INSERT INTO invoices_tb VALUES("527","3834");
INSERT INTO invoices_tb VALUES("528","2864");
INSERT INTO invoices_tb VALUES("529","4764");
INSERT INTO invoices_tb VALUES("530","879");
INSERT INTO invoices_tb VALUES("531","4502");
INSERT INTO invoices_tb VALUES("532","4467");
INSERT INTO invoices_tb VALUES("533","1762");
INSERT INTO invoices_tb VALUES("534","3062");
INSERT INTO invoices_tb VALUES("535","1627");
INSERT INTO invoices_tb VALUES("536","2960");
INSERT INTO invoices_tb VALUES("537","3403");
INSERT INTO invoices_tb VALUES("538","1976");
INSERT INTO invoices_tb VALUES("539","2452");
INSERT INTO invoices_tb VALUES("540","4366");
INSERT INTO invoices_tb VALUES("541","3643");
INSERT INTO invoices_tb VALUES("542","320");
INSERT INTO invoices_tb VALUES("543","320");
INSERT INTO invoices_tb VALUES("544","2605");
INSERT INTO invoices_tb VALUES("545","3651");
INSERT INTO invoices_tb VALUES("546","3390");
INSERT INTO invoices_tb VALUES("547","2427");
INSERT INTO invoices_tb VALUES("548","2427");
INSERT INTO invoices_tb VALUES("549","614");
INSERT INTO invoices_tb VALUES("550","2535");
INSERT INTO invoices_tb VALUES("551","1882");
INSERT INTO invoices_tb VALUES("552","1832");
INSERT INTO invoices_tb VALUES("553","4106");
INSERT INTO invoices_tb VALUES("554","4100");
INSERT INTO invoices_tb VALUES("555","1979");
INSERT INTO invoices_tb VALUES("556","3914");
INSERT INTO invoices_tb VALUES("557","1145");
INSERT INTO invoices_tb VALUES("558","1650");
INSERT INTO invoices_tb VALUES("559","1282");
INSERT INTO invoices_tb VALUES("560","4994");
INSERT INTO invoices_tb VALUES("561","3580");
INSERT INTO invoices_tb VALUES("562","1718");
INSERT INTO invoices_tb VALUES("563","436");
INSERT INTO invoices_tb VALUES("564","2765");
INSERT INTO invoices_tb VALUES("565","2295");
INSERT INTO invoices_tb VALUES("566","1008");
INSERT INTO invoices_tb VALUES("567","1017");
INSERT INTO invoices_tb VALUES("568","2747");
INSERT INTO invoices_tb VALUES("569","4118");
INSERT INTO invoices_tb VALUES("570","931");
INSERT INTO invoices_tb VALUES("571","1262");
INSERT INTO invoices_tb VALUES("572","1927");
INSERT INTO invoices_tb VALUES("573","4268");
INSERT INTO invoices_tb VALUES("574","2786");
INSERT INTO invoices_tb VALUES("575","466");
INSERT INTO invoices_tb VALUES("576","4851");
INSERT INTO invoices_tb VALUES("577","446");
INSERT INTO invoices_tb VALUES("578","687");
INSERT INTO invoices_tb VALUES("579","3528");
INSERT INTO invoices_tb VALUES("580","4931");
INSERT INTO invoices_tb VALUES("581","4794");
INSERT INTO invoices_tb VALUES("582","1868");
INSERT INTO invoices_tb VALUES("583","871");
INSERT INTO invoices_tb VALUES("584","3773");
INSERT INTO invoices_tb VALUES("585","2398");
INSERT INTO invoices_tb VALUES("586","1830");
INSERT INTO invoices_tb VALUES("587","1830");
INSERT INTO invoices_tb VALUES("588","1888");
INSERT INTO invoices_tb VALUES("589","4488");
INSERT INTO invoices_tb VALUES("590","4488");
INSERT INTO invoices_tb VALUES("591","4374");
INSERT INTO invoices_tb VALUES("592","1272");
INSERT INTO invoices_tb VALUES("593","269");
INSERT INTO invoices_tb VALUES("594","2151");
INSERT INTO invoices_tb VALUES("595","2864");
INSERT INTO invoices_tb VALUES("596","368");
INSERT INTO invoices_tb VALUES("597","3878");
INSERT INTO invoices_tb VALUES("598","327");
INSERT INTO invoices_tb VALUES("599","4304");
INSERT INTO invoices_tb VALUES("600","2435");
INSERT INTO invoices_tb VALUES("601","1703");
INSERT INTO invoices_tb VALUES("602","1084");
INSERT INTO invoices_tb VALUES("603","1084");
INSERT INTO invoices_tb VALUES("604","1084");
INSERT INTO invoices_tb VALUES("605","4904");
INSERT INTO invoices_tb VALUES("606","4120");
INSERT INTO invoices_tb VALUES("607","1354");
INSERT INTO invoices_tb VALUES("608","2977");
INSERT INTO invoices_tb VALUES("609","1434");
INSERT INTO invoices_tb VALUES("610","1434");
INSERT INTO invoices_tb VALUES("611","1434");
INSERT INTO invoices_tb VALUES("612","3867");
INSERT INTO invoices_tb VALUES("613","3892");
INSERT INTO invoices_tb VALUES("614","3556");
INSERT INTO invoices_tb VALUES("615","3981");
INSERT INTO invoices_tb VALUES("616","4223");
INSERT INTO invoices_tb VALUES("617","402");
INSERT INTO invoices_tb VALUES("618","524");
INSERT INTO invoices_tb VALUES("619","3997");
INSERT INTO invoices_tb VALUES("620","318");
INSERT INTO invoices_tb VALUES("621","1464");
INSERT INTO invoices_tb VALUES("622","4239");
INSERT INTO invoices_tb VALUES("623","4075");
INSERT INTO invoices_tb VALUES("624","4782");
INSERT INTO invoices_tb VALUES("625","1894");
INSERT INTO invoices_tb VALUES("626","1443");
INSERT INTO invoices_tb VALUES("627","3190");
INSERT INTO invoices_tb VALUES("628","3722");
INSERT INTO invoices_tb VALUES("629","2381");
INSERT INTO invoices_tb VALUES("630","781");
INSERT INTO invoices_tb VALUES("631","983");
INSERT INTO invoices_tb VALUES("632","2813");
INSERT INTO invoices_tb VALUES("633","76");
INSERT INTO invoices_tb VALUES("634","307");
INSERT INTO invoices_tb VALUES("635","1068");
INSERT INTO invoices_tb VALUES("636","3085");
INSERT INTO invoices_tb VALUES("637","2436");
INSERT INTO invoices_tb VALUES("638","4643");
INSERT INTO invoices_tb VALUES("639","1219");
INSERT INTO invoices_tb VALUES("640","4573");
INSERT INTO invoices_tb VALUES("641","3185");
INSERT INTO invoices_tb VALUES("642","4766");
INSERT INTO invoices_tb VALUES("643","269");
INSERT INTO invoices_tb VALUES("644","765");
INSERT INTO invoices_tb VALUES("645","804");
INSERT INTO invoices_tb VALUES("646","4669");
INSERT INTO invoices_tb VALUES("647","3413");
INSERT INTO invoices_tb VALUES("648","2285");
INSERT INTO invoices_tb VALUES("649","4576");
INSERT INTO invoices_tb VALUES("650","1380");
INSERT INTO invoices_tb VALUES("651","4852");
INSERT INTO invoices_tb VALUES("652","4163");
INSERT INTO invoices_tb VALUES("653","3841");
INSERT INTO invoices_tb VALUES("654","2017");
INSERT INTO invoices_tb VALUES("655","3271");
INSERT INTO invoices_tb VALUES("656","2072");
INSERT INTO invoices_tb VALUES("657","4496");
INSERT INTO invoices_tb VALUES("658","4837");
INSERT INTO invoices_tb VALUES("659","430");
INSERT INTO invoices_tb VALUES("660","4309");
INSERT INTO invoices_tb VALUES("661","3180");
INSERT INTO invoices_tb VALUES("662","4611");
INSERT INTO invoices_tb VALUES("663","3847");
INSERT INTO invoices_tb VALUES("664","2499");
INSERT INTO invoices_tb VALUES("665","430");
INSERT INTO invoices_tb VALUES("666","655");
INSERT INTO invoices_tb VALUES("667","3478");
INSERT INTO invoices_tb VALUES("668","4958");
INSERT INTO invoices_tb VALUES("669","814");
INSERT INTO invoices_tb VALUES("670","2489");
INSERT INTO invoices_tb VALUES("671","4282");
INSERT INTO invoices_tb VALUES("672","2545");
INSERT INTO invoices_tb VALUES("673","1252");
INSERT INTO invoices_tb VALUES("674","1748");
INSERT INTO invoices_tb VALUES("675","3724");
INSERT INTO invoices_tb VALUES("676","383");
INSERT INTO invoices_tb VALUES("677","3084");
INSERT INTO invoices_tb VALUES("678","647");
INSERT INTO invoices_tb VALUES("679","1054");
INSERT INTO invoices_tb VALUES("680","2812");
INSERT INTO invoices_tb VALUES("681","1001");
INSERT INTO invoices_tb VALUES("682","4142");
INSERT INTO invoices_tb VALUES("683","1514");
INSERT INTO invoices_tb VALUES("684","1514");
INSERT INTO invoices_tb VALUES("685","4729");
INSERT INTO invoices_tb VALUES("686","4286");
INSERT INTO invoices_tb VALUES("687","4631");
INSERT INTO invoices_tb VALUES("688","1717");
INSERT INTO invoices_tb VALUES("689","1717");
INSERT INTO invoices_tb VALUES("690","2845");
INSERT INTO invoices_tb VALUES("691","2808");
INSERT INTO invoices_tb VALUES("692","4458");
INSERT INTO invoices_tb VALUES("693","4722");
INSERT INTO invoices_tb VALUES("694","2882");
INSERT INTO invoices_tb VALUES("695","4817");
INSERT INTO invoices_tb VALUES("696","4270");
INSERT INTO invoices_tb VALUES("697","2628");
INSERT INTO invoices_tb VALUES("698","966");
INSERT INTO invoices_tb VALUES("699","2651");
INSERT INTO invoices_tb VALUES("700","1680");
INSERT INTO invoices_tb VALUES("701","1506");
INSERT INTO invoices_tb VALUES("702","1506");
INSERT INTO invoices_tb VALUES("703","797");
INSERT INTO invoices_tb VALUES("704","547");
INSERT INTO invoices_tb VALUES("705","3676");
INSERT INTO invoices_tb VALUES("706","3327");
INSERT INTO invoices_tb VALUES("707","1913");
INSERT INTO invoices_tb VALUES("708","2942");
INSERT INTO invoices_tb VALUES("709","2692");
INSERT INTO invoices_tb VALUES("710","687");
INSERT INTO invoices_tb VALUES("711","3007");
INSERT INTO invoices_tb VALUES("712","2271");
INSERT INTO invoices_tb VALUES("713","301");
INSERT INTO invoices_tb VALUES("714","1088");
INSERT INTO invoices_tb VALUES("715","1746");
INSERT INTO invoices_tb VALUES("716","1994");
INSERT INTO invoices_tb VALUES("717","4245");
INSERT INTO invoices_tb VALUES("718","3617");
INSERT INTO invoices_tb VALUES("719","4953");
INSERT INTO invoices_tb VALUES("720","3617");
INSERT INTO invoices_tb VALUES("721","1624");
INSERT INTO invoices_tb VALUES("722","4908");
INSERT INTO invoices_tb VALUES("723","1688");
INSERT INTO invoices_tb VALUES("724","1410");
INSERT INTO invoices_tb VALUES("725","1900");
INSERT INTO invoices_tb VALUES("726","823");
INSERT INTO invoices_tb VALUES("727","2530");
INSERT INTO invoices_tb VALUES("728","2530");
INSERT INTO invoices_tb VALUES("729","560");
INSERT INTO invoices_tb VALUES("730","3618");
INSERT INTO invoices_tb VALUES("731","1619");
INSERT INTO invoices_tb VALUES("732","1618");
INSERT INTO invoices_tb VALUES("733","874");
INSERT INTO invoices_tb VALUES("734","331");
INSERT INTO invoices_tb VALUES("735","3674");
INSERT INTO invoices_tb VALUES("736","3194");
INSERT INTO invoices_tb VALUES("737","4003");
INSERT INTO invoices_tb VALUES("738","718");
INSERT INTO invoices_tb VALUES("739","1829");
INSERT INTO invoices_tb VALUES("740","2197");
INSERT INTO invoices_tb VALUES("741","2009");
INSERT INTO invoices_tb VALUES("742","4866");
INSERT INTO invoices_tb VALUES("743","4549");
INSERT INTO invoices_tb VALUES("744","4420");
INSERT INTO invoices_tb VALUES("745","4737");
INSERT INTO invoices_tb VALUES("746","1874");
INSERT INTO invoices_tb VALUES("747","1874");
INSERT INTO invoices_tb VALUES("748","3606");
INSERT INTO invoices_tb VALUES("749","3606");
INSERT INTO invoices_tb VALUES("750","3685");
INSERT INTO invoices_tb VALUES("751","2433");
INSERT INTO invoices_tb VALUES("752","529");
INSERT INTO invoices_tb VALUES("753","2526");
INSERT INTO invoices_tb VALUES("754","801");
INSERT INTO invoices_tb VALUES("755","4435");
INSERT INTO invoices_tb VALUES("756","1449");
INSERT INTO invoices_tb VALUES("757","149");
INSERT INTO invoices_tb VALUES("758","3554");
INSERT INTO invoices_tb VALUES("759","2261");
INSERT INTO invoices_tb VALUES("760","781");
INSERT INTO invoices_tb VALUES("761","1613");
INSERT INTO invoices_tb VALUES("762","4333");
INSERT INTO invoices_tb VALUES("763","4333");
INSERT INTO invoices_tb VALUES("764","2066");
INSERT INTO invoices_tb VALUES("765","1689");
INSERT INTO invoices_tb VALUES("766","2517");
INSERT INTO invoices_tb VALUES("767","263");
INSERT INTO invoices_tb VALUES("768","3059");
INSERT INTO invoices_tb VALUES("769","3350");
INSERT INTO invoices_tb VALUES("770","4188");
INSERT INTO invoices_tb VALUES("771","4919");
INSERT INTO invoices_tb VALUES("772","1544");
INSERT INTO invoices_tb VALUES("773","3500");
INSERT INTO invoices_tb VALUES("774","4667");
INSERT INTO invoices_tb VALUES("775","3347");
INSERT INTO invoices_tb VALUES("776","4874");
INSERT INTO invoices_tb VALUES("777","3225");
INSERT INTO invoices_tb VALUES("778","4924");
INSERT INTO invoices_tb VALUES("779","1608");
INSERT INTO invoices_tb VALUES("780","4886");
INSERT INTO invoices_tb VALUES("781","3699");
INSERT INTO invoices_tb VALUES("782","2892");
INSERT INTO invoices_tb VALUES("783","3481");
INSERT INTO invoices_tb VALUES("784","1203");
INSERT INTO invoices_tb VALUES("785","3497");
INSERT INTO invoices_tb VALUES("786","2142");
INSERT INTO invoices_tb VALUES("787","3560");
INSERT INTO invoices_tb VALUES("788","2236");
INSERT INTO invoices_tb VALUES("789","590");
INSERT INTO invoices_tb VALUES("790","3829");
INSERT INTO invoices_tb VALUES("791","3219");
INSERT INTO invoices_tb VALUES("792","2937");
INSERT INTO invoices_tb VALUES("793","4312");
INSERT INTO invoices_tb VALUES("794","3630");
INSERT INTO invoices_tb VALUES("795","3367");
INSERT INTO invoices_tb VALUES("796","2538");
INSERT INTO invoices_tb VALUES("797","753");
INSERT INTO invoices_tb VALUES("798","1251");
INSERT INTO invoices_tb VALUES("799","3302");
INSERT INTO invoices_tb VALUES("800","686");
INSERT INTO invoices_tb VALUES("801","1957");
INSERT INTO invoices_tb VALUES("802","3645");
INSERT INTO invoices_tb VALUES("803","1982");
INSERT INTO invoices_tb VALUES("804","2988");
INSERT INTO invoices_tb VALUES("805","2093");
INSERT INTO invoices_tb VALUES("806","3974");
INSERT INTO invoices_tb VALUES("807","1405");
INSERT INTO invoices_tb VALUES("808","4697");
INSERT INTO invoices_tb VALUES("809","666");
INSERT INTO invoices_tb VALUES("810","232");
INSERT INTO invoices_tb VALUES("811","4984");
INSERT INTO invoices_tb VALUES("812","3073");
INSERT INTO invoices_tb VALUES("813","1049");
INSERT INTO invoices_tb VALUES("814","3941");
INSERT INTO invoices_tb VALUES("815","1575");
INSERT INTO invoices_tb VALUES("816","1575");
INSERT INTO invoices_tb VALUES("817","522");
INSERT INTO invoices_tb VALUES("818","4285");
INSERT INTO invoices_tb VALUES("819","880");
INSERT INTO invoices_tb VALUES("820","1699");
INSERT INTO invoices_tb VALUES("821","4191");
INSERT INTO invoices_tb VALUES("822","2708");
INSERT INTO invoices_tb VALUES("823","1644");
INSERT INTO invoices_tb VALUES("824","1053");
INSERT INTO invoices_tb VALUES("825","2905");
INSERT INTO invoices_tb VALUES("826","2190");
INSERT INTO invoices_tb VALUES("827","3094");
INSERT INTO invoices_tb VALUES("828","3664");
INSERT INTO invoices_tb VALUES("829","1601");
INSERT INTO invoices_tb VALUES("830","1648");
INSERT INTO invoices_tb VALUES("831","2611");
INSERT INTO invoices_tb VALUES("832","2712");
INSERT INTO invoices_tb VALUES("833","2851");
INSERT INTO invoices_tb VALUES("834","3252");
INSERT INTO invoices_tb VALUES("835","3978");
INSERT INTO invoices_tb VALUES("836","2974");
INSERT INTO invoices_tb VALUES("837","400");
INSERT INTO invoices_tb VALUES("838","400");
INSERT INTO invoices_tb VALUES("839","3274");
INSERT INTO invoices_tb VALUES("840","212");
INSERT INTO invoices_tb VALUES("841","3602");
INSERT INTO invoices_tb VALUES("842","2341");
INSERT INTO invoices_tb VALUES("843","3549");
INSERT INTO invoices_tb VALUES("844","143");
INSERT INTO invoices_tb VALUES("845","3426");
INSERT INTO invoices_tb VALUES("846","192");
INSERT INTO invoices_tb VALUES("847","1683");
INSERT INTO invoices_tb VALUES("848","567");
INSERT INTO invoices_tb VALUES("849","2042");
INSERT INTO invoices_tb VALUES("850","2329");
INSERT INTO invoices_tb VALUES("851","4110");
INSERT INTO invoices_tb VALUES("852","4110");
INSERT INTO invoices_tb VALUES("853","677");
INSERT INTO invoices_tb VALUES("854","4896");
INSERT INTO invoices_tb VALUES("855","4896");
INSERT INTO invoices_tb VALUES("856","4418");
INSERT INTO invoices_tb VALUES("857","4418");
INSERT INTO invoices_tb VALUES("858","4418");
INSERT INTO invoices_tb VALUES("859","627");
INSERT INTO invoices_tb VALUES("860","4302");
INSERT INTO invoices_tb VALUES("861","3025");
INSERT INTO invoices_tb VALUES("862","855");
INSERT INTO invoices_tb VALUES("863","1363");
INSERT INTO invoices_tb VALUES("864","1946");
INSERT INTO invoices_tb VALUES("865","2580");
INSERT INTO invoices_tb VALUES("866","2045");
INSERT INTO invoices_tb VALUES("867","3711");
INSERT INTO invoices_tb VALUES("868","1962");
INSERT INTO invoices_tb VALUES("869","4967");
INSERT INTO invoices_tb VALUES("870","2289");
INSERT INTO invoices_tb VALUES("871","1845");
INSERT INTO invoices_tb VALUES("872","4508");
INSERT INTO invoices_tb VALUES("873","2366");
INSERT INTO invoices_tb VALUES("874","3176");
INSERT INTO invoices_tb VALUES("875","3237");
INSERT INTO invoices_tb VALUES("876","4834");
INSERT INTO invoices_tb VALUES("877","1387");
INSERT INTO invoices_tb VALUES("878","4608");
INSERT INTO invoices_tb VALUES("879","4992");
INSERT INTO invoices_tb VALUES("880","4992");
INSERT INTO invoices_tb VALUES("881","2703");
INSERT INTO invoices_tb VALUES("882","2303");
INSERT INTO invoices_tb VALUES("883","3983");
INSERT INTO invoices_tb VALUES("884","393");
INSERT INTO invoices_tb VALUES("885","395");
INSERT INTO invoices_tb VALUES("886","1655");
INSERT INTO invoices_tb VALUES("887","827");
INSERT INTO invoices_tb VALUES("888","4895");
INSERT INTO invoices_tb VALUES("889","3010");
INSERT INTO invoices_tb VALUES("890","2167");
INSERT INTO invoices_tb VALUES("891","1088");
INSERT INTO invoices_tb VALUES("892","1942");
INSERT INTO invoices_tb VALUES("893","3477");
INSERT INTO invoices_tb VALUES("894","3904");
INSERT INTO invoices_tb VALUES("895","4020");
INSERT INTO invoices_tb VALUES("896","4732");
INSERT INTO invoices_tb VALUES("897","4800");
INSERT INTO invoices_tb VALUES("898","2417");
INSERT INTO invoices_tb VALUES("899","1801");
INSERT INTO invoices_tb VALUES("900","4191");
INSERT INTO invoices_tb VALUES("901","4178");
INSERT INTO invoices_tb VALUES("902","1866");
INSERT INTO invoices_tb VALUES("903","1912");
INSERT INTO invoices_tb VALUES("904","2761");
INSERT INTO invoices_tb VALUES("905","2840");
INSERT INTO invoices_tb VALUES("906","2977");
INSERT INTO invoices_tb VALUES("907","2977");
INSERT INTO invoices_tb VALUES("908","1994");
INSERT INTO invoices_tb VALUES("909","2542");
INSERT INTO invoices_tb VALUES("910","1117");
INSERT INTO invoices_tb VALUES("911","4356");
INSERT INTO invoices_tb VALUES("912","358");
INSERT INTO invoices_tb VALUES("913","4229");
INSERT INTO invoices_tb VALUES("914","4199");
INSERT INTO invoices_tb VALUES("915","1419");
INSERT INTO invoices_tb VALUES("916","3768");
INSERT INTO invoices_tb VALUES("917","3255");
INSERT INTO invoices_tb VALUES("918","4137");
INSERT INTO invoices_tb VALUES("919","1962");
INSERT INTO invoices_tb VALUES("920","2434");
INSERT INTO invoices_tb VALUES("921","1488");
INSERT INTO invoices_tb VALUES("922","161");
INSERT INTO invoices_tb VALUES("923","4911");
INSERT INTO invoices_tb VALUES("924","2513");
INSERT INTO invoices_tb VALUES("925","4661");
INSERT INTO invoices_tb VALUES("926","86");
INSERT INTO invoices_tb VALUES("927","4224");
INSERT INTO invoices_tb VALUES("928","3387");
INSERT INTO invoices_tb VALUES("929","1719");
INSERT INTO invoices_tb VALUES("930","2735");
INSERT INTO invoices_tb VALUES("931","4440");
INSERT INTO invoices_tb VALUES("932","3335");
INSERT INTO invoices_tb VALUES("933","4202");
INSERT INTO invoices_tb VALUES("934","1466");
INSERT INTO invoices_tb VALUES("935","4388");
INSERT INTO invoices_tb VALUES("936","196");
INSERT INTO invoices_tb VALUES("937","78");
INSERT INTO invoices_tb VALUES("938","1167");
INSERT INTO invoices_tb VALUES("939","4264");
INSERT INTO invoices_tb VALUES("940","1657");
INSERT INTO invoices_tb VALUES("941","3449");
INSERT INTO invoices_tb VALUES("942","4114");
INSERT INTO invoices_tb VALUES("943","2367");
INSERT INTO invoices_tb VALUES("944","2513");
INSERT INTO invoices_tb VALUES("945","2941");
INSERT INTO invoices_tb VALUES("946","776");
INSERT INTO invoices_tb VALUES("947","202");
INSERT INTO invoices_tb VALUES("948","4584");
INSERT INTO invoices_tb VALUES("949","4317");
INSERT INTO invoices_tb VALUES("950","2268");
INSERT INTO invoices_tb VALUES("951","3034");
INSERT INTO invoices_tb VALUES("952","3930");
INSERT INTO invoices_tb VALUES("953","2192");
INSERT INTO invoices_tb VALUES("954","3052");
INSERT INTO invoices_tb VALUES("955","2235");
INSERT INTO invoices_tb VALUES("956","2943");
INSERT INTO invoices_tb VALUES("957","1601");
INSERT INTO invoices_tb VALUES("958","1601");
INSERT INTO invoices_tb VALUES("959","1601");
INSERT INTO invoices_tb VALUES("960","1601");
INSERT INTO invoices_tb VALUES("961","1601");
INSERT INTO invoices_tb VALUES("962","1601");
INSERT INTO invoices_tb VALUES("963","1601");
INSERT INTO invoices_tb VALUES("964","1601");
INSERT INTO invoices_tb VALUES("965","1601");
INSERT INTO invoices_tb VALUES("966","1601");
INSERT INTO invoices_tb VALUES("967","1601");
INSERT INTO invoices_tb VALUES("968","1601");
INSERT INTO invoices_tb VALUES("969","1601");
INSERT INTO invoices_tb VALUES("970","1601");
INSERT INTO invoices_tb VALUES("971","1601");
INSERT INTO invoices_tb VALUES("972","1601");
INSERT INTO invoices_tb VALUES("973","1601");
INSERT INTO invoices_tb VALUES("974","1601");
INSERT INTO invoices_tb VALUES("975","1601");
INSERT INTO invoices_tb VALUES("976","3004");
INSERT INTO invoices_tb VALUES("977","3004");
INSERT INTO invoices_tb VALUES("978","744");
INSERT INTO invoices_tb VALUES("979","1818");
INSERT INTO invoices_tb VALUES("980","1818");
INSERT INTO invoices_tb VALUES("981","1818");
INSERT INTO invoices_tb VALUES("982","1818");
INSERT INTO invoices_tb VALUES("983","2193");
INSERT INTO invoices_tb VALUES("984","517");
INSERT INTO invoices_tb VALUES("985","4771");
INSERT INTO invoices_tb VALUES("986","4771");
INSERT INTO invoices_tb VALUES("987","3672");
INSERT INTO invoices_tb VALUES("988","1685");
INSERT INTO invoices_tb VALUES("989","251");
INSERT INTO invoices_tb VALUES("990","1484");
INSERT INTO invoices_tb VALUES("991","4183");
INSERT INTO invoices_tb VALUES("992","2653");
INSERT INTO invoices_tb VALUES("993","2727");
INSERT INTO invoices_tb VALUES("994","2727");
INSERT INTO invoices_tb VALUES("995","2321");
INSERT INTO invoices_tb VALUES("996","3097");
INSERT INTO invoices_tb VALUES("997","0");
INSERT INTO invoices_tb VALUES("998","4998");
INSERT INTO invoices_tb VALUES("999","1794");
INSERT INTO invoices_tb VALUES("1000","0");
INSERT INTO invoices_tb VALUES("1001","2271");
INSERT INTO invoices_tb VALUES("1002","1599");
INSERT INTO invoices_tb VALUES("1003","3167");
INSERT INTO invoices_tb VALUES("1004","3167");
INSERT INTO invoices_tb VALUES("1005","461");
INSERT INTO invoices_tb VALUES("1006","774");
INSERT INTO invoices_tb VALUES("1007","4394");
INSERT INTO invoices_tb VALUES("1008","3732");
INSERT INTO invoices_tb VALUES("1009","4556");
INSERT INTO invoices_tb VALUES("1010","530");
INSERT INTO invoices_tb VALUES("1011","4166");
INSERT INTO invoices_tb VALUES("1012","3848");
INSERT INTO invoices_tb VALUES("1013","2855");
INSERT INTO invoices_tb VALUES("1014","4321");
INSERT INTO invoices_tb VALUES("1015","460");
INSERT INTO invoices_tb VALUES("1016","4841");
INSERT INTO invoices_tb VALUES("1017","1279");
INSERT INTO invoices_tb VALUES("1018","1279");
INSERT INTO invoices_tb VALUES("1019","1279");
INSERT INTO invoices_tb VALUES("1020","2767");
INSERT INTO invoices_tb VALUES("1021","3531");
INSERT INTO invoices_tb VALUES("1022","3375");
INSERT INTO invoices_tb VALUES("1023","3430");
INSERT INTO invoices_tb VALUES("1024","3430");
INSERT INTO invoices_tb VALUES("1025","3430");
INSERT INTO invoices_tb VALUES("1026","3430");
INSERT INTO invoices_tb VALUES("1027","3430");
INSERT INTO invoices_tb VALUES("1028","3430");
INSERT INTO invoices_tb VALUES("1029","3430");
INSERT INTO invoices_tb VALUES("1030","3430");
INSERT INTO invoices_tb VALUES("1031","3430");
INSERT INTO invoices_tb VALUES("1032","3430");
INSERT INTO invoices_tb VALUES("1033","3430");
INSERT INTO invoices_tb VALUES("1034","3430");
INSERT INTO invoices_tb VALUES("1035","3430");
INSERT INTO invoices_tb VALUES("1036","3430");
INSERT INTO invoices_tb VALUES("1037","3430");
INSERT INTO invoices_tb VALUES("1038","3430");
INSERT INTO invoices_tb VALUES("1039","3430");
INSERT INTO invoices_tb VALUES("1040","3430");
INSERT INTO invoices_tb VALUES("1041","3430");
INSERT INTO invoices_tb VALUES("1042","3430");
INSERT INTO invoices_tb VALUES("1043","3430");
INSERT INTO invoices_tb VALUES("1044","3430");
INSERT INTO invoices_tb VALUES("1045","3430");
INSERT INTO invoices_tb VALUES("1046","3430");
INSERT INTO invoices_tb VALUES("1047","3430");
INSERT INTO invoices_tb VALUES("1048","3430");
INSERT INTO invoices_tb VALUES("1049","3430");
INSERT INTO invoices_tb VALUES("1050","3430");
INSERT INTO invoices_tb VALUES("1051","3430");
INSERT INTO invoices_tb VALUES("1052","3430");
INSERT INTO invoices_tb VALUES("1053","3430");
INSERT INTO invoices_tb VALUES("1054","3430");
INSERT INTO invoices_tb VALUES("1055","3430");
INSERT INTO invoices_tb VALUES("1056","3430");
INSERT INTO invoices_tb VALUES("1057","3430");
INSERT INTO invoices_tb VALUES("1058","3430");
INSERT INTO invoices_tb VALUES("1059","3430");
INSERT INTO invoices_tb VALUES("1060","3430");
INSERT INTO invoices_tb VALUES("1061","3430");
INSERT INTO invoices_tb VALUES("1062","3430");
INSERT INTO invoices_tb VALUES("1063","3430");
INSERT INTO invoices_tb VALUES("1064","3430");
INSERT INTO invoices_tb VALUES("1065","3430");
INSERT INTO invoices_tb VALUES("1066","3430");
INSERT INTO invoices_tb VALUES("1067","3430");
INSERT INTO invoices_tb VALUES("1068","3430");
INSERT INTO invoices_tb VALUES("1069","3430");
INSERT INTO invoices_tb VALUES("1070","3430");
INSERT INTO invoices_tb VALUES("1071","3430");
INSERT INTO invoices_tb VALUES("1072","3430");
INSERT INTO invoices_tb VALUES("1073","3430");
INSERT INTO invoices_tb VALUES("1074","3430");
INSERT INTO invoices_tb VALUES("1075","3430");
INSERT INTO invoices_tb VALUES("1076","3430");
INSERT INTO invoices_tb VALUES("1077","3430");
INSERT INTO invoices_tb VALUES("1078","3430");
INSERT INTO invoices_tb VALUES("1079","4318");
INSERT INTO invoices_tb VALUES("1080","4318");
INSERT INTO invoices_tb VALUES("1081","4318");
INSERT INTO invoices_tb VALUES("1082","4318");
INSERT INTO invoices_tb VALUES("1083","4318");
INSERT INTO invoices_tb VALUES("1084","4318");
INSERT INTO invoices_tb VALUES("1085","4318");
INSERT INTO invoices_tb VALUES("1086","4318");
INSERT INTO invoices_tb VALUES("1087","4318");
INSERT INTO invoices_tb VALUES("1088","4318");
INSERT INTO invoices_tb VALUES("1089","4318");
INSERT INTO invoices_tb VALUES("1090","4318");
INSERT INTO invoices_tb VALUES("1091","4318");
INSERT INTO invoices_tb VALUES("1092","4318");
INSERT INTO invoices_tb VALUES("1093","4318");
INSERT INTO invoices_tb VALUES("1094","4318");
INSERT INTO invoices_tb VALUES("1095","4318");
INSERT INTO invoices_tb VALUES("1096","4318");
INSERT INTO invoices_tb VALUES("1097","4318");
INSERT INTO invoices_tb VALUES("1098","4318");
INSERT INTO invoices_tb VALUES("1099","4318");
INSERT INTO invoices_tb VALUES("1100","930");
INSERT INTO invoices_tb VALUES("1101","4878");
INSERT INTO invoices_tb VALUES("1102","1290");
INSERT INTO invoices_tb VALUES("1103","2178");
INSERT INTO invoices_tb VALUES("1104","3649");
INSERT INTO invoices_tb VALUES("1105","792");
INSERT INTO invoices_tb VALUES("1106","55");
INSERT INTO invoices_tb VALUES("1107","895");
INSERT INTO invoices_tb VALUES("1108","1951");
INSERT INTO invoices_tb VALUES("1109","4915");
INSERT INTO invoices_tb VALUES("1110","2389");
INSERT INTO invoices_tb VALUES("1111","2389");
INSERT INTO invoices_tb VALUES("1112","2389");
INSERT INTO invoices_tb VALUES("1113","2389");
INSERT INTO invoices_tb VALUES("1114","2389");
INSERT INTO invoices_tb VALUES("1115","2389");
INSERT INTO invoices_tb VALUES("1116","1139");
INSERT INTO invoices_tb VALUES("1117","1139");
INSERT INTO invoices_tb VALUES("1118","0");
INSERT INTO invoices_tb VALUES("1119","0");
INSERT INTO invoices_tb VALUES("1120","3487");
INSERT INTO invoices_tb VALUES("1121","0");
INSERT INTO invoices_tb VALUES("1122","0");
INSERT INTO invoices_tb VALUES("1123","0");
INSERT INTO invoices_tb VALUES("1124","0");
INSERT INTO invoices_tb VALUES("1125","0");
INSERT INTO invoices_tb VALUES("1126","0");
INSERT INTO invoices_tb VALUES("1127","0");
INSERT INTO invoices_tb VALUES("1128","0");
INSERT INTO invoices_tb VALUES("1129","0");
INSERT INTO invoices_tb VALUES("1130","0");
INSERT INTO invoices_tb VALUES("1131","0");
INSERT INTO invoices_tb VALUES("1132","0");
INSERT INTO invoices_tb VALUES("1133","0");
INSERT INTO invoices_tb VALUES("1134","0");
INSERT INTO invoices_tb VALUES("1135","0");
INSERT INTO invoices_tb VALUES("1136","2995");
INSERT INTO invoices_tb VALUES("1137","4700");
INSERT INTO invoices_tb VALUES("1138","3839");
INSERT INTO invoices_tb VALUES("1139","3806");
INSERT INTO invoices_tb VALUES("1140","1617");
INSERT INTO invoices_tb VALUES("1141","1617");
INSERT INTO invoices_tb VALUES("1142","0");
INSERT INTO invoices_tb VALUES("1143","2751");
INSERT INTO invoices_tb VALUES("1144","2751");
INSERT INTO invoices_tb VALUES("1145","0");
INSERT INTO invoices_tb VALUES("1146","0");
INSERT INTO invoices_tb VALUES("1147","824");
INSERT INTO invoices_tb VALUES("1148","824");
INSERT INTO invoices_tb VALUES("1149","3565");
INSERT INTO invoices_tb VALUES("1150","1582");
INSERT INTO invoices_tb VALUES("1151","1807");
INSERT INTO invoices_tb VALUES("1152","0");
INSERT INTO invoices_tb VALUES("1153","0");
INSERT INTO invoices_tb VALUES("1154","0");
INSERT INTO invoices_tb VALUES("1155","4812");
INSERT INTO invoices_tb VALUES("1156","4212");
INSERT INTO invoices_tb VALUES("1157","423");
INSERT INTO invoices_tb VALUES("1158","4640");
INSERT INTO invoices_tb VALUES("1159","1282");
INSERT INTO invoices_tb VALUES("1160","150");
INSERT INTO invoices_tb VALUES("1161","4125");
INSERT INTO invoices_tb VALUES("1162","1954");
INSERT INTO invoices_tb VALUES("1163","3532");
INSERT INTO invoices_tb VALUES("1164","1880");
INSERT INTO invoices_tb VALUES("1165","1880");
INSERT INTO invoices_tb VALUES("1166","1880");
INSERT INTO invoices_tb VALUES("1167","1880");
INSERT INTO invoices_tb VALUES("1168","1880");
INSERT INTO invoices_tb VALUES("1169","1880");
INSERT INTO invoices_tb VALUES("1170","0");
INSERT INTO invoices_tb VALUES("1171","2425");
INSERT INTO invoices_tb VALUES("1172","522");
INSERT INTO invoices_tb VALUES("1173","522");
INSERT INTO invoices_tb VALUES("1174","522");
INSERT INTO invoices_tb VALUES("1175","4449");
INSERT INTO invoices_tb VALUES("1176","4206");
INSERT INTO invoices_tb VALUES("1177","2057");
INSERT INTO invoices_tb VALUES("1178","2537");
INSERT INTO invoices_tb VALUES("1179","2130");
INSERT INTO invoices_tb VALUES("1180","4770");
INSERT INTO invoices_tb VALUES("1181","3404");
INSERT INTO invoices_tb VALUES("1182","3404");
INSERT INTO invoices_tb VALUES("1183","2542");
INSERT INTO invoices_tb VALUES("1184","0");
INSERT INTO invoices_tb VALUES("1185","2542");
INSERT INTO invoices_tb VALUES("1186","2542");
INSERT INTO invoices_tb VALUES("1187","2542");
INSERT INTO invoices_tb VALUES("1188","2542");
INSERT INTO invoices_tb VALUES("1189","2542");
INSERT INTO invoices_tb VALUES("1190","2542");
INSERT INTO invoices_tb VALUES("1191","2542");
INSERT INTO invoices_tb VALUES("1192","2542");
INSERT INTO invoices_tb VALUES("1193","2542");
INSERT INTO invoices_tb VALUES("1194","2542");
INSERT INTO invoices_tb VALUES("1195","2542");
INSERT INTO invoices_tb VALUES("1196","3073");
INSERT INTO invoices_tb VALUES("1197","2684");
INSERT INTO invoices_tb VALUES("1198","2684");
INSERT INTO invoices_tb VALUES("1199","2684");
INSERT INTO invoices_tb VALUES("1200","4412");
INSERT INTO invoices_tb VALUES("1201","4412");
INSERT INTO invoices_tb VALUES("1202","4922");
INSERT INTO invoices_tb VALUES("1203","3092");
INSERT INTO invoices_tb VALUES("1204","0");
INSERT INTO invoices_tb VALUES("1205","0");
INSERT INTO invoices_tb VALUES("1206","0");
INSERT INTO invoices_tb VALUES("1207","0");
INSERT INTO invoices_tb VALUES("1208","0");
INSERT INTO invoices_tb VALUES("1209","0");
INSERT INTO invoices_tb VALUES("1210","0");
INSERT INTO invoices_tb VALUES("1211","640");
INSERT INTO invoices_tb VALUES("1212","299");
INSERT INTO invoices_tb VALUES("1213","0");
INSERT INTO invoices_tb VALUES("1214","0");
INSERT INTO invoices_tb VALUES("1215","0");
INSERT INTO invoices_tb VALUES("1216","1228");
INSERT INTO invoices_tb VALUES("1217","0");
INSERT INTO invoices_tb VALUES("1218","1193");
INSERT INTO invoices_tb VALUES("1219","3893");
INSERT INTO invoices_tb VALUES("1220","3893");
INSERT INTO invoices_tb VALUES("1221","3563");
INSERT INTO invoices_tb VALUES("1222","1445");
INSERT INTO invoices_tb VALUES("1223","4504");
INSERT INTO invoices_tb VALUES("1224","4504");
INSERT INTO invoices_tb VALUES("1225","4504");
INSERT INTO invoices_tb VALUES("1226","3452");
INSERT INTO invoices_tb VALUES("1227","3262");
INSERT INTO invoices_tb VALUES("1228","3190");
INSERT INTO invoices_tb VALUES("1229","1626");
INSERT INTO invoices_tb VALUES("1230","4010");
INSERT INTO invoices_tb VALUES("1231","2529");
INSERT INTO invoices_tb VALUES("1232","2556");
INSERT INTO invoices_tb VALUES("1233","2429");
INSERT INTO invoices_tb VALUES("1234","2429");
INSERT INTO invoices_tb VALUES("1235","2429");
INSERT INTO invoices_tb VALUES("1236","1564");
INSERT INTO invoices_tb VALUES("1237","2562");
INSERT INTO invoices_tb VALUES("1238","2562");
INSERT INTO invoices_tb VALUES("1239","523");
INSERT INTO invoices_tb VALUES("1240","523");
INSERT INTO invoices_tb VALUES("1241","659");
INSERT INTO invoices_tb VALUES("1242","659");
INSERT INTO invoices_tb VALUES("1243","659");
INSERT INTO invoices_tb VALUES("1244","659");
INSERT INTO invoices_tb VALUES("1245","659");
INSERT INTO invoices_tb VALUES("1246","4847");
INSERT INTO invoices_tb VALUES("1247","1732");
INSERT INTO invoices_tb VALUES("1248","1727");
INSERT INTO invoices_tb VALUES("1249","4235");
INSERT INTO invoices_tb VALUES("1250","1069");
INSERT INTO invoices_tb VALUES("1251","318");
INSERT INTO invoices_tb VALUES("1252","3150");
INSERT INTO invoices_tb VALUES("1253","942");
INSERT INTO invoices_tb VALUES("1254","3040");
INSERT INTO invoices_tb VALUES("1255","3040");
INSERT INTO invoices_tb VALUES("1256","4239");
INSERT INTO invoices_tb VALUES("1257","724");
INSERT INTO invoices_tb VALUES("1258","4323");
INSERT INTO invoices_tb VALUES("1259","4323");
INSERT INTO invoices_tb VALUES("1260","4323");
INSERT INTO invoices_tb VALUES("1261","3942");
INSERT INTO invoices_tb VALUES("1262","3942");
INSERT INTO invoices_tb VALUES("1263","3942");
INSERT INTO invoices_tb VALUES("1264","2848");
INSERT INTO invoices_tb VALUES("1265","2848");
INSERT INTO invoices_tb VALUES("1266","2848");
INSERT INTO invoices_tb VALUES("1267","4059");
INSERT INTO invoices_tb VALUES("1268","2231");
INSERT INTO invoices_tb VALUES("1269","2114");
INSERT INTO invoices_tb VALUES("1270","2479");



CREATE TABLE `licience_reg_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `exp_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `licience_key` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO licience_reg_tb VALUES("1","2021-01-01 00:00:00","");



CREATE TABLE `modes_of_payment_tb` (
  `payment_mode_id` int(12) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`payment_mode_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO modes_of_payment_tb VALUES("1","Cash","2019-11-03 18:29:35");
INSERT INTO modes_of_payment_tb VALUES("3","MTN Mobile Money","2019-11-03 18:40:42");
INSERT INTO modes_of_payment_tb VALUES("4","VISA","2019-11-05 09:22:27");



CREATE TABLE `notifications_settings_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `not_count` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `open_close_cashout_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `login` text NOT NULL,
  `logout` text NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=latin1;

INSERT INTO open_close_cashout_tb VALUES("69","9","2019-05-01 16:26:56","2019-05-01 16:38:52","closed");
INSERT INTO open_close_cashout_tb VALUES("70","9","2019-05-01 17:47:22","2019-05-01 17:47:36","closed");
INSERT INTO open_close_cashout_tb VALUES("71","9","2019-05-02 08:05:35","2019-05-02 08:53:45","closed");
INSERT INTO open_close_cashout_tb VALUES("72","9","2019-05-02 09:15:44","2019-05-03 17:21:56","closed");
INSERT INTO open_close_cashout_tb VALUES("73","9","2019-05-04 07:52:51","2019-05-06 08:43:56","closed");
INSERT INTO open_close_cashout_tb VALUES("74","9","2019-05-06 09:31:14","2019-05-06 17:00:53","closed");
INSERT INTO open_close_cashout_tb VALUES("75","8","2019-05-06 13:02:54","2019-05-06 13:03:43","closed");
INSERT INTO open_close_cashout_tb VALUES("76","9","2019-05-06 17:21:32","2019-05-06 17:52:22","closed");
INSERT INTO open_close_cashout_tb VALUES("77","9","2019-05-07 07:42:09","2019-05-07 09:10:06","closed");
INSERT INTO open_close_cashout_tb VALUES("78","9","2019-05-07 09:10:13","2019-05-07 09:10:16","closed");
INSERT INTO open_close_cashout_tb VALUES("79","9","2019-05-07 09:18:20","2019-05-07 11:32:03","closed");
INSERT INTO open_close_cashout_tb VALUES("80","9","2019-05-07 15:07:30","2019-05-08 08:44:27","closed");
INSERT INTO open_close_cashout_tb VALUES("81","9","2019-05-08 08:57:30","2019-05-08 09:03:21","closed");
INSERT INTO open_close_cashout_tb VALUES("82","9","2019-05-08 09:06:08","2019-05-08 09:15:43","closed");
INSERT INTO open_close_cashout_tb VALUES("83","9","2019-05-08 09:30:18","2019-05-08 09:30:37","closed");
INSERT INTO open_close_cashout_tb VALUES("84","9","2019-05-08 10:32:11","2019-05-08 10:50:35","closed");
INSERT INTO open_close_cashout_tb VALUES("85","13","2019-05-08 10:50:24","2019-05-08 10:51:16","closed");
INSERT INTO open_close_cashout_tb VALUES("86","9","2019-05-08 10:52:58","2019-05-08 18:58:58","closed");
INSERT INTO open_close_cashout_tb VALUES("87","9","2019-05-08 19:02:44","2019-05-08 20:54:59","closed");
INSERT INTO open_close_cashout_tb VALUES("88","9","2019-05-09 08:06:22","2019-05-09 18:55:12","closed");
INSERT INTO open_close_cashout_tb VALUES("89","8","2019-05-09 14:35:30","2019-05-09 14:36:34","closed");
INSERT INTO open_close_cashout_tb VALUES("90","9","2019-05-10 08:10:45","2019-05-10 18:32:31","closed");
INSERT INTO open_close_cashout_tb VALUES("91","9","2019-05-11 07:58:20","2019-05-11 21:29:49","closed");
INSERT INTO open_close_cashout_tb VALUES("92","9","2019-05-13 08:07:25","2019-05-13 08:44:14","closed");
INSERT INTO open_close_cashout_tb VALUES("93","9","2019-05-13 08:46:20","2019-05-13 11:22:16","closed");
INSERT INTO open_close_cashout_tb VALUES("94","9","2019-05-13 12:07:54","2019-05-13 17:43:31","closed");
INSERT INTO open_close_cashout_tb VALUES("95","9","2019-05-14 07:45:44","2019-05-14 17:47:48","closed");
INSERT INTO open_close_cashout_tb VALUES("96","9","2019-05-15 08:07:26","2019-05-16 18:47:22","closed");
INSERT INTO open_close_cashout_tb VALUES("97","9","2019-05-17 08:12:21","2019-05-17 17:48:00","closed");
INSERT INTO open_close_cashout_tb VALUES("98","9","2019-05-18 08:17:03","2019-05-18 17:37:41","closed");
INSERT INTO open_close_cashout_tb VALUES("99","9","2019-05-20 08:19:52","2019-05-20 17:35:27","closed");
INSERT INTO open_close_cashout_tb VALUES("100","9","2019-05-21 08:14:43","2019-05-21 17:06:59","closed");
INSERT INTO open_close_cashout_tb VALUES("101","9","2019-05-21 17:35:55","2019-05-21 17:36:29","closed");
INSERT INTO open_close_cashout_tb VALUES("102","9","2019-05-21 18:25:02","2019-05-21 18:28:40","closed");
INSERT INTO open_close_cashout_tb VALUES("103","9","2019-05-22 07:51:03","2019-05-22 09:00:57","closed");
INSERT INTO open_close_cashout_tb VALUES("104","9","2019-05-22 14:28:27","2019-05-22 18:26:45","closed");
INSERT INTO open_close_cashout_tb VALUES("105","8","2019-05-22 16:56:53","2019-05-22 16:57:32","closed");
INSERT INTO open_close_cashout_tb VALUES("106","9","2019-05-23 08:17:22","2019-05-23 19:06:48","closed");
INSERT INTO open_close_cashout_tb VALUES("107","9","2019-05-23 19:26:22","2019-05-23 19:26:44","closed");
INSERT INTO open_close_cashout_tb VALUES("108","9","2019-05-24 08:10:19","2019-05-24 21:23:54","closed");
INSERT INTO open_close_cashout_tb VALUES("109","13","2019-05-24 10:09:54","","");
INSERT INTO open_close_cashout_tb VALUES("110","9","2019-05-25 09:05:30","2019-05-27 17:36:35","closed");
INSERT INTO open_close_cashout_tb VALUES("111","9","2019-05-28 08:09:46","2019-05-28 08:51:46","closed");
INSERT INTO open_close_cashout_tb VALUES("112","9","2019-05-28 09:27:14","2019-05-28 09:31:26","closed");
INSERT INTO open_close_cashout_tb VALUES("113","9","2019-05-28 11:03:32","2019-05-29 21:31:24","closed");
INSERT INTO open_close_cashout_tb VALUES("114","9","2019-05-29 21:31:40","2019-05-29 21:32:10","closed");
INSERT INTO open_close_cashout_tb VALUES("115","9","2019-05-30 08:22:30","2019-05-30 09:24:18","closed");
INSERT INTO open_close_cashout_tb VALUES("116","9","2019-05-30 09:24:24","2019-05-30 09:24:32","closed");
INSERT INTO open_close_cashout_tb VALUES("117","9","2019-05-30 13:20:46","2019-05-30 17:56:44","closed");
INSERT INTO open_close_cashout_tb VALUES("118","9","2019-05-31 09:38:09","2019-05-31 20:10:08","closed");
INSERT INTO open_close_cashout_tb VALUES("119","9","2019-06-01 08:33:59","2019-06-01 08:36:35","closed");
INSERT INTO open_close_cashout_tb VALUES("120","9","2019-06-01 08:38:18","2019-06-01 18:01:10","closed");
INSERT INTO open_close_cashout_tb VALUES("121","9","2019-06-03 08:25:37","2019-06-03 18:36:43","closed");
INSERT INTO open_close_cashout_tb VALUES("122","9","2019-06-03 18:37:39","2019-06-03 18:40:15","closed");
INSERT INTO open_close_cashout_tb VALUES("123","9","2019-06-03 18:47:25","2019-06-03 18:47:49","closed");
INSERT INTO open_close_cashout_tb VALUES("124","9","2019-06-04 08:19:15","2019-06-04 18:35:13","closed");
INSERT INTO open_close_cashout_tb VALUES("125","9","2019-06-04 18:35:26","2019-06-04 18:35:39","closed");
INSERT INTO open_close_cashout_tb VALUES("126","9","2019-06-05 08:25:14","2019-06-05 17:47:52","closed");
INSERT INTO open_close_cashout_tb VALUES("127","9","2019-06-05 17:48:05","2019-06-05 17:48:17","closed");
INSERT INTO open_close_cashout_tb VALUES("128","9","2019-06-06 08:16:25","2019-06-06 19:16:08","closed");
INSERT INTO open_close_cashout_tb VALUES("129","9","2019-06-06 19:16:22","2019-06-06 19:16:40","closed");
INSERT INTO open_close_cashout_tb VALUES("130","9","2019-06-07 08:17:57","2019-06-07 18:06:53","closed");
INSERT INTO open_close_cashout_tb VALUES("131","9","2019-06-07 20:30:38","2019-06-07 20:30:51","closed");
INSERT INTO open_close_cashout_tb VALUES("132","9","2019-06-08 08:54:39","2019-06-08 18:06:07","closed");
INSERT INTO open_close_cashout_tb VALUES("133","9","2019-06-08 18:06:22","2019-06-08 18:06:47","closed");
INSERT INTO open_close_cashout_tb VALUES("134","9","2019-06-10 08:46:29","2019-06-10 08:47:45","closed");
INSERT INTO open_close_cashout_tb VALUES("135","9","2019-06-10 08:47:58","2019-06-10 19:31:14","closed");
INSERT INTO open_close_cashout_tb VALUES("136","9","2019-06-10 19:31:30","2019-06-10 19:32:02","closed");
INSERT INTO open_close_cashout_tb VALUES("137","9","2019-06-11 08:08:14","2019-06-11 19:52:57","closed");
INSERT INTO open_close_cashout_tb VALUES("138","9","2019-06-11 19:53:20","2019-06-11 19:53:28","closed");
INSERT INTO open_close_cashout_tb VALUES("139","9","2019-06-12 07:43:32","2019-06-12 20:20:08","closed");
INSERT INTO open_close_cashout_tb VALUES("140","9","2019-06-13 08:21:09","2019-06-13 17:40:56","closed");
INSERT INTO open_close_cashout_tb VALUES("141","9","2019-06-13 17:41:08","2019-06-13 17:41:30","closed");
INSERT INTO open_close_cashout_tb VALUES("142","9","2019-06-14 08:13:49","2019-06-14 21:49:14","closed");
INSERT INTO open_close_cashout_tb VALUES("143","9","2019-06-14 21:49:25","2019-06-14 21:49:45","closed");
INSERT INTO open_close_cashout_tb VALUES("144","9","2019-06-15 08:20:02","2019-06-15 19:48:56","closed");
INSERT INTO open_close_cashout_tb VALUES("145","9","2019-06-15 19:49:06","2019-06-15 19:49:09","closed");
INSERT INTO open_close_cashout_tb VALUES("146","9","2019-06-15 19:49:20","2019-06-15 19:49:32","closed");
INSERT INTO open_close_cashout_tb VALUES("147","9","2019-06-15 19:52:47","2019-06-15 19:52:50","closed");
INSERT INTO open_close_cashout_tb VALUES("148","9","2019-06-17 08:15:02","2019-06-17 17:48:27","closed");
INSERT INTO open_close_cashout_tb VALUES("149","9","2019-06-17 17:48:39","2019-06-17 19:53:31","closed");
INSERT INTO open_close_cashout_tb VALUES("150","9","2019-06-18 08:28:55","2019-06-18 19:19:22","closed");
INSERT INTO open_close_cashout_tb VALUES("151","9","2019-06-19 08:13:54","2019-06-19 17:38:37","closed");
INSERT INTO open_close_cashout_tb VALUES("152","9","2019-06-20 08:13:36","2019-06-20 17:56:07","closed");
INSERT INTO open_close_cashout_tb VALUES("153","9","2019-06-20 08:13:36","2019-06-20 17:56:07","closed");
INSERT INTO open_close_cashout_tb VALUES("154","9","2019-06-21 07:03:19","2019-06-21 22:00:08","closed");
INSERT INTO open_close_cashout_tb VALUES("155","9","2019-06-22 08:41:47","2019-06-22 20:06:40","closed");
INSERT INTO open_close_cashout_tb VALUES("156","9","2019-06-24 08:58:21","2019-06-24 17:44:16","closed");
INSERT INTO open_close_cashout_tb VALUES("157","9","2019-06-24 17:45:00","2019-06-24 21:30:42","closed");
INSERT INTO open_close_cashout_tb VALUES("158","9","2019-06-24 22:20:19","","");
INSERT INTO open_close_cashout_tb VALUES("159","8","2019-06-25 15:05:54","2019-06-25 15:07:37","closed");
INSERT INTO open_close_cashout_tb VALUES("160","8","2019-06-25 16:40:23","2019-07-16 16:54:17","closed");
INSERT INTO open_close_cashout_tb VALUES("161","8","2019-07-22 20:00:22","2019-07-23 08:53:35","closed");
INSERT INTO open_close_cashout_tb VALUES("162","8","2019-07-31 13:12:01","2019-07-31 13:19:14","closed");
INSERT INTO open_close_cashout_tb VALUES("163","8","2019-07-31 13:19:21","2019-08-04 11:54:18","closed");
INSERT INTO open_close_cashout_tb VALUES("164","8","2019-08-04 11:54:45","2019-08-06 11:58:16","closed");
INSERT INTO open_close_cashout_tb VALUES("165","8","2019-08-06 12:12:03","2019-08-06 12:14:26","closed");
INSERT INTO open_close_cashout_tb VALUES("166","8","2019-08-06 14:15:34","2019-08-06 14:15:39","closed");
INSERT INTO open_close_cashout_tb VALUES("167","8","2019-08-08 14:58:09","2019-08-08 14:58:23","closed");
INSERT INTO open_close_cashout_tb VALUES("168","8","2019-08-08 15:02:09","2019-08-08 15:03:41","closed");
INSERT INTO open_close_cashout_tb VALUES("169","8","2019-08-08 15:09:59","2019-08-08 15:14:24","closed");
INSERT INTO open_close_cashout_tb VALUES("170","8","2019-08-08 17:19:30","2019-08-10 14:16:00","closed");
INSERT INTO open_close_cashout_tb VALUES("171","8","2019-08-10 14:16:12","2019-08-11 15:53:32","closed");
INSERT INTO open_close_cashout_tb VALUES("172","8","2019-08-11 16:23:27","2019-08-11 17:20:59","closed");
INSERT INTO open_close_cashout_tb VALUES("173","8","2019-08-11 17:21:05","2019-08-12 08:19:53","closed");
INSERT INTO open_close_cashout_tb VALUES("174","8","2019-08-12 08:20:01","2019-08-12 08:21:37","closed");
INSERT INTO open_close_cashout_tb VALUES("175","8","2019-08-12 13:59:07","2019-08-12 17:04:27","closed");
INSERT INTO open_close_cashout_tb VALUES("176","8","2019-08-12 17:04:32","2019-08-12 17:04:34","closed");
INSERT INTO open_close_cashout_tb VALUES("177","8","2019-08-13 08:14:56","2019-08-14 09:52:09","closed");
INSERT INTO open_close_cashout_tb VALUES("178","8","2019-08-14 10:28:35","2019-08-14 11:27:21","closed");
INSERT INTO open_close_cashout_tb VALUES("179","8","2019-08-15 13:57:23","2019-08-16 11:06:11","closed");
INSERT INTO open_close_cashout_tb VALUES("180","8","2019-08-17 15:16:30","2019-09-04 19:46:15","closed");
INSERT INTO open_close_cashout_tb VALUES("181","8","2019-09-04 19:51:04","2019-09-04 19:51:50","closed");
INSERT INTO open_close_cashout_tb VALUES("182","8","2019-09-06 17:08:12","2019-09-07 11:16:15","closed");
INSERT INTO open_close_cashout_tb VALUES("183","8","2019-09-18 08:54:36","2019-09-18 08:56:28","closed");
INSERT INTO open_close_cashout_tb VALUES("184","8","2019-09-18 10:38:23","2019-09-18 10:38:53","closed");
INSERT INTO open_close_cashout_tb VALUES("185","8","2019-09-18 11:01:23","2019-09-18 11:10:49","closed");
INSERT INTO open_close_cashout_tb VALUES("186","8","2019-09-18 11:58:36","2019-09-20 15:17:37","closed");
INSERT INTO open_close_cashout_tb VALUES("187","8","2019-09-28 13:08:04","2019-09-28 13:14:03","closed");
INSERT INTO open_close_cashout_tb VALUES("188","8","2019-10-05 19:25:47","2019-10-05 19:25:57","closed");
INSERT INTO open_close_cashout_tb VALUES("189","8","2019-10-05 19:54:42","2019-10-05 23:37:02","closed");
INSERT INTO open_close_cashout_tb VALUES("190","8","2019-10-05 23:37:07","2019-10-05 23:44:36","closed");
INSERT INTO open_close_cashout_tb VALUES("191","8","2019-10-05 23:44:42","2019-10-05 23:46:28","closed");
INSERT INTO open_close_cashout_tb VALUES("192","8","2019-10-05 23:48:46","2019-10-05 23:49:37","closed");
INSERT INTO open_close_cashout_tb VALUES("193","8","2019-10-05 23:49:46","2019-10-05 23:51:12","closed");
INSERT INTO open_close_cashout_tb VALUES("194","8","2019-10-05 23:51:36","2019-10-07 10:48:26","closed");
INSERT INTO open_close_cashout_tb VALUES("195","8","2019-10-08 08:12:56","2019-10-08 08:44:17","closed");
INSERT INTO open_close_cashout_tb VALUES("196","8","2019-10-08 23:45:50","2019-10-10 14:21:42","closed");
INSERT INTO open_close_cashout_tb VALUES("197","8","2019-10-13 16:03:33","2019-10-13 18:29:16","closed");
INSERT INTO open_close_cashout_tb VALUES("198","8","2019-10-18 08:10:01","2019-10-18 08:10:18","closed");
INSERT INTO open_close_cashout_tb VALUES("199","8","2019-10-18 08:12:36","2019-10-18 08:15:10","closed");
INSERT INTO open_close_cashout_tb VALUES("200","8","2019-10-25 19:20:03","2019-10-25 20:48:14","closed");
INSERT INTO open_close_cashout_tb VALUES("201","8","2019-10-27 20:02:02","2019-10-28 08:09:22","closed");
INSERT INTO open_close_cashout_tb VALUES("202","8","2019-10-28 13:54:12","2019-10-28 13:54:28","closed");
INSERT INTO open_close_cashout_tb VALUES("203","8","2019-10-28 13:55:13","2019-10-28 13:57:27","closed");
INSERT INTO open_close_cashout_tb VALUES("204","8","2019-10-28 14:00:19","2019-10-28 14:15:38","closed");
INSERT INTO open_close_cashout_tb VALUES("205","8","2019-11-03 16:32:48","2019-11-03 19:00:25","closed");
INSERT INTO open_close_cashout_tb VALUES("206","8","2019-11-07 19:04:05","2019-11-07 19:07:17","closed");
INSERT INTO open_close_cashout_tb VALUES("207","8","2019-11-07 19:19:12","2019-11-10 17:48:45","closed");
INSERT INTO open_close_cashout_tb VALUES("208","8","2019-11-11 09:19:58","2019-11-11 09:30:25","closed");
INSERT INTO open_close_cashout_tb VALUES("209","8","2019-11-11 09:31:09","2019-11-11 10:19:25","closed");
INSERT INTO open_close_cashout_tb VALUES("210","8","2019-11-11 16:11:27","2019-11-11 16:25:21","closed");
INSERT INTO open_close_cashout_tb VALUES("211","8","2019-11-11 16:32:44","2019-11-11 16:45:30","closed");
INSERT INTO open_close_cashout_tb VALUES("212","8","2019-11-11 16:46:56","2019-11-12 15:55:24","closed");
INSERT INTO open_close_cashout_tb VALUES("213","8","2019-11-16 15:06:46","2019-11-19 12:07:17","closed");
INSERT INTO open_close_cashout_tb VALUES("214","8","2019-11-19 12:26:49","2019-11-19 12:32:14","closed");
INSERT INTO open_close_cashout_tb VALUES("215","8","2019-11-19 18:02:31","2019-11-21 19:11:22","closed");
INSERT INTO open_close_cashout_tb VALUES("216","8","2019-11-21 19:13:47","2019-11-26 16:09:13","closed");
INSERT INTO open_close_cashout_tb VALUES("217","8","2019-11-27 16:54:49","2019-11-29 22:31:17","closed");
INSERT INTO open_close_cashout_tb VALUES("218","11","2019-11-29 22:32:16","","");
INSERT INTO open_close_cashout_tb VALUES("219","8","2019-11-29 23:17:55","2019-12-12 11:30:23","closed");
INSERT INTO open_close_cashout_tb VALUES("220","8","2019-12-12 17:38:34","2019-12-14 17:13:24","closed");
INSERT INTO open_close_cashout_tb VALUES("221","8","2019-12-14 17:13:57","2019-12-14 17:25:56","closed");
INSERT INTO open_close_cashout_tb VALUES("222","8","2019-12-14 17:26:01","2019-12-14 17:26:33","closed");
INSERT INTO open_close_cashout_tb VALUES("223","8","2019-12-15 16:11:25","2019-12-15 16:11:35","closed");
INSERT INTO open_close_cashout_tb VALUES("224","8","2019-12-15 16:14:24","2019-12-15 21:18:23","closed");
INSERT INTO open_close_cashout_tb VALUES("225","8","2019-12-15 21:20:53","2019-12-15 21:24:58","closed");
INSERT INTO open_close_cashout_tb VALUES("226","8","2019-12-16 15:52:22","2019-12-17 14:36:09","closed");
INSERT INTO open_close_cashout_tb VALUES("227","8","2019-12-17 14:36:32","2019-12-17 14:36:54","closed");
INSERT INTO open_close_cashout_tb VALUES("228","8","2019-12-17 15:45:56","2019-12-17 15:46:23","closed");
INSERT INTO open_close_cashout_tb VALUES("229","8","2019-12-17 15:47:12","2019-12-17 15:53:13","closed");
INSERT INTO open_close_cashout_tb VALUES("230","8","2019-12-17 16:42:26","2019-12-19 09:45:02","closed");
INSERT INTO open_close_cashout_tb VALUES("231","8","2019-12-19 09:45:14","2019-12-19 09:49:05","closed");
INSERT INTO open_close_cashout_tb VALUES("232","8","2019-12-19 10:22:05","2019-12-19 10:22:18","closed");
INSERT INTO open_close_cashout_tb VALUES("233","8","2019-12-19 10:30:14","2019-12-19 10:42:08","closed");
INSERT INTO open_close_cashout_tb VALUES("234","8","2019-12-19 10:50:07","2019-12-19 10:50:38","closed");
INSERT INTO open_close_cashout_tb VALUES("235","8","2019-12-19 14:56:32","2019-12-20 11:53:28","closed");
INSERT INTO open_close_cashout_tb VALUES("236","8","2019-12-21 18:05:30","2019-12-22 16:46:39","closed");
INSERT INTO open_close_cashout_tb VALUES("237","8","2019-12-22 16:47:30","2019-12-22 16:48:06","closed");
INSERT INTO open_close_cashout_tb VALUES("238","8","2019-12-23 10:44:19","2019-12-25 12:06:19","closed");
INSERT INTO open_close_cashout_tb VALUES("239","8","2019-12-28 18:26:00","2019-12-30 18:05:58","closed");
INSERT INTO open_close_cashout_tb VALUES("240","8","2020-01-01 13:04:21","2020-01-01 13:04:35","closed");
INSERT INTO open_close_cashout_tb VALUES("241","8","2020-01-01 13:05:38","2020-01-03 10:20:22","closed");



CREATE TABLE `open_close_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `open_bal` int(12) NOT NULL,
  `close_bal` int(12) NOT NULL,
  `date` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1288 DEFAULT CHARSET=latin1;

INSERT INTO open_close_tb VALUES("138","458","60","53","2019-05-06");
INSERT INTO open_close_tb VALUES("139","320","27","0","2019-05-06");
INSERT INTO open_close_tb VALUES("140","295","143","0","2019-05-06");
INSERT INTO open_close_tb VALUES("141","327","13","0","2019-05-06");
INSERT INTO open_close_tb VALUES("142","319","82","0","2019-05-06");
INSERT INTO open_close_tb VALUES("143","446","48","0","2019-05-06");
INSERT INTO open_close_tb VALUES("144","27","14","0","2019-05-06");
INSERT INTO open_close_tb VALUES("145","98","31","0","2019-05-06");
INSERT INTO open_close_tb VALUES("146","211","2","0","2019-05-06");
INSERT INTO open_close_tb VALUES("147","159","19","0","2019-05-06");
INSERT INTO open_close_tb VALUES("148","445","86","0","2019-05-06");
INSERT INTO open_close_tb VALUES("149","307","3","0","2019-05-06");
INSERT INTO open_close_tb VALUES("150","436","76","0","2019-05-06");
INSERT INTO open_close_tb VALUES("151","209","19","0","2019-05-06");
INSERT INTO open_close_tb VALUES("152","212","3","0","2019-05-06");
INSERT INTO open_close_tb VALUES("153","397","12","0","2019-05-06");
INSERT INTO open_close_tb VALUES("154","129","21","0","2019-05-06");
INSERT INTO open_close_tb VALUES("155","128","118","0","2019-05-06");
INSERT INTO open_close_tb VALUES("156","373","47","0","2019-05-06");
INSERT INTO open_close_tb VALUES("157","5","54","0","2019-05-06");
INSERT INTO open_close_tb VALUES("158","459","1","0","2019-05-06");
INSERT INTO open_close_tb VALUES("159","130","4","0","2019-05-06");
INSERT INTO open_close_tb VALUES("160","294","29","0","2019-05-06");
INSERT INTO open_close_tb VALUES("161","492","103","92","2019-05-07");
INSERT INTO open_close_tb VALUES("162","237","16","14","2019-05-07");
INSERT INTO open_close_tb VALUES("163","440","108","94","2019-05-07");
INSERT INTO open_close_tb VALUES("164","252","28","0","2019-05-07");
INSERT INTO open_close_tb VALUES("165","174","4","0","2019-05-07");
INSERT INTO open_close_tb VALUES("166","160","43","0","2019-05-07");
INSERT INTO open_close_tb VALUES("167","449","76","0","2019-05-07");
INSERT INTO open_close_tb VALUES("168","303","3","0","2019-05-07");
INSERT INTO open_close_tb VALUES("169","228","4","0","2019-05-07");
INSERT INTO open_close_tb VALUES("170","277","320","0","2019-05-07");
INSERT INTO open_close_tb VALUES("171","448","31","0","2019-05-07");
INSERT INTO open_close_tb VALUES("172","115","27","24","2019-05-07");
INSERT INTO open_close_tb VALUES("173","120","59","0","2019-05-07");
INSERT INTO open_close_tb VALUES("174","247","13","11","2019-05-07");
INSERT INTO open_close_tb VALUES("175","467","17","0","2019-05-07");
INSERT INTO open_close_tb VALUES("176","463","8","0","2019-05-07");
INSERT INTO open_close_tb VALUES("177","295","142","139","2019-05-07");
INSERT INTO open_close_tb VALUES("178","327","11","0","2019-05-07");
INSERT INTO open_close_tb VALUES("179","137","6","0","2019-05-07");
INSERT INTO open_close_tb VALUES("180","460","48","43","2019-05-07");
INSERT INTO open_close_tb VALUES("181","128","117","109","2019-05-07");
INSERT INTO open_close_tb VALUES("182","319","72","0","2019-05-07");
INSERT INTO open_close_tb VALUES("183","458","53","48","2019-05-07");
INSERT INTO open_close_tb VALUES("184","372","17","12","2019-05-07");
INSERT INTO open_close_tb VALUES("185","320","20","0","2019-05-07");
INSERT INTO open_close_tb VALUES("186","301","15","12","2019-05-07");
INSERT INTO open_close_tb VALUES("187","373","38","0","2019-05-07");
INSERT INTO open_close_tb VALUES("188","376","27","25","2019-05-07");
INSERT INTO open_close_tb VALUES("189","289","40","0","2019-05-07");
INSERT INTO open_close_tb VALUES("190","322","80","10","2019-05-07");
INSERT INTO open_close_tb VALUES("191","98","28","22","2019-05-07");
INSERT INTO open_close_tb VALUES("192","447","78","0","2019-05-07");
INSERT INTO open_close_tb VALUES("193","436","75","64","2019-05-07");
INSERT INTO open_close_tb VALUES("194","441","670","659","2019-05-07");
INSERT INTO open_close_tb VALUES("195","24","2","0","2019-05-07");
INSERT INTO open_close_tb VALUES("196","35","436","0","2019-05-07");
INSERT INTO open_close_tb VALUES("197","119","15","9","2019-05-07");
INSERT INTO open_close_tb VALUES("198","22","800","0","2019-05-07");
INSERT INTO open_close_tb VALUES("199","180","265","262","2019-05-07");
INSERT INTO open_close_tb VALUES("200","47","42","0","2019-05-07");
INSERT INTO open_close_tb VALUES("201","45","72","0","2019-05-07");
INSERT INTO open_close_tb VALUES("202","20","398","0","2019-05-07");
INSERT INTO open_close_tb VALUES("203","350","48","0","2019-05-07");
INSERT INTO open_close_tb VALUES("204","27","13","0","2019-05-07");
INSERT INTO open_close_tb VALUES("205","361","7","0","2019-05-07");
INSERT INTO open_close_tb VALUES("206","112","9","0","2019-05-07");
INSERT INTO open_close_tb VALUES("207","342","3","0","2019-05-07");
INSERT INTO open_close_tb VALUES("208","68","4","0","2019-05-07");
INSERT INTO open_close_tb VALUES("209","15","7","0","2019-05-07");
INSERT INTO open_close_tb VALUES("210","402","148","0","2019-05-07");
INSERT INTO open_close_tb VALUES("211","294","28","0","2019-05-07");
INSERT INTO open_close_tb VALUES("212","129","19","0","2019-05-07");
INSERT INTO open_close_tb VALUES("213","305","48","0","2019-05-07");
INSERT INTO open_close_tb VALUES("214","321","132","0","2019-05-07");
INSERT INTO open_close_tb VALUES("215","299","12","0","2019-05-07");
INSERT INTO open_close_tb VALUES("216","96","13","0","2019-05-07");
INSERT INTO open_close_tb VALUES("217","426","100","0","2019-05-07");
INSERT INTO open_close_tb VALUES("218","253","6","0","2019-05-07");
INSERT INTO open_close_tb VALUES("219","362","10","0","2019-05-07");
INSERT INTO open_close_tb VALUES("220","265","15","0","2019-05-07");
INSERT INTO open_close_tb VALUES("221","384","15","0","2019-05-07");
INSERT INTO open_close_tb VALUES("222","446","46","0","2019-05-07");
INSERT INTO open_close_tb VALUES("223","209","18","0","2019-05-07");
INSERT INTO open_close_tb VALUES("224","283","43","0","2019-05-07");
INSERT INTO open_close_tb VALUES("225","469","38","0","2019-05-07");
INSERT INTO open_close_tb VALUES("226","452","21","0","2019-05-07");
INSERT INTO open_close_tb VALUES("227","148","187","0","2019-05-07");
INSERT INTO open_close_tb VALUES("228","116","33","0","2019-05-07");
INSERT INTO open_close_tb VALUES("229","19","1","0","2019-05-07");
INSERT INTO open_close_tb VALUES("230","65","601","541","2019-05-08");
INSERT INTO open_close_tb VALUES("231","295","139","137","2019-05-08");
INSERT INTO open_close_tb VALUES("232","128","109","100","2019-05-08");
INSERT INTO open_close_tb VALUES("233","460","43","39","2019-05-08");
INSERT INTO open_close_tb VALUES("234","458","48","17","2019-05-08");
INSERT INTO open_close_tb VALUES("235","293","22","0","2019-05-08");
INSERT INTO open_close_tb VALUES("236","96","12","0","2019-05-08");
INSERT INTO open_close_tb VALUES("237","277","312","0","2019-05-08");
INSERT INTO open_close_tb VALUES("238","170","4","0","2019-05-08");
INSERT INTO open_close_tb VALUES("239","400","10","0","2019-05-08");
INSERT INTO open_close_tb VALUES("240","254","35","0","2019-05-08");
INSERT INTO open_close_tb VALUES("241","248","35","0","2019-05-08");
INSERT INTO open_close_tb VALUES("242","247","11","0","2019-05-08");
INSERT INTO open_close_tb VALUES("243","246","27","0","2019-05-08");
INSERT INTO open_close_tb VALUES("244","133","24","0","2019-05-08");
INSERT INTO open_close_tb VALUES("245","475","21","0","2019-05-08");
INSERT INTO open_close_tb VALUES("246","129","14","0","2019-05-08");
INSERT INTO open_close_tb VALUES("247","373","36","0","2019-05-08");
INSERT INTO open_close_tb VALUES("248","436","64","0","2019-05-08");
INSERT INTO open_close_tb VALUES("249","440","94","0","2019-05-08");
INSERT INTO open_close_tb VALUES("250","61","79","0","2019-05-08");
INSERT INTO open_close_tb VALUES("251","75","8","0","2019-05-08");
INSERT INTO open_close_tb VALUES("252","256","29","0","2019-05-08");
INSERT INTO open_close_tb VALUES("253","12","1","0","2019-05-08");
INSERT INTO open_close_tb VALUES("254","492","92","0","2019-05-08");
INSERT INTO open_close_tb VALUES("255","148","186","0","2019-05-08");
INSERT INTO open_close_tb VALUES("256","209","17","0","2019-05-08");
INSERT INTO open_close_tb VALUES("257","88","13","0","2019-05-08");
INSERT INTO open_close_tb VALUES("258","390","23","0","2019-05-08");
INSERT INTO open_close_tb VALUES("259","426","99","0","2019-05-08");
INSERT INTO open_close_tb VALUES("260","428","9","0","2019-05-08");
INSERT INTO open_close_tb VALUES("261","98","22","0","2019-05-08");
INSERT INTO open_close_tb VALUES("262","455","7","0","2019-05-08");
INSERT INTO open_close_tb VALUES("263","252","27","0","2019-05-08");
INSERT INTO open_close_tb VALUES("264","68","3","0","2019-05-08");
INSERT INTO open_close_tb VALUES("265","276","338","0","2019-05-09");
INSERT INTO open_close_tb VALUES("266","303","2","0","2019-05-09");
INSERT INTO open_close_tb VALUES("267","80","5","0","2019-05-09");
INSERT INTO open_close_tb VALUES("268","267","12","0","2019-05-09");
INSERT INTO open_close_tb VALUES("269","400","9","0","2019-05-09");
INSERT INTO open_close_tb VALUES("270","205","44","0","2019-05-09");
INSERT INTO open_close_tb VALUES("271","65","541","500","2019-05-09");
INSERT INTO open_close_tb VALUES("272","327","7","1","2019-05-09");
INSERT INTO open_close_tb VALUES("273","128","100","0","2019-05-09");
INSERT INTO open_close_tb VALUES("274","295","137","0","2019-05-09");
INSERT INTO open_close_tb VALUES("275","458","17","14","2019-05-09");
INSERT INTO open_close_tb VALUES("276","436","54","0","2019-05-09");
INSERT INTO open_close_tb VALUES("277","440","84","0","2019-05-09");
INSERT INTO open_close_tb VALUES("278","492","91","0","2019-05-09");
INSERT INTO open_close_tb VALUES("279","441","659","0","2019-05-09");
INSERT INTO open_close_tb VALUES("280","254","34","0","2019-05-09");
INSERT INTO open_close_tb VALUES("281","253","5","0","2019-05-09");
INSERT INTO open_close_tb VALUES("282","460","39","28","2019-05-09");
INSERT INTO open_close_tb VALUES("283","368","12","0","2019-05-09");
INSERT INTO open_close_tb VALUES("284","370","16","0","2019-05-09");
INSERT INTO open_close_tb VALUES("285","467","16","0","2019-05-09");
INSERT INTO open_close_tb VALUES("286","185","25","0","2019-05-09");
INSERT INTO open_close_tb VALUES("287","458","14","0","2019-05-10");
INSERT INTO open_close_tb VALUES("288","289","39","0","2019-05-10");
INSERT INTO open_close_tb VALUES("289","440","78","0","2019-05-10");
INSERT INTO open_close_tb VALUES("290","436","48","0","2019-05-10");
INSERT INTO open_close_tb VALUES("291","441","653","0","2019-05-10");
INSERT INTO open_close_tb VALUES("292","65","500","484","2019-05-10");
INSERT INTO open_close_tb VALUES("293","444","8","0","2019-05-10");
INSERT INTO open_close_tb VALUES("294","208","10","0","2019-05-10");
INSERT INTO open_close_tb VALUES("295","305","43","0","2019-05-10");
INSERT INTO open_close_tb VALUES("296","452","20","0","2019-05-10");
INSERT INTO open_close_tb VALUES("297","35","431","0","2019-05-10");
INSERT INTO open_close_tb VALUES("298","24","1","0","2019-05-10");
INSERT INTO open_close_tb VALUES("299","248","34","0","2019-05-10");
INSERT INTO open_close_tb VALUES("300","22","788","0","2019-05-10");
INSERT INTO open_close_tb VALUES("301","460","28","0","2019-05-10");
INSERT INTO open_close_tb VALUES("302","277","292","0","2019-05-10");
INSERT INTO open_close_tb VALUES("303","475","20","0","2019-05-10");
INSERT INTO open_close_tb VALUES("304","367","6","0","2019-05-10");
INSERT INTO open_close_tb VALUES("305","88","12","0","2019-05-11");
INSERT INTO open_close_tb VALUES("306","65","484","455","2019-05-11");
INSERT INTO open_close_tb VALUES("307","106","555","0","2019-05-11");
INSERT INTO open_close_tb VALUES("308","492","90","0","2019-05-11");
INSERT INTO open_close_tb VALUES("309","450","85","0","2019-05-11");
INSERT INTO open_close_tb VALUES("310","460","27","23","2019-05-11");
INSERT INTO open_close_tb VALUES("311","22","778","0","2019-05-11");
INSERT INTO open_close_tb VALUES("312","444","7","0","2019-05-11");
INSERT INTO open_close_tb VALUES("313","251","41","0","2019-05-11");
INSERT INTO open_close_tb VALUES("314","228","3","0","2019-05-11");
INSERT INTO open_close_tb VALUES("315","313","29","0","2019-05-11");
INSERT INTO open_close_tb VALUES("316","243","8","0","2019-05-11");
INSERT INTO open_close_tb VALUES("317","178","13","0","2019-05-11");
INSERT INTO open_close_tb VALUES("318","283","42","0","2019-05-11");
INSERT INTO open_close_tb VALUES("319","284","50","0","2019-05-11");
INSERT INTO open_close_tb VALUES("320","4","29","0","2019-05-11");
INSERT INTO open_close_tb VALUES("321","375","57","0","2019-05-11");
INSERT INTO open_close_tb VALUES("322","70","1","0","2019-05-11");
INSERT INTO open_close_tb VALUES("323","305","41","0","2019-05-11");
INSERT INTO open_close_tb VALUES("324","237","14","0","2019-05-11");
INSERT INTO open_close_tb VALUES("325","131","11","0","2019-05-11");
INSERT INTO open_close_tb VALUES("326","322","10","0","2019-05-11");
INSERT INTO open_close_tb VALUES("327","15","6","0","2019-05-11");
INSERT INTO open_close_tb VALUES("328","225","7","0","2019-05-11");
INSERT INTO open_close_tb VALUES("329","442","616","0","2019-05-11");
INSERT INTO open_close_tb VALUES("330","127","17","0","2019-05-11");
INSERT INTO open_close_tb VALUES("331","370","15","0","2019-05-13");
INSERT INTO open_close_tb VALUES("332","250","38","0","2019-05-13");
INSERT INTO open_close_tb VALUES("333","148","184","177","2019-05-13");
INSERT INTO open_close_tb VALUES("334","20","397","0","2019-05-13");
INSERT INTO open_close_tb VALUES("335","147","315","300","2019-05-13");
INSERT INTO open_close_tb VALUES("336","65","455","0","2019-05-13");
INSERT INTO open_close_tb VALUES("337","129","11","0","2019-05-13");
INSERT INTO open_close_tb VALUES("338","128","93","89","2019-05-13");
INSERT INTO open_close_tb VALUES("339","299","10","0","2019-05-13");
INSERT INTO open_close_tb VALUES("340","364","2","0","2019-05-13");
INSERT INTO open_close_tb VALUES("341","436","44","0","2019-05-13");
INSERT INTO open_close_tb VALUES("342","441","649","0","2019-05-13");
INSERT INTO open_close_tb VALUES("343","251","39","0","2019-05-13");
INSERT INTO open_close_tb VALUES("344","200","3","0","2019-05-13");
INSERT INTO open_close_tb VALUES("345","192","6","0","2019-05-13");
INSERT INTO open_close_tb VALUES("346","391","15","0","2019-05-13");
INSERT INTO open_close_tb VALUES("347","63","13","0","2019-05-13");
INSERT INTO open_close_tb VALUES("348","236","68","0","2019-05-13");
INSERT INTO open_close_tb VALUES("349","45","67","0","2019-05-14");
INSERT INTO open_close_tb VALUES("350","47","37","0","2019-05-14");
INSERT INTO open_close_tb VALUES("351","20","387","0","2019-05-14");
INSERT INTO open_close_tb VALUES("352","35","424","0","2019-05-14");
INSERT INTO open_close_tb VALUES("353","193","172","0","2019-05-14");
INSERT INTO open_close_tb VALUES("354","128","89","0","2019-05-14");
INSERT INTO open_close_tb VALUES("355","1","1","0","2019-05-14");
INSERT INTO open_close_tb VALUES("356","83","11","0","2019-05-14");
INSERT INTO open_close_tb VALUES("357","205","42","0","2019-05-14");
INSERT INTO open_close_tb VALUES("358","204","61","0","2019-05-14");
INSERT INTO open_close_tb VALUES("359","475","19","0","2019-05-14");
INSERT INTO open_close_tb VALUES("360","252","26","0","2019-05-14");
INSERT INTO open_close_tb VALUES("361","283","39","0","2019-05-14");
INSERT INTO open_close_tb VALUES("362","446","45","0","2019-05-14");
INSERT INTO open_close_tb VALUES("363","492","89","0","2019-05-14");
INSERT INTO open_close_tb VALUES("364","434","5","0","2019-05-14");
INSERT INTO open_close_tb VALUES("365","186","2","0","2019-05-14");
INSERT INTO open_close_tb VALUES("366","428","7","0","2019-05-15");
INSERT INTO open_close_tb VALUES("367","125","29","27","2019-05-15");
INSERT INTO open_close_tb VALUES("368","130","2","0","2019-05-15");
INSERT INTO open_close_tb VALUES("369","128","85","83","2019-05-15");
INSERT INTO open_close_tb VALUES("370","65","440","424","2019-05-15");
INSERT INTO open_close_tb VALUES("371","436","37","0","2019-05-15");
INSERT INTO open_close_tb VALUES("372","440","74","70","2019-05-15");
INSERT INTO open_close_tb VALUES("373","98","18","0","2019-05-15");
INSERT INTO open_close_tb VALUES("374","265","14","0","2019-05-15");
INSERT INTO open_close_tb VALUES("375","156","2","0","2019-05-15");
INSERT INTO open_close_tb VALUES("376","160","42","0","2019-05-15");
INSERT INTO open_close_tb VALUES("377","452","19","0","2019-05-15");
INSERT INTO open_close_tb VALUES("378","106","553","550","2019-05-15");
INSERT INTO open_close_tb VALUES("379","461","5","0","2019-05-15");
INSERT INTO open_close_tb VALUES("380","402","103","0","2019-05-15");
INSERT INTO open_close_tb VALUES("381","390","21","0","2019-05-15");
INSERT INTO open_close_tb VALUES("382","35","429","0","2019-05-15");
INSERT INTO open_close_tb VALUES("383","88","11","0","2019-05-15");
INSERT INTO open_close_tb VALUES("384","310","14","0","2019-05-16");
INSERT INTO open_close_tb VALUES("385","176","2","0","2019-05-16");
INSERT INTO open_close_tb VALUES("386","163","14","0","2019-05-16");
INSERT INTO open_close_tb VALUES("387","305","37","0","2019-05-16");
INSERT INTO open_close_tb VALUES("388","295","136","0","2019-05-16");
INSERT INTO open_close_tb VALUES("389","289","33","0","2019-05-16");
INSERT INTO open_close_tb VALUES("390","299","9","7","2019-05-16");
INSERT INTO open_close_tb VALUES("391","106","550","0","2019-05-16");
INSERT INTO open_close_tb VALUES("392","452","18","0","2019-05-16");
INSERT INTO open_close_tb VALUES("393","22","768","0","2019-05-16");
INSERT INTO open_close_tb VALUES("394","251","38","35","2019-05-16");
INSERT INTO open_close_tb VALUES("395","128","83","79","2019-05-17");
INSERT INTO open_close_tb VALUES("396","65","424","0","2019-05-17");
INSERT INTO open_close_tb VALUES("397","133","19","0","2019-05-17");
INSERT INTO open_close_tb VALUES("398","207","9","0","2019-05-17");
INSERT INTO open_close_tb VALUES("399","148","177","0","2019-05-17");
INSERT INTO open_close_tb VALUES("400","147","300","0","2019-05-17");
INSERT INTO open_close_tb VALUES("401","426","98","78","2019-05-17");
INSERT INTO open_close_tb VALUES("402","446","44","38","2019-05-17");
INSERT INTO open_close_tb VALUES("403","447","77","71","2019-05-17");
INSERT INTO open_close_tb VALUES("404","257","54","48","2019-05-17");
INSERT INTO open_close_tb VALUES("405","30","50","34","2019-05-17");
INSERT INTO open_close_tb VALUES("406","436","34","0","2019-05-17");
INSERT INTO open_close_tb VALUES("407","441","642","0","2019-05-17");
INSERT INTO open_close_tb VALUES("408","460","75","73","2019-05-17");
INSERT INTO open_close_tb VALUES("409","204","60","0","2019-05-17");
INSERT INTO open_close_tb VALUES("410","0","0","0","2019-05-17");
INSERT INTO open_close_tb VALUES("411","402","101","0","2019-05-17");
INSERT INTO open_close_tb VALUES("412","294","27","0","2019-05-17");
INSERT INTO open_close_tb VALUES("413","320","150","0","2019-05-17");
INSERT INTO open_close_tb VALUES("414","2","100","0","2019-05-17");
INSERT INTO open_close_tb VALUES("415","458","94","0","2019-05-17");
INSERT INTO open_close_tb VALUES("416","214","10","0","2019-05-17");
INSERT INTO open_close_tb VALUES("417","118","2","0","2019-05-17");
INSERT INTO open_close_tb VALUES("418","116","32","0","2019-05-17");
INSERT INTO open_close_tb VALUES("419","459","182","0","2019-05-17");
INSERT INTO open_close_tb VALUES("420","283","38","0","2019-05-17");
INSERT INTO open_close_tb VALUES("421","303","11","0","2019-05-18");
INSERT INTO open_close_tb VALUES("422","376","25","0","2019-05-18");
INSERT INTO open_close_tb VALUES("423","98","50","0","2019-05-18");
INSERT INTO open_close_tb VALUES("424","377","16","11","2019-05-18");
INSERT INTO open_close_tb VALUES("425","276","327","0","2019-05-18");
INSERT INTO open_close_tb VALUES("426","21","500","490","2019-05-18");
INSERT INTO open_close_tb VALUES("427","446","38","0","2019-05-18");
INSERT INTO open_close_tb VALUES("428","463","7","0","2019-05-18");
INSERT INTO open_close_tb VALUES("429","375","51","0","2019-05-18");
INSERT INTO open_close_tb VALUES("430","131","10","0","2019-05-18");
INSERT INTO open_close_tb VALUES("431","213","27","0","2019-05-18");
INSERT INTO open_close_tb VALUES("432","283","37","0","2019-05-18");
INSERT INTO open_close_tb VALUES("433","374","120","0","2019-05-18");
INSERT INTO open_close_tb VALUES("434","444","6","0","2019-05-18");
INSERT INTO open_close_tb VALUES("435","30","34","0","2019-05-18");
INSERT INTO open_close_tb VALUES("436","65","409","400","2019-05-18");
INSERT INTO open_close_tb VALUES("437","203","4","0","2019-05-18");
INSERT INTO open_close_tb VALUES("438","460","73","68","2019-05-18");
INSERT INTO open_close_tb VALUES("439","180","262","0","2019-05-18");
INSERT INTO open_close_tb VALUES("440","96","10","0","2019-05-18");
INSERT INTO open_close_tb VALUES("441","466","11","0","2019-05-18");
INSERT INTO open_close_tb VALUES("442","122","26","0","2019-05-18");
INSERT INTO open_close_tb VALUES("443","475","18","0","2019-05-18");
INSERT INTO open_close_tb VALUES("444","128","79","75","2019-05-20");
INSERT INTO open_close_tb VALUES("445","30","32","0","2019-05-20");
INSERT INTO open_close_tb VALUES("446","295","135","0","2019-05-20");
INSERT INTO open_close_tb VALUES("447","458","93","90","2019-05-20");
INSERT INTO open_close_tb VALUES("448","180","261","258","2019-05-20");
INSERT INTO open_close_tb VALUES("449","34","21","0","2019-05-20");
INSERT INTO open_close_tb VALUES("450","249","28","0","2019-05-20");
INSERT INTO open_close_tb VALUES("451","65","400","394","2019-05-20");
INSERT INTO open_close_tb VALUES("452","98","49","0","2019-05-20");
INSERT INTO open_close_tb VALUES("453","283","36","0","2019-05-20");
INSERT INTO open_close_tb VALUES("454","319","150","0","2019-05-20");
INSERT INTO open_close_tb VALUES("455","123","24","0","2019-05-20");
INSERT INTO open_close_tb VALUES("456","4","22","0","2019-05-20");
INSERT INTO open_close_tb VALUES("457","445","85","0","2019-05-20");
INSERT INTO open_close_tb VALUES("458","134","4","0","2019-05-20");
INSERT INTO open_close_tb VALUES("459","402","97","0","2019-05-20");
INSERT INTO open_close_tb VALUES("460","35","428","0","2019-05-20");
INSERT INTO open_close_tb VALUES("461","148","175","0","2019-05-20");
INSERT INTO open_close_tb VALUES("462","30","31","0","2019-05-21");
INSERT INTO open_close_tb VALUES("463","128","75","66","2019-05-21");
INSERT INTO open_close_tb VALUES("464","65","394","0","2019-05-21");
INSERT INTO open_close_tb VALUES("465","123","23","0","2019-05-21");
INSERT INTO open_close_tb VALUES("466","403","42","0","2019-05-21");
INSERT INTO open_close_tb VALUES("467","450","84","0","2019-05-21");
INSERT INTO open_close_tb VALUES("468","13","23","0","2019-05-21");
INSERT INTO open_close_tb VALUES("469","449","75","0","2019-05-21");
INSERT INTO open_close_tb VALUES("470","140","5","0","2019-05-21");
INSERT INTO open_close_tb VALUES("471","5","53","0","2019-05-21");
INSERT INTO open_close_tb VALUES("472","32","7","0","2019-05-21");
INSERT INTO open_close_tb VALUES("473","320","136","131","2019-05-21");
INSERT INTO open_close_tb VALUES("474","213","24","0","2019-05-21");
INSERT INTO open_close_tb VALUES("475","475","17","0","2019-05-21");
INSERT INTO open_close_tb VALUES("476","129","136","0","2019-05-21");
INSERT INTO open_close_tb VALUES("477","299","7","0","2019-05-21");
INSERT INTO open_close_tb VALUES("478","98","48","0","2019-05-21");
INSERT INTO open_close_tb VALUES("479","35","427","0","2019-05-21");
INSERT INTO open_close_tb VALUES("480","180","258","0","2019-05-21");
INSERT INTO open_close_tb VALUES("481","467","15","0","2019-05-21");
INSERT INTO open_close_tb VALUES("482","75","7","0","2019-05-22");
INSERT INTO open_close_tb VALUES("483","302","10","0","2019-05-22");
INSERT INTO open_close_tb VALUES("484","128","66","0","2019-05-22");
INSERT INTO open_close_tb VALUES("485","129","124","0","2019-05-22");
INSERT INTO open_close_tb VALUES("486","440","71","0","2019-05-22");
INSERT INTO open_close_tb VALUES("487","441","640","0","2019-05-22");
INSERT INTO open_close_tb VALUES("488","436","32","0","2019-05-22");
INSERT INTO open_close_tb VALUES("489","185","24","0","2019-05-22");
INSERT INTO open_close_tb VALUES("490","374","100","0","2019-05-22");
INSERT INTO open_close_tb VALUES("491","445","81","0","2019-05-22");
INSERT INTO open_close_tb VALUES("492","295","134","132","2019-05-22");
INSERT INTO open_close_tb VALUES("493","319","145","0","2019-05-22");
INSERT INTO open_close_tb VALUES("494","442","615","0","2019-05-22");
INSERT INTO open_close_tb VALUES("495","132","10","0","2019-05-22");
INSERT INTO open_close_tb VALUES("496","217","8","0","2019-05-22");
INSERT INTO open_close_tb VALUES("497","106","548","0","2019-05-22");
INSERT INTO open_close_tb VALUES("498","262","17","0","2019-05-22");
INSERT INTO open_close_tb VALUES("499","320","131","72","2019-05-22");
INSERT INTO open_close_tb VALUES("500","317","53","0","2019-05-22");
INSERT INTO open_close_tb VALUES("501","448","29","0","2019-05-22");
INSERT INTO open_close_tb VALUES("502","422","60","0","2019-05-22");
INSERT INTO open_close_tb VALUES("503","305","66","0","2019-05-22");
INSERT INTO open_close_tb VALUES("504","255","25","0","2019-05-22");
INSERT INTO open_close_tb VALUES("505","6","20","0","2019-05-22");
INSERT INTO open_close_tb VALUES("506","65","389","382","2019-05-22");
INSERT INTO open_close_tb VALUES("507","2","90","0","2019-05-22");
INSERT INTO open_close_tb VALUES("508","63","12","0","2019-05-22");
INSERT INTO open_close_tb VALUES("509","116","31","0","2019-05-23");
INSERT INTO open_close_tb VALUES("510","249","27","0","2019-05-23");
INSERT INTO open_close_tb VALUES("511","216","20","0","2019-05-23");
INSERT INTO open_close_tb VALUES("512","83","10","0","2019-05-23");
INSERT INTO open_close_tb VALUES("513","305","61","0","2019-05-23");
INSERT INTO open_close_tb VALUES("514","128","64","60","2019-05-23");
INSERT INTO open_close_tb VALUES("515","98","47","0","2019-05-23");
INSERT INTO open_close_tb VALUES("516","436","20","0","2019-05-23");
INSERT INTO open_close_tb VALUES("517","441","628","0","2019-05-23");
INSERT INTO open_close_tb VALUES("518","440","59","0","2019-05-23");
INSERT INTO open_close_tb VALUES("519","301","12","11","2019-05-23");
INSERT INTO open_close_tb VALUES("520","377","11","9","2019-05-23");
INSERT INTO open_close_tb VALUES("521","302","9","0","2019-05-23");
INSERT INTO open_close_tb VALUES("522","106","546","0","2019-05-23");
INSERT INTO open_close_tb VALUES("523","30","21","13","2019-05-23");
INSERT INTO open_close_tb VALUES("524","130","4","0","2019-05-23");
INSERT INTO open_close_tb VALUES("525","327","25","0","2019-05-23");
INSERT INTO open_close_tb VALUES("526","126","28","0","2019-05-23");
INSERT INTO open_close_tb VALUES("527","373","54","0","2019-05-23");
INSERT INTO open_close_tb VALUES("528","61","78","0","2019-05-23");
INSERT INTO open_close_tb VALUES("529","492","88","0","2019-05-23");
INSERT INTO open_close_tb VALUES("530","290","15","0","2019-05-23");
INSERT INTO open_close_tb VALUES("531","449","74","0","2019-05-23");
INSERT INTO open_close_tb VALUES("532","317","48","0","2019-05-23");
INSERT INTO open_close_tb VALUES("533","155","1","0","2019-05-23");
INSERT INTO open_close_tb VALUES("534","185","22","0","2019-05-23");
INSERT INTO open_close_tb VALUES("535","445","80","0","2019-05-23");
INSERT INTO open_close_tb VALUES("536","460","68","0","2019-05-23");
INSERT INTO open_close_tb VALUES("537","98","46","0","2019-05-24");
INSERT INTO open_close_tb VALUES("538","461","2","0","2019-05-24");
INSERT INTO open_close_tb VALUES("539","208","9","0","2019-05-24");
INSERT INTO open_close_tb VALUES("540","80","4","0","2019-05-24");
INSERT INTO open_close_tb VALUES("541","446","37","0","2019-05-24");
INSERT INTO open_close_tb VALUES("542","128","59","57","2019-05-24");
INSERT INTO open_close_tb VALUES("543","301","10","0","2019-05-24");
INSERT INTO open_close_tb VALUES("544","373","52","0","2019-05-24");
INSERT INTO open_close_tb VALUES("545","133","17","0","2019-05-24");
INSERT INTO open_close_tb VALUES("546","322","103","0","2019-05-24");
INSERT INTO open_close_tb VALUES("547","317","46","45","2019-05-24");
INSERT INTO open_close_tb VALUES("548","147","295","0","2019-05-24");
INSERT INTO open_close_tb VALUES("549","148","174","0","2019-05-24");
INSERT INTO open_close_tb VALUES("550","193","170","0","2019-05-24");
INSERT INTO open_close_tb VALUES("551","469","36","0","2019-05-24");
INSERT INTO open_close_tb VALUES("552","188","67","0","2019-05-24");
INSERT INTO open_close_tb VALUES("553","112","8","0","2019-05-24");
INSERT INTO open_close_tb VALUES("554","27","12","0","2019-05-24");
INSERT INTO open_close_tb VALUES("555","15","4","3","2019-05-24");
INSERT INTO open_close_tb VALUES("556","350","45","0","2019-05-24");
INSERT INTO open_close_tb VALUES("557","134","3","2","2019-05-24");
INSERT INTO open_close_tb VALUES("558","65","382","379","2019-05-24");
INSERT INTO open_close_tb VALUES("559","372","22","0","2019-05-24");
INSERT INTO open_close_tb VALUES("560","20","386","0","2019-05-24");
INSERT INTO open_close_tb VALUES("561","63","11","0","2019-05-24");
INSERT INTO open_close_tb VALUES("562","442","614","0","2019-05-24");
INSERT INTO open_close_tb VALUES("563","236","58","0","2019-05-24");
INSERT INTO open_close_tb VALUES("564","87","13","0","2019-05-24");
INSERT INTO open_close_tb VALUES("565","327","23","0","2019-05-24");
INSERT INTO open_close_tb VALUES("566","132","8","0","2019-05-24");
INSERT INTO open_close_tb VALUES("567","65","378","336","2019-05-25");
INSERT INTO open_close_tb VALUES("568","468","11","0","2019-05-25");
INSERT INTO open_close_tb VALUES("569","422","56","0","2019-05-25");
INSERT INTO open_close_tb VALUES("570","374","100","93","2019-05-25");
INSERT INTO open_close_tb VALUES("571","160","41","40","2019-05-25");
INSERT INTO open_close_tb VALUES("572","317","44","37","2019-05-25");
INSERT INTO open_close_tb VALUES("573","305","60","0","2019-05-25");
INSERT INTO open_close_tb VALUES("574","283","34","0","2019-05-25");
INSERT INTO open_close_tb VALUES("575","20","361","0","2019-05-25");
INSERT INTO open_close_tb VALUES("576","209","16","14","2019-05-25");
INSERT INTO open_close_tb VALUES("577","212","25","0","2019-05-25");
INSERT INTO open_close_tb VALUES("578","132","6","0","2019-05-25");
INSERT INTO open_close_tb VALUES("579","123","22","21","2019-05-25");
INSERT INTO open_close_tb VALUES("580","459","181","180","2019-05-25");
INSERT INTO open_close_tb VALUES("581","328","11","0","2019-05-25");
INSERT INTO open_close_tb VALUES("582","322","99","0","2019-05-25");
INSERT INTO open_close_tb VALUES("583","445","79","0","2019-05-25");
INSERT INTO open_close_tb VALUES("584","372","20","19","2019-05-25");
INSERT INTO open_close_tb VALUES("585","5","52","49","2019-05-25");
INSERT INTO open_close_tb VALUES("586","136","10","0","2019-05-25");
INSERT INTO open_close_tb VALUES("587","98","45","44","2019-05-25");
INSERT INTO open_close_tb VALUES("588","370","14","0","2019-05-25");
INSERT INTO open_close_tb VALUES("589","125","27","0","2019-05-25");
INSERT INTO open_close_tb VALUES("590","460","67","0","2019-05-25");
INSERT INTO open_close_tb VALUES("591","13","22","0","2019-05-25");
INSERT INTO open_close_tb VALUES("592","376","22","0","2019-05-25");
INSERT INTO open_close_tb VALUES("593","362","9","0","2019-05-27");
INSERT INTO open_close_tb VALUES("594","160","39","0","2019-05-27");
INSERT INTO open_close_tb VALUES("595","319","127","86","2019-05-27");
INSERT INTO open_close_tb VALUES("596","458","90","0","2019-05-27");
INSERT INTO open_close_tb VALUES("597","65","330","321","2019-05-27");
INSERT INTO open_close_tb VALUES("598","322","96","0","2019-05-27");
INSERT INTO open_close_tb VALUES("599","293","6","5","2019-05-27");
INSERT INTO open_close_tb VALUES("600","301","9","0","2019-05-27");
INSERT INTO open_close_tb VALUES("601","128","55","50","2019-05-27");
INSERT INTO open_close_tb VALUES("602","372","14","8","2019-05-27");
INSERT INTO open_close_tb VALUES("603","35","425","0","2019-05-27");
INSERT INTO open_close_tb VALUES("604","98","43","42","2019-05-27");
INSERT INTO open_close_tb VALUES("605","440","57","0","2019-05-27");
INSERT INTO open_close_tb VALUES("606","436","18","0","2019-05-27");
INSERT INTO open_close_tb VALUES("607","447","71","0","2019-05-27");
INSERT INTO open_close_tb VALUES("608","446","36","0","2019-05-27");
INSERT INTO open_close_tb VALUES("609","212","23","0","2019-05-27");
INSERT INTO open_close_tb VALUES("610","459","176","0","2019-05-27");
INSERT INTO open_close_tb VALUES("611","133","16","0","2019-05-27");
INSERT INTO open_close_tb VALUES("612","251","35","0","2019-05-27");
INSERT INTO open_close_tb VALUES("613","368","11","0","2019-05-27");
INSERT INTO open_close_tb VALUES("614","276","247","0","2019-05-27");
INSERT INTO open_close_tb VALUES("615","300","2","0","2019-05-27");
INSERT INTO open_close_tb VALUES("616","61","76","74","2019-05-27");
INSERT INTO open_close_tb VALUES("617","428","6","0","2019-05-27");
INSERT INTO open_close_tb VALUES("618","446","35","33","2019-05-28");
INSERT INTO open_close_tb VALUES("619","428","5","0","2019-05-28");
INSERT INTO open_close_tb VALUES("620","460","66","0","2019-05-28");
INSERT INTO open_close_tb VALUES("621","461","1","0","2019-05-28");
INSERT INTO open_close_tb VALUES("622","319","76","0","2019-05-28");
INSERT INTO open_close_tb VALUES("623","322","89","0","2019-05-28");
INSERT INTO open_close_tb VALUES("624","422","55","0","2019-05-28");
INSERT INTO open_close_tb VALUES("625","65","320","318","2019-05-28");
INSERT INTO open_close_tb VALUES("626","251","34","0","2019-05-28");
INSERT INTO open_close_tb VALUES("627","368","10","0","2019-05-28");
INSERT INTO open_close_tb VALUES("628","128","49","0","2019-05-28");
INSERT INTO open_close_tb VALUES("629","468","10","9","2019-05-28");
INSERT INTO open_close_tb VALUES("630","123","20","0","2019-05-28");
INSERT INTO open_close_tb VALUES("631","445","78","77","2019-05-28");
INSERT INTO open_close_tb VALUES("632","293","4","0","2019-05-28");
INSERT INTO open_close_tb VALUES("633","374","92","0","2019-05-28");
INSERT INTO open_close_tb VALUES("634","130","3","0","2019-05-28");
INSERT INTO open_close_tb VALUES("635","157","22","0","2019-05-28");
INSERT INTO open_close_tb VALUES("636","143","21","0","2019-05-28");
INSERT INTO open_close_tb VALUES("637","501","40","0","2019-05-28");
INSERT INTO open_close_tb VALUES("638","441","626","0","2019-05-28");
INSERT INTO open_close_tb VALUES("639","500","40","0","2019-05-28");
INSERT INTO open_close_tb VALUES("640","307","2","0","2019-05-28");
INSERT INTO open_close_tb VALUES("641","73","28","0","2019-05-28");
INSERT INTO open_close_tb VALUES("642","34","20","0","2019-05-28");
INSERT INTO open_close_tb VALUES("643","276","197","0","2019-05-28");
INSERT INTO open_close_tb VALUES("644","156","1","0","2019-05-28");
INSERT INTO open_close_tb VALUES("645","430","8","0","2019-05-28");
INSERT INTO open_close_tb VALUES("646","212","22","0","2019-05-28");
INSERT INTO open_close_tb VALUES("647","305","58","0","2019-05-29");
INSERT INTO open_close_tb VALUES("648","319","74","0","2019-05-29");
INSERT INTO open_close_tb VALUES("649","370","12","0","2019-05-29");
INSERT INTO open_close_tb VALUES("650","128","45","40","2019-05-29");
INSERT INTO open_close_tb VALUES("651","221","5","0","2019-05-29");
INSERT INTO open_close_tb VALUES("652","312","30","0","2019-05-29");
INSERT INTO open_close_tb VALUES("653","257","48","0","2019-05-29");
INSERT INTO open_close_tb VALUES("654","444","5","0","2019-05-29");
INSERT INTO open_close_tb VALUES("655","132","4","0","2019-05-29");
INSERT INTO open_close_tb VALUES("656","178","12","0","2019-05-29");
INSERT INTO open_close_tb VALUES("657","239","7","0","2019-05-29");
INSERT INTO open_close_tb VALUES("658","322","86","0","2019-05-29");
INSERT INTO open_close_tb VALUES("659","65","317","0","2019-05-29");
INSERT INTO open_close_tb VALUES("660","2","87","0","2019-05-29");
INSERT INTO open_close_tb VALUES("661","459","171","0","2019-05-29");
INSERT INTO open_close_tb VALUES("662","30","500","487","2019-05-29");
INSERT INTO open_close_tb VALUES("663","374","91","87","2019-05-29");
INSERT INTO open_close_tb VALUES("664","5","45","0","2019-05-29");
INSERT INTO open_close_tb VALUES("665","212","21","0","2019-05-29");
INSERT INTO open_close_tb VALUES("666","320","72","0","2019-05-29");
INSERT INTO open_close_tb VALUES("667","98","41","0","2019-05-29");
INSERT INTO open_close_tb VALUES("668","216","19","0","2019-05-29");
INSERT INTO open_close_tb VALUES("669","251","33","0","2019-05-29");
INSERT INTO open_close_tb VALUES("670","302","8","0","2019-05-29");
INSERT INTO open_close_tb VALUES("671","496","5","0","2019-05-29");
INSERT INTO open_close_tb VALUES("672","492","87","0","2019-05-29");
INSERT INTO open_close_tb VALUES("673","388","8","0","2019-05-29");
INSERT INTO open_close_tb VALUES("674","250","37","0","2019-05-29");
INSERT INTO open_close_tb VALUES("675","277","242","0","2019-05-30");
INSERT INTO open_close_tb VALUES("676","294","7","0","2019-05-30");
INSERT INTO open_close_tb VALUES("677","128","39","31","2019-05-30");
INSERT INTO open_close_tb VALUES("678","458","87","0","2019-05-30");
INSERT INTO open_close_tb VALUES("679","122","25","0","2019-05-30");
INSERT INTO open_close_tb VALUES("680","27","10","0","2019-05-30");
INSERT INTO open_close_tb VALUES("681","212","20","0","2019-05-30");
INSERT INTO open_close_tb VALUES("682","312","29","0","2019-05-30");
INSERT INTO open_close_tb VALUES("683","257","47","0","2019-05-30");
INSERT INTO open_close_tb VALUES("684","320","66","0","2019-05-30");
INSERT INTO open_close_tb VALUES("685","5","44","43","2019-05-30");
INSERT INTO open_close_tb VALUES("686","422","54","0","2019-05-30");
INSERT INTO open_close_tb VALUES("687","252","25","0","2019-05-30");
INSERT INTO open_close_tb VALUES("688","22","766","0","2019-05-30");
INSERT INTO open_close_tb VALUES("689","208","8","0","2019-05-30");
INSERT INTO open_close_tb VALUES("690","295","132","131","2019-05-30");
INSERT INTO open_close_tb VALUES("691","402","87","0","2019-05-30");
INSERT INTO open_close_tb VALUES("692","290","14","0","2019-05-30");
INSERT INTO open_close_tb VALUES("693","250","36","0","2019-05-30");
INSERT INTO open_close_tb VALUES("694","376","21","0","2019-05-30");
INSERT INTO open_close_tb VALUES("695","30","484","478","2019-05-30");
INSERT INTO open_close_tb VALUES("696","21","490","0","2019-05-30");
INSERT INTO open_close_tb VALUES("697","65","302","0","2019-05-30");
INSERT INTO open_close_tb VALUES("698","143","20","0","2019-05-30");
INSERT INTO open_close_tb VALUES("699","62","100","0","2019-05-30");
INSERT INTO open_close_tb VALUES("700","133","15","0","2019-05-31");
INSERT INTO open_close_tb VALUES("701","373","50","0","2019-05-31");
INSERT INTO open_close_tb VALUES("702","372","7","0","2019-05-31");
INSERT INTO open_close_tb VALUES("703","320","40","30","2019-05-31");
INSERT INTO open_close_tb VALUES("704","30","477","476","2019-05-31");
INSERT INTO open_close_tb VALUES("705","449","73","0","2019-05-31");
INSERT INTO open_close_tb VALUES("706","303","10","0","2019-05-31");
INSERT INTO open_close_tb VALUES("707","137","10","0","2019-05-31");
INSERT INTO open_close_tb VALUES("708","61","73","0","2019-05-31");
INSERT INTO open_close_tb VALUES("709","4","21","0","2019-05-31");
INSERT INTO open_close_tb VALUES("710","62","99","98","2019-05-31");
INSERT INTO open_close_tb VALUES("711","440","55","0","2019-05-31");
INSERT INTO open_close_tb VALUES("712","460","65","0","2019-05-31");
INSERT INTO open_close_tb VALUES("713","65","295","0","2019-05-31");
INSERT INTO open_close_tb VALUES("714","73","27","0","2019-05-31");
INSERT INTO open_close_tb VALUES("715","120","58","0","2019-05-31");
INSERT INTO open_close_tb VALUES("716","495","10","0","2019-05-31");
INSERT INTO open_close_tb VALUES("717","20","359","0","2019-05-31");
INSERT INTO open_close_tb VALUES("718","63","10","8","2019-05-31");
INSERT INTO open_close_tb VALUES("719","432","15","0","2019-05-31");
INSERT INTO open_close_tb VALUES("720","299","3","0","2019-05-31");
INSERT INTO open_close_tb VALUES("721","180","257","0","2019-05-31");
INSERT INTO open_close_tb VALUES("722","376","19","0","2019-05-31");
INSERT INTO open_close_tb VALUES("723","277","222","0","2019-05-31");
INSERT INTO open_close_tb VALUES("724","22","756","0","2019-05-31");
INSERT INTO open_close_tb VALUES("725","458","86","0","2019-05-31");
INSERT INTO open_close_tb VALUES("726","128","27","0","2019-05-31");
INSERT INTO open_close_tb VALUES("727","129","123","0","2019-05-31");
INSERT INTO open_close_tb VALUES("728","450","83","82","2019-05-31");
INSERT INTO open_close_tb VALUES("729","192","5","0","2019-05-31");
INSERT INTO open_close_tb VALUES("730","198","5","0","2019-05-31");
INSERT INTO open_close_tb VALUES("731","51","38","0","2019-05-31");
INSERT INTO open_close_tb VALUES("732","50","43","0","2019-05-31");
INSERT INTO open_close_tb VALUES("733","194","83","0","2019-05-31");
INSERT INTO open_close_tb VALUES("734","195","64","0","2019-05-31");
INSERT INTO open_close_tb VALUES("735","202","114","0","2019-05-31");
INSERT INTO open_close_tb VALUES("736","52","6","0","2019-05-31");
INSERT INTO open_close_tb VALUES("737","53","6","0","2019-05-31");
INSERT INTO open_close_tb VALUES("738","54","6","0","2019-05-31");
INSERT INTO open_close_tb VALUES("739","112","7","0","2019-05-31");
INSERT INTO open_close_tb VALUES("740","107","15","0","2019-05-31");
INSERT INTO open_close_tb VALUES("741","390","20","0","2019-05-31");
INSERT INTO open_close_tb VALUES("742","254","30","0","2019-05-31");
INSERT INTO open_close_tb VALUES("743","283","33","0","2019-05-31");
INSERT INTO open_close_tb VALUES("744","249","22","0","2019-05-31");
INSERT INTO open_close_tb VALUES("745","5","42","0","2019-05-31");
INSERT INTO open_close_tb VALUES("746","65","294","291","2019-06-01");
INSERT INTO open_close_tb VALUES("747","434","4","0","2019-06-01");
INSERT INTO open_close_tb VALUES("748","128","26","24","2019-06-01");
INSERT INTO open_close_tb VALUES("749","302","4","0","2019-06-01");
INSERT INTO open_close_tb VALUES("750","338","9","0","2019-06-01");
INSERT INTO open_close_tb VALUES("751","339","13","10","2019-06-01");
INSERT INTO open_close_tb VALUES("752","262","16","0","2019-06-01");
INSERT INTO open_close_tb VALUES("753","261","39","0","2019-06-01");
INSERT INTO open_close_tb VALUES("754","319","69","0","2019-06-01");
INSERT INTO open_close_tb VALUES("755","373","49","0","2019-06-01");
INSERT INTO open_close_tb VALUES("756","448","27","0","2019-06-01");
INSERT INTO open_close_tb VALUES("757","295","130","0","2019-06-01");
INSERT INTO open_close_tb VALUES("758","458","85","0","2019-06-01");
INSERT INTO open_close_tb VALUES("759","123","19","0","2019-06-01");
INSERT INTO open_close_tb VALUES("760","355","16","0","2019-06-01");
INSERT INTO open_close_tb VALUES("761","3","30","0","2019-06-01");
INSERT INTO open_close_tb VALUES("762","4","20","0","2019-06-01");
INSERT INTO open_close_tb VALUES("763","225","6","0","2019-06-01");
INSERT INTO open_close_tb VALUES("764","212","18","0","2019-06-01");
INSERT INTO open_close_tb VALUES("765","320","26","20","2019-06-01");
INSERT INTO open_close_tb VALUES("766","452","17","0","2019-06-01");
INSERT INTO open_close_tb VALUES("767","299","2","0","2019-06-01");
INSERT INTO open_close_tb VALUES("768","460","62","0","2019-06-01");
INSERT INTO open_close_tb VALUES("769","443","65","0","2019-06-01");
INSERT INTO open_close_tb VALUES("770","501","38","0","2019-06-01");
INSERT INTO open_close_tb VALUES("771","374","84","82","2019-06-01");
INSERT INTO open_close_tb VALUES("772","322","66","0","2019-06-01");
INSERT INTO open_close_tb VALUES("773","317","36","0","2019-06-01");
INSERT INTO open_close_tb VALUES("774","442","613","0","2019-06-01");
INSERT INTO open_close_tb VALUES("775","5","40","0","2019-06-01");
INSERT INTO open_close_tb VALUES("776","375","51","0","2019-06-01");
INSERT INTO open_close_tb VALUES("777","137","8","0","2019-06-01");
INSERT INTO open_close_tb VALUES("778","62","97","0","2019-06-03");
INSERT INTO open_close_tb VALUES("779","123","15","0","2019-06-03");
INSERT INTO open_close_tb VALUES("780","128","14","12","2019-06-03");
INSERT INTO open_close_tb VALUES("781","295","128","0","2019-06-03");
INSERT INTO open_close_tb VALUES("782","182","15","0","2019-06-03");
INSERT INTO open_close_tb VALUES("783","137","5","4","2019-06-03");
INSERT INTO open_close_tb VALUES("784","373","48","43","2019-06-03");
INSERT INTO open_close_tb VALUES("785","362","8","0","2019-06-03");
INSERT INTO open_close_tb VALUES("786","312","28","0","2019-06-03");
INSERT INTO open_close_tb VALUES("787","313","28","0","2019-06-03");
INSERT INTO open_close_tb VALUES("788","31","7","6","2019-06-03");
INSERT INTO open_close_tb VALUES("789","492","45","0","2019-06-03");
INSERT INTO open_close_tb VALUES("790","30","474","0","2019-06-03");
INSERT INTO open_close_tb VALUES("791","303","8","0","2019-06-03");
INSERT INTO open_close_tb VALUES("792","372","3","0","2019-06-03");
INSERT INTO open_close_tb VALUES("793","5","38","0","2019-06-03");
INSERT INTO open_close_tb VALUES("794","4","19","0","2019-06-03");
INSERT INTO open_close_tb VALUES("795","163","13","0","2019-06-03");
INSERT INTO open_close_tb VALUES("796","310","13","0","2019-06-03");
INSERT INTO open_close_tb VALUES("797","209","13","0","2019-06-03");
INSERT INTO open_close_tb VALUES("798","374","81","0","2019-06-03");
INSERT INTO open_close_tb VALUES("799","88","10","0","2019-06-03");
INSERT INTO open_close_tb VALUES("800","277","221","0","2019-06-03");
INSERT INTO open_close_tb VALUES("801","322","59","0","2019-06-03");
INSERT INTO open_close_tb VALUES("802","178","11","0","2019-06-03");
INSERT INTO open_close_tb VALUES("803","30","472","452","2019-06-04");
INSERT INTO open_close_tb VALUES("804","320","10","0","2019-06-04");
INSERT INTO open_close_tb VALUES("805","133","14","13","2019-06-04");
INSERT INTO open_close_tb VALUES("806","375","45","0","2019-06-04");
INSERT INTO open_close_tb VALUES("807","21","487","0","2019-06-04");
INSERT INTO open_close_tb VALUES("808","64","57","0","2019-06-04");
INSERT INTO open_close_tb VALUES("809","373","42","0","2019-06-04");
INSERT INTO open_close_tb VALUES("810","443","56","0","2019-06-04");
INSERT INTO open_close_tb VALUES("811","131","7","0","2019-06-04");
INSERT INTO open_close_tb VALUES("812","217","7","0","2019-06-04");
INSERT INTO open_close_tb VALUES("813","182","14","0","2019-06-04");
INSERT INTO open_close_tb VALUES("814","211","1","0","2019-06-04");
INSERT INTO open_close_tb VALUES("815","89","13","12","2019-06-04");
INSERT INTO open_close_tb VALUES("816","98","39","38","2019-06-04");
INSERT INTO open_close_tb VALUES("817","319","66","0","2019-06-04");
INSERT INTO open_close_tb VALUES("818","42","4","0","2019-06-04");
INSERT INTO open_close_tb VALUES("819","492","60","0","2019-06-04");
INSERT INTO open_close_tb VALUES("820","501","37","0","2019-06-04");
INSERT INTO open_close_tb VALUES("821","500","39","0","2019-06-04");
INSERT INTO open_close_tb VALUES("822","317","35","0","2019-06-04");
INSERT INTO open_close_tb VALUES("823","449","71","0","2019-06-04");
INSERT INTO open_close_tb VALUES("824","422","51","0","2019-06-04");
INSERT INTO open_close_tb VALUES("825","65","287","277","2019-06-05");
INSERT INTO open_close_tb VALUES("826","208","7","0","2019-06-05");
INSERT INTO open_close_tb VALUES("827","30","432","0","2019-06-05");
INSERT INTO open_close_tb VALUES("828","98","37","0","2019-06-05");
INSERT INTO open_close_tb VALUES("829","20","344","0","2019-06-05");
INSERT INTO open_close_tb VALUES("830","458","84","0","2019-06-05");
INSERT INTO open_close_tb VALUES("831","214","9","0","2019-06-05");
INSERT INTO open_close_tb VALUES("832","312","27","0","2019-06-05");
INSERT INTO open_close_tb VALUES("833","13","20","0","2019-06-05");
INSERT INTO open_close_tb VALUES("834","255","23","0","2019-06-05");
INSERT INTO open_close_tb VALUES("835","256","28","0","2019-06-05");
INSERT INTO open_close_tb VALUES("836","319","59","57","2019-06-05");
INSERT INTO open_close_tb VALUES("837","317","33","0","2019-06-05");
INSERT INTO open_close_tb VALUES("838","448","24","0","2019-06-05");
INSERT INTO open_close_tb VALUES("839","102","13","0","2019-06-05");
INSERT INTO open_close_tb VALUES("840","374","80","0","2019-06-05");
INSERT INTO open_close_tb VALUES("841","459","168","0","2019-06-05");
INSERT INTO open_close_tb VALUES("842","302","3","0","2019-06-05");
INSERT INTO open_close_tb VALUES("843","123","13","0","2019-06-05");
INSERT INTO open_close_tb VALUES("844","371","16","0","2019-06-05");
INSERT INTO open_close_tb VALUES("845","469","35","0","2019-06-06");
INSERT INTO open_close_tb VALUES("846","133","6","0","2019-06-06");
INSERT INTO open_close_tb VALUES("847","98","36","0","2019-06-06");
INSERT INTO open_close_tb VALUES("848","305","51","0","2019-06-06");
INSERT INTO open_close_tb VALUES("849","283","31","0","2019-06-06");
INSERT INTO open_close_tb VALUES("850","214","8","6","2019-06-06");
INSERT INTO open_close_tb VALUES("851","442","612","0","2019-06-06");
INSERT INTO open_close_tb VALUES("852","400","6","0","2019-06-06");
INSERT INTO open_close_tb VALUES("853","62","95","0","2019-06-06");
INSERT INTO open_close_tb VALUES("854","65","276","270","2019-06-06");
INSERT INTO open_close_tb VALUES("855","460","61","0","2019-06-06");
INSERT INTO open_close_tb VALUES("856","116","29","0","2019-06-06");
INSERT INTO open_close_tb VALUES("857","35","424","0","2019-06-06");
INSERT INTO open_close_tb VALUES("858","270","1","0","2019-06-06");
INSERT INTO open_close_tb VALUES("859","435","15","0","2019-06-06");
INSERT INTO open_close_tb VALUES("860","370","10","0","2019-06-06");
INSERT INTO open_close_tb VALUES("861","254","24","0","2019-06-06");
INSERT INTO open_close_tb VALUES("862","299","1","0","2019-06-07");
INSERT INTO open_close_tb VALUES("863","327","22","0","2019-06-07");
INSERT INTO open_close_tb VALUES("864","295","127","0","2019-06-07");
INSERT INTO open_close_tb VALUES("865","122","23","0","2019-06-07");
INSERT INTO open_close_tb VALUES("866","390","19","0","2019-06-07");
INSERT INTO open_close_tb VALUES("867","6","19","0","2019-06-07");
INSERT INTO open_close_tb VALUES("868","62","65","0","2019-06-07");
INSERT INTO open_close_tb VALUES("869","212","17","0","2019-06-07");
INSERT INTO open_close_tb VALUES("870","96","8","0","2019-06-07");
INSERT INTO open_close_tb VALUES("871","375","44","0","2019-06-07");
INSERT INTO open_close_tb VALUES("872","373","40","0","2019-06-07");
INSERT INTO open_close_tb VALUES("873","460","59","0","2019-06-07");
INSERT INTO open_close_tb VALUES("874","65","268","265","2019-06-07");
INSERT INTO open_close_tb VALUES("875","444","4","0","2019-06-07");
INSERT INTO open_close_tb VALUES("876","450","81","0","2019-06-07");
INSERT INTO open_close_tb VALUES("877","445","76","0","2019-06-07");
INSERT INTO open_close_tb VALUES("878","305","43","0","2019-06-07");
INSERT INTO open_close_tb VALUES("879","317","32","31","2019-06-07");
INSERT INTO open_close_tb VALUES("880","458","83","59","2019-06-07");
INSERT INTO open_close_tb VALUES("881","27","9","0","2019-06-07");
INSERT INTO open_close_tb VALUES("882","384","14","0","2019-06-07");
INSERT INTO open_close_tb VALUES("883","383","20","0","2019-06-07");
INSERT INTO open_close_tb VALUES("884","446","32","0","2019-06-07");
INSERT INTO open_close_tb VALUES("885","447","70","69","2019-06-07");
INSERT INTO open_close_tb VALUES("886","463","6","0","2019-06-07");
INSERT INTO open_close_tb VALUES("887","180","256","0","2019-06-07");
INSERT INTO open_close_tb VALUES("888","422","49","0","2019-06-07");
INSERT INTO open_close_tb VALUES("889","459","167","0","2019-06-07");
INSERT INTO open_close_tb VALUES("890","21","485","0","2019-06-07");
INSERT INTO open_close_tb VALUES("891","4","16","0","2019-06-07");
INSERT INTO open_close_tb VALUES("892","13","17","0","2019-06-08");
INSERT INTO open_close_tb VALUES("893","27","8","0","2019-06-08");
INSERT INTO open_close_tb VALUES("894","129","122","0","2019-06-08");
INSERT INTO open_close_tb VALUES("895","373","39","0","2019-06-08");
INSERT INTO open_close_tb VALUES("896","65","263","213","2019-06-08");
INSERT INTO open_close_tb VALUES("897","193","155","135","2019-06-08");
INSERT INTO open_close_tb VALUES("898","20","341","331","2019-06-08");
INSERT INTO open_close_tb VALUES("899","241","147","0","2019-06-08");
INSERT INTO open_close_tb VALUES("900","305","38","0","2019-06-08");
INSERT INTO open_close_tb VALUES("901","495","9","8","2019-06-08");
INSERT INTO open_close_tb VALUES("902","75","6","0","2019-06-08");
INSERT INTO open_close_tb VALUES("903","319","43","0","2019-06-08");
INSERT INTO open_close_tb VALUES("904","317","29","0","2019-06-08");
INSERT INTO open_close_tb VALUES("905","422","48","0","2019-06-08");
INSERT INTO open_close_tb VALUES("906","446","30","0","2019-06-08");
INSERT INTO open_close_tb VALUES("907","277","206","0","2019-06-08");
INSERT INTO open_close_tb VALUES("908","458","58","0","2019-06-08");
INSERT INTO open_close_tb VALUES("909","255","22","0","2019-06-08");
INSERT INTO open_close_tb VALUES("910","375","42","0","2019-06-08");
INSERT INTO open_close_tb VALUES("911","249","21","0","2019-06-08");
INSERT INTO open_close_tb VALUES("912","35","423","0","2019-06-08");
INSERT INTO open_close_tb VALUES("913","390","18","0","2019-06-08");
INSERT INTO open_close_tb VALUES("914","133","5","0","2019-06-08");
INSERT INTO open_close_tb VALUES("915","30","431","0","2019-06-10");
INSERT INTO open_close_tb VALUES("916","459","166","0","2019-06-10");
INSERT INTO open_close_tb VALUES("917","393","9","0","2019-06-10");
INSERT INTO open_close_tb VALUES("918","322","53","0","2019-06-10");
INSERT INTO open_close_tb VALUES("919","302","2","0","2019-06-10");
INSERT INTO open_close_tb VALUES("920","20","329","0","2019-06-10");
INSERT INTO open_close_tb VALUES("921","436","16","0","2019-06-10");
INSERT INTO open_close_tb VALUES("922","440","42","0","2019-06-10");
INSERT INTO open_close_tb VALUES("923","441","624","0","2019-06-10");
INSERT INTO open_close_tb VALUES("924","319","41","0","2019-06-10");
INSERT INTO open_close_tb VALUES("925","307","1","0","2019-06-10");
INSERT INTO open_close_tb VALUES("926","213","24","0","2019-06-10");
INSERT INTO open_close_tb VALUES("927","65","205","183","2019-06-10");
INSERT INTO open_close_tb VALUES("928","261","29","0","2019-06-10");
INSERT INTO open_close_tb VALUES("929","262","6","0","2019-06-10");
INSERT INTO open_close_tb VALUES("930","246","26","0","2019-06-10");
INSERT INTO open_close_tb VALUES("931","458","57","0","2019-06-10");
INSERT INTO open_close_tb VALUES("932","212","16","0","2019-06-10");
INSERT INTO open_close_tb VALUES("933","295","123","0","2019-06-10");
INSERT INTO open_close_tb VALUES("934","321","129","123","2019-06-11");
INSERT INTO open_close_tb VALUES("935","317","28","0","2019-06-11");
INSERT INTO open_close_tb VALUES("936","98","35","0","2019-06-11");
INSERT INTO open_close_tb VALUES("937","133","4","0","2019-06-11");
INSERT INTO open_close_tb VALUES("938","373","38","0","2019-06-11");
INSERT INTO open_close_tb VALUES("939","295","122","0","2019-06-11");
INSERT INTO open_close_tb VALUES("940","440","37","0","2019-06-11");
INSERT INTO open_close_tb VALUES("941","305","35","0","2019-06-11");
INSERT INTO open_close_tb VALUES("942","65","178","172","2019-06-11");
INSERT INTO open_close_tb VALUES("943","120","57","0","2019-06-11");
INSERT INTO open_close_tb VALUES("944","117","6","0","2019-06-11");
INSERT INTO open_close_tb VALUES("945","35","422","0","2019-06-11");
INSERT INTO open_close_tb VALUES("946","319","33","0","2019-06-11");
INSERT INTO open_close_tb VALUES("947","3","29","0","2019-06-11");
INSERT INTO open_close_tb VALUES("948","65","162","129","2019-06-12");
INSERT INTO open_close_tb VALUES("949","322","100","0","2019-06-12");
INSERT INTO open_close_tb VALUES("950","75","5","0","2019-06-13");
INSERT INTO open_close_tb VALUES("951","34","19","0","2019-06-13");
INSERT INTO open_close_tb VALUES("952","208","6","0","2019-06-13");
INSERT INTO open_close_tb VALUES("953","128","165","0","2019-06-14");
INSERT INTO open_close_tb VALUES("954","295","121","0","2019-06-14");
INSERT INTO open_close_tb VALUES("955","440","36","0","2019-06-14");
INSERT INTO open_close_tb VALUES("956","441","619","0","2019-06-14");
INSERT INTO open_close_tb VALUES("957","106","545","0","2019-06-14");
INSERT INTO open_close_tb VALUES("958","180","254","0","2019-06-14");
INSERT INTO open_close_tb VALUES("959","460","53","0","2019-06-14");
INSERT INTO open_close_tb VALUES("960","214","5","0","2019-06-14");
INSERT INTO open_close_tb VALUES("961","442","611","0","2019-06-14");
INSERT INTO open_close_tb VALUES("962","65","128","0","2019-06-14");
INSERT INTO open_close_tb VALUES("963","56","14","0","2019-06-14");
INSERT INTO open_close_tb VALUES("964","402","84","0","2019-06-14");
INSERT INTO open_close_tb VALUES("965","317","27","0","2019-06-14");
INSERT INTO open_close_tb VALUES("966","221","4","0","2019-06-14");
INSERT INTO open_close_tb VALUES("967","446","29","0","2019-06-14");
INSERT INTO open_close_tb VALUES("968","96","7","0","2019-06-14");
INSERT INTO open_close_tb VALUES("969","5","36","0","2019-06-14");
INSERT INTO open_close_tb VALUES("970","133","3","0","2019-06-14");
INSERT INTO open_close_tb VALUES("971","458","56","0","2019-06-14");
INSERT INTO open_close_tb VALUES("972","57","12","0","2019-06-14");
INSERT INTO open_close_tb VALUES("973","248","33","0","2019-06-14");
INSERT INTO open_close_tb VALUES("974","249","20","0","2019-06-14");
INSERT INTO open_close_tb VALUES("975","260","22","0","2019-06-14");
INSERT INTO open_close_tb VALUES("976","30","428","0","2019-06-14");
INSERT INTO open_close_tb VALUES("977","273","1","0","2019-06-14");
INSERT INTO open_close_tb VALUES("978","151","3","0","2019-06-14");
INSERT INTO open_close_tb VALUES("979","384","13","0","2019-06-15");
INSERT INTO open_close_tb VALUES("980","7","3","0","2019-06-15");
INSERT INTO open_close_tb VALUES("981","321","103","98","2019-06-15");
INSERT INTO open_close_tb VALUES("982","393","8","0","2019-06-15");
INSERT INTO open_close_tb VALUES("983","129","121","0","2019-06-15");
INSERT INTO open_close_tb VALUES("984","128","159","153","2019-06-15");
INSERT INTO open_close_tb VALUES("985","295","119","0","2019-06-15");
INSERT INTO open_close_tb VALUES("986","65","120","89","2019-06-15");
INSERT INTO open_close_tb VALUES("987","73","26","0","2019-06-15");
INSERT INTO open_close_tb VALUES("988","375","41","0","2019-06-15");
INSERT INTO open_close_tb VALUES("989","373","34","0","2019-06-15");
INSERT INTO open_close_tb VALUES("990","5","34","0","2019-06-15");
INSERT INTO open_close_tb VALUES("991","61","71","0","2019-06-15");
INSERT INTO open_close_tb VALUES("992","122","22","0","2019-06-15");
INSERT INTO open_close_tb VALUES("993","317","26","21","2019-06-15");
INSERT INTO open_close_tb VALUES("994","319","17","0","2019-06-15");
INSERT INTO open_close_tb VALUES("995","120","53","0","2019-06-15");
INSERT INTO open_close_tb VALUES("996","62","62","61","2019-06-15");
INSERT INTO open_close_tb VALUES("997","248","32","0","2019-06-15");
INSERT INTO open_close_tb VALUES("998","89","9","0","2019-06-15");
INSERT INTO open_close_tb VALUES("999","283","30","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1000","501","35","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1001","422","47","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1002","30","426","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1003","251","32","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1004","13","16","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1005","14","24","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1006","98","34","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1007","136","9","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1008","163","12","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1009","20","322","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1010","65","69","54","2019-06-17");
INSERT INTO open_close_tb VALUES("1011","26","20","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1012","232","17","16","2019-06-17");
INSERT INTO open_close_tb VALUES("1013","305","32","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1014","375","39","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1015","128","151","145","2019-06-17");
INSERT INTO open_close_tb VALUES("1016","295","116","115","2019-06-17");
INSERT INTO open_close_tb VALUES("1017","373","32","29","2019-06-17");
INSERT INTO open_close_tb VALUES("1018","422","45","43","2019-06-17");
INSERT INTO open_close_tb VALUES("1019","459","165","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1020","319","13","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1021","317","19","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1022","98","33","32","2019-06-17");
INSERT INTO open_close_tb VALUES("1023","30","420","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1024","5","32","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1025","321","88","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1026","460","51","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1027","214","4","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1028","449","70","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1029","246","25","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1030","434","3","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1031","495","7","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1032","65","43","42","2019-06-18");
INSERT INTO open_close_tb VALUES("1033","106","544","541","2019-06-18");
INSERT INTO open_close_tb VALUES("1034","20","312","310","2019-06-18");
INSERT INTO open_close_tb VALUES("1035","236","38","36","2019-06-18");
INSERT INTO open_close_tb VALUES("1036","128","143","138","2019-06-18");
INSERT INTO open_close_tb VALUES("1037","129","115","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1038","373","26","19","2019-06-18");
INSERT INTO open_close_tb VALUES("1039","98","30","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1040","449","69","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1041","328","9","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1042","375","38","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1043","36","94","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1044","320","1","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1045","63","7","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1046","148","172","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1047","147","287","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1048","460","49","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1049","30","416","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1050","283","29","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1051","193","134","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1052","447","68","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1053","445","75","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1054","446","28","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1055","384","12","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1056","369","17","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1057","422","42","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1058","133","2","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1059","303","7","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1060","65","7","4","2019-06-19");
INSERT INTO open_close_tb VALUES("1061","128","135","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1062","295","114","113","2019-06-19");
INSERT INTO open_close_tb VALUES("1063","458","54","52","2019-06-19");
INSERT INTO open_close_tb VALUES("1064","207","8","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1065","30","406","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1066","98","29","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1067","237","13","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1068","3","28","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1069","275","130","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1070","16","8","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1071","458","51","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1072","128","133","128","2019-06-20");
INSERT INTO open_close_tb VALUES("1073","343","56","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1074","301","8","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1075","2","83","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1076","295","112","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1077","321","85","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1078","440","32","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1079","5","27","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1080","333","11","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1081","30","401","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1082","373","17","15","2019-06-20");
INSERT INTO open_close_tb VALUES("1083","176","1","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1084","447","62","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1085","442","607","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1086","203","2","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1087","209","12","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1088","61","69","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1089","214","3","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1090","452","16","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1091","106","540","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1092","98","28","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1093","247","10","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1094","107","13","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1095","390","17","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1096","333","10","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1097","237","12","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1098","373","11","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1099","369","16","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1100","30","395","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1101","458","50","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1102","31","4","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1103","130","1","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1104","136","8","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1105","133","1","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1106","76","3","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1107","452","15","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1108","400","5","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1109","30","394","388","2019-06-22");
INSERT INTO open_close_tb VALUES("1110","369","15","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1111","449","68","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1112","446","22","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1113","317","18","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1114","422","39","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1115","128","127","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1116","98","27","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1117","300","1","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1118","302","1","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1119","226","5","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1120","228","2","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1121","312","26","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1122","6","18","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1123","459","164","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1124","20","300","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1125","366","1","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1126","116","28","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1127","283","27","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1128","390","13","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1129","322","96","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1130","88","9","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1131","336","19","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1132","160","38","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1133","467","14","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1134","442","606","599","2019-06-24");
INSERT INTO open_close_tb VALUES("1135","422","35","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1136","119","9","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1137","321","81","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1138","428","4","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1139","128","126","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1140","295","111","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1141","495","5","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1142","434","2","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1143","124","13","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1144","460","45","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1145","180","252","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1146","319","7","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1147","106","539","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1148","449","65","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1149","21","479","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1150","179","96","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1151","147","286","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1152","214","2","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1153","277","201","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1154","62","58","0","2019-06-25");
INSERT INTO open_close_tb VALUES("1155","64","56","0","2019-06-25");
INSERT INTO open_close_tb VALUES("1156","312","25","0","2019-06-25");
INSERT INTO open_close_tb VALUES("1157","1","1","0","2019-06-25");
INSERT INTO open_close_tb VALUES("1158","3","26","0","2019-07-16");
INSERT INTO open_close_tb VALUES("1159","6","16","0","2019-07-16");
INSERT INTO open_close_tb VALUES("1160","2","81","77","2019-07-22");
INSERT INTO open_close_tb VALUES("1161","2","75","0","2019-07-23");
INSERT INTO open_close_tb VALUES("1162","7","1","0","2019-07-23");
INSERT INTO open_close_tb VALUES("1163","10","3","0","2019-07-23");
INSERT INTO open_close_tb VALUES("1164","6","15","0","2019-07-23");
INSERT INTO open_close_tb VALUES("1165","12","22","20","2019-07-31");
INSERT INTO open_close_tb VALUES("1166","13","20","18","2019-07-31");
INSERT INTO open_close_tb VALUES("1167","23","0","0","2019-08-08");
INSERT INTO open_close_tb VALUES("1168","25","20","14","2019-08-08");
INSERT INTO open_close_tb VALUES("1169","2","144","141","2019-08-11");
INSERT INTO open_close_tb VALUES("1170","18","6","0","2019-08-11");
INSERT INTO open_close_tb VALUES("1171","8","30","0","2019-08-11");
INSERT INTO open_close_tb VALUES("1172","25","36","0","2019-08-11");
INSERT INTO open_close_tb VALUES("1173","29","120","0","2019-08-11");
INSERT INTO open_close_tb VALUES("1174","2","138","124","2019-08-12");
INSERT INTO open_close_tb VALUES("1175","5","72","0","2019-08-12");
INSERT INTO open_close_tb VALUES("1176","4","72","0","2019-08-12");
INSERT INTO open_close_tb VALUES("1177","50","72","0","2019-08-12");
INSERT INTO open_close_tb VALUES("1178","29","119","115","2019-08-12");
INSERT INTO open_close_tb VALUES("1179","2","122","0","2019-08-15");
INSERT INTO open_close_tb VALUES("1180","2","119","111","2019-08-17");
INSERT INTO open_close_tb VALUES("1181","29","114","113","2019-08-17");
INSERT INTO open_close_tb VALUES("1182","56","36","0","2019-08-17");
INSERT INTO open_close_tb VALUES("1183","3","144","0","2019-08-17");
INSERT INTO open_close_tb VALUES("1184","27","36","35","2019-08-17");
INSERT INTO open_close_tb VALUES("1185","2","108","107","2019-08-20");
INSERT INTO open_close_tb VALUES("1186","29","111","110","2019-08-20");
INSERT INTO open_close_tb VALUES("1187","2","116","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1188","5","71","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1189","4","71","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1190","27","32","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1191","29","109","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1192","52","48","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1193","2","114","111","2019-09-18");
INSERT INTO open_close_tb VALUES("1194","5","70","70","2019-09-18");
INSERT INTO open_close_tb VALUES("1195","4","70","0","2019-09-18");
INSERT INTO open_close_tb VALUES("1196","29","108","0","2019-09-18");
INSERT INTO open_close_tb VALUES("1197","2","109","0","2019-09-20");
INSERT INTO open_close_tb VALUES("1198","4","70","0","2019-09-20");
INSERT INTO open_close_tb VALUES("1199","767","10","1","2019-10-05");
INSERT INTO open_close_tb VALUES("1200","172","10","9","2019-10-05");
INSERT INTO open_close_tb VALUES("1201","372","10","9","2019-10-05");
INSERT INTO open_close_tb VALUES("1202","779","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1203","502","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1204","117","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1205","371","10","8","2019-10-05");
INSERT INTO open_close_tb VALUES("1206","246","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1207","100","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1208","471","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1209","471","9","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1210","371","7","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1211","172","7","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1212","133","10","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1213","24","10","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1214","32","10","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1215","840","10","0","2019-10-08");
INSERT INTO open_close_tb VALUES("1216","729","10","0","2019-10-08");
INSERT INTO open_close_tb VALUES("1217","471","8","0","2019-10-08");
INSERT INTO open_close_tb VALUES("1218","33","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1219","779","9","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1220","61","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1221","2","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1222","18","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1223","471","6","5","2019-10-09");
INSERT INTO open_close_tb VALUES("1224","371","6","3","2019-10-09");
INSERT INTO open_close_tb VALUES("1225","372","8","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1226","663","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1227","767","15","14","2019-10-13");
INSERT INTO open_close_tb VALUES("1228","371","1","0","2019-10-13");
INSERT INTO open_close_tb VALUES("1229","172","30","0","2019-10-13");
INSERT INTO open_close_tb VALUES("1230","133","9","0","2019-10-13");
INSERT INTO open_close_tb VALUES("1231","471","4","0","2019-10-13");
INSERT INTO open_close_tb VALUES("1232","832","10","0","2019-10-18");
INSERT INTO open_close_tb VALUES("1233","420","10","0","2019-10-18");
INSERT INTO open_close_tb VALUES("1234","767","1693","1691","2019-10-25");
INSERT INTO open_close_tb VALUES("1235","371","650","0","2019-10-25");
INSERT INTO open_close_tb VALUES("1236","172","29","0","2019-10-25");
INSERT INTO open_close_tb VALUES("1237","501","11","0","2019-10-25");
INSERT INTO open_close_tb VALUES("1238","767","2391","2390","2019-10-27");
INSERT INTO open_close_tb VALUES("1239","767","2388","0","2019-10-28");
INSERT INTO open_close_tb VALUES("1240","371","649","0","2019-10-28");
INSERT INTO open_close_tb VALUES("1241","779","323","0","2019-10-28");
INSERT INTO open_close_tb VALUES("1242","307","10","5","2019-10-28");
INSERT INTO open_close_tb VALUES("1243","767","2385","2371","2019-11-03");
INSERT INTO open_close_tb VALUES("1244","779","322","321","2019-11-03");
INSERT INTO open_close_tb VALUES("1245","172","28","27","2019-11-03");
INSERT INTO open_close_tb VALUES("1246","767","2369","2363","2019-11-07");
INSERT INTO open_close_tb VALUES("1247","372","7","6","2019-11-07");
INSERT INTO open_close_tb VALUES("1248","172","26","0","2019-11-07");
INSERT INTO open_close_tb VALUES("1249","371","648","0","2019-11-07");
INSERT INTO open_close_tb VALUES("1250","501","460","0","2019-11-07");
INSERT INTO open_close_tb VALUES("1251","471","893","0","2019-11-07");
INSERT INTO open_close_tb VALUES("1252","767","2362","0","2019-11-08");
INSERT INTO open_close_tb VALUES("1253","767","2361","0","2019-11-11");
INSERT INTO open_close_tb VALUES("1254","172","25","0","2019-11-11");
INSERT INTO open_close_tb VALUES("1255","372","5","4","2019-11-11");
INSERT INTO open_close_tb VALUES("1256","897","6","4","2019-11-11");
INSERT INTO open_close_tb VALUES("1257","779","320","0","2019-11-11");
INSERT INTO open_close_tb VALUES("1258","371","647","0","2019-11-11");
INSERT INTO open_close_tb VALUES("1259","502","109","0","2019-11-11");
INSERT INTO open_close_tb VALUES("1260","767","2360","0","2019-11-12");
INSERT INTO open_close_tb VALUES("1261","172","23","0","2019-11-12");
INSERT INTO open_close_tb VALUES("1262","767","2359","2356","2019-11-16");
INSERT INTO open_close_tb VALUES("1263","767","2355","0","2019-11-27");
INSERT INTO open_close_tb VALUES("1264","172","22","0","2019-11-27");
INSERT INTO open_close_tb VALUES("1265","767","2352","2348","2019-11-29");
INSERT INTO open_close_tb VALUES("1266","172","21","19","2019-11-29");
INSERT INTO open_close_tb VALUES("1267","371","646","0","2019-11-29");
INSERT INTO open_close_tb VALUES("1268","372","3","0","2019-11-29");
INSERT INTO open_close_tb VALUES("1269","502","108","0","2019-11-29");
INSERT INTO open_close_tb VALUES("1270","501","459","0","2019-11-29");
INSERT INTO open_close_tb VALUES("1271","542","11","0","2019-11-29");
INSERT INTO open_close_tb VALUES("1272","782","10","0","2019-11-29");
INSERT INTO open_close_tb VALUES("1273","185","10","0","2019-11-29");
INSERT INTO open_close_tb VALUES("1274","900","597","589","2019-12-12");
INSERT INTO open_close_tb VALUES("1275","899","29","25","2019-12-12");
INSERT INTO open_close_tb VALUES("1276","900","0","0","2019-12-14");
INSERT INTO open_close_tb VALUES("1277","682","2","0","2019-12-14");
INSERT INTO open_close_tb VALUES("1278","682","1000","950","2019-12-15");
INSERT INTO open_close_tb VALUES("1279","682","940","938","2019-12-16");
INSERT INTO open_close_tb VALUES("1280","682","934","0","2019-12-17");
INSERT INTO open_close_tb VALUES("1281","683","20","17","2019-12-17");
INSERT INTO open_close_tb VALUES("1282","683","13","1","2019-12-19");
INSERT INTO open_close_tb VALUES("1283","682","930","923","2019-12-19");
INSERT INTO open_close_tb VALUES("1284","682","921","0","2019-12-22");
INSERT INTO open_close_tb VALUES("1285","683","30","0","2019-12-22");
INSERT INTO open_close_tb VALUES("1286","683","27","0","2019-12-30");
INSERT INTO open_close_tb VALUES("1287","682","920","0","2019-12-30");



CREATE TABLE `part_payments_tb` (
  `payment_id` int(12) NOT NULL AUTO_INCREMENT,
  `amount` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_no` int(11) NOT NULL,
  `user_id` int(12) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO part_payments_tb VALUES("1","20","2019-12-10 09:27:55","4160","8");



CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `payment` decimal(10,2) NOT NULL,
  `payment_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `payment_for` date NOT NULL,
  `due` decimal(10,2) NOT NULL,
  `interest` decimal(10,2) NOT NULL,
  `remaining` decimal(10,2) NOT NULL,
  `status` varchar(20) NOT NULL,
  `rebate` decimal(10,2) NOT NULL,
  `or_no` int(11) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

INSERT INTO payment VALUES("1","0","1","104.50","2019-10-05 19:56:40","8","1","2019-10-05","104.50","0.00","0.00","paid","0.00","1901");
INSERT INTO payment VALUES("2","0","2","45.00","2019-10-05 21:46:16","8","1","2019-10-05","45.00","0.00","0.00","paid","0.00","1902");
INSERT INTO payment VALUES("3","0","3","15.50","2019-10-05 22:05:56","8","1","2019-10-05","15.50","0.00","0.00","paid","0.00","1903");
INSERT INTO payment VALUES("4","0","4","36.00","2019-10-05 22:06:56","8","1","2019-10-05","36.00","0.00","0.00","paid","0.00","1904");
INSERT INTO payment VALUES("5","0","5","30.00","2019-10-05 22:08:50","8","1","2019-10-05","30.00","0.00","0.00","paid","0.00","1905");
INSERT INTO payment VALUES("6","0","6","206.00","2019-10-05 22:09:30","8","1","2019-10-05","206.00","0.00","0.00","paid","0.00","1906");
INSERT INTO payment VALUES("7","0","7","30.00","2019-10-05 22:28:31","8","1","2019-10-05","30.00","0.00","0.00","paid","0.00","1907");
INSERT INTO payment VALUES("8","0","8","96.00","2019-10-05 22:48:50","8","1","2019-10-05","96.00","0.00","0.00","paid","0.00","1908");
INSERT INTO payment VALUES("9","0","9","110.50","2019-10-06 00:02:58","8","1","2019-10-06","110.50","0.00","0.00","paid","0.00","1909");
INSERT INTO payment VALUES("10","0","10","36.20","2019-10-06 13:54:39","8","1","2019-10-06","36.20","0.00","0.00","paid","0.00","1910");
INSERT INTO payment VALUES("11","0","11","66.50","2019-10-08 08:13:04","8","1","2019-10-08","66.50","0.00","0.00","paid","0.00","1911");
INSERT INTO payment VALUES("12","0","12","72.00","2019-10-08 08:13:18","8","1","2019-10-08","72.00","0.00","0.00","paid","0.00","1912");
INSERT INTO payment VALUES("13","0","13","11.85","2019-10-09 12:13:34","8","1","2019-10-09","11.85","0.00","0.00","paid","0.00","1913");
INSERT INTO payment VALUES("14","0","14","0.50","2019-10-09 12:24:25","8","1","2019-10-09","0.50","0.00","0.00","paid","0.00","1914");
INSERT INTO payment VALUES("15","0","15","30.00","2019-10-09 12:28:46","8","1","2019-10-09","30.00","0.00","0.00","paid","0.00","1915");
INSERT INTO payment VALUES("16","0","16","61.00","2019-10-09 12:30:58","8","1","2019-10-09","61.00","0.00","0.00","paid","0.00","1916");
INSERT INTO payment VALUES("17","0","17","160.00","2019-10-09 12:32:25","8","1","2019-10-09","160.00","0.00","0.00","paid","0.00","1917");
INSERT INTO payment VALUES("18","0","18","232.50","2019-10-09 12:48:24","8","1","2019-10-09","232.50","0.00","0.00","paid","0.00","1918");
INSERT INTO payment VALUES("19","0","19","75.00","2019-10-13 16:03:42","8","1","2019-10-13","75.00","0.00","0.00","paid","0.00","1919");
INSERT INTO payment VALUES("20","0","20","29.50","2019-10-13 16:08:45","8","1","2019-10-13","29.50","0.00","0.00","paid","0.00","1920");
INSERT INTO payment VALUES("21","0","21","36.00","2019-10-13 16:11:22","8","1","2019-10-13","36.00","0.00","0.00","paid","0.00","1921");
INSERT INTO payment VALUES("22","0","22","38.00","2019-10-18 08:13:14","8","1","2019-10-18","38.00","0.00","0.00","paid","0.00","1922");
INSERT INTO payment VALUES("23","0","23","90.00","2019-10-25 19:20:15","8","1","2019-10-25","90.00","0.00","0.00","paid","0.00","1923");
INSERT INTO payment VALUES("24","0","24","26.00","2019-10-25 19:21:22","8","1","2019-10-25","26.00","0.00","0.00","paid","0.00","1924");
INSERT INTO payment VALUES("25","0","25","15.00","2019-10-27 20:02:08","8","1","2019-10-27","15.00","0.00","0.00","paid","0.00","1925");
INSERT INTO payment VALUES("26","0","26","30.00","2019-10-27 20:03:14","8","1","2019-10-27","30.00","0.00","0.00","paid","0.00","1926");
INSERT INTO payment VALUES("27","0","27","105.00","2019-10-28 08:05:14","8","1","2019-10-28","105.00","0.00","0.00","paid","0.00","1927");
INSERT INTO payment VALUES("28","0","28","15.50","2019-10-28 13:56:29","8","1","2019-10-28","15.50","0.00","0.00","paid","0.00","1928");
INSERT INTO payment VALUES("29","0","29","9.00","2019-10-28 13:57:12","8","1","2019-10-28","9.00","0.00","0.00","paid","0.00","1929");
INSERT INTO payment VALUES("30","0","43","36.00","2019-11-07 19:21:51","8","1","2019-11-07","36.00","0.00","0.00","paid","0.00","1930");
INSERT INTO payment VALUES("31","0","51","30.00","2019-11-16 15:09:50","8","1","2019-11-16","30.00","0.00","0.00","paid","0.00","1931");
INSERT INTO payment VALUES("32","0","53","15.00","2019-11-16 15:15:21","8","1","2019-11-16","15.00","0.00","0.00","paid","0.00","1932");
INSERT INTO payment VALUES("33","0","54","45.00","2019-11-27 16:55:48","8","1","2019-11-27","45.00","0.00","0.00","paid","0.00","1933");
INSERT INTO payment VALUES("34","0","55","10.50","2019-11-27 16:56:37","8","1","2019-11-27","10.50","0.00","0.00","paid","0.00","1934");
INSERT INTO payment VALUES("35","0","3","8.00","2019-12-16 16:45:58","8","1","2019-12-16","8.00","0.00","0.00","paid","0.00","1904");
INSERT INTO payment VALUES("36","0","15","2.00","2019-12-19 14:59:20","8","1","2019-12-19","2.00","0.00","0.00","paid","0.00","1916");
INSERT INTO payment VALUES("37","0","16","4.00","2019-12-19 16:01:13","8","1","2019-12-19","4.00","0.00","0.00","paid","0.00","1917");
INSERT INTO payment VALUES("38","0","17","4.00","2019-12-19 16:10:18","8","1","2019-12-19","4.00","0.00","0.00","paid","0.00","1918");



CREATE TABLE `price_adjustments_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `product` text NOT NULL,
  `type_adjustment` text NOT NULL,
  `instruction` text NOT NULL,
  `amount` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

INSERT INTO price_adjustments_tb VALUES("6","All","Amount","Decrease","9.2","2019-12-01 15:54:12","4");
INSERT INTO price_adjustments_tb VALUES("7","All","Amount","Increase","5","2019-12-01 15:54:47","4");
INSERT INTO price_adjustments_tb VALUES("8","900","Amount","Increase","2","2019-12-01 16:05:58","4");
INSERT INTO price_adjustments_tb VALUES("9","900","Amount","Decrease","2","2019-12-01 16:06:22","4");
INSERT INTO price_adjustments_tb VALUES("10","900","Amount","Decrease","2","2019-12-01 16:07:20","4");
INSERT INTO price_adjustments_tb VALUES("11","900","Amount","Decrease","3","2019-12-01 16:07:45","4");
INSERT INTO price_adjustments_tb VALUES("12","900","Percent","Increase","10","2019-12-01 16:08:04","4");
INSERT INTO price_adjustments_tb VALUES("13","899","Amount","Increase","0.55","2019-12-01 16:11:44","4");



CREATE TABLE `product` (
  `prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_name` varchar(100) NOT NULL,
  `prod_desc` varchar(500) NOT NULL,
  `prod_price` decimal(10,2) NOT NULL,
  `prod_sell_price` text NOT NULL,
  `prod_pic` varchar(300) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `prod_qty` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `reorder` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `serial` varchar(50) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `belongs_to` int(12) NOT NULL,
  `stock_branch_id` int(12) NOT NULL,
  `barcode` text NOT NULL,
  `manufactor_date` text NOT NULL,
  `expire_date` text NOT NULL,
  `vat_status` text NOT NULL,
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=684 DEFAULT CHARSET=latin1;

INSERT INTO product VALUES("2","KELLOGG'S CORN FLAKES ","KELLOGG'S CORN FLAKES ","0.00","30","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001306015501","","","inclusive");
INSERT INTO product VALUES("3","NESCAFE RICOFFY ","NESCAFE RICOFFY ","0.00","45","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6001068323005","","","inclusive");
INSERT INTO product VALUES("4","NESTLE NIDO ","NESTLE NIDO ","0.00","64","default.gif","0","25","1","0","0","Non","0000-00-00 00:00:00","1","1","6001068705504","","","inclusive");
INSERT INTO product VALUES("5","GO FRESH ORANGE SQUASH ","GO FRESH ORANGE SQUASH ","0.00","21","default.gif","0","4","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644924431","","","inclusive");
INSERT INTO product VALUES("6","GO FRESH FRUIT COCKTAIL","GO FRESH FRUIT COCKTAIL","12.75","21","default.gif","0","16","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644925261","","","inclusive");
INSERT INTO product VALUES("7","THIRSTY ORANGE SQUASH ","THIRSTY ORANGE SQUASH ","16.08","26","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009650800156","","","inclusive");
INSERT INTO product VALUES("8","JUST ORANGE ","JUST ORANGE ","0.00","28","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652782559","","","inclusive");
INSERT INTO product VALUES("9","BAKERS CHOICE ASSORTED ","BAKERS CHOICE ASSORTED ","17.20","24","default.gif","0","24","1","0","0","Non","0000-00-00 00:00:00","1","1","6009704170488","","","inclusive");
INSERT INTO product VALUES("10","LIQUIFRUIT SUMMER PINE","LIQUIFRUIT SUMMER PINE","0.00","23","default.gif","0","3","1","0","0","Non","0000-00-00 00:00:00","1","1","6001048000407","","","inclusive");
INSERT INTO product VALUES("11","MONSTA CREAM  LEMON FLAVOURED ","MONSTA CREAM  LEMON FLAVOURED ","0.00","4","default.gif","0","39","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644999675","","","inclusive");
INSERT INTO product VALUES("12","JUNGLE OATS","JUNGLE OATS","0.00","24","default.gif","0","25","1","0","0","Non","0000-00-00 00:00:00","1","1","6009518201958","","","inclusive");
INSERT INTO product VALUES("13","SUMO COCONUT FLAVOURED BISCUITS ","SUMO COCONUT FLAVOURED BISCUITS ","0.00","2.5","default.gif","0","81","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644926855","","","inclusive");
INSERT INTO product VALUES("14","CHICCO BUTTER COOKIES ","CHICCO BUTTER COOKIES ","0.00","2.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6291103080059","","","inclusive");
INSERT INTO product VALUES("15","CHICCO COCONUT COOKIES ","CHICCO COCONUT COOKIES ","0.00","2.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6291103080080","","","inclusive");
INSERT INTO product VALUES("16","SUMO CREAMY BUTTER ","SUMO CREAMY BUTTER ","0.00","2.5","default.gif","0","66","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644926916","","","inclusive");
INSERT INTO product VALUES("17","CHICCO MALT & MILK ","CHICCO MALT & MILK ","0.00","2.5","default.gif","0","13","1","0","0","Non","0000-00-00 00:00:00","1","1","6291103080073","","","inclusive");
INSERT INTO product VALUES("18","CHICCO CHOCOLATE COOKIES","CHICCO CHOCOLATE COOKIES","0.00","2.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6291103080066","","","inclusive");
INSERT INTO product VALUES("19","JOLLY JUICE ","JOLLY JUICE ","0.00","22","default.gif","0","25","1","0","0","Non","0000-00-00 00:00:00","1","1","5449000131218","","","inclusive");
INSERT INTO product VALUES("20","GO FRESH PINEAPPLE SQUASH ","GO FRESH PINEAPPLE SQUASH ","12.75","21","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644925247","","","inclusive");
INSERT INTO product VALUES("21","THIRSTY  PINEAPPLE SQUASH ","THIRSTY  PINEAPPLE SQUASH ","16.08","26","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6009650800118","","","inclusive");
INSERT INTO product VALUES("22","MORTEIN MULTI INSECT KILLER LOW ODOUR ","MORTEIN MULTI INSECT KILLER LOW ODOUR ","0.00","22","default.gif","0","71","1","0","0","Non","0000-00-00 00:00:00","1","1","6001106103941","","","inclusive");
INSERT INTO product VALUES("23","DOOM SUPER MULTI INSECT KILLER ","DOOM SUPER MULTI INSECT KILLER ","0.00","28","default.gif","0","9","1","0","0","Non","0000-00-00 00:00:00","1","1","6001206413681","","","inclusive");
INSERT INTO product VALUES("24","MORTEIN MULTII INSECT ODOURLESS ","MORTEIN MULTII INSECT ODOURLESS ","0.00","22","default.gif","0","6","1","0","0","Non","0000-00-00 00:00:00","1","1","6001106225636","","","inclusive");
INSERT INTO product VALUES("25","ANYTIME PEANUT BUTTER ","ANYTIME PEANUT BUTTER ","0.00","35","default.gif","0","48","1","0","0","Non","0000-00-00 00:00:00","1","1","PEANUT ","","","inclusive");
INSERT INTO product VALUES("26","FIVE ROSES ","FIVE ROSES ","0.00","32","default.gif","0","6","1","0","0","Non","0000-00-00 00:00:00","1","1","6001156230154","","","inclusive");
INSERT INTO product VALUES("27","LYONS QUICK BREW TEA BAGS ","LYONS QUICK BREW TEA BAGS ","0.00","15","default.gif","0","10","1","0","0","Non","0000-00-00 00:00:00","1","1","6004842001519","","","inclusive");
INSERT INTO product VALUES("28","FRESHPAK TEA BAGS ","FRESHPAK TEA BAGS ","12.50","17.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001156905106","","","inclusive");
INSERT INTO product VALUES("29","LUCKY STAR SARDINES ","LUCKY STAR SARDINES ","0.00","12","default.gif","0","58","1","0","0","Non","0000-00-00 00:00:00","1","1","6001115000613","","","inclusive");
INSERT INTO product VALUES("30","KOO BAKED BEANS","KOO BAKED BEANS","0.00","12","default.gif","0","4","1","0","0","Non","0000-00-00 00:00:00","1","1","6009522300586","","","inclusive");
INSERT INTO product VALUES("31","RIVONNIA LEMON MARMALADE ","RIVONNIA LEMON MARMALADE ","0.00","15","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6009608440236","","","inclusive");
INSERT INTO product VALUES("32","RIVONNIA MIXED FRUIT JAM ","RIVONNIA MIXED FRUIT JAM ","0.00","15","default.gif","0","23","1","0","0","Non","0000-00-00 00:00:00","1","1","6009608440274","","","inclusive");
INSERT INTO product VALUES("33","RIVONNIA ORANGE MARMALADE ","RIVONNIA ORANGE MARMALADE ","0.00","15","default.gif","0","17","1","0","0","Non","0000-00-00 00:00:00","1","1","6009608440205","","","inclusive");
INSERT INTO product VALUES("34","RIVONNIA MANGO JAM ","RIVONNIA MANGO JAM ","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009608440250","","","inclusive");
INSERT INTO product VALUES("35","ALL GOLD TOMATO SAUCE ","ALL GOLD TOMATO SAUCE ","0.00","20","default.gif","0","21","1","0","0","Non","0000-00-00 00:00:00","1","1","60019578","","","inclusive");
INSERT INTO product VALUES("36","FATTI'S & MONI'S SPAGHETTI","FATTI'S & MONI'S SPAGHETTI","0.00","13","default.gif","0","50","1","0","0","Non","0000-00-00 00:00:00","1","1","6009522300098","","","inclusive");
INSERT INTO product VALUES("37","FATTI'S & MONI'S MACARONI ","FATTI'S & MONI'S MACARONI ","0.00","14","default.gif","0","36","1","0","0","Non","0000-00-00 00:00:00","1","1","6009522300111","","","inclusive");
INSERT INTO product VALUES("38","CREAMBELL FULL CREAM MILK  SACHETS ","CREAMBELL FULL CREAM MILK  SACHETS ","0.00","8","default.gif","0","5","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672043","","","inclusive");
INSERT INTO product VALUES("39","LUX MAGICAL BEAUTY ","LUX MAGICAL BEAUTY ","0.00","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6221048062070","","","inclusive");
INSERT INTO product VALUES("40","VASELINE BLUE SEAL ","VASELINE BLUE SEAL ","0.00","30","default.gif","0","23","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087005630","","","inclusive");
INSERT INTO product VALUES("41","KIWI BLACK SHOE POLISH ","KIWI BLACK SHOE POLISH ","0.00","10","default.gif","0","25","1","0","0","Non","0000-00-00 00:00:00","1","1","6161108970030","","","inclusive");
INSERT INTO product VALUES("42","COLGATE  REGULAR FLAVOUR ","COLGATE  REGULAR FLAVOUR ","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001067066613","","","inclusive");
INSERT INTO product VALUES("43","PROTEX ALOE  ANTIGERM  ","PROTEX ALOE  ANTIGERM  ","0.00","11","default.gif","0","9","1","0","0","Non","0000-00-00 00:00:00","1","1","6001067004301","","","inclusive");
INSERT INTO product VALUES("44","PROTEX DEEP CLEAN ANTIGERM ","PROTEX DEEP CLEAN ANTIGERM ","0.00","11","default.gif","0","44","1","0","0","Non","0000-00-00 00:00:00","1","1","6001067004318","","","inclusive");
INSERT INTO product VALUES("45","PROTEX ALOE ANTIGERM ","PROTEX ALOE ANTIGERM ","0.00","6","default.gif","0","3","1","0","0","Non","0000-00-00 00:00:00","1","1","6001067024989","","","inclusive");
INSERT INTO product VALUES("46","PROTEX HERBAL ANTIGERM ","PROTEX HERBAL ANTIGERM ","0.00","6","default.gif","0","48","1","0","0","Non","0000-00-00 00:00:00","1","1","6001067022480","","","inclusive");
INSERT INTO product VALUES("47","PROTEX DEEP CLEAN ANTI GERM ","PROTEX DEEP CLEAN ANTI GERM ","0.00","6","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6001067026877","","","inclusive");
INSERT INTO product VALUES("48","PROTEX GENTLE ","PROTEX GENTLE ","0.00","6","default.gif","0","55","1","0","0","Non","0000-00-00 00:00:00","1","1","6001067020059","","","inclusive");
INSERT INTO product VALUES("49","PROTEX FRESH ANTIGERM ","PROTEX FRESH ANTIGERM ","0.00","11","default.gif","0","25","1","0","0","Non","0000-00-00 00:00:00","1","1","6001067004264","","","inclusive");
INSERT INTO product VALUES("50","LIFEBUOY MOISTURE PLUS","LIFEBUOY MOISTURE PLUS","0.00","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087358668","","","inclusive");
INSERT INTO product VALUES("51","DETTOL FRESH ANTI BACTERIAL ","DETTOL FRESH ANTI BACTERIAL ","0.00","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001106225261","","","inclusive");
INSERT INTO product VALUES("52","DETTOL EVEN TONE","DETTOL EVEN TONE","0.00","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001106225070","","","inclusive");
INSERT INTO product VALUES("53","LUX SOFT TOUCH ","LUX SOFT TOUCH ","0.00","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6221048062100","","","inclusive");
INSERT INTO product VALUES("54","LUX SOFT TOUCH ","LUX SOFT TOUCH ","8.25","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087361293","","","inclusive");
INSERT INTO product VALUES("55","ROMEO HERBAL ","ROMEO HERBAL ","5.50","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644923274","","","inclusive");
INSERT INTO product VALUES("56","ROMEO CLASSIC ","ROMEO CLASSIC ","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644923267","","","inclusive");
INSERT INTO product VALUES("57","LIFEBUOY CARE ","LIFEBUOY CARE ","0.00","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087358620","","","inclusive");
INSERT INTO product VALUES("58","SOFTEX TOILET TISSUE ","SOFTEX TOILET TISSUE ","3.31","6","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652782023","","","inclusive");
INSERT INTO product VALUES("59","BOOM PASTE ","BOOM PASTE ","7.48","14","default.gif","0","54","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644920167","","","inclusive");
INSERT INTO product VALUES("60","BOOM  DETERGENT PASTE","BOOM  DETERGENT PASTE","4.48","7.5","default.gif","0","83","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("61","COLGATE TOOTH BRUSH ","COLGATE TOOTH BRUSH ","0.00","6","default.gif","0","71","1","0","0","Non","0000-00-00 00:00:00","1","1","6001067007289","","","inclusive");
INSERT INTO product VALUES("62","LUX SOFT TOUCH BODY LOTION ","LUX SOFT TOUCH BODY LOTION ","0.00","28","default.gif","0","29","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087010078","","","inclusive");
INSERT INTO product VALUES("63","BIG 1 CANDLES ","BIG 1 CANDLES ","0.00","13","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("64","DAWN TROPICAL CREAM BODY LOTION  ","DAWN TROPICAL CREAM BODY LOTION  ","0.00","15","default.gif","0","20","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087012386","","","inclusive");
INSERT INTO product VALUES("65","DAWN RADIANT GLOW BODY LOTION ","DAWN RADIANT GLOW BODY LOTION ","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087010979","","","inclusive");
INSERT INTO product VALUES("66","DAWN SPECIAL MUSK ","DAWN SPECIAL MUSK ","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087012379","","","inclusive");
INSERT INTO product VALUES("67","ALWAYS MAXI PADS ","ALWAYS MAXI PADS ","0.00","25","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","4015400267157","","","inclusive");
INSERT INTO product VALUES("68","FREESTYLE THICK PADS ","FREESTYLE THICK PADS ","0.00","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6997063750543","","","inclusive");
INSERT INTO product VALUES("69","VASELINE ORIGINAL BLUE SEAL ","VASELINE ORIGINAL BLUE SEAL ","0.00","30","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087005661","","","inclusive");
INSERT INTO product VALUES("70","DAWN COCOA BUTTER ","DAWN COCOA BUTTER ","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087012348","","","inclusive");
INSERT INTO product VALUES("71","DAWN VITAMIN E BODY LOTION ","DAWN VITAMIN E BODY LOTION ","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087012393","","","inclusive");
INSERT INTO product VALUES("72","DAWN RICH LANOLIN ","DAWN RICH LANOLIN ","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087012362","","","inclusive");
INSERT INTO product VALUES("73","DAWN ALOE VERA","DAWN ALOE VERA","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087012331","","","inclusive");
INSERT INTO product VALUES("74","FREESTYLE COMPACT PADS ","FREESTYLE COMPACT PADS ","0.00","14","default.gif","0","50","1","0","0","Non","0000-00-00 00:00:00","1","1","6932833851105","","","inclusive");
INSERT INTO product VALUES("75","WHITEROSE COTTON WOOL ","WHITEROSE COTTON WOOL ","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009681267782","","","inclusive");
INSERT INTO product VALUES("76","BOOM POWDER ","BOOM POWDER ","0.00","23","default.gif","0","50","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644923441","","","inclusive");
INSERT INTO product VALUES("77","BOOM POWDER ","BOOM POWDER ","0.00","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644923458","","","inclusive");
INSERT INTO product VALUES("78","SHIELD SEXY ","SHIELD SEXY ","0.00","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","60018038","","","inclusive");
INSERT INTO product VALUES("79","SHIELD MUSK ","SHIELD MUSK ","0.00","16","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","60098009","","","inclusive");
INSERT INTO product VALUES("80","SHIELD SHOWER FRESH ","SHIELD SHOWER FRESH ","0.00","16","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","60022042","","","inclusive");
INSERT INTO product VALUES("81","SHIELD OXYGEN ","SHIELD OXYGEN ","0.00","16","default.gif","0","4","1","0","0","Non","0000-00-00 00:00:00","1","1","60099099","","","inclusive");
INSERT INTO product VALUES("82","MIMS METHYLATED SPIRIT","MIMS METHYLATED SPIRIT","0.00","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009602980523","","","inclusive");
INSERT INTO product VALUES("83","SKYN CONDOMS ","SKYN CONDOMS ","0.00","60","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","5011831092107","","","inclusive");
INSERT INTO product VALUES("84","CREAMBELL SHAKERS  BANANA MILK SHAKE","CREAMBELL SHAKERS  BANANA MILK SHAKE","0.00","8","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672623","","","inclusive");
INSERT INTO product VALUES("85","CREAMBELL SHAKERS VANILLA  ","CREAMBELL SHAKERS VANILLA  ","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672647","","","inclusive");
INSERT INTO product VALUES("86","CREAMBELL SHAKERS STRAWBERRY ","CREAMBELL SHAKERS STRAWBERRY ","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672630","","","inclusive");
INSERT INTO product VALUES("87","CREAMBELL SHAKERS VANILLA","CREAMBELL SHAKERS VANILLA","0.00","4","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672661","","","inclusive");
INSERT INTO product VALUES("88","CREAMBELL SHAKERS BANANA","CREAMBELL SHAKERS BANANA","0.00","4","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672678","","","inclusive");
INSERT INTO product VALUES("89","MIRINDA BOTTLED ","MIRINDA BOTTLED ","0.00","4.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("90","CREAMBELL MILK MAHEU  BANANA","CREAMBELL MILK MAHEU  BANANA","0.00","3.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672470","","","inclusive");
INSERT INTO product VALUES("91","MIRINDA ORANGE ","MIRINDA ORANGE ","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009681261285","","","inclusive");
INSERT INTO product VALUES("92","MIRINDA FRUITY ","MIRINDA FRUITY ","0.00","8","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6009681261209","","","inclusive");
INSERT INTO product VALUES("93","MIRINDA PINEAPPLE ","MIRINDA PINEAPPLE ","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009681261483","","","inclusive");
INSERT INTO product VALUES("94","MIRINDA FRUITY ","MIRINDA FRUITY ","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009681267157","","","inclusive");
INSERT INTO product VALUES("95","PEPSI ","PEPSI ","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009681267645","","","inclusive");
INSERT INTO product VALUES("96","SUNDANCE ORANGE FLAVOURED ","SUNDANCE ORANGE FLAVOURED ","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644991280","","","inclusive");
INSERT INTO product VALUES("97","SWIFT MIXED BERRY ","SWIFT MIXED BERRY ","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652782986","","","inclusive");
INSERT INTO product VALUES("98","CASTLE LITE ","CASTLE LITE ","0.00","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6003326002936","","","inclusive");
INSERT INTO product VALUES("99","FRUITREE GUAVA ","FRUITREE GUAVA ","7.27","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240430033","","","inclusive");
INSERT INTO product VALUES("100","HUNTERS DRY","HUNTERS DRY","0.00","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001108055187","","","inclusive");
INSERT INTO product VALUES("101","KUNG FU ENERGY DRINK ","KUNG FU ENERGY DRINK ","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537015956","","","inclusive");
INSERT INTO product VALUES("102","AQUA MINERAL WATER ","AQUA MINERAL WATER ","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009701985122","","","inclusive");
INSERT INTO product VALUES("103","AQUA MINERAL WATER ","AQUA MINERAL WATER ","0.00","4","default.gif","0","31","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("104","SUPERSHAKE BANANA & STRAWBERRY ","SUPERSHAKE BANANA & STRAWBERRY ","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644921492","","","inclusive");
INSERT INTO product VALUES("105","POWER ","POWER ","0.00","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6161109780416","","","inclusive");
INSERT INTO product VALUES("106","TANGAWIZI ","TANGAWIZI ","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6161109780072","","","inclusive");
INSERT INTO product VALUES("107","APPY GRAPO","APPY GRAPO","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644996995","","","inclusive");
INSERT INTO product VALUES("108","TONIC WATER ","TONIC WATER ","0.00","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652782863","","","inclusive");
INSERT INTO product VALUES("109","HAVANA COLA","HAVANA COLA","0.00","5","default.gif","0","13","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644922963","","","inclusive");
INSERT INTO product VALUES("110","AZAM EMBE ","AZAM EMBE ","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6203011060801","","","inclusive");
INSERT INTO product VALUES("111","CREAMBELL MAHEU  BUTTER CREAM ","CREAMBELL MAHEU  BUTTER CREAM ","0.00","4.5","default.gif","0","53","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672821","","","inclusive");
INSERT INTO product VALUES("112","CREAMBELL FULL CREAM MILK ","CREAMBELL FULL CREAM MILK ","0.00","8","default.gif","0","24","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672012","","","inclusive");
INSERT INTO product VALUES("113","CREAMBELL MAHEU BANANA ","CREAMBELL MAHEU BANANA ","0.00","4.5","default.gif","0","22","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672838","","","inclusive");
INSERT INTO product VALUES("114","AFIA MANGO ","AFIA MANGO ","0.00","13","default.gif","0","10","1","0","0","Non","0000-00-00 00:00:00","1","1","6008459000453","","","inclusive");
INSERT INTO product VALUES("115","DRAGON ENERGY ","DRAGON ENERGY ","0.00","12","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6009694633116","","","inclusive");
INSERT INTO product VALUES("116","FRUITREE MEDITERRANEAN ","FRUITREE MEDITERRANEAN ","0.00","12","default.gif","0","3","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240430323","","","inclusive");
INSERT INTO product VALUES("117","KUMBUCHA GINGER/RAW  ","KUMBUCHA GINGER/RAW  ","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","KUMBUCHA","","","inclusive");
INSERT INTO product VALUES("118","SUNDANCE TROPICAL ","SUNDANCE TROPICAL ","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644991297","","","inclusive");
INSERT INTO product VALUES("119","FANTA ORANGE BOTTLED ","FANTA ORANGE BOTTLED ","0.00","4.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("120","COCA COLA BOTTLED ","COCA COLA BOTTLED ","0.00","4.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("121","DUNHILL RED ","DUNHILL RED ","0.00","35","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001060309823","","","inclusive");
INSERT INTO product VALUES("122","DUNHILL SWITCH ","DUNHILL SWITCH ","0.00","35","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001060164828","","","inclusive");
INSERT INTO product VALUES("123","DUNHILL BLUE ","DUNHILL BLUE ","0.00","35","default.gif","0","127","1","0","0","Non","0000-00-00 00:00:00","1","1","6001060310829","","","inclusive");
INSERT INTO product VALUES("124","PALL MALL","PALL MALL","0.00","23","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009667410270","","","inclusive");
INSERT INTO product VALUES("125","PETER RED ","PETER RED ","0.00","20","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009667410003","","","inclusive");
INSERT INTO product VALUES("126","PETER BLUE ","PETER BLUE ","0.00","25","default.gif","0","25","1","0","0","Non","0000-00-00 00:00:00","1","1","6008720034828","","","inclusive");
INSERT INTO product VALUES("127","WET N WILD ","WET N WILD ","0.00","30","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5011831076893","","","inclusive");
INSERT INTO product VALUES("128","EROTICA RIBBED CONDOMS ","EROTICA RIBBED CONDOMS ","0.00","30","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5011831084218","","","inclusive");
INSERT INTO product VALUES("129","KING SIZE EXTRA LARGE CONDOMS ","KING SIZE EXTRA LARGE CONDOMS ","0.00","30","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5011831079818","","","inclusive");
INSERT INTO product VALUES("130","POWER PLAY ","POWER PLAY ","0.00","30","default.gif","0","5","1","0","0","Non","0000-00-00 00:00:00","1","1","5011831076879","","","inclusive");
INSERT INTO product VALUES("131","ROUGH RIDER STUDDED CONDOMS ","ROUGH RIDER STUDDED CONDOMS ","0.00","30","default.gif","0","17","1","0","0","Non","0000-00-00 00:00:00","1","1","5011831076695","","","inclusive");
INSERT INTO product VALUES("132","FOUR COUSINS NATURAL SWEET RED ","FOUR COUSINS NATURAL SWEET RED ","0.00","75","default.gif","0","12","1","0","0","Non","0000-00-00 00:00:00","1","1","6002269000566","","","inclusive");
INSERT INTO product VALUES("133","FOUR COUSINS SWEET ROSE ","FOUR COUSINS SWEET ROSE ","0.00","75","default.gif","0","39","1","0","0","Non","0000-00-00 00:00:00","1","1","6002269000573","","","inclusive");
INSERT INTO product VALUES("134","CELLAR CASK RED ","CELLAR CASK RED ","0.00","70","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001496301804","","","inclusive");
INSERT INTO product VALUES("135","NAMAQUA CARBENET ","NAMAQUA CARBENET ","0.00","75","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6004442004057","","","inclusive");
INSERT INTO product VALUES("136","NAMAQUA PINOTAGE ","NAMAQUA PINOTAGE ","0.00","75","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6004442003302","","","inclusive");
INSERT INTO product VALUES("137","NAMAQUA MERLOT ","NAMAQUA MERLOT ","0.00","75","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6004442004040","","","inclusive");
INSERT INTO product VALUES("138","NAMAQUA SHIRAZ ","NAMAQUA SHIRAZ ","0.00","75","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6004442003319","","","inclusive");
INSERT INTO product VALUES("139","ROBERT'S ROCK ","ROBERT'S ROCK ","0.00","65","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6002323307136","","","inclusive");
INSERT INTO product VALUES("140","NAMAQUA SMOOTH  DRY RED ","NAMAQUA SMOOTH  DRY RED ","0.00","185","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6004442001179","","","inclusive");
INSERT INTO product VALUES("141","NAMAQUAA SWEET ROSE ","NAMAQUAA SWEET ROSE ","0.00","185","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6004442003371","","","inclusive");
INSERT INTO product VALUES("142","TALL HORSE MERLOT ","TALL HORSE MERLOT ","0.00","87","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","663194000388","","","inclusive");
INSERT INTO product VALUES("143","TALL HORSE PINOTAGE ","TALL HORSE PINOTAGE ","0.00","87","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001506901215","","","inclusive");
INSERT INTO product VALUES("144","TALL HORSE SHIRAZ ","TALL HORSE SHIRAZ ","0.00","87","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001506900263","","","inclusive");
INSERT INTO product VALUES("145","TALL HORSE CHENIN BLANC ","TALL HORSE CHENIN BLANC ","0.00","87","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001506901222","","","inclusive");
INSERT INTO product VALUES("146","NEDERBERG BARONNE ","NEDERBERG BARONNE ","0.00","88","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001452296861","","","inclusive");
INSERT INTO product VALUES("147","GORDON'S DRY GIN","GORDON'S DRY GIN","0.00","85","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001398122699","","","inclusive");
INSERT INTO product VALUES("148","GILBEY'S GIN ","GILBEY'S GIN ","0.00","65","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001398101021","","","inclusive");
INSERT INTO product VALUES("149","KAHLUA COFFEE LIQUOR ","KAHLUA COFFEE LIQUOR ","0.00","165","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","7610594456256","","","inclusive");
INSERT INTO product VALUES("150","MARTINI ","MARTINI ","0.00","142","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5010677914000","","","inclusive");
INSERT INTO product VALUES("151","SOUTHERN COMFORT ","SOUTHERN COMFORT ","0.00","175","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009873018482","","","inclusive");
INSERT INTO product VALUES("152","BEST WHISKY","BEST WHISKY","0.00","65","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009675692576","","","inclusive");
INSERT INTO product VALUES("153","AUTUMN HARVEST ","AUTUMN HARVEST ","0.00","45","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001108036827","","","inclusive");
INSERT INTO product VALUES("154","AMARULA CREAM ","AMARULA CREAM ","0.00","80","default.gif","0","5","1","0","0","Non","0000-00-00 00:00:00","1","1","6001495062478","","","inclusive");
INSERT INTO product VALUES("155","AMARULA  CREAM ","AMARULA  CREAM ","0.00","142","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001495062508","","","inclusive");
INSERT INTO product VALUES("156","FAMOUS GROUSE ","FAMOUS GROUSE ","0.00","137","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5010314750008","","","inclusive");
INSERT INTO product VALUES("157","CLAN MURRAY ","CLAN MURRAY ","0.00","120","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009675690497","","","inclusive");
INSERT INTO product VALUES("158","JACK DANIELS ","JACK DANIELS ","0.00","275","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","82184090466","","","inclusive");
INSERT INTO product VALUES("159","GRANT'S BLENDED SCOTCH WHISKY ","GRANT'S BLENDED SCOTCH WHISKY ","0.00","127","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5010327000046","","","inclusive");
INSERT INTO product VALUES("160","SMIRNOFF VODKA ","SMIRNOFF VODKA ","0.00","60","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001398119545","","","inclusive");
INSERT INTO product VALUES("161","JAGERMEISTER ","JAGERMEISTER ","0.00","185","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","4067700014559","","","inclusive");
INSERT INTO product VALUES("162","BELL'S WHISKY","BELL'S WHISKY","0.00","150","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5000387905719","","","inclusive");
INSERT INTO product VALUES("163","CELLAR CASK RED ","CELLAR CASK RED ","0.00","230","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001496301842","","","inclusive");
INSERT INTO product VALUES("164","4TH STREET SWEET RED WINE ","4TH STREET SWEET RED WINE ","0.00","180","default.gif","0","12","1","0","0","Non","0000-00-00 00:00:00","1","1","6001108059178","","","inclusive");
INSERT INTO product VALUES("165","NAMAQUA SMOOTH DRY RED","NAMAQUA SMOOTH DRY RED","0.00","205","default.gif","0","13","1","0","0","Non","0000-00-00 00:00:00","1","1","6004442001506","","","inclusive");
INSERT INTO product VALUES("166","NAMAQUA SWEET ROSE ","NAMAQUA SWEET ROSE ","0.00","190","default.gif","0","19","1","0","0","Non","0000-00-00 00:00:00","1","1","6004442003388","","","inclusive");
INSERT INTO product VALUES("167","GLENFIDDICH SINGLE MALT  12 YEARS ","GLENFIDDICH SINGLE MALT  12 YEARS ","0.00","345","default.gif","0","16","1","0","0","Non","0000-00-00 00:00:00","1","1","5010327000176","","","inclusive");
INSERT INTO product VALUES("168","GLENFIDDICH SINGLE MALT 18 YEARS ","GLENFIDDICH SINGLE MALT 18 YEARS ","0.00","830","default.gif","0","23","1","0","0","Non","0000-00-00 00:00:00","1","1","5010327325132","","","inclusive");
INSERT INTO product VALUES("169","CAPTAIN MORGAN  SPICED GOLD ","CAPTAIN MORGAN  SPICED GOLD ","0.00","142","default.gif","0","78","1","0","0","Non","0000-00-00 00:00:00","1","1","5000299223031","","","inclusive");
INSERT INTO product VALUES("170"," BLACK LABEL 12  YEARS "," BLACK LABEL 12  YEARS ","0.00","315","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5000267024004","","","inclusive");
INSERT INTO product VALUES("171","CHIVAS REGAL ","CHIVAS REGAL ","0.00","320","default.gif","0","70","1","0","0","Non","0000-00-00 00:00:00","1","1","80432400395","","","inclusive");
INSERT INTO product VALUES("172","OLMECA TEQUILA ","OLMECA TEQUILA ","0.00","203","default.gif","0","22","1","0","0","Non","0000-00-00 00:00:00","1","1","80432101865","","","inclusive");
INSERT INTO product VALUES("173","JOSE CUERVO SILVER TEQUILA","JOSE CUERVO SILVER TEQUILA","0.00","203","default.gif","0","43","1","0","0","Non","0000-00-00 00:00:00","1","1","7501035042322","","","inclusive");
INSERT INTO product VALUES("174","WILD AFRICA CREAM ","WILD AFRICA CREAM ","0.00","130","default.gif","0","43","1","0","0","Non","0000-00-00 00:00:00","1","1","6009600220027","","","inclusive");
INSERT INTO product VALUES("175","RICHELIEU","RICHELIEU","0.00","90","default.gif","0","9","1","0","0","Non","0000-00-00 00:00:00","1","1","6001495001088","","","inclusive");
INSERT INTO product VALUES("176","KWV 3YEARS","KWV 3YEARS","0.00","93","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6002323800163","","","inclusive");
INSERT INTO product VALUES("177","KLIPDRIFT","KLIPDRIFT","0.00","115","default.gif","0","44","1","0","0","Non","0000-00-00 00:00:00","1","1","6001496010577","","","inclusive");
INSERT INTO product VALUES("178","KWV CHENIN BLANC","KWV CHENIN BLANC","0.00","80","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6002323410737","","","inclusive");
INSERT INTO product VALUES("179","NAMAQUA BLANC 750ML","NAMAQUA BLANC 750ML","0.00","75","default.gif","0","43","1","0","0","Non","0000-00-00 00:00:00","1","1","6004442002749","","","inclusive");
INSERT INTO product VALUES("180","KWV CHARDONNAY","KWV CHARDONNAY","0.00","80","default.gif","0","49","1","0","0","Non","0000-00-00 00:00:00","1","1","6002323414230","","","inclusive");
INSERT INTO product VALUES("181","KWV 5YEARS","KWV 5YEARS","0.00","100","default.gif","0","70","1","0","0","Non","0000-00-00 00:00:00","1","1","6002323800231","","","inclusive");
INSERT INTO product VALUES("182","J.C.LE ROUX LE DOMAINE","J.C.LE ROUX LE DOMAINE","0.00","110","default.gif","0","77","1","0","0","Non","0000-00-00 00:00:00","1","1","6001497112010","","","inclusive");
INSERT INTO product VALUES("183","J.C.LE ROUX LA CHANSON","J.C.LE ROUX LA CHANSON","0.00","110","default.gif","0","16","1","0","0","Non","0000-00-00 00:00:00","1","1","6001497114007","","","inclusive");
INSERT INTO product VALUES("184","4TH STREET SWEET WHITE","4TH STREET SWEET WHITE","0.00","70","default.gif","0","27","1","0","0","Non","0000-00-00 00:00:00","1","1","6001108049599","","","inclusive");
INSERT INTO product VALUES("185","BRANDY 10YEARS","BRANDY 10YEARS","0.00","165","default.gif","0","17","1","0","0","Non","0000-00-00 00:00:00","1","1","6002323801337","","","inclusive");
INSERT INTO product VALUES("186","BREAD","BREAD","0.00","12","default.gif","0","72","1","0","0","Non","0000-00-00 00:00:00","1","1","BREAD","","","inclusive");
INSERT INTO product VALUES("187","BEEF BILTONG","BEEF BILTONG","0.00","20","default.gif","0","14","1","0","0","Non","0000-00-00 00:00:00","1","1","9506000060231","","","inclusive");
INSERT INTO product VALUES("188","CRACKERS","CRACKERS","0.00","4","default.gif","0","16","1","0","0","Non","0000-00-00 00:00:00","1","1","CRACKER","","","inclusive");
INSERT INTO product VALUES("189","HALLS XTRA STRONG","HALLS XTRA STRONG","0.00","13","default.gif","0","80","1","0","0","Non","0000-00-00 00:00:00","1","1","60025272","","","inclusive");
INSERT INTO product VALUES("190","HALLS FRUIT EXPLOSION","HALLS FRUIT EXPLOSION","0.00","13","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","60026125","","","inclusive");
INSERT INTO product VALUES("191","HALLS CHERRY FLAVOURED","HALLS CHERRY FLAVOURED","0.00","13","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","60025265","","","inclusive");
INSERT INTO product VALUES("192","SCONES","SCONES","0.00","2","default.gif","0","32","1","0","0","Non","0000-00-00 00:00:00","1","1","SCONES","","","inclusive");
INSERT INTO product VALUES("193","LOLIPOPS","LOLIPOPS","0.00","1","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","LOLI","","","inclusive");
INSERT INTO product VALUES("194","ICE CUBES","ICE CUBES","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","ICE","","","inclusive");
INSERT INTO product VALUES("195","CURAMOL","CURAMOL","0.00","2","default.gif","0","33","1","0","0","Non","0000-00-00 00:00:00","1","1","CURAMOL","","","inclusive");
INSERT INTO product VALUES("196","PENS","PENS","0.00","2","default.gif","0","102","1","0","0","Non","0000-00-00 00:00:00","1","1","PEN","","","inclusive");
INSERT INTO product VALUES("197","PEANUTS","PEANUTS","0.00","5","default.gif","0","50","1","0","0","Non","0000-00-00 00:00:00","1","1","JR","","","inclusive");
INSERT INTO product VALUES("198","SUPERSHAKE BANANA & STRAWBERRY ","SUPERSHAKE BANANA & STRAWBERRY ","0.00","4","default.gif","0","78","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644923854","","","inclusive");
INSERT INTO product VALUES("199","APPY PINE ","APPY PINE ","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644993284","","","inclusive");
INSERT INTO product VALUES("200","APPY APPLE","APPY APPLE","0.00","5","default.gif","0","7","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644991778","","","inclusive");
INSERT INTO product VALUES("201","WILDCAT ENERGY","WILDCAT ENERGY","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644991198","","","inclusive");
INSERT INTO product VALUES("202","CREAMBELL MAHEU STRAWBERRY ","CREAMBELL MAHEU STRAWBERRY ","0.00","7","default.gif","0","35","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672845","","","inclusive");
INSERT INTO product VALUES("203","LACTO MABISI ","LACTO MABISI ","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672159","","","inclusive");
INSERT INTO product VALUES("204","PEPSI LITE ","PEPSI LITE ","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009681261148","","","inclusive");
INSERT INTO product VALUES("205","7 UP","7 UP","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009681261254","","","inclusive");
INSERT INTO product VALUES("206","PLASTIC CUPS ","PLASTIC CUPS ","0.00","2","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","CUPS","","","inclusive");
INSERT INTO product VALUES("207","MILK IT VANILLA FLAVOURED","MILK IT VANILLA FLAVOURED","0.00","12","default.gif","0","6","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644997176","","","inclusive");
INSERT INTO product VALUES("208","MILK IT STRAWBERRY FLAVOURED","MILK IT STRAWBERRY FLAVOURED","0.00","12","default.gif","0","45","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644997169","","","inclusive");
INSERT INTO product VALUES("209","MILK IT BANANA FLAVOURED","MILK IT BANANA FLAVOURED","0.00","12","default.gif","0","19","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644997145","","","inclusive");
INSERT INTO product VALUES("210","MILKIT CHOCOLATE FLAVOURED ","MILKIT CHOCOLATE FLAVOURED ","0.00","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644997152","","","inclusive");
INSERT INTO product VALUES("211","MILKIT CHOCOLATE FLAVOURED","MILKIT CHOCOLATE FLAVOURED","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644997091","","","inclusive");
INSERT INTO product VALUES("212","SHAKE N SIP BANANA & STRAWBERRY ","SHAKE N SIP BANANA & STRAWBERRY ","4.85","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537015765","","","inclusive");
INSERT INTO product VALUES("213","MILKIT VANILLA FLAVOURED ","MILKIT VANILLA FLAVOURED ","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644997114","","","inclusive");
INSERT INTO product VALUES("214","EAT-EM-ALL BISCUITS","EAT-EM-ALL BISCUITS","0.00","11","default.gif","0","100","1","0","0","Non","0000-00-00 00:00:00","1","1","6291103082503","","","inclusive");
INSERT INTO product VALUES("215","MUKAKA BISCUITS ","MUKAKA BISCUITS ","0.00","3","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6009630400819","","","inclusive");
INSERT INTO product VALUES("216","MILKY MOO ","MILKY MOO ","0.00","3","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537013457","","","inclusive");
INSERT INTO product VALUES("217","AMA SIPSIP BANANA FLAVOUR","AMA SIPSIP BANANA FLAVOUR","0.00","7","default.gif","0","23","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537019435","","","inclusive");
INSERT INTO product VALUES("218","AMA SIPSIP STRAWBERRY ","AMA SIPSIP STRAWBERRY ","0.00","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537019428","","","inclusive");
INSERT INTO product VALUES("219","AMA SIPSIP TRADITIONAL MAIZE MAHEU ","AMA SIPSIP TRADITIONAL MAIZE MAHEU ","0.00","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537021599","","","inclusive");
INSERT INTO product VALUES("220","SHAKE N SIP MIXED BERRY ","SHAKE N SIP MIXED BERRY ","4.85","7","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644999118","","","inclusive");
INSERT INTO product VALUES("221","CREAMBOS CUSTARD FLAVOUR ","CREAMBOS CUSTARD FLAVOUR ","0.00","4","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644927760","","","inclusive");
INSERT INTO product VALUES("222","CREAMBOS COCONUT FLAVOURED","CREAMBOS COCONUT FLAVOURED","0.00","4","default.gif","0","33","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644927746","","","inclusive");
INSERT INTO product VALUES("223","FLAMINGO TANGOPINA","FLAMINGO TANGOPINA","0.00","5","default.gif","0","5","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644922949","","","inclusive");
INSERT INTO product VALUES("224","TINGLING GINGER BEER ","TINGLING GINGER BEER ","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652782900","","","inclusive");
INSERT INTO product VALUES("225","FROOTY  ","FROOTY  ","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652782962","","","inclusive");
INSERT INTO product VALUES("226","MONSTA CREAM VANILLA FLAVOUR ","MONSTA CREAM VANILLA FLAVOUR ","0.00","4","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644999651","","","inclusive");
INSERT INTO product VALUES("227","MONSTA CREAM STRAWBERRY FLAVOUR ","MONSTA CREAM STRAWBERRY FLAVOUR ","0.00","4","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644999644","","","inclusive");
INSERT INTO product VALUES("228","YOYO CHIPSY ORIENTAL SWEET CHILLI ","YOYO CHIPSY ORIENTAL SWEET CHILLI ","0.00","5","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644925445","","","inclusive");
INSERT INTO product VALUES("229","YOYO CHIPSY FRENCH ONION & CHEDDAR ","YOYO CHIPSY FRENCH ONION & CHEDDAR ","0.00","5","default.gif","0","14","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644925414","","","inclusive");
INSERT INTO product VALUES("230","YOYO CHIPSY SPICY CHICKEN TIKKA ","YOYO CHIPSY SPICY CHICKEN TIKKA ","0.00","5","default.gif","0","15","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644925407","","","inclusive");
INSERT INTO product VALUES("231","YOYO CHIPSY CARIBBEAN ONION ","YOYO CHIPSY CARIBBEAN ONION ","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537016625","","","inclusive");
INSERT INTO product VALUES("232","JUST GRANADILLA ","JUST GRANADILLA ","16.83","28","default.gif","0","15","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652782573","","","inclusive");
INSERT INTO product VALUES("233","JUST PINEAPPLE","JUST PINEAPPLE","16.83","28","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652782566","","","inclusive");
INSERT INTO product VALUES("234","BUBBLEGUM ROLLS ","BUBBLEGUM ROLLS ","0.00","1","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","BUBBLE ","","","inclusive");
INSERT INTO product VALUES("235","MIRINDA ORANGE","MIRINDA ORANGE","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009681267140","","","inclusive");
INSERT INTO product VALUES("236","SAUSAGE ","SAUSAGE ","0.00","25","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","SAUSAGE","","","inclusive");
INSERT INTO product VALUES("237","MEALI MEAL 10KG","MEALI MEAL 10KG","0.00","58","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","MMEAL10","","","inclusive");
INSERT INTO product VALUES("238","MEALIE MEAL ","MEALIE MEAL ","0.00","120","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","MMEAL25","","","inclusive");
INSERT INTO product VALUES("239","YOGHURT BANANA 250G","YOGHURT BANANA 250G","0.00","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672272","","","inclusive");
INSERT INTO product VALUES("240","CREAMBELL YORGHUT RASPBERRY ","CREAMBELL YORGHUT RASPBERRY ","0.00","7","default.gif","0","22","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672234","","","inclusive");
INSERT INTO product VALUES("241","CREAMBELL THICK PINEAPPLE YORGHUT ","CREAMBELL THICK PINEAPPLE YORGHUT ","0.00","3.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672357","","","inclusive");
INSERT INTO product VALUES("242","MEALI MEAL 10KG","MEALI MEAL 10KG","0.00","60","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","MEAL","","","inclusive");
INSERT INTO product VALUES("243","ABSOLUTE VODKA","ABSOLUTE VODKA","0.00","195","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","7312040017010","","","inclusive");
INSERT INTO product VALUES("244","ABSOLUT VANILIA","ABSOLUT VANILIA","0.00","195","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","7312040060757","","","inclusive");
INSERT INTO product VALUES("245","ABSOLUT RASPBERRI","ABSOLUT RASPBERRI","0.00","195","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","7312040040759","","","inclusive");
INSERT INTO product VALUES("246","STRAWBERRY LIPS","STRAWBERRY LIPS","0.00","142","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001812942001","","","inclusive");
INSERT INTO product VALUES("247","BACARDI CARTA BLANCA","BACARDI CARTA BLANCA","0.00","142","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5010677015615","","","inclusive");
INSERT INTO product VALUES("248","ZAPPA BLUE","ZAPPA BLUE","0.00","165","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001812500027","","","inclusive");
INSERT INTO product VALUES("249","ZAPPA RED","ZAPPA RED","0.00","165","default.gif","0","47","1","0","0","Non","0000-00-00 00:00:00","1","1","6001812500034","","","inclusive");
INSERT INTO product VALUES("250","KWV CABERNET SAUVIGNON","KWV CABERNET SAUVIGNON","0.00","95","default.gif","0","48","1","0","0","Non","0000-00-00 00:00:00","1","1","6002323400332","","","inclusive");
INSERT INTO product VALUES("251","4THSTREET SWEET RED","4THSTREET SWEET RED","0.00","70","default.gif","0","45","1","0","0","Non","0000-00-00 00:00:00","1","1","6001108049582","","","inclusive");
INSERT INTO product VALUES("252","MALIBU","MALIBU","0.00","175","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6001812013626","","","inclusive");
INSERT INTO product VALUES("253","KWV PINOTAGE","KWV PINOTAGE","0.00","95","default.gif","0","47","1","0","0","Non","0000-00-00 00:00:00","1","1","6002323400530","","","inclusive");
INSERT INTO product VALUES("254","KWV SHIRAZ","KWV SHIRAZ","0.00","95","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6002323400639","","","inclusive");
INSERT INTO product VALUES("255","PEPSI BOTTLED","PEPSI BOTTLED","0.00","4.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","PEPSI","","","inclusive");
INSERT INTO product VALUES("256","MOUNTAIN DEW BOTTLED ","MOUNTAIN DEW BOTTLED ","0.00","4.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","MDEW","","","inclusive");
INSERT INTO product VALUES("257","PEPSI","PEPSI","0.00","8","default.gif","0","50","1","0","0","Non","0000-00-00 00:00:00","1","1","6009681260196","","","inclusive");
INSERT INTO product VALUES("258","MOUNTAIN DEW","MOUNTAIN DEW","0.00","8","default.gif","0","38","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803227489","","","inclusive");
INSERT INTO product VALUES("259","WHITESPOON SUGAR ","WHITESPOON SUGAR ","0.00","39","default.gif","0","46","1","0","0","Non","0000-00-00 00:00:00","1","1","6009670892032","","","inclusive");
INSERT INTO product VALUES("260","WHITESPOON SUGAR","WHITESPOON SUGAR","0.00","19.5","default.gif","0","50","1","0","0","Non","0000-00-00 00:00:00","1","1","6009670892025","","","inclusive");
INSERT INTO product VALUES("261","APPLE MAX ","APPLE MAX ","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652782931","","","inclusive");
INSERT INTO product VALUES("262","GRAPE MAX","GRAPE MAX","0.00","5","default.gif","0","37","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652782993","","","inclusive");
INSERT INTO product VALUES("263","HUBBLY BUBBLY","HUBBLY BUBBLY","0.00","5","default.gif","0","30","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652782955","","","inclusive");
INSERT INTO product VALUES("264","SWIFT ORANGE","SWIFT ORANGE","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652782948","","","inclusive");
INSERT INTO product VALUES("265","SWIFT LIMON ","SWIFT LIMON ","0.00","5","default.gif","0","43","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652782979","","","inclusive");
INSERT INTO product VALUES("266","FRUITREE TROPICAL","FRUITREE TROPICAL","7.27","12","default.gif","0","16","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240430309","","","inclusive");
INSERT INTO product VALUES("267","FRUITREE RED GRAPE ","FRUITREE RED GRAPE ","0.00","12","default.gif","0","39","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240225158","","","inclusive");
INSERT INTO product VALUES("268","THIRSTY STRAWBERRY ","THIRSTY STRAWBERRY ","16.08","26","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009650800057","","","inclusive");
INSERT INTO product VALUES("269","PEACEFUL SLEEP MOSQUITO REPELLANT","PEACEFUL SLEEP MOSQUITO REPELLANT","0.00","48","default.gif","0","17","1","0","0","Non","0000-00-00 00:00:00","1","1","6001038358105","","","inclusive");
INSERT INTO product VALUES("270","AMA SIPSIP  CHOCOLATE FLAVOUR ","AMA SIPSIP  CHOCOLATE FLAVOUR ","0.00","7","default.gif","0","87","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537019374","","","inclusive");
INSERT INTO product VALUES("271","AMA SIPSIP VANILLA","AMA SIPSIP VANILLA","0.00","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537019398","","","inclusive");
INSERT INTO product VALUES("272","AMIGO CHEESE CURLS ","AMIGO CHEESE CURLS ","0.00","12","default.gif","0","50","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652780296","","","inclusive");
INSERT INTO product VALUES("273","AMIGO CHEESE CURLS ","AMIGO CHEESE CURLS ","0.00","2.5","default.gif","0","50","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652780289","","","inclusive");
INSERT INTO product VALUES("274","RICOFFY ","RICOFFY ","0.00","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009188000349","","","inclusive");
INSERT INTO product VALUES("275","BULL BRAND CORNED MEAT","BULL BRAND CORNED MEAT","0.00","21","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001330002034","","","inclusive");
INSERT INTO product VALUES("276","CERES MEDLEY FRUITS ","CERES MEDLEY FRUITS ","0.00","23","default.gif","0","12","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240100660","","","inclusive");
INSERT INTO product VALUES("277","CERES ORANGE ","CERES ORANGE ","0.00","23","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240100059","","","inclusive");
INSERT INTO product VALUES("278","LAYS THAI SWEET CHILLI","LAYS THAI SWEET CHILLI","0.00","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510804607","","","inclusive");
INSERT INTO product VALUES("279","LAYS SPRING ONION ","LAYS SPRING ONION ","0.00","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510804591","","","inclusive");
INSERT INTO product VALUES("280","LAYS SOUR CREAM ","LAYS SOUR CREAM ","9.98","14","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510806175","","","inclusive");
INSERT INTO product VALUES("281","LIQUIFRUIT BERRYBLAZE","LIQUIFRUIT BERRYBLAZE","0.00","23","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240500460","","","inclusive");
INSERT INTO product VALUES("282","LIQUIFRUIT ORANGE ","LIQUIFRUIT ORANGE ","0.00","23","default.gif","0","3","1","0","0","Non","0000-00-00 00:00:00","1","1","6001048000315","","","inclusive");
INSERT INTO product VALUES("283","RIVONNIA TOMATO SAUCE ","RIVONNIA TOMATO SAUCE ","0.00","12","default.gif","0","5","1","0","0","Non","0000-00-00 00:00:00","1","1","6009608440069","","","inclusive");
INSERT INTO product VALUES("284","MILO MALT","MILO MALT","0.00","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001068370108","","","inclusive");
INSERT INTO product VALUES("285","ANYTIME PEANUT BUTTER","ANYTIME PEANUT BUTTER","0.00","16","default.gif","0","10","1","0","0","Non","0000-00-00 00:00:00","1","1","PEANUT BUTTER","","","inclusive");
INSERT INTO product VALUES("286","DETTOL LIQUID ","DETTOL LIQUID ","0.00","38","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","60050281","","","inclusive");
INSERT INTO product VALUES("287","LAYS THAI CHILLI","LAYS THAI CHILLI","0.00","14","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510807462","","","inclusive");
INSERT INTO product VALUES("288","LAYS SPRING ONION","LAYS SPRING ONION","0.00","14","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510806182","","","inclusive");
INSERT INTO product VALUES("289","LAYS CARRIBEAN ONION","LAYS CARRIBEAN ONION","0.00","14","default.gif","0","4","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510806151","","","inclusive");
INSERT INTO product VALUES("290","COCACOLA ","COCACOLA ","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","54491472","","","inclusive");
INSERT INTO product VALUES("291","FANTA GRAPE","FANTA GRAPE","0.00","10","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","87126815","","","inclusive");
INSERT INTO product VALUES("292","FANTA PINEAPPLE ","FANTA PINEAPPLE ","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","50112203","","","inclusive");
INSERT INTO product VALUES("293","FANTA ORANGE","FANTA ORANGE","0.00","10","default.gif","0","7","1","0","0","Non","0000-00-00 00:00:00","1","1","40822938","","","inclusive");
INSERT INTO product VALUES("294","SPARLETTA","SPARLETTA","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5449000120311","","","inclusive");
INSERT INTO product VALUES("295","STONEY GINGER BEER ","STONEY GINGER BEER ","0.00","7","default.gif","0","8","1","0","0","Non","0000-00-00 00:00:00","1","1","5449000103079","","","inclusive");
INSERT INTO product VALUES("296","SUNDANCE PINEAPPLE","SUNDANCE PINEAPPLE","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644991624","","","inclusive");
INSERT INTO product VALUES("297","LIFEBUOY TOTAL 10","LIFEBUOY TOTAL 10","0.00","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087358613","","","inclusive");
INSERT INTO product VALUES("298","LIFEBUOY LEMON FRESH ","LIFEBUOY LEMON FRESH ","0.00","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087358651","","","inclusive");
INSERT INTO product VALUES("299","EET SUM MOR","EET SUM MOR","12.08","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001056201001","","","inclusive");
INSERT INTO product VALUES("300","LAYS CARIBEAN ONION","LAYS CARIBEAN ONION","0.00","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510804560","","","inclusive");
INSERT INTO product VALUES("301","LAYS SALT & VINEGAR ","LAYS SALT & VINEGAR ","0.00","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510804775","","","inclusive");
INSERT INTO product VALUES("302","DORITOS CHEESE  SUPREME","DORITOS CHEESE  SUPREME","0.00","5","default.gif","0","24","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510802559","","","inclusive");
INSERT INTO product VALUES("303","POPICO BUTTER SALT","POPICO BUTTER SALT","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652784140","","","inclusive");
INSERT INTO product VALUES("304"," CADBURY DAIRY MILK TOP DECK "," CADBURY DAIRY MILK TOP DECK ","10.21","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001065601090","","","inclusive");
INSERT INTO product VALUES("305","ROMANY CREAM MINT CHOC ","ROMANY CREAM MINT CHOC ","0.00","28.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001056004619","","","inclusive");
INSERT INTO product VALUES("306","ROMANY CREAM CHOC FUDGE","ROMANY CREAM CHOC FUDGE","0.00","28.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001056412551","","","inclusive");
INSERT INTO product VALUES("307","POPICO SOUR CREAM ","POPICO SOUR CREAM ","0.00","5","default.gif","0","9","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652784157","","","inclusive");
INSERT INTO product VALUES("308","POPICO CHEESE FLAVOUR ","POPICO CHEESE FLAVOUR ","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652784133","","","inclusive");
INSERT INTO product VALUES("309","ROMANY CREAM CLASSIC CHOC ","ROMANY CREAM CLASSIC CHOC ","0.00","28.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001125001877","","","inclusive");
INSERT INTO product VALUES("310","TENNIS CLASSIC BISCUITS ","TENNIS CLASSIC BISCUITS ","0.00","15","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6001056662000","","","inclusive");
INSERT INTO product VALUES("311","KATUNJILA SALT ","KATUNJILA SALT ","0.00","20","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009681260165","","","inclusive");
INSERT INTO product VALUES("312","DUNHILL WHITE ","DUNHILL WHITE ","0.00","35","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001060311321","","","inclusive");
INSERT INTO product VALUES("313","BLUE BAND ORIGINAL ","BLUE BAND ORIGINAL ","0.00","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6161100600157","","","inclusive");
INSERT INTO product VALUES("314","BEST MARULA FRUIT CREAM","BEST MARULA FRUIT CREAM","0.00","103","default.gif","0","14","1","0","0","Non","0000-00-00 00:00:00","1","1","6009675692422","","","inclusive");
INSERT INTO product VALUES("315","J & B  WHISKY ","J & B  WHISKY ","0.00","165","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5010103800303","","","inclusive");
INSERT INTO product VALUES("316","GLENFIDDICH SINGLE MALT 15 YEARS","GLENFIDDICH SINGLE MALT 15 YEARS","0.00","510","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5010327325125","","","inclusive");
INSERT INTO product VALUES("317","PARMALAT LONG LIFE MILK","PARMALAT LONG LIFE MILK","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009683610517","","","inclusive");
INSERT INTO product VALUES("318","CREAMBELL FLAVOURD MILK CHOCOLATE","CREAMBELL FLAVOURD MILK CHOCOLATE","4.17","6","default.gif","0","8","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672111","","","inclusive");
INSERT INTO product VALUES("319","CREAMBELL YORGHUT BANANA","CREAMBELL YORGHUT BANANA","0.00","15","default.gif","0","36","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672265","","","inclusive");
INSERT INTO product VALUES("320","KATUNGILA SALT","KATUNGILA SALT","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("321","CREAMBEL MILK MAHEU","CREAMBEL MILK MAHEU","0.00","5.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("322","TEQUILA GOLD JOSE CUERVO","TEQUILA GOLD JOSE CUERVO","0.00","203","default.gif","0","32","1","0","0","Non","0000-00-00 00:00:00","1","1","7501035042148","","","inclusive");
INSERT INTO product VALUES("323","JUICY TROPICAL ","JUICY TROPICAL ","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6161109781062","","","inclusive");
INSERT INTO product VALUES("324","FLAVOURS CONDOMS ","FLAVOURS CONDOMS ","0.00","30","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5011831087196","","","inclusive");
INSERT INTO product VALUES("325","BAREBACK CONDOMS","BAREBACK CONDOMS","0.00","30","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5011831076855","","","inclusive");
INSERT INTO product VALUES("326","FANTASY CONDOMS ","FANTASY CONDOMS ","0.00","20","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5011831091773","","","inclusive");
INSERT INTO product VALUES("327","SENSITIVE CONDOMS","SENSITIVE CONDOMS","0.00","20","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5011831091865","","","inclusive");
INSERT INTO product VALUES("328","RED LABEL","RED LABEL","0.00","150","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5000267014005","","","inclusive");
INSERT INTO product VALUES("329","JAMESON WHISKEY","JAMESON WHISKEY","0.00","200","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5011007003029","","","inclusive");
INSERT INTO product VALUES("330","SMIRNOFF 1818 VODKA","SMIRNOFF 1818 VODKA","0.00","75","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001398130311","","","inclusive");
INSERT INTO product VALUES("331","FOUR COUSINS NATURAL SWEET WHITE","FOUR COUSINS NATURAL SWEET WHITE","0.00","75","default.gif","0","32","1","0","0","Non","0000-00-00 00:00:00","1","1","6002269000559","","","inclusive");
INSERT INTO product VALUES("332","FLAVOURED  MILK  STRAWBERRY","FLAVOURED  MILK  STRAWBERRY","0.00","3.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672104","","","inclusive");
INSERT INTO product VALUES("333","KLIPDRIFT PREMIUM","KLIPDRIFT PREMIUM","0.00","155","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001496010010","","","inclusive");
INSERT INTO product VALUES("334","SPRITE ","SPRITE ","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","54491069","","","inclusive");
INSERT INTO product VALUES("335","COKE ZERO ","COKE ZERO ","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5449000131836","","","inclusive");
INSERT INTO product VALUES("336","OVERMEER STEIN","OVERMEER STEIN","0.00","170","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001452061001","","","inclusive");
INSERT INTO product VALUES("337","CREAMBELL THICK STRAWBERRY YORGHUT ","CREAMBELL THICK STRAWBERRY YORGHUT ","0.00","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672326","","","inclusive");
INSERT INTO product VALUES("338","CREAMBELL STRAWBERRY MAHEU ","CREAMBELL STRAWBERRY MAHEU ","0.00","3.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672876","","","inclusive");
INSERT INTO product VALUES("339","CREAMBELL BANANA MAHEU ","CREAMBELL BANANA MAHEU ","0.00","3.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672869","","","inclusive");
INSERT INTO product VALUES("340","CABANA PINEAPPLE","CABANA PINEAPPLE","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009683612016","","","inclusive");
INSERT INTO product VALUES("341","CABANA TROPICAL","CABANA TROPICAL","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009683612023","","","inclusive");
INSERT INTO product VALUES("342","CABANA ORANGE","CABANA ORANGE","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009683612009","","","inclusive");
INSERT INTO product VALUES("343","RED BULL ","RED BULL ","0.00","22","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","9002490100070","","","inclusive");
INSERT INTO product VALUES("344","CHEEKY PERI PERI SAUCE ","CHEEKY PERI PERI SAUCE ","0.00","17","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6004809000043","","","inclusive");
INSERT INTO product VALUES("345","LOLLIPOPS ","LOLLIPOPS ","0.00","1","default.gif","0","4","1","0","0","Non","0000-00-00 00:00:00","1","1","LOLLI","","","inclusive");
INSERT INTO product VALUES("346","ABSOLUT KURANT","ABSOLUT KURANT","0.00","195","default.gif","0","3","1","0","0","Non","0000-00-00 00:00:00","1","1","7312040020751","","","inclusive");
INSERT INTO product VALUES("347","PALL MALL RED","PALL MALL RED","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6008165401117","","","inclusive");
INSERT INTO product VALUES("348","BEST WHISKY 200ML","BEST WHISKY 200ML","0.00","25","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009675693184","","","inclusive");
INSERT INTO product VALUES("349","CLAN MAC GREGOR","CLAN MAC GREGOR","0.00","120","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","5010327904214","","","inclusive");
INSERT INTO product VALUES("350","MONGU RICE ","MONGU RICE ","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","RICE","","","inclusive");
INSERT INTO product VALUES("351","OVERMEER GRAND CRU","OVERMEER GRAND CRU","0.00","190","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001452058001","","","inclusive");
INSERT INTO product VALUES("352","LUX WAKE ME UP ","LUX WAKE ME UP ","8.25","12","default.gif","0","39","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087361316","","","inclusive");
INSERT INTO product VALUES("353","DETTOL ACTIVE HYGIENE ","DETTOL ACTIVE HYGIENE ","7.79","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001106123895","","","inclusive");
INSERT INTO product VALUES("354","DETTOL SENSITIVE ","DETTOL SENSITIVE ","7.79","11","default.gif","0","682","1","0","0","Non","0000-00-00 00:00:00","1","1","6001106123871","","","inclusive");
INSERT INTO product VALUES("355","DETTOL NATURAL CARING ","DETTOL NATURAL CARING ","7.79","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001106123819","","","inclusive");
INSERT INTO product VALUES("356","COLGATE TRIPLE ACTION ","COLGATE TRIPLE ACTION ","8.91","12.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6920354814891","","","inclusive");
INSERT INTO product VALUES("357","TOPPERS VANILLA","TOPPERS VANILLA","5.00","9","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001056412933","","","inclusive");
INSERT INTO product VALUES("358","TOPPERS RASPBERRY ","TOPPERS RASPBERRY ","5.00","9","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001056412926","","","inclusive");
INSERT INTO product VALUES("359","TOPPERS CHOCOLATE","TOPPERS CHOCOLATE","5.00","9","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001056412919","","","inclusive");
INSERT INTO product VALUES("360","SIMBA CREAMY CHEDDAR ","SIMBA CREAMY CHEDDAR ","9.35","16","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510800043","","","inclusive");
INSERT INTO product VALUES("361","SIMBA MRS H.S. BALLS ","SIMBA MRS H.S. BALLS ","9.35","16","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001069201944","","","inclusive");
INSERT INTO product VALUES("362","SIMBA CREAMY CHEDDAR ","SIMBA CREAMY CHEDDAR ","3.11","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510804324","","","inclusive");
INSERT INTO product VALUES("363","SIMBA MHS BALLS ","SIMBA MHS BALLS ","3.11","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510804355","","","inclusive");
INSERT INTO product VALUES("364","YESS PINEAPPLE","YESS PINEAPPLE","2.50","5","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644921027","","","inclusive");
INSERT INTO product VALUES("365","YESS FRUIT PUNCH ","YESS FRUIT PUNCH ","2.50","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644921003","","","inclusive");
INSERT INTO product VALUES("366","ROMANY CREAMS CAPPUCCINO ","ROMANY CREAMS CAPPUCCINO ","16.63","28.5","default.gif","0","5","1","0","0","Non","0000-00-00 00:00:00","1","1","6009704171263","","","inclusive");
INSERT INTO product VALUES("367","DAIRY MILK CHOCOLATE ","DAIRY MILK CHOCOLATE ","10.21","18","default.gif","0","3","1","0","0","Non","0000-00-00 00:00:00","1","1","6001065601069","","","inclusive");
INSERT INTO product VALUES("368","MAZOE RASPBERRY","MAZOE RASPBERRY","21.58","35","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5449000244208","","","inclusive");
INSERT INTO product VALUES("369","FOUR COUSINS NATURAL SWEET RED ","FOUR COUSINS NATURAL SWEET RED ","0.00","130","default.gif","0","3","1","0","0","Non","0000-00-00 00:00:00","1","1","6002269000542","","","inclusive");
INSERT INTO product VALUES("370","ENDUARANCE DELAY","ENDUARANCE DELAY","0.00","30","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5011831079856","","","inclusive");
INSERT INTO product VALUES("371","SENSATION LIFESTYLES","SENSATION LIFESTYLES","0.00","20","default.gif","0","42","1","0","0","Non","0000-00-00 00:00:00","1","1","5011831091834","","","inclusive");
INSERT INTO product VALUES("372","CERES MANGO ","CERES MANGO ","14.96","23","default.gif","0","11","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240100257","","","inclusive");
INSERT INTO product VALUES("373","CERES APPLE ","CERES APPLE ","14.96","23","default.gif","0","63","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240100011","","","inclusive");
INSERT INTO product VALUES("374","SIMBA CHEESE & ONION","SIMBA CHEESE & ONION","9.35","16","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6001069201920","","","inclusive");
INSERT INTO product VALUES("375","SIMBA MEXICAN CHILLI ","SIMBA MEXICAN CHILLI ","9.35","16","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001069201937","","","inclusive");
INSERT INTO product VALUES("376","DORITOS SWEET CHLLI PEPPER 150G","DORITOS SWEET CHLLI PEPPER 150G","0.00","14","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510802573","","","inclusive");
INSERT INTO product VALUES("377","DORITOS CHEESE SUPREME 150G","DORITOS CHEESE SUPREME 150G","0.00","14","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510802580","","","inclusive");
INSERT INTO product VALUES("378","LUCKY STAR SARDINES ","LUCKY STAR SARDINES ","15.38","21.5","default.gif","0","4","1","0","0","Non","0000-00-00 00:00:00","1","1","6001115002860","","","inclusive");
INSERT INTO product VALUES("379","CREAMICA VANILA","CREAMICA VANILA","2.06","3","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6291103081094","","","inclusive");
INSERT INTO product VALUES("380","CREAMICA MILKY ","CREAMICA MILKY ","2.06","3","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6291103081100","","","inclusive");
INSERT INTO product VALUES("381","CREAMICA  CUSTARD ","CREAMICA  CUSTARD ","2.06","3","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6291103081087","","","inclusive");
INSERT INTO product VALUES("382","MARIE BLUE LABEL BISCUITS ","MARIE BLUE LABEL BISCUITS ","8.29","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001056453004","","","inclusive");
INSERT INTO product VALUES("383","MARIE LOBELS ","MARIE LOBELS ","3.75","5.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6007265000251","","","inclusive");
INSERT INTO product VALUES("384","CHICCO MARIE BISCUITS ","CHICCO MARIE BISCUITS ","0.00","3.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6291103081698","","","inclusive");
INSERT INTO product VALUES("385","FROOTY ","FROOTY ","8.91","13","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652782801","","","inclusive");
INSERT INTO product VALUES("386","MILKIT BANANA FLAVOUR ","MILKIT BANANA FLAVOUR ","3.31","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644997084","","","inclusive");
INSERT INTO product VALUES("387","MILKIT STRAWBERRY FLAVOUR ","MILKIT STRAWBERRY FLAVOUR ","3.31","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644997107","","","inclusive");
INSERT INTO product VALUES("388","CADBURY DAIRY MILK  MINT CRISP ","CADBURY DAIRY MILK  MINT CRISP ","0.00","18","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6001065601076","","","inclusive");
INSERT INTO product VALUES("389","CADBURY DAIRY MILK FRUIT & NUT ","CADBURY DAIRY MILK FRUIT & NUT ","0.00","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001065601014","","","inclusive");
INSERT INTO product VALUES("390","THIRSTY APPLE & RASPBERRY ","THIRSTY APPLE & RASPBERRY ","16.08","26","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6009650800606","","","inclusive");
INSERT INTO product VALUES("391","THIRSTY GRANADILLA ","THIRSTY GRANADILLA ","16.08","26","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009650800149","","","inclusive");
INSERT INTO product VALUES("392","GO FRESH PEACH ","GO FRESH PEACH ","0.00","21","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644925254","","","inclusive");
INSERT INTO product VALUES("393","MAZOE BLACKBERRY ","MAZOE BLACKBERRY ","0.00","35","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5449000243973","","","inclusive");
INSERT INTO product VALUES("394","MORTEIN CRAWLING INSECT KILLER ","MORTEIN CRAWLING INSECT KILLER ","0.00","22","default.gif","0","6","1","0","0","Non","0000-00-00 00:00:00","1","1","6001106103903","","","inclusive");
INSERT INTO product VALUES("395","OLE  VEGETABLE OIL ","OLE  VEGETABLE OIL ","32.08","45","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009674250067","","","inclusive");
INSERT INTO product VALUES("396","OLE VEGETABLE OIL ","OLE VEGETABLE OIL ","11.88","19","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009674250036","","","inclusive");
INSERT INTO product VALUES("397","HANDY ANDY ","HANDY ANDY ","15.90","22.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087006071","","","inclusive");
INSERT INTO product VALUES("398","ROYCO BEEF FLAVOUR ","ROYCO BEEF FLAVOUR ","11.44","17","default.gif","0","17","1","0","0","Non","0000-00-00 00:00:00","1","1","6161100605442","","","inclusive");
INSERT INTO product VALUES("399","ROYCO CHICKEN FLAVOUR ","ROYCO CHICKEN FLAVOUR ","11.44","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6161100605534","","","inclusive");
INSERT INTO product VALUES("400","COLGATE HERBAL ","COLGATE HERBAL ","9.13","13","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6001067021452","","","inclusive");
INSERT INTO product VALUES("401","CLORETS ELIMINATOR ","CLORETS ELIMINATOR ","7.27","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","7622210748324","","","inclusive");
INSERT INTO product VALUES("402","DENTYNE STRAWBERRY ","DENTYNE STRAWBERRY ","7.27","11","default.gif","0","7","1","0","0","Non","0000-00-00 00:00:00","1","1","6001065680934","","","inclusive");
INSERT INTO product VALUES("403","DENTYNE SPEARMINT ","DENTYNE SPEARMINT ","7.27","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001065680927","","","inclusive");
INSERT INTO product VALUES("404","HALLS ICE BLUE","HALLS ICE BLUE","0.00","13","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","60025258","","","inclusive");
INSERT INTO product VALUES("405","HALLS MENTHOLYPTUS ","HALLS MENTHOLYPTUS ","0.00","13","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","60025296","","","inclusive");
INSERT INTO product VALUES("406","CHAPPIES ","CHAPPIES ","0.00","0.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","CHAPPIES ","","","inclusive");
INSERT INTO product VALUES("407","KIWI QUALITY SHOE POLISH BLACK ","KIWI QUALITY SHOE POLISH BLACK ","12.46","20","default.gif","0","11","1","0","0","Non","0000-00-00 00:00:00","1","1","6001298959883","","","inclusive");
INSERT INTO product VALUES("408","ENERGY SAVER BULBS","ENERGY SAVER BULBS","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","BULB","","","inclusive");
INSERT INTO product VALUES("409","MAX SHAVERS ","MAX SHAVERS ","0.00","5","default.gif","0","4","1","0","0","Non","0000-00-00 00:00:00","1","1","6947739400722","","","inclusive");
INSERT INTO product VALUES("410","BLUE SEAL VASELINE 50ML","BLUE SEAL VASELINE 50ML","0.00","7","default.gif","0","10","1","0","0","Non","0000-00-00 00:00:00","1","1","60014399","","","inclusive");
INSERT INTO product VALUES("411","ADIDAS GET READY 4 HIM 150ML","ADIDAS GET READY 4 HIM 150ML","0.00","30","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001567220669","","","inclusive");
INSERT INTO product VALUES("412","ADIDAS CHAMPIONS LEAGUE 150ML","ADIDAS CHAMPIONS LEAGUE 150ML","0.00","30","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001567731776","","","inclusive");
INSERT INTO product VALUES("413","AXE BLACK BY BLACK COFFEE 150ML","AXE BLACK BY BLACK COFFEE 150ML","0.00","30","default.gif","0","65","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087364614","","","inclusive");
INSERT INTO product VALUES("414","ROMAN ROCK VODKA","ROMAN ROCK VODKA","0.00","70","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6002323015185","","","inclusive");
INSERT INTO product VALUES("415","RIVONNIA HOT CHILLI SAUCE ","RIVONNIA HOT CHILLI SAUCE ","0.00","12","default.gif","0","28","1","0","0","Non","0000-00-00 00:00:00","1","1","6009608440083","","","inclusive");
INSERT INTO product VALUES("416","TENNIS RED LABEL ","TENNIS RED LABEL ","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001056411004","","","inclusive");
INSERT INTO product VALUES("417","CREAMBELL BANANA CUSTARD THICK YORGHUT ","CREAMBELL BANANA CUSTARD THICK YORGHUT ","0.00","18","default.gif","0","1565","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672364","","","inclusive");
INSERT INTO product VALUES("418","CREAMBELL ORANGE YORGHUT ","CREAMBELL ORANGE YORGHUT ","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672241","","","inclusive");
INSERT INTO product VALUES("419","CREAMBELL RASPBERRY YORGHUT ","CREAMBELL RASPBERRY YORGHUT ","0.00","15","default.gif","0","48","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672227","","","inclusive");
INSERT INTO product VALUES("420","CREAMBELL STRAWBERRY YORGHUT ","CREAMBELL STRAWBERRY YORGHUT ","0.00","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672333","","","inclusive");
INSERT INTO product VALUES("421","STAY FREE THICK PADS ","STAY FREE THICK PADS ","0.00","13","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001012550044","","","inclusive");
INSERT INTO product VALUES("422","FOUR COUSINS N.S.RED 3LTR","FOUR COUSINS N.S.RED 3LTR","0.00","185","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6002269001518","","","inclusive");
INSERT INTO product VALUES("423","BUTTERCUP MARGARINE","BUTTERCUP MARGARINE","0.00","32","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009603060415","","","inclusive");
INSERT INTO product VALUES("424","TASSENBERG 750ML","TASSENBERG 750ML","0.00","65","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001452495004","","","inclusive");
INSERT INTO product VALUES("425","WILDCAT 330ML","WILDCAT 330ML","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644991204","","","inclusive");
INSERT INTO product VALUES("426","STING ENERGY DRINK 330ML","STING ENERGY DRINK 330ML","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009880715022","","","inclusive");
INSERT INTO product VALUES("427","STING ENERGY DRINK BERRY CHARGE 330ML","STING ENERGY DRINK BERRY CHARGE 330ML","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009880715039","","","inclusive");
INSERT INTO product VALUES("428","POPICO BUTTER SALT  150G","POPICO BUTTER SALT  150G","0.00","12","default.gif","0","9","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652784171","","","inclusive");
INSERT INTO product VALUES("429","POPICO CHEESE FLAVOUR","POPICO CHEESE FLAVOUR","0.00","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652784164","","","inclusive");
INSERT INTO product VALUES("430","HUNTERS GOLD","HUNTERS GOLD","0.00","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001108055231","","","inclusive");
INSERT INTO product VALUES("431","SAVANNA PREMIUM CIDER","SAVANNA PREMIUM CIDER","0.00","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001108028044","","","inclusive");
INSERT INTO product VALUES("432","CASTLE LITE","CASTLE LITE","0.00","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","631963000631","","","inclusive");
INSERT INTO product VALUES("433","HUNTERS DRY","HUNTERS DRY","0.00","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","HUNTERS","","","inclusive");
INSERT INTO product VALUES("434","WHITE HORSE","WHITE HORSE","0.00","137","default.gif","0","32","1","0","0","Non","0000-00-00 00:00:00","1","1","5000265001007","","","inclusive");
INSERT INTO product VALUES("435","PASSPORT SCOTCH","PASSPORT SCOTCH","0.00","142","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","80432401767","","","inclusive");
INSERT INTO product VALUES("436","KWV MERLOT","KWV MERLOT","0.00","95","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6002323300533","","","inclusive");
INSERT INTO product VALUES("437","SPRITE BOTTLED","SPRITE BOTTLED","0.00","4.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","SPRITE","","","inclusive");
INSERT INTO product VALUES("438","ALIVE PINEAPPLE","ALIVE PINEAPPLE","0.00","12","default.gif","0","7","1","0","0","Non","0000-00-00 00:00:00","1","1","6164004510508","","","inclusive");
INSERT INTO product VALUES("439","MOSI-O-TUNYA FALLS MERLOT","MOSI-O-TUNYA FALLS MERLOT","0.00","60","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009702490571","","","inclusive");
INSERT INTO product VALUES("440","MOSI-O-TUNYA FALLS CABERNET SAUVIGNON","MOSI-O-TUNYA FALLS CABERNET SAUVIGNON","0.00","50","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009702490519","","","inclusive");
INSERT INTO product VALUES("441","CELEBRATION RED N.SWEET","CELEBRATION RED N.SWEET","0.00","50","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009802516164","","","inclusive");
INSERT INTO product VALUES("442"," MOSI-O-TUNYA SAUVIGNON BLANC"," MOSI-O-TUNYA SAUVIGNON BLANC","0.00","50","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009702490434","","","inclusive");
INSERT INTO product VALUES("443","CELEBRATION ROSE N.SWEET","CELEBRATION ROSE N.SWEET","0.00","50","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009802516201","","","inclusive");
INSERT INTO product VALUES("444","MOSQUITO TRAPS","MOSQUITO TRAPS","0.00","235","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","TRAP","","","inclusive");
INSERT INTO product VALUES("445","FRUIT YOGHURT MANGO","FRUIT YOGHURT MANGO","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672388","","","inclusive");
INSERT INTO product VALUES("446","FRUIT YOGHURT MANGO","FRUIT YOGHURT MANGO","0.00","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672395","","","inclusive");
INSERT INTO product VALUES("447","SUPER MAHEU   CHOCOLATE","SUPER MAHEU   CHOCOLATE","0.00","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001299044601","","","inclusive");
INSERT INTO product VALUES("448","SUPER MAHEU  STRAWBERRY","SUPER MAHEU  STRAWBERRY","0.00","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001299044519","","","inclusive");
INSERT INTO product VALUES("449","GERM X HAND SENSITISER TERRIFIC TANGERINE  ","GERM X HAND SENSITISER TERRIFIC TANGERINE  ","0.00","20","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009690281687","","","inclusive");
INSERT INTO product VALUES("450","GERM-X HAND SANITISER AWESOME APPLE ","GERM-X HAND SANITISER AWESOME APPLE ","0.00","20","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6009690281700","","","inclusive");
INSERT INTO product VALUES("451","GERM-X HAND SENITISER  BLAZING BLUEBERRY ","GERM-X HAND SENITISER  BLAZING BLUEBERRY ","0.00","20","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009690281694","","","inclusive");
INSERT INTO product VALUES("452","CAMEL FILTER CIGARETTES ","CAMEL FILTER CIGARETTES ","0.00","30","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","40329055","","","inclusive");
INSERT INTO product VALUES("453","MAZOE CREAM SODA ","MAZOE CREAM SODA ","0.00","35","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5449000243867","","","inclusive");
INSERT INTO product VALUES("454","THIRSTY PEACH SQUASH ","THIRSTY PEACH SQUASH ","16.08","26","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009650800132","","","inclusive");
INSERT INTO product VALUES("455","SHAKE 'N SIP BANANA","SHAKE 'N SIP BANANA","4.04","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644999132","","","inclusive");
INSERT INTO product VALUES("456","SHAKE 'N SIP PINEAPPLE ","SHAKE 'N SIP PINEAPPLE ","4.04","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644999125","","","inclusive");
INSERT INTO product VALUES("457","TOPPERS MINT CHOC FLAVOURED CREAM 6009704170136","TOPPERS MINT CHOC FLAVOURED CREAM 6009704170136","5.42","9","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009704170136","","","inclusive");
INSERT INTO product VALUES("458","TOPPERS LEMON FLAVOURED CREAM","TOPPERS LEMON FLAVOURED CREAM","5.42","9","default.gif","0","34","1","0","0","Non","0000-00-00 00:00:00","1","1","6009704170822","","","inclusive");
INSERT INTO product VALUES("459","TOPPERS CUSTARD FLAVOURED CREAM ","TOPPERS CUSTARD FLAVOURED CREAM ","0.00","9","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001056412940","","","inclusive");
INSERT INTO product VALUES("460","YESS FRUIT PUNCH ","YESS FRUIT PUNCH ","2.23","3.5","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","606110287975","","","inclusive");
INSERT INTO product VALUES("461","YESS PINEAPPLE ","YESS PINEAPPLE ","2.23","3.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","606110287982","","","inclusive");
INSERT INTO product VALUES("462","LUNCH BAR ","LUNCH BAR ","6.63","10","default.gif","0","8","1","0","0","Non","0000-00-00 00:00:00","1","1","6001065034027","","","inclusive");
INSERT INTO product VALUES("463","PS CARAMILK ","PS CARAMILK ","0.00","0","default.gif","0","6","1","0","0","Non","0000-00-00 00:00:00","1","1","6001065034089","","","inclusive");
INSERT INTO product VALUES("464","FRUITREE MEDITERRANEAN ","FRUITREE MEDITERRANEAN ","14.13","22","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240390320","","","inclusive");
INSERT INTO product VALUES("465","OLE COOKING OIL ","OLE COOKING OIL ","0.00","60","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","996009674250074","","","inclusive");
INSERT INTO product VALUES("466","FRUITREE TROPICAL ","FRUITREE TROPICAL ","0.00","22","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240390306","","","inclusive");
INSERT INTO product VALUES("467","FIVE ROSES ","FIVE ROSES ","0.00","52.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001156230253","","","inclusive");
INSERT INTO product VALUES("468","FIVBE ROSES ","FIVBE ROSES ","0.00","20","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001156230062","","","inclusive");
INSERT INTO product VALUES("469","FIRELIGHTERS CUBES","FIRELIGHTERS CUBES","0.00","20","default.gif","0","3","1","0","0","Non","0000-00-00 00:00:00","1","1","6001539603100","","","inclusive");
INSERT INTO product VALUES("470","BIC MEGA LIGHTER","BIC MEGA LIGHTER","0.00","45","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","3086126788388","","","inclusive");
INSERT INTO product VALUES("471","SHAVERS ","SHAVERS ","0.00","14","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","4027800318858","","","inclusive");
INSERT INTO product VALUES("472","PROTEX HERBAL ANTIGERM ","PROTEX HERBAL ANTIGERM ","0.00","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001067004271","","","inclusive");
INSERT INTO product VALUES("473","FANTA SPARKLING ORANGE","FANTA SPARKLING ORANGE","0.00","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5449000106476","","","inclusive");
INSERT INTO product VALUES("474","COCA COLA 440ML","COCA COLA 440ML","0.00","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5449000140906","","","inclusive");
INSERT INTO product VALUES("475","CHIBWANTU","CHIBWANTU","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001983110278","","","inclusive");
INSERT INTO product VALUES("476","CREAMBELL YORGHUT ORANGE ","CREAMBELL YORGHUT ORANGE ","0.00","7","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672258","","","inclusive");
INSERT INTO product VALUES("477","NAMAQUA 3L WHITE","NAMAQUA 3L WHITE","0.00","185","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6004442001391","","","inclusive");
INSERT INTO product VALUES("478","VITA DELITE GUAVA JUICE ","VITA DELITE GUAVA JUICE ","8.75","14","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6009622532900","","","inclusive");
INSERT INTO product VALUES("479","J & B BLENDED SCOTCH WHISKY","J & B BLENDED SCOTCH WHISKY","0.00","165","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","JB","","","inclusive");
INSERT INTO product VALUES("480","MIRINDA ORANGE ","MIRINDA ORANGE ","7.17","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009880793105","","","inclusive");
INSERT INTO product VALUES("481","MIRINDA FRUITY ","MIRINDA FRUITY ","7.17","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009880793112","","","inclusive");
INSERT INTO product VALUES("482","PEPSI ","PEPSI ","7.17","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009880793082","","","inclusive");
INSERT INTO product VALUES("483","ALIVE MANGO ","ALIVE MANGO ","0.00","12","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6164004510492","","","inclusive");
INSERT INTO product VALUES("484","CREAMBELL BANANA FLAVOURED MILK ","CREAMBELL BANANA FLAVOURED MILK ","4.17","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672081","","","inclusive");
INSERT INTO product VALUES("485","SUNLIGHT  LEMON ","SUNLIGHT  LEMON ","0.00","32","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087358743","","","inclusive");
INSERT INTO product VALUES("486","INSTANT NOODLE","INSTANT NOODLE","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6091005008003","","","inclusive");
INSERT INTO product VALUES("487","SHIELD SHEEN","SHIELD SHEEN","0.00","38","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001878001186","","","inclusive");
INSERT INTO product VALUES("488","LINX ENGINE CLEANER","LINX ENGINE CLEANER","0.00","17","default.gif","0","9","1","0","0","Non","0000-00-00 00:00:00","1","1","6009834370901","","","inclusive");
INSERT INTO product VALUES("489","CAR POLISH","CAR POLISH","0.00","65.5","default.gif","0","15","1","0","0","Non","0000-00-00 00:00:00","1","1","6001878001520","","","inclusive");
INSERT INTO product VALUES("490","TYRE GLOSS","TYRE GLOSS","0.00","60","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001878001148","","","inclusive");
INSERT INTO product VALUES("491","FOUR COUSINS SWEET ROSE 1.5L","FOUR COUSINS SWEET ROSE 1.5L","0.00","130","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6002269000153","","","inclusive");
INSERT INTO product VALUES("492","SAINT ANNA NATURAL SWEET","SAINT ANNA NATURAL SWEET","0.00","40","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001812011912","","","inclusive");
INSERT INTO product VALUES("493","COCA COLA CANNED","COCA COLA CANNED","7.08","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5449000256836","","","inclusive");
INSERT INTO product VALUES("494","7 UP ","7 UP ","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("495","ALIVE MIXED BERRY PUNCH JUICE ","ALIVE MIXED BERRY PUNCH JUICE ","0.00","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6164004510553","","","inclusive");
INSERT INTO product VALUES("496","CREAMBELL THICK PINEAPPLE YORGHUT ","CREAMBELL THICK PINEAPPLE YORGHUT ","0.00","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672340","","","inclusive");
INSERT INTO product VALUES("497","CREAMBELL SHAKERS STRAWBERRY ","CREAMBELL SHAKERS STRAWBERRY ","0.00","3","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672685","","","inclusive");
INSERT INTO product VALUES("498"," CREAMBELL STRAWBERRY MILK MAHEU "," CREAMBELL STRAWBERRY MILK MAHEU ","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672517","","","inclusive");
INSERT INTO product VALUES("499","CREAMBELL BANANA MILK MAHEU","CREAMBELL BANANA MILK MAHEU","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672500","","","inclusive");
INSERT INTO product VALUES("500","CREAMBELL BUTTER CREAM MILK MAHEU ","CREAMBELL BUTTER CREAM MILK MAHEU ","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672494","","","inclusive");
INSERT INTO product VALUES("501","CREAMBELL BUTTER CREAM MILK MAHEU","CREAMBELL BUTTER CREAM MILK MAHEU","0.00","3.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672463","","","inclusive");
INSERT INTO product VALUES("502","ALIVE FLAVOURED DRINK MANGO","ALIVE FLAVOURED DRINK MANGO","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6164004510379","","","inclusive");
INSERT INTO product VALUES("503","ALIVE PINEAPPLE","ALIVE PINEAPPLE","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6164004510386","","","inclusive");
INSERT INTO product VALUES("504","HANDY ANDY LEMON FRESH ","HANDY ANDY LEMON FRESH ","16.90","24","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087006057","","","inclusive");
INSERT INTO product VALUES("505","BOOM FORCE PINE FRESH ","BOOM FORCE PINE FRESH ","13.20","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537022114","","","inclusive");
INSERT INTO product VALUES("506","JUNGLE OATS ","JUNGLE OATS ","24.90","35","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009518201224","","","inclusive");
INSERT INTO product VALUES("507","ROMEO ACTIVE ","ROMEO ACTIVE ","5.50","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644923281","","","inclusive");
INSERT INTO product VALUES("508","LIFEBUOY DEO FRESH ","LIFEBUOY DEO FRESH ","8.25","12","default.gif","0","3","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087358644","","","inclusive");
INSERT INTO product VALUES("509","ROMEO PINK BEAUTY SOAP ","ROMEO PINK BEAUTY SOAP ","5.50","8","default.gif","0","5","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644922734","","","inclusive");
INSERT INTO product VALUES("510","ROMEO WHITE BEAUTY SOAP ","ROMEO WHITE BEAUTY SOAP ","5.50","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644922628","","","inclusive");
INSERT INTO product VALUES("511","DENTYNE WHITE ","DENTYNE WHITE ","0.00","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","7622210626134","","","inclusive");
INSERT INTO product VALUES("512","CLORETS GREEN MINT ","CLORETS GREEN MINT ","0.00","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","7622210748331","","","inclusive");
INSERT INTO product VALUES("513","DORITOS SWEET CHILLI PEPPER ","DORITOS SWEET CHILLI PEPPER ","3.33","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510802542","","","inclusive");
INSERT INTO product VALUES("514","CREAMICA CHOCOLATE ","CREAMICA CHOCOLATE ","2.19","4","default.gif","0","11","1","0","0","Non","0000-00-00 00:00:00","1","1","6291103081070","","","inclusive");
INSERT INTO product VALUES("515","DLITE COOKING OIL","DLITE COOKING OIL","32.50","57","default.gif","0","38","1","0","0","Non","0000-00-00 00:00:00","1","1","6001565009037","","","inclusive");
INSERT INTO product VALUES("516","BIC PENS ","BIC PENS ","0.00","2","default.gif","0","32","1","0","0","Non","0000-00-00 00:00:00","1","1","PENS","","","inclusive");
INSERT INTO product VALUES("517","RIVONIA BBQ SAUCE ","RIVONIA BBQ SAUCE ","10.95","16","default.gif","0","8","1","0","0","Non","0000-00-00 00:00:00","1","1","6009608440434","","","inclusive");
INSERT INTO product VALUES("518","SOFT TOUCH PETROLEUM JELLY ","SOFT TOUCH PETROLEUM JELLY ","9.42","13","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009673920008","","","inclusive");
INSERT INTO product VALUES("519","DAIRY MILK BISCUIT","DAIRY MILK BISCUIT","0.00","18","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001065601083","","","inclusive");
INSERT INTO product VALUES("520","DAIRY CREAMY WHITE","DAIRY CREAMY WHITE","0.00","18","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6001065601038","","","inclusive");
INSERT INTO product VALUES("521","POPICO SOUR CREAM & CHIVES ","POPICO SOUR CREAM & CHIVES ","0.00","11","default.gif","0","3","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652784188","","","inclusive");
INSERT INTO product VALUES("522","ENO REGULAR FLAVOUR ","ENO REGULAR FLAVOUR ","0.00","4","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","60050205","","","inclusive");
INSERT INTO product VALUES("523","BEST GIN","BEST GIN","0.00","60","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6009675691951","","","inclusive");
INSERT INTO product VALUES("524","CREAMBELL MAHEU STRAWBERRY","CREAMBELL MAHEU STRAWBERRY","0.00","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672616","","","inclusive");
INSERT INTO product VALUES("525","CREAMBELL MAHEU  BUTTER CREAM","CREAMBELL MAHEU  BUTTER CREAM","0.00","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803672593","","","inclusive");
INSERT INTO product VALUES("526","JAGERMEISTER","JAGERMEISTER","0.00","237","default.gif","0","11","1","0","0","Non","0000-00-00 00:00:00","1","1","4067700014535","","","inclusive");
INSERT INTO product VALUES("527","FLYING FISH","FLYING FISH","0.00","18","default.gif","0","43","1","0","0","Non","0000-00-00 00:00:00","1","1","6003326009584","","","inclusive");
INSERT INTO product VALUES("528","HEINEKEN","HEINEKEN","0.00","15","default.gif","0","15","1","0","0","Non","0000-00-00 00:00:00","1","1","6009801472102","","","inclusive");
INSERT INTO product VALUES("529","CHICKEN","CHICKEN","0.00","50","default.gif","0","24","1","0","0","Non","0000-00-00 00:00:00","1","1","CK","","","inclusive");
INSERT INTO product VALUES("530","TWIST","TWIST","0.00","8","default.gif","0","94","1","0","0","Non","0000-00-00 00:00:00","1","1","87303339","","","inclusive");
INSERT INTO product VALUES("531","TWIST","TWIST","0.00","8","default.gif","0","4","1","0","0","Non","0000-00-00 00:00:00","1","1","40822747","","","inclusive");
INSERT INTO product VALUES("532","LIQUI FRUIT CLEAR APPLE","LIQUI FRUIT CLEAR APPLE","0.00","23","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6001048000339","","","inclusive");
INSERT INTO product VALUES("533","LIQUI FRUIT MANGO & ORANGE","LIQUI FRUIT MANGO & ORANGE","0.00","23","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240500262","","","inclusive");
INSERT INTO product VALUES("534","FRUITREE GUAVA 1LTR","FRUITREE GUAVA 1LTR","0.00","23","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240390030","","","inclusive");
INSERT INTO product VALUES("535","FRUITREE RED GRAPE 1LTR","FRUITREE RED GRAPE 1LTR","0.00","23","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240390108","","","inclusive");
INSERT INTO product VALUES("536","SIMBA SALTED PEANUTS ","SIMBA SALTED PEANUTS ","0.00","6.5","default.gif","0","106","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510802924","","","inclusive");
INSERT INTO product VALUES("537","SIMBA SALTED PEANUTS AND RAISINS ","SIMBA SALTED PEANUTS AND RAISINS ","0.00","6.5","default.gif","0","31","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510802931","","","inclusive");
INSERT INTO product VALUES("538","PLASTIC BAG","PLASTIC BAG","0.00","1","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","BAG","","","inclusive");
INSERT INTO product VALUES("539","FALLS BLENDED WHISKY","FALLS BLENDED WHISKY","0.00","70","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009802516447","","","inclusive");
INSERT INTO product VALUES("540","MAcCAIRN'S","MAcCAIRN'S","0.00","70","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009802516416","","","inclusive");
INSERT INTO product VALUES("541","CELLAR CASK WHITE N.SWEET","CELLAR CASK WHITE N.SWEET","0.00","70","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001496301705","","","inclusive");
INSERT INTO product VALUES("542","BEEFEATER LONDON","BEEFEATER LONDON","0.00","150","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5000329002254","","","inclusive");
INSERT INTO product VALUES("543","CHAMDOR SPARKLING GRAPE","CHAMDOR SPARKLING GRAPE","0.00","100","default.gif","0","9","1","0","0","Non","0000-00-00 00:00:00","1","1","6001495080007","","","inclusive");
INSERT INTO product VALUES("544","NATURAL HONEY ","NATURAL HONEY ","0.00","25","default.gif","0","51","1","0","0","Non","0000-00-00 00:00:00","1","1","711841508616","","","inclusive");
INSERT INTO product VALUES("545","VOLCANO  ENERGY DRINK ","VOLCANO  ENERGY DRINK ","2.79","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009801423715","","","inclusive");
INSERT INTO product VALUES("546","COCO PINE ","COCO PINE ","3.08","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652784539","","","inclusive");
INSERT INTO product VALUES("547","FROOTY RED FRUITB NECTAR ","FROOTY RED FRUITB NECTAR ","0.00","5","default.gif","0","3","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652784591","","","inclusive");
INSERT INTO product VALUES("548","PASSION ORANGE ","PASSION ORANGE ","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652784805","","","inclusive");
INSERT INTO product VALUES("549","SODA KING TONIC WATER MIXERS ","SODA KING TONIC WATER MIXERS ","0.00","0","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6009609611024","","","inclusive");
INSERT INTO product VALUES("550","GINGER NUTS ","GINGER NUTS ","14.13","20","default.gif","0","106","1","0","0","Non","0000-00-00 00:00:00","1","1","6001056294003","","","inclusive");
INSERT INTO product VALUES("551","NIVEA MAXIMUM HYDRATION ","NIVEA MAXIMUM HYDRATION ","32.41","45","default.gif","0","92","1","0","0","Non","0000-00-00 00:00:00","1","1","4005808661930","","","inclusive");
INSERT INTO product VALUES("552","VASELINE MEN EXTRA STRENGTH ","VASELINE MEN EXTRA STRENGTH ","22.25","32","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087012010","","","inclusive");
INSERT INTO product VALUES("553","VASELINE MEN COOLING ","VASELINE MEN COOLING ","0.00","32","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087012027","","","inclusive");
INSERT INTO product VALUES("554","VASELINE INTENSIVE CARE ","VASELINE INTENSIVE CARE ","0.00","32","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001087357050","","","inclusive");
INSERT INTO product VALUES("555","CHELSEA SHORTBREAD BISCUIT","CHELSEA SHORTBREAD BISCUIT","8.13","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537014041","","","inclusive");
INSERT INTO product VALUES("556","CHELSEA OAT CRUNCH COOKIES ","CHELSEA OAT CRUNCH COOKIES ","8.13","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644992317","","","inclusive");
INSERT INTO product VALUES("557","CHELSEA CHOC CHIP COOKIES ","CHELSEA CHOC CHIP COOKIES ","8.13","12","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537014072","","","inclusive");
INSERT INTO product VALUES("558","CERES DELIGHT TROPICAL","CERES DELIGHT TROPICAL","16.21","23","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240223130","","","inclusive");
INSERT INTO product VALUES("559","CERES DELIGHT CLOUDY APPLE AND PEAR ","CERES DELIGHT CLOUDY APPLE AND PEAR ","16.21","23","default.gif","0","7","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240100684","","","inclusive");
INSERT INTO product VALUES("560","CERES PINEAPPLE ","CERES PINEAPPLE ","16.21","23","default.gif","0","29","1","0","0","Non","0000-00-00 00:00:00","1","1","6001240100066","","","inclusive");
INSERT INTO product VALUES("561","TIGER HEAD BATTERIES ","TIGER HEAD BATTERIES ","2.71","4.5","default.gif","0","5","1","0","0","Non","0000-00-00 00:00:00","1","1","TIGERHEAD ","","","inclusive");
INSERT INTO product VALUES("562","DLITE GROWNUPS STRAWBERRY ","DLITE GROWNUPS STRAWBERRY ","21.00","30","default.gif","0","28","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644928484","","","inclusive");
INSERT INTO product VALUES("563","DLITE  GROWN UPS BANANA","DLITE  GROWN UPS BANANA","21.00","30","default.gif","0","4","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537013051","","","inclusive");
INSERT INTO product VALUES("564","DLIRTE GROWNUPS WHEAT ","DLIRTE GROWNUPS WHEAT ","21.00","30","default.gif","0","56","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537014348","","","inclusive");
INSERT INTO product VALUES("565","ADIDAS DYNAMIC PULSE ","ADIDAS DYNAMIC PULSE ","19.42","28","default.gif","0","10","1","0","0","Non","0000-00-00 00:00:00","1","1","6001567179820","","","inclusive");
INSERT INTO product VALUES("566","EMILLIOS CORN PUFFS CHEESE ","EMILLIOS CORN PUFFS CHEESE ","0.44","1","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009670770019","","","inclusive");
INSERT INTO product VALUES("567","EMILLIOS CORN PUFFS CHICKEN ","EMILLIOS CORN PUFFS CHICKEN ","0.44","1","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009670770026","","","inclusive");
INSERT INTO product VALUES("568","LIGHTERS ","LIGHTERS ","0.99","5","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6739225235815","","","inclusive");
INSERT INTO product VALUES("569","FAMILY CARE MOSQUITO REPELLANT ","FAMILY CARE MOSQUITO REPELLANT ","4.13","7","default.gif","0","3","1","0","0","Non","0000-00-00 00:00:00","1","1","6201100032128","","","inclusive");
INSERT INTO product VALUES("570","COLGATE HERBAL ","COLGATE HERBAL ","4.13","6","default.gif","0","6","1","0","0","Non","0000-00-00 00:00:00","1","1","6920354817809","","","inclusive");
INSERT INTO product VALUES("571","AMIGO CRISPS CHEESE & ONION ","AMIGO CRISPS CHEESE & ONION ","2.70","4","default.gif","0","2","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652780142","","","inclusive");
INSERT INTO product VALUES("572","AMIGO CLASSIC CRISPS ","AMIGO CLASSIC CRISPS ","2.70","4","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652781897","","","inclusive");
INSERT INTO product VALUES("573","AMIGO CLASSIC CRISPS SPICY TOMATO ","AMIGO CLASSIC CRISPS SPICY TOMATO ","0.00","14","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652780050","","","inclusive");
INSERT INTO product VALUES("574","AMIGO CLASSIC CRISPS SOUR  CREAM & ONION ","AMIGO CLASSIC CRISPS SOUR  CREAM & ONION ","0.00","14","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652780326","","","inclusive");
INSERT INTO product VALUES("575","AMIGO CLASSIC CRISPS CHEESE & ONION ","AMIGO CLASSIC CRISPS CHEESE & ONION ","0.00","14","default.gif","0","1","1","0","0","Non","0000-00-00 00:00:00","1","1","6009652780159","","","inclusive");
INSERT INTO product VALUES("576","JUNGLE OATS 1KG","JUNGLE OATS 1KG","0.00","48","default.gif","0","20","1","0","0","Non","0000-00-00 00:00:00","1","1","6001275000003","","","inclusive");
INSERT INTO product VALUES("577","D'LITE BANANA 250G","D'LITE BANANA 250G","0.00","25","default.gif","0","3","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644923144","","","inclusive");
INSERT INTO product VALUES("578","SIMBA CHEESE & ONION","SIMBA CHEESE & ONION","0.00","16","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510808582","","","inclusive");
INSERT INTO product VALUES("579","MORTEIN POWERGARD","MORTEIN POWERGARD","0.00","22","default.gif","0","18","1","0","0","Non","0000-00-00 00:00:00","1","1","6001106225551","","","inclusive");
INSERT INTO product VALUES("580","NATURAL NKETU 250ML","NATURAL NKETU 250ML","0.00","7.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6161109780690","","","inclusive");
INSERT INTO product VALUES("581","NATURAL MORINGA LEMON GRASS 250ML","NATURAL MORINGA LEMON GRASS 250ML","0.00","7.5","default.gif","0","84","1","0","0","Non","0000-00-00 00:00:00","1","1","6161109780331","","","inclusive");
INSERT INTO product VALUES("582","PETER MODERN RED","PETER MODERN RED","0.00","15","default.gif","0","94","1","0","0","Non","0000-00-00 00:00:00","1","1","6008165407102","","","inclusive");
INSERT INTO product VALUES("583","OVERMEER HARVEST","OVERMEER HARVEST","0.00","180","default.gif","0","64","1","0","0","Non","0000-00-00 00:00:00","1","1","6001452059008","","","inclusive");
INSERT INTO product VALUES("584","OVERMEER SMOOTH RED","OVERMEER SMOOTH RED","0.00","190","default.gif","0","33","1","0","0","Non","0000-00-00 00:00:00","1","1","6001452578004","","","inclusive");
INSERT INTO product VALUES("585","ROUND BREAD","ROUND BREAD","0.00","13","default.gif","0","10","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("586","CAPTAIN MORGAN J RUM","CAPTAIN MORGAN J RUM","0.00","142","default.gif","0","53","1","0","0","Non","0000-00-00 00:00:00","1","1","87000006928","","","inclusive");
INSERT INTO product VALUES("587","CAREX CONDOMS","CAREX CONDOMS","0.00","20","default.gif","0","64","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("588","TASTY BLACK CURRANT SQUASH","TASTY BLACK CURRANT SQUASH","0.00","21","default.gif","0","7","1","0","0","Non","0000-00-00 00:00:00","1","1","6161109780904","","","inclusive");
INSERT INTO product VALUES("589","TASS","TASS","0.00","0","default.gif","0","13","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("590","TASTY ORANGE SQUASH","TASTY ORANGE SQUASH","0.00","21","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6161109780881","","","inclusive");
INSERT INTO product VALUES("591","TASTY TROPICAL SQUASH","TASTY TROPICAL SQUASH","0.00","142","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6161109780928","","","inclusive");
INSERT INTO product VALUES("592","TASTY RASPBERRY SQUASH","TASTY RASPBERRY SQUASH","0.00","21","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6161109780898","","","inclusive");
INSERT INTO product VALUES("593","CHIBWANTU","CHIBWANTU","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("594","9V MAX","9V MAX","0.00","75","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","8888021205275","","","inclusive");
INSERT INTO product VALUES("595","D 2PACK MAX","D 2PACK MAX","0.00","85","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","8888021205251","","","inclusive");
INSERT INTO product VALUES("596","C 2PACK MAX","C 2PACK MAX","0.00","60","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","8888021205244","","","inclusive");
INSERT INTO product VALUES("597","CHOC PEANUTS","CHOC PEANUTS","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6005837001682","","","inclusive");
INSERT INTO product VALUES("598","CHOC SHORTCAKE","CHOC SHORTCAKE","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6005837006908","","","inclusive");
INSERT INTO product VALUES("599","MINT IMPERIALS","MINT IMPERIALS","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6005837000494","","","inclusive");
INSERT INTO product VALUES("600","MINT IMPERIALS","MINT IMPERIALS","0.00","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6005837000791","","","inclusive");
INSERT INTO product VALUES("601","MUNKOYO","MUNKOYO","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","Munkoyo","","","inclusive");
INSERT INTO product VALUES("602","amarula cream","amarula cream","0.00","190","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001495062669","","","inclusive");
INSERT INTO product VALUES("603","NAMAQUA JO RED","NAMAQUA JO RED","0.00","205","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6004442002497","","","inclusive");
INSERT INTO product VALUES("604","VITA PEACH","VITA PEACH","0.00","14","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009622532931","","","inclusive");
INSERT INTO product VALUES("605","VITA MANGO & ORANGE","VITA MANGO & ORANGE","0.00","14","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009622532917","","","inclusive");
INSERT INTO product VALUES("606","VITA TROPICAL","VITA TROPICAL","0.00","14","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009622534263","","","inclusive");
INSERT INTO product VALUES("607","GOLDEN NUTS ","GOLDEN NUTS ","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","21533","","","inclusive");
INSERT INTO product VALUES("608","CELEBRATION WHITE","CELEBRATION WHITE","0.00","50","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009802516188","","","inclusive");
INSERT INTO product VALUES("609","MOSI-O-TUNYA RIVER RED","MOSI-O-TUNYA RIVER RED","0.00","60","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009802516072","","","inclusive");
INSERT INTO product VALUES("610","MOSI-O-TUNYA PINOTAGE","MOSI-O-TUNYA PINOTAGE","0.00","60","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009802516393","","","inclusive");
INSERT INTO product VALUES("611","MOSI-O-TUNYA RIVER WHITE","MOSI-O-TUNYA RIVER WHITE","0.00","60","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009802516058","","","inclusive");
INSERT INTO product VALUES("612","CELEBRATION CHARDONNAY","CELEBRATION CHARDONNAY","0.00","60","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009702490472","","","inclusive");
INSERT INTO product VALUES("613","TALL HORSE  CHARDONNAY","TALL HORSE  CHARDONNAY","0.00","87","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001506900232","","","inclusive");
INSERT INTO product VALUES("614","CHICKEN HALF","CHICKEN HALF","0.00","27","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("615","pork meat","pork meat","0.00","25","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("616","ENERGY BAR-COFFEE","ENERGY BAR-COFFEE","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","9304903753379","","","inclusive");
INSERT INTO product VALUES("617","ENERGY BAR -CRANBERRY ","ENERGY BAR -CRANBERRY ","0.00","15","default.gif","0","24","1","0","0","Non","0000-00-00 00:00:00","1","1","9304903284453","","","inclusive");
INSERT INTO product VALUES("618","FRUIT BAR-BERRY","FRUIT BAR-BERRY","0.00","15","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","9304903326283","","","inclusive");
INSERT INTO product VALUES("619","SIMBA SMOKED BEEF CHIPS ","SIMBA SMOKED BEEF CHIPS ","0.00","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510808490","","","inclusive");
INSERT INTO product VALUES("620","SIMBA CREAMY CHEDDAR     POTATO CHIPS ","SIMBA CREAMY CHEDDAR     POTATO CHIPS ","0.00","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510808537","","","inclusive");
INSERT INTO product VALUES("621","LAYS ITALIAN CHEESE ","LAYS ITALIAN CHEESE ","0.00","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510808216","","","inclusive");
INSERT INTO product VALUES("622","SIMBA ALL GOLD TOMATO SAUCE ","SIMBA ALL GOLD TOMATO SAUCE ","0.00","16","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510808605","","","inclusive");
INSERT INTO product VALUES("623","SIMBA ALL GOLD TOMATO SAUCE","SIMBA ALL GOLD TOMATO SAUCE","0.00","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510808544","","","inclusive");
INSERT INTO product VALUES("624","TUMBWI BARKING POWDER","TUMBWI BARKING POWDER","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537014683","","","inclusive");
INSERT INTO product VALUES("625","PEPSI ","PEPSI ","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009880793334","","","inclusive");
INSERT INTO product VALUES("626","MIRINDA GRAPE","MIRINDA GRAPE","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009880793365","","","inclusive");
INSERT INTO product VALUES("627","MIRINDA ORANGE","MIRINDA ORANGE","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009880793358","","","inclusive");
INSERT INTO product VALUES("628","AMA SIPSIP BUTTER CREAM","AMA SIPSIP BUTTER CREAM","0.00","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009537019442","","","inclusive");
INSERT INTO product VALUES("629","JUICY GINGER","JUICY GINGER","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6161109781345","","","inclusive");
INSERT INTO product VALUES("630","MWABUKA BREAD","MWABUKA BREAD","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("631","SALA BREAD","SALA BREAD","0.00","11","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","","","","inclusive");
INSERT INTO product VALUES("632","MIRINDA GREEN APPLE","MIRINDA GREEN APPLE","0.00","8","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803227557","","","inclusive");
INSERT INTO product VALUES("633","VITALCARE ABSORBENT COTTON WOOL 500G","VITALCARE ABSORBENT COTTON WOOL 500G","0.00","32","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6958590414100","","","inclusive");
INSERT INTO product VALUES("634","appy berry","appy berry","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009709822436","","","inclusive");
INSERT INTO product VALUES("635","juicy","juicy","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6161109781031","","","inclusive");
INSERT INTO product VALUES("636","juicy","juicy","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6161109781017","","","inclusive");
INSERT INTO product VALUES("637","juicy","juicy","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6161109781024","","","inclusive");
INSERT INTO product VALUES("638","sun mixed jam","sun mixed jam","0.00","35","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6007497100859","","","inclusive");
INSERT INTO product VALUES("639","SHUWA JUICE ","SHUWA JUICE ","0.00","32","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009673320556","","","inclusive");
INSERT INTO product VALUES("640","THIRSTY BLACKCURRANT","THIRSTY BLACKCURRANT","0.00","26","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009650800101","","","inclusive");
INSERT INTO product VALUES("641","DLITE PURE COOKING OIL","DLITE PURE COOKING OIL","0.00","24","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001565009020","","","inclusive");
INSERT INTO product VALUES("642","LAYS SALTED POTATO CHIPS","LAYS SALTED POTATO CHIPS","0.00","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009510804782","","","inclusive");
INSERT INTO product VALUES("643","BAKERS CHOC.KITS ","BAKERS CHOC.KITS ","0.00","23","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6001056119009","","","inclusive");
INSERT INTO product VALUES("644","KUNG FU ENERGY ","KUNG FU ENERGY ","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644992089","","","inclusive");
INSERT INTO product VALUES("645","YESS GRANADILLA","YESS GRANADILLA","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","606110288118","","","inclusive");
INSERT INTO product VALUES("646","TANGO ORANGE","TANGO ORANGE","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","606110288101","","","inclusive");
INSERT INTO product VALUES("647","BEST LONDON DRY GIN","BEST LONDON DRY GIN","0.00","30","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009675695324","","","inclusive");
INSERT INTO product VALUES("648","PEPSI CANNED ","PEPSI CANNED ","0.00","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803227250","","","inclusive");
INSERT INTO product VALUES("649","7 UP CANNED","7 UP CANNED","0.00","6.5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009803227304","","","inclusive");
INSERT INTO product VALUES("650","divine power kombucha","divine power kombucha","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009881099121","","","inclusive");
INSERT INTO product VALUES("651","divine power kombucha","divine power kombucha","0.00","10","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009881099138","","","inclusive");
INSERT INTO product VALUES("652","BEEFEATER GIN PINK","BEEFEATER GIN PINK","0.00","150","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5000299618073","","","inclusive");
INSERT INTO product VALUES("653","TANQUERAY GIN","TANQUERAY GIN","0.00","270","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","5000291020706","","","inclusive");
INSERT INTO product VALUES("654","CIROC VODKA","CIROC VODKA","0.00","600","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","88076161863","","","inclusive");
INSERT INTO product VALUES("655","HENNESSY","HENNESSY","0.00","600","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","3245996208116","","","inclusive");
INSERT INTO product VALUES("656","MOJO APPLE","MOJO APPLE","0.00","6","default.gif","0","17","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644995813","","","inclusive");
INSERT INTO product VALUES("657","MOJO PINEAPPLE ","MOJO PINEAPPLE ","0.00","6","default.gif","0","3","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644995820","","","inclusive");
INSERT INTO product VALUES("658","ROBERTSON WINERY NATURAL SWEET RED","ROBERTSON WINERY NATURAL SWEET RED","0.00","85","default.gif","0","29","1","0","0","Non","0000-00-00 00:00:00","1","1","6002039006019","","","inclusive");
INSERT INTO product VALUES("659","ROBERTSON WINERY NATURAL SWEET WHITE","ROBERTSON WINERY NATURAL SWEET WHITE","0.00","85","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6002039005753","","","inclusive");
INSERT INTO product VALUES("660","super maheu strawberry","super maheu strawberry","0.00","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644920006","","","inclusive");
INSERT INTO product VALUES("661","super maheu banana","super maheu banana","0.00","7","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009644920020","","","inclusive");
INSERT INTO product VALUES("662","apply orange","apply orange","0.00","5","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6009709822443","","","inclusive");
INSERT INTO product VALUES("663","juicy guava","juicy guava","0.00","6","default.gif","0","0","1","0","0","Non","0000-00-00 00:00:00","1","1","6161109782496","","","inclusive");
INSERT INTO product VALUES("664","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("665","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("666","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("667","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("668","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("669","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("670","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("671","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("672","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("673","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("674","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("675","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("676","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("677","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("678","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("679","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("680","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("681","","","0.00","","","0","0","0","0","0","","0000-00-00 00:00:00","1","1","","","","");
INSERT INTO product VALUES("682","test","","2.00","2","default.gif","63","920","1","0","11","0","2020-01-03 10:20:54","5","5","0.0","","","inclusive");
INSERT INTO product VALUES("683","FUNDBAT","","0.00","18","default.gif","63","24","1","0","11","0","2019-12-30 18:05:22","5","5","8906009234397","","","free");



CREATE TABLE `purchase_request` (
  `pr_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `request_date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `purchase_status` varchar(10) NOT NULL,
  PRIMARY KEY (`pr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `reversals_log` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `qty` int(12) NOT NULL,
  `action` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_no` text NOT NULL,
  `user_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO reversals_log VALUES("5","4","1","Deleted","2019-09-18 11:11:09","3531","4");
INSERT INTO reversals_log VALUES("6","29","20","Edited","2019-09-18 11:11:32","3531","4");
INSERT INTO reversals_log VALUES("7","767","6","Deleted","2019-12-08 11:10:13","3092","4");
INSERT INTO reversals_log VALUES("8","682","1","Deleted","2020-01-03 10:20:54","2479","4");



CREATE TABLE `sales` (
  `sales_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cash_tendered` decimal(10,2) DEFAULT NULL,
  `discount` text,
  `amount_due` decimal(10,2) NOT NULL,
  `cash_change` decimal(10,2) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `modeofpayment` varchar(15) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `order_no` int(12) NOT NULL,
  `special_remarks` text NOT NULL,
  PRIMARY KEY (`sales_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

INSERT INTO sales VALUES("11","0","8","80.00","0","2.00","8.00","2019-12-19 10:41:59","1","1","0.00","942","");
INSERT INTO sales VALUES("12","0","8","70.00","0","10.00","6.00","2019-12-19 10:50:34","1","1","0.00","3040","");
INSERT INTO sales VALUES("13","0","8","18.00","0","0.00","0.00","2019-12-19 14:57:47","1","1","0.00","4239","");
INSERT INTO sales VALUES("14","0","8","2.00","0","0.00","0.00","2019-12-19 14:58:46","1","1","0.00","724","");
INSERT INTO sales VALUES("15","0","8","2.00","0","2.00","0.00","2019-12-19 14:59:20","cash","1","2.00","4323","");
INSERT INTO sales VALUES("16","0","8","4.00","0","4.00","0.00","2019-12-19 16:01:13","cash","1","4.00","3942","");
INSERT INTO sales VALUES("17","0","8","50.00","0","4.00","46.00","2019-12-19 16:10:18","cash","1","4.00","2848","Chilli Hot");
INSERT INTO sales VALUES("18","0","8","3.00","0","0.00","1.00","2019-12-22 16:46:32","1","1","0.00","4059","");
INSERT INTO sales VALUES("19","0","8","60.00","0","0.00","6.00","2019-12-22 16:47:58","1","1","0.00","2231","");
INSERT INTO sales VALUES("20","0","8","50.00","0","20.00","16.00","2019-12-30 18:05:22","1","1","0.00","2114","");
INSERT INTO sales VALUES("21","0","8","1.00","0","1.00","0.00","2019-12-30 18:05:50","1","1","0.00","2479","");



CREATE TABLE `sales_details` (
  `sales_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `order_no` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `discount` text NOT NULL,
  `discount_type` text NOT NULL,
  PRIMARY KEY (`sales_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

INSERT INTO sales_details VALUES("44","11","683","18.00","4","942","8","2","Amount");
INSERT INTO sales_details VALUES("45","11","682","2.00","1","942","8","","");
INSERT INTO sales_details VALUES("46","12","683","18.00","4","3040","8","10","Amount");
INSERT INTO sales_details VALUES("47","12","682","2.00","1","3040","8","","");
INSERT INTO sales_details VALUES("48","13","683","18.00","1","4239","8","","");
INSERT INTO sales_details VALUES("49","14","682","2.00","1","724","8","","");
INSERT INTO sales_details VALUES("50","15","682","2.00","1","4323","8","","");
INSERT INTO sales_details VALUES("51","16","682","2.00","2","3942","8","","");
INSERT INTO sales_details VALUES("52","17","682","2.00","2","2848","8","","");
INSERT INTO sales_details VALUES("53","18","682","2.00","1","4059","8","","");
INSERT INTO sales_details VALUES("54","19","683","18.00","3","2231","8","","");
INSERT INTO sales_details VALUES("55","20","683","18.00","3","2114","8","20","Amount");



CREATE TABLE `sales_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `item_sold_id` int(12) NOT NULL,
  `quantity` int(12) NOT NULL,
  `sales_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(12) NOT NULL,
  `price` int(12) NOT NULL,
  `date` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `shop_category_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO shop_category_tb VALUES("1","Bar");
INSERT INTO shop_category_tb VALUES("2","Restruant");



CREATE TABLE `stock_audit_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` text NOT NULL,
  `count` int(12) NOT NULL,
  `added_to` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `action` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

INSERT INTO stock_audit_tb VALUES("1","PARM/BANAN LOW FAT YOG 175G","10","New Station Mumbwa","2019-10-14 17:51:32","Edited");
INSERT INTO stock_audit_tb VALUES("2","Classic Choclate","100","New Station Mumbwa","2019-10-18 08:11:59","Added");
INSERT INTO stock_audit_tb VALUES("3","Parmalat Milk","500","New Station Mumbwa","2019-10-18 08:12:28","Added");
INSERT INTO stock_audit_tb VALUES("4","Added Item","21","Kitchen","2019-11-11 08:49:43","Added");
INSERT INTO stock_audit_tb VALUES("5","Added Item","21","Kitchen","2019-11-11 09:04:50","Edited");
INSERT INTO stock_audit_tb VALUES("6","Added Item","1","Kitchen","2019-11-11 15:55:24","Deleted");
INSERT INTO stock_audit_tb VALUES("7","Added Item","6","Kitchen","2019-11-11 15:55:38","Added");
INSERT INTO stock_audit_tb VALUES("8","TEST VAT","6","Kitchen","2019-11-11 15:57:09","Added");
INSERT INTO stock_audit_tb VALUES("9","TEST VAT","6","Kitchen","2019-11-11 16:04:32","Edited");
INSERT INTO stock_audit_tb VALUES("10","Parmalat Milk","500","Kitchen","2019-11-11 16:10:55","Edited");
INSERT INTO stock_audit_tb VALUES("11","Coolant","50","Kitchen","2019-11-19 12:11:15","Added");
INSERT INTO stock_audit_tb VALUES("12","Shampoo","30","Kitchen","2019-12-01 15:35:58","Added");
INSERT INTO stock_audit_tb VALUES("13","FANTA ORANGE 500ML","600","Kitchen","2019-12-01 15:36:18","Added");
INSERT INTO stock_audit_tb VALUES("14","test","2","Kitchen","2019-12-14 17:13:47","Added");
INSERT INTO stock_audit_tb VALUES("15","test","1000","Kitchen","2019-12-15 16:12:07","Edited");
INSERT INTO stock_audit_tb VALUES("16","FUNDBAT","20","Kitchen","2019-12-17 15:47:02","Added");
INSERT INTO stock_audit_tb VALUES("17","FUNDBAT","30","Kitchen","2019-12-22 16:47:10","Edited");



CREATE TABLE `stock_damages_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `no_damages` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `stock_purchases_tb` (
  `purchase_id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `qty` text NOT NULL,
  `user_id` int(12) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `supplier_id` int(12) NOT NULL,
  `status` text NOT NULL,
  `invoice` int(12) NOT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO stock_purchases_tb VALUES("3","779","300","4","2019-10-14 18:28:41","14","purchased","1992");
INSERT INTO stock_purchases_tb VALUES("5","406","580","4","2019-10-14 18:55:22","11","purchased","1993");
INSERT INTO stock_purchases_tb VALUES("7","767","701","4","2019-10-27 08:19:25","11","purchased","0");
INSERT INTO stock_purchases_tb VALUES("8","471","890","4","2019-10-27 08:19:25","11","purchased","0");
INSERT INTO stock_purchases_tb VALUES("9","501","450","4","2019-10-27 08:19:25","11","purchased","0");
INSERT INTO stock_purchases_tb VALUES("10","770","780","4","2019-10-27 08:19:25","11","purchased","0");



CREATE TABLE `stock_trasfers_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `qty` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `moved_to` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `stockin` (
  `stockin_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `qty` int(6) NOT NULL,
  `date` datetime NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`stockin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `stores_branch` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `branch_name` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO stores_branch VALUES("5","Kitchen","2019-10-30 11:07:24");
INSERT INTO stores_branch VALUES("6","CAFE","2019-10-30 11:07:15");
INSERT INTO stores_branch VALUES("7","BAR","2019-10-30 11:07:09");



CREATE TABLE `supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_address` varchar(300) NOT NULL,
  `supplier_contact` varchar(50) NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

INSERT INTO supplier VALUES("11","AQUARITE LIMITED","Stand 8643, Chinika Industrial Area
Lusaka","Ashish");
INSERT INTO supplier VALUES("12","BIEN DONNE FARM (LTD)","
Lusaka","Ashish");
INSERT INTO supplier VALUES("13","C & M HOLDSWORTHS LTD","
Lusaka","Ashish");
INSERT INTO supplier VALUES("14","CAPITAL FISHERIES","
Lusaka","Ashish");
INSERT INTO supplier VALUES("15","COMIC GENERAL DEALERS ","
Lusaka","Ashish");
INSERT INTO supplier VALUES("16","CONTINENTAL MILLING CO. LTD","
Lusaka","Ashish");
INSERT INTO supplier VALUES("17","EVERJEMU ENTERPRISES LTD","
Lusaka","Ashish");
INSERT INTO supplier VALUES("18","GRANNYS BAKERY LTD","
Lusaka","Ashish");
INSERT INTO supplier VALUES("19","HAMSGOLD INVESTMENTS LTD","
Lusaka","Ashish");
INSERT INTO supplier VALUES("20","PRODUCTION","
Lusaka","Ashish");
INSERT INTO supplier VALUES("21","OTHERS","
Lusaka","Ashish");



CREATE TABLE `supplier_payments_tb` (
  `payment_id` int(12) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount_paid` text NOT NULL,
  `balance` text NOT NULL,
  `total_amount` text NOT NULL,
  `status` text NOT NULL,
  `invoice_no` int(12) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `temp_trans` (
  `temp_trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` text NOT NULL,
  `branch_id` int(11) NOT NULL,
  `user_id` int(12) NOT NULL,
  `discount_type` text NOT NULL,
  `amount` text NOT NULL,
  PRIMARY KEY (`temp_trans_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO temp_trans VALUES("1","683","18.00","1","1","8","Amount","2");
INSERT INTO temp_trans VALUES("2","682","2.00","1","1","8","","");



CREATE TABLE `term` (
  `term_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_id` int(11) DEFAULT NULL,
  `payable_for` varchar(10) NOT NULL,
  `term` varchar(11) NOT NULL,
  `due` decimal(10,2) NOT NULL,
  `payment_start` date NOT NULL,
  `down` decimal(10,2) NOT NULL,
  `due_date` date NOT NULL,
  `interest` decimal(10,2) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`term_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `user_type` text NOT NULL,
  `branch_id_user` int(12) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO user VALUES("4","admin","21232f297a57a5a743894a0e4a801fc3","Mona Lisas","active","1","Admin","3");
INSERT INTO user VALUES("8","user","81dc9bdb52d04dc20036dbd8313ed055","test USer","active","1","User","5");
INSERT INTO user VALUES("10","user","2eeecd72c567401e6988624b179d0b14","Supervisor","active","1","Supervisor","0");
INSERT INTO user VALUES("11","Monde","d5c186983b52c4551ee00f72316c6eaa","Monde Sichuma","active","1","User","5");



CREATE TABLE `ware_house_tb` (
  `prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_name` varchar(100) NOT NULL,
  `prod_desc` varchar(500) NOT NULL,
  `prod_price` decimal(10,2) NOT NULL,
  `prod_sell_price` text NOT NULL,
  `prod_pic` varchar(300) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `prod_qty` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `serial` varchar(50) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(12) NOT NULL,
  `barcode` text NOT NULL,
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


