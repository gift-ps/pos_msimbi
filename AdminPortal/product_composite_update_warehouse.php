<?php
session_start();
if (empty($_SESSION['id'])):
    header('Location:../index.php');
endif;

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Warehouse Product Composites | <?php include('../dist/includes/title.php'); ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="../plugins/select2/select2.min.css">

        <style>
            select {
                display: block;
                width: 100%;
                height: 35px;
            }
        </style>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </head>
    <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
    <body class="hold-transition skin-<?php echo $_SESSION['skin']; ?> layout-top-nav">
        <div class="wrapper">
            <?php
            include('../dist/includes/header_admin.php');
            include('../dist/includes/dbcon.php');
            ?>
            <!-- Full Width Column -->
            <div class="content-wrapper">
                <div class="container">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            <a class="btn btn-lg btn-primary" href="product_composites_warehouse.php">Back</a>
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li><a href="product_composites_warehouse.php"><i class="fa fa-dashboard"></i> Warehouse Product Composites</a></li>
                            <li class="active"> Update</li>
                        </ol>
                    </section>

                    <?php
                        $composite_name = $_GET['name'];
                        $query1 = mysqli_query($con, "SELECT * FROM product_composites_warehouse WHERE composite_name = '$composite_name' ")or die(mysqli_error($con));
                        while ($row1 = mysqli_fetch_array($query1)) {
                            $discount = $row1['discount'];
                        }

                    ?>

                    <!-- Main content -->
                    <section class="content col-md-12">
                        <div style="display: right;">
                            <div class="col-md-4">
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title" style=" color: black"><b>Update <?php echo $composite_name ?> Composite</b></h3>
                                    </div>
                                    <div class="box-body">
                                        <?php include('product_composite_server.php') ?>
                                        <form autocomplete="off" method="post" action="#" enctype="multipart/form-data">
                                        
                                            <div class="form-group">
                                                <label for="date" style=" color: black"><b> Composite Name </b> </label>
                                                <div class="input-group col-md-12">
                                                    <input type="text" class="form-control pull-right" id="date" name="name" value="<?php echo $composite_name ?>" readonly>
                                                </div><!-- /.input group -->
                                            </div><!-- /.form group -->
                                            <?php
                                                // return var_dump($composite_name);
                                                function products_options($con){
                                                    $output = '';
                                                    $query = mysqli_query($con, "SELECT prod_id, prod_name FROM ware_house_tb ORDER BY prod_name ASC ")or die(mysqli_error($con));
                                                    while ($row = mysqli_fetch_array($query)) {
                                                        $output .= '<option value="'.$row["prod_id"].'">'.$row["prod_name"].'</option>';
                                                    }
                                                    return $output;
                                                }
                                            ?>

                                            <div class="form-group">
                                                <label for="date" style=" color: black"><b> Products </b> </label>
                                                <div class="input-group col-md-12">
                                                    <table class="table table-bordered" id="item_table">
                                                        <tr>
                                                            <th>Click to add/remove product </th>
                                                            <th><button type="button" name="add" class="btn btn-success btn-sm add"><span class="glyphicon glyphicon-plus"></span></button></th>
                                                        </tr>
                                                        <?php
                                                           $query2 = mysqli_query($con, "SELECT ware_house_tb.prod_id, ware_house_tb.prod_name AS prod_name FROM ware_house_tb 
                                                                        INNER JOIN product_composites_warehouse ON ware_house_tb.prod_id = product_composites_warehouse.prod_id 
                                                                        WHERE composite_name = '$composite_name' ")or die(mysqli_error($con));
                                                           while ($row2 = mysqli_fetch_array($query2)) {
                                                               $prod_name = $row2['prod_name'];
                                                               $prod_id = $row2['prod_id'];
                                                        ?><tr>
                                                            <td><select name="prod_id[]" class="form-control ">
                                                                    <option value="<?php echo $prod_id; ?>"> <?php echo $prod_name ?> </option>
                                                                </select></td>
                                                            <td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><span class="glyphicon glyphicon-minus"></span></button></td>
                                                            <tr>
                                                        <?php } ?>
                                                        
                                                    </table> 
                                                </div><!-- /.input group -->
                                            </div><!-- /.form group -->

                                            <div class="form-group">
                                                <label style="color: black"><b> Discount </b> <small> (in percentage) </small> </label>
                                                <div class="input-group col-md-12">
                                                    <input type="number" min="0" max="100" class="form-control pull-right" value="<?php echo $discount; ?>" name="discount" placeholder="Discount (%)" required>
                                                </div><!-- /.input group -->
                                            </div><!-- /.form group -->

                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="submit" value="Update" name="update_composite_warehouse" class="form-control btn btn-primary btn-sm" />
                                                </div>
                                            </div><!-- /.form group -->
                                        </form>	
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                            </div><!-- /.col (right) -->

                        </div>


                    </section><!-- /.content -->
                </div><!-- /.container -->
            </div><!-- /.content-wrapper -->
            <?php include('../dist/includes/footer.php'); ?>
        </div><!-- ./wrapper -->

        <!-- jQuery 2.1.4 -->
        <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../plugins/fastclick/fastclick.min.js"></script>
        <!-- AdminLTE App -->
        <script src="../dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../dist/js/demo.js"></script>
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>



        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });

        </script>
    </body>
</html>

<script>
  $(document).ready(function(){
      $(document).on('click', '.add', function(){
        var html = '';
        html += '<tr>';
        html += '<td><select name="prod_id[]" class="form-control  select2"><option> </option><?php echo products_options($con); ?></select></td>';
        html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><span class="glyphicon glyphicon-minus"></span></button></td></tr>';
        $('#item_table').append(html);
      });
  
      $(document).on('click', '.remove', function(){
        $(this).closest('tr').remove();
      });

  });

  $(".select2").select2();

</script>
<script src="../plugins/select2/select2.full.min.js"></script>       
