<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DAO
 *
 * @author User
 */
class DAO
{

    function getTotalUnitsSoldWithTAX()
    {
    }

    public function getTotalVatTax($param)
    {
    }

    public function getEarPieceSales($param)
    {
    }

    public function getInventoryDashboard($con)
    {
        $query = mysqli_query($con, "SELECT prod_name,prod_qty,(prod_sell_price*prod_qty) AS value FROM `product` GROUP BY prod_name") or die("Err 6 " . mysqli_error($con));
        return $query;
    }

    public function layBySalesByStore($param)
    {
    }

    public function getTotalUnitsSoldWithNoTAX($param)
    {
    }

    public function getStockValues($param)
    {
    }

    public function getTopStockOuts($con)
    {
        $query = mysqli_query($con, "SELECT product.prod_name,product.prod_qty,(product.prod_sell_price*product.prod_qty) AS value,COUNT(purchase_orders.prod_id) AS no_purchases FROM `purchase_orders` INNER JOIN product on product.prod_id=purchase_orders.prod_id
        WHERE status='closed'
        GROUP BY purchase_orders.prod_id ORDER BY COUNT(purchase_orders.prod_id) DESC LIMIT 5") or die("Err 6 " . mysqli_error($con));
        return $query;
    }

    public function getAgedStock($con)
    {

        // check for products that were added 3 months ago, 
        // SELECT * FROM `product` WHERE MONTH(DATE(DATE_ADD(product.date_added, INTERVAL 90 DAY))) =  MONTH(DATE( NOW()))   
        // check that the product has not been sold in 3 months...

        $query = mysqli_query($con, "SELECT product.prod_name,SUM(product.prod_qty) AS prod_qty,SUM((product.prod_sell_price*product.prod_qty)) AS value FROM `product` WHERE MONTH(DATE(DATE_ADD(product.date_added, INTERVAL 90 DAY))) =  MONTH(DATE( NOW())) GROUP BY prod_name") or die("Err 6 " . mysqli_error($con));
        return $query;
    }

    public function getDeadStock($con)
    {

        // dead stock..

        $query = mysqli_query($con, "SELECT * FROM `product` WHERE ( DATE(DATE_ADD(product.date_added, INTERVAL 365 DAY)) =  DATE( NOW()) "
            . " OR DATE(DATE_ADD(product.date_added, INTERVAL 183 DAY)) =  DATE( NOW()))") or die("Err 6 " . mysqli_error($con));
        return $query;
    }

    // customer functions...


    public function getRepeatCustomerList($con)
    {
        $query = mysqli_query($con, "SELECT customer.cust_contact,cust_first,cust_last,amount_due FROM sales INNER JOIn customer on customer.cust_id=sales.cust_id
          GROUP BY sales.cust_id 
          HAVING count(sales.cust_id) >=3") or die("Err 6 " . mysqli_error($con));
        return $query;
    }

    public function getNewCustomers($con)
    {
        $query = mysqli_query($con, "select COUNT(*) AS new_customers from customer
        where yearweek(date_added) = yearweek(curdate())") or die("Err 6 " . mysqli_error($con));
        $row = mysqli_fetch_array($query);
        $new_customers = $row['new_customers'];
        return $new_customers;
    }

    public function getCustomersPreviousWeek($con)
    {
        $query = mysqli_query($con, "select COUNT(*) AS prev_customer from customer
        where WEEK (date_added) = WEEK( current_date ) - 1 AND YEAR( date_added) = YEAR( current_date )") or die("Err 6 " . mysqli_error($con));


        $row = mysqli_fetch_array($query);
        $prev_customer = $row['prev_customer'];
        return $prev_customer;
    }

    public function getCustomerLatMonth($con)
    {
        $query = mysqli_query($con, "select COUNT(*) AS prev_month from customer
        where WEEK (date_added) = WEEK( current_date ) - 1 AND YEAR( date_added) = YEAR( current_date )") or die("Err 6 " . mysqli_error($con));
        $row = mysqli_fetch_array($query);
        $prev_month = $row['prev_month'];
        return $prev_month;
    }

    public function getRiskRepeatCustomers($param)
    {
    }

    public function getAmountCollectedForMonth($con)
    {
        $query = mysqli_query($con, "select SUM(amount_due) AS total_collected from sales WHERE MONTH(DATE(sales.date_added))= MONTH(DATE(NOW()))") or die("Err 6 " . mysqli_error($con));
        $row = mysqli_fetch_array($query);
        $total_collected = $row['total_collected'];
        return $total_collected;
    }

    public function getTotalCustomersMonth($con)
    {
        $query = mysqli_query($con, "select COUNT(cust_id) AS number_transactions from sales WHERE MONTH(DATE(sales.date_added))= MONTH(DATE(NOW()))") or die("Err 6 " . mysqli_error($con));
        $row = mysqli_fetch_array($query);
        $number_transactions = $row['number_transactions'];
        return $number_transactions;
    }

    public function getTotalCustomerTransactionsMonth($con)
    {
        $query = mysqli_query($con, "select COUNT(cust_id) AS number_transactions from sales WHERE MONTH(DATE(sales.date_added))= MONTH(DATE(NOW()))") or die("Err 6 " . mysqli_error($con));
        return $query;
    }

    public function customerConversionRate($con)
    {
        $conversion = ($this->getTotalCustomersMonth($con) / $this->getTotalCustomerTransactionsMonth($con)) * 100;
        return $conversion;
    }

    public function customerSpending($con)
    {
        $spending = ($this->getAmountCollectedForMonth($con) / $this->getTotalCustomersMonth($con)) * 100;
        return $spending;
    }

    public function getGrowthPreviousWeek($param)
    {
    }

    public function getGrowthPreviousMonth($param)
    {
    }

    public function getSalesByPaymentMethod($con, $startDate, $stop_date, $branch_id)
    {

        if ($branch_id == "all_branches") {
            $queryPayments = mysqli_query($con, "SELECT modes_of_payment_tb.name AS modepayment,SUM(sales.amount_due) AS total  FROM sales   
                                                            INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment 
                                                            INNER JOIN user on user.user_id=sales.user_id
                                                            WHERE  sales.date_added BETWEEN '$startDate' AND '$stop_date' 
                                                            AND sales_id NOT IN (SELECT sales_id from split_payments WHERE DATE(sales.date_added)=DATE(NOW()))        
                                                            GROUP BY modes_of_payment_tb.payment_mode_id") or die("Err 6 " . mysqli_error($con));

            //  echo '6 '.$branch_id;

            $querySplitPayments = mysqli_query($con, "SELECT split_payments.date_added,split_payments.amount AS total,modes_of_payment_tb.name AS modepayment FROM `split_payments`
                                                    INNER JOIN sales on sales.sales_id=split_payments.sales_id
                                                    INNER JOIN user on user.user_id=sales.user_id
                                                    INNER JOiN modes_of_payment_tb on modes_of_payment_tb.payment_mode_id=split_payments.payment_id
                                                    WHERE sales.date_added BETWEEN '$startDate' AND '$stop_date'  ") or die("Err 7 " . mysqli_error($con));
        } else {
            $queryPayments = mysqli_query($con, "SELECT modes_of_payment_tb.name AS modepayment,SUM(sales.amount_due) AS total  FROM sales   
                                                            INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment 
                                                            INNER JOIN user on user.user_id=sales.user_id
                                                            WHERE  sales.date_added BETWEEN '$startDate' AND '$stop_date' AND sales.branch_id='$branch_id'
                                                            AND sales_id NOT IN (SELECT sales_id from split_payments WHERE DATE(sales.date_added)=DATE(NOW()))        
                                                            GROUP BY modes_of_payment_tb.payment_mode_id") or die("Err 6 " . mysqli_error($con));

            //  echo '6 '.$branch_id;

            $querySplitPayments = mysqli_query($con, "SELECT split_payments.date_added,split_payments.amount AS total,modes_of_payment_tb.name AS modepayment FROM `split_payments`
                                                    INNER JOIN sales on sales.sales_id=split_payments.sales_id
                                                    INNER JOIN user on user.user_id=sales.user_id
                                                    INNER JOiN modes_of_payment_tb on modes_of_payment_tb.payment_mode_id=split_payments.payment_id
                                                    WHERE sales.date_added BETWEEN '$startDate' AND '$stop_date' AND sales.branch_id='$branch_id'  ") or die("Err 7 " . mysqli_error($con));
        }
    }

    public function salesBySalesPerson($param)
    {
    }

    // Sales Dashboard

    function sales_by_payment_method($con, $date_filter)
    {
        $query = mysqli_query($con, "SELECT sales.modeofpayment AS mop,

            (SELECT SUM(sales_details.price)*qty FROM sales_details INNER JOIN sales ON sales.sales_id = sales_details.sales_id WHERE sales.modeofpayment = mop) AS total_sales,
             
            modes_of_payment_tb.name AS modepayment
            FROM sales_details 
            INNER JOIN sales ON sales.sales_id=sales_details.sales_id 
            INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment
            $date_filter
            GROUP BY sales.modeofpayment") or die("Err sales_by_payment_method: " . mysqli_error($con));

        echo ' <center><h3 class="box-title" style=" color: black"><b><u> Sales By Payment Method </u></b></h3></center><br>';
        return $query;
    }

    function best_selling_products($con, $date_filter)
    {

        $query = mysqli_query($con, "SELECT sales.modeofpayment,
                product.prod_name,
                SUM(qty) AS qty,
                SUM(sales_details.price)*qty AS total_price,
                (SELECT SUM(sales_details.price)*qty FROM sales_details) AS total_sales,
                (SELECT SUM(sales_details.qty) FROM sales_details) AS total_qty,
                ((SUM(sales_details.price)*qty) / (SELECT SUM(sales_details.price)*qty FROM sales_details))*100 AS percentage

                FROM sales_details 
                INNER JOIN product ON product.prod_id = sales_details.prod_id
                INNER JOIN sales ON sales.sales_id=sales_details.sales_id 
                
                
                $date_filter
                
                GROUP BY sales_details.prod_id ORDER BY total_price DESC LIMIT 5") or die("Err 'best_selling_products':: " . mysqli_error($con));

        echo ' <center><h3 class="box-title" style=" color: black"><b><u> Top 5 Sellers </u></b></h3></center><br>';
        return $query;
    }

    function worst_selling_products($con, $date_filter)
    {
        $query = mysqli_query($con, "SELECT sales.modeofpayment,
                product.prod_name,
                SUM(qty) AS qty,
                SUM(sales_details.price)*qty AS total_price,
                (SELECT SUM(sales_details.price)*qty FROM sales_details) AS total_sales,
                (SELECT SUM(sales_details.qty) FROM sales_details) AS total_qty,
                ((SUM(sales_details.price)*qty) / (SELECT SUM(sales_details.price)*qty FROM sales_details))*100 AS percentage                         

                FROM sales_details 
                INNER JOIN product ON product.prod_id = sales_details.prod_id
                INNER JOIN sales ON sales.sales_id=sales_details.sales_id 
                $date_filter
                
                GROUP BY sales_details.prod_id ORDER BY total_price ASC LIMIT 5") or die("Err 'worst_selling_products':: " . mysqli_error($con));

        echo ' <center><h3 class="box-title" style=" color: black"><b><u> Bottom 5 Sellers </u></b></h3></center><br>';
        return $query;
    }

    function sales_by_salesperson($con, $date_filter)
    {
        $query = mysqli_query($con, "SELECT SUM(sales_details.qty) as  qty,
        SUM(sales_details.price*sales_details.qty) as total, user.name,
        ((SUM(sales_details.price)*qty) / (SELECT SUM(sales_details.price)*qty FROM sales_details))*100 AS contribution

        FROM `sales_details` 
        INNER JOIN sales on sales.sales_id=sales_details.sales_id
        INNER JOIN user on user.user_id=sales_details.user_id
        $date_filter
        GROUP BY name 
        ORDER BY total DESC") or die("Error sales_by_salesperson: " . mysqli_error($con));

        echo ' <center><h3 class="box-title" style=" color: black"><b><u> Sales By Salesperson </u></b></h3></center><br>';
        return $query;
    }


    function sales_by_earpircing($con, $date_filter)
    {
        $query = mysqli_query($con, "SELECT product.prod_id,
                SUM(qty) AS qty,
                SUM(sales_details.price)*qty AS total_sales,
                ((SUM(sales_details.price)*qty) / (SELECT SUM(sales_details.price)*qty FROM sales_details))*100 AS percentage

                FROM sales_details 
                INNER JOIN product ON product.prod_id = sales_details.prod_id
                INNER JOIN sales ON sales.sales_id=sales_details.sales_id 
                WHERE product.prod_name  LIKE 'EAR PIE%'
                $date_filter
                
                GROUP BY sales_details.prod_id
                ORDER BY prod_name") or die("Err sales_by_earpircing: 'best_selling_products':: " . mysqli_error($con));

        echo ' <center><h3 class="box-title" style=" color: black"><b><u> Sales By EARPIERCING </u></b></h3></center><br>';
        return $query;
    }

    function profit_loss($con)
    {
        $query = mysqli_query($con, "SELECT SUM(sales_details.price)*qty AS total_sales,
                (SELECT SUM(amount) FROM expenses_tb) AS expense,
                ((SUM(sales_details.price)*qty) - (SELECT SUM(amount) FROM expenses_tb) ) AS net_sales

                FROM sales_details
                ") or die("Errooooor 'profit_loss': " . mysqli_error($con));

        echo ' <center><h3 class="box-title" style=" color: black"><b><u> Profit And Loss </u></b></h3></center><br>';
        return $query;
    }

    public function getPartPayments($con, $date_filter_part_pay) {
        $partPayments = mysqli_query($con, "SELECT SUM(amount) AS amount 
        FROM `part_payments_tb` 
        $date_filter_part_pay  ")or die("Err getPartPayments " . mysqli_error($con));
        $row = mysqli_fetch_array($partPayments);
        $amount = $row['amount'];
        return $amount;
    }

    function getTotalSales($con, $date_filter_where, $date_filter_part_pay) {
        $salesTotals = mysqli_query($con, "SELECT SUM(amount_due) as amount_due 
        FROM `sales` 
        $date_filter_where ")or die("Err getTotalSales " . mysqli_error($con));
        $row = mysqli_fetch_array($salesTotals);
        $amount_due = $row['amount_due'] + $this->getPartPayments($con, $date_filter_part_pay);
        return $amount_due;
    }

    public function getTotalExpenses($con, $date_filter) {
        $query = mysqli_query($con, "SELECT SUM(amount) AS total_expenses FROM expenses_tb 
        -- $date_filter
        ")or die("Error getTotalExpenses".mysqli_error($con));
        $row = mysqli_fetch_array($query);
        $total_expenses = $row['total_expenses'];
        return $total_expenses;
    }
















    function net_sales($con, $date_filter)
    {
        $query = mysqli_query($con, "SELECT SUM(sales_details.price*qty) AS cur_total_sales,
         (SELECT SUM(sales_details.qty) AS total_qty
         FROM sales_details
         INNER JOIN sales ON sales.sales_id=sales_details.sales_id
         $date_filter
         WHERE yearweek(sales.date_added) = yearweek(curdate())) AS current_gross_qty,
         
            ( 	  (SELECT SUM(sales_details.price*qty) AS prev_week_sales
                FROM sales_details
                INNER JOIN sales ON sales.sales_id=sales_details.sales_id
                WHERE yearweek(sales.date_added) = yearweek(curdate()))
                $date_filter
            
            - (SELECT SUM(sales_details.price*qty)
                    FROM sales_details
                    INNER JOIN sales ON sales.sales_id=sales_details.sales_id
                    WHERE yearweek(sales.date_added) = yearweek(curdate()) - 1)
                    $date_filter
            )/ 100 AS prev_week_growth,
        
            ( 	  (SELECT SUM(sales_details.price*qty) AS prev_week_sales
                FROM sales_details
                INNER JOIN sales ON sales.sales_id=sales_details.sales_id
                WHERE yearweek(sales.date_added) = yearweek(curdate())) 
                $date_filter
            
            - (SELECT SUM(sales_details.price*qty)
                    FROM sales_details
                    INNER JOIN sales ON sales.sales_id=sales_details.sales_id
                    WHERE MONTH (sales.date_added) = MONTH(curdate()) - 1 AND YEAR( date_added) = YEAR( current_date ))
                    $date_filter
            )/ 100 AS prev_month_growth
        
            FROM sales_details
            INNER JOIN sales ON sales.sales_id=sales_details.sales_id
            WHERE yearweek(sales.date_added) = yearweek(curdate())
        $date_filter") or die("Err gross_rate: " . mysqli_error($con));

        echo ' <center><h3 class="box-title" style=" color: black"><b><u> Net Sales </u></b></h3></center><br>';
        return $query;
    }

    public function getSalesPreviousWeek($con)
    {
        $query = mysqli_query($con, "SELECT SUM(sales_details.price*qty) AS prev_week_sales
                    FROM sales_details
                    INNER JOIN sales ON sales.sales_id=sales_details.sales_id
                    WHERE yearweek(sales.date_added) = yearweek(curdate()) - 1
        ")
        or die("Err getSalesPreviousWeek " . mysqli_error($con));

        $row = mysqli_fetch_array($query);
        $prev_week_sales = $row['prev_week_sales'];
        return $prev_week_sales;
    }

    public function getSalesPreviousMonth($con)
    {
        $query = mysqli_query($con, "SELECT SUM(sales_details.price*qty) AS prev_month_sales
            FROM sales_details
            INNER JOIN sales ON sales.sales_id=sales_details.sales_id
            WHERE MONTH (sales.date_added) = MONTH(curdate()) - 1 AND YEAR( date_added) = YEAR( current_date )

            ") or die("Err getSalesPreviousMonth " . mysqli_error($con));
        $row = mysqli_fetch_array($query);
        $prev_month_sales = $row['prev_month_sales'];
        return $prev_month_sales;
    }

    function gross_sales($con)
    {
        $query = mysqli_query($con, "SELECT SUM(sales_details.price*qty) AS cur_total_sales,
         (SELECT SUM(sales_details.qty) AS total_qty
         FROM sales_details
         INNER JOIN sales ON sales.sales_id=sales_details.sales_id
         WHERE yearweek(sales.date_added) = yearweek(curdate())) AS current_gross_qty,
         
         ( 	  (SELECT SUM(sales_details.price*qty) AS prev_week_sales
              FROM sales_details
              INNER JOIN sales ON sales.sales_id=sales_details.sales_id
             WHERE yearweek(sales.date_added) = yearweek(curdate())) 
          
          - (SELECT SUM(sales_details.price*qty)
                FROM sales_details
                INNER JOIN sales ON sales.sales_id=sales_details.sales_id
                WHERE yearweek(sales.date_added) = yearweek(curdate()) - 1)
          )/ 100 AS prev_week_growth,
    
          ( 	  (SELECT SUM(sales_details.price*qty) AS prev_week_sales
              FROM sales_details
              INNER JOIN sales ON sales.sales_id=sales_details.sales_id
             WHERE yearweek(sales.date_added) = yearweek(curdate())) 
          
          - (SELECT SUM(sales_details.price*qty)
                FROM sales_details
                INNER JOIN sales ON sales.sales_id=sales_details.sales_id
                WHERE MONTH (sales.date_added) = MONTH(curdate()) - 1 AND YEAR( date_added) = YEAR( current_date ))
          )/ 100 AS prev_month_growth
    
        FROM sales_details
        INNER JOIN sales ON sales.sales_id=sales_details.sales_id
        WHERE yearweek(sales.date_added) = yearweek(curdate())") or die("Err gross_rate: " . mysqli_error($con));

        echo ' <center><h3 class="box-title" style=" color: black"><b><u> Gross Sales </u></b></h3></center><br>';
        return $query;
    }


    function current_gross_sales($con)
    {
        $query = mysqli_query($con, "SELECT SUM(sales_details.price*qty) AS cur_total_sales
            FROM sales_details
            INNER JOIN sales ON sales.sales_id=sales_details.sales_id
            WHERE yearweek(sales.date_added) = yearweek(curdate())") or die("Err current_gross_sales: " . mysqli_error($con));

        $row = mysqli_fetch_array($query);
        $cur_total_sales = $row['cur_total_sales'];

        return $cur_total_sales;
    }

    function current_gross_qty($con)
    {
        $query = mysqli_query($con, "SELECT SUM(sales_details.qty) AS total_qty
            FROM sales_details
            INNER JOIN sales ON sales.sales_id=sales_details.sales_id
            WHERE yearweek(sales.date_added) = yearweek(curdate())") or die("Err current_gross_sales: " . mysqli_error($con));

        $row = mysqli_fetch_array($query);
        $total_qty = $row['total_qty'];

        return $total_qty;
    }

    function prev_week_gross_sales($con)
    {
        $query = mysqli_query($con, "SELECT SUM(sales_details.price*qty) AS prev_week_sales
            FROM sales_details
            INNER JOIN sales ON sales.sales_id=sales_details.sales_id
            WHERE WEEK(sales.date_added) = WEEK(curdate()) - 1 AND YEAR(sales.date_added) = YEAR(current_date)")
            or die("Err prev_week_gross_sales: " . mysqli_error($con));

        $row = mysqli_fetch_array($query);
        $prev_week_sales = $row['prev_week_sales'];
        return $prev_week_sales;
    }

    function prev_month_gross_sales($con)
    {
        $query = mysqli_query($con, "SELECT SUM(sales_details.price*qty) AS prev_month_sales
        FROM sales_details
        INNER JOIN sales ON sales.sales_id=sales_details.sales_id
        WHERE MONTH (date_added) = MONTH( current_date ) - 1 AND YEAR( date_added) = YEAR( current_date )")
            or die("Err prev_month_gross_sales " . mysqli_error($con));
        $row = mysqli_fetch_array($query);
        $prev_month = $row['prev_month_sales'];
        return $prev_month;
    }

    function growth_rate($previous_yr_sales)
    {
    }

    function sales_per_square_meter($square_meter, $rent)
    {
    }

    function top_stock_out()
    {
    }
}
