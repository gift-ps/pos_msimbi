<?php

session_start();
if (empty($_SESSION['id'])):
    header('Location:../index.php');
endif;

include('../dist/includes/dbcon.php');

$product = $_POST['product'];
$type_adjustment = $_POST['type_adjustment'];
$instruction = $_POST['instruction'];
$amount = $_POST['amount'];
$userid = $_SESSION['id'];

if ($product == "all") {

    // check the type of adjustment... 

    if ($type_adjustment == "Percent") {

        // check if the change is an increase or decrease ...

        if ($instruction == "Increase") {
            $updateAllPrices = mysqli_query($con, "SELECT prod_sell_price,prod_id FROM product ")or die(mysqli_error($con));
            while ($row = mysqli_fetch_array($updateAllPrices)) {
                $previousPrice = $row['prod_sell_price'];
                $adjustment = ($amount / 100) * $previousPrice;
                $increasePrices = $previousPrice + $adjustment;
                $prod_id = $row['prod_id'];

                // create a log for the adjustment.. 

                mysqli_query($con, "update product set prod_sell_price='$increasePrices' WHERE prod_id='$prod_id' ")or die(mysqli_error($con));
            }
            
            mysqli_query($con, "INSERT INTO price_adjustments_tb(product,type_adjustment,instruction,amount,user_id)
			VALUES('All','$type_adjustment','$instruction','$amount','$userid')")or die(mysqli_error($con));
        } else {
            $updateAllPrices = mysqli_query($con, "SELECT prod_sell_price,prod_id FROM product ")or die(mysqli_error($con));
            while ($row = mysqli_fetch_array($updateAllPrices)) {
                $previousPrice = $row['prod_sell_price'];
                $adjustment = ($amount / 100) * $previousPrice;
                $decreasePrices = $previousPrice - $adjustment;
                $prod_id = $row['prod_id'];

                mysqli_query($con, "update product set prod_sell_price='$decreasePrices' WHERE prod_id='$prod_id' ")or die(mysqli_error($con));
            }
            mysqli_query($con, "INSERT INTO price_adjustments_tb(product,type_adjustment,instruction,amount,user_id)
			VALUES('All','$type_adjustment','$instruction','$amount','$userid')")or die(mysqli_error($con));
        }
    } else {

        // adjust prices based on amounts.. 

        if ($instruction == "Increase") {

            $updateAllPrices = mysqli_query($con, "SELECT prod_sell_price,prod_id FROM product ")or die(mysqli_error($con));
            while ($row = mysqli_fetch_array($updateAllPrices)) {
                $previousPrice = $row['prod_sell_price'];
                $prod_id = $row['prod_id'];

                mysqli_query($con, "update product set prod_sell_price=prod_sell_price+'$amount' WHERE prod_id='$prod_id' ")or die(mysqli_error($con));
            }
            mysqli_query($con, "INSERT INTO price_adjustments_tb(product,type_adjustment,instruction,amount,user_id)
			VALUES('All','$type_adjustment','$instruction','$amount','$userid')")or die(mysqli_error($con));
        } else {
            $updateAllPrices = mysqli_query($con, "SELECT prod_sell_price,prod_id FROM product ")or die(mysqli_error($con));
            while ($row = mysqli_fetch_array($updateAllPrices)) {
                $previousPrice = $row['prod_sell_price'];
                $prod_id = $row['prod_id'];

                mysqli_query($con, "update product set prod_sell_price=prod_sell_price-'$amount' WHERE prod_id='$prod_id' ")or die(mysqli_error($con));
            }
            mysqli_query($con, "INSERT INTO price_adjustments_tb(product,type_adjustment,instruction,amount,user_id)
			VALUES('All','$type_adjustment','$instruction','$amount','$userid')")or die(mysqli_error($con));
        }
    }
} else {


    // check the type of adjustment... 

    if ($type_adjustment == "Percent") {

        // check if the change is an increase or decrease ...

        if ($instruction == "Increase") {
            $updateAllPrices = mysqli_query($con, "SELECT prod_sell_price,prod_id FROM product WHERE prod_id='$product' ")or die(mysqli_error($con));
            $row = mysqli_fetch_array($updateAllPrices);
            $previousPrice = $row['prod_sell_price'];
            $adjustment = ($amount / 100) * $previousPrice;
            $increasePrices = $previousPrice + $adjustment;

            // create a log for the adjustment.. 

            mysqli_query($con, "update product set prod_sell_price='$increasePrices' WHERE prod_id='$product' ")or die(mysqli_error($con));

            mysqli_query($con, "INSERT INTO price_adjustments_tb(product,type_adjustment,instruction,amount,user_id)
			VALUES('$product','$type_adjustment','$instruction','$amount','$userid')")or die(mysqli_error($con));
        } else {
            $updateAllPrices = mysqli_query($con, "SELECT prod_sell_price,prod_id FROM product WHERE prod_id='$product' ")or die(mysqli_error($con));
             $row = mysqli_fetch_array($updateAllPrices);
            $previousPrice = $row['prod_sell_price'];
            $adjustment = ($amount / 100) * $previousPrice;
            $decreasePrices = $previousPrice - $adjustment;

            mysqli_query($con, "update product set prod_sell_price='$decreasePrices' WHERE prod_id='$product' ")or die(mysqli_error($con));

            mysqli_query($con, "INSERT INTO price_adjustments_tb(product,type_adjustment,instruction,amount,user_id)
			VALUES('$product','$type_adjustment','$instruction','$amount','$userid')")or die(mysqli_error($con));
        }
    } else {

        // adjust prices based on amounts.. 

        if ($instruction == "Increase") {
            $updateAllPrices = mysqli_query($con, "SELECT prod_sell_price,prod_id FROM product WHERE prod_id='$product' ")or die(mysqli_error($con));
             $row = mysqli_fetch_array($updateAllPrices);
            $previousPrice = $row['prod_sell_price'];

            mysqli_query($con, "update product set prod_sell_price=prod_sell_price+'$amount' WHERE prod_id='$product' ")or die(mysqli_error($con));

            mysqli_query($con, "INSERT INTO price_adjustments_tb(product,type_adjustment,instruction,amount,user_id)
			VALUES('$product','$type_adjustment','$instruction','$amount','$userid')")or die(mysqli_error($con));
        } else {
            $updateAllPrices = mysqli_query($con, "SELECT prod_sell_price,prod_id FROM product WHERE prod_id='$product' ")or die(mysqli_error($con));
             $row = mysqli_fetch_array($updateAllPrices);
            $previousPrice = $row['prod_sell_price'];
           
            mysqli_query($con, "update product set prod_sell_price=prod_sell_price-'$amount' WHERE prod_id='$product' ")or die(mysqli_error($con));

            mysqli_query($con, "INSERT INTO price_adjustments_tb(product,type_adjustment,instruction,amount,user_id)
			VALUES('$product','$type_adjustment','$instruction','$amount','$userid')")or die(mysqli_error($con));
        }
    }
}

echo "<script type='text/javascript'>alert('Successfully Updated Prices!');</script>";
echo "<script>document.location='price-adjusgtments-menu.php'</script>";


