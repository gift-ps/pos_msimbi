<?php

header('Content-Type: application/json');

session_start();

$conn = mysqli_connect("localhost", "root", "", "inventory_db");

if ($_SESSION['startDate']=="today" ) {
    $sqlQuery = "SELECT product.prod_name,SUM(sales_details.qty*product.prod_sell_price) AS Total FROM `sales_details` INNER JOIN product "
            . "on product.prod_id=sales_details.prod_id INNER JOIN category on category.cat_id=product.cat_id "
            . " INNER JOIN sales on sales.sales_id=sales_details.sales_id AND DATE(sales.date_added) = DATE(NOW()) GROUP BY prod_name";
} else {
    $startDate = $_SESSION['startDate'];
    $stop_date = $_SESSION['stop_date'];

    $sqlQuery = "SELECT product.prod_name,SUM(sales_details.qty*product.prod_sell_price) AS Total FROM `sales_details` INNER JOIN product "
            . "on product.prod_id=sales_details.prod_id INNER JOIN category on category.cat_id=product.cat_id "
            . " INNER JOIN sales on sales.sales_id=sales_details.sales_id AND sales.date_added BETWEEN '$startDate' AND '$stop_date' GROUP BY prod_name";
}

$result = mysqli_query($conn, $sqlQuery);

$data = array();
foreach ($result as $row) {
    $data[] = $row;
}

mysqli_close($conn);

echo json_encode($data);
?>