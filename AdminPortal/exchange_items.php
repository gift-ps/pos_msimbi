<?php
session_start();
if (empty($_SESSION['id'])) :
    header('Location:../index.php');
endif;

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Exchange Items | <?php include('../dist/includes/title.php'); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="../plugins/select2/select2.min.css">

    <style>
        select {
            display: block;
            width: 100%;
            height: 35px;
        }
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->

<body class="hold-transition skin-<?php echo $_SESSION['skin']; ?> layout-top-nav">
    <div class="wrapper">
        <?php
        include('../dist/includes/header_admin.php');
        include('../dist/includes/dbcon.php');
        ?>
        <!-- Full Width Column -->
        <div class="content-wrapper">
            <div class="container">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <a class="btn btn-lg btn-primary" href="home.php">Back</a>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"> Exchange Products </li>
                    </ol>
                </section>

                <?php
                $order_no = "";
                if (isset($_GET['order_no'])) {
                    $order_no = $_GET['order_no'];
                }
                ?>

                <!-- Main content -->
                <section class="content col-md-12">
                    <div style="display: right;">
                        <div class="col-md-5">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title" style=" color: black"><b> Exchange Products </b></h3>
                                </div>
                                <div class="box-body">
                                    <?php include('product_composite_server.php') ?>
                                    <form autocomplete="off" method="post" name="autoSumForm" action="#">

                                        <div class="form-group">
                                            <label for="date" style=" color: black"><b> Receipt Number </b> </label>
                                            <div class="input-group col-md-12">

                                                <table class="table table-bordered" id="">
                                                    <tr>
                                                        <?php require_once('exchange_items_server.php'); ?>
                                                        <form autocomplete="off" action="#" method="POST">
                                                            <th><input type="number" class="form-control pull-right" id="date" name="order_no" Value="<?php echo $order_no; ?>"> </th>
                                                            <th><input type="submit" value="Go" name="get_order" class="form-control btn btn-circle btn-success btn-sm" /></th>
                                                        </form>
                                                    </tr>

                                                </table>

                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->

                                        <?php if (isset($_GET['order_no'])) { ?>

                                            <?php
                                            // return var_dump($order_no);
                                            function products_options($con)
                                            {
                                                $output = '';
                                                $query = mysqli_query($con, "SELECT prod_id, prod_name, prod_sell_price FROM product ORDER BY prod_name ASC ") or die(mysqli_error($con));
                                                while ($row = mysqli_fetch_array($query)) {
                                                    $output .= '<option value="' . $row["prod_id"] . '">' . $row["prod_name"] . ' - K ' . $row['prod_sell_price'] . '</option>';
                                                }
                                                return $output;
                                            }
                                            ?>

                                            <div class="form-group">
                                                <label for="date" style=" color: black"><b> Products </b> </label>
                                                <div class="input-group col-md-12">
                                                    <table class="table table-bordered" id="item_table">
                                                        <tr>
                                                            <th>Remove products </th>
                                                            <th>
                                                                <!-- <button type="button" name="add" class="btn btn-success btn-sm add"><span class="glyphicon glyphicon-plus"></span></button> -->
                                                            </th>
                                                        </tr>
                                                        <?php
                                                        $query2 = mysqli_query($con, "SELECT product.prod_id, product.prod_name AS prod_name,
                                                                                    product.prod_sell_price AS prod_price,
                                                                                    sales_details.order_no,
                                                                                    sales_details.cost_price AS cost_price,
                                                                                    sales_details.qty AS qty,
                                                                                    sales_details.price AS price,
                                                                                    sales_details.sales_id AS sales_id,
                                                                                    (SELECT SUM(price*qty) FROM sales_details WHERE order_no = '$order_no') AS total
                                                                                    FROM product INNER JOIN sales_details
                                                                                    ON product.prod_id = sales_details.prod_id 
                                                                                    WHERE order_no = '$order_no' GROUP BY prod_name ") or die(mysqli_error($con));
                                                        while ($row2 = mysqli_fetch_array($query2)) {
                                                            $prod_name = $row2['prod_name'];
                                                            $prod_id = $row2['prod_id'];
                                                            $total = $row2['total'];
                                                            $qty = $row2['qty'];
                                                            $price = $row2['price'];
                                                            $sales_id = $row2['sales_id'];
                                                            $cost_price = $row2['cost_price'];
                                                            $prod_price = $row2['prod_price'];
                                                        ?><tr>
                                                                <td><select name="prod_id[]" class="form-control ">
                                                                        <option value="<?php echo $prod_id; ?>"> <?php echo $prod_name . " x " . $qty . " x k" . $prod_price; ?> </option>
                                                                    </select>
                                                                </td>
                                                                <input type="hidden" name="qty[]" value="<?php echo $qty ?>" />

                                                                <td>
                                                                    <button type="button" name="remove" class="btn btn-danger btn-sm remove"><span class="glyphicon glyphicon-minus"></span></button>
                                                                </td>
                                                            <tr>
                                                            <?php } ?>

                                                    </table>
                                                </div><!-- /.input group -->
                                            </div><!-- /.form group -->

                                            <!-- <div class="col-md-6"> -->
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <b>Total: <input type="text" value="<?php if (isset($total)) {
                                                                                            echo "K " . number_format($total, 2);
                                                                                        } ?>" class="form-control btn btn-primary btn-sm" /> </b>
                                                    <!-- <input type="text" style="text-align:right" class="form-control" id="total" name="total" placeholder="Total" value="<?php echo $total; ?>" onFocus="startCalc();" onBlur="stopCalc();" tabindex="5" readonly> -->

                                                </div>
                                            </div>
                                            <!-- </div> -->

                                            <div class="form-group">
                                                <div class="input-group col-md-12">
                                                    <table class="table table-bordered" id="item_table2">
                                                        <tr>
                                                            <th>Replace with: </th>
                                                            <th>
                                                                <button type="button" name="add" class="btn btn-success btn-sm add"><span class="glyphicon glyphicon-plus"></span></button>
                                                            </th>
                                                        </tr>

                                                    </table>
                                                </div><!-- /.input group -->
                                            </div><!-- /.form group -->

                                            <div class="form-group">
                                                <label style="color: black"><b> Reason for exchange </b> </label>
                                                <div class="input-group col-md-12">
                                                    <textarea name="reason" required class="form-control pull-right" cols="30" rows="3"></textarea>
                                                </div><!-- /.input group -->
                                            </div><!-- /.form group -->

                                            <div class="form-group">
                                                <label style="color: black"><b> Customer name </b> </label>
                                                <div class="input-group col-md-12">
                                                    <input type="text" required class="form-control pull-right" name="customer_name" placeholder="Customer full names">
                                                </div><!-- /.input group -->
                                            </div><!-- /.form group -->

                                            <div class="form-group">
                                                <label style="color: black"><b> Customer phone number </b> </label>
                                                <div class="input-group col-md-12">
                                                    <input type="number" required min="0" class="form-control pull-right" name="customer_contact" placeholder="Customer phone number">
                                                </div><!-- /.input group -->
                                            </div><!-- /.form group -->

                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="hidden" name="sales_id" value="<?php echo $sales_id ?>" />
                                                    <input type="hidden" name="order_no" value="<?php echo $order_no ?>" />
                                                    <input type="hidden" name="total" value="<?php echo $total; ?>" />
                                                    <input type="hidden" name="branch" value="<?php echo $_SESSION['branch']; ?>" />
                                                    <input type="hidden" name="cost_price" value="<?php echo $cost_price; ?>" />
                                                    <input type="hidden" name="user_id" value="<?php echo $_SESSION['id']; ?>" />
                                                    <div class="row btn-group">
                                                        <div class="col-md-6"><input type="submit" value="Finish" name="apply_exchange" class="btn btn-primary btn-sm" /> </div>
                                                        <div class="col-md-6"><a href="exchange_items.php?reset=true" class="btn btn-sm btn-danger"> Reset </a> </div>
                                                    </div>
                                                </div>
                                            </div><!-- /.form group -->
                                        <?php } ?>
                                    </form>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                            <div><a class="btn btn-primary text-light" href="transaction_history.php">Pick From Transaction History</a></div>
                        </div><!-- /.col (right) -->


                        <?php
                        // Logic for the reset btn
                        if (isset($_REQUEST['reset'])) {
                            echo "<script>document.location='exchange_items.php'</script>";
                        }

                        ?>


                    </div>


                </section><!-- /.content -->
            </div><!-- /.container -->
        </div><!-- /.content-wrapper -->
        <?php include('../dist/includes/footer.php'); ?>
    </div><!-- ./wrapper -->


    <script type="text/javascript" src="autosum.js"></script>
    <!-- jQuery 2.1.4 -->
    <script src="../dist/js/jquery.min.js"></script>

    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>



    <script>
        $(function() {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
</body>

</html>

<script>
    $(document).ready(function() {


        $(document).on('click', '.remove', function() {
            $(this).closest('tr').remove();
        });

        $(document).on('click', '.add', function() {
            var html = '';
            html += '<tr>';
            html += '<td><select name="prod_id[]" class="form-control  select2"><option> </option><?php echo products_options($con); ?></select></td>';
            html += ' <input type="hidden" name="qty[]" value="1" /> ';
            html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><span class="glyphicon glyphicon-minus"></span></button></td></tr>';
            $('#item_table2').append(html);
        });

    });

    $(".select2").select2();
</script>
<script src="../plugins/select2/select2.full.min.js"></script>