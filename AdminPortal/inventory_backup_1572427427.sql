

CREATE TABLE `advance_payments_tb` (
  `advace_id` int(12) NOT NULL AUTO_INCREMENT,
  `amount` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(12) NOT NULL,
  `prod_id` int(12) NOT NULL,
  `qty` int(12) NOT NULL,
  `branch_id` int(12) NOT NULL,
  `order_no` int(12) NOT NULL,
  `customer_name` text NOT NULL,
  `price` int(12) NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`advace_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO advance_payments_tb VALUES("1","10","2019-10-09 12:40:03","8","371","1","1","2747","USER","60","Sold");
INSERT INTO advance_payments_tb VALUES("2","10","2019-10-09 12:40:03","8","172","1","1","2747","USER","11","Sold");
INSERT INTO advance_payments_tb VALUES("3","60","2019-10-09 12:46:19","8","371","1","1","4679","Choolwe","60","Sold");



CREATE TABLE `branch` (
  `branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(50) NOT NULL,
  `branch_address` varchar(100) NOT NULL,
  `branch_contact` varchar(50) NOT NULL,
  `reciept_footer_text` text NOT NULL,
  `notification_count` int(12) NOT NULL,
  `skin` varchar(15) NOT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO branch VALUES("1","Majuju Cafe","Kabulonga , LSK, Zambia.","0977754735","Thank you for shopping with us. ","5","red");



CREATE TABLE `cashout_limits_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `cashoutlimit` text NOT NULL,
  `status` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO cashout_limits_tb VALUES("1","20000","Not Active","2019-10-05 13:03:19");



CREATE TABLE `category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(30) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

INSERT INTO category VALUES("63","BEEF");
INSERT INTO category VALUES("65","CHICKENS");
INSERT INTO category VALUES("66","FISH");
INSERT INTO category VALUES("67","GOAT");
INSERT INTO category VALUES("68","GROCERY");
INSERT INTO category VALUES("69","PORK");
INSERT INTO category VALUES("70","SAUSAGES");
INSERT INTO category VALUES("71","SHEEP");



CREATE TABLE `customer` (
  `cust_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_first` varchar(50) NOT NULL,
  `cust_last` varchar(30) NOT NULL,
  `cust_address` varchar(100) NOT NULL,
  `cust_contact` varchar(30) NOT NULL,
  `balance` decimal(10,2) NOT NULL,
  `cust_pic` varchar(300) NOT NULL,
  `bday` date NOT NULL,
  `nickname` varchar(30) NOT NULL,
  `house_status` varchar(30) NOT NULL,
  `years` varchar(20) NOT NULL,
  `rent` varchar(10) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `emp_no` varchar(30) NOT NULL,
  `emp_address` varchar(100) NOT NULL,
  `emp_year` varchar(10) NOT NULL,
  `occupation` varchar(30) NOT NULL,
  `salary` varchar(30) NOT NULL,
  `spouse` varchar(30) NOT NULL,
  `spouse_no` varchar(30) NOT NULL,
  `spouse_emp` varchar(50) NOT NULL,
  `spouse_details` varchar(100) NOT NULL,
  `spouse_income` decimal(10,2) NOT NULL,
  `comaker` varchar(30) NOT NULL,
  `comaker_details` varchar(100) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `credit_status` varchar(10) NOT NULL,
  `ci_remarks` varchar(1000) NOT NULL,
  `ci_name` varchar(50) NOT NULL,
  `ci_date` date NOT NULL,
  `payslip` int(11) NOT NULL,
  `valid_id` int(11) NOT NULL,
  `cert` int(11) NOT NULL,
  `cedula` int(11) NOT NULL,
  `income` int(11) NOT NULL,
  `email` text NOT NULL,
  PRIMARY KEY (`cust_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `damages_log_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(12) NOT NULL,
  `qty_damage` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `demo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(100) NOT NULL,
  `state` varchar(40) NOT NULL,
  `city` varchar(40) NOT NULL,
  `pin` varchar(40) NOT NULL,
  `submission_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO demo VALUES("1","Zambia","21111","Chilenje","212121","2019-10-02");
INSERT INTO demo VALUES("2","Congo","21111","Congo","212121","2019-10-02");
INSERT INTO demo VALUES("3","South Africa","21111","Congo","212121","2019-10-02");



CREATE TABLE `discount_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `discount_price` int(12) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `discount_from` text NOT NULL,
  `discount_to` text NOT NULL,
  `status` text NOT NULL,
  `price_before_disc` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;




CREATE TABLE `draft_sales_tb` (
  `sales_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `order_no` int(12) NOT NULL,
  `client_name` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sales_details_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `draft_temp_trans` (
  `temp_trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `order_no` int(12) NOT NULL,
  `customer_name` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(12) NOT NULL,
  `is_printed` int(11) NOT NULL,
  PRIMARY KEY (`temp_trans_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO draft_temp_trans VALUES("11","471","36.00","1","1","2622","cholwe","2019-10-09 13:06:25","8","1");



CREATE TABLE `expenses_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `amount` int(12) NOT NULL,
  `date` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO expenses_tb VALUES("1"," August Container Delivery","64761","2019-08-14 10:00:07");



CREATE TABLE `history_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

INSERT INTO history_log VALUES("1","4","has logged out the system at ","2019-10-05 19:25:34");
INSERT INTO history_log VALUES("2","8","has logged in the system at ","2019-10-05 19:25:47");
INSERT INTO history_log VALUES("3","8","has logged out the system at ","2019-10-05 19:25:57");
INSERT INTO history_log VALUES("4","8","has logged in the system at ","2019-10-05 19:54:42");
INSERT INTO history_log VALUES("5","8","has logged out the system at ","2019-10-05 23:37:02");
INSERT INTO history_log VALUES("6","8","has logged in the system at ","2019-10-05 23:37:07");
INSERT INTO history_log VALUES("7","8","has logged out the system at ","2019-10-05 23:44:36");
INSERT INTO history_log VALUES("8","8","has logged in the system at ","2019-10-05 23:44:42");
INSERT INTO history_log VALUES("9","8","has logged out the system at ","2019-10-05 23:46:28");
INSERT INTO history_log VALUES("10","4","has logged in the system at ","2019-10-05 23:46:33");
INSERT INTO history_log VALUES("11","4","has logged out the system at ","2019-10-05 23:48:41");
INSERT INTO history_log VALUES("12","8","has logged in the system at ","2019-10-05 23:48:46");
INSERT INTO history_log VALUES("13","8","has logged out the system at ","2019-10-05 23:49:37");
INSERT INTO history_log VALUES("14","8","has logged in the system at ","2019-10-05 23:49:46");
INSERT INTO history_log VALUES("15","8","has logged out the system at ","2019-10-05 23:51:12");
INSERT INTO history_log VALUES("16","4","has logged in the system at ","2019-10-05 23:51:23");
INSERT INTO history_log VALUES("17","4","has logged out the system at ","2019-10-05 23:51:32");
INSERT INTO history_log VALUES("18","8","has logged in the system at ","2019-10-05 23:51:36");
INSERT INTO history_log VALUES("19","8","has logged in the system at ","2019-10-06 13:04:47");
INSERT INTO history_log VALUES("20","8","has logged in the system at ","2019-10-06 14:47:17");
INSERT INTO history_log VALUES("21","8","has logged in the system at ","2019-10-07 10:46:48");
INSERT INTO history_log VALUES("22","8","has logged out the system at ","2019-10-07 10:48:26");
INSERT INTO history_log VALUES("23","8","has logged in the system at ","2019-10-08 08:12:56");
INSERT INTO history_log VALUES("24","8","has logged out the system at ","2019-10-08 08:44:17");
INSERT INTO history_log VALUES("25","8","has logged in the system at ","2019-10-08 23:45:50");
INSERT INTO history_log VALUES("26","8","has logged in the system at ","2019-10-08 23:47:16");
INSERT INTO history_log VALUES("27","8","has logged in the system at ","2019-10-09 00:12:34");
INSERT INTO history_log VALUES("28","4","has logged in the system at ","2019-10-09 10:07:12");
INSERT INTO history_log VALUES("29","8","has logged in the system at ","2019-10-09 12:06:05");
INSERT INTO history_log VALUES("30","4","has logged in the system at ","2019-10-10 11:11:38");
INSERT INTO history_log VALUES("31","4","has logged in the system at ","2019-10-10 13:25:39");
INSERT INTO history_log VALUES("32","8","has logged in the system at ","2019-10-10 14:19:27");
INSERT INTO history_log VALUES("33","8","has logged out the system at ","2019-10-10 14:21:42");
INSERT INTO history_log VALUES("34","4","has logged in the system at ","2019-10-10 14:21:48");
INSERT INTO history_log VALUES("35","4","has logged out the system at ","2019-10-10 14:21:58");
INSERT INTO history_log VALUES("36","4","has logged in the system at ","2019-10-11 18:39:06");
INSERT INTO history_log VALUES("37","4","has logged in the system at ","2019-10-12 08:33:07");
INSERT INTO history_log VALUES("38","4","has logged in the system at ","2019-10-12 19:51:12");
INSERT INTO history_log VALUES("39","4","has logged out the system at ","2019-10-12 20:53:22");
INSERT INTO history_log VALUES("40","4","has logged in the system at ","2019-10-12 20:58:50");
INSERT INTO history_log VALUES("41","8","has logged in the system at ","2019-10-13 16:03:33");
INSERT INTO history_log VALUES("42","8","has logged out the system at ","2019-10-13 18:29:16");
INSERT INTO history_log VALUES("43","4","has logged in the system at ","2019-10-14 17:49:42");
INSERT INTO history_log VALUES("44","4","has logged in the system at ","2019-10-17 08:27:17");
INSERT INTO history_log VALUES("45","4","has logged in the system at ","2019-10-17 10:53:49");
INSERT INTO history_log VALUES("46","8","has logged in the system at ","2019-10-18 08:10:01");
INSERT INTO history_log VALUES("47","8","has logged out the system at ","2019-10-18 08:10:18");
INSERT INTO history_log VALUES("48","4","has logged in the system at ","2019-10-18 08:10:23");
INSERT INTO history_log VALUES("49","4","has logged out the system at ","2019-10-18 08:12:30");
INSERT INTO history_log VALUES("50","8","has logged in the system at ","2019-10-18 08:12:36");
INSERT INTO history_log VALUES("51","8","has logged out the system at ","2019-10-18 08:15:10");
INSERT INTO history_log VALUES("52","4","has logged in the system at ","2019-10-18 18:49:01");
INSERT INTO history_log VALUES("53","4","has logged in the system at ","2019-10-24 09:23:36");
INSERT INTO history_log VALUES("54","4","has logged in the system at ","2019-10-24 09:29:25");
INSERT INTO history_log VALUES("55","4","has logged in the system at ","2019-10-25 11:02:02");
INSERT INTO history_log VALUES("56","8","has logged in the system at ","2019-10-25 19:20:03");
INSERT INTO history_log VALUES("57","8","has logged out the system at ","2019-10-25 20:48:14");
INSERT INTO history_log VALUES("58","4","has logged in the system at ","2019-10-25 20:48:21");
INSERT INTO history_log VALUES("59","4","has logged in the system at ","2019-10-27 08:13:32");
INSERT INTO history_log VALUES("60","8","has logged in the system at ","2019-10-27 20:02:02");
INSERT INTO history_log VALUES("61","8","has logged in the system at ","2019-10-28 08:05:02");
INSERT INTO history_log VALUES("62","8","has logged out the system at ","2019-10-28 08:09:22");
INSERT INTO history_log VALUES("63","4","has logged in the system at ","2019-10-28 11:12:17");
INSERT INTO history_log VALUES("64","8","has logged in the system at ","2019-10-28 13:54:12");
INSERT INTO history_log VALUES("65","8","has logged out the system at ","2019-10-28 13:54:28");
INSERT INTO history_log VALUES("66","4","has logged in the system at ","2019-10-28 13:54:33");
INSERT INTO history_log VALUES("67","4","has logged out the system at ","2019-10-28 13:55:09");
INSERT INTO history_log VALUES("68","8","has logged in the system at ","2019-10-28 13:55:13");
INSERT INTO history_log VALUES("69","8","has logged out the system at ","2019-10-28 13:57:27");
INSERT INTO history_log VALUES("70","4","has logged in the system at ","2019-10-28 13:57:46");
INSERT INTO history_log VALUES("71","4","has logged out the system at ","2019-10-28 14:00:09");
INSERT INTO history_log VALUES("72","8","has logged in the system at ","2019-10-28 14:00:19");
INSERT INTO history_log VALUES("73","8","has logged out the system at ","2019-10-28 14:15:38");
INSERT INTO history_log VALUES("74","4","has logged in the system at ","2019-10-28 14:15:43");
INSERT INTO history_log VALUES("75","4","has logged out the system at ","2019-10-28 14:33:12");
INSERT INTO history_log VALUES("76","4","has logged in the system at ","2019-10-30 11:05:47");



CREATE TABLE `inv_damages_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `inv_id` int(12) NOT NULL,
  `quantity` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `inventory_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `quantity` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `invoices_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `order_no` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1151 DEFAULT CHARSET=latin1;

INSERT INTO invoices_tb VALUES("91","2383");
INSERT INTO invoices_tb VALUES("92","2383");
INSERT INTO invoices_tb VALUES("93","2383");
INSERT INTO invoices_tb VALUES("94","2383");
INSERT INTO invoices_tb VALUES("95","2383");
INSERT INTO invoices_tb VALUES("96","2307");
INSERT INTO invoices_tb VALUES("97","1256");
INSERT INTO invoices_tb VALUES("98","4565");
INSERT INTO invoices_tb VALUES("99","1165");
INSERT INTO invoices_tb VALUES("100","207");
INSERT INTO invoices_tb VALUES("101","1070");
INSERT INTO invoices_tb VALUES("102","2062");
INSERT INTO invoices_tb VALUES("103","0");
INSERT INTO invoices_tb VALUES("104","0");
INSERT INTO invoices_tb VALUES("105","0");
INSERT INTO invoices_tb VALUES("106","0");
INSERT INTO invoices_tb VALUES("107","0");
INSERT INTO invoices_tb VALUES("108","0");
INSERT INTO invoices_tb VALUES("109","0");
INSERT INTO invoices_tb VALUES("110","0");
INSERT INTO invoices_tb VALUES("111","0");
INSERT INTO invoices_tb VALUES("112","0");
INSERT INTO invoices_tb VALUES("113","0");
INSERT INTO invoices_tb VALUES("114","752");
INSERT INTO invoices_tb VALUES("115","752");
INSERT INTO invoices_tb VALUES("116","1393");
INSERT INTO invoices_tb VALUES("117","1393");
INSERT INTO invoices_tb VALUES("118","1393");
INSERT INTO invoices_tb VALUES("119","1393");
INSERT INTO invoices_tb VALUES("120","1393");
INSERT INTO invoices_tb VALUES("121","1393");
INSERT INTO invoices_tb VALUES("122","1393");
INSERT INTO invoices_tb VALUES("123","4611");
INSERT INTO invoices_tb VALUES("124","4611");
INSERT INTO invoices_tb VALUES("125","4611");
INSERT INTO invoices_tb VALUES("126","4611");
INSERT INTO invoices_tb VALUES("127","4611");
INSERT INTO invoices_tb VALUES("128","4611");
INSERT INTO invoices_tb VALUES("129","4611");
INSERT INTO invoices_tb VALUES("130","4611");
INSERT INTO invoices_tb VALUES("131","4611");
INSERT INTO invoices_tb VALUES("132","4611");
INSERT INTO invoices_tb VALUES("133","4611");
INSERT INTO invoices_tb VALUES("134","3197");
INSERT INTO invoices_tb VALUES("135","3197");
INSERT INTO invoices_tb VALUES("136","3197");
INSERT INTO invoices_tb VALUES("137","3197");
INSERT INTO invoices_tb VALUES("138","3197");
INSERT INTO invoices_tb VALUES("139","3197");
INSERT INTO invoices_tb VALUES("140","3197");
INSERT INTO invoices_tb VALUES("141","3197");
INSERT INTO invoices_tb VALUES("142","3197");
INSERT INTO invoices_tb VALUES("143","3197");
INSERT INTO invoices_tb VALUES("144","1865");
INSERT INTO invoices_tb VALUES("145","1865");
INSERT INTO invoices_tb VALUES("146","1865");
INSERT INTO invoices_tb VALUES("147","1865");
INSERT INTO invoices_tb VALUES("148","2692");
INSERT INTO invoices_tb VALUES("149","1737");
INSERT INTO invoices_tb VALUES("150","1737");
INSERT INTO invoices_tb VALUES("151","4089");
INSERT INTO invoices_tb VALUES("152","1082");
INSERT INTO invoices_tb VALUES("153","1082");
INSERT INTO invoices_tb VALUES("154","807");
INSERT INTO invoices_tb VALUES("155","2203");
INSERT INTO invoices_tb VALUES("156","3341");
INSERT INTO invoices_tb VALUES("157","4087");
INSERT INTO invoices_tb VALUES("158","1719");
INSERT INTO invoices_tb VALUES("159","348");
INSERT INTO invoices_tb VALUES("160","3039");
INSERT INTO invoices_tb VALUES("161","3039");
INSERT INTO invoices_tb VALUES("162","3136");
INSERT INTO invoices_tb VALUES("163","4598");
INSERT INTO invoices_tb VALUES("164","4893");
INSERT INTO invoices_tb VALUES("165","1609");
INSERT INTO invoices_tb VALUES("166","212");
INSERT INTO invoices_tb VALUES("167","812");
INSERT INTO invoices_tb VALUES("168","812");
INSERT INTO invoices_tb VALUES("169","3692");
INSERT INTO invoices_tb VALUES("170","4518");
INSERT INTO invoices_tb VALUES("171","0");
INSERT INTO invoices_tb VALUES("172","0");
INSERT INTO invoices_tb VALUES("173","1641");
INSERT INTO invoices_tb VALUES("174","1805");
INSERT INTO invoices_tb VALUES("175","1805");
INSERT INTO invoices_tb VALUES("176","3393");
INSERT INTO invoices_tb VALUES("177","4879");
INSERT INTO invoices_tb VALUES("178","1827");
INSERT INTO invoices_tb VALUES("179","159");
INSERT INTO invoices_tb VALUES("180","4752");
INSERT INTO invoices_tb VALUES("181","2982");
INSERT INTO invoices_tb VALUES("182","3302");
INSERT INTO invoices_tb VALUES("183","1402");
INSERT INTO invoices_tb VALUES("184","1881");
INSERT INTO invoices_tb VALUES("185","2618");
INSERT INTO invoices_tb VALUES("186","1377");
INSERT INTO invoices_tb VALUES("187","1878");
INSERT INTO invoices_tb VALUES("188","383");
INSERT INTO invoices_tb VALUES("189","84");
INSERT INTO invoices_tb VALUES("190","749");
INSERT INTO invoices_tb VALUES("191","4322");
INSERT INTO invoices_tb VALUES("192","1170");
INSERT INTO invoices_tb VALUES("193","978");
INSERT INTO invoices_tb VALUES("194","4821");
INSERT INTO invoices_tb VALUES("195","3640");
INSERT INTO invoices_tb VALUES("196","4218");
INSERT INTO invoices_tb VALUES("197","1540");
INSERT INTO invoices_tb VALUES("198","2757");
INSERT INTO invoices_tb VALUES("199","2757");
INSERT INTO invoices_tb VALUES("200","2757");
INSERT INTO invoices_tb VALUES("201","1075");
INSERT INTO invoices_tb VALUES("202","1075");
INSERT INTO invoices_tb VALUES("203","3476");
INSERT INTO invoices_tb VALUES("204","2018");
INSERT INTO invoices_tb VALUES("205","879");
INSERT INTO invoices_tb VALUES("206","2446");
INSERT INTO invoices_tb VALUES("207","4443");
INSERT INTO invoices_tb VALUES("208","2307");
INSERT INTO invoices_tb VALUES("209","1344");
INSERT INTO invoices_tb VALUES("210","3631");
INSERT INTO invoices_tb VALUES("211","3631");
INSERT INTO invoices_tb VALUES("212","1708");
INSERT INTO invoices_tb VALUES("213","3431");
INSERT INTO invoices_tb VALUES("214","2182");
INSERT INTO invoices_tb VALUES("215","2700");
INSERT INTO invoices_tb VALUES("216","1498");
INSERT INTO invoices_tb VALUES("217","1085");
INSERT INTO invoices_tb VALUES("218","1178");
INSERT INTO invoices_tb VALUES("219","353");
INSERT INTO invoices_tb VALUES("220","4819");
INSERT INTO invoices_tb VALUES("221","4819");
INSERT INTO invoices_tb VALUES("222","4323");
INSERT INTO invoices_tb VALUES("223","511");
INSERT INTO invoices_tb VALUES("224","511");
INSERT INTO invoices_tb VALUES("225","4531");
INSERT INTO invoices_tb VALUES("226","812");
INSERT INTO invoices_tb VALUES("227","1422");
INSERT INTO invoices_tb VALUES("228","442");
INSERT INTO invoices_tb VALUES("229","4438");
INSERT INTO invoices_tb VALUES("230","4126");
INSERT INTO invoices_tb VALUES("231","3095");
INSERT INTO invoices_tb VALUES("232","2277");
INSERT INTO invoices_tb VALUES("233","2784");
INSERT INTO invoices_tb VALUES("234","1218");
INSERT INTO invoices_tb VALUES("235","1617");
INSERT INTO invoices_tb VALUES("236","1617");
INSERT INTO invoices_tb VALUES("237","3399");
INSERT INTO invoices_tb VALUES("238","4617");
INSERT INTO invoices_tb VALUES("239","4181");
INSERT INTO invoices_tb VALUES("240","4181");
INSERT INTO invoices_tb VALUES("241","4181");
INSERT INTO invoices_tb VALUES("242","637");
INSERT INTO invoices_tb VALUES("243","637");
INSERT INTO invoices_tb VALUES("244","637");
INSERT INTO invoices_tb VALUES("245","1940");
INSERT INTO invoices_tb VALUES("246","1940");
INSERT INTO invoices_tb VALUES("247","1389");
INSERT INTO invoices_tb VALUES("248","1389");
INSERT INTO invoices_tb VALUES("249","391");
INSERT INTO invoices_tb VALUES("250","391");
INSERT INTO invoices_tb VALUES("251","391");
INSERT INTO invoices_tb VALUES("252","2486");
INSERT INTO invoices_tb VALUES("253","2486");
INSERT INTO invoices_tb VALUES("254","2486");
INSERT INTO invoices_tb VALUES("255","131");
INSERT INTO invoices_tb VALUES("256","2984");
INSERT INTO invoices_tb VALUES("257","2984");
INSERT INTO invoices_tb VALUES("258","892");
INSERT INTO invoices_tb VALUES("259","892");
INSERT INTO invoices_tb VALUES("260","4069");
INSERT INTO invoices_tb VALUES("261","4069");
INSERT INTO invoices_tb VALUES("262","2752");
INSERT INTO invoices_tb VALUES("263","2440");
INSERT INTO invoices_tb VALUES("264","2446");
INSERT INTO invoices_tb VALUES("265","2688");
INSERT INTO invoices_tb VALUES("266","2485");
INSERT INTO invoices_tb VALUES("267","1222");
INSERT INTO invoices_tb VALUES("268","588");
INSERT INTO invoices_tb VALUES("269","915");
INSERT INTO invoices_tb VALUES("270","3974");
INSERT INTO invoices_tb VALUES("271","3974");
INSERT INTO invoices_tb VALUES("272","3974");
INSERT INTO invoices_tb VALUES("273","3974");
INSERT INTO invoices_tb VALUES("274","4300");
INSERT INTO invoices_tb VALUES("275","699");
INSERT INTO invoices_tb VALUES("276","4703");
INSERT INTO invoices_tb VALUES("277","4494");
INSERT INTO invoices_tb VALUES("278","2254");
INSERT INTO invoices_tb VALUES("279","2911");
INSERT INTO invoices_tb VALUES("280","3146");
INSERT INTO invoices_tb VALUES("281","3608");
INSERT INTO invoices_tb VALUES("282","0");
INSERT INTO invoices_tb VALUES("283","3836");
INSERT INTO invoices_tb VALUES("284","2680");
INSERT INTO invoices_tb VALUES("285","3657");
INSERT INTO invoices_tb VALUES("286","1072");
INSERT INTO invoices_tb VALUES("287","4464");
INSERT INTO invoices_tb VALUES("288","3391");
INSERT INTO invoices_tb VALUES("289","3951");
INSERT INTO invoices_tb VALUES("290","3398");
INSERT INTO invoices_tb VALUES("291","3398");
INSERT INTO invoices_tb VALUES("292","3398");
INSERT INTO invoices_tb VALUES("293","653");
INSERT INTO invoices_tb VALUES("294","653");
INSERT INTO invoices_tb VALUES("295","2413");
INSERT INTO invoices_tb VALUES("296","286");
INSERT INTO invoices_tb VALUES("297","4499");
INSERT INTO invoices_tb VALUES("298","1036");
INSERT INTO invoices_tb VALUES("299","3394");
INSERT INTO invoices_tb VALUES("300","3519");
INSERT INTO invoices_tb VALUES("301","3519");
INSERT INTO invoices_tb VALUES("302","3519");
INSERT INTO invoices_tb VALUES("303","2595");
INSERT INTO invoices_tb VALUES("304","2595");
INSERT INTO invoices_tb VALUES("305","3808");
INSERT INTO invoices_tb VALUES("306","4275");
INSERT INTO invoices_tb VALUES("307","4275");
INSERT INTO invoices_tb VALUES("308","0");
INSERT INTO invoices_tb VALUES("309","0");
INSERT INTO invoices_tb VALUES("310","1816");
INSERT INTO invoices_tb VALUES("311","1816");
INSERT INTO invoices_tb VALUES("312","1899");
INSERT INTO invoices_tb VALUES("313","1899");
INSERT INTO invoices_tb VALUES("314","3778");
INSERT INTO invoices_tb VALUES("315","693");
INSERT INTO invoices_tb VALUES("316","3647");
INSERT INTO invoices_tb VALUES("317","3549");
INSERT INTO invoices_tb VALUES("318","3100");
INSERT INTO invoices_tb VALUES("319","168");
INSERT INTO invoices_tb VALUES("320","168");
INSERT INTO invoices_tb VALUES("321","4731");
INSERT INTO invoices_tb VALUES("322","4731");
INSERT INTO invoices_tb VALUES("323","497");
INSERT INTO invoices_tb VALUES("324","497");
INSERT INTO invoices_tb VALUES("325","220");
INSERT INTO invoices_tb VALUES("326","220");
INSERT INTO invoices_tb VALUES("327","2995");
INSERT INTO invoices_tb VALUES("328","2995");
INSERT INTO invoices_tb VALUES("329","1760");
INSERT INTO invoices_tb VALUES("330","1760");
INSERT INTO invoices_tb VALUES("331","1486");
INSERT INTO invoices_tb VALUES("332","642");
INSERT INTO invoices_tb VALUES("333","2133");
INSERT INTO invoices_tb VALUES("334","2133");
INSERT INTO invoices_tb VALUES("335","3488");
INSERT INTO invoices_tb VALUES("336","3091");
INSERT INTO invoices_tb VALUES("337","4196");
INSERT INTO invoices_tb VALUES("338","4196");
INSERT INTO invoices_tb VALUES("339","544");
INSERT INTO invoices_tb VALUES("340","3713");
INSERT INTO invoices_tb VALUES("341","3713");
INSERT INTO invoices_tb VALUES("342","4074");
INSERT INTO invoices_tb VALUES("343","184");
INSERT INTO invoices_tb VALUES("344","420");
INSERT INTO invoices_tb VALUES("345","3102");
INSERT INTO invoices_tb VALUES("346","3102");
INSERT INTO invoices_tb VALUES("347","3770");
INSERT INTO invoices_tb VALUES("348","1047");
INSERT INTO invoices_tb VALUES("349","1149");
INSERT INTO invoices_tb VALUES("350","138");
INSERT INTO invoices_tb VALUES("351","138");
INSERT INTO invoices_tb VALUES("352","2261");
INSERT INTO invoices_tb VALUES("353","2261");
INSERT INTO invoices_tb VALUES("354","2261");
INSERT INTO invoices_tb VALUES("355","2526");
INSERT INTO invoices_tb VALUES("356","2526");
INSERT INTO invoices_tb VALUES("357","1967");
INSERT INTO invoices_tb VALUES("358","1967");
INSERT INTO invoices_tb VALUES("359","2081");
INSERT INTO invoices_tb VALUES("360","1554");
INSERT INTO invoices_tb VALUES("361","2411");
INSERT INTO invoices_tb VALUES("362","1624");
INSERT INTO invoices_tb VALUES("363","2567");
INSERT INTO invoices_tb VALUES("364","2567");
INSERT INTO invoices_tb VALUES("365","1487");
INSERT INTO invoices_tb VALUES("366","233");
INSERT INTO invoices_tb VALUES("367","4357");
INSERT INTO invoices_tb VALUES("368","4862");
INSERT INTO invoices_tb VALUES("369","4862");
INSERT INTO invoices_tb VALUES("370","306");
INSERT INTO invoices_tb VALUES("371","3632");
INSERT INTO invoices_tb VALUES("372","3470");
INSERT INTO invoices_tb VALUES("373","3470");
INSERT INTO invoices_tb VALUES("374","3470");
INSERT INTO invoices_tb VALUES("375","3470");
INSERT INTO invoices_tb VALUES("376","3470");
INSERT INTO invoices_tb VALUES("377","3938");
INSERT INTO invoices_tb VALUES("378","3938");
INSERT INTO invoices_tb VALUES("379","4685");
INSERT INTO invoices_tb VALUES("380","3715");
INSERT INTO invoices_tb VALUES("381","1023");
INSERT INTO invoices_tb VALUES("382","1005");
INSERT INTO invoices_tb VALUES("383","4867");
INSERT INTO invoices_tb VALUES("384","1613");
INSERT INTO invoices_tb VALUES("385","2917");
INSERT INTO invoices_tb VALUES("386","4087");
INSERT INTO invoices_tb VALUES("387","4952");
INSERT INTO invoices_tb VALUES("388","4534");
INSERT INTO invoices_tb VALUES("389","1937");
INSERT INTO invoices_tb VALUES("390","1937");
INSERT INTO invoices_tb VALUES("391","660");
INSERT INTO invoices_tb VALUES("392","3285");
INSERT INTO invoices_tb VALUES("393","3285");
INSERT INTO invoices_tb VALUES("394","743");
INSERT INTO invoices_tb VALUES("395","743");
INSERT INTO invoices_tb VALUES("396","1316");
INSERT INTO invoices_tb VALUES("397","658");
INSERT INTO invoices_tb VALUES("398","912");
INSERT INTO invoices_tb VALUES("399","789");
INSERT INTO invoices_tb VALUES("400","4114");
INSERT INTO invoices_tb VALUES("401","3777");
INSERT INTO invoices_tb VALUES("402","3777");
INSERT INTO invoices_tb VALUES("403","2616");
INSERT INTO invoices_tb VALUES("404","4904");
INSERT INTO invoices_tb VALUES("405","1963");
INSERT INTO invoices_tb VALUES("406","1126");
INSERT INTO invoices_tb VALUES("407","1835");
INSERT INTO invoices_tb VALUES("408","2012");
INSERT INTO invoices_tb VALUES("409","2012");
INSERT INTO invoices_tb VALUES("410","2012");
INSERT INTO invoices_tb VALUES("411","2012");
INSERT INTO invoices_tb VALUES("412","4392");
INSERT INTO invoices_tb VALUES("413","2529");
INSERT INTO invoices_tb VALUES("414","2965");
INSERT INTO invoices_tb VALUES("415","2965");
INSERT INTO invoices_tb VALUES("416","3495");
INSERT INTO invoices_tb VALUES("417","3495");
INSERT INTO invoices_tb VALUES("418","2121");
INSERT INTO invoices_tb VALUES("419","3664");
INSERT INTO invoices_tb VALUES("420","3664");
INSERT INTO invoices_tb VALUES("421","4271");
INSERT INTO invoices_tb VALUES("422","1955");
INSERT INTO invoices_tb VALUES("423","1955");
INSERT INTO invoices_tb VALUES("424","4708");
INSERT INTO invoices_tb VALUES("425","4708");
INSERT INTO invoices_tb VALUES("426","553");
INSERT INTO invoices_tb VALUES("427","553");
INSERT INTO invoices_tb VALUES("428","629");
INSERT INTO invoices_tb VALUES("429","4166");
INSERT INTO invoices_tb VALUES("430","4326");
INSERT INTO invoices_tb VALUES("431","976");
INSERT INTO invoices_tb VALUES("432","2912");
INSERT INTO invoices_tb VALUES("433","4255");
INSERT INTO invoices_tb VALUES("434","4255");
INSERT INTO invoices_tb VALUES("435","662");
INSERT INTO invoices_tb VALUES("436","1090");
INSERT INTO invoices_tb VALUES("437","106");
INSERT INTO invoices_tb VALUES("438","2661");
INSERT INTO invoices_tb VALUES("439","813");
INSERT INTO invoices_tb VALUES("440","2042");
INSERT INTO invoices_tb VALUES("441","2042");
INSERT INTO invoices_tb VALUES("442","1195");
INSERT INTO invoices_tb VALUES("443","2126");
INSERT INTO invoices_tb VALUES("444","3624");
INSERT INTO invoices_tb VALUES("445","4427");
INSERT INTO invoices_tb VALUES("446","4427");
INSERT INTO invoices_tb VALUES("447","4427");
INSERT INTO invoices_tb VALUES("448","2890");
INSERT INTO invoices_tb VALUES("449","619");
INSERT INTO invoices_tb VALUES("450","3800");
INSERT INTO invoices_tb VALUES("451","3800");
INSERT INTO invoices_tb VALUES("452","4375");
INSERT INTO invoices_tb VALUES("453","2080");
INSERT INTO invoices_tb VALUES("454","2080");
INSERT INTO invoices_tb VALUES("455","2080");
INSERT INTO invoices_tb VALUES("456","1964");
INSERT INTO invoices_tb VALUES("457","3032");
INSERT INTO invoices_tb VALUES("458","1038");
INSERT INTO invoices_tb VALUES("459","1736");
INSERT INTO invoices_tb VALUES("460","1736");
INSERT INTO invoices_tb VALUES("461","2884");
INSERT INTO invoices_tb VALUES("462","980");
INSERT INTO invoices_tb VALUES("463","3142");
INSERT INTO invoices_tb VALUES("464","1737");
INSERT INTO invoices_tb VALUES("465","3570");
INSERT INTO invoices_tb VALUES("466","175");
INSERT INTO invoices_tb VALUES("467","4139");
INSERT INTO invoices_tb VALUES("468","2000");
INSERT INTO invoices_tb VALUES("469","2085");
INSERT INTO invoices_tb VALUES("470","2240");
INSERT INTO invoices_tb VALUES("471","1786");
INSERT INTO invoices_tb VALUES("472","1786");
INSERT INTO invoices_tb VALUES("473","1576");
INSERT INTO invoices_tb VALUES("474","1576");
INSERT INTO invoices_tb VALUES("475","402");
INSERT INTO invoices_tb VALUES("476","3397");
INSERT INTO invoices_tb VALUES("477","1318");
INSERT INTO invoices_tb VALUES("478","1411");
INSERT INTO invoices_tb VALUES("479","3078");
INSERT INTO invoices_tb VALUES("480","975");
INSERT INTO invoices_tb VALUES("481","1337");
INSERT INTO invoices_tb VALUES("482","1072");
INSERT INTO invoices_tb VALUES("483","1339");
INSERT INTO invoices_tb VALUES("484","1912");
INSERT INTO invoices_tb VALUES("485","2166");
INSERT INTO invoices_tb VALUES("486","370");
INSERT INTO invoices_tb VALUES("487","3931");
INSERT INTO invoices_tb VALUES("488","3340");
INSERT INTO invoices_tb VALUES("489","2272");
INSERT INTO invoices_tb VALUES("490","1747");
INSERT INTO invoices_tb VALUES("491","4076");
INSERT INTO invoices_tb VALUES("492","3523");
INSERT INTO invoices_tb VALUES("493","3617");
INSERT INTO invoices_tb VALUES("494","2526");
INSERT INTO invoices_tb VALUES("495","2067");
INSERT INTO invoices_tb VALUES("496","2440");
INSERT INTO invoices_tb VALUES("497","3468");
INSERT INTO invoices_tb VALUES("498","4434");
INSERT INTO invoices_tb VALUES("499","4434");
INSERT INTO invoices_tb VALUES("500","4434");
INSERT INTO invoices_tb VALUES("501","4434");
INSERT INTO invoices_tb VALUES("502","4434");
INSERT INTO invoices_tb VALUES("503","4649");
INSERT INTO invoices_tb VALUES("504","4711");
INSERT INTO invoices_tb VALUES("505","4272");
INSERT INTO invoices_tb VALUES("506","4347");
INSERT INTO invoices_tb VALUES("507","1139");
INSERT INTO invoices_tb VALUES("508","1252");
INSERT INTO invoices_tb VALUES("509","474");
INSERT INTO invoices_tb VALUES("510","4486");
INSERT INTO invoices_tb VALUES("511","4486");
INSERT INTO invoices_tb VALUES("512","1716");
INSERT INTO invoices_tb VALUES("513","1877");
INSERT INTO invoices_tb VALUES("514","431");
INSERT INTO invoices_tb VALUES("515","247");
INSERT INTO invoices_tb VALUES("516","1732");
INSERT INTO invoices_tb VALUES("517","1732");
INSERT INTO invoices_tb VALUES("518","472");
INSERT INTO invoices_tb VALUES("519","472");
INSERT INTO invoices_tb VALUES("520","2283");
INSERT INTO invoices_tb VALUES("521","1875");
INSERT INTO invoices_tb VALUES("522","4680");
INSERT INTO invoices_tb VALUES("523","3444");
INSERT INTO invoices_tb VALUES("524","3950");
INSERT INTO invoices_tb VALUES("525","4586");
INSERT INTO invoices_tb VALUES("526","1446");
INSERT INTO invoices_tb VALUES("527","3834");
INSERT INTO invoices_tb VALUES("528","2864");
INSERT INTO invoices_tb VALUES("529","4764");
INSERT INTO invoices_tb VALUES("530","879");
INSERT INTO invoices_tb VALUES("531","4502");
INSERT INTO invoices_tb VALUES("532","4467");
INSERT INTO invoices_tb VALUES("533","1762");
INSERT INTO invoices_tb VALUES("534","3062");
INSERT INTO invoices_tb VALUES("535","1627");
INSERT INTO invoices_tb VALUES("536","2960");
INSERT INTO invoices_tb VALUES("537","3403");
INSERT INTO invoices_tb VALUES("538","1976");
INSERT INTO invoices_tb VALUES("539","2452");
INSERT INTO invoices_tb VALUES("540","4366");
INSERT INTO invoices_tb VALUES("541","3643");
INSERT INTO invoices_tb VALUES("542","320");
INSERT INTO invoices_tb VALUES("543","320");
INSERT INTO invoices_tb VALUES("544","2605");
INSERT INTO invoices_tb VALUES("545","3651");
INSERT INTO invoices_tb VALUES("546","3390");
INSERT INTO invoices_tb VALUES("547","2427");
INSERT INTO invoices_tb VALUES("548","2427");
INSERT INTO invoices_tb VALUES("549","614");
INSERT INTO invoices_tb VALUES("550","2535");
INSERT INTO invoices_tb VALUES("551","1882");
INSERT INTO invoices_tb VALUES("552","1832");
INSERT INTO invoices_tb VALUES("553","4106");
INSERT INTO invoices_tb VALUES("554","4100");
INSERT INTO invoices_tb VALUES("555","1979");
INSERT INTO invoices_tb VALUES("556","3914");
INSERT INTO invoices_tb VALUES("557","1145");
INSERT INTO invoices_tb VALUES("558","1650");
INSERT INTO invoices_tb VALUES("559","1282");
INSERT INTO invoices_tb VALUES("560","4994");
INSERT INTO invoices_tb VALUES("561","3580");
INSERT INTO invoices_tb VALUES("562","1718");
INSERT INTO invoices_tb VALUES("563","436");
INSERT INTO invoices_tb VALUES("564","2765");
INSERT INTO invoices_tb VALUES("565","2295");
INSERT INTO invoices_tb VALUES("566","1008");
INSERT INTO invoices_tb VALUES("567","1017");
INSERT INTO invoices_tb VALUES("568","2747");
INSERT INTO invoices_tb VALUES("569","4118");
INSERT INTO invoices_tb VALUES("570","931");
INSERT INTO invoices_tb VALUES("571","1262");
INSERT INTO invoices_tb VALUES("572","1927");
INSERT INTO invoices_tb VALUES("573","4268");
INSERT INTO invoices_tb VALUES("574","2786");
INSERT INTO invoices_tb VALUES("575","466");
INSERT INTO invoices_tb VALUES("576","4851");
INSERT INTO invoices_tb VALUES("577","446");
INSERT INTO invoices_tb VALUES("578","687");
INSERT INTO invoices_tb VALUES("579","3528");
INSERT INTO invoices_tb VALUES("580","4931");
INSERT INTO invoices_tb VALUES("581","4794");
INSERT INTO invoices_tb VALUES("582","1868");
INSERT INTO invoices_tb VALUES("583","871");
INSERT INTO invoices_tb VALUES("584","3773");
INSERT INTO invoices_tb VALUES("585","2398");
INSERT INTO invoices_tb VALUES("586","1830");
INSERT INTO invoices_tb VALUES("587","1830");
INSERT INTO invoices_tb VALUES("588","1888");
INSERT INTO invoices_tb VALUES("589","4488");
INSERT INTO invoices_tb VALUES("590","4488");
INSERT INTO invoices_tb VALUES("591","4374");
INSERT INTO invoices_tb VALUES("592","1272");
INSERT INTO invoices_tb VALUES("593","269");
INSERT INTO invoices_tb VALUES("594","2151");
INSERT INTO invoices_tb VALUES("595","2864");
INSERT INTO invoices_tb VALUES("596","368");
INSERT INTO invoices_tb VALUES("597","3878");
INSERT INTO invoices_tb VALUES("598","327");
INSERT INTO invoices_tb VALUES("599","4304");
INSERT INTO invoices_tb VALUES("600","2435");
INSERT INTO invoices_tb VALUES("601","1703");
INSERT INTO invoices_tb VALUES("602","1084");
INSERT INTO invoices_tb VALUES("603","1084");
INSERT INTO invoices_tb VALUES("604","1084");
INSERT INTO invoices_tb VALUES("605","4904");
INSERT INTO invoices_tb VALUES("606","4120");
INSERT INTO invoices_tb VALUES("607","1354");
INSERT INTO invoices_tb VALUES("608","2977");
INSERT INTO invoices_tb VALUES("609","1434");
INSERT INTO invoices_tb VALUES("610","1434");
INSERT INTO invoices_tb VALUES("611","1434");
INSERT INTO invoices_tb VALUES("612","3867");
INSERT INTO invoices_tb VALUES("613","3892");
INSERT INTO invoices_tb VALUES("614","3556");
INSERT INTO invoices_tb VALUES("615","3981");
INSERT INTO invoices_tb VALUES("616","4223");
INSERT INTO invoices_tb VALUES("617","402");
INSERT INTO invoices_tb VALUES("618","524");
INSERT INTO invoices_tb VALUES("619","3997");
INSERT INTO invoices_tb VALUES("620","318");
INSERT INTO invoices_tb VALUES("621","1464");
INSERT INTO invoices_tb VALUES("622","4239");
INSERT INTO invoices_tb VALUES("623","4075");
INSERT INTO invoices_tb VALUES("624","4782");
INSERT INTO invoices_tb VALUES("625","1894");
INSERT INTO invoices_tb VALUES("626","1443");
INSERT INTO invoices_tb VALUES("627","3190");
INSERT INTO invoices_tb VALUES("628","3722");
INSERT INTO invoices_tb VALUES("629","2381");
INSERT INTO invoices_tb VALUES("630","781");
INSERT INTO invoices_tb VALUES("631","983");
INSERT INTO invoices_tb VALUES("632","2813");
INSERT INTO invoices_tb VALUES("633","76");
INSERT INTO invoices_tb VALUES("634","307");
INSERT INTO invoices_tb VALUES("635","1068");
INSERT INTO invoices_tb VALUES("636","3085");
INSERT INTO invoices_tb VALUES("637","2436");
INSERT INTO invoices_tb VALUES("638","4643");
INSERT INTO invoices_tb VALUES("639","1219");
INSERT INTO invoices_tb VALUES("640","4573");
INSERT INTO invoices_tb VALUES("641","3185");
INSERT INTO invoices_tb VALUES("642","4766");
INSERT INTO invoices_tb VALUES("643","269");
INSERT INTO invoices_tb VALUES("644","765");
INSERT INTO invoices_tb VALUES("645","804");
INSERT INTO invoices_tb VALUES("646","4669");
INSERT INTO invoices_tb VALUES("647","3413");
INSERT INTO invoices_tb VALUES("648","2285");
INSERT INTO invoices_tb VALUES("649","4576");
INSERT INTO invoices_tb VALUES("650","1380");
INSERT INTO invoices_tb VALUES("651","4852");
INSERT INTO invoices_tb VALUES("652","4163");
INSERT INTO invoices_tb VALUES("653","3841");
INSERT INTO invoices_tb VALUES("654","2017");
INSERT INTO invoices_tb VALUES("655","3271");
INSERT INTO invoices_tb VALUES("656","2072");
INSERT INTO invoices_tb VALUES("657","4496");
INSERT INTO invoices_tb VALUES("658","4837");
INSERT INTO invoices_tb VALUES("659","430");
INSERT INTO invoices_tb VALUES("660","4309");
INSERT INTO invoices_tb VALUES("661","3180");
INSERT INTO invoices_tb VALUES("662","4611");
INSERT INTO invoices_tb VALUES("663","3847");
INSERT INTO invoices_tb VALUES("664","2499");
INSERT INTO invoices_tb VALUES("665","430");
INSERT INTO invoices_tb VALUES("666","655");
INSERT INTO invoices_tb VALUES("667","3478");
INSERT INTO invoices_tb VALUES("668","4958");
INSERT INTO invoices_tb VALUES("669","814");
INSERT INTO invoices_tb VALUES("670","2489");
INSERT INTO invoices_tb VALUES("671","4282");
INSERT INTO invoices_tb VALUES("672","2545");
INSERT INTO invoices_tb VALUES("673","1252");
INSERT INTO invoices_tb VALUES("674","1748");
INSERT INTO invoices_tb VALUES("675","3724");
INSERT INTO invoices_tb VALUES("676","383");
INSERT INTO invoices_tb VALUES("677","3084");
INSERT INTO invoices_tb VALUES("678","647");
INSERT INTO invoices_tb VALUES("679","1054");
INSERT INTO invoices_tb VALUES("680","2812");
INSERT INTO invoices_tb VALUES("681","1001");
INSERT INTO invoices_tb VALUES("682","4142");
INSERT INTO invoices_tb VALUES("683","1514");
INSERT INTO invoices_tb VALUES("684","1514");
INSERT INTO invoices_tb VALUES("685","4729");
INSERT INTO invoices_tb VALUES("686","4286");
INSERT INTO invoices_tb VALUES("687","4631");
INSERT INTO invoices_tb VALUES("688","1717");
INSERT INTO invoices_tb VALUES("689","1717");
INSERT INTO invoices_tb VALUES("690","2845");
INSERT INTO invoices_tb VALUES("691","2808");
INSERT INTO invoices_tb VALUES("692","4458");
INSERT INTO invoices_tb VALUES("693","4722");
INSERT INTO invoices_tb VALUES("694","2882");
INSERT INTO invoices_tb VALUES("695","4817");
INSERT INTO invoices_tb VALUES("696","4270");
INSERT INTO invoices_tb VALUES("697","2628");
INSERT INTO invoices_tb VALUES("698","966");
INSERT INTO invoices_tb VALUES("699","2651");
INSERT INTO invoices_tb VALUES("700","1680");
INSERT INTO invoices_tb VALUES("701","1506");
INSERT INTO invoices_tb VALUES("702","1506");
INSERT INTO invoices_tb VALUES("703","797");
INSERT INTO invoices_tb VALUES("704","547");
INSERT INTO invoices_tb VALUES("705","3676");
INSERT INTO invoices_tb VALUES("706","3327");
INSERT INTO invoices_tb VALUES("707","1913");
INSERT INTO invoices_tb VALUES("708","2942");
INSERT INTO invoices_tb VALUES("709","2692");
INSERT INTO invoices_tb VALUES("710","687");
INSERT INTO invoices_tb VALUES("711","3007");
INSERT INTO invoices_tb VALUES("712","2271");
INSERT INTO invoices_tb VALUES("713","301");
INSERT INTO invoices_tb VALUES("714","1088");
INSERT INTO invoices_tb VALUES("715","1746");
INSERT INTO invoices_tb VALUES("716","1994");
INSERT INTO invoices_tb VALUES("717","4245");
INSERT INTO invoices_tb VALUES("718","3617");
INSERT INTO invoices_tb VALUES("719","4953");
INSERT INTO invoices_tb VALUES("720","3617");
INSERT INTO invoices_tb VALUES("721","1624");
INSERT INTO invoices_tb VALUES("722","4908");
INSERT INTO invoices_tb VALUES("723","1688");
INSERT INTO invoices_tb VALUES("724","1410");
INSERT INTO invoices_tb VALUES("725","1900");
INSERT INTO invoices_tb VALUES("726","823");
INSERT INTO invoices_tb VALUES("727","2530");
INSERT INTO invoices_tb VALUES("728","2530");
INSERT INTO invoices_tb VALUES("729","560");
INSERT INTO invoices_tb VALUES("730","3618");
INSERT INTO invoices_tb VALUES("731","1619");
INSERT INTO invoices_tb VALUES("732","1618");
INSERT INTO invoices_tb VALUES("733","874");
INSERT INTO invoices_tb VALUES("734","331");
INSERT INTO invoices_tb VALUES("735","3674");
INSERT INTO invoices_tb VALUES("736","3194");
INSERT INTO invoices_tb VALUES("737","4003");
INSERT INTO invoices_tb VALUES("738","718");
INSERT INTO invoices_tb VALUES("739","1829");
INSERT INTO invoices_tb VALUES("740","2197");
INSERT INTO invoices_tb VALUES("741","2009");
INSERT INTO invoices_tb VALUES("742","4866");
INSERT INTO invoices_tb VALUES("743","4549");
INSERT INTO invoices_tb VALUES("744","4420");
INSERT INTO invoices_tb VALUES("745","4737");
INSERT INTO invoices_tb VALUES("746","1874");
INSERT INTO invoices_tb VALUES("747","1874");
INSERT INTO invoices_tb VALUES("748","3606");
INSERT INTO invoices_tb VALUES("749","3606");
INSERT INTO invoices_tb VALUES("750","3685");
INSERT INTO invoices_tb VALUES("751","2433");
INSERT INTO invoices_tb VALUES("752","529");
INSERT INTO invoices_tb VALUES("753","2526");
INSERT INTO invoices_tb VALUES("754","801");
INSERT INTO invoices_tb VALUES("755","4435");
INSERT INTO invoices_tb VALUES("756","1449");
INSERT INTO invoices_tb VALUES("757","149");
INSERT INTO invoices_tb VALUES("758","3554");
INSERT INTO invoices_tb VALUES("759","2261");
INSERT INTO invoices_tb VALUES("760","781");
INSERT INTO invoices_tb VALUES("761","1613");
INSERT INTO invoices_tb VALUES("762","4333");
INSERT INTO invoices_tb VALUES("763","4333");
INSERT INTO invoices_tb VALUES("764","2066");
INSERT INTO invoices_tb VALUES("765","1689");
INSERT INTO invoices_tb VALUES("766","2517");
INSERT INTO invoices_tb VALUES("767","263");
INSERT INTO invoices_tb VALUES("768","3059");
INSERT INTO invoices_tb VALUES("769","3350");
INSERT INTO invoices_tb VALUES("770","4188");
INSERT INTO invoices_tb VALUES("771","4919");
INSERT INTO invoices_tb VALUES("772","1544");
INSERT INTO invoices_tb VALUES("773","3500");
INSERT INTO invoices_tb VALUES("774","4667");
INSERT INTO invoices_tb VALUES("775","3347");
INSERT INTO invoices_tb VALUES("776","4874");
INSERT INTO invoices_tb VALUES("777","3225");
INSERT INTO invoices_tb VALUES("778","4924");
INSERT INTO invoices_tb VALUES("779","1608");
INSERT INTO invoices_tb VALUES("780","4886");
INSERT INTO invoices_tb VALUES("781","3699");
INSERT INTO invoices_tb VALUES("782","2892");
INSERT INTO invoices_tb VALUES("783","3481");
INSERT INTO invoices_tb VALUES("784","1203");
INSERT INTO invoices_tb VALUES("785","3497");
INSERT INTO invoices_tb VALUES("786","2142");
INSERT INTO invoices_tb VALUES("787","3560");
INSERT INTO invoices_tb VALUES("788","2236");
INSERT INTO invoices_tb VALUES("789","590");
INSERT INTO invoices_tb VALUES("790","3829");
INSERT INTO invoices_tb VALUES("791","3219");
INSERT INTO invoices_tb VALUES("792","2937");
INSERT INTO invoices_tb VALUES("793","4312");
INSERT INTO invoices_tb VALUES("794","3630");
INSERT INTO invoices_tb VALUES("795","3367");
INSERT INTO invoices_tb VALUES("796","2538");
INSERT INTO invoices_tb VALUES("797","753");
INSERT INTO invoices_tb VALUES("798","1251");
INSERT INTO invoices_tb VALUES("799","3302");
INSERT INTO invoices_tb VALUES("800","686");
INSERT INTO invoices_tb VALUES("801","1957");
INSERT INTO invoices_tb VALUES("802","3645");
INSERT INTO invoices_tb VALUES("803","1982");
INSERT INTO invoices_tb VALUES("804","2988");
INSERT INTO invoices_tb VALUES("805","2093");
INSERT INTO invoices_tb VALUES("806","3974");
INSERT INTO invoices_tb VALUES("807","1405");
INSERT INTO invoices_tb VALUES("808","4697");
INSERT INTO invoices_tb VALUES("809","666");
INSERT INTO invoices_tb VALUES("810","232");
INSERT INTO invoices_tb VALUES("811","4984");
INSERT INTO invoices_tb VALUES("812","3073");
INSERT INTO invoices_tb VALUES("813","1049");
INSERT INTO invoices_tb VALUES("814","3941");
INSERT INTO invoices_tb VALUES("815","1575");
INSERT INTO invoices_tb VALUES("816","1575");
INSERT INTO invoices_tb VALUES("817","522");
INSERT INTO invoices_tb VALUES("818","4285");
INSERT INTO invoices_tb VALUES("819","880");
INSERT INTO invoices_tb VALUES("820","1699");
INSERT INTO invoices_tb VALUES("821","4191");
INSERT INTO invoices_tb VALUES("822","2708");
INSERT INTO invoices_tb VALUES("823","1644");
INSERT INTO invoices_tb VALUES("824","1053");
INSERT INTO invoices_tb VALUES("825","2905");
INSERT INTO invoices_tb VALUES("826","2190");
INSERT INTO invoices_tb VALUES("827","3094");
INSERT INTO invoices_tb VALUES("828","3664");
INSERT INTO invoices_tb VALUES("829","1601");
INSERT INTO invoices_tb VALUES("830","1648");
INSERT INTO invoices_tb VALUES("831","2611");
INSERT INTO invoices_tb VALUES("832","2712");
INSERT INTO invoices_tb VALUES("833","2851");
INSERT INTO invoices_tb VALUES("834","3252");
INSERT INTO invoices_tb VALUES("835","3978");
INSERT INTO invoices_tb VALUES("836","2974");
INSERT INTO invoices_tb VALUES("837","400");
INSERT INTO invoices_tb VALUES("838","400");
INSERT INTO invoices_tb VALUES("839","3274");
INSERT INTO invoices_tb VALUES("840","212");
INSERT INTO invoices_tb VALUES("841","3602");
INSERT INTO invoices_tb VALUES("842","2341");
INSERT INTO invoices_tb VALUES("843","3549");
INSERT INTO invoices_tb VALUES("844","143");
INSERT INTO invoices_tb VALUES("845","3426");
INSERT INTO invoices_tb VALUES("846","192");
INSERT INTO invoices_tb VALUES("847","1683");
INSERT INTO invoices_tb VALUES("848","567");
INSERT INTO invoices_tb VALUES("849","2042");
INSERT INTO invoices_tb VALUES("850","2329");
INSERT INTO invoices_tb VALUES("851","4110");
INSERT INTO invoices_tb VALUES("852","4110");
INSERT INTO invoices_tb VALUES("853","677");
INSERT INTO invoices_tb VALUES("854","4896");
INSERT INTO invoices_tb VALUES("855","4896");
INSERT INTO invoices_tb VALUES("856","4418");
INSERT INTO invoices_tb VALUES("857","4418");
INSERT INTO invoices_tb VALUES("858","4418");
INSERT INTO invoices_tb VALUES("859","627");
INSERT INTO invoices_tb VALUES("860","4302");
INSERT INTO invoices_tb VALUES("861","3025");
INSERT INTO invoices_tb VALUES("862","855");
INSERT INTO invoices_tb VALUES("863","1363");
INSERT INTO invoices_tb VALUES("864","1946");
INSERT INTO invoices_tb VALUES("865","2580");
INSERT INTO invoices_tb VALUES("866","2045");
INSERT INTO invoices_tb VALUES("867","3711");
INSERT INTO invoices_tb VALUES("868","1962");
INSERT INTO invoices_tb VALUES("869","4967");
INSERT INTO invoices_tb VALUES("870","2289");
INSERT INTO invoices_tb VALUES("871","1845");
INSERT INTO invoices_tb VALUES("872","4508");
INSERT INTO invoices_tb VALUES("873","2366");
INSERT INTO invoices_tb VALUES("874","3176");
INSERT INTO invoices_tb VALUES("875","3237");
INSERT INTO invoices_tb VALUES("876","4834");
INSERT INTO invoices_tb VALUES("877","1387");
INSERT INTO invoices_tb VALUES("878","4608");
INSERT INTO invoices_tb VALUES("879","4992");
INSERT INTO invoices_tb VALUES("880","4992");
INSERT INTO invoices_tb VALUES("881","2703");
INSERT INTO invoices_tb VALUES("882","2303");
INSERT INTO invoices_tb VALUES("883","3983");
INSERT INTO invoices_tb VALUES("884","393");
INSERT INTO invoices_tb VALUES("885","395");
INSERT INTO invoices_tb VALUES("886","1655");
INSERT INTO invoices_tb VALUES("887","827");
INSERT INTO invoices_tb VALUES("888","4895");
INSERT INTO invoices_tb VALUES("889","3010");
INSERT INTO invoices_tb VALUES("890","2167");
INSERT INTO invoices_tb VALUES("891","1088");
INSERT INTO invoices_tb VALUES("892","1942");
INSERT INTO invoices_tb VALUES("893","3477");
INSERT INTO invoices_tb VALUES("894","3904");
INSERT INTO invoices_tb VALUES("895","4020");
INSERT INTO invoices_tb VALUES("896","4732");
INSERT INTO invoices_tb VALUES("897","4800");
INSERT INTO invoices_tb VALUES("898","2417");
INSERT INTO invoices_tb VALUES("899","1801");
INSERT INTO invoices_tb VALUES("900","4191");
INSERT INTO invoices_tb VALUES("901","4178");
INSERT INTO invoices_tb VALUES("902","1866");
INSERT INTO invoices_tb VALUES("903","1912");
INSERT INTO invoices_tb VALUES("904","2761");
INSERT INTO invoices_tb VALUES("905","2840");
INSERT INTO invoices_tb VALUES("906","2977");
INSERT INTO invoices_tb VALUES("907","2977");
INSERT INTO invoices_tb VALUES("908","1994");
INSERT INTO invoices_tb VALUES("909","2542");
INSERT INTO invoices_tb VALUES("910","1117");
INSERT INTO invoices_tb VALUES("911","4356");
INSERT INTO invoices_tb VALUES("912","358");
INSERT INTO invoices_tb VALUES("913","4229");
INSERT INTO invoices_tb VALUES("914","4199");
INSERT INTO invoices_tb VALUES("915","1419");
INSERT INTO invoices_tb VALUES("916","3768");
INSERT INTO invoices_tb VALUES("917","3255");
INSERT INTO invoices_tb VALUES("918","4137");
INSERT INTO invoices_tb VALUES("919","1962");
INSERT INTO invoices_tb VALUES("920","2434");
INSERT INTO invoices_tb VALUES("921","1488");
INSERT INTO invoices_tb VALUES("922","161");
INSERT INTO invoices_tb VALUES("923","4911");
INSERT INTO invoices_tb VALUES("924","2513");
INSERT INTO invoices_tb VALUES("925","4661");
INSERT INTO invoices_tb VALUES("926","86");
INSERT INTO invoices_tb VALUES("927","4224");
INSERT INTO invoices_tb VALUES("928","3387");
INSERT INTO invoices_tb VALUES("929","1719");
INSERT INTO invoices_tb VALUES("930","2735");
INSERT INTO invoices_tb VALUES("931","4440");
INSERT INTO invoices_tb VALUES("932","3335");
INSERT INTO invoices_tb VALUES("933","4202");
INSERT INTO invoices_tb VALUES("934","1466");
INSERT INTO invoices_tb VALUES("935","4388");
INSERT INTO invoices_tb VALUES("936","196");
INSERT INTO invoices_tb VALUES("937","78");
INSERT INTO invoices_tb VALUES("938","1167");
INSERT INTO invoices_tb VALUES("939","4264");
INSERT INTO invoices_tb VALUES("940","1657");
INSERT INTO invoices_tb VALUES("941","3449");
INSERT INTO invoices_tb VALUES("942","4114");
INSERT INTO invoices_tb VALUES("943","2367");
INSERT INTO invoices_tb VALUES("944","2513");
INSERT INTO invoices_tb VALUES("945","2941");
INSERT INTO invoices_tb VALUES("946","776");
INSERT INTO invoices_tb VALUES("947","202");
INSERT INTO invoices_tb VALUES("948","4584");
INSERT INTO invoices_tb VALUES("949","4317");
INSERT INTO invoices_tb VALUES("950","2268");
INSERT INTO invoices_tb VALUES("951","3034");
INSERT INTO invoices_tb VALUES("952","3930");
INSERT INTO invoices_tb VALUES("953","2192");
INSERT INTO invoices_tb VALUES("954","3052");
INSERT INTO invoices_tb VALUES("955","2235");
INSERT INTO invoices_tb VALUES("956","2943");
INSERT INTO invoices_tb VALUES("957","1601");
INSERT INTO invoices_tb VALUES("958","1601");
INSERT INTO invoices_tb VALUES("959","1601");
INSERT INTO invoices_tb VALUES("960","1601");
INSERT INTO invoices_tb VALUES("961","1601");
INSERT INTO invoices_tb VALUES("962","1601");
INSERT INTO invoices_tb VALUES("963","1601");
INSERT INTO invoices_tb VALUES("964","1601");
INSERT INTO invoices_tb VALUES("965","1601");
INSERT INTO invoices_tb VALUES("966","1601");
INSERT INTO invoices_tb VALUES("967","1601");
INSERT INTO invoices_tb VALUES("968","1601");
INSERT INTO invoices_tb VALUES("969","1601");
INSERT INTO invoices_tb VALUES("970","1601");
INSERT INTO invoices_tb VALUES("971","1601");
INSERT INTO invoices_tb VALUES("972","1601");
INSERT INTO invoices_tb VALUES("973","1601");
INSERT INTO invoices_tb VALUES("974","1601");
INSERT INTO invoices_tb VALUES("975","1601");
INSERT INTO invoices_tb VALUES("976","3004");
INSERT INTO invoices_tb VALUES("977","3004");
INSERT INTO invoices_tb VALUES("978","744");
INSERT INTO invoices_tb VALUES("979","1818");
INSERT INTO invoices_tb VALUES("980","1818");
INSERT INTO invoices_tb VALUES("981","1818");
INSERT INTO invoices_tb VALUES("982","1818");
INSERT INTO invoices_tb VALUES("983","2193");
INSERT INTO invoices_tb VALUES("984","517");
INSERT INTO invoices_tb VALUES("985","4771");
INSERT INTO invoices_tb VALUES("986","4771");
INSERT INTO invoices_tb VALUES("987","3672");
INSERT INTO invoices_tb VALUES("988","1685");
INSERT INTO invoices_tb VALUES("989","251");
INSERT INTO invoices_tb VALUES("990","1484");
INSERT INTO invoices_tb VALUES("991","4183");
INSERT INTO invoices_tb VALUES("992","2653");
INSERT INTO invoices_tb VALUES("993","2727");
INSERT INTO invoices_tb VALUES("994","2727");
INSERT INTO invoices_tb VALUES("995","2321");
INSERT INTO invoices_tb VALUES("996","3097");
INSERT INTO invoices_tb VALUES("997","0");
INSERT INTO invoices_tb VALUES("998","4998");
INSERT INTO invoices_tb VALUES("999","1794");
INSERT INTO invoices_tb VALUES("1000","0");
INSERT INTO invoices_tb VALUES("1001","2271");
INSERT INTO invoices_tb VALUES("1002","1599");
INSERT INTO invoices_tb VALUES("1003","3167");
INSERT INTO invoices_tb VALUES("1004","3167");
INSERT INTO invoices_tb VALUES("1005","461");
INSERT INTO invoices_tb VALUES("1006","774");
INSERT INTO invoices_tb VALUES("1007","4394");
INSERT INTO invoices_tb VALUES("1008","3732");
INSERT INTO invoices_tb VALUES("1009","4556");
INSERT INTO invoices_tb VALUES("1010","530");
INSERT INTO invoices_tb VALUES("1011","4166");
INSERT INTO invoices_tb VALUES("1012","3848");
INSERT INTO invoices_tb VALUES("1013","2855");
INSERT INTO invoices_tb VALUES("1014","4321");
INSERT INTO invoices_tb VALUES("1015","460");
INSERT INTO invoices_tb VALUES("1016","4841");
INSERT INTO invoices_tb VALUES("1017","1279");
INSERT INTO invoices_tb VALUES("1018","1279");
INSERT INTO invoices_tb VALUES("1019","1279");
INSERT INTO invoices_tb VALUES("1020","2767");
INSERT INTO invoices_tb VALUES("1021","3531");
INSERT INTO invoices_tb VALUES("1022","3375");
INSERT INTO invoices_tb VALUES("1023","3430");
INSERT INTO invoices_tb VALUES("1024","3430");
INSERT INTO invoices_tb VALUES("1025","3430");
INSERT INTO invoices_tb VALUES("1026","3430");
INSERT INTO invoices_tb VALUES("1027","3430");
INSERT INTO invoices_tb VALUES("1028","3430");
INSERT INTO invoices_tb VALUES("1029","3430");
INSERT INTO invoices_tb VALUES("1030","3430");
INSERT INTO invoices_tb VALUES("1031","3430");
INSERT INTO invoices_tb VALUES("1032","3430");
INSERT INTO invoices_tb VALUES("1033","3430");
INSERT INTO invoices_tb VALUES("1034","3430");
INSERT INTO invoices_tb VALUES("1035","3430");
INSERT INTO invoices_tb VALUES("1036","3430");
INSERT INTO invoices_tb VALUES("1037","3430");
INSERT INTO invoices_tb VALUES("1038","3430");
INSERT INTO invoices_tb VALUES("1039","3430");
INSERT INTO invoices_tb VALUES("1040","3430");
INSERT INTO invoices_tb VALUES("1041","3430");
INSERT INTO invoices_tb VALUES("1042","3430");
INSERT INTO invoices_tb VALUES("1043","3430");
INSERT INTO invoices_tb VALUES("1044","3430");
INSERT INTO invoices_tb VALUES("1045","3430");
INSERT INTO invoices_tb VALUES("1046","3430");
INSERT INTO invoices_tb VALUES("1047","3430");
INSERT INTO invoices_tb VALUES("1048","3430");
INSERT INTO invoices_tb VALUES("1049","3430");
INSERT INTO invoices_tb VALUES("1050","3430");
INSERT INTO invoices_tb VALUES("1051","3430");
INSERT INTO invoices_tb VALUES("1052","3430");
INSERT INTO invoices_tb VALUES("1053","3430");
INSERT INTO invoices_tb VALUES("1054","3430");
INSERT INTO invoices_tb VALUES("1055","3430");
INSERT INTO invoices_tb VALUES("1056","3430");
INSERT INTO invoices_tb VALUES("1057","3430");
INSERT INTO invoices_tb VALUES("1058","3430");
INSERT INTO invoices_tb VALUES("1059","3430");
INSERT INTO invoices_tb VALUES("1060","3430");
INSERT INTO invoices_tb VALUES("1061","3430");
INSERT INTO invoices_tb VALUES("1062","3430");
INSERT INTO invoices_tb VALUES("1063","3430");
INSERT INTO invoices_tb VALUES("1064","3430");
INSERT INTO invoices_tb VALUES("1065","3430");
INSERT INTO invoices_tb VALUES("1066","3430");
INSERT INTO invoices_tb VALUES("1067","3430");
INSERT INTO invoices_tb VALUES("1068","3430");
INSERT INTO invoices_tb VALUES("1069","3430");
INSERT INTO invoices_tb VALUES("1070","3430");
INSERT INTO invoices_tb VALUES("1071","3430");
INSERT INTO invoices_tb VALUES("1072","3430");
INSERT INTO invoices_tb VALUES("1073","3430");
INSERT INTO invoices_tb VALUES("1074","3430");
INSERT INTO invoices_tb VALUES("1075","3430");
INSERT INTO invoices_tb VALUES("1076","3430");
INSERT INTO invoices_tb VALUES("1077","3430");
INSERT INTO invoices_tb VALUES("1078","3430");
INSERT INTO invoices_tb VALUES("1079","4318");
INSERT INTO invoices_tb VALUES("1080","4318");
INSERT INTO invoices_tb VALUES("1081","4318");
INSERT INTO invoices_tb VALUES("1082","4318");
INSERT INTO invoices_tb VALUES("1083","4318");
INSERT INTO invoices_tb VALUES("1084","4318");
INSERT INTO invoices_tb VALUES("1085","4318");
INSERT INTO invoices_tb VALUES("1086","4318");
INSERT INTO invoices_tb VALUES("1087","4318");
INSERT INTO invoices_tb VALUES("1088","4318");
INSERT INTO invoices_tb VALUES("1089","4318");
INSERT INTO invoices_tb VALUES("1090","4318");
INSERT INTO invoices_tb VALUES("1091","4318");
INSERT INTO invoices_tb VALUES("1092","4318");
INSERT INTO invoices_tb VALUES("1093","4318");
INSERT INTO invoices_tb VALUES("1094","4318");
INSERT INTO invoices_tb VALUES("1095","4318");
INSERT INTO invoices_tb VALUES("1096","4318");
INSERT INTO invoices_tb VALUES("1097","4318");
INSERT INTO invoices_tb VALUES("1098","4318");
INSERT INTO invoices_tb VALUES("1099","4318");
INSERT INTO invoices_tb VALUES("1100","930");
INSERT INTO invoices_tb VALUES("1101","4878");
INSERT INTO invoices_tb VALUES("1102","1290");
INSERT INTO invoices_tb VALUES("1103","2178");
INSERT INTO invoices_tb VALUES("1104","3649");
INSERT INTO invoices_tb VALUES("1105","792");
INSERT INTO invoices_tb VALUES("1106","55");
INSERT INTO invoices_tb VALUES("1107","895");
INSERT INTO invoices_tb VALUES("1108","1951");
INSERT INTO invoices_tb VALUES("1109","4915");
INSERT INTO invoices_tb VALUES("1110","2389");
INSERT INTO invoices_tb VALUES("1111","2389");
INSERT INTO invoices_tb VALUES("1112","2389");
INSERT INTO invoices_tb VALUES("1113","2389");
INSERT INTO invoices_tb VALUES("1114","2389");
INSERT INTO invoices_tb VALUES("1115","2389");
INSERT INTO invoices_tb VALUES("1116","1139");
INSERT INTO invoices_tb VALUES("1117","1139");
INSERT INTO invoices_tb VALUES("1118","0");
INSERT INTO invoices_tb VALUES("1119","0");
INSERT INTO invoices_tb VALUES("1120","3487");
INSERT INTO invoices_tb VALUES("1121","0");
INSERT INTO invoices_tb VALUES("1122","0");
INSERT INTO invoices_tb VALUES("1123","0");
INSERT INTO invoices_tb VALUES("1124","0");
INSERT INTO invoices_tb VALUES("1125","0");
INSERT INTO invoices_tb VALUES("1126","0");
INSERT INTO invoices_tb VALUES("1127","0");
INSERT INTO invoices_tb VALUES("1128","0");
INSERT INTO invoices_tb VALUES("1129","0");
INSERT INTO invoices_tb VALUES("1130","0");
INSERT INTO invoices_tb VALUES("1131","0");
INSERT INTO invoices_tb VALUES("1132","0");
INSERT INTO invoices_tb VALUES("1133","0");
INSERT INTO invoices_tb VALUES("1134","0");
INSERT INTO invoices_tb VALUES("1135","0");
INSERT INTO invoices_tb VALUES("1136","2995");
INSERT INTO invoices_tb VALUES("1137","4700");
INSERT INTO invoices_tb VALUES("1138","3839");
INSERT INTO invoices_tb VALUES("1139","3806");
INSERT INTO invoices_tb VALUES("1140","1617");
INSERT INTO invoices_tb VALUES("1141","1617");
INSERT INTO invoices_tb VALUES("1142","0");
INSERT INTO invoices_tb VALUES("1143","2751");
INSERT INTO invoices_tb VALUES("1144","2751");
INSERT INTO invoices_tb VALUES("1145","0");
INSERT INTO invoices_tb VALUES("1146","0");
INSERT INTO invoices_tb VALUES("1147","824");
INSERT INTO invoices_tb VALUES("1148","824");
INSERT INTO invoices_tb VALUES("1149","3565");
INSERT INTO invoices_tb VALUES("1150","1582");



CREATE TABLE `licience_reg_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `exp_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `licience_key` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO licience_reg_tb VALUES("1","2020-01-02 00:00:00","");



CREATE TABLE `notifications_settings_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `not_count` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `open_close_cashout_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `login` text NOT NULL,
  `logout` text NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=latin1;

INSERT INTO open_close_cashout_tb VALUES("69","9","2019-05-01 16:26:56","2019-05-01 16:38:52","closed");
INSERT INTO open_close_cashout_tb VALUES("70","9","2019-05-01 17:47:22","2019-05-01 17:47:36","closed");
INSERT INTO open_close_cashout_tb VALUES("71","9","2019-05-02 08:05:35","2019-05-02 08:53:45","closed");
INSERT INTO open_close_cashout_tb VALUES("72","9","2019-05-02 09:15:44","2019-05-03 17:21:56","closed");
INSERT INTO open_close_cashout_tb VALUES("73","9","2019-05-04 07:52:51","2019-05-06 08:43:56","closed");
INSERT INTO open_close_cashout_tb VALUES("74","9","2019-05-06 09:31:14","2019-05-06 17:00:53","closed");
INSERT INTO open_close_cashout_tb VALUES("75","8","2019-05-06 13:02:54","2019-05-06 13:03:43","closed");
INSERT INTO open_close_cashout_tb VALUES("76","9","2019-05-06 17:21:32","2019-05-06 17:52:22","closed");
INSERT INTO open_close_cashout_tb VALUES("77","9","2019-05-07 07:42:09","2019-05-07 09:10:06","closed");
INSERT INTO open_close_cashout_tb VALUES("78","9","2019-05-07 09:10:13","2019-05-07 09:10:16","closed");
INSERT INTO open_close_cashout_tb VALUES("79","9","2019-05-07 09:18:20","2019-05-07 11:32:03","closed");
INSERT INTO open_close_cashout_tb VALUES("80","9","2019-05-07 15:07:30","2019-05-08 08:44:27","closed");
INSERT INTO open_close_cashout_tb VALUES("81","9","2019-05-08 08:57:30","2019-05-08 09:03:21","closed");
INSERT INTO open_close_cashout_tb VALUES("82","9","2019-05-08 09:06:08","2019-05-08 09:15:43","closed");
INSERT INTO open_close_cashout_tb VALUES("83","9","2019-05-08 09:30:18","2019-05-08 09:30:37","closed");
INSERT INTO open_close_cashout_tb VALUES("84","9","2019-05-08 10:32:11","2019-05-08 10:50:35","closed");
INSERT INTO open_close_cashout_tb VALUES("85","13","2019-05-08 10:50:24","2019-05-08 10:51:16","closed");
INSERT INTO open_close_cashout_tb VALUES("86","9","2019-05-08 10:52:58","2019-05-08 18:58:58","closed");
INSERT INTO open_close_cashout_tb VALUES("87","9","2019-05-08 19:02:44","2019-05-08 20:54:59","closed");
INSERT INTO open_close_cashout_tb VALUES("88","9","2019-05-09 08:06:22","2019-05-09 18:55:12","closed");
INSERT INTO open_close_cashout_tb VALUES("89","8","2019-05-09 14:35:30","2019-05-09 14:36:34","closed");
INSERT INTO open_close_cashout_tb VALUES("90","9","2019-05-10 08:10:45","2019-05-10 18:32:31","closed");
INSERT INTO open_close_cashout_tb VALUES("91","9","2019-05-11 07:58:20","2019-05-11 21:29:49","closed");
INSERT INTO open_close_cashout_tb VALUES("92","9","2019-05-13 08:07:25","2019-05-13 08:44:14","closed");
INSERT INTO open_close_cashout_tb VALUES("93","9","2019-05-13 08:46:20","2019-05-13 11:22:16","closed");
INSERT INTO open_close_cashout_tb VALUES("94","9","2019-05-13 12:07:54","2019-05-13 17:43:31","closed");
INSERT INTO open_close_cashout_tb VALUES("95","9","2019-05-14 07:45:44","2019-05-14 17:47:48","closed");
INSERT INTO open_close_cashout_tb VALUES("96","9","2019-05-15 08:07:26","2019-05-16 18:47:22","closed");
INSERT INTO open_close_cashout_tb VALUES("97","9","2019-05-17 08:12:21","2019-05-17 17:48:00","closed");
INSERT INTO open_close_cashout_tb VALUES("98","9","2019-05-18 08:17:03","2019-05-18 17:37:41","closed");
INSERT INTO open_close_cashout_tb VALUES("99","9","2019-05-20 08:19:52","2019-05-20 17:35:27","closed");
INSERT INTO open_close_cashout_tb VALUES("100","9","2019-05-21 08:14:43","2019-05-21 17:06:59","closed");
INSERT INTO open_close_cashout_tb VALUES("101","9","2019-05-21 17:35:55","2019-05-21 17:36:29","closed");
INSERT INTO open_close_cashout_tb VALUES("102","9","2019-05-21 18:25:02","2019-05-21 18:28:40","closed");
INSERT INTO open_close_cashout_tb VALUES("103","9","2019-05-22 07:51:03","2019-05-22 09:00:57","closed");
INSERT INTO open_close_cashout_tb VALUES("104","9","2019-05-22 14:28:27","2019-05-22 18:26:45","closed");
INSERT INTO open_close_cashout_tb VALUES("105","8","2019-05-22 16:56:53","2019-05-22 16:57:32","closed");
INSERT INTO open_close_cashout_tb VALUES("106","9","2019-05-23 08:17:22","2019-05-23 19:06:48","closed");
INSERT INTO open_close_cashout_tb VALUES("107","9","2019-05-23 19:26:22","2019-05-23 19:26:44","closed");
INSERT INTO open_close_cashout_tb VALUES("108","9","2019-05-24 08:10:19","2019-05-24 21:23:54","closed");
INSERT INTO open_close_cashout_tb VALUES("109","13","2019-05-24 10:09:54","","");
INSERT INTO open_close_cashout_tb VALUES("110","9","2019-05-25 09:05:30","2019-05-27 17:36:35","closed");
INSERT INTO open_close_cashout_tb VALUES("111","9","2019-05-28 08:09:46","2019-05-28 08:51:46","closed");
INSERT INTO open_close_cashout_tb VALUES("112","9","2019-05-28 09:27:14","2019-05-28 09:31:26","closed");
INSERT INTO open_close_cashout_tb VALUES("113","9","2019-05-28 11:03:32","2019-05-29 21:31:24","closed");
INSERT INTO open_close_cashout_tb VALUES("114","9","2019-05-29 21:31:40","2019-05-29 21:32:10","closed");
INSERT INTO open_close_cashout_tb VALUES("115","9","2019-05-30 08:22:30","2019-05-30 09:24:18","closed");
INSERT INTO open_close_cashout_tb VALUES("116","9","2019-05-30 09:24:24","2019-05-30 09:24:32","closed");
INSERT INTO open_close_cashout_tb VALUES("117","9","2019-05-30 13:20:46","2019-05-30 17:56:44","closed");
INSERT INTO open_close_cashout_tb VALUES("118","9","2019-05-31 09:38:09","2019-05-31 20:10:08","closed");
INSERT INTO open_close_cashout_tb VALUES("119","9","2019-06-01 08:33:59","2019-06-01 08:36:35","closed");
INSERT INTO open_close_cashout_tb VALUES("120","9","2019-06-01 08:38:18","2019-06-01 18:01:10","closed");
INSERT INTO open_close_cashout_tb VALUES("121","9","2019-06-03 08:25:37","2019-06-03 18:36:43","closed");
INSERT INTO open_close_cashout_tb VALUES("122","9","2019-06-03 18:37:39","2019-06-03 18:40:15","closed");
INSERT INTO open_close_cashout_tb VALUES("123","9","2019-06-03 18:47:25","2019-06-03 18:47:49","closed");
INSERT INTO open_close_cashout_tb VALUES("124","9","2019-06-04 08:19:15","2019-06-04 18:35:13","closed");
INSERT INTO open_close_cashout_tb VALUES("125","9","2019-06-04 18:35:26","2019-06-04 18:35:39","closed");
INSERT INTO open_close_cashout_tb VALUES("126","9","2019-06-05 08:25:14","2019-06-05 17:47:52","closed");
INSERT INTO open_close_cashout_tb VALUES("127","9","2019-06-05 17:48:05","2019-06-05 17:48:17","closed");
INSERT INTO open_close_cashout_tb VALUES("128","9","2019-06-06 08:16:25","2019-06-06 19:16:08","closed");
INSERT INTO open_close_cashout_tb VALUES("129","9","2019-06-06 19:16:22","2019-06-06 19:16:40","closed");
INSERT INTO open_close_cashout_tb VALUES("130","9","2019-06-07 08:17:57","2019-06-07 18:06:53","closed");
INSERT INTO open_close_cashout_tb VALUES("131","9","2019-06-07 20:30:38","2019-06-07 20:30:51","closed");
INSERT INTO open_close_cashout_tb VALUES("132","9","2019-06-08 08:54:39","2019-06-08 18:06:07","closed");
INSERT INTO open_close_cashout_tb VALUES("133","9","2019-06-08 18:06:22","2019-06-08 18:06:47","closed");
INSERT INTO open_close_cashout_tb VALUES("134","9","2019-06-10 08:46:29","2019-06-10 08:47:45","closed");
INSERT INTO open_close_cashout_tb VALUES("135","9","2019-06-10 08:47:58","2019-06-10 19:31:14","closed");
INSERT INTO open_close_cashout_tb VALUES("136","9","2019-06-10 19:31:30","2019-06-10 19:32:02","closed");
INSERT INTO open_close_cashout_tb VALUES("137","9","2019-06-11 08:08:14","2019-06-11 19:52:57","closed");
INSERT INTO open_close_cashout_tb VALUES("138","9","2019-06-11 19:53:20","2019-06-11 19:53:28","closed");
INSERT INTO open_close_cashout_tb VALUES("139","9","2019-06-12 07:43:32","2019-06-12 20:20:08","closed");
INSERT INTO open_close_cashout_tb VALUES("140","9","2019-06-13 08:21:09","2019-06-13 17:40:56","closed");
INSERT INTO open_close_cashout_tb VALUES("141","9","2019-06-13 17:41:08","2019-06-13 17:41:30","closed");
INSERT INTO open_close_cashout_tb VALUES("142","9","2019-06-14 08:13:49","2019-06-14 21:49:14","closed");
INSERT INTO open_close_cashout_tb VALUES("143","9","2019-06-14 21:49:25","2019-06-14 21:49:45","closed");
INSERT INTO open_close_cashout_tb VALUES("144","9","2019-06-15 08:20:02","2019-06-15 19:48:56","closed");
INSERT INTO open_close_cashout_tb VALUES("145","9","2019-06-15 19:49:06","2019-06-15 19:49:09","closed");
INSERT INTO open_close_cashout_tb VALUES("146","9","2019-06-15 19:49:20","2019-06-15 19:49:32","closed");
INSERT INTO open_close_cashout_tb VALUES("147","9","2019-06-15 19:52:47","2019-06-15 19:52:50","closed");
INSERT INTO open_close_cashout_tb VALUES("148","9","2019-06-17 08:15:02","2019-06-17 17:48:27","closed");
INSERT INTO open_close_cashout_tb VALUES("149","9","2019-06-17 17:48:39","2019-06-17 19:53:31","closed");
INSERT INTO open_close_cashout_tb VALUES("150","9","2019-06-18 08:28:55","2019-06-18 19:19:22","closed");
INSERT INTO open_close_cashout_tb VALUES("151","9","2019-06-19 08:13:54","2019-06-19 17:38:37","closed");
INSERT INTO open_close_cashout_tb VALUES("152","9","2019-06-20 08:13:36","2019-06-20 17:56:07","closed");
INSERT INTO open_close_cashout_tb VALUES("153","9","2019-06-20 08:13:36","2019-06-20 17:56:07","closed");
INSERT INTO open_close_cashout_tb VALUES("154","9","2019-06-21 07:03:19","2019-06-21 22:00:08","closed");
INSERT INTO open_close_cashout_tb VALUES("155","9","2019-06-22 08:41:47","2019-06-22 20:06:40","closed");
INSERT INTO open_close_cashout_tb VALUES("156","9","2019-06-24 08:58:21","2019-06-24 17:44:16","closed");
INSERT INTO open_close_cashout_tb VALUES("157","9","2019-06-24 17:45:00","2019-06-24 21:30:42","closed");
INSERT INTO open_close_cashout_tb VALUES("158","9","2019-06-24 22:20:19","","");
INSERT INTO open_close_cashout_tb VALUES("159","8","2019-06-25 15:05:54","2019-06-25 15:07:37","closed");
INSERT INTO open_close_cashout_tb VALUES("160","8","2019-06-25 16:40:23","2019-07-16 16:54:17","closed");
INSERT INTO open_close_cashout_tb VALUES("161","8","2019-07-22 20:00:22","2019-07-23 08:53:35","closed");
INSERT INTO open_close_cashout_tb VALUES("162","8","2019-07-31 13:12:01","2019-07-31 13:19:14","closed");
INSERT INTO open_close_cashout_tb VALUES("163","8","2019-07-31 13:19:21","2019-08-04 11:54:18","closed");
INSERT INTO open_close_cashout_tb VALUES("164","8","2019-08-04 11:54:45","2019-08-06 11:58:16","closed");
INSERT INTO open_close_cashout_tb VALUES("165","8","2019-08-06 12:12:03","2019-08-06 12:14:26","closed");
INSERT INTO open_close_cashout_tb VALUES("166","8","2019-08-06 14:15:34","2019-08-06 14:15:39","closed");
INSERT INTO open_close_cashout_tb VALUES("167","8","2019-08-08 14:58:09","2019-08-08 14:58:23","closed");
INSERT INTO open_close_cashout_tb VALUES("168","8","2019-08-08 15:02:09","2019-08-08 15:03:41","closed");
INSERT INTO open_close_cashout_tb VALUES("169","8","2019-08-08 15:09:59","2019-08-08 15:14:24","closed");
INSERT INTO open_close_cashout_tb VALUES("170","8","2019-08-08 17:19:30","2019-08-10 14:16:00","closed");
INSERT INTO open_close_cashout_tb VALUES("171","8","2019-08-10 14:16:12","2019-08-11 15:53:32","closed");
INSERT INTO open_close_cashout_tb VALUES("172","8","2019-08-11 16:23:27","2019-08-11 17:20:59","closed");
INSERT INTO open_close_cashout_tb VALUES("173","8","2019-08-11 17:21:05","2019-08-12 08:19:53","closed");
INSERT INTO open_close_cashout_tb VALUES("174","8","2019-08-12 08:20:01","2019-08-12 08:21:37","closed");
INSERT INTO open_close_cashout_tb VALUES("175","8","2019-08-12 13:59:07","2019-08-12 17:04:27","closed");
INSERT INTO open_close_cashout_tb VALUES("176","8","2019-08-12 17:04:32","2019-08-12 17:04:34","closed");
INSERT INTO open_close_cashout_tb VALUES("177","8","2019-08-13 08:14:56","2019-08-14 09:52:09","closed");
INSERT INTO open_close_cashout_tb VALUES("178","8","2019-08-14 10:28:35","2019-08-14 11:27:21","closed");
INSERT INTO open_close_cashout_tb VALUES("179","8","2019-08-15 13:57:23","2019-08-16 11:06:11","closed");
INSERT INTO open_close_cashout_tb VALUES("180","8","2019-08-17 15:16:30","2019-09-04 19:46:15","closed");
INSERT INTO open_close_cashout_tb VALUES("181","8","2019-09-04 19:51:04","2019-09-04 19:51:50","closed");
INSERT INTO open_close_cashout_tb VALUES("182","8","2019-09-06 17:08:12","2019-09-07 11:16:15","closed");
INSERT INTO open_close_cashout_tb VALUES("183","8","2019-09-18 08:54:36","2019-09-18 08:56:28","closed");
INSERT INTO open_close_cashout_tb VALUES("184","8","2019-09-18 10:38:23","2019-09-18 10:38:53","closed");
INSERT INTO open_close_cashout_tb VALUES("185","8","2019-09-18 11:01:23","2019-09-18 11:10:49","closed");
INSERT INTO open_close_cashout_tb VALUES("186","8","2019-09-18 11:58:36","2019-09-20 15:17:37","closed");
INSERT INTO open_close_cashout_tb VALUES("187","8","2019-09-28 13:08:04","2019-09-28 13:14:03","closed");
INSERT INTO open_close_cashout_tb VALUES("188","8","2019-10-05 19:25:47","2019-10-05 19:25:57","closed");
INSERT INTO open_close_cashout_tb VALUES("189","8","2019-10-05 19:54:42","2019-10-05 23:37:02","closed");
INSERT INTO open_close_cashout_tb VALUES("190","8","2019-10-05 23:37:07","2019-10-05 23:44:36","closed");
INSERT INTO open_close_cashout_tb VALUES("191","8","2019-10-05 23:44:42","2019-10-05 23:46:28","closed");
INSERT INTO open_close_cashout_tb VALUES("192","8","2019-10-05 23:48:46","2019-10-05 23:49:37","closed");
INSERT INTO open_close_cashout_tb VALUES("193","8","2019-10-05 23:49:46","2019-10-05 23:51:12","closed");
INSERT INTO open_close_cashout_tb VALUES("194","8","2019-10-05 23:51:36","2019-10-07 10:48:26","closed");
INSERT INTO open_close_cashout_tb VALUES("195","8","2019-10-08 08:12:56","2019-10-08 08:44:17","closed");
INSERT INTO open_close_cashout_tb VALUES("196","8","2019-10-08 23:45:50","2019-10-10 14:21:42","closed");
INSERT INTO open_close_cashout_tb VALUES("197","8","2019-10-13 16:03:33","2019-10-13 18:29:16","closed");
INSERT INTO open_close_cashout_tb VALUES("198","8","2019-10-18 08:10:01","2019-10-18 08:10:18","closed");
INSERT INTO open_close_cashout_tb VALUES("199","8","2019-10-18 08:12:36","2019-10-18 08:15:10","closed");
INSERT INTO open_close_cashout_tb VALUES("200","8","2019-10-25 19:20:03","2019-10-25 20:48:14","closed");
INSERT INTO open_close_cashout_tb VALUES("201","8","2019-10-27 20:02:02","2019-10-28 08:09:22","closed");
INSERT INTO open_close_cashout_tb VALUES("202","8","2019-10-28 13:54:12","2019-10-28 13:54:28","closed");
INSERT INTO open_close_cashout_tb VALUES("203","8","2019-10-28 13:55:13","2019-10-28 13:57:27","closed");
INSERT INTO open_close_cashout_tb VALUES("204","8","2019-10-28 14:00:19","2019-10-28 14:15:38","closed");



CREATE TABLE `open_close_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `open_bal` int(12) NOT NULL,
  `close_bal` int(12) NOT NULL,
  `date` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1243 DEFAULT CHARSET=latin1;

INSERT INTO open_close_tb VALUES("138","458","60","53","2019-05-06");
INSERT INTO open_close_tb VALUES("139","320","27","0","2019-05-06");
INSERT INTO open_close_tb VALUES("140","295","143","0","2019-05-06");
INSERT INTO open_close_tb VALUES("141","327","13","0","2019-05-06");
INSERT INTO open_close_tb VALUES("142","319","82","0","2019-05-06");
INSERT INTO open_close_tb VALUES("143","446","48","0","2019-05-06");
INSERT INTO open_close_tb VALUES("144","27","14","0","2019-05-06");
INSERT INTO open_close_tb VALUES("145","98","31","0","2019-05-06");
INSERT INTO open_close_tb VALUES("146","211","2","0","2019-05-06");
INSERT INTO open_close_tb VALUES("147","159","19","0","2019-05-06");
INSERT INTO open_close_tb VALUES("148","445","86","0","2019-05-06");
INSERT INTO open_close_tb VALUES("149","307","3","0","2019-05-06");
INSERT INTO open_close_tb VALUES("150","436","76","0","2019-05-06");
INSERT INTO open_close_tb VALUES("151","209","19","0","2019-05-06");
INSERT INTO open_close_tb VALUES("152","212","3","0","2019-05-06");
INSERT INTO open_close_tb VALUES("153","397","12","0","2019-05-06");
INSERT INTO open_close_tb VALUES("154","129","21","0","2019-05-06");
INSERT INTO open_close_tb VALUES("155","128","118","0","2019-05-06");
INSERT INTO open_close_tb VALUES("156","373","47","0","2019-05-06");
INSERT INTO open_close_tb VALUES("157","5","54","0","2019-05-06");
INSERT INTO open_close_tb VALUES("158","459","1","0","2019-05-06");
INSERT INTO open_close_tb VALUES("159","130","4","0","2019-05-06");
INSERT INTO open_close_tb VALUES("160","294","29","0","2019-05-06");
INSERT INTO open_close_tb VALUES("161","492","103","92","2019-05-07");
INSERT INTO open_close_tb VALUES("162","237","16","14","2019-05-07");
INSERT INTO open_close_tb VALUES("163","440","108","94","2019-05-07");
INSERT INTO open_close_tb VALUES("164","252","28","0","2019-05-07");
INSERT INTO open_close_tb VALUES("165","174","4","0","2019-05-07");
INSERT INTO open_close_tb VALUES("166","160","43","0","2019-05-07");
INSERT INTO open_close_tb VALUES("167","449","76","0","2019-05-07");
INSERT INTO open_close_tb VALUES("168","303","3","0","2019-05-07");
INSERT INTO open_close_tb VALUES("169","228","4","0","2019-05-07");
INSERT INTO open_close_tb VALUES("170","277","320","0","2019-05-07");
INSERT INTO open_close_tb VALUES("171","448","31","0","2019-05-07");
INSERT INTO open_close_tb VALUES("172","115","27","24","2019-05-07");
INSERT INTO open_close_tb VALUES("173","120","59","0","2019-05-07");
INSERT INTO open_close_tb VALUES("174","247","13","11","2019-05-07");
INSERT INTO open_close_tb VALUES("175","467","17","0","2019-05-07");
INSERT INTO open_close_tb VALUES("176","463","8","0","2019-05-07");
INSERT INTO open_close_tb VALUES("177","295","142","139","2019-05-07");
INSERT INTO open_close_tb VALUES("178","327","11","0","2019-05-07");
INSERT INTO open_close_tb VALUES("179","137","6","0","2019-05-07");
INSERT INTO open_close_tb VALUES("180","460","48","43","2019-05-07");
INSERT INTO open_close_tb VALUES("181","128","117","109","2019-05-07");
INSERT INTO open_close_tb VALUES("182","319","72","0","2019-05-07");
INSERT INTO open_close_tb VALUES("183","458","53","48","2019-05-07");
INSERT INTO open_close_tb VALUES("184","372","17","12","2019-05-07");
INSERT INTO open_close_tb VALUES("185","320","20","0","2019-05-07");
INSERT INTO open_close_tb VALUES("186","301","15","12","2019-05-07");
INSERT INTO open_close_tb VALUES("187","373","38","0","2019-05-07");
INSERT INTO open_close_tb VALUES("188","376","27","25","2019-05-07");
INSERT INTO open_close_tb VALUES("189","289","40","0","2019-05-07");
INSERT INTO open_close_tb VALUES("190","322","80","10","2019-05-07");
INSERT INTO open_close_tb VALUES("191","98","28","22","2019-05-07");
INSERT INTO open_close_tb VALUES("192","447","78","0","2019-05-07");
INSERT INTO open_close_tb VALUES("193","436","75","64","2019-05-07");
INSERT INTO open_close_tb VALUES("194","441","670","659","2019-05-07");
INSERT INTO open_close_tb VALUES("195","24","2","0","2019-05-07");
INSERT INTO open_close_tb VALUES("196","35","436","0","2019-05-07");
INSERT INTO open_close_tb VALUES("197","119","15","9","2019-05-07");
INSERT INTO open_close_tb VALUES("198","22","800","0","2019-05-07");
INSERT INTO open_close_tb VALUES("199","180","265","262","2019-05-07");
INSERT INTO open_close_tb VALUES("200","47","42","0","2019-05-07");
INSERT INTO open_close_tb VALUES("201","45","72","0","2019-05-07");
INSERT INTO open_close_tb VALUES("202","20","398","0","2019-05-07");
INSERT INTO open_close_tb VALUES("203","350","48","0","2019-05-07");
INSERT INTO open_close_tb VALUES("204","27","13","0","2019-05-07");
INSERT INTO open_close_tb VALUES("205","361","7","0","2019-05-07");
INSERT INTO open_close_tb VALUES("206","112","9","0","2019-05-07");
INSERT INTO open_close_tb VALUES("207","342","3","0","2019-05-07");
INSERT INTO open_close_tb VALUES("208","68","4","0","2019-05-07");
INSERT INTO open_close_tb VALUES("209","15","7","0","2019-05-07");
INSERT INTO open_close_tb VALUES("210","402","148","0","2019-05-07");
INSERT INTO open_close_tb VALUES("211","294","28","0","2019-05-07");
INSERT INTO open_close_tb VALUES("212","129","19","0","2019-05-07");
INSERT INTO open_close_tb VALUES("213","305","48","0","2019-05-07");
INSERT INTO open_close_tb VALUES("214","321","132","0","2019-05-07");
INSERT INTO open_close_tb VALUES("215","299","12","0","2019-05-07");
INSERT INTO open_close_tb VALUES("216","96","13","0","2019-05-07");
INSERT INTO open_close_tb VALUES("217","426","100","0","2019-05-07");
INSERT INTO open_close_tb VALUES("218","253","6","0","2019-05-07");
INSERT INTO open_close_tb VALUES("219","362","10","0","2019-05-07");
INSERT INTO open_close_tb VALUES("220","265","15","0","2019-05-07");
INSERT INTO open_close_tb VALUES("221","384","15","0","2019-05-07");
INSERT INTO open_close_tb VALUES("222","446","46","0","2019-05-07");
INSERT INTO open_close_tb VALUES("223","209","18","0","2019-05-07");
INSERT INTO open_close_tb VALUES("224","283","43","0","2019-05-07");
INSERT INTO open_close_tb VALUES("225","469","38","0","2019-05-07");
INSERT INTO open_close_tb VALUES("226","452","21","0","2019-05-07");
INSERT INTO open_close_tb VALUES("227","148","187","0","2019-05-07");
INSERT INTO open_close_tb VALUES("228","116","33","0","2019-05-07");
INSERT INTO open_close_tb VALUES("229","19","1","0","2019-05-07");
INSERT INTO open_close_tb VALUES("230","65","601","541","2019-05-08");
INSERT INTO open_close_tb VALUES("231","295","139","137","2019-05-08");
INSERT INTO open_close_tb VALUES("232","128","109","100","2019-05-08");
INSERT INTO open_close_tb VALUES("233","460","43","39","2019-05-08");
INSERT INTO open_close_tb VALUES("234","458","48","17","2019-05-08");
INSERT INTO open_close_tb VALUES("235","293","22","0","2019-05-08");
INSERT INTO open_close_tb VALUES("236","96","12","0","2019-05-08");
INSERT INTO open_close_tb VALUES("237","277","312","0","2019-05-08");
INSERT INTO open_close_tb VALUES("238","170","4","0","2019-05-08");
INSERT INTO open_close_tb VALUES("239","400","10","0","2019-05-08");
INSERT INTO open_close_tb VALUES("240","254","35","0","2019-05-08");
INSERT INTO open_close_tb VALUES("241","248","35","0","2019-05-08");
INSERT INTO open_close_tb VALUES("242","247","11","0","2019-05-08");
INSERT INTO open_close_tb VALUES("243","246","27","0","2019-05-08");
INSERT INTO open_close_tb VALUES("244","133","24","0","2019-05-08");
INSERT INTO open_close_tb VALUES("245","475","21","0","2019-05-08");
INSERT INTO open_close_tb VALUES("246","129","14","0","2019-05-08");
INSERT INTO open_close_tb VALUES("247","373","36","0","2019-05-08");
INSERT INTO open_close_tb VALUES("248","436","64","0","2019-05-08");
INSERT INTO open_close_tb VALUES("249","440","94","0","2019-05-08");
INSERT INTO open_close_tb VALUES("250","61","79","0","2019-05-08");
INSERT INTO open_close_tb VALUES("251","75","8","0","2019-05-08");
INSERT INTO open_close_tb VALUES("252","256","29","0","2019-05-08");
INSERT INTO open_close_tb VALUES("253","12","1","0","2019-05-08");
INSERT INTO open_close_tb VALUES("254","492","92","0","2019-05-08");
INSERT INTO open_close_tb VALUES("255","148","186","0","2019-05-08");
INSERT INTO open_close_tb VALUES("256","209","17","0","2019-05-08");
INSERT INTO open_close_tb VALUES("257","88","13","0","2019-05-08");
INSERT INTO open_close_tb VALUES("258","390","23","0","2019-05-08");
INSERT INTO open_close_tb VALUES("259","426","99","0","2019-05-08");
INSERT INTO open_close_tb VALUES("260","428","9","0","2019-05-08");
INSERT INTO open_close_tb VALUES("261","98","22","0","2019-05-08");
INSERT INTO open_close_tb VALUES("262","455","7","0","2019-05-08");
INSERT INTO open_close_tb VALUES("263","252","27","0","2019-05-08");
INSERT INTO open_close_tb VALUES("264","68","3","0","2019-05-08");
INSERT INTO open_close_tb VALUES("265","276","338","0","2019-05-09");
INSERT INTO open_close_tb VALUES("266","303","2","0","2019-05-09");
INSERT INTO open_close_tb VALUES("267","80","5","0","2019-05-09");
INSERT INTO open_close_tb VALUES("268","267","12","0","2019-05-09");
INSERT INTO open_close_tb VALUES("269","400","9","0","2019-05-09");
INSERT INTO open_close_tb VALUES("270","205","44","0","2019-05-09");
INSERT INTO open_close_tb VALUES("271","65","541","500","2019-05-09");
INSERT INTO open_close_tb VALUES("272","327","7","1","2019-05-09");
INSERT INTO open_close_tb VALUES("273","128","100","0","2019-05-09");
INSERT INTO open_close_tb VALUES("274","295","137","0","2019-05-09");
INSERT INTO open_close_tb VALUES("275","458","17","14","2019-05-09");
INSERT INTO open_close_tb VALUES("276","436","54","0","2019-05-09");
INSERT INTO open_close_tb VALUES("277","440","84","0","2019-05-09");
INSERT INTO open_close_tb VALUES("278","492","91","0","2019-05-09");
INSERT INTO open_close_tb VALUES("279","441","659","0","2019-05-09");
INSERT INTO open_close_tb VALUES("280","254","34","0","2019-05-09");
INSERT INTO open_close_tb VALUES("281","253","5","0","2019-05-09");
INSERT INTO open_close_tb VALUES("282","460","39","28","2019-05-09");
INSERT INTO open_close_tb VALUES("283","368","12","0","2019-05-09");
INSERT INTO open_close_tb VALUES("284","370","16","0","2019-05-09");
INSERT INTO open_close_tb VALUES("285","467","16","0","2019-05-09");
INSERT INTO open_close_tb VALUES("286","185","25","0","2019-05-09");
INSERT INTO open_close_tb VALUES("287","458","14","0","2019-05-10");
INSERT INTO open_close_tb VALUES("288","289","39","0","2019-05-10");
INSERT INTO open_close_tb VALUES("289","440","78","0","2019-05-10");
INSERT INTO open_close_tb VALUES("290","436","48","0","2019-05-10");
INSERT INTO open_close_tb VALUES("291","441","653","0","2019-05-10");
INSERT INTO open_close_tb VALUES("292","65","500","484","2019-05-10");
INSERT INTO open_close_tb VALUES("293","444","8","0","2019-05-10");
INSERT INTO open_close_tb VALUES("294","208","10","0","2019-05-10");
INSERT INTO open_close_tb VALUES("295","305","43","0","2019-05-10");
INSERT INTO open_close_tb VALUES("296","452","20","0","2019-05-10");
INSERT INTO open_close_tb VALUES("297","35","431","0","2019-05-10");
INSERT INTO open_close_tb VALUES("298","24","1","0","2019-05-10");
INSERT INTO open_close_tb VALUES("299","248","34","0","2019-05-10");
INSERT INTO open_close_tb VALUES("300","22","788","0","2019-05-10");
INSERT INTO open_close_tb VALUES("301","460","28","0","2019-05-10");
INSERT INTO open_close_tb VALUES("302","277","292","0","2019-05-10");
INSERT INTO open_close_tb VALUES("303","475","20","0","2019-05-10");
INSERT INTO open_close_tb VALUES("304","367","6","0","2019-05-10");
INSERT INTO open_close_tb VALUES("305","88","12","0","2019-05-11");
INSERT INTO open_close_tb VALUES("306","65","484","455","2019-05-11");
INSERT INTO open_close_tb VALUES("307","106","555","0","2019-05-11");
INSERT INTO open_close_tb VALUES("308","492","90","0","2019-05-11");
INSERT INTO open_close_tb VALUES("309","450","85","0","2019-05-11");
INSERT INTO open_close_tb VALUES("310","460","27","23","2019-05-11");
INSERT INTO open_close_tb VALUES("311","22","778","0","2019-05-11");
INSERT INTO open_close_tb VALUES("312","444","7","0","2019-05-11");
INSERT INTO open_close_tb VALUES("313","251","41","0","2019-05-11");
INSERT INTO open_close_tb VALUES("314","228","3","0","2019-05-11");
INSERT INTO open_close_tb VALUES("315","313","29","0","2019-05-11");
INSERT INTO open_close_tb VALUES("316","243","8","0","2019-05-11");
INSERT INTO open_close_tb VALUES("317","178","13","0","2019-05-11");
INSERT INTO open_close_tb VALUES("318","283","42","0","2019-05-11");
INSERT INTO open_close_tb VALUES("319","284","50","0","2019-05-11");
INSERT INTO open_close_tb VALUES("320","4","29","0","2019-05-11");
INSERT INTO open_close_tb VALUES("321","375","57","0","2019-05-11");
INSERT INTO open_close_tb VALUES("322","70","1","0","2019-05-11");
INSERT INTO open_close_tb VALUES("323","305","41","0","2019-05-11");
INSERT INTO open_close_tb VALUES("324","237","14","0","2019-05-11");
INSERT INTO open_close_tb VALUES("325","131","11","0","2019-05-11");
INSERT INTO open_close_tb VALUES("326","322","10","0","2019-05-11");
INSERT INTO open_close_tb VALUES("327","15","6","0","2019-05-11");
INSERT INTO open_close_tb VALUES("328","225","7","0","2019-05-11");
INSERT INTO open_close_tb VALUES("329","442","616","0","2019-05-11");
INSERT INTO open_close_tb VALUES("330","127","17","0","2019-05-11");
INSERT INTO open_close_tb VALUES("331","370","15","0","2019-05-13");
INSERT INTO open_close_tb VALUES("332","250","38","0","2019-05-13");
INSERT INTO open_close_tb VALUES("333","148","184","177","2019-05-13");
INSERT INTO open_close_tb VALUES("334","20","397","0","2019-05-13");
INSERT INTO open_close_tb VALUES("335","147","315","300","2019-05-13");
INSERT INTO open_close_tb VALUES("336","65","455","0","2019-05-13");
INSERT INTO open_close_tb VALUES("337","129","11","0","2019-05-13");
INSERT INTO open_close_tb VALUES("338","128","93","89","2019-05-13");
INSERT INTO open_close_tb VALUES("339","299","10","0","2019-05-13");
INSERT INTO open_close_tb VALUES("340","364","2","0","2019-05-13");
INSERT INTO open_close_tb VALUES("341","436","44","0","2019-05-13");
INSERT INTO open_close_tb VALUES("342","441","649","0","2019-05-13");
INSERT INTO open_close_tb VALUES("343","251","39","0","2019-05-13");
INSERT INTO open_close_tb VALUES("344","200","3","0","2019-05-13");
INSERT INTO open_close_tb VALUES("345","192","6","0","2019-05-13");
INSERT INTO open_close_tb VALUES("346","391","15","0","2019-05-13");
INSERT INTO open_close_tb VALUES("347","63","13","0","2019-05-13");
INSERT INTO open_close_tb VALUES("348","236","68","0","2019-05-13");
INSERT INTO open_close_tb VALUES("349","45","67","0","2019-05-14");
INSERT INTO open_close_tb VALUES("350","47","37","0","2019-05-14");
INSERT INTO open_close_tb VALUES("351","20","387","0","2019-05-14");
INSERT INTO open_close_tb VALUES("352","35","424","0","2019-05-14");
INSERT INTO open_close_tb VALUES("353","193","172","0","2019-05-14");
INSERT INTO open_close_tb VALUES("354","128","89","0","2019-05-14");
INSERT INTO open_close_tb VALUES("355","1","1","0","2019-05-14");
INSERT INTO open_close_tb VALUES("356","83","11","0","2019-05-14");
INSERT INTO open_close_tb VALUES("357","205","42","0","2019-05-14");
INSERT INTO open_close_tb VALUES("358","204","61","0","2019-05-14");
INSERT INTO open_close_tb VALUES("359","475","19","0","2019-05-14");
INSERT INTO open_close_tb VALUES("360","252","26","0","2019-05-14");
INSERT INTO open_close_tb VALUES("361","283","39","0","2019-05-14");
INSERT INTO open_close_tb VALUES("362","446","45","0","2019-05-14");
INSERT INTO open_close_tb VALUES("363","492","89","0","2019-05-14");
INSERT INTO open_close_tb VALUES("364","434","5","0","2019-05-14");
INSERT INTO open_close_tb VALUES("365","186","2","0","2019-05-14");
INSERT INTO open_close_tb VALUES("366","428","7","0","2019-05-15");
INSERT INTO open_close_tb VALUES("367","125","29","27","2019-05-15");
INSERT INTO open_close_tb VALUES("368","130","2","0","2019-05-15");
INSERT INTO open_close_tb VALUES("369","128","85","83","2019-05-15");
INSERT INTO open_close_tb VALUES("370","65","440","424","2019-05-15");
INSERT INTO open_close_tb VALUES("371","436","37","0","2019-05-15");
INSERT INTO open_close_tb VALUES("372","440","74","70","2019-05-15");
INSERT INTO open_close_tb VALUES("373","98","18","0","2019-05-15");
INSERT INTO open_close_tb VALUES("374","265","14","0","2019-05-15");
INSERT INTO open_close_tb VALUES("375","156","2","0","2019-05-15");
INSERT INTO open_close_tb VALUES("376","160","42","0","2019-05-15");
INSERT INTO open_close_tb VALUES("377","452","19","0","2019-05-15");
INSERT INTO open_close_tb VALUES("378","106","553","550","2019-05-15");
INSERT INTO open_close_tb VALUES("379","461","5","0","2019-05-15");
INSERT INTO open_close_tb VALUES("380","402","103","0","2019-05-15");
INSERT INTO open_close_tb VALUES("381","390","21","0","2019-05-15");
INSERT INTO open_close_tb VALUES("382","35","429","0","2019-05-15");
INSERT INTO open_close_tb VALUES("383","88","11","0","2019-05-15");
INSERT INTO open_close_tb VALUES("384","310","14","0","2019-05-16");
INSERT INTO open_close_tb VALUES("385","176","2","0","2019-05-16");
INSERT INTO open_close_tb VALUES("386","163","14","0","2019-05-16");
INSERT INTO open_close_tb VALUES("387","305","37","0","2019-05-16");
INSERT INTO open_close_tb VALUES("388","295","136","0","2019-05-16");
INSERT INTO open_close_tb VALUES("389","289","33","0","2019-05-16");
INSERT INTO open_close_tb VALUES("390","299","9","7","2019-05-16");
INSERT INTO open_close_tb VALUES("391","106","550","0","2019-05-16");
INSERT INTO open_close_tb VALUES("392","452","18","0","2019-05-16");
INSERT INTO open_close_tb VALUES("393","22","768","0","2019-05-16");
INSERT INTO open_close_tb VALUES("394","251","38","35","2019-05-16");
INSERT INTO open_close_tb VALUES("395","128","83","79","2019-05-17");
INSERT INTO open_close_tb VALUES("396","65","424","0","2019-05-17");
INSERT INTO open_close_tb VALUES("397","133","19","0","2019-05-17");
INSERT INTO open_close_tb VALUES("398","207","9","0","2019-05-17");
INSERT INTO open_close_tb VALUES("399","148","177","0","2019-05-17");
INSERT INTO open_close_tb VALUES("400","147","300","0","2019-05-17");
INSERT INTO open_close_tb VALUES("401","426","98","78","2019-05-17");
INSERT INTO open_close_tb VALUES("402","446","44","38","2019-05-17");
INSERT INTO open_close_tb VALUES("403","447","77","71","2019-05-17");
INSERT INTO open_close_tb VALUES("404","257","54","48","2019-05-17");
INSERT INTO open_close_tb VALUES("405","30","50","34","2019-05-17");
INSERT INTO open_close_tb VALUES("406","436","34","0","2019-05-17");
INSERT INTO open_close_tb VALUES("407","441","642","0","2019-05-17");
INSERT INTO open_close_tb VALUES("408","460","75","73","2019-05-17");
INSERT INTO open_close_tb VALUES("409","204","60","0","2019-05-17");
INSERT INTO open_close_tb VALUES("410","0","0","0","2019-05-17");
INSERT INTO open_close_tb VALUES("411","402","101","0","2019-05-17");
INSERT INTO open_close_tb VALUES("412","294","27","0","2019-05-17");
INSERT INTO open_close_tb VALUES("413","320","150","0","2019-05-17");
INSERT INTO open_close_tb VALUES("414","2","100","0","2019-05-17");
INSERT INTO open_close_tb VALUES("415","458","94","0","2019-05-17");
INSERT INTO open_close_tb VALUES("416","214","10","0","2019-05-17");
INSERT INTO open_close_tb VALUES("417","118","2","0","2019-05-17");
INSERT INTO open_close_tb VALUES("418","116","32","0","2019-05-17");
INSERT INTO open_close_tb VALUES("419","459","182","0","2019-05-17");
INSERT INTO open_close_tb VALUES("420","283","38","0","2019-05-17");
INSERT INTO open_close_tb VALUES("421","303","11","0","2019-05-18");
INSERT INTO open_close_tb VALUES("422","376","25","0","2019-05-18");
INSERT INTO open_close_tb VALUES("423","98","50","0","2019-05-18");
INSERT INTO open_close_tb VALUES("424","377","16","11","2019-05-18");
INSERT INTO open_close_tb VALUES("425","276","327","0","2019-05-18");
INSERT INTO open_close_tb VALUES("426","21","500","490","2019-05-18");
INSERT INTO open_close_tb VALUES("427","446","38","0","2019-05-18");
INSERT INTO open_close_tb VALUES("428","463","7","0","2019-05-18");
INSERT INTO open_close_tb VALUES("429","375","51","0","2019-05-18");
INSERT INTO open_close_tb VALUES("430","131","10","0","2019-05-18");
INSERT INTO open_close_tb VALUES("431","213","27","0","2019-05-18");
INSERT INTO open_close_tb VALUES("432","283","37","0","2019-05-18");
INSERT INTO open_close_tb VALUES("433","374","120","0","2019-05-18");
INSERT INTO open_close_tb VALUES("434","444","6","0","2019-05-18");
INSERT INTO open_close_tb VALUES("435","30","34","0","2019-05-18");
INSERT INTO open_close_tb VALUES("436","65","409","400","2019-05-18");
INSERT INTO open_close_tb VALUES("437","203","4","0","2019-05-18");
INSERT INTO open_close_tb VALUES("438","460","73","68","2019-05-18");
INSERT INTO open_close_tb VALUES("439","180","262","0","2019-05-18");
INSERT INTO open_close_tb VALUES("440","96","10","0","2019-05-18");
INSERT INTO open_close_tb VALUES("441","466","11","0","2019-05-18");
INSERT INTO open_close_tb VALUES("442","122","26","0","2019-05-18");
INSERT INTO open_close_tb VALUES("443","475","18","0","2019-05-18");
INSERT INTO open_close_tb VALUES("444","128","79","75","2019-05-20");
INSERT INTO open_close_tb VALUES("445","30","32","0","2019-05-20");
INSERT INTO open_close_tb VALUES("446","295","135","0","2019-05-20");
INSERT INTO open_close_tb VALUES("447","458","93","90","2019-05-20");
INSERT INTO open_close_tb VALUES("448","180","261","258","2019-05-20");
INSERT INTO open_close_tb VALUES("449","34","21","0","2019-05-20");
INSERT INTO open_close_tb VALUES("450","249","28","0","2019-05-20");
INSERT INTO open_close_tb VALUES("451","65","400","394","2019-05-20");
INSERT INTO open_close_tb VALUES("452","98","49","0","2019-05-20");
INSERT INTO open_close_tb VALUES("453","283","36","0","2019-05-20");
INSERT INTO open_close_tb VALUES("454","319","150","0","2019-05-20");
INSERT INTO open_close_tb VALUES("455","123","24","0","2019-05-20");
INSERT INTO open_close_tb VALUES("456","4","22","0","2019-05-20");
INSERT INTO open_close_tb VALUES("457","445","85","0","2019-05-20");
INSERT INTO open_close_tb VALUES("458","134","4","0","2019-05-20");
INSERT INTO open_close_tb VALUES("459","402","97","0","2019-05-20");
INSERT INTO open_close_tb VALUES("460","35","428","0","2019-05-20");
INSERT INTO open_close_tb VALUES("461","148","175","0","2019-05-20");
INSERT INTO open_close_tb VALUES("462","30","31","0","2019-05-21");
INSERT INTO open_close_tb VALUES("463","128","75","66","2019-05-21");
INSERT INTO open_close_tb VALUES("464","65","394","0","2019-05-21");
INSERT INTO open_close_tb VALUES("465","123","23","0","2019-05-21");
INSERT INTO open_close_tb VALUES("466","403","42","0","2019-05-21");
INSERT INTO open_close_tb VALUES("467","450","84","0","2019-05-21");
INSERT INTO open_close_tb VALUES("468","13","23","0","2019-05-21");
INSERT INTO open_close_tb VALUES("469","449","75","0","2019-05-21");
INSERT INTO open_close_tb VALUES("470","140","5","0","2019-05-21");
INSERT INTO open_close_tb VALUES("471","5","53","0","2019-05-21");
INSERT INTO open_close_tb VALUES("472","32","7","0","2019-05-21");
INSERT INTO open_close_tb VALUES("473","320","136","131","2019-05-21");
INSERT INTO open_close_tb VALUES("474","213","24","0","2019-05-21");
INSERT INTO open_close_tb VALUES("475","475","17","0","2019-05-21");
INSERT INTO open_close_tb VALUES("476","129","136","0","2019-05-21");
INSERT INTO open_close_tb VALUES("477","299","7","0","2019-05-21");
INSERT INTO open_close_tb VALUES("478","98","48","0","2019-05-21");
INSERT INTO open_close_tb VALUES("479","35","427","0","2019-05-21");
INSERT INTO open_close_tb VALUES("480","180","258","0","2019-05-21");
INSERT INTO open_close_tb VALUES("481","467","15","0","2019-05-21");
INSERT INTO open_close_tb VALUES("482","75","7","0","2019-05-22");
INSERT INTO open_close_tb VALUES("483","302","10","0","2019-05-22");
INSERT INTO open_close_tb VALUES("484","128","66","0","2019-05-22");
INSERT INTO open_close_tb VALUES("485","129","124","0","2019-05-22");
INSERT INTO open_close_tb VALUES("486","440","71","0","2019-05-22");
INSERT INTO open_close_tb VALUES("487","441","640","0","2019-05-22");
INSERT INTO open_close_tb VALUES("488","436","32","0","2019-05-22");
INSERT INTO open_close_tb VALUES("489","185","24","0","2019-05-22");
INSERT INTO open_close_tb VALUES("490","374","100","0","2019-05-22");
INSERT INTO open_close_tb VALUES("491","445","81","0","2019-05-22");
INSERT INTO open_close_tb VALUES("492","295","134","132","2019-05-22");
INSERT INTO open_close_tb VALUES("493","319","145","0","2019-05-22");
INSERT INTO open_close_tb VALUES("494","442","615","0","2019-05-22");
INSERT INTO open_close_tb VALUES("495","132","10","0","2019-05-22");
INSERT INTO open_close_tb VALUES("496","217","8","0","2019-05-22");
INSERT INTO open_close_tb VALUES("497","106","548","0","2019-05-22");
INSERT INTO open_close_tb VALUES("498","262","17","0","2019-05-22");
INSERT INTO open_close_tb VALUES("499","320","131","72","2019-05-22");
INSERT INTO open_close_tb VALUES("500","317","53","0","2019-05-22");
INSERT INTO open_close_tb VALUES("501","448","29","0","2019-05-22");
INSERT INTO open_close_tb VALUES("502","422","60","0","2019-05-22");
INSERT INTO open_close_tb VALUES("503","305","66","0","2019-05-22");
INSERT INTO open_close_tb VALUES("504","255","25","0","2019-05-22");
INSERT INTO open_close_tb VALUES("505","6","20","0","2019-05-22");
INSERT INTO open_close_tb VALUES("506","65","389","382","2019-05-22");
INSERT INTO open_close_tb VALUES("507","2","90","0","2019-05-22");
INSERT INTO open_close_tb VALUES("508","63","12","0","2019-05-22");
INSERT INTO open_close_tb VALUES("509","116","31","0","2019-05-23");
INSERT INTO open_close_tb VALUES("510","249","27","0","2019-05-23");
INSERT INTO open_close_tb VALUES("511","216","20","0","2019-05-23");
INSERT INTO open_close_tb VALUES("512","83","10","0","2019-05-23");
INSERT INTO open_close_tb VALUES("513","305","61","0","2019-05-23");
INSERT INTO open_close_tb VALUES("514","128","64","60","2019-05-23");
INSERT INTO open_close_tb VALUES("515","98","47","0","2019-05-23");
INSERT INTO open_close_tb VALUES("516","436","20","0","2019-05-23");
INSERT INTO open_close_tb VALUES("517","441","628","0","2019-05-23");
INSERT INTO open_close_tb VALUES("518","440","59","0","2019-05-23");
INSERT INTO open_close_tb VALUES("519","301","12","11","2019-05-23");
INSERT INTO open_close_tb VALUES("520","377","11","9","2019-05-23");
INSERT INTO open_close_tb VALUES("521","302","9","0","2019-05-23");
INSERT INTO open_close_tb VALUES("522","106","546","0","2019-05-23");
INSERT INTO open_close_tb VALUES("523","30","21","13","2019-05-23");
INSERT INTO open_close_tb VALUES("524","130","4","0","2019-05-23");
INSERT INTO open_close_tb VALUES("525","327","25","0","2019-05-23");
INSERT INTO open_close_tb VALUES("526","126","28","0","2019-05-23");
INSERT INTO open_close_tb VALUES("527","373","54","0","2019-05-23");
INSERT INTO open_close_tb VALUES("528","61","78","0","2019-05-23");
INSERT INTO open_close_tb VALUES("529","492","88","0","2019-05-23");
INSERT INTO open_close_tb VALUES("530","290","15","0","2019-05-23");
INSERT INTO open_close_tb VALUES("531","449","74","0","2019-05-23");
INSERT INTO open_close_tb VALUES("532","317","48","0","2019-05-23");
INSERT INTO open_close_tb VALUES("533","155","1","0","2019-05-23");
INSERT INTO open_close_tb VALUES("534","185","22","0","2019-05-23");
INSERT INTO open_close_tb VALUES("535","445","80","0","2019-05-23");
INSERT INTO open_close_tb VALUES("536","460","68","0","2019-05-23");
INSERT INTO open_close_tb VALUES("537","98","46","0","2019-05-24");
INSERT INTO open_close_tb VALUES("538","461","2","0","2019-05-24");
INSERT INTO open_close_tb VALUES("539","208","9","0","2019-05-24");
INSERT INTO open_close_tb VALUES("540","80","4","0","2019-05-24");
INSERT INTO open_close_tb VALUES("541","446","37","0","2019-05-24");
INSERT INTO open_close_tb VALUES("542","128","59","57","2019-05-24");
INSERT INTO open_close_tb VALUES("543","301","10","0","2019-05-24");
INSERT INTO open_close_tb VALUES("544","373","52","0","2019-05-24");
INSERT INTO open_close_tb VALUES("545","133","17","0","2019-05-24");
INSERT INTO open_close_tb VALUES("546","322","103","0","2019-05-24");
INSERT INTO open_close_tb VALUES("547","317","46","45","2019-05-24");
INSERT INTO open_close_tb VALUES("548","147","295","0","2019-05-24");
INSERT INTO open_close_tb VALUES("549","148","174","0","2019-05-24");
INSERT INTO open_close_tb VALUES("550","193","170","0","2019-05-24");
INSERT INTO open_close_tb VALUES("551","469","36","0","2019-05-24");
INSERT INTO open_close_tb VALUES("552","188","67","0","2019-05-24");
INSERT INTO open_close_tb VALUES("553","112","8","0","2019-05-24");
INSERT INTO open_close_tb VALUES("554","27","12","0","2019-05-24");
INSERT INTO open_close_tb VALUES("555","15","4","3","2019-05-24");
INSERT INTO open_close_tb VALUES("556","350","45","0","2019-05-24");
INSERT INTO open_close_tb VALUES("557","134","3","2","2019-05-24");
INSERT INTO open_close_tb VALUES("558","65","382","379","2019-05-24");
INSERT INTO open_close_tb VALUES("559","372","22","0","2019-05-24");
INSERT INTO open_close_tb VALUES("560","20","386","0","2019-05-24");
INSERT INTO open_close_tb VALUES("561","63","11","0","2019-05-24");
INSERT INTO open_close_tb VALUES("562","442","614","0","2019-05-24");
INSERT INTO open_close_tb VALUES("563","236","58","0","2019-05-24");
INSERT INTO open_close_tb VALUES("564","87","13","0","2019-05-24");
INSERT INTO open_close_tb VALUES("565","327","23","0","2019-05-24");
INSERT INTO open_close_tb VALUES("566","132","8","0","2019-05-24");
INSERT INTO open_close_tb VALUES("567","65","378","336","2019-05-25");
INSERT INTO open_close_tb VALUES("568","468","11","0","2019-05-25");
INSERT INTO open_close_tb VALUES("569","422","56","0","2019-05-25");
INSERT INTO open_close_tb VALUES("570","374","100","93","2019-05-25");
INSERT INTO open_close_tb VALUES("571","160","41","40","2019-05-25");
INSERT INTO open_close_tb VALUES("572","317","44","37","2019-05-25");
INSERT INTO open_close_tb VALUES("573","305","60","0","2019-05-25");
INSERT INTO open_close_tb VALUES("574","283","34","0","2019-05-25");
INSERT INTO open_close_tb VALUES("575","20","361","0","2019-05-25");
INSERT INTO open_close_tb VALUES("576","209","16","14","2019-05-25");
INSERT INTO open_close_tb VALUES("577","212","25","0","2019-05-25");
INSERT INTO open_close_tb VALUES("578","132","6","0","2019-05-25");
INSERT INTO open_close_tb VALUES("579","123","22","21","2019-05-25");
INSERT INTO open_close_tb VALUES("580","459","181","180","2019-05-25");
INSERT INTO open_close_tb VALUES("581","328","11","0","2019-05-25");
INSERT INTO open_close_tb VALUES("582","322","99","0","2019-05-25");
INSERT INTO open_close_tb VALUES("583","445","79","0","2019-05-25");
INSERT INTO open_close_tb VALUES("584","372","20","19","2019-05-25");
INSERT INTO open_close_tb VALUES("585","5","52","49","2019-05-25");
INSERT INTO open_close_tb VALUES("586","136","10","0","2019-05-25");
INSERT INTO open_close_tb VALUES("587","98","45","44","2019-05-25");
INSERT INTO open_close_tb VALUES("588","370","14","0","2019-05-25");
INSERT INTO open_close_tb VALUES("589","125","27","0","2019-05-25");
INSERT INTO open_close_tb VALUES("590","460","67","0","2019-05-25");
INSERT INTO open_close_tb VALUES("591","13","22","0","2019-05-25");
INSERT INTO open_close_tb VALUES("592","376","22","0","2019-05-25");
INSERT INTO open_close_tb VALUES("593","362","9","0","2019-05-27");
INSERT INTO open_close_tb VALUES("594","160","39","0","2019-05-27");
INSERT INTO open_close_tb VALUES("595","319","127","86","2019-05-27");
INSERT INTO open_close_tb VALUES("596","458","90","0","2019-05-27");
INSERT INTO open_close_tb VALUES("597","65","330","321","2019-05-27");
INSERT INTO open_close_tb VALUES("598","322","96","0","2019-05-27");
INSERT INTO open_close_tb VALUES("599","293","6","5","2019-05-27");
INSERT INTO open_close_tb VALUES("600","301","9","0","2019-05-27");
INSERT INTO open_close_tb VALUES("601","128","55","50","2019-05-27");
INSERT INTO open_close_tb VALUES("602","372","14","8","2019-05-27");
INSERT INTO open_close_tb VALUES("603","35","425","0","2019-05-27");
INSERT INTO open_close_tb VALUES("604","98","43","42","2019-05-27");
INSERT INTO open_close_tb VALUES("605","440","57","0","2019-05-27");
INSERT INTO open_close_tb VALUES("606","436","18","0","2019-05-27");
INSERT INTO open_close_tb VALUES("607","447","71","0","2019-05-27");
INSERT INTO open_close_tb VALUES("608","446","36","0","2019-05-27");
INSERT INTO open_close_tb VALUES("609","212","23","0","2019-05-27");
INSERT INTO open_close_tb VALUES("610","459","176","0","2019-05-27");
INSERT INTO open_close_tb VALUES("611","133","16","0","2019-05-27");
INSERT INTO open_close_tb VALUES("612","251","35","0","2019-05-27");
INSERT INTO open_close_tb VALUES("613","368","11","0","2019-05-27");
INSERT INTO open_close_tb VALUES("614","276","247","0","2019-05-27");
INSERT INTO open_close_tb VALUES("615","300","2","0","2019-05-27");
INSERT INTO open_close_tb VALUES("616","61","76","74","2019-05-27");
INSERT INTO open_close_tb VALUES("617","428","6","0","2019-05-27");
INSERT INTO open_close_tb VALUES("618","446","35","33","2019-05-28");
INSERT INTO open_close_tb VALUES("619","428","5","0","2019-05-28");
INSERT INTO open_close_tb VALUES("620","460","66","0","2019-05-28");
INSERT INTO open_close_tb VALUES("621","461","1","0","2019-05-28");
INSERT INTO open_close_tb VALUES("622","319","76","0","2019-05-28");
INSERT INTO open_close_tb VALUES("623","322","89","0","2019-05-28");
INSERT INTO open_close_tb VALUES("624","422","55","0","2019-05-28");
INSERT INTO open_close_tb VALUES("625","65","320","318","2019-05-28");
INSERT INTO open_close_tb VALUES("626","251","34","0","2019-05-28");
INSERT INTO open_close_tb VALUES("627","368","10","0","2019-05-28");
INSERT INTO open_close_tb VALUES("628","128","49","0","2019-05-28");
INSERT INTO open_close_tb VALUES("629","468","10","9","2019-05-28");
INSERT INTO open_close_tb VALUES("630","123","20","0","2019-05-28");
INSERT INTO open_close_tb VALUES("631","445","78","77","2019-05-28");
INSERT INTO open_close_tb VALUES("632","293","4","0","2019-05-28");
INSERT INTO open_close_tb VALUES("633","374","92","0","2019-05-28");
INSERT INTO open_close_tb VALUES("634","130","3","0","2019-05-28");
INSERT INTO open_close_tb VALUES("635","157","22","0","2019-05-28");
INSERT INTO open_close_tb VALUES("636","143","21","0","2019-05-28");
INSERT INTO open_close_tb VALUES("637","501","40","0","2019-05-28");
INSERT INTO open_close_tb VALUES("638","441","626","0","2019-05-28");
INSERT INTO open_close_tb VALUES("639","500","40","0","2019-05-28");
INSERT INTO open_close_tb VALUES("640","307","2","0","2019-05-28");
INSERT INTO open_close_tb VALUES("641","73","28","0","2019-05-28");
INSERT INTO open_close_tb VALUES("642","34","20","0","2019-05-28");
INSERT INTO open_close_tb VALUES("643","276","197","0","2019-05-28");
INSERT INTO open_close_tb VALUES("644","156","1","0","2019-05-28");
INSERT INTO open_close_tb VALUES("645","430","8","0","2019-05-28");
INSERT INTO open_close_tb VALUES("646","212","22","0","2019-05-28");
INSERT INTO open_close_tb VALUES("647","305","58","0","2019-05-29");
INSERT INTO open_close_tb VALUES("648","319","74","0","2019-05-29");
INSERT INTO open_close_tb VALUES("649","370","12","0","2019-05-29");
INSERT INTO open_close_tb VALUES("650","128","45","40","2019-05-29");
INSERT INTO open_close_tb VALUES("651","221","5","0","2019-05-29");
INSERT INTO open_close_tb VALUES("652","312","30","0","2019-05-29");
INSERT INTO open_close_tb VALUES("653","257","48","0","2019-05-29");
INSERT INTO open_close_tb VALUES("654","444","5","0","2019-05-29");
INSERT INTO open_close_tb VALUES("655","132","4","0","2019-05-29");
INSERT INTO open_close_tb VALUES("656","178","12","0","2019-05-29");
INSERT INTO open_close_tb VALUES("657","239","7","0","2019-05-29");
INSERT INTO open_close_tb VALUES("658","322","86","0","2019-05-29");
INSERT INTO open_close_tb VALUES("659","65","317","0","2019-05-29");
INSERT INTO open_close_tb VALUES("660","2","87","0","2019-05-29");
INSERT INTO open_close_tb VALUES("661","459","171","0","2019-05-29");
INSERT INTO open_close_tb VALUES("662","30","500","487","2019-05-29");
INSERT INTO open_close_tb VALUES("663","374","91","87","2019-05-29");
INSERT INTO open_close_tb VALUES("664","5","45","0","2019-05-29");
INSERT INTO open_close_tb VALUES("665","212","21","0","2019-05-29");
INSERT INTO open_close_tb VALUES("666","320","72","0","2019-05-29");
INSERT INTO open_close_tb VALUES("667","98","41","0","2019-05-29");
INSERT INTO open_close_tb VALUES("668","216","19","0","2019-05-29");
INSERT INTO open_close_tb VALUES("669","251","33","0","2019-05-29");
INSERT INTO open_close_tb VALUES("670","302","8","0","2019-05-29");
INSERT INTO open_close_tb VALUES("671","496","5","0","2019-05-29");
INSERT INTO open_close_tb VALUES("672","492","87","0","2019-05-29");
INSERT INTO open_close_tb VALUES("673","388","8","0","2019-05-29");
INSERT INTO open_close_tb VALUES("674","250","37","0","2019-05-29");
INSERT INTO open_close_tb VALUES("675","277","242","0","2019-05-30");
INSERT INTO open_close_tb VALUES("676","294","7","0","2019-05-30");
INSERT INTO open_close_tb VALUES("677","128","39","31","2019-05-30");
INSERT INTO open_close_tb VALUES("678","458","87","0","2019-05-30");
INSERT INTO open_close_tb VALUES("679","122","25","0","2019-05-30");
INSERT INTO open_close_tb VALUES("680","27","10","0","2019-05-30");
INSERT INTO open_close_tb VALUES("681","212","20","0","2019-05-30");
INSERT INTO open_close_tb VALUES("682","312","29","0","2019-05-30");
INSERT INTO open_close_tb VALUES("683","257","47","0","2019-05-30");
INSERT INTO open_close_tb VALUES("684","320","66","0","2019-05-30");
INSERT INTO open_close_tb VALUES("685","5","44","43","2019-05-30");
INSERT INTO open_close_tb VALUES("686","422","54","0","2019-05-30");
INSERT INTO open_close_tb VALUES("687","252","25","0","2019-05-30");
INSERT INTO open_close_tb VALUES("688","22","766","0","2019-05-30");
INSERT INTO open_close_tb VALUES("689","208","8","0","2019-05-30");
INSERT INTO open_close_tb VALUES("690","295","132","131","2019-05-30");
INSERT INTO open_close_tb VALUES("691","402","87","0","2019-05-30");
INSERT INTO open_close_tb VALUES("692","290","14","0","2019-05-30");
INSERT INTO open_close_tb VALUES("693","250","36","0","2019-05-30");
INSERT INTO open_close_tb VALUES("694","376","21","0","2019-05-30");
INSERT INTO open_close_tb VALUES("695","30","484","478","2019-05-30");
INSERT INTO open_close_tb VALUES("696","21","490","0","2019-05-30");
INSERT INTO open_close_tb VALUES("697","65","302","0","2019-05-30");
INSERT INTO open_close_tb VALUES("698","143","20","0","2019-05-30");
INSERT INTO open_close_tb VALUES("699","62","100","0","2019-05-30");
INSERT INTO open_close_tb VALUES("700","133","15","0","2019-05-31");
INSERT INTO open_close_tb VALUES("701","373","50","0","2019-05-31");
INSERT INTO open_close_tb VALUES("702","372","7","0","2019-05-31");
INSERT INTO open_close_tb VALUES("703","320","40","30","2019-05-31");
INSERT INTO open_close_tb VALUES("704","30","477","476","2019-05-31");
INSERT INTO open_close_tb VALUES("705","449","73","0","2019-05-31");
INSERT INTO open_close_tb VALUES("706","303","10","0","2019-05-31");
INSERT INTO open_close_tb VALUES("707","137","10","0","2019-05-31");
INSERT INTO open_close_tb VALUES("708","61","73","0","2019-05-31");
INSERT INTO open_close_tb VALUES("709","4","21","0","2019-05-31");
INSERT INTO open_close_tb VALUES("710","62","99","98","2019-05-31");
INSERT INTO open_close_tb VALUES("711","440","55","0","2019-05-31");
INSERT INTO open_close_tb VALUES("712","460","65","0","2019-05-31");
INSERT INTO open_close_tb VALUES("713","65","295","0","2019-05-31");
INSERT INTO open_close_tb VALUES("714","73","27","0","2019-05-31");
INSERT INTO open_close_tb VALUES("715","120","58","0","2019-05-31");
INSERT INTO open_close_tb VALUES("716","495","10","0","2019-05-31");
INSERT INTO open_close_tb VALUES("717","20","359","0","2019-05-31");
INSERT INTO open_close_tb VALUES("718","63","10","8","2019-05-31");
INSERT INTO open_close_tb VALUES("719","432","15","0","2019-05-31");
INSERT INTO open_close_tb VALUES("720","299","3","0","2019-05-31");
INSERT INTO open_close_tb VALUES("721","180","257","0","2019-05-31");
INSERT INTO open_close_tb VALUES("722","376","19","0","2019-05-31");
INSERT INTO open_close_tb VALUES("723","277","222","0","2019-05-31");
INSERT INTO open_close_tb VALUES("724","22","756","0","2019-05-31");
INSERT INTO open_close_tb VALUES("725","458","86","0","2019-05-31");
INSERT INTO open_close_tb VALUES("726","128","27","0","2019-05-31");
INSERT INTO open_close_tb VALUES("727","129","123","0","2019-05-31");
INSERT INTO open_close_tb VALUES("728","450","83","82","2019-05-31");
INSERT INTO open_close_tb VALUES("729","192","5","0","2019-05-31");
INSERT INTO open_close_tb VALUES("730","198","5","0","2019-05-31");
INSERT INTO open_close_tb VALUES("731","51","38","0","2019-05-31");
INSERT INTO open_close_tb VALUES("732","50","43","0","2019-05-31");
INSERT INTO open_close_tb VALUES("733","194","83","0","2019-05-31");
INSERT INTO open_close_tb VALUES("734","195","64","0","2019-05-31");
INSERT INTO open_close_tb VALUES("735","202","114","0","2019-05-31");
INSERT INTO open_close_tb VALUES("736","52","6","0","2019-05-31");
INSERT INTO open_close_tb VALUES("737","53","6","0","2019-05-31");
INSERT INTO open_close_tb VALUES("738","54","6","0","2019-05-31");
INSERT INTO open_close_tb VALUES("739","112","7","0","2019-05-31");
INSERT INTO open_close_tb VALUES("740","107","15","0","2019-05-31");
INSERT INTO open_close_tb VALUES("741","390","20","0","2019-05-31");
INSERT INTO open_close_tb VALUES("742","254","30","0","2019-05-31");
INSERT INTO open_close_tb VALUES("743","283","33","0","2019-05-31");
INSERT INTO open_close_tb VALUES("744","249","22","0","2019-05-31");
INSERT INTO open_close_tb VALUES("745","5","42","0","2019-05-31");
INSERT INTO open_close_tb VALUES("746","65","294","291","2019-06-01");
INSERT INTO open_close_tb VALUES("747","434","4","0","2019-06-01");
INSERT INTO open_close_tb VALUES("748","128","26","24","2019-06-01");
INSERT INTO open_close_tb VALUES("749","302","4","0","2019-06-01");
INSERT INTO open_close_tb VALUES("750","338","9","0","2019-06-01");
INSERT INTO open_close_tb VALUES("751","339","13","10","2019-06-01");
INSERT INTO open_close_tb VALUES("752","262","16","0","2019-06-01");
INSERT INTO open_close_tb VALUES("753","261","39","0","2019-06-01");
INSERT INTO open_close_tb VALUES("754","319","69","0","2019-06-01");
INSERT INTO open_close_tb VALUES("755","373","49","0","2019-06-01");
INSERT INTO open_close_tb VALUES("756","448","27","0","2019-06-01");
INSERT INTO open_close_tb VALUES("757","295","130","0","2019-06-01");
INSERT INTO open_close_tb VALUES("758","458","85","0","2019-06-01");
INSERT INTO open_close_tb VALUES("759","123","19","0","2019-06-01");
INSERT INTO open_close_tb VALUES("760","355","16","0","2019-06-01");
INSERT INTO open_close_tb VALUES("761","3","30","0","2019-06-01");
INSERT INTO open_close_tb VALUES("762","4","20","0","2019-06-01");
INSERT INTO open_close_tb VALUES("763","225","6","0","2019-06-01");
INSERT INTO open_close_tb VALUES("764","212","18","0","2019-06-01");
INSERT INTO open_close_tb VALUES("765","320","26","20","2019-06-01");
INSERT INTO open_close_tb VALUES("766","452","17","0","2019-06-01");
INSERT INTO open_close_tb VALUES("767","299","2","0","2019-06-01");
INSERT INTO open_close_tb VALUES("768","460","62","0","2019-06-01");
INSERT INTO open_close_tb VALUES("769","443","65","0","2019-06-01");
INSERT INTO open_close_tb VALUES("770","501","38","0","2019-06-01");
INSERT INTO open_close_tb VALUES("771","374","84","82","2019-06-01");
INSERT INTO open_close_tb VALUES("772","322","66","0","2019-06-01");
INSERT INTO open_close_tb VALUES("773","317","36","0","2019-06-01");
INSERT INTO open_close_tb VALUES("774","442","613","0","2019-06-01");
INSERT INTO open_close_tb VALUES("775","5","40","0","2019-06-01");
INSERT INTO open_close_tb VALUES("776","375","51","0","2019-06-01");
INSERT INTO open_close_tb VALUES("777","137","8","0","2019-06-01");
INSERT INTO open_close_tb VALUES("778","62","97","0","2019-06-03");
INSERT INTO open_close_tb VALUES("779","123","15","0","2019-06-03");
INSERT INTO open_close_tb VALUES("780","128","14","12","2019-06-03");
INSERT INTO open_close_tb VALUES("781","295","128","0","2019-06-03");
INSERT INTO open_close_tb VALUES("782","182","15","0","2019-06-03");
INSERT INTO open_close_tb VALUES("783","137","5","4","2019-06-03");
INSERT INTO open_close_tb VALUES("784","373","48","43","2019-06-03");
INSERT INTO open_close_tb VALUES("785","362","8","0","2019-06-03");
INSERT INTO open_close_tb VALUES("786","312","28","0","2019-06-03");
INSERT INTO open_close_tb VALUES("787","313","28","0","2019-06-03");
INSERT INTO open_close_tb VALUES("788","31","7","6","2019-06-03");
INSERT INTO open_close_tb VALUES("789","492","45","0","2019-06-03");
INSERT INTO open_close_tb VALUES("790","30","474","0","2019-06-03");
INSERT INTO open_close_tb VALUES("791","303","8","0","2019-06-03");
INSERT INTO open_close_tb VALUES("792","372","3","0","2019-06-03");
INSERT INTO open_close_tb VALUES("793","5","38","0","2019-06-03");
INSERT INTO open_close_tb VALUES("794","4","19","0","2019-06-03");
INSERT INTO open_close_tb VALUES("795","163","13","0","2019-06-03");
INSERT INTO open_close_tb VALUES("796","310","13","0","2019-06-03");
INSERT INTO open_close_tb VALUES("797","209","13","0","2019-06-03");
INSERT INTO open_close_tb VALUES("798","374","81","0","2019-06-03");
INSERT INTO open_close_tb VALUES("799","88","10","0","2019-06-03");
INSERT INTO open_close_tb VALUES("800","277","221","0","2019-06-03");
INSERT INTO open_close_tb VALUES("801","322","59","0","2019-06-03");
INSERT INTO open_close_tb VALUES("802","178","11","0","2019-06-03");
INSERT INTO open_close_tb VALUES("803","30","472","452","2019-06-04");
INSERT INTO open_close_tb VALUES("804","320","10","0","2019-06-04");
INSERT INTO open_close_tb VALUES("805","133","14","13","2019-06-04");
INSERT INTO open_close_tb VALUES("806","375","45","0","2019-06-04");
INSERT INTO open_close_tb VALUES("807","21","487","0","2019-06-04");
INSERT INTO open_close_tb VALUES("808","64","57","0","2019-06-04");
INSERT INTO open_close_tb VALUES("809","373","42","0","2019-06-04");
INSERT INTO open_close_tb VALUES("810","443","56","0","2019-06-04");
INSERT INTO open_close_tb VALUES("811","131","7","0","2019-06-04");
INSERT INTO open_close_tb VALUES("812","217","7","0","2019-06-04");
INSERT INTO open_close_tb VALUES("813","182","14","0","2019-06-04");
INSERT INTO open_close_tb VALUES("814","211","1","0","2019-06-04");
INSERT INTO open_close_tb VALUES("815","89","13","12","2019-06-04");
INSERT INTO open_close_tb VALUES("816","98","39","38","2019-06-04");
INSERT INTO open_close_tb VALUES("817","319","66","0","2019-06-04");
INSERT INTO open_close_tb VALUES("818","42","4","0","2019-06-04");
INSERT INTO open_close_tb VALUES("819","492","60","0","2019-06-04");
INSERT INTO open_close_tb VALUES("820","501","37","0","2019-06-04");
INSERT INTO open_close_tb VALUES("821","500","39","0","2019-06-04");
INSERT INTO open_close_tb VALUES("822","317","35","0","2019-06-04");
INSERT INTO open_close_tb VALUES("823","449","71","0","2019-06-04");
INSERT INTO open_close_tb VALUES("824","422","51","0","2019-06-04");
INSERT INTO open_close_tb VALUES("825","65","287","277","2019-06-05");
INSERT INTO open_close_tb VALUES("826","208","7","0","2019-06-05");
INSERT INTO open_close_tb VALUES("827","30","432","0","2019-06-05");
INSERT INTO open_close_tb VALUES("828","98","37","0","2019-06-05");
INSERT INTO open_close_tb VALUES("829","20","344","0","2019-06-05");
INSERT INTO open_close_tb VALUES("830","458","84","0","2019-06-05");
INSERT INTO open_close_tb VALUES("831","214","9","0","2019-06-05");
INSERT INTO open_close_tb VALUES("832","312","27","0","2019-06-05");
INSERT INTO open_close_tb VALUES("833","13","20","0","2019-06-05");
INSERT INTO open_close_tb VALUES("834","255","23","0","2019-06-05");
INSERT INTO open_close_tb VALUES("835","256","28","0","2019-06-05");
INSERT INTO open_close_tb VALUES("836","319","59","57","2019-06-05");
INSERT INTO open_close_tb VALUES("837","317","33","0","2019-06-05");
INSERT INTO open_close_tb VALUES("838","448","24","0","2019-06-05");
INSERT INTO open_close_tb VALUES("839","102","13","0","2019-06-05");
INSERT INTO open_close_tb VALUES("840","374","80","0","2019-06-05");
INSERT INTO open_close_tb VALUES("841","459","168","0","2019-06-05");
INSERT INTO open_close_tb VALUES("842","302","3","0","2019-06-05");
INSERT INTO open_close_tb VALUES("843","123","13","0","2019-06-05");
INSERT INTO open_close_tb VALUES("844","371","16","0","2019-06-05");
INSERT INTO open_close_tb VALUES("845","469","35","0","2019-06-06");
INSERT INTO open_close_tb VALUES("846","133","6","0","2019-06-06");
INSERT INTO open_close_tb VALUES("847","98","36","0","2019-06-06");
INSERT INTO open_close_tb VALUES("848","305","51","0","2019-06-06");
INSERT INTO open_close_tb VALUES("849","283","31","0","2019-06-06");
INSERT INTO open_close_tb VALUES("850","214","8","6","2019-06-06");
INSERT INTO open_close_tb VALUES("851","442","612","0","2019-06-06");
INSERT INTO open_close_tb VALUES("852","400","6","0","2019-06-06");
INSERT INTO open_close_tb VALUES("853","62","95","0","2019-06-06");
INSERT INTO open_close_tb VALUES("854","65","276","270","2019-06-06");
INSERT INTO open_close_tb VALUES("855","460","61","0","2019-06-06");
INSERT INTO open_close_tb VALUES("856","116","29","0","2019-06-06");
INSERT INTO open_close_tb VALUES("857","35","424","0","2019-06-06");
INSERT INTO open_close_tb VALUES("858","270","1","0","2019-06-06");
INSERT INTO open_close_tb VALUES("859","435","15","0","2019-06-06");
INSERT INTO open_close_tb VALUES("860","370","10","0","2019-06-06");
INSERT INTO open_close_tb VALUES("861","254","24","0","2019-06-06");
INSERT INTO open_close_tb VALUES("862","299","1","0","2019-06-07");
INSERT INTO open_close_tb VALUES("863","327","22","0","2019-06-07");
INSERT INTO open_close_tb VALUES("864","295","127","0","2019-06-07");
INSERT INTO open_close_tb VALUES("865","122","23","0","2019-06-07");
INSERT INTO open_close_tb VALUES("866","390","19","0","2019-06-07");
INSERT INTO open_close_tb VALUES("867","6","19","0","2019-06-07");
INSERT INTO open_close_tb VALUES("868","62","65","0","2019-06-07");
INSERT INTO open_close_tb VALUES("869","212","17","0","2019-06-07");
INSERT INTO open_close_tb VALUES("870","96","8","0","2019-06-07");
INSERT INTO open_close_tb VALUES("871","375","44","0","2019-06-07");
INSERT INTO open_close_tb VALUES("872","373","40","0","2019-06-07");
INSERT INTO open_close_tb VALUES("873","460","59","0","2019-06-07");
INSERT INTO open_close_tb VALUES("874","65","268","265","2019-06-07");
INSERT INTO open_close_tb VALUES("875","444","4","0","2019-06-07");
INSERT INTO open_close_tb VALUES("876","450","81","0","2019-06-07");
INSERT INTO open_close_tb VALUES("877","445","76","0","2019-06-07");
INSERT INTO open_close_tb VALUES("878","305","43","0","2019-06-07");
INSERT INTO open_close_tb VALUES("879","317","32","31","2019-06-07");
INSERT INTO open_close_tb VALUES("880","458","83","59","2019-06-07");
INSERT INTO open_close_tb VALUES("881","27","9","0","2019-06-07");
INSERT INTO open_close_tb VALUES("882","384","14","0","2019-06-07");
INSERT INTO open_close_tb VALUES("883","383","20","0","2019-06-07");
INSERT INTO open_close_tb VALUES("884","446","32","0","2019-06-07");
INSERT INTO open_close_tb VALUES("885","447","70","69","2019-06-07");
INSERT INTO open_close_tb VALUES("886","463","6","0","2019-06-07");
INSERT INTO open_close_tb VALUES("887","180","256","0","2019-06-07");
INSERT INTO open_close_tb VALUES("888","422","49","0","2019-06-07");
INSERT INTO open_close_tb VALUES("889","459","167","0","2019-06-07");
INSERT INTO open_close_tb VALUES("890","21","485","0","2019-06-07");
INSERT INTO open_close_tb VALUES("891","4","16","0","2019-06-07");
INSERT INTO open_close_tb VALUES("892","13","17","0","2019-06-08");
INSERT INTO open_close_tb VALUES("893","27","8","0","2019-06-08");
INSERT INTO open_close_tb VALUES("894","129","122","0","2019-06-08");
INSERT INTO open_close_tb VALUES("895","373","39","0","2019-06-08");
INSERT INTO open_close_tb VALUES("896","65","263","213","2019-06-08");
INSERT INTO open_close_tb VALUES("897","193","155","135","2019-06-08");
INSERT INTO open_close_tb VALUES("898","20","341","331","2019-06-08");
INSERT INTO open_close_tb VALUES("899","241","147","0","2019-06-08");
INSERT INTO open_close_tb VALUES("900","305","38","0","2019-06-08");
INSERT INTO open_close_tb VALUES("901","495","9","8","2019-06-08");
INSERT INTO open_close_tb VALUES("902","75","6","0","2019-06-08");
INSERT INTO open_close_tb VALUES("903","319","43","0","2019-06-08");
INSERT INTO open_close_tb VALUES("904","317","29","0","2019-06-08");
INSERT INTO open_close_tb VALUES("905","422","48","0","2019-06-08");
INSERT INTO open_close_tb VALUES("906","446","30","0","2019-06-08");
INSERT INTO open_close_tb VALUES("907","277","206","0","2019-06-08");
INSERT INTO open_close_tb VALUES("908","458","58","0","2019-06-08");
INSERT INTO open_close_tb VALUES("909","255","22","0","2019-06-08");
INSERT INTO open_close_tb VALUES("910","375","42","0","2019-06-08");
INSERT INTO open_close_tb VALUES("911","249","21","0","2019-06-08");
INSERT INTO open_close_tb VALUES("912","35","423","0","2019-06-08");
INSERT INTO open_close_tb VALUES("913","390","18","0","2019-06-08");
INSERT INTO open_close_tb VALUES("914","133","5","0","2019-06-08");
INSERT INTO open_close_tb VALUES("915","30","431","0","2019-06-10");
INSERT INTO open_close_tb VALUES("916","459","166","0","2019-06-10");
INSERT INTO open_close_tb VALUES("917","393","9","0","2019-06-10");
INSERT INTO open_close_tb VALUES("918","322","53","0","2019-06-10");
INSERT INTO open_close_tb VALUES("919","302","2","0","2019-06-10");
INSERT INTO open_close_tb VALUES("920","20","329","0","2019-06-10");
INSERT INTO open_close_tb VALUES("921","436","16","0","2019-06-10");
INSERT INTO open_close_tb VALUES("922","440","42","0","2019-06-10");
INSERT INTO open_close_tb VALUES("923","441","624","0","2019-06-10");
INSERT INTO open_close_tb VALUES("924","319","41","0","2019-06-10");
INSERT INTO open_close_tb VALUES("925","307","1","0","2019-06-10");
INSERT INTO open_close_tb VALUES("926","213","24","0","2019-06-10");
INSERT INTO open_close_tb VALUES("927","65","205","183","2019-06-10");
INSERT INTO open_close_tb VALUES("928","261","29","0","2019-06-10");
INSERT INTO open_close_tb VALUES("929","262","6","0","2019-06-10");
INSERT INTO open_close_tb VALUES("930","246","26","0","2019-06-10");
INSERT INTO open_close_tb VALUES("931","458","57","0","2019-06-10");
INSERT INTO open_close_tb VALUES("932","212","16","0","2019-06-10");
INSERT INTO open_close_tb VALUES("933","295","123","0","2019-06-10");
INSERT INTO open_close_tb VALUES("934","321","129","123","2019-06-11");
INSERT INTO open_close_tb VALUES("935","317","28","0","2019-06-11");
INSERT INTO open_close_tb VALUES("936","98","35","0","2019-06-11");
INSERT INTO open_close_tb VALUES("937","133","4","0","2019-06-11");
INSERT INTO open_close_tb VALUES("938","373","38","0","2019-06-11");
INSERT INTO open_close_tb VALUES("939","295","122","0","2019-06-11");
INSERT INTO open_close_tb VALUES("940","440","37","0","2019-06-11");
INSERT INTO open_close_tb VALUES("941","305","35","0","2019-06-11");
INSERT INTO open_close_tb VALUES("942","65","178","172","2019-06-11");
INSERT INTO open_close_tb VALUES("943","120","57","0","2019-06-11");
INSERT INTO open_close_tb VALUES("944","117","6","0","2019-06-11");
INSERT INTO open_close_tb VALUES("945","35","422","0","2019-06-11");
INSERT INTO open_close_tb VALUES("946","319","33","0","2019-06-11");
INSERT INTO open_close_tb VALUES("947","3","29","0","2019-06-11");
INSERT INTO open_close_tb VALUES("948","65","162","129","2019-06-12");
INSERT INTO open_close_tb VALUES("949","322","100","0","2019-06-12");
INSERT INTO open_close_tb VALUES("950","75","5","0","2019-06-13");
INSERT INTO open_close_tb VALUES("951","34","19","0","2019-06-13");
INSERT INTO open_close_tb VALUES("952","208","6","0","2019-06-13");
INSERT INTO open_close_tb VALUES("953","128","165","0","2019-06-14");
INSERT INTO open_close_tb VALUES("954","295","121","0","2019-06-14");
INSERT INTO open_close_tb VALUES("955","440","36","0","2019-06-14");
INSERT INTO open_close_tb VALUES("956","441","619","0","2019-06-14");
INSERT INTO open_close_tb VALUES("957","106","545","0","2019-06-14");
INSERT INTO open_close_tb VALUES("958","180","254","0","2019-06-14");
INSERT INTO open_close_tb VALUES("959","460","53","0","2019-06-14");
INSERT INTO open_close_tb VALUES("960","214","5","0","2019-06-14");
INSERT INTO open_close_tb VALUES("961","442","611","0","2019-06-14");
INSERT INTO open_close_tb VALUES("962","65","128","0","2019-06-14");
INSERT INTO open_close_tb VALUES("963","56","14","0","2019-06-14");
INSERT INTO open_close_tb VALUES("964","402","84","0","2019-06-14");
INSERT INTO open_close_tb VALUES("965","317","27","0","2019-06-14");
INSERT INTO open_close_tb VALUES("966","221","4","0","2019-06-14");
INSERT INTO open_close_tb VALUES("967","446","29","0","2019-06-14");
INSERT INTO open_close_tb VALUES("968","96","7","0","2019-06-14");
INSERT INTO open_close_tb VALUES("969","5","36","0","2019-06-14");
INSERT INTO open_close_tb VALUES("970","133","3","0","2019-06-14");
INSERT INTO open_close_tb VALUES("971","458","56","0","2019-06-14");
INSERT INTO open_close_tb VALUES("972","57","12","0","2019-06-14");
INSERT INTO open_close_tb VALUES("973","248","33","0","2019-06-14");
INSERT INTO open_close_tb VALUES("974","249","20","0","2019-06-14");
INSERT INTO open_close_tb VALUES("975","260","22","0","2019-06-14");
INSERT INTO open_close_tb VALUES("976","30","428","0","2019-06-14");
INSERT INTO open_close_tb VALUES("977","273","1","0","2019-06-14");
INSERT INTO open_close_tb VALUES("978","151","3","0","2019-06-14");
INSERT INTO open_close_tb VALUES("979","384","13","0","2019-06-15");
INSERT INTO open_close_tb VALUES("980","7","3","0","2019-06-15");
INSERT INTO open_close_tb VALUES("981","321","103","98","2019-06-15");
INSERT INTO open_close_tb VALUES("982","393","8","0","2019-06-15");
INSERT INTO open_close_tb VALUES("983","129","121","0","2019-06-15");
INSERT INTO open_close_tb VALUES("984","128","159","153","2019-06-15");
INSERT INTO open_close_tb VALUES("985","295","119","0","2019-06-15");
INSERT INTO open_close_tb VALUES("986","65","120","89","2019-06-15");
INSERT INTO open_close_tb VALUES("987","73","26","0","2019-06-15");
INSERT INTO open_close_tb VALUES("988","375","41","0","2019-06-15");
INSERT INTO open_close_tb VALUES("989","373","34","0","2019-06-15");
INSERT INTO open_close_tb VALUES("990","5","34","0","2019-06-15");
INSERT INTO open_close_tb VALUES("991","61","71","0","2019-06-15");
INSERT INTO open_close_tb VALUES("992","122","22","0","2019-06-15");
INSERT INTO open_close_tb VALUES("993","317","26","21","2019-06-15");
INSERT INTO open_close_tb VALUES("994","319","17","0","2019-06-15");
INSERT INTO open_close_tb VALUES("995","120","53","0","2019-06-15");
INSERT INTO open_close_tb VALUES("996","62","62","61","2019-06-15");
INSERT INTO open_close_tb VALUES("997","248","32","0","2019-06-15");
INSERT INTO open_close_tb VALUES("998","89","9","0","2019-06-15");
INSERT INTO open_close_tb VALUES("999","283","30","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1000","501","35","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1001","422","47","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1002","30","426","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1003","251","32","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1004","13","16","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1005","14","24","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1006","98","34","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1007","136","9","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1008","163","12","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1009","20","322","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1010","65","69","54","2019-06-17");
INSERT INTO open_close_tb VALUES("1011","26","20","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1012","232","17","16","2019-06-17");
INSERT INTO open_close_tb VALUES("1013","305","32","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1014","375","39","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1015","128","151","145","2019-06-17");
INSERT INTO open_close_tb VALUES("1016","295","116","115","2019-06-17");
INSERT INTO open_close_tb VALUES("1017","373","32","29","2019-06-17");
INSERT INTO open_close_tb VALUES("1018","422","45","43","2019-06-17");
INSERT INTO open_close_tb VALUES("1019","459","165","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1020","319","13","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1021","317","19","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1022","98","33","32","2019-06-17");
INSERT INTO open_close_tb VALUES("1023","30","420","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1024","5","32","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1025","321","88","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1026","460","51","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1027","214","4","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1028","449","70","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1029","246","25","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1030","434","3","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1031","495","7","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1032","65","43","42","2019-06-18");
INSERT INTO open_close_tb VALUES("1033","106","544","541","2019-06-18");
INSERT INTO open_close_tb VALUES("1034","20","312","310","2019-06-18");
INSERT INTO open_close_tb VALUES("1035","236","38","36","2019-06-18");
INSERT INTO open_close_tb VALUES("1036","128","143","138","2019-06-18");
INSERT INTO open_close_tb VALUES("1037","129","115","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1038","373","26","19","2019-06-18");
INSERT INTO open_close_tb VALUES("1039","98","30","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1040","449","69","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1041","328","9","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1042","375","38","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1043","36","94","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1044","320","1","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1045","63","7","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1046","148","172","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1047","147","287","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1048","460","49","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1049","30","416","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1050","283","29","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1051","193","134","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1052","447","68","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1053","445","75","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1054","446","28","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1055","384","12","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1056","369","17","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1057","422","42","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1058","133","2","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1059","303","7","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1060","65","7","4","2019-06-19");
INSERT INTO open_close_tb VALUES("1061","128","135","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1062","295","114","113","2019-06-19");
INSERT INTO open_close_tb VALUES("1063","458","54","52","2019-06-19");
INSERT INTO open_close_tb VALUES("1064","207","8","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1065","30","406","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1066","98","29","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1067","237","13","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1068","3","28","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1069","275","130","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1070","16","8","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1071","458","51","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1072","128","133","128","2019-06-20");
INSERT INTO open_close_tb VALUES("1073","343","56","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1074","301","8","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1075","2","83","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1076","295","112","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1077","321","85","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1078","440","32","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1079","5","27","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1080","333","11","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1081","30","401","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1082","373","17","15","2019-06-20");
INSERT INTO open_close_tb VALUES("1083","176","1","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1084","447","62","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1085","442","607","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1086","203","2","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1087","209","12","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1088","61","69","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1089","214","3","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1090","452","16","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1091","106","540","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1092","98","28","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1093","247","10","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1094","107","13","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1095","390","17","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1096","333","10","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1097","237","12","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1098","373","11","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1099","369","16","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1100","30","395","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1101","458","50","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1102","31","4","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1103","130","1","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1104","136","8","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1105","133","1","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1106","76","3","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1107","452","15","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1108","400","5","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1109","30","394","388","2019-06-22");
INSERT INTO open_close_tb VALUES("1110","369","15","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1111","449","68","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1112","446","22","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1113","317","18","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1114","422","39","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1115","128","127","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1116","98","27","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1117","300","1","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1118","302","1","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1119","226","5","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1120","228","2","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1121","312","26","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1122","6","18","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1123","459","164","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1124","20","300","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1125","366","1","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1126","116","28","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1127","283","27","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1128","390","13","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1129","322","96","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1130","88","9","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1131","336","19","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1132","160","38","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1133","467","14","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1134","442","606","599","2019-06-24");
INSERT INTO open_close_tb VALUES("1135","422","35","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1136","119","9","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1137","321","81","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1138","428","4","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1139","128","126","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1140","295","111","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1141","495","5","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1142","434","2","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1143","124","13","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1144","460","45","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1145","180","252","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1146","319","7","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1147","106","539","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1148","449","65","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1149","21","479","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1150","179","96","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1151","147","286","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1152","214","2","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1153","277","201","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1154","62","58","0","2019-06-25");
INSERT INTO open_close_tb VALUES("1155","64","56","0","2019-06-25");
INSERT INTO open_close_tb VALUES("1156","312","25","0","2019-06-25");
INSERT INTO open_close_tb VALUES("1157","1","1","0","2019-06-25");
INSERT INTO open_close_tb VALUES("1158","3","26","0","2019-07-16");
INSERT INTO open_close_tb VALUES("1159","6","16","0","2019-07-16");
INSERT INTO open_close_tb VALUES("1160","2","81","77","2019-07-22");
INSERT INTO open_close_tb VALUES("1161","2","75","0","2019-07-23");
INSERT INTO open_close_tb VALUES("1162","7","1","0","2019-07-23");
INSERT INTO open_close_tb VALUES("1163","10","3","0","2019-07-23");
INSERT INTO open_close_tb VALUES("1164","6","15","0","2019-07-23");
INSERT INTO open_close_tb VALUES("1165","12","22","20","2019-07-31");
INSERT INTO open_close_tb VALUES("1166","13","20","18","2019-07-31");
INSERT INTO open_close_tb VALUES("1167","23","0","0","2019-08-08");
INSERT INTO open_close_tb VALUES("1168","25","20","14","2019-08-08");
INSERT INTO open_close_tb VALUES("1169","2","144","141","2019-08-11");
INSERT INTO open_close_tb VALUES("1170","18","6","0","2019-08-11");
INSERT INTO open_close_tb VALUES("1171","8","30","0","2019-08-11");
INSERT INTO open_close_tb VALUES("1172","25","36","0","2019-08-11");
INSERT INTO open_close_tb VALUES("1173","29","120","0","2019-08-11");
INSERT INTO open_close_tb VALUES("1174","2","138","124","2019-08-12");
INSERT INTO open_close_tb VALUES("1175","5","72","0","2019-08-12");
INSERT INTO open_close_tb VALUES("1176","4","72","0","2019-08-12");
INSERT INTO open_close_tb VALUES("1177","50","72","0","2019-08-12");
INSERT INTO open_close_tb VALUES("1178","29","119","115","2019-08-12");
INSERT INTO open_close_tb VALUES("1179","2","122","0","2019-08-15");
INSERT INTO open_close_tb VALUES("1180","2","119","111","2019-08-17");
INSERT INTO open_close_tb VALUES("1181","29","114","113","2019-08-17");
INSERT INTO open_close_tb VALUES("1182","56","36","0","2019-08-17");
INSERT INTO open_close_tb VALUES("1183","3","144","0","2019-08-17");
INSERT INTO open_close_tb VALUES("1184","27","36","35","2019-08-17");
INSERT INTO open_close_tb VALUES("1185","2","108","107","2019-08-20");
INSERT INTO open_close_tb VALUES("1186","29","111","110","2019-08-20");
INSERT INTO open_close_tb VALUES("1187","2","116","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1188","5","71","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1189","4","71","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1190","27","32","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1191","29","109","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1192","52","48","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1193","2","114","111","2019-09-18");
INSERT INTO open_close_tb VALUES("1194","5","70","70","2019-09-18");
INSERT INTO open_close_tb VALUES("1195","4","70","0","2019-09-18");
INSERT INTO open_close_tb VALUES("1196","29","108","0","2019-09-18");
INSERT INTO open_close_tb VALUES("1197","2","109","0","2019-09-20");
INSERT INTO open_close_tb VALUES("1198","4","70","0","2019-09-20");
INSERT INTO open_close_tb VALUES("1199","767","10","1","2019-10-05");
INSERT INTO open_close_tb VALUES("1200","172","10","9","2019-10-05");
INSERT INTO open_close_tb VALUES("1201","372","10","9","2019-10-05");
INSERT INTO open_close_tb VALUES("1202","779","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1203","502","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1204","117","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1205","371","10","8","2019-10-05");
INSERT INTO open_close_tb VALUES("1206","246","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1207","100","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1208","471","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1209","471","9","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1210","371","7","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1211","172","7","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1212","133","10","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1213","24","10","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1214","32","10","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1215","840","10","0","2019-10-08");
INSERT INTO open_close_tb VALUES("1216","729","10","0","2019-10-08");
INSERT INTO open_close_tb VALUES("1217","471","8","0","2019-10-08");
INSERT INTO open_close_tb VALUES("1218","33","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1219","779","9","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1220","61","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1221","2","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1222","18","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1223","471","6","5","2019-10-09");
INSERT INTO open_close_tb VALUES("1224","371","6","3","2019-10-09");
INSERT INTO open_close_tb VALUES("1225","372","8","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1226","663","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1227","767","15","14","2019-10-13");
INSERT INTO open_close_tb VALUES("1228","371","1","0","2019-10-13");
INSERT INTO open_close_tb VALUES("1229","172","30","0","2019-10-13");
INSERT INTO open_close_tb VALUES("1230","133","9","0","2019-10-13");
INSERT INTO open_close_tb VALUES("1231","471","4","0","2019-10-13");
INSERT INTO open_close_tb VALUES("1232","832","10","0","2019-10-18");
INSERT INTO open_close_tb VALUES("1233","420","10","0","2019-10-18");
INSERT INTO open_close_tb VALUES("1234","767","1693","1691","2019-10-25");
INSERT INTO open_close_tb VALUES("1235","371","650","0","2019-10-25");
INSERT INTO open_close_tb VALUES("1236","172","29","0","2019-10-25");
INSERT INTO open_close_tb VALUES("1237","501","11","0","2019-10-25");
INSERT INTO open_close_tb VALUES("1238","767","2391","2390","2019-10-27");
INSERT INTO open_close_tb VALUES("1239","767","2388","0","2019-10-28");
INSERT INTO open_close_tb VALUES("1240","371","649","0","2019-10-28");
INSERT INTO open_close_tb VALUES("1241","779","323","0","2019-10-28");
INSERT INTO open_close_tb VALUES("1242","307","10","5","2019-10-28");



CREATE TABLE `part_payments_tb` (
  `payment_id` int(12) NOT NULL AUTO_INCREMENT,
  `amount` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_no` int(11) NOT NULL,
  `user_id` int(12) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `payment` decimal(10,2) NOT NULL,
  `payment_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `payment_for` date NOT NULL,
  `due` decimal(10,2) NOT NULL,
  `interest` decimal(10,2) NOT NULL,
  `remaining` decimal(10,2) NOT NULL,
  `status` varchar(20) NOT NULL,
  `rebate` decimal(10,2) NOT NULL,
  `or_no` int(11) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

INSERT INTO payment VALUES("1","0","1","104.50","2019-10-05 19:56:40","8","1","2019-10-05","104.50","0.00","0.00","paid","0.00","1901");
INSERT INTO payment VALUES("2","0","2","45.00","2019-10-05 21:46:16","8","1","2019-10-05","45.00","0.00","0.00","paid","0.00","1902");
INSERT INTO payment VALUES("3","0","3","15.50","2019-10-05 22:05:56","8","1","2019-10-05","15.50","0.00","0.00","paid","0.00","1903");
INSERT INTO payment VALUES("4","0","4","36.00","2019-10-05 22:06:56","8","1","2019-10-05","36.00","0.00","0.00","paid","0.00","1904");
INSERT INTO payment VALUES("5","0","5","30.00","2019-10-05 22:08:50","8","1","2019-10-05","30.00","0.00","0.00","paid","0.00","1905");
INSERT INTO payment VALUES("6","0","6","206.00","2019-10-05 22:09:30","8","1","2019-10-05","206.00","0.00","0.00","paid","0.00","1906");
INSERT INTO payment VALUES("7","0","7","30.00","2019-10-05 22:28:31","8","1","2019-10-05","30.00","0.00","0.00","paid","0.00","1907");
INSERT INTO payment VALUES("8","0","8","96.00","2019-10-05 22:48:50","8","1","2019-10-05","96.00","0.00","0.00","paid","0.00","1908");
INSERT INTO payment VALUES("9","0","9","110.50","2019-10-06 00:02:58","8","1","2019-10-06","110.50","0.00","0.00","paid","0.00","1909");
INSERT INTO payment VALUES("10","0","10","36.20","2019-10-06 13:54:39","8","1","2019-10-06","36.20","0.00","0.00","paid","0.00","1910");
INSERT INTO payment VALUES("11","0","11","66.50","2019-10-08 08:13:04","8","1","2019-10-08","66.50","0.00","0.00","paid","0.00","1911");
INSERT INTO payment VALUES("12","0","12","72.00","2019-10-08 08:13:18","8","1","2019-10-08","72.00","0.00","0.00","paid","0.00","1912");
INSERT INTO payment VALUES("13","0","13","11.85","2019-10-09 12:13:34","8","1","2019-10-09","11.85","0.00","0.00","paid","0.00","1913");
INSERT INTO payment VALUES("14","0","14","0.50","2019-10-09 12:24:25","8","1","2019-10-09","0.50","0.00","0.00","paid","0.00","1914");
INSERT INTO payment VALUES("15","0","15","30.00","2019-10-09 12:28:46","8","1","2019-10-09","30.00","0.00","0.00","paid","0.00","1915");
INSERT INTO payment VALUES("16","0","16","61.00","2019-10-09 12:30:58","8","1","2019-10-09","61.00","0.00","0.00","paid","0.00","1916");
INSERT INTO payment VALUES("17","0","17","160.00","2019-10-09 12:32:25","8","1","2019-10-09","160.00","0.00","0.00","paid","0.00","1917");
INSERT INTO payment VALUES("18","0","18","232.50","2019-10-09 12:48:24","8","1","2019-10-09","232.50","0.00","0.00","paid","0.00","1918");
INSERT INTO payment VALUES("19","0","19","75.00","2019-10-13 16:03:42","8","1","2019-10-13","75.00","0.00","0.00","paid","0.00","1919");
INSERT INTO payment VALUES("20","0","20","29.50","2019-10-13 16:08:45","8","1","2019-10-13","29.50","0.00","0.00","paid","0.00","1920");
INSERT INTO payment VALUES("21","0","21","36.00","2019-10-13 16:11:22","8","1","2019-10-13","36.00","0.00","0.00","paid","0.00","1921");
INSERT INTO payment VALUES("22","0","22","38.00","2019-10-18 08:13:14","8","1","2019-10-18","38.00","0.00","0.00","paid","0.00","1922");
INSERT INTO payment VALUES("23","0","23","90.00","2019-10-25 19:20:15","8","1","2019-10-25","90.00","0.00","0.00","paid","0.00","1923");
INSERT INTO payment VALUES("24","0","24","26.00","2019-10-25 19:21:22","8","1","2019-10-25","26.00","0.00","0.00","paid","0.00","1924");
INSERT INTO payment VALUES("25","0","25","15.00","2019-10-27 20:02:08","8","1","2019-10-27","15.00","0.00","0.00","paid","0.00","1925");
INSERT INTO payment VALUES("26","0","26","30.00","2019-10-27 20:03:14","8","1","2019-10-27","30.00","0.00","0.00","paid","0.00","1926");
INSERT INTO payment VALUES("27","0","27","105.00","2019-10-28 08:05:14","8","1","2019-10-28","105.00","0.00","0.00","paid","0.00","1927");
INSERT INTO payment VALUES("28","0","28","15.50","2019-10-28 13:56:29","8","1","2019-10-28","15.50","0.00","0.00","paid","0.00","1928");
INSERT INTO payment VALUES("29","0","29","9.00","2019-10-28 13:57:12","8","1","2019-10-28","9.00","0.00","0.00","paid","0.00","1929");



CREATE TABLE `product` (
  `prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_name` varchar(100) NOT NULL,
  `prod_desc` varchar(500) NOT NULL,
  `prod_price` decimal(10,2) NOT NULL,
  `prod_sell_price` text NOT NULL,
  `prod_pic` varchar(300) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `prod_qty` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `reorder` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `serial` varchar(50) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `belongs_to` int(12) NOT NULL,
  `stock_branch_id` int(12) NOT NULL,
  `barcode` text NOT NULL,
  `manufactor_date` text NOT NULL,
  `expire_date` text NOT NULL,
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=895 DEFAULT CHARSET=latin1;

INSERT INTO product VALUES("1","DIAMOND RICH BAKE BREAD","DIAMOND RICH BAKE BREAD","5.00","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","786","","");
INSERT INTO product VALUES("2","BREAD CITY BREAD EACH","BREAD CITY BREAD EACH","6.50","8","","68","8","1","0","21","","2019-10-09 12:30:59","8","5","6009803678496","","");
INSERT INTO product VALUES("3","ROUND MILK BREAD","ROUND MILK BREAD","6.50","8","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803678816","","");
INSERT INTO product VALUES("4","DIAMOND RICH BAKE FAMILY BREAD","DIAMOND RICH BAKE FAMILY BREAD","6.00","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","785","","");
INSERT INTO product VALUES("5","ZAMBIKA FAMILY BREAD WHITE","ZAMBIKA FAMILY BREAD WHITE","7.00","9","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652781910","","");
INSERT INTO product VALUES("6","BONFWA  BREAD","BONFWA  BREAD","6.50","8.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009697001493","","");
INSERT INTO product VALUES("7","AGOGOS ORIGINAL WHITE BREAD","AGOGOS ORIGINAL WHITE BREAD","7.50","8.5","","68","10","1","0","18","","2019-10-05 23:47:32","6","5","6009630400505","","");
INSERT INTO product VALUES("8","AGOGO AMAMAS BRED 700G","AGOGO AMAMAS BRED 700G","7.50","8.5","","68","10","1","0","18","","2019-10-05 23:47:32","6","5","6009803291022","","");
INSERT INTO product VALUES("9","NUTRIBAKE WHITE BREAD","NUTRIBAKE WHITE BREAD","6.50","8.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","100","","");
INSERT INTO product VALUES("10","AGOGOS BREAD ROLLS 10S","AGOGOS BREAD ROLLS 10S","8.50","9.5","","68","10","1","0","18","","2019-10-05 23:47:32","6","5","6009803291299","","");
INSERT INTO product VALUES("11","AGOGOS ROUND LOAF 600G","AGOGOS ROUND LOAF 600G","7.50","8.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803291237","","");
INSERT INTO product VALUES("12","FRESH KAPENTA","FRESH KAPENTA","7.00","12","","66","10","1","0","21","","2019-10-05 23:47:32","6","5","451","","");
INSERT INTO product VALUES("13","NCHENGA","NCHENGA","7.00","15","","66","10","1","0","21","","2019-10-05 23:47:32","6","5","452","","");
INSERT INTO product VALUES("14","OX HEAD","OX HEAD","22.00","50","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","1000","","");
INSERT INTO product VALUES("15","OX TONGUE","OX TONGUE","22.00","35","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001110000000","","");
INSERT INTO product VALUES("16","STEAK ON BONE","STEAK ON BONE","22.00","45","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001270000000","","");
INSERT INTO product VALUES("17","BOEREWORS","BOEREWORS","22.00","44","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001220000000","","");
INSERT INTO product VALUES("18","TOP SIDE","TOP SIDE","22.00","45","","63","9","1","0","20","","2019-10-09 12:30:59","6","5","2001070000000","","");
INSERT INTO product VALUES("19","BEEF BONES","BEEF BONES","22.00","10","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001260000000","","");
INSERT INTO product VALUES("20","T BONE","T BONE","22.00","42","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001090000000","","");
INSERT INTO product VALUES("21","BRISKET","BRISKET","22.00","39.5","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001010000000","","");
INSERT INTO product VALUES("22","OX HOOVES","OX HOOVES","10.00","18","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001160000000","","");
INSERT INTO product VALUES("23","BEEF OFFALS","BEEF OFFALS","10.00","20","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001140000000","","");
INSERT INTO product VALUES("24","BEEF MINCE","BEEF MINCE","22.00","39.5","","63","9","1","0","20","","2019-10-06 13:54:39","6","5","2001200000000","","");
INSERT INTO product VALUES("25","OX KIDNEY","OX KIDNEY","22.00","55","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001130000000","","");
INSERT INTO product VALUES("26","BEEF SAUSAGE","BEEF SAUSAGE","22.00","44","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001210000000","","");
INSERT INTO product VALUES("27","OX TAIL","OX TAIL","22.00","40","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001150000000","","");
INSERT INTO product VALUES("28","BOTTLE FISH","BOTTLE FISH","22.00","33","","66","10","1","0","21","","2019-10-05 23:47:32","6","5","2001250000000","","");
INSERT INTO product VALUES("29","MIXED CUT","MIXED CUT","22.00","36","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2010000000000","","");
INSERT INTO product VALUES("30","RUMP STEAK","RUMP STEAK","22.00","40","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001080000000","","");
INSERT INTO product VALUES("31","T BONE ORDER","T BONE ORDER","22.00","55","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001350000000","","");
INSERT INTO product VALUES("32","MIXED CUT","MIXED CUT","22.00","34.5","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001000000000","","");
INSERT INTO product VALUES("33","SHOULDER","SHOULDER","22.00","39.5","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001030000000","","");
INSERT INTO product VALUES("34","SHIN","SHIN","22.00","39.5","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001040000000","","");
INSERT INTO product VALUES("35","OX LIVER","OX LIVER","22.00","40","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001120000000","","");
INSERT INTO product VALUES("36","COOKED HAM","COOKED HAM","22.00","34.5","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001330000000","","");
INSERT INTO product VALUES("37","BILTON","BILTON","54.75","170","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001410000000","","");
INSERT INTO product VALUES("38","BEEF RIBS","BEEF RIBS","22.00","40","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001100000000","","");
INSERT INTO product VALUES("39","SPECIAL MINCE TOPSIDE","SPECIAL MINCE TOPSIDE","22.00","49.5","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","200130000000","","");
INSERT INTO product VALUES("40","BRAIIWORS","BRAIIWORS","22.00","29.5","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2000400813048","","");
INSERT INTO product VALUES("41","BUKA FISH","BUKA FISH","22.00","40","","66","10","1","0","21","","2019-10-05 23:47:32","6","5","2001340000000","","");
INSERT INTO product VALUES("42","BEEF FILLET","BEEF FILLET","22.00","40","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001180000000","","");
INSERT INTO product VALUES("43","BEEF FAT","BEEF FAT","22.00","12","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001290000000","","");
INSERT INTO product VALUES("44","SAW DUST","SAW DUST","22.00","10","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001280000000","","");
INSERT INTO product VALUES("45","OX HEART","OX HEART","22.00","40","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001170000000","","");
INSERT INTO product VALUES("46","BRAIIWORS","BRAIIWORS","22.00","29.5","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2004000000000","","");
INSERT INTO product VALUES("47","BEEF B/FAS SAUSAGE","BEEF B/FAS SAUSAGE","22.00","44","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001240000000","","");
INSERT INTO product VALUES("48","CHUCK","CHUCK","22.00","39.5","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001020000000","","");
INSERT INTO product VALUES("49","BEEF DRY WORS","BEEF DRY WORS","54.75","160","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2014800000000","","");
INSERT INTO product VALUES("50","MIXED PORTIONS","MIXED PORTIONS","14.50","30","","65","10","1","0","20","","2019-10-05 23:47:32","6","5","2003230000000","","");
INSERT INTO product VALUES("51","WHOLE CHICKEN","WHOLE CHICKEN","22.00","30","","65","10","1","0","20","","2019-10-05 23:47:32","6","5","2003000000000","","");
INSERT INTO product VALUES("52","CHICKEN THIGH","CHICKEN THIGH","14.50","30","","65","10","1","0","20","","2019-10-05 23:47:32","6","5","2003040000000","","");
INSERT INTO product VALUES("53","SMOKED CHICKEN ","SMOKED CHICKEN ","25.00","50","","65","10","1","0","20","","2019-10-05 23:47:32","6","5","2003270000000","","");
INSERT INTO product VALUES("54","CHICKEN BREASTS","CHICKEN BREASTS","14.50","30","","65","10","1","0","20","","2019-10-05 23:47:32","6","5","2003030000000","","");
INSERT INTO product VALUES("55","CHICKEN NECK","CHICKEN NECK","12.00","26","","65","10","1","0","20","","2019-10-05 23:47:32","6","5","2003190000000","","");
INSERT INTO product VALUES("56","CHICKEN LIVER","CHICKEN LIVER","5.00","25","","65","10","1","0","20","","2019-10-05 23:47:32","6","5","2003180000000","","");
INSERT INTO product VALUES("57","CHICKEN SHANKS","CHICKEN SHANKS","11.88","16.97","","65","10","1","0","20","","2019-10-05 23:47:32","6","5","6009607610470","","");
INSERT INTO product VALUES("58","FRENCH QUAIL 550G","FRENCH QUAIL 550G","32.00","45","","65","10","1","0","12","","2019-10-05 23:47:32","6","5","6009670161619","","");
INSERT INTO product VALUES("59","HORSE MACKREL","HORSE MACKREL","10.00","24","","66","10","1","0","21","","2019-10-05 23:47:32","6","5","2004070000000","","");
INSERT INTO product VALUES("60","CHICKEN DRUMSTICK","CHICKEN DRUMSTICK","14.50","30","","65","10","1","0","20","","2019-10-05 23:47:32","6","5","2003010000000","","");
INSERT INTO product VALUES("61","CHICKEN GIZARDS","CHICKEN GIZARDS","12.00","30","","65","9","1","0","20","","2019-10-09 12:28:47","6","5","2003070000000","","");
INSERT INTO product VALUES("62","CHICKEN WINGS ","CHICKEN WINGS ","14.50","29","","65","10","1","0","20","","2019-10-05 23:47:32","6","5","2003020000000","","");
INSERT INTO product VALUES("63","CHICKEN BACKS","CHICKEN BACKS","14.50","30","","65","10","1","0","20","","2019-10-05 23:47:32","6","5","2003060000000","","");
INSERT INTO product VALUES("64","CHICKEN SHANKS","CHICKEN SHANKS","17.50","25","","65","10","1","0","20","","2019-10-05 23:47:32","6","5","2003050000000","","");
INSERT INTO product VALUES("65","GOAT HEAD","GOAT HEAD","7.00","10","","67","10","1","0","21","","2019-10-05 23:47:32","6","5","2225","","");
INSERT INTO product VALUES("66","GOAT OFFAL","GOAT OFFAL","22.00","35","","67","10","1","0","20","","2019-10-05 23:47:32","6","5","2002150000000","","");
INSERT INTO product VALUES("67","GOAT LIVER","GOAT LIVER","22.00","39","","67","10","1","0","20","","2019-10-05 23:47:32","6","5","2002210000000","","");
INSERT INTO product VALUES("68","GOAT MEAT","GOAT MEAT","22.00","39","","67","10","1","0","20","","2019-10-05 23:47:32","6","5","2002140000000","","");
INSERT INTO product VALUES("69","MUTTON","MUTTON","22.00","40","","71","10","1","0","20","","2019-10-05 23:47:32","6","5","2002490000000","","");
INSERT INTO product VALUES("70","PORK HEAD","PORK HEAD","22.00","25","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","1113","","");
INSERT INTO product VALUES("71","SMOKED PORK RIBS","SMOKED PORK RIBS","22.00","55","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002000000000","","");
INSERT INTO product VALUES("72","PORK LIVER","PORK LIVER","22.00","10","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002200000000","","");
INSERT INTO product VALUES("73","BEEF HUNGARIAN","BEEF HUNGARIAN","22.00","29.5","","70","10","1","0","20","","2019-10-05 23:47:32","6","5","2001230000000","","");
INSERT INTO product VALUES("74","PORK SKIN","PORK SKIN","10.00","10","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001990000000","","");
INSERT INTO product VALUES("75","PORK SAUSAGE","PORK SAUSAGE","22.00","50","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002080000000","","");
INSERT INTO product VALUES("76","PORK TROTTERS","PORK TROTTERS","22.00","18","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002040000000","","");
INSERT INTO product VALUES("77","FRENCH POLONY","FRENCH POLONY","22.00","55","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002180000000","","");
INSERT INTO product VALUES("78","GARLIC POLONY","GARLIC POLONY","22.00","55","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002190000000","","");
INSERT INTO product VALUES("79","PORK NECK","PORK NECK","22.00","35","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002030000000","","");
INSERT INTO product VALUES("80","PORK BONES","PORK BONES","16.00","25","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002220000000","","");
INSERT INTO product VALUES("81","PORK LEG CHOPS","PORK LEG CHOPS","22.00","35","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002060000000","","");
INSERT INTO product VALUES("82","PORK SHOULDER","PORK SHOULDER","22.00","35","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002020000000","","");
INSERT INTO product VALUES("83","PORK CHOPS","PORK CHOPS","22.00","38","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002010000000","","");
INSERT INTO product VALUES("84","PORK MINCE","PORK MINCE","22.00","40","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002120000000","","");
INSERT INTO product VALUES("85","PORK HUNGARIAN","PORK HUNGARIAN","22.00","48","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2014000000000","","");
INSERT INTO product VALUES("86","BACON","BACON","22.00","65","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001320000000","","");
INSERT INTO product VALUES("87","PORK FAT","PORK FAT","8.00","10","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2001370000000","","");
INSERT INTO product VALUES("88","PORK HEAD SMALL","PORK HEAD SMALL","22.00","25","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","1111","","");
INSERT INTO product VALUES("89","SMOKED HUNGARIAN","SMOKED HUNGARIAN","22.00","35","","70","10","1","0","20","","2019-10-05 23:47:32","6","5","2002070000000","","");
INSERT INTO product VALUES("90","PORK SHANKS","PORK SHANKS","22.00","25","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002050000000","","");
INSERT INTO product VALUES("91","PORK  LEG","PORK  LEG","22.00","39.5","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002090000000","","");
INSERT INTO product VALUES("92","PORK STEW","PORK STEW","22.00","30","","63","10","1","0","20","","2019-10-05 23:47:32","6","5","2002170000000","","");
INSERT INTO product VALUES("93","HALF CHICKEN","HALF CHICKEN","22.00","30","","65","10","1","0","20","","2019-10-05 23:47:32","6","5","2001050000000","","");
INSERT INTO product VALUES("94","VILLAGE CHICKEN ","VILLAGE CHICKEN ","20.00","40","","65","10","1","0","20","","2019-10-05 23:47:32","6","5","2003120000000","","");
INSERT INTO product VALUES("95","BUKA FISH","BUKA FISH","35.00","40","","66","10","1","0","14","","2019-10-05 23:47:32","6","5","2004010000000","","");
INSERT INTO product VALUES("96","BREAM","BREAM","26.50","36.5","","66","10","1","0","14","","2019-10-05 23:47:32","6","5","2003150000000","","");
INSERT INTO product VALUES("97","BEEF BBQ SAUSAGE","BEEF BBQ SAUSAGE","22.00","29.5","","70","10","1","0","20","","2019-10-05 23:47:32","6","5","2001470000000","","");
INSERT INTO product VALUES("98","VIENNA SAUSAGE P/KG","VIENNA SAUSAGE P/KG","22.00","50","","70","10","1","0","20","","2019-10-05 23:47:32","6","5","2001310000000","","");
INSERT INTO product VALUES("99","SPECIAL HUNGARIAN","SPECIAL HUNGARIAN","14.50","29.5","","70","10","1","0","20","","2019-10-05 23:47:32","6","5","2002110000000","","");
INSERT INTO product VALUES("100","SPECIAL HUNGARIAN","SPECIAL HUNGARIAN","2.00","4","","70","9","1","0","20","","2019-10-05 23:47:32","6","5","6112","","");
INSERT INTO product VALUES("101","FRESH KAPENTA","FRESH KAPENTA","30.00","48","","66","10","1","0","21","","2019-10-05 23:47:32","6","5","2001360000000","","");
INSERT INTO product VALUES("102","CHAKALAKA SAUSAGE","CHAKALAKA SAUSAGE","22.00","44","","70","10","1","0","20","","2019-10-05 23:47:32","6","5","2001460000000","","");
INSERT INTO product VALUES("103","MIRINDA BOTTLED DRINK 250ML","MIRINDA BOTTLED DRINK 250ML","1.50","2","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","123","","");
INSERT INTO product VALUES("104","APPLE MAX 350ML","APPLE MAX 350ML","2.96","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652782931","","");
INSERT INTO product VALUES("105","TINGLING GINGER BEER350ML","TINGLING GINGER BEER350ML","2.96","5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009652782900","","");
INSERT INTO product VALUES("106","MIRINDA ORANGE  250ML","MIRINDA ORANGE  250ML","1.58","2","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","321","","");
INSERT INTO product VALUES("107","SPRITE 500ML","SPRITE 500ML","4.75","8","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","54491069","","");
INSERT INTO product VALUES("108","MOUNTAIN DEW 600ML","MOUNTAIN DEW 600ML","5.00","7","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803227489","","");
INSERT INTO product VALUES("109","MIRINDA FRUITY FLAV 2L","MIRINDA FRUITY FLAV 2L","10.00","14.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009681260929","","");
INSERT INTO product VALUES("110","PEPSI 2L","PEPSI 2L","14.67","17","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009681260998","","");
INSERT INTO product VALUES("111","GRAPE MAX ALOE VERA 350ML","GRAPE MAX ALOE VERA 350ML","2.90","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652782993","","");
INSERT INTO product VALUES("112","FANTA 2LT","FANTA 2LT","14.67","19.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","5449000010049","","");
INSERT INTO product VALUES("113","MIRINDA ORA 600ML","MIRINDA ORA 600ML","5.00","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009880793358","","");
INSERT INTO product VALUES("114","COOLSPLASH CREAM SODA 2L","COOLSPLASH CREAM SODA 2L","13.33","19","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001651042030","","");
INSERT INTO product VALUES("115","MIRINDA 7UP 600ML","MIRINDA 7UP 600ML","5.00","7","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009681261254","","");
INSERT INTO product VALUES("116","COCA COLA 1L","COCA COLA 1L","8.00","12","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","5449000054227","","");
INSERT INTO product VALUES("117","YESS GINGER BEER DRINK 350ML","YESS GINGER BEER DRINK 350ML","1.66","2.5","","68","9","1","0","21","","2019-10-05 23:47:32","6","5","606110287753","","");
INSERT INTO product VALUES("118","MIRINDA ORANGE 600ML","MIRINDA ORANGE 600ML","5.00","7","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009681261285","","");
INSERT INTO product VALUES("119","COCA COLA LIGHT 500ML","COCA COLA LIGHT 500ML","4.75","7","","68","10","1","0","17","","2019-10-05 23:47:32","6","5","54492387","","");
INSERT INTO product VALUES("120","MOUNTAIN DEW 350ML","MOUNTAIN DEW 350ML","2.50","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803227441","","");
INSERT INTO product VALUES("121","THIRSTY ORANGE SQUASH 2L","THIRSTY ORANGE SQUASH 2L","15.67","21.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009650800156","","");
INSERT INTO product VALUES("122","COCA COLA 2LT","COCA COLA 2LT","14.67","19.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","5449000000286","","");
INSERT INTO product VALUES("123","YESS ENERGY 350ml","YESS ENERGY 350ml","2.16","3.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","606110287968","","");
INSERT INTO product VALUES("124","MIRINDA GREEN APPLE 330ML","MIRINDA GREEN APPLE 330ML","3.17","4","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803227540","","");
INSERT INTO product VALUES("125","SPRITE 2LT","SPRITE 2LT","14.67","19.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","5449000010117","","");
INSERT INTO product VALUES("126","MIRINDA FRUIT 330L","MIRINDA FRUIT 330L","3.50","5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009681267157","","");
INSERT INTO product VALUES("127","COCA COLA 500ML","COCA COLA 500ML","4.75","8","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","54491472","","");
INSERT INTO product VALUES("128","FANTA ORANGE 500ML","FANTA ORANGE 500ML","4.83","8","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","40822938","","");
INSERT INTO product VALUES("129","RED ROCK COLA 500ML","RED ROCK COLA 500ML","3.08","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644991266","","");
INSERT INTO product VALUES("130","PEPSI 330ML","PEPSI 330ML","3.17","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009681267645","","");
INSERT INTO product VALUES("131","SPRITE 1L","SPRITE 1L","8.00","12","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","5449000050939","","");
INSERT INTO product VALUES("132","MOUNTAIN DEW 2L","MOUNTAIN DEW 2L","14.66","21","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803227502","","");
INSERT INTO product VALUES("133","330 ml","330 ml","2.75","4","","68","8","1","0","19","","2019-10-13 16:08:46","6","5","6009681260981","","");
INSERT INTO product VALUES("134","COKE  ZERO 500ML","COKE  ZERO 500ML","4.75","7","","68","10","1","0","17","","2019-10-05 23:47:32","6","5","5449000131836","","");
INSERT INTO product VALUES("135","TWIST LEMON FLAV 500ML","TWIST LEMON FLAV 500ML","4.75","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","40822747","","");
INSERT INTO product VALUES("136","YES 350ML GING","YES 350ML GING","1.66","2.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","606110287906","","");
INSERT INTO product VALUES("137","YES DRINK 350 C/S","YES DRINK 350 C/S","1.54","2.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","606110287760","","");
INSERT INTO product VALUES("138","MIRINDA ORANGE FLAV 2L","MIRINDA ORANGE FLAV 2L","10.00","14.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009681260080","","");
INSERT INTO product VALUES("139","MIRINDA PINEAPPLE 2L","MIRINDA PINEAPPLE 2L","14.66","17","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009681261124","","");
INSERT INTO product VALUES("140","COCACOLA350ML","COCACOLA350ML","3.54","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","42117131","","");
INSERT INTO product VALUES("141","TANGO PINA P/APPLE 425ML","TANGO PINA P/APPLE 425ML","3.08","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644922949","","");
INSERT INTO product VALUES("142","PEPSI LIGHT 600ML","PEPSI LIGHT 600ML","5.00","7","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009681261148","","");
INSERT INTO product VALUES("143","MIRINDA 600ML","MIRINDA 600ML","5.00","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009880793365","","");
INSERT INTO product VALUES("144","MIRINDA FRUITY FLAV 600ML","MIRINDA FRUITY FLAV 600ML","5.00","7","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009681261209","","");
INSERT INTO product VALUES("145","SPAR LETTA 500ml","SPAR LETTA 500ml","4.75","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","5449000120311","","");
INSERT INTO product VALUES("146","7UP 600ML","7UP 600ML","5.00","7","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009880793402","","");
INSERT INTO product VALUES("147","SPRITE 350ML","SPRITE 350ML","3.54","5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","40822099","","");
INSERT INTO product VALUES("148","FANTA 350ML","FANTA 350ML","3.54","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","90377242","","");
INSERT INTO product VALUES("149","MIRINDA P/APPLE 330ML","MIRINDA P/APPLE 330ML","3.17","3.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009681261483","","");
INSERT INTO product VALUES("150","PEPSI 600ML","PEPSI 600ML","5.00","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009880793334","","");
INSERT INTO product VALUES("151","SANTAL ORANGE FRUIT BLEND 1L","SANTAL ORANGE FRUIT BLEND 1L","18.00","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683613501","","");
INSERT INTO product VALUES("152","MIRINDA ORANGE 330ML","MIRINDA ORANGE 330ML","3.17","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009681267140","","");
INSERT INTO product VALUES("153","HUBBLY BUBBLY 350ML","HUBBLY BUBBLY 350ML","2.60","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652782955","","");
INSERT INTO product VALUES("154","MIRINDA PINEAPPLE 600ML","MIRINDA PINEAPPLE 600ML","4.75","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009681260011","","");
INSERT INTO product VALUES("155","SUPER SHAKE MANGO 300ML ","SUPER SHAKE MANGO 300ML ","1.60","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009823800556","","");
INSERT INTO product VALUES("156","SUPER SHAKE PINE. FLAV 300ML","SUPER SHAKE PINE. FLAV 300ML","1.60","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644923861","","");
INSERT INTO product VALUES("157","REACTOR 500ML","REACTOR 500ML","4.00","8","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009801423449","","");
INSERT INTO product VALUES("158","COOLSPLASH ORANGE CRUSH 5L","COOLSPLASH ORANGE CRUSH 5L","31.75","42","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001651041996","","");
INSERT INTO product VALUES("159","MIRRINDA ORANGE 500ML","MIRRINDA ORANGE 500ML","4.00","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","111111","","");
INSERT INTO product VALUES("160","REAKTOR E/DRINK CAN 330ML","REAKTOR E/DRINK CAN 330ML","3.48","6","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009801423562","","");
INSERT INTO product VALUES("161","SANTA ORANGE 500ML","SANTA ORANGE 500ML","9.00","12","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683613075","","");
INSERT INTO product VALUES("162","MILKIT STRAW. FLAV. 500ML","MILKIT STRAW. FLAV. 500ML","6.58","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644997169","","");
INSERT INTO product VALUES("163","FROOTY 350ML","FROOTY 350ML","2.96","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652782962","","");
INSERT INTO product VALUES("164","VOCANO DRINK 330ML","VOCANO DRINK 330ML","2.20","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009801423715","","");
INSERT INTO product VALUES("165","KUNG FU 500ML","KUNG FU 500ML","5.42","8","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009537015956","","");
INSERT INTO product VALUES("166","SUN DANCE ORANGE 500ML","SUN DANCE ORANGE 500ML","3.00","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644991280","","");
INSERT INTO product VALUES("167","KUNG FU ENERGY CANNED 330ML","KUNG FU ENERGY CANNED 330ML","4.21","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644992089","","");
INSERT INTO product VALUES("168","SUN DANCE PINEAPPLE 500ML","SUN DANCE PINEAPPLE 500ML","3.00","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644991624","","");
INSERT INTO product VALUES("169","APPY PINE500ML","APPY PINE500ML","3.20","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644993284","","");
INSERT INTO product VALUES("170","WILD CAT ENERGY 330ML","WILD CAT ENERGY 330ML","2.80","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644991204","","");
INSERT INTO product VALUES("171","DENDAIRY SHAKE/CHOC/MILK 200ML","DENDAIRY SHAKE/CHOC/MILK 200ML","2.29","3.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009698811985","","");
INSERT INTO product VALUES("172"," POWER ENERGY DRINK 250ML"," POWER ENERGY DRINK 250ML","7.00","10.5","","68","28","1","0","21","","2019-10-25 19:21:22","6","5","6161109780416","","");
INSERT INTO product VALUES("173","COOLSPLASH CRAEM SODA 5L","COOLSPLASH CRAEM SODA 5L","31.75","42","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001651041989","","");
INSERT INTO product VALUES("174","SANTAL APPLE FRUIT BLEND  1L","SANTAL APPLE FRUIT BLEND  1L","18.00","24","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683613532","","");
INSERT INTO product VALUES("175","PARMALAT VANILLA CUSTARD 1L","PARMALAT VANILLA CUSTARD 1L","27.00","35.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001049062329","","");
INSERT INTO product VALUES("176","SANTAL APPLE FRUIT BLEND 250ML","SANTAL APPLE FRUIT BLEND 250ML","5.00","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683613235","","");
INSERT INTO product VALUES("177","SANTAL E/ F/ BLEND 250ML","SANTAL E/ F/ BLEND 250ML","5.00","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683613228","","");
INSERT INTO product VALUES("178","SANTA APPLE 500ML","SANTA APPLE 500ML","9.00","12","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683613105","","");
INSERT INTO product VALUES("179","SANTAL BOISSON 500ML","SANTAL BOISSON 500ML","9.00","12","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683613099","","");
INSERT INTO product VALUES("180","DRAGON ENERGY 500ML","DRAGON ENERGY 500ML","6.25","9","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009694633116","","");
INSERT INTO product VALUES("181","DENDAIRY SHAKE/CHOC/MILK 100ML","DENDAIRY SHAKE/CHOC/MILK 100ML","1.43","2","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009698812364","","");
INSERT INTO product VALUES("182","TWIST GRANAD FLAV 500ML","TWIST GRANAD FLAV 500ML","4.75","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","87303339","","");
INSERT INTO product VALUES("183","SWIFT DRINK 350ML","SWIFT DRINK 350ML","2.92","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652782948","","");
INSERT INTO product VALUES("184","FANTA GRAPE 500ML","FANTA GRAPE 500ML","4.75","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","87126815","","");
INSERT INTO product VALUES("185","THUNDERLIGHT E/DRINK 330ML","THUNDERLIGHT E/DRINK 330ML","2.58","4","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009880335671","","");
INSERT INTO product VALUES("186","VOLCANO E/DRINK CAN 500ML","VOLCANO E/DRINK CAN 500ML","3.67","5.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009801423722","","");
INSERT INTO product VALUES("187","CABANA TROPICA 2L","CABANA TROPICA 2L","20.85","28","","68","10","1","0","15","","2019-10-05 23:47:32","6","5","6009683612078","","");
INSERT INTO product VALUES("188","REACTOR330ML","REACTOR330ML","2.08","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009801423340","","");
INSERT INTO product VALUES("189","SUN DANCE TROPICAL 500ML","SUN DANCE TROPICAL 500ML","3.00","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644991297","","");
INSERT INTO product VALUES("190","SUN DANCE GUAVA 500ML","SUN DANCE GUAVA 500ML","3.00","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644995745","","");
INSERT INTO product VALUES("191","RED BULL 250ML","RED BULL 250ML","11.25","15","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","9002490100070","","");
INSERT INTO product VALUES("192","PURE JOY GUAVA 1L","PURE JOY GUAVA 1L","19.00","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683613419","","");
INSERT INTO product VALUES("193","COOLSPLASH BLACK SYRUP2L","COOLSPLASH BLACK SYRUP2L","13.67","19","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001651042016","","");
INSERT INTO product VALUES("194","JUST PINEAPPLE 2L","JUST PINEAPPLE 2L","17.17","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652782566","","");
INSERT INTO product VALUES("195","WILD CAT ENERGY 500ML","WILD CAT ENERGY 500ML","3.92","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644991198","","");
INSERT INTO product VALUES("196","GO FRESH FRUIT COCKTAIL 1.5L","GO FRESH FRUIT COCKTAIL 1.5L","13.67","18.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644925261","","");
INSERT INTO product VALUES("197","A/APPLE 500ML","A/APPLE 500ML","3.21","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644991778","","");
INSERT INTO product VALUES("198","APPY GRAPO 500ML","APPY GRAPO 500ML","3.21","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644996995","","");
INSERT INTO product VALUES("199","PURE JOY MANGO 1L","PURE JOY MANGO 1L","19.00","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683613433","","");
INSERT INTO product VALUES("200","MIRINDA 7UP 330ML","MIRINDA 7UP 330ML","3.17","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009681267102","","");
INSERT INTO product VALUES("201","JUST ORANGE 1L","JUST ORANGE 1L","9.33","14","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652782658","","");
INSERT INTO product VALUES("202","GLASS CERES DRINK","GLASS CERES DRINK","0.00","11","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240222676","","");
INSERT INTO product VALUES("203","JUST GRANADILA 1LTR","JUST GRANADILA 1LTR","9.33","15","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652782672","","");
INSERT INTO product VALUES("204","PURE JOY BERRY NICE 1L","PURE JOY BERRY NICE 1L","19.00","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001049052702","","");
INSERT INTO product VALUES("205","CERES MANGO 1LT","CERES MANGO 1LT","10.00","16.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001240100257","","");
INSERT INTO product VALUES("206","CERES PINEAPPLE 1L","CERES PINEAPPLE 1L","11.00","16.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240100066","","");
INSERT INTO product VALUES("207","CERES ORANGE 1L","CERES ORANGE 1L","11.00","16.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240100059","","");
INSERT INTO product VALUES("208","SANTAL ORANGE NECTOR 250ML","SANTAL ORANGE NECTOR 250ML","4.50","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683613204","","");
INSERT INTO product VALUES("209","DENDAIRY RED/G/JUICE 500ML","DENDAIRY RED/G/JUICE 500ML","7.48","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009698812074","","");
INSERT INTO product VALUES("210","CERES  MANAGO 200ML","CERES  MANAGO 200ML","2.70","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240200254","","");
INSERT INTO product VALUES("211","GO FRESH 1.5LT","GO FRESH 1.5LT","11.00","16","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","600964492547","","");
INSERT INTO product VALUES("212","FRUITREE ORANGE 350ML","FRUITREE ORANGE 350ML","7.00","10.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001240430057","","");
INSERT INTO product VALUES("213","DENDAIRY B/B/JUICE 1L","DENDAIRY B/B/JUICE 1L","13.69","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009698812838","","");
INSERT INTO product VALUES("214","DENDAIRY APPLE PURE JUICE 1L","DENDAIRY APPLE PURE JUICE 1L","13.69","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009698811572","","");
INSERT INTO product VALUES("215","SANTAL PEACH AND APRICOT 1L","SANTAL PEACH AND APRICOT 1L","18.00","24","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683613549","","");
INSERT INTO product VALUES("216","SMART JUICE S/PINEAPPLE 2L","SMART JUICE S/PINEAPPLE 2L","11.67","16","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537015369","","");
INSERT INTO product VALUES("217","DENDAIRY MANGO/ORANGE JUICE 1L","DENDAIRY MANGO/ORANGE JUICE 1L","13.69","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009698811565","","");
INSERT INTO product VALUES("218","CERES ORANGE 200ML","CERES ORANGE 200ML","4.00","6.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001240200056","","");
INSERT INTO product VALUES("219","FRUITREE GUAVA BANANA 350ML","FRUITREE GUAVA BANANA 350ML","6.67","10.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001240232323","","");
INSERT INTO product VALUES("220","CERES APPLE 1L","CERES APPLE 1L","14.08","17.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240100011","","");
INSERT INTO product VALUES("221","THIRSTY APPLE N RASB 2L","THIRSTY APPLE N RASB 2L","15.00","21.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009650800606","","");
INSERT INTO product VALUES("222","THIRSTY PEACH 2L","THIRSTY PEACH 2L","16.60","24","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009650800132","","");
INSERT INTO product VALUES("223","GO FRESH PEACH SQUASH 1.5L","GO FRESH PEACH SQUASH 1.5L","13.67","18.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644925254","","");
INSERT INTO product VALUES("224","YES DRINK ORANGE 350ML","YES DRINK ORANGE 350ML","1.66","2.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","606110287746","","");
INSERT INTO product VALUES("225","SMART JUICE S/ORANGE 2L","SMART JUICE S/ORANGE 2L","11.00","16","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537015352","","");
INSERT INTO product VALUES("226","DENDAIRY LITCHI.JUICE 1L","DENDAIRY LITCHI.JUICE 1L","13.69","13.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009698812845","","");
INSERT INTO product VALUES("227","GO FRESH PINEAPPLE  1.5L","GO FRESH PINEAPPLE  1.5L","13.67","18.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644925247","","");
INSERT INTO product VALUES("228","PURE JOY MANGO 500ML","PURE JOY MANGO 500ML","10.37","14","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683613068","","");
INSERT INTO product VALUES("229","FROOTY RED 1L","FROOTY RED 1L","8.33","15","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009652784843","","");
INSERT INTO product VALUES("230","CERES SECRETS OF VAL.200ML","CERES SECRETS OF VAL.200ML","4.60","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240200711","","");
INSERT INTO product VALUES("231","CERES RED GRAPE 275ML","CERES RED GRAPE 275ML","9.00","11.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240222737","","");
INSERT INTO product VALUES("232","PURE JOY APPLE 500ML","PURE JOY APPLE 500ML","10.37","14","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683613303","","");
INSERT INTO product VALUES("233","PURE JOY TROPICAL 500ML","PURE JOY TROPICAL 500ML","10.37","14","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683613051","","");
INSERT INTO product VALUES("234","CERES PINEAPPLE 200ML","CERES PINEAPPLE 200ML","5.69","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240225325","","");
INSERT INTO product VALUES("235","MINUTE MAID ORANGE 400ML","MINUTE MAID ORANGE 400ML","7.50","9.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","90399497","","");
INSERT INTO product VALUES("236","PURE JOY ORANGE 1L","PURE JOY ORANGE 1L","19.00","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683613402","","");
INSERT INTO product VALUES("237","PURE JOY PINEAPPLE 1LTR","PURE JOY PINEAPPLE 1LTR","19.00","25","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683613471","","");
INSERT INTO product VALUES("238","PURE JOY TROPICAL 1L","PURE JOY TROPICAL 1L","19.00","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683613426","","");
INSERT INTO product VALUES("239","CAB TROP 500ML","CAB TROP 500ML","6.05","8.5","","68","10","1","0","15","","2019-10-05 23:47:32","6","5","6009683612023","","");
INSERT INTO product VALUES("240","PARMALAT CABANA PEACH 2L","PARMALAT CABANA PEACH 2L","20.85","29","","68","10","1","0","15","","2019-10-05 23:47:32","6","5","6009683612115","","");
INSERT INTO product VALUES("241","PURE JOY APPLE 1LTR","PURE JOY APPLE 1LTR","19.00","25","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683613457","","");
INSERT INTO product VALUES("242","SMOOTH SIDE MANGO ORANGE 1L","SMOOTH SIDE MANGO ORANGE 1L","5.20","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009697008935","","");
INSERT INTO product VALUES("243","DENDAIRY CITRO/P/JUICE 500ML","DENDAIRY CITRO/P/JUICE 500ML","6.56","9","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009698811831","","");
INSERT INTO product VALUES("244","AFIA MIXED FRUIT FLAV 500ML","AFIA MIXED FRUIT FLAV 500ML","7.50","11","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6008459000972","","");
INSERT INTO product VALUES("245","THIRSTY GRANADILA 2L","THIRSTY GRANADILA 2L","16.33","24","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009650800149","","");
INSERT INTO product VALUES("246","PASSIONORANGE 1L","PASSIONORANGE 1L","8.33","15","","68","9","1","0","19","","2019-10-05 23:47:32","6","5","6009652784867","","");
INSERT INTO product VALUES("247","CERES RED GRAPE 200ML","CERES RED GRAPE 200ML","4.00","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240200070","","");
INSERT INTO product VALUES("248","AFIA ORANGE FLAV 500ML","AFIA ORANGE FLAV 500ML","7.57","11","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6008459000279","","");
INSERT INTO product VALUES("249","DENDAIRY ORANGE PURE JUICE 1L","DENDAIRY ORANGE PURE JUICE 1L","13.69","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009698811558","","");
INSERT INTO product VALUES("250","YESS PINEAPPLE 350ML","YESS PINEAPPLE 350ML","2.20","3.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","606110287982","","");
INSERT INTO product VALUES("251","DENDAIRY CITRO ORANGE 200ML","DENDAIRY CITRO ORANGE 200ML","2.30","3.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009698812005","","");
INSERT INTO product VALUES("252","DENDAIRY CITRO/P/JUICE 200ML","DENDAIRY CITRO/P/JUICE 200ML","2.30","3.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009698812036","","");
INSERT INTO product VALUES("253","DENDAIRY TROPICAL /P/JUICE 1L","DENDAIRY TROPICAL /P/JUICE 1L","13.69","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009698812821","","");
INSERT INTO product VALUES("254","FROOTY 1L","FROOTY 1L","8.33","15","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009652782801","","");
INSERT INTO product VALUES("255","CERES LITCHI FLAV.  200ML","CERES LITCHI FLAV.  200ML","4.00","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240200292","","");
INSERT INTO product VALUES("256","SAVANAH L/CORDIAL 750ML","SAVANAH L/CORDIAL 750ML","7.50","11","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009650800248","","");
INSERT INTO product VALUES("257","FRUITREE RED GRAPE 350ML","FRUITREE RED GRAPE 350ML","6.67","10.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001240225158","","");
INSERT INTO product VALUES("258","PARMALAT CABANA/PINEAPPLE 2L","PARMALAT CABANA/PINEAPPLE 2L","20.85","28","","68","10","1","0","15","","2019-10-05 23:47:32","6","5","6009683612061","","");
INSERT INTO product VALUES("259","FRUITREE GUAVA 350ML","FRUITREE GUAVA 350ML","6.67","10.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001240430033","","");
INSERT INTO product VALUES("260","SMOOTHSIDE P/PPLE 1L","SMOOTHSIDE P/PPLE 1L","5.50","8","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009697009109","","");
INSERT INTO product VALUES("261","EMBEE 500ML","EMBEE 500ML","6.50","9","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6203011061327","","");
INSERT INTO product VALUES("262","MINUTE MAID ORANGE 400ML","MINUTE MAID ORANGE 400ML","7.50","9.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","90490279","","");
INSERT INTO product VALUES("263","MINUTE MAID TROPICAL 400ML","MINUTE MAID TROPICAL 400ML","7.50","9.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","90399480","","");
INSERT INTO product VALUES("264","DENDAIRY APPLE/P/JUICE 500ML","DENDAIRY APPLE/P/JUICE 500ML","7.48","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009698812067","","");
INSERT INTO product VALUES("265","CERES APPLE 200ML","CERES APPLE 200ML","4.00","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240200018","","");
INSERT INTO product VALUES("266","DENDAIRY ORANGE/P/JUICE 500ML","DENDAIRY ORANGE/P/JUICE 500ML","7.48","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009698812043","","");
INSERT INTO product VALUES("267","DENDAIRY CITRO/O/JUICE 500ML","DENDAIRY CITRO/O/JUICE 500ML","6.56","9","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009698811343","","");
INSERT INTO product VALUES("268","CERES MED OF FRUITS 200ML","CERES MED OF FRUITS 200ML","4.00","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240200667","","");
INSERT INTO product VALUES("269","SMOOTH SIDE APR/PEACH 1L","SMOOTH SIDE APR/PEACH 1L","5.42","8","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009697008959","","");
INSERT INTO product VALUES("270","CERES GUAVA 1L","CERES GUAVA 1L","14.08","17.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240100035","","");
INSERT INTO product VALUES("271","SMOOTH COCO-PINE 1L","SMOOTH COCO-PINE 1L","5.42","8","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009697008973","","");
INSERT INTO product VALUES("272","FRUITREE MED 350ML","FRUITREE MED 350ML","6.88","10.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001240430323","","");
INSERT INTO product VALUES("273","Ceres White Grape Juice 275 ml","Ceres White Grape Juice 275 ml","9.00","11.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240222706","","");
INSERT INTO product VALUES("274","PURE JOY LIGHT CRANB","PURE JOY LIGHT CRANB","18.87","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001049050319","","");
INSERT INTO product VALUES("275","FRUITREE TROPICAL 350ML","FRUITREE TROPICAL 350ML","6.67","10.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001240430309","","");
INSERT INTO product VALUES("276","JUST GRANADILA  2L","JUST GRANADILA  2L","17.17","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652782573","","");
INSERT INTO product VALUES("277","AFIA MANGO FLAV 500ML","AFIA MANGO FLAV 500ML","8.42","12","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6008459000453","","");
INSERT INTO product VALUES("278","THIRSTY PINEAPPLE 2L","THIRSTY PINEAPPLE 2L","15.67","21","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009650800118","","");
INSERT INTO product VALUES("279","FRUITREE APPLE 1L","FRUITREE APPLE 1L","12.90","17","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001240390016","","");
INSERT INTO product VALUES("280","PURE JOY CRANBERRY 500ML","PURE JOY CRANBERRY 500ML","10.37","14","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683613129","","");
INSERT INTO product VALUES("281","SMOOTH SIDE GRANADILA 1L","SMOOTH SIDE GRANADILA 1L","5.20","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6006427001815","","");
INSERT INTO product VALUES("282","PARMALAT CABANA PEACH 500ML","PARMALAT CABANA PEACH 500ML","6.05","8.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683612108","","");
INSERT INTO product VALUES("283","COCOPINE 350 ML","COCOPINE 350 ML","4.00","5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009652784539","","");
INSERT INTO product VALUES("284","PARMALAT CABANA ORANGE 2L","PARMALAT CABANA ORANGE 2L","20.85","28","","68","10","1","0","15","","2019-10-05 23:47:32","6","5","6009683612054","","");
INSERT INTO product VALUES("285","COCO PINE 1L","COCO PINE 1L","8.33","15","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009652784829","","");
INSERT INTO product VALUES("286","H2 ZERO WATER 750ML","H2 ZERO WATER 750ML","3.00","6","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009817150049","","");
INSERT INTO product VALUES("287","H2ZERO 500ML","H2ZERO 500ML","2.50","4","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009817150001","","");
INSERT INTO product VALUES("288","AQUA FALLS","AQUA FALLS","2.50","3.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009671990614","","");
INSERT INTO product VALUES("289","MANZI VALLEY WATER 750ML","MANZI VALLEY WATER 750ML","2.17","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009645820145","","");
INSERT INTO product VALUES("290","H2 ZERO 250ML","H2 ZERO 250ML","1.50","2.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009817150124","","");
INSERT INTO product VALUES("291","MANZI VALLEY WATER 500ML","MANZI VALLEY WATER 500ML","1.92","3","","68","11","1","0","21","","2019-10-11 18:40:01","6","5","6009645820213","","");
INSERT INTO product VALUES("292","AQUARITE PURIFIED WATER 500ML","AQUARITE PURIFIED WATER 500ML","1.83","3","","68","10","1","0","11","","2019-10-05 23:47:32","6","5","6009706163419","","");
INSERT INTO product VALUES("293","AQUA MAX 750 MLS","AQUA MAX 750 MLS","2.50","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652782719","","");
INSERT INTO product VALUES("294","AQUA WATER 750 MLS","AQUA WATER 750 MLS","1.41","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","7000839297230","","");
INSERT INTO product VALUES("295","AQUA MAX 500  MLS","AQUA MAX 500  MLS","2.50","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652782757","","");
INSERT INTO product VALUES("296","AQUAVITA  M/WATER 500ML","AQUAVITA  M/WATER 500ML","1.75","3.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009671260021","","");
INSERT INTO product VALUES("297","AQUAVITA  M/WATER 750ML","AQUAVITA  M/WATER 750ML","2.17","4","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009671260038","","");
INSERT INTO product VALUES("298","ALASKA WATER 600 ML","ALASKA WATER 600 ML","1.10","2.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644928040","","");
INSERT INTO product VALUES("299","AQUACLEAR 1L","AQUACLEAR 1L","3.33","5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803227342","","");
INSERT INTO product VALUES("300","AQUA SAVANA 1 LT","AQUA SAVANA 1 LT","5.30","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009801423005","","");
INSERT INTO product VALUES("301","AQUASAVANA 1L","AQUASAVANA 1L","3.33","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009801423166","","");
INSERT INTO product VALUES("302","AQUAVITA  M/WATER 1.5L","AQUAVITA  M/WATER 1.5L","4.33","7","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009671260069","","");
INSERT INTO product VALUES("303","AQUASAVANA 750ML","AQUASAVANA 750ML","2.21","5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009801423050","","");
INSERT INTO product VALUES("304","AQUAVITA  M/WATER 350ML","AQUAVITA  M/WATER 350ML","1.31","2","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009671260052","","");
INSERT INTO product VALUES("305","AQUA  WATER 500ML","AQUA  WATER 500ML","1.10","2","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","7000839297162","","");
INSERT INTO product VALUES("306","AQUACLEAR M/WATER 500ML","AQUACLEAR M/WATER 500ML","2.17","3.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803227328","","");
INSERT INTO product VALUES("307","AQUASAVANA M/WATER 500ML","AQUASAVANA M/WATER 500ML","2.08","3","","68","2","1","0","19","","2019-10-28 13:57:12","6","5","6009801423043","","");
INSERT INTO product VALUES("308","AMA SIP SIP MILK MAHEU 470ML","AMA SIP SIP MILK MAHEU 470ML","3.70","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537019381","","");
INSERT INTO product VALUES("309","MAZOE ","MAZOE ","13.30","19.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","5449000095725","","");
INSERT INTO product VALUES("310","KRUSH FRUIT PUNCH 1.5L","KRUSH FRUIT PUNCH 1.5L","10.50","16","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","7000839296936","","");
INSERT INTO product VALUES("311","AMA SIP SIP STRAW 470ML","AMA SIP SIP STRAW 470ML","3.92","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537019428","","");
INSERT INTO product VALUES("312","SOURCE ICE TEA","SOURCE ICE TEA","2.00","3.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644991440600000000000","","");
INSERT INTO product VALUES("313","THIRSTY LEMON SQUASH 2LT","THIRSTY LEMON SQUASH 2LT","16.33","17","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009650800125","","");
INSERT INTO product VALUES("314","MAHEU STRAWBERRY  300ML","MAHEU STRAWBERRY  300ML","2.33","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644920051","","");
INSERT INTO product VALUES("315","SHAKERS BANA/MILK SHAKE300ML","SHAKERS BANA/MILK SHAKE300ML","3.50","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672678","","");
INSERT INTO product VALUES("316","MILKIT BANANA FLAV 500ML","MILKIT BANANA FLAV 500ML","6.58","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644997145","","");
INSERT INTO product VALUES("317","AMA SIPSIP B/CREAM 300ML","AMA SIPSIP B/CREAM 300ML","2.25","4","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009537021575","","");
INSERT INTO product VALUES("318","KRUSH MANGO1.5L","KRUSH MANGO1.5L","10.50","16","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","7000839296868","","");
INSERT INTO product VALUES("319","MAZOE CRUSH  CREAM SODA 1L","MAZOE CRUSH  CREAM SODA 1L","12.30","19.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","544900009890","","");
INSERT INTO product VALUES("320","SHAKERS VANILLA/MILK 300ML","SHAKERS VANILLA/MILK 300ML","3.50","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672661","","");
INSERT INTO product VALUES("321","MAZOE ORANGE 1LT","MAZOE ORANGE 1LT","12.50","12.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","5449000062970","","");
INSERT INTO product VALUES("322","JOLLY JUICE2L","JOLLY JUICE2L","13.17","21","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","5449000131218","","");
INSERT INTO product VALUES("323","YESS APPLE 500MLS","YESS APPLE 500MLS","2.00","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537019800","","");
INSERT INTO product VALUES("324","AMA SIPSIP CHOC. FLAV 470ML","AMA SIPSIP CHOC. FLAV 470ML","3.80","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537019374","","");
INSERT INTO product VALUES("325","JOLLY JUICE 1LT","JOLLY JUICE 1LT","12.00","15","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","5449000063052","","");
INSERT INTO product VALUES("326","M/MAHEU 300ML","M/MAHEU 300ML","2.50","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537021063","","");
INSERT INTO product VALUES("327","AMA SIP SIPBUTTER MILK 470G","AMA SIP SIPBUTTER MILK 470G","3.40","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537019404","","");
INSERT INTO product VALUES("328","AMA SIP SIP TRAW300ML","AMA SIP SIP TRAW300ML","2.25","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537021568","","");
INSERT INTO product VALUES("329","PARMALAT  LACTO 2L","PARMALAT  LACTO 2L","21.00","27","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683611538","","");
INSERT INTO product VALUES("330","MAZOE CREAM SODA","MAZOE CREAM SODA","8.00","12.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","5449000009890","","");
INSERT INTO product VALUES("331","AMA SIP SIP MAHEU. 300ML","AMA SIP SIP MAHEU. 300ML","2.25","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537021049","","");
INSERT INTO product VALUES("332","SHAKERS VAN/MILK 500ML","SHAKERS VAN/MILK 500ML","5.25","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672647","","");
INSERT INTO product VALUES("333","SHAKERS STRAW/MILKSHAKE300ML","SHAKERS STRAW/MILKSHAKE300ML","3.50","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672685","","");
INSERT INTO product VALUES("334","AMA SIP SIP VAN. 300ML","AMA SIP SIP VAN. 300ML","2.25","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537021056","","");
INSERT INTO product VALUES("335","AMA SIPSIP MAHEU VANILLA 470ML","AMA SIPSIP MAHEU VANILLA 470ML","4.83","7","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009537019398","","");
INSERT INTO product VALUES("336","YESS PINE 500MLS","YESS PINE 500MLS","2.00","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644921027","","");
INSERT INTO product VALUES("337","YESS FRUITY PUNCH 500MLS","YESS FRUITY PUNCH 500MLS","2.00","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644921003","","");
INSERT INTO product VALUES("338","MAZOECRUSH  C/SODA 2LT","MAZOECRUSH  C/SODA 2LT","20.80","29.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","5449000009401","","");
INSERT INTO product VALUES("339","YESS STRAW BAN 500MLS","YESS STRAW BAN 500MLS","2.00","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537019916","","");
INSERT INTO product VALUES("340","MAZOE ORANGE CRUSH 2LT","MAZOE ORANGE CRUSH 2LT","20.67","29.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","5449000062994","","");
INSERT INTO product VALUES("341","AMA SIP SIP BANA FLAV. 300ML","AMA SIP SIP BANA FLAV. 300ML","2.25","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537021544","","");
INSERT INTO product VALUES("342","KRUSH PINEAPPLE 1.5L","KRUSH PINEAPPLE 1.5L","10.50","16","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","7000839296622","","");
INSERT INTO product VALUES("343","KRUSH ORANGE 1.5L","KRUSH ORANGE 1.5L","10.50","16","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","7000839296790","","");
INSERT INTO product VALUES("344","COOLSPLASH ORANGE 2LT","COOLSPLASH ORANGE 2LT","13.67","19","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001651042023","","");
INSERT INTO product VALUES("345","EMBE DRINK 300MLS","EMBE DRINK 300MLS","3.50","5.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6203011060801","","");
INSERT INTO product VALUES("346","EMBE DRINK 300MLS","EMBE DRINK 300MLS","3.50","5.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644547647","","");
INSERT INTO product VALUES("347","AMA SIP SIP BUTTER CREAM 470G","AMA SIP SIP BUTTER CREAM 470G","3.92","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537019442","","");
INSERT INTO product VALUES("348","PEPSI  500ML","PEPSI  500ML","5.00","7","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009681260196","","");
INSERT INTO product VALUES("349","JUST PINEAPPLE 1LT","JUST PINEAPPLE 1LT","9.83","14","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652782665","","");
INSERT INTO product VALUES("350","GO FRESH ORANGE 1.5LTR","GO FRESH ORANGE 1.5LTR","13.67","18.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644924431","","");
INSERT INTO product VALUES("351","JUST ORANGE 2LT","JUST ORANGE 2LT","17.17","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652782559","","");
INSERT INTO product VALUES("352","MILKIT CHOCO/FLAV500ML","MILKIT CHOCO/FLAV500ML","7.25","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644997152","","");
INSERT INTO product VALUES("353","KABULONGA BEANS","KABULONGA BEANS","6.00","10.96","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","2000104010965","","");
INSERT INTO product VALUES("354","RED ONION","RED ONION","2.00","7.5","","68","10","1","0","20","","2019-10-05 23:47:32","6","5","2006060000000","","");
INSERT INTO product VALUES("355","DRY GROUNDNUTS","DRY GROUNDNUTS","8.50","17","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","2000080000000","","");
INSERT INTO product VALUES("356","CHAKUS GROUNDNUTS 1KG","CHAKUS GROUNDNUTS 1KG","8.00","12","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","783","","");
INSERT INTO product VALUES("357","LUSAKA BEANS","LUSAKA BEANS","9.00","25","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","2000060000000","","");
INSERT INTO product VALUES("358","KABULANGETI BEANS","KABULANGETI BEANS","11.00","25","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","2000100000000","","");
INSERT INTO product VALUES("359","WHITE BEANS","WHITE BEANS","8.50","19","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","2000110000000","","");
INSERT INTO product VALUES("360","F N V FRESH RAPE","F N V FRESH RAPE","1.00","2","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","152","","");
INSERT INTO product VALUES("361","DRY CHIBWABWA","DRY CHIBWABWA","1.50","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","2000","","");
INSERT INTO product VALUES("362","WHITE ONION","WHITE ONION","2.00","6.5","","68","10","1","0","20","","2019-10-05 23:47:32","6","5","2006070000000","","");
INSERT INTO product VALUES("363","DRY CHILLI","DRY CHILLI","1.50","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","3000","","");
INSERT INTO product VALUES("364","EGG PLANT","EGG PLANT","2.00","10","","68","10","1","0","20","","2019-10-05 23:47:32","6","5","2006100000000","","");
INSERT INTO product VALUES("365","BAKERS ROMANY CREAMS MINT 200G","BAKERS ROMANY CREAMS MINT 200G","14.17","19","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001056004619","","");
INSERT INTO product VALUES("366","DLITE BANANA 500G","DLITE BANANA 500G","14.17","21","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644924738","","");
INSERT INTO product VALUES("367","DLITE GROWN UP 500G","DLITE GROWN UP 500G","14.17","20.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537013051","","");
INSERT INTO product VALUES("368","DLITE BANANA 250G","DLITE BANANA 250G","13.60","20","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644923144","","");
INSERT INTO product VALUES("369","NESTLE CERELAC REGULAR 250G","NESTLE CERELAC REGULAR 250G","19.50","24.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001068625109","","");
INSERT INTO product VALUES("370","NESTLE CERELAC WHEAT 250G","NESTLE CERELAC WHEAT 250G","18.67","24.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6161107791643","","");
INSERT INTO product VALUES("371"," NAN FORMULA 2 400G"," NAN FORMULA 2 400G","0.00","60","","68","648","1","0","21","","2019-10-28 08:05:15","6","5","6001068582402","","");
INSERT INTO product VALUES("372"," S-26 FORMULA 1"," S-26 FORMULA 1","44.50","64","","68","7","1","0","21","","2019-10-09 12:32:25","6","5","6009691190056","","");
INSERT INTO product VALUES("373","D'LITE GROWNUPS WHEAT 250G","D'LITE GROWNUPS WHEAT 250G","9.70","14","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537015031","","");
INSERT INTO product VALUES("374","NAN FORMULA 1 400G","NAN FORMULA 1 400G","42.50","60","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001068687800","","");
INSERT INTO product VALUES("375","SUPA CEREAL ORIGINAL 50G","SUPA CEREAL ORIGINAL 50G","1.22","1.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","797776103577","","");
INSERT INTO product VALUES("376","CERELAC WHEAT 500G","CERELAC WHEAT 500G","35.00","45","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6161107791636","","");
INSERT INTO product VALUES("377","HAPPY TOts WHEAT 250G","HAPPY TOts WHEAT 250G","11.25","14","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644996278","","");
INSERT INTO product VALUES("378","SUPA CEREAL STRAW. FLAV 500G","SUPA CEREAL STRAW. FLAV 500G","9.50","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","797776103560","","");
INSERT INTO product VALUES("379","CEREVITA NUTRITIOUS I/S 500G","CEREVITA NUTRITIOUS I/S 500G","29.67","42","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6007588003083","","");
INSERT INTO product VALUES("380","SUPA CEREAL BANANA FLAV. 500G","SUPA CEREAL BANANA FLAV. 500G","9.50","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","797776103553","","");
INSERT INTO product VALUES("381","SUPA CEREAL STRAW. FLAV 50G","SUPA CEREAL STRAW. FLAV 50G","1.28","2","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","797776103591","","");
INSERT INTO product VALUES("382","SUPA CEREAL ORIGINAL 500G","SUPA CEREAL ORIGINAL 500G","9.50","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","797776103546","","");
INSERT INTO product VALUES("383","SUPA CEREAL BANA. FLAV 50G","SUPA CEREAL BANA. FLAV 50G","1.22","1.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","797776103584","","");
INSERT INTO product VALUES("384","GROWNUPS  CHOCOLATE 250G","GROWNUPS  CHOCOLATE 250G","7.80","11.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537015048","","");
INSERT INTO product VALUES("385","DLITE GROWN UPS WHEAT 500G","DLITE GROWN UPS WHEAT 500G","17.40","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537014348","","");
INSERT INTO product VALUES("386","HAPPY TOts BANANA 250G","HAPPY TOts BANANA 250G","0.00","14","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644996285","","");
INSERT INTO product VALUES("387","D'LITE MALT N MILK 250G","D'LITE MALT N MILK 250G","13.16","22","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644924608","","");
INSERT INTO product VALUES("388","NESTLE LACTOGEN FORMULA 2 400G","NESTLE LACTOGEN FORMULA 2 400G","31.17","35","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001068578306","","");
INSERT INTO product VALUES("389","ROYAL BAKI NG POWDER 200G","ROYAL BAKI NG POWDER 200G","17.50","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001015506017","","");
INSERT INTO product VALUES("390","CHAPA MANDASHI B/POWDER 100G","CHAPA MANDASHI B/POWDER 100G","3.50","5.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6161101661713","","");
INSERT INTO product VALUES("391","ROYAL BAKING POWDER 100G","ROYAL BAKING POWDER 100G","15.50","20","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","60051417","","");
INSERT INTO product VALUES("392","NESTLE HOT CHOCOLATE 500G","NESTLE HOT CHOCOLATE 500G","52.00","68","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001068379200","","");
INSERT INTO product VALUES("393","YATU LOOSE TEA 25G","YATU LOOSE TEA 25G","0.30","1.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683950064","","");
INSERT INTO product VALUES("394","FRESHPAK ROOIBOS TEA 40","FRESHPAK ROOIBOS TEA 40","16.08","21","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001156905106","","");
INSERT INTO product VALUES("395","FIVE ROSES125G","FIVE ROSES125G","17.50","24.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001156230154","","");
INSERT INTO product VALUES("396","FIVE ROSES 26s 62.5G","FIVE ROSES 26s 62.5G","10.00","14.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001156230062","","");
INSERT INTO product VALUES("397","YATU TEA BAGS 50,S ","YATU TEA BAGS 50,S ","9.00","15.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683950026","","");
INSERT INTO product VALUES("398","NESTLE MILO 250G","NESTLE MILO 250G","21.42","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001068371204","","");
INSERT INTO product VALUES("399","YATU TEA BAGS 25'S","YATU TEA BAGS 25'S","4.33","6.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683950019","","");
INSERT INTO product VALUES("400","FIVE ROSES 125G","FIVE ROSES 125G","6.50","8.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001156589696","","");
INSERT INTO product VALUES("401","NESTLE COCOA 62.5 G","NESTLE COCOA 62.5 G","9.50","14","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001068002207","","");
INSERT INTO product VALUES("402","TIPS TEA BAGS 100S 250G","TIPS TEA BAGS 100S 250G","12.50","16","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6005418003654","","");
INSERT INTO product VALUES("403","FRISCO RICH CREAMY 250G","FRISCO RICH CREAMY 250G","26.80","36","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009702440965","","");
INSERT INTO product VALUES("404","TIPS TEA BAGS 50S 125G","TIPS TEA BAGS 50S 125G","10.00","15.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6005418003678","","");
INSERT INTO product VALUES("405","FRISCO B/STRONG COFFEE 250G","FRISCO B/STRONG COFFEE 250G","23.33","30","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009702440033","","");
INSERT INTO product VALUES("406","MILO 125 G","MILO 125 G","12.05","17.5","","68","590","1","0","21","","2019-10-14 18:28:41","6","5","6001068370108","","");
INSERT INTO product VALUES("407","COCOA 125G","COCOA 125G","17.00","23.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001068002306","","");
INSERT INTO product VALUES("408","JOKO TEA BAGS 250G","JOKO TEA BAGS 250G","20.00","26","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001089120843","","");
INSERT INTO product VALUES("409","FIVE ROSES 50G TEA","FIVE ROSES 50G TEA","2.10","3.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001156589689","","");
INSERT INTO product VALUES("410","FIVE ROSES 250G","FIVE ROSES 250G","29.20","20","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001156230253","","");
INSERT INTO product VALUES("411","FRESH PAK 20s","FRESH PAK 20s","8.75","13","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001156905236","","");
INSERT INTO product VALUES("412","KELLOGS CORN FLAKES 1KG","KELLOGS CORN FLAKES 1KG","41.00","48","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001306000996","","");
INSERT INTO product VALUES("413","LYONS QUICKBREW 100S","LYONS QUICKBREW 100S","20.00","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6004842001526","","");
INSERT INTO product VALUES("414","TIPS TEA LEAVES 125G","TIPS TEA LEAVES 125G","5.25","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6005418000530","","");
INSERT INTO product VALUES("415","TEA BAGS 80S","TEA BAGS 80S","26.50","30","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001156905205","","");
INSERT INTO product VALUES("416","LYONS QUICK BREW 50S","LYONS QUICK BREW 50S","7.80","11","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6004842001519","","");
INSERT INTO product VALUES("417","TIPS TEA 26 TAGLESS BAGS 62.5G","TIPS TEA 26 TAGLESS BAGS 62.5G","8.00","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6005418003692","","");
INSERT INTO product VALUES("418","CHELSEA CREAMS 150G","CHELSEA CREAMS 150G","4.95","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537013761","","");
INSERT INTO product VALUES("419","CHELSEA OAT CRUNCH 200G","CHELSEA OAT CRUNCH 200G","6.75","9.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644992317","","");
INSERT INTO product VALUES("420","BARKERS CLASSIC CHOC/OAT 200G","BARKERS CLASSIC CHOC/OAT 200G","13.75","20","","68","9","1","0","21","","2019-10-18 08:13:15","6","5","6001056119009","","");
INSERT INTO product VALUES("421","ROYAL ANYTIME VANILLA 30G","ROYAL ANYTIME VANILLA 30G","0.43","1","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009880330454","","");
INSERT INTO product VALUES("422","CHELSEA CHOC CHIP 150G","CHELSEA CHOC CHIP 150G","6.75","9.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537014072","","");
INSERT INTO product VALUES("423","BAKERS ROMANY CREAMS 200G","BAKERS ROMANY CREAMS 200G","15.00","19.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009704171263","","");
INSERT INTO product VALUES("424","LOBELS LEMON CREAM","LOBELS LEMON CREAM","4.17","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6007265000152","","");
INSERT INTO product VALUES("425","BAKERS TENIS BISCUITS 200G","BAKERS TENIS BISCUITS 200G","9.42","13.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001056662000","","");
INSERT INTO product VALUES("426","MUSA MUKAKA","MUSA MUKAKA","1.30","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009630400819","","");
INSERT INTO product VALUES("427","CREAMBOS STRAW","CREAMBOS STRAW","1.85","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644927753","","");
INSERT INTO product VALUES("428","EET-SUM-MOR CHOC/CHIP 200G","EET-SUM-MOR CHOC/CHIP 200G","11.25","15","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009704171294","","");
INSERT INTO product VALUES("429","ROYAL MCHUZI BEEF 10G","ROYAL MCHUZI BEEF 10G","0.53","1.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6161100609426","","");
INSERT INTO product VALUES("430","BAKERS TOPPER CHOCO. 125G","BAKERS TOPPER CHOCO. 125G","4.17","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001056412919","","");
INSERT INTO product VALUES("431","CHELSEA CHOC MELTZ","CHELSEA CHOC MELTZ","7.50","11","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537014195","","");
INSERT INTO product VALUES("432","LOBELS C/ A/BISCUITS 200G","LOBELS C/ A/BISCUITS 200G","5.16","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6007265000190","","");
INSERT INTO product VALUES("433","BAK T MINT FLAV. 125G","BAK T MINT FLAV. 125G","4.83","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009704170136","","");
INSERT INTO product VALUES("434","CHICCO LEMON DREAM CREAM140G","CHICCO LEMON DREAM CREAM140G","1.80","2.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6291103082039","","");
INSERT INTO product VALUES("435","CHICCO LEMON BISCUIT 125G","CHICCO LEMON BISCUIT 125G","4.50","5.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6291103080325","","");
INSERT INTO product VALUES("436","AMAZON CREAMBOS  CUSTARD95G","AMAZON CREAMBOS  CUSTARD95G","1.83","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644927760","","");
INSERT INTO product VALUES("437","CHICCO VANILA 125G","CHICCO VANILA 125G","4.50","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6291103080332","","");
INSERT INTO product VALUES("438","BAKERS TOPPER RAS. FLAV. 125G","BAKERS TOPPER RAS. FLAV. 125G","4.83","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001056412926","","");
INSERT INTO product VALUES("439","AMAZON CHELSEA C/COOKIES 150G","AMAZON CHELSEA C/COOKIES 150G","4.20","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537014058","","");
INSERT INTO product VALUES("440","CHICCO EET-EM-ALL 200G","CHICCO EET-EM-ALL 200G","5.71","8","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6291103082503","","");
INSERT INTO product VALUES("441","ROYAL ANYTIME CHOC 60G","ROYAL ANYTIME CHOC 60G","0.91","1.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009880330171","","");
INSERT INTO product VALUES("442","BAKERS GINGER NUTS","BAKERS GINGER NUTS","12.67","17.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001056294003","","");
INSERT INTO product VALUES("443","ROYAL ANYTIME CSTARD 30G","ROYAL ANYTIME CSTARD 30G","0.43","1","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009880330249","","");
INSERT INTO product VALUES("444","BAKERS ROMANY CREAMS/C/200G","BAKERS ROMANY CREAMS/C/200G","13.75","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001125001877","","");
INSERT INTO product VALUES("445","MUSA CHOCOLATE","MUSA CHOCOLATE","1.92","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009630400031","","");
INSERT INTO product VALUES("446","CHELSEA BUTTER MOR 150G","CHELSEA BUTTER MOR 150G","6.75","9.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644996148","","");
INSERT INTO product VALUES("447","ROYAL  ANYTIME LEMON 60G","ROYAL  ANYTIME LEMON 60G","1.00","1.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009880330232","","");
INSERT INTO product VALUES("448","BAKERS CHOICE ASSORTED","BAKERS CHOICE ASSORTED","15.83","20.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009704170488","","");
INSERT INTO product VALUES("449","BAKERS TOPPER LEMON 125G","BAKERS TOPPER LEMON 125G","4.83","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009704170822","","");
INSERT INTO product VALUES("450","MUSA BABY BISCUITS 230G","MUSA BABY BISCUITS 230G","3.30","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009630400048","","");
INSERT INTO product VALUES("451","LOBELS GINGER NUTS 200G ","LOBELS GINGER NUTS 200G ","4.17","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6007265001913","","");
INSERT INTO product VALUES("452","BAKERS EET-SUM-MOR 200G","BAKERS EET-SUM-MOR 200G","10.90","15.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001056201001","","");
INSERT INTO product VALUES("453","LOBELS MARIE BISCUIT 200G","LOBELS MARIE BISCUIT 200G","3.50","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6007265000251","","");
INSERT INTO product VALUES("454","MUSA VANILLA","MUSA VANILLA","2.08","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009630400000","","");
INSERT INTO product VALUES("455","BAKERS TOPPER CUSTARD 125G","BAKERS TOPPER CUSTARD 125G","4.83","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001056412940","","");
INSERT INTO product VALUES("456","AMAZON CHELSEA SCOTTISH/B 185G","AMAZON CHELSEA SCOTTISH/B 185G","6.75","9.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537014041","","");
INSERT INTO product VALUES("457","BAKERS LEMON CREAM","BAKERS LEMON CREAM","10.41","14","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001056411004","","");
INSERT INTO product VALUES("458","BAKERS TOPPER VANI. 125G","BAKERS TOPPER VANI. 125G","4.83","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001056412933","","");
INSERT INTO product VALUES("459","BAKERS WHRLS STRAW. BISC 200G","BAKERS WHRLS STRAW. BISC 200G","16.25","23.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001056633000","","");
INSERT INTO product VALUES("460","BAKERS BLUE LABEL","BAKERS BLUE LABEL","7.75","11.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001056453004","","");
INSERT INTO product VALUES("461","LUCKY STAR SARDINES 155G","LUCKY STAR SARDINES 155G","5.00","10","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001115003584","","");
INSERT INTO product VALUES("462","LUCKY STAR 215G","LUCKY STAR 215G","3.33","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001115002853","","");
INSERT INTO product VALUES("463","LUCKY STAR SARDINES 155G","LUCKY STAR SARDINES 155G","6.75","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001115000613","","");
INSERT INTO product VALUES("464","BULL BRAND C/MEAT 190G","BULL BRAND C/MEAT 190G","6.50","12","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001330000122","","");
INSERT INTO product VALUES("465","FRAY BENTOS 250G","FRAY BENTOS 250G","14.16","19","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009667440505","","");
INSERT INTO product VALUES("466","B/B C/MEAT 300G","B/B C/MEAT 300G","12.80","22","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001330000146","","");
INSERT INTO product VALUES("467","BULL BRAND CORNED MEAT 190G","BULL BRAND CORNED MEAT 190G","12.50","18","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6005044008115","","");
INSERT INTO product VALUES("468","RHODES SUGAR BEANS 400G","RHODES SUGAR BEANS 400G","11.00","14.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6005044006210","","");
INSERT INTO product VALUES("469","KELLOGS CORN FLAKES 1KG","KELLOGS CORN FLAKES 1KG","30.00","48","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","2002009010231","","");
INSERT INTO product VALUES("470","KATUNJILA RICE 500G","KATUNJILA RICE 500G","4.88","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009706164386","","");
INSERT INTO product VALUES("471"," CEREVITA 500G"," CEREVITA 500G","30.00","36","","68","893","1","0","21","","2019-10-27 08:19:25","6","5","6161107791681","","");
INSERT INTO product VALUES("472","JUNGLE OATS 1KG","JUNGLE OATS 1KG","19.00","27.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009518201224","","");
INSERT INTO product VALUES("473","KELLOGS ALL BRAN 500G ","KELLOGS ALL BRAN 500G ","19.50","28","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001306000897","","");
INSERT INTO product VALUES("474","NESTLE CERELAC BANANA 250G","NESTLE CERELAC BANANA 250G","18.92","25.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001068625307","","");
INSERT INTO product VALUES("475","BOKOMO CORN FLAKES 500G","BOKOMO CORN FLAKES 500G","16.10","22","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001052001506","","");
INSERT INTO product VALUES("476","BOK CFLAKES 750G","BOK CFLAKES 750G","19.00","26","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001052001650","","");
INSERT INTO product VALUES("477","JUNGLE OATS 500G","JUNGLE OATS 500G","14.00","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001275000010","","");
INSERT INTO product VALUES("478","JUNGLE OATS 1KG","JUNGLE OATS 1KG","19.00","36","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001275000003","","");
INSERT INTO product VALUES("479","DLITE WHEAT 250G","DLITE WHEAT 250G","11.50","22","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644923052","","");
INSERT INTO product VALUES("480","MOIRS CUSTARD POWDER 500G","MOIRS CUSTARD POWDER 500G","18.83","25","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001325240328","","");
INSERT INTO product VALUES("481","BOKOMO CORN FLAKES 750G","BOKOMO CORN FLAKES 750G","19.00","35","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001052002459","","");
INSERT INTO product VALUES("482","JUNGLE OATS 500G","JUNGLE OATS 500G","12.10","16.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001275003653","","");
INSERT INTO product VALUES("483","MOIRS CUSTARD POWDER 250G","MOIRS CUSTARD POWDER 250G","12.00","17","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001325230336","","");
INSERT INTO product VALUES("484","LAYS P/ CHIPS S/C/ONION 105G","LAYS P/ CHIPS S/C/ONION 105G","9.10","13.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510806175","","");
INSERT INTO product VALUES("485","SIMBA P/CHIPS CHUTNEY 36G","SIMBA P/CHIPS CHUTNEY 36G","3.10","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510804355","","");
INSERT INTO product VALUES("486","LAYS P/SALT/VINEGAR 105G","LAYS P/SALT/VINEGAR 105G","9.10","13.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510807455","","");
INSERT INTO product VALUES("487","LAYS CARIBBEAN ONOIN 105G","LAYS CARIBBEAN ONOIN 105G","9.50","13","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","9510","","");
INSERT INTO product VALUES("488","DORITOS SUPREM  CHEESE 45G","DORITOS SUPREM  CHEESE 45G","2.40","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510802559","","");
INSERT INTO product VALUES("489","LAYS SPRING ONION & CHEESE 105G","LAYS SPRING ONION & CHEESE 105G","9.50","13","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6182","","");
INSERT INTO product VALUES("490","TINGA TINGA BBQ ","TINGA TINGA BBQ ","0.42","1","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644998883","","");
INSERT INTO product VALUES("491","DORITOS SWEET CHILLI","DORITOS SWEET CHILLI","2.00","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510805888","","");
INSERT INTO product VALUES("492","LAYS THAI SWEET CHILLI 105G","LAYS THAI SWEET CHILLI 105G","9.50","13","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","7462","","");
INSERT INTO product VALUES("493","LAYS P/CHIPS SALTED 36G","LAYS P/CHIPS SALTED 36G","2.40","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510804782","","");
INSERT INTO product VALUES("494","DOR S/ CHILI PEP 45G","DOR S/ CHILI PEP 45G","2.92","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510802542","","");
INSERT INTO product VALUES("495","LAYS SOUR CREAM & ONION 36G","LAYS SOUR CREAM & ONION 36G","2.92","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009510804584","","");
INSERT INTO product VALUES("496","SIMBA P/CHIPS CHEESE/ONION 36G","SIMBA P/CHIPS CHEESE/ONION 36G","3.10","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510804317","","");
INSERT INTO product VALUES("497","LAYS P/CHIPS SALT/VINEGER 36G","LAYS P/CHIPS SALT/VINEGER 36G","2.40","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510804775","","");
INSERT INTO product VALUES("498","YOYO CRISPS  SOUR CREAM","YOYO CRISPS  SOUR CREAM","1.91","3.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644925414","","");
INSERT INTO product VALUES("499","LAYS CARIBBEAN ONION  36G","LAYS CARIBBEAN ONION  36G","2.92","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009","","");
INSERT INTO product VALUES("500","HIP HOP 20g","HIP HOP 20g","0.79","1","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009880607051","","");
INSERT INTO product VALUES("501","ZUMBA NAX TOMATO REDISH","ZUMBA NAX TOMATO REDISH","0.48","0.5","","68","460","1","0","21","","2019-10-27 08:19:25","6","5","6009537017004","","");
INSERT INTO product VALUES("502","ZUMBA NAX","ZUMBA NAX","0.48","0.5","","68","109","1","0","21","","2019-10-12 08:41:36","6","5","6009537016991","","");
INSERT INTO product VALUES("503","TINGA SAVOURY","TINGA SAVOURY","0.42","1","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009675100637","","");
INSERT INTO product VALUES("504","DORITOS CHEESE SUPREME 150G","DORITOS CHEESE SUPREME 150G","12.00","13.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510802580","","");
INSERT INTO product VALUES("505","MAGIC CRUNCH","MAGIC CRUNCH","0.36","0.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009801903972","","");
INSERT INTO product VALUES("506","NIKNAKS 20G","NIKNAKS 20G","0.66","1","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510804683","","");
INSERT INTO product VALUES("507","DOR S/CHILI PEP150G","DOR S/CHILI PEP150G","13.75","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510802573","","");
INSERT INTO product VALUES("508","TINGA BUTTE R","TINGA BUTTE R","0.42","1","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009675100620","","");
INSERT INTO product VALUES("509","LAYS THAI SWEET CHILLI 36G","LAYS THAI SWEET CHILLI 36G","2.92","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009510804607","","");
INSERT INTO product VALUES("510","TINGA SOUR","TINGA SOUR","0.42","1","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009675100590","","");
INSERT INTO product VALUES("511","NIKNAKS BEEF BBQ 50G","NIKNAKS BEEF BBQ 50G","0.66","1","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510804867","","");
INSERT INTO product VALUES("512","POPICO CHEESE SMALL","POPICO CHEESE SMALL","1.52","2.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652784133","","");
INSERT INTO product VALUES("513","CHEESE CURLS 150G","CHEESE CURLS 150G","5.16","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009652780296","","");
INSERT INTO product VALUES("514","CHEESE CURLS  36","CHEESE CURLS  36","0.60","1","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009652780289","","");
INSERT INTO product VALUES("515","POPICO S CREAM 36G","POPICO S CREAM 36G","1.56","2.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009652784157","","");
INSERT INTO product VALUES("516","YOYO CRISPS BBQ","YOYO CRISPS BBQ","2.20","3.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644925421","","");
INSERT INTO product VALUES("517","YOYO CRISPS SPICY TIKKA","YOYO CRISPS SPICY TIKKA","1.91","3.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644925407","","");
INSERT INTO product VALUES("518","LUNCH BAR LARGE","LUNCH BAR LARGE","6.25","9","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001065034027","","");
INSERT INTO product VALUES("519","FRISCO  100G","FRISCO  100G","13.00","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001092100276","","");
INSERT INTO product VALUES("520","HOT CHOCOLATE 250G","HOT CHOCOLATE 250G","26.50","27","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001068379101","","");
INSERT INTO product VALUES("521","MILO M/E/D 500G","MILO M/E/D 500G","37.50","55","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001068480401","","");
INSERT INTO product VALUES("522","FRISCO B/S GRANULES 100G","FRISCO B/S GRANULES 100G","13.00","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009702440002","","");
INSERT INTO product VALUES("523","NESCAFE RICOFFY 250G","NESCAFE RICOFFY 250G","26.33","36","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001068323005","","");
INSERT INTO product VALUES("524","SUN DROP 750ML","SUN DROP 750ML","11.00","16.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6164000733734","","");
INSERT INTO product VALUES("525","SOYOLA COOKNG OIL 750ML","SOYOLA COOKNG OIL 750ML","11.67","16.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6164000733888","","");
INSERT INTO product VALUES("526","SOYOLA 500MLS","SOYOLA 500MLS","7.50","11.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6164000733895","","");
INSERT INTO product VALUES("527","OLE COOKING OIL 750ML","OLE COOKING OIL 750ML","15.40","19.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009674250036","","");
INSERT INTO product VALUES("528","SUPA OIL 2.5L","SUPA OIL 2.5L","55.00","59","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001565031953","","");
INSERT INTO product VALUES("529","DLITE COOKING 2LT","DLITE COOKING 2LT","32.25","38","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001565009037","","");
INSERT INTO product VALUES("530","SOYOLA 2.5L","SOYOLA 2.5L","39.17","56","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6164000733901","","");
INSERT INTO product VALUES("531","ZAMANITA COOKING OIL 5L","ZAMANITA COOKING OIL 5L","80.00","105","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009646320798","","");
INSERT INTO product VALUES("532","SUNFOIL COOKING OIL 2LTR","SUNFOIL COOKING OIL 2LTR","41.00","48.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001565010415","","");
INSERT INTO product VALUES("533","SOYOLA COOKING OIL 5LT","SOYOLA COOKING OIL 5LT","77.00","86","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6164000733925","","");
INSERT INTO product VALUES("534","SUNSHINE COOKING OIL 2LTS","SUNSHINE COOKING OIL 2LTS","33.33","50","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6164000733260","","");
INSERT INTO product VALUES("535","MAMA'S COOKING OIL 2.5L","MAMA'S COOKING OIL 2.5L","42.50","54","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009646320569","","");
INSERT INTO product VALUES("536","MAMAS COOKING OIL 750ML","MAMAS COOKING OIL 750ML","11.50","16.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009646320736","","");
INSERT INTO product VALUES("537","D LITE PURE COOKING OIL 5LT","D LITE PURE COOKING OIL 5LT","82.50","94","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001565009044","","");
INSERT INTO product VALUES("538","SOYOLA COOKING OIL 2LT","SOYOLA COOKING OIL 2LT","30.00","41","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6164000733918","","");
INSERT INTO product VALUES("539","ZAMANITA COOKING OIL 2.5LT","ZAMANITA COOKING OIL 2.5LT","64.00","56","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009646320712","","");
INSERT INTO product VALUES("540","ZAMANITA COOKING OIL 750ML","ZAMANITA COOKING OIL 750ML","12.08","16","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009646320330","","");
INSERT INTO product VALUES("541","ZAMGOLD CS/OIL 5LITR","ZAMGOLD CS/OIL 5LITR","100.00","110","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009706160586","","");
INSERT INTO product VALUES("542","ZAMGOLD C/OIL 2LITRS","ZAMGOLD C/OIL 2LITRS","37.00","48","","68","11","1","0","21","","2019-10-11 18:40:01","6","5","6009706160500","","");
INSERT INTO product VALUES("543","OLE VEG. OIL 2.5 L","OLE VEG. OIL 2.5 L","50.83","59","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009674250074","","");
INSERT INTO product VALUES("544","D LITE COOKING OIL 750ML","D LITE COOKING OIL 750ML","12.67","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001565009020","","");
INSERT INTO product VALUES("545","SUPA OIL 5L","SUPA OIL 5L","100.00","110","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001565031960","","");
INSERT INTO product VALUES("546","ZAMGOLD C/OIL 2.5LTRS","ZAMGOLD C/OIL 2.5LTRS","49.00","59","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009706160517","","");
INSERT INTO product VALUES("547","WHITE VINKUBALA","WHITE VINKUBALA","55.00","65","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","2001420000000","","");
INSERT INTO product VALUES("548","CONTINENTAL B/FAST 12.5KG","CONTINENTAL B/FAST 12.5KG","48.00","52","","68","10","1","0","16","","2019-10-05 23:47:32","6","5","125","","");
INSERT INTO product VALUES("549","CONTINENTAL ROLLER MEAL  12.5 KG","CONTINENTAL ROLLER MEAL  12.5 KG","32.00","36","","68","10","1","0","16","","2019-10-05 23:47:32","6","5","12","","");
INSERT INTO product VALUES("550","GOLDEN SOYA PIECES BEEF 90G","GOLDEN SOYA PIECES BEEF 90G","2.35","3.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009670770521","","");
INSERT INTO product VALUES("551","CONTINENTAL  BREAKFAST 10KG","CONTINENTAL  BREAKFAST 10KG","41.00","47","","68","10","1","0","16","","2019-10-05 23:47:32","6","5","6001983112234","","");
INSERT INTO product VALUES("552","CONTINENTAL S/FINE B/FAST 25KG","CONTINENTAL S/FINE B/FAST 25KG","96.00","105","","68","10","1","0","16","","2019-10-05 23:47:32","6","5","6001983110445","","");
INSERT INTO product VALUES("553","NESTLE NIDO MILK POWD 400G","NESTLE NIDO MILK POWD 400G","48.00","61","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001068705504","","");
INSERT INTO product VALUES("554","NESTLE NIDO400G","NESTLE NIDO400G","42.50","60","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","8717896006642","","");
INSERT INTO product VALUES("555","NESTLE CREMORA 1KG","NESTLE CREMORA 1KG","26.00","32","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001308204316","","");
INSERT INTO product VALUES("556","BIG TREE BAKING POWDER 100G","BIG TREE BAKING POWDER 100G","2.80","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537014683","","");
INSERT INTO product VALUES("557","CREAM IT 1KG","CREAM IT 1KG","26.00","35","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644996322","","");
INSERT INTO product VALUES("558","BIG TREE 12G","BIG TREE 12G","0.90","1.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644995776","","");
INSERT INTO product VALUES("559","NESTLE CREMORA 1KG","NESTLE CREMORA 1KG","29.00","35","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","29*6","","");
INSERT INTO product VALUES("560","CREMORA ORIGINAL 1KG","CREMORA ORIGINAL 1KG","26.00","32","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009188000899","","");
INSERT INTO product VALUES("561","NESTLE LACTOGEN 1 400G","NESTLE LACTOGEN 1 400G","33.33","46","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001068580002","","");
INSERT INTO product VALUES("562","CB FULL CREAM 500G","CB FULL CREAM 500G","4.50","8","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672043","","");
INSERT INTO product VALUES("563","KOFFE MORR 20G","KOFFE MORR 20G","2.50","3.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537015185","","");
INSERT INTO product VALUES("564","CREMBOS CREAM95G","CREMBOS CREAM95G","3.00","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644927746","","");
INSERT INTO product VALUES("565","NICE SALT 2KG","NICE SALT 2KG","6.00","9","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","792","","");
INSERT INTO product VALUES("566","GOLDEN SO/P/CHICKEN  90G","GOLDEN SO/P/CHICKEN  90G","1.60","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","779","","");
INSERT INTO product VALUES("567","BEST SALT500G","BEST SALT500G","1.88","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009697005293","","");
INSERT INTO product VALUES("568","KELLOGY'S CORN FLAKES 750G","KELLOGY'S CORN FLAKES 750G","32.57","42","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001306001009","","");
INSERT INTO product VALUES("569","LAYS ONION AND CHEESE 36G","LAYS ONION AND CHEESE 36G","2.92","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009510804591","","");
INSERT INTO product VALUES("570","A/FRESH H 100ML","A/FRESH H 100ML","8.30","12","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001076001032","","");
INSERT INTO product VALUES("571","AMIGO POPICO BUTTER SALT","AMIGO POPICO BUTTER SALT","1.56","2.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652784140","","");
INSERT INTO product VALUES("572","KELLOG'S CORN FLAKES 1KG","KELLOG'S CORN FLAKES 1KG","41.25","48","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001306002341","","");
INSERT INTO product VALUES("573","COLGATE TRIPLE ACTION 70G","COLGATE TRIPLE ACTION 70G","5.21","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6920354817847","","");
INSERT INTO product VALUES("574","AQUAFRESH F/MINTY 50G","AQUAFRESH F/MINTY 50G","4.16","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001076001018","","");
INSERT INTO product VALUES("575","EEZEE CHICK FLA 75G","EEZEE CHICK FLA 75G","2.90","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009815040014","","");
INSERT INTO product VALUES("576","HERPIC MOUNTAIN 500ML","HERPIC MOUNTAIN 500ML","9.00","16.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001106102940","","");
INSERT INTO product VALUES("577","DOOM SUPER  MULTI INSECT 180ML","DOOM SUPER  MULTI INSECT 180ML","9.67","13.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001206413742","","");
INSERT INTO product VALUES("578","BIG 1 CANDLES","BIG 1 CANDLES","5.60","8.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644921737","","");
INSERT INTO product VALUES("579","FIVE ROSES 52s 125G","FIVE ROSES 52s 125G","17.50","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","794","","");
INSERT INTO product VALUES("580","LAYS SALTED 105G","LAYS SALTED 105G","9.10","13.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510806168","","");
INSERT INTO product VALUES("581","CASSAVA MEAL","CASSAVA MEAL","12.00","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","2001450000000","","");
INSERT INTO product VALUES("582","RAID I/KILLER LAVENDER 300ML","RAID I/KILLER LAVENDER 300ML","11.17","16","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001298600051","","");
INSERT INTO product VALUES("583","LAYS ITALIAN 36G","LAYS ITALIAN 36G","2.40","4","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510808216","","");
INSERT INTO product VALUES("584","MORTEIN I/ K 300ML","MORTEIN I/ K 300ML","13.60","19","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001106103859","","");
INSERT INTO product VALUES("585","NICE SALT 500G","NICE SALT 500G","1.50","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","791","","");
INSERT INTO product VALUES("586","CHEESE TWIRLS 110G","CHEESE TWIRLS 110G","7.14","11","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009510805987","","");
INSERT INTO product VALUES("587","POP SWEETNSALTY150g","POP SWEETNSALTY150g","6.67","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652784362","","");
INSERT INTO product VALUES("588","POPICO CHEESE FLAV 150g","POPICO CHEESE FLAV 150g","6.67","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652784164","","");
INSERT INTO product VALUES("589","CB MAYO 250G","CB MAYO 250G","10.83","15.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009522301248","","");
INSERT INTO product VALUES("590","BEST SALT1KG","BEST SALT1KG","3.75","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009697005415","","");
INSERT INTO product VALUES("591","ANU ANU PEANUT BUTTER","ANU ANU PEANUT BUTTER","6.20","9","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","5012727902968","","");
INSERT INTO product VALUES("592","STEAK NCHOPS 160","STEAK NCHOPS 160","15.12","23.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009001006053","","");
INSERT INTO product VALUES("593","HEINZ BEANS 410G","HEINZ BEANS 410G","9.06","13","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001030003430","","");
INSERT INTO product VALUES("594","MR MIN LAVENDER 300ML","MR MIN LAVENDER 300ML","17.90","24.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001106224240","","");
INSERT INTO product VALUES("595","POPICO SOUR150g","POPICO SOUR150g","7.30","10.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652784188","","");
INSERT INTO product VALUES("596","LUCKY STAR SARDINES 425G","LUCKY STAR SARDINES 425G","14.50","21","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001115002860","","");
INSERT INTO product VALUES("597","BEST SALT 200G","BEST SALT 200G","1.00","1.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001001","","");
INSERT INTO product VALUES("598","POPICO B/S150g","POPICO B/S150g","6.67","10.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009652784171","","");
INSERT INTO product VALUES("599","KOO B/ B IN T/SAUCE 410G","KOO B/ B IN T/SAUCE 410G","8.04","12","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009522300586","","");
INSERT INTO product VALUES("600","PANADO ","PANADO ","0.26","1","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009647010254","","");
INSERT INTO product VALUES("601","MUSAS LEMON CREAM","MUSAS LEMON CREAM","2.08","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009630400024","","");
INSERT INTO product VALUES("602","NESCAFE RICOFY 100G","NESCAFE RICOFY 100G","12.00","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009188000349","","");
INSERT INTO product VALUES("603","KELLOG'S CORN FLAKES 500G","KELLOG'S CORN FLAKES 500G","23.71","29","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001306015501","","");
INSERT INTO product VALUES("604","POLONA MACARON  3KG","POLONA MACARON  3KG","22.95","33","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6008079005128","","");
INSERT INTO product VALUES("605","KATUJILA THAI RICE","KATUJILA THAI RICE","3.00","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","200001000000","","");
INSERT INTO product VALUES("606","FATTIS N MONIS SPAGHET 500G","FATTIS N MONIS SPAGHET 500G","13.16","17","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009522300098","","");
INSERT INTO product VALUES("607","BELLA MACARON 3KG","BELLA MACARON 3KG","20.00","29","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009802818268","","");
INSERT INTO product VALUES("608","CREAM CRACKERS","CREAM CRACKERS","7.60","11.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009704171492","","");
INSERT INTO product VALUES("609","TASTY SOYA NUGGETS 700g","TASTY SOYA NUGGETS 700g","7.00","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","614458820419","","");
INSERT INTO product VALUES("610","EEZEE SPICY CHICKEN FLAV 75G","EEZEE SPICY CHICKEN FLAV 75G","2.90","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6006110194860","","");
INSERT INTO product VALUES("611","EEZEE VEGETABLE FLAV 75G","EEZEE VEGETABLE FLAV 75G","2.90","5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009815040038","","");
INSERT INTO product VALUES("612","YA MUSHE BEEF NOODLES 60G","YA MUSHE BEEF NOODLES 60G","2.00","1.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6003165415126","","");
INSERT INTO product VALUES("613","YA MUSHE CHICKEN NOODLES 60G","YA MUSHE CHICKEN NOODLES 60G","2.00","1.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6003165415119","","");
INSERT INTO product VALUES("614","EL SPAGHET","EL SPAGHET","3.65","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6223001512530","","");
INSERT INTO product VALUES("615","EEZEE BEEF FLAV 75G","EEZEE BEEF FLAV 75G","2.90","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009815040045","","");
INSERT INTO product VALUES("616","DADA SPAGHETTI 400G","DADA SPAGHETTI 400G","5.00","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","8912388877117","","");
INSERT INTO product VALUES("617","MARIANA SPAGHETTI 400G","MARIANA SPAGHETTI 400G","4.00","6","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009819740699","","");
INSERT INTO product VALUES("618","BELA SPAGHETTI","BELA SPAGHETTI","3.90","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009808630314","","");
INSERT INTO product VALUES("619","VEGA NOODLES CHICKEN 65G","VEGA NOODLES CHICKEN 65G","3.10","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","88864","","");
INSERT INTO product VALUES("620","VEGA VEGETABLE 65G","VEGA VEGETABLE 65G","3.10","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","8886409570021","","");
INSERT INTO product VALUES("621","MARIANA SPAGHETTI 400G","MARIANA SPAGHETTI 400G","4.00","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6223001511649","","");
INSERT INTO product VALUES("622","DELICIA  PASTA 500G","DELICIA  PASTA 500G","5.05","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6221022225200","","");
INSERT INTO product VALUES("623","FATTIS N MONIS MACARONI 500G","FATTIS N MONIS MACARONI 500G","8.10","12","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009522300111","","");
INSERT INTO product VALUES("624","KATUJILA WHITE RICE 2KG","KATUJILA WHITE RICE 2KG","19.50","25","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009706164362","","");
INSERT INTO product VALUES("625","RIVONIA MANGO PICKLE 400G","RIVONIA MANGO PICKLE 400G","10.50","18","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009608440328","","");
INSERT INTO product VALUES("626","JUST CHILLI 250G","JUST CHILLI 250G","10.50","15","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001983112289","","");
INSERT INTO product VALUES("627","MONGU  RICE ","MONGU  RICE ","8.50","17","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","2000050000000","","");
INSERT INTO product VALUES("628","KATUNJILA THAI RICE 500G ","KATUNJILA THAI RICE 500G ","6.20","8","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","750","","");
INSERT INTO product VALUES("629","KATUNJILAS RICE 1KG","KATUNJILAS RICE 1KG","9.75","13","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009706164379","","");
INSERT INTO product VALUES("630","D LITE PARBOILED RICE 2KG","D LITE PARBOILED RICE 2KG","25.20","35","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001565029295","","");
INSERT INTO product VALUES("631","MARRIANA RICE 1KG","MARRIANA RICE 1KG","8.00","11.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009819740439","","");
INSERT INTO product VALUES("632","DLITE WHITE RICE 1KG","DLITE WHITE RICE 1KG","10.50","12.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001565030574","","");
INSERT INTO product VALUES("633","DLITE RICE WHITE RICE 2KG","DLITE RICE WHITE RICE 2KG","18.00","29","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001565030581","","");
INSERT INTO product VALUES("634","MARIANA WHITE RICE 500G","MARIANA WHITE RICE 500G","3.60","6.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","9504000181079","","");
INSERT INTO product VALUES("635","D'LITE  RICE 1KG","D'LITE  RICE 1KG","13.20","19","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001565029288","","");
INSERT INTO product VALUES("636","MARIANA WHITE RICE 2KG","MARIANA WHITE RICE 2KG","14.50","25.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009819740446","","");
INSERT INTO product VALUES("637","NAKONDE RICE ","NAKONDE RICE ","10.00","20","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","2006110000000","","");
INSERT INTO product VALUES("638","C/B MAYO 375G","C/B MAYO 375G","13.33","19","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009522301224","","");
INSERT INTO product VALUES("639","CROSSE & B/WELL  MAYONNAISE 750G","CROSSE & B/WELL  MAYONNAISE 750G","19.17","27","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009522301262","","");
INSERT INTO product VALUES("640","NOLA MAYONNAISE750G","NOLA MAYONNAISE750G","16.00","23","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001069601744","","");
INSERT INTO product VALUES("641","FINE SALT 2KG","FINE SALT 2KG","12.00","15","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009706165826","","");
INSERT INTO product VALUES("642","KATUNJILAS SALT 2KG","KATUNJILAS SALT 2KG","6.50","15","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009681260165","","");
INSERT INTO product VALUES("643","DADDIES GARLIC SAUCE375ML","DADDIES GARLIC SAUCE375ML","12.00","17.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","20001","","");
INSERT INTO product VALUES("644","CEREBOS SALT 500G","CEREBOS SALT 500G","7.56","17","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001021021023","","");
INSERT INTO product VALUES("645","BEST SALT 2KG","BEST SALT 2KG","7.50","11","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009697005422","","");
INSERT INTO product VALUES("646","MUMMYS TOM/SAUCE 2LTR","MUMMYS TOM/SAUCE 2LTR","5.00","29.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009537015567","","");
INSERT INTO product VALUES("647","ALL GOLD TOMATO SAUCE 700ML","ALL GOLD TOMATO SAUCE 700ML","22.00","30","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","60019578","","");
INSERT INTO product VALUES("648","TOMATO SAUCE 375ML","TOMATO SAUCE 375ML","8.20","12","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009001005","","");
INSERT INTO product VALUES("649","DADA TOMATO PASTE 70G","DADA TOMATO PASTE 70G","1.00","2.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6903646516659","","");
INSERT INTO product VALUES("650","PURE GOLD BBQ SAUCE 375ML","PURE GOLD BBQ SAUCE 375ML","5.00","9","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","700371562960","","");
INSERT INTO product VALUES("651","RIVONIA BBQ SAUCE 375ML","RIVONIA BBQ SAUCE 375ML","10.50","15","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009608440434","","");
INSERT INTO product VALUES("652","RIVONIA G/TOM CHUTNEY 375ML","RIVONIA G/TOM CHUTNEY 375ML","11.00","18","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009608440168","","");
INSERT INTO product VALUES("653","RIVONIA TOMATO KETCHUP 375ML","RIVONIA TOMATO KETCHUP 375ML","11.00","16","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009608440090","","");
INSERT INTO product VALUES("654","DADDIES GARLIC SAUCE 375ML","DADDIES GARLIC SAUCE 375ML","8.75","17.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537015413","","");
INSERT INTO product VALUES("655","CHEEKY CHILLI PERI PERI 200ML","CHEEKY CHILLI PERI PERI 200ML","9.83","14.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6004809000043","","");
INSERT INTO product VALUES("656","VEG TOM PAS 70G","VEG TOM PAS 70G","3.20","5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","8886409507393","","");
INSERT INTO product VALUES("657","FALLSWAY  TOMATO SAUCE 375G","FALLSWAY  TOMATO SAUCE 375G","11.00","18","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","700083155160","","");
INSERT INTO product VALUES("658","PURE GOLD HOT/T/SAUCE 375ML","PURE GOLD HOT/T/SAUCE 375ML","5.00","9","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","700371562953","","");
INSERT INTO product VALUES("659","ALL GOLD TOMATO PASTE 50G","ALL GOLD TOMATO PASTE 50G","3.60","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001059982822","","");
INSERT INTO product VALUES("660","ROYCO MCHUZI MIX15G","ROYCO MCHUZI MIX15G","0.50","1.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6161100605503","","");
INSERT INTO product VALUES("661","CHEEKY CHILLI SAUCE 100ML","CHEEKY CHILLI SAUCE 100ML","7.00","10","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6004809000012","","");
INSERT INTO product VALUES("662","WELLINGTONS T/SAUCE 700ML","WELLINGTONS T/SAUCE 700ML","18.33","26.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001030003966","","");
INSERT INTO product VALUES("663","WELLING T/SAUCE375ML","WELLING T/SAUCE375ML","11.41","16.5","","68","9","1","0","21","","2019-10-09 12:48:25","6","5","6001030003997","","");
INSERT INTO product VALUES("664","PURE GOLD TOMATO SAUCE 375ML","PURE GOLD TOMATO SAUCE 375ML","5.00","9","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","700371562977","","");
INSERT INTO product VALUES("665","ALL GOLD TOMATO SAUCE 350ML","ALL GOLD TOMATO SAUCE 350ML","11.66","15.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","60019585","","");
INSERT INTO product VALUES("666","VEGTOM PAS 400G","VEGTOM PAS 400G","9.00","17","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","8886409507492","","");
INSERT INTO product VALUES("667","RIVONIA HUNTERS SAUCE 375ML","RIVONIA HUNTERS SAUCE 375ML","11.00","15","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009608440403","","");
INSERT INTO product VALUES("668","HOT CHILLI N GARLIC SAUCE 375","HOT CHILLI N GARLIC SAUCE 375","11.00","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009608440076","","");
INSERT INTO product VALUES("669","RIVONIA S/CHILI SAUCE 375ML","RIVONIA S/CHILI SAUCE 375ML","11.00","18","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009608440366","","");
INSERT INTO product VALUES("670","DADDIES CHILLI SAUCE 375ML","DADDIES CHILLI SAUCE 375ML","8.33","15","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644928019","","");
INSERT INTO product VALUES("671","MISTER CHILL PERI PERI  110ML","MISTER CHILL PERI PERI  110ML","4.33","6.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644928163","","");
INSERT INTO product VALUES("672","RIVONIA HOT CHILLI SAUCE","RIVONIA HOT CHILLI SAUCE","8.70","12.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009608440083","","");
INSERT INTO product VALUES("673","RIVONIA TOMATO SAUCE 375ML","RIVONIA TOMATO SAUCE 375ML","8.80","12.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009608440069","","");
INSERT INTO product VALUES("674","DADDIES TOMATO KETCHUP 375ML","DADDIES TOMATO KETCHUP 375ML","8.33","13.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644928002","","");
INSERT INTO product VALUES("675","KNOOR SOUP 75G","KNOOR SOUP 75G","4.50","11","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001038072704","","");
INSERT INTO product VALUES("676","MIN SOUP ","MIN SOUP ","5.00","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001087359597","","");
INSERT INTO product VALUES("677","AROMAT CHILLI BEEF 15G","AROMAT CHILLI BEEF 15G","3.00","2.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009001001577","","");
INSERT INTO product VALUES("678","KNORR THICK VEG SOUP 50G","KNORR THICK VEG SOUP 50G","5.00","7","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001087359573","","");
INSERT INTO product VALUES("679","AROMAT  ORIGINAL","AROMAT  ORIGINAL","4.00","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001038072858","","");
INSERT INTO product VALUES("680","RICH OXTAIL SOUP","RICH OXTAIL SOUP","5.00","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001087359610","","");
INSERT INTO product VALUES("681","KNORR CREAM CHICKEN 50G","KNORR CREAM CHICKEN 50G","5.00","7","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001038002152","","");
INSERT INTO product VALUES("682","KNR W SOUP 50G","KNR W SOUP 50G","4.50","7","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001087359559","","");
INSERT INTO product VALUES("683","KNORR CREAM OF TOMATO 50G","KNORR CREAM OF TOMATO 50G","2.90","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001087353144","","");
INSERT INTO product VALUES("684","KNOR CHIC NOD ","KNOR CHIC NOD ","2.90","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001038001551","","");
INSERT INTO product VALUES("685","ROBERTSONSGOLD CRISPY RAJAH 200G","ROBERTSONSGOLD CRISPY RAJAH 200G","18.00","22","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009001006138","","");
INSERT INTO product VALUES("686","ROB JIKELELE SISHEBO MIX 100G","ROB JIKELELE SISHEBO MIX 100G","8.00","14.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009001005490","","");
INSERT INTO product VALUES("687","ROBERTSONS GOLD CRISPY 200G","ROBERTSONS GOLD CRISPY 200G","16.00","22","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009001006114","","");
INSERT INTO product VALUES("688","ROBERTSONS CRISPY  200G","ROBERTSONS CRISPY  200G","16.00","22","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009001006152","","");
INSERT INTO product VALUES("689","ONGA SPICE 10G","ONGA SPICE 10G","0.62","1.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6008155009705","","");
INSERT INTO product VALUES("690","ROB. RAJAH ALL IN ONE 50G","ROB. RAJAH ALL IN ONE 50G","5.00","8.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009001015802","","");
INSERT INTO product VALUES("691","EVERPLUS MILDSPICE 50G","EVERPLUS MILDSPICE 50G","3.00","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","8908008974106","","");
INSERT INTO product VALUES("692","REAL SPICY MILD & SPICY 8G","REAL SPICY MILD & SPICY 8G","1.75","2.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","8908008974083","","");
INSERT INTO product VALUES("693","MADRAS CURRY POWDER 50G","MADRAS CURRY POWDER 50G","2.00","6","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001010425733","","");
INSERT INTO product VALUES("694","ONGA SPICY BEEF  13G","ONGA SPICY BEEF  13G","0.70","1.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6008155008524","","");
INSERT INTO product VALUES("695","IMANA chichen FLAV","IMANA chichen FLAV","0.50","2","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6002657510301","","");
INSERT INTO product VALUES("696","VEG MCHUZI SPICE","VEG MCHUZI SPICE","1.00","2","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","8908008974007","","");
INSERT INTO product VALUES("697","CHICKEN MCHUZI SPICE 15G","CHICKEN MCHUZI SPICE 15G","1.00","2","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","8908008974182","","");
INSERT INTO product VALUES("698","ROB CHICKEN SPICE 84G","ROB CHICKEN SPICE 84G","12.40","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001038285005","","");
INSERT INTO product VALUES("699","ROBERTSON BBQ SPICE 50G","ROBERTSON BBQ SPICE 50G","12.80","18.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001038284954","","");
INSERT INTO product VALUES("700","RAJAH MEDIUM CURRY POWDER 50G","RAJAH MEDIUM CURRY POWDER 50G","6.50","9.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001038201159","","");
INSERT INTO product VALUES("701","ROYCO MCHUZI BEEF 200G","ROYCO MCHUZI BEEF 200G","10.41","15","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6161100605442","","");
INSERT INTO product VALUES("702","EVERPLUS MILD CURRY 8G","EVERPLUS MILD CURRY 8G","0.43","1","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","8908008974045","","");
INSERT INTO product VALUES("703","ONGA SPICY BEEF  200G","ONGA SPICY BEEF  200G","12.50","18","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6008155000665","","");
INSERT INTO product VALUES("704","RAJAH MEDIUM CURRY POWR 100G","RAJAH MEDIUM CURRY POWR 100G","9.25","13.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001038201555","","");
INSERT INTO product VALUES("705","ROYCO MCHUZI BEEF MIX 500G","ROYCO MCHUZI BEEF MIX 500G","25.00","35","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6161100605459","","");
INSERT INTO product VALUES("706","STEAK N CHOPS 80G","STEAK N CHOPS 80G","13.00","18.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001038285050","","");
INSERT INTO product VALUES("707","ROB PORTUGUESE CHICKEN  75G","ROB PORTUGUESE CHICKEN  75G","14.90","21.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009001005896","","");
INSERT INTO product VALUES("708","HINDS PERI PERI 7.5G","HINDS PERI PERI 7.5G","2.00","4","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001010336305","","");
INSERT INTO product VALUES("709","ROB SPICE FOR RICE 7G","ROB SPICE FOR RICE 7G","1.63","2.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001038256302","","");
INSERT INTO product VALUES("710","BISTO ORIGINAL C/POWDER 250G","BISTO ORIGINAL C/POWDER 250G","11.25","16.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001021031022","","");
INSERT INTO product VALUES("711","KNORROX BEEF S/CUBES 120G","KNORROX BEEF S/CUBES 120G","7.95","11.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001038062453","","");
INSERT INTO product VALUES("712","MISTER  CHILLI PERI PERI 220ML","MISTER  CHILLI PERI PERI 220ML","6.60","9.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537015390","","");
INSERT INTO product VALUES("713","IMANA BEEF FLAV. CUBES","IMANA BEEF FLAV. CUBES","0.50","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6002657500012","","");
INSERT INTO product VALUES("714","RAJAH FLAV. N  MILD 200G","RAJAH FLAV. N  MILD 200G","14.50","23.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001087357739","","");
INSERT INTO product VALUES("715","ROB CHICKEN SPICY 168G","ROB CHICKEN SPICY 168G","15.12","23.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009001005971","","");
INSERT INTO product VALUES("716","ROB MILD N SPICY C/POWDER 50G","ROB MILD N SPICY C/POWDER 50G","6.50","9.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001038203153","","");
INSERT INTO product VALUES("717","ROB PORTUGUESE CHICKEN  7G","ROB PORTUGUESE CHICKEN  7G","1.63","2.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001087303651","","");
INSERT INTO product VALUES("718","BEEF MCHUZI SPICE 75G","BEEF MCHUZI SPICE 75G","3.50","6.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","8908008974236","","");
INSERT INTO product VALUES("719","RAJAH FLAV/MILD CURRY /P 50G","RAJAH FLAV/MILD CURRY /P 50G","4.19","8.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001087357715","","");
INSERT INTO product VALUES("720","REAL SPICE MCHUZI MIX STEW 15G","REAL SPICE MCHUZI MIX STEW 15G","1.75","2.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","8908008974229","","");
INSERT INTO product VALUES("721","RAJAH ALL IN ONE 100G","RAJAH ALL IN ONE 100G","11.50","16.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009001015864","","");
INSERT INTO product VALUES("722","EVERPLUS HOT 15G","EVERPLUS HOT 15G","0.60","1","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","8908008974151","","");
INSERT INTO product VALUES("723","RAJAH HOT CURRY 50G","RAJAH HOT CURRY 50G","5.50","8","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001038204150","","");
INSERT INTO product VALUES("724","RAJAH  MILD SPICY 100G","RAJAH  MILD SPICY 100G","11.40","16.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001038203252","","");
INSERT INTO product VALUES("725","RAJAH MILD SPICY 200G","RAJAH MILD SPICY 200G","16.34","23.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001038203351","","");
INSERT INTO product VALUES("726","REAL SPICE  MILD  50G","REAL SPICE  MILD  50G","6.00","9.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","8908008974069","","");
INSERT INTO product VALUES("727","ROYCO MCHUZI MIX CHICKEN 200G","ROYCO MCHUZI MIX CHICKEN 200G","10.41","15","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6161100605534","","");
INSERT INTO product VALUES("728","CHICKEN MCHUZI SPICE 10G","CHICKEN MCHUZI SPICE 10G","0.54","1.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6162006601484","","");
INSERT INTO product VALUES("729","IMANA BEEF FLAV. CUBES 6","IMANA BEEF FLAV. CUBES 6","3.50","7","","68","9","1","0","21","","2019-10-08 08:13:05","6","5","6002657500111","","");
INSERT INTO product VALUES("730","IMANA BEEF FLAV. CUBES","IMANA BEEF FLAV. CUBES","4.00","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6002657500142","","");
INSERT INTO product VALUES("731","IMANA CHIC FLAV. CUBES","IMANA CHIC FLAV. CUBES","0.50","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6002657500043","","");
INSERT INTO product VALUES("732","BISTO GRAVY POWDER 125G","BISTO GRAVY POWDER 125G","6.25","9","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6001021031015","","");
INSERT INTO product VALUES("733","ROB BBQ SPICY 7G","ROB BBQ SPICY 7G","1.62","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001038256104","","");
INSERT INTO product VALUES("734","ROB STEAK N CHOP 7G","ROB STEAK N CHOP 7G","1.63","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001038256005","","");
INSERT INTO product VALUES("735","ROB SPICY CHICKEN 7G","ROB SPICY CHICKEN 7G","1.62","3","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001038256050","","");
INSERT INTO product VALUES("736","G/L P/NUT 310G","G/L P/NUT 310G","8.80","13","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009706161477","","");
INSERT INTO product VALUES("737","RIVONIA FLAVRD JAM 500G","RIVONIA FLAVRD JAM 500G","9.50","12","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009608440410","","");
INSERT INTO product VALUES("738","GLADE SECRETS 180ML","GLADE SECRETS 180ML","10.00","13.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001298988883","","");
INSERT INTO product VALUES("739","PARMALAT FULL CREAM CHEESE200G","PARMALAT FULL CREAM CHEESE200G","23.00","30","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001049038133","","");
INSERT INTO product VALUES("740","PARMALT  CHEDDA CHEES 200G","PARMALT  CHEDDA CHEES 200G","23.00","30","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001049038140","","");
INSERT INTO product VALUES("741","ILLOVO CARAMEL SYRUP 500G","ILLOVO CARAMEL SYRUP 500G","45.00","54","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001241006213","","");
INSERT INTO product VALUES("742","FRSHPIKT PEANUT BUTTER 310G","FRSHPIKT PEANUT BUTTER 310G","8.80","13","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009815600058","","");
INSERT INTO product VALUES("743","RIVONIA LEMON MARMALADE 500G","RIVONIA LEMON MARMALADE 500G","0.00","0","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009608440236","","");
INSERT INTO product VALUES("744","RIVONIA MANGO JAM 500G","RIVONIA MANGO JAM 500G","10.40","14.99","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009608440250","","");
INSERT INTO product VALUES("745","HEALING HONEY 500G","HEALING HONEY 500G","20.58","27","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009701982411","","");
INSERT INTO product VALUES("746","FRSHPIKT MIXED FRUIT JAM 500G","FRSHPIKT MIXED FRUIT JAM 500G","11.00","16","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009815600409","","");
INSERT INTO product VALUES("747","RIVONIA MIXED FRUIT JAM 500G","RIVONIA MIXED FRUIT JAM 500G","14.40","21","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009608440274","","");
INSERT INTO product VALUES("748","BLUE BAND MARGARINE 250G","BLUE BAND MARGARINE 250G","8.83","12.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6161100602267","","");
INSERT INTO product VALUES("749","NUT FRESH PEANUT BUTTER 400G","NUT FRESH PEANUT BUTTER 400G","11.50","15.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803247012","","");
INSERT INTO product VALUES("750","BLUE BAND MARGARINE 500G","BLUE BAND MARGARINE 500G","15.50","15.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6161100600157","","");
INSERT INTO product VALUES("751","EVERY DAY  S/P BUTTER 1KG","EVERY DAY  S/P BUTTER 1KG","19.60","26.9","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","705632553978","","");
INSERT INTO product VALUES("752","GOLDEN LEAVES P/BUTTER 310G","GOLDEN LEAVES P/BUTTER 310G","7.30","9.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009706161484","","");
INSERT INTO product VALUES("753","GOLD BAND 100G","GOLD BAND 100G","3.50","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","161107773007","","");
INSERT INTO product VALUES("754","FRESHPIKT P/B/E/CRUNCHY 1KG ","FRESHPIKT P/B/E/CRUNCHY 1KG ","22.17","29","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009815600263","","");
INSERT INTO product VALUES("755","FRESHPIKT  P/B/SMOOTH 1KG","FRESHPIKT  P/B/SMOOTH 1KG","21.60","29","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009815600034","","");
INSERT INTO product VALUES("756","POWER FINE PEANUT BUTTER 1KG","POWER FINE PEANUT BUTTER 1KG","16.50","24","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009609699565","","");
INSERT INTO product VALUES("757","COLGATE 100ML","COLGATE 100ML","7.08","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001067066613","","");
INSERT INTO product VALUES("758","RIVONIA ORANGE MARMALADE 500G","RIVONIA ORANGE MARMALADE 500G","11.30","14.95","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009608440205","","");
INSERT INTO product VALUES("759","COLGATE HERBAL 50 MLS","COLGATE HERBAL 50 MLS","5.41","8","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001067021605","","");
INSERT INTO product VALUES("760","GOLD BAND MARGARINE 500G","GOLD BAND MARGARINE 500G","14.16","20.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6161107773021","","");
INSERT INTO product VALUES("761","ZAMBIAN GOLD HONEY 500G ","ZAMBIAN GOLD HONEY 500G ","29.17","37","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009819220061","","");
INSERT INTO product VALUES("762","COLGATE 75G","COLGATE 75G","4.17","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001067103721","","");
INSERT INTO product VALUES("763","GOLDEN LEAVES S/P BUTTER 1KG","GOLDEN LEAVES S/P BUTTER 1KG","19.60","27","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009706161460","","");
INSERT INTO product VALUES("764","GOLD BAND MARGARINE 250G","GOLD BAND MARGARINE 250G","8.75","12.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6161107773014","","");
INSERT INTO product VALUES("765","ZAMBEZI GOLD 375G","ZAMBEZI GOLD 375G","25.00","32.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009819220054","","");
INSERT INTO product VALUES("766","BEST PEANUT BUTTER 320G","BEST PEANUT BUTTER 320G","6.30","8.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009809430906","","");
INSERT INTO product VALUES("767"," BROWN SUGAR 1KG"," BROWN SUGAR 1KG","12.10","15","","68","2385","1","0","19","","2019-10-28 08:05:14","6","5","6009670890014","","");
INSERT INTO product VALUES("768","WHITESPOON SUGAR 2KG","WHITESPOON SUGAR 2KG","25.00","28","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009670892032","","");
INSERT INTO product VALUES("769","BIGTREE ICING SUGAR","BIGTREE ICING SUGAR","9.00","13","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644924806","","");
INSERT INTO product VALUES("770","WHITESPOON SUGAR 1KG","WHITESPOON SUGAR 1KG","12.75","16","","68","790","1","0","19","","2019-10-27 08:19:25","6","5","6009670892025","","");
INSERT INTO product VALUES("771","WHITESPOON SUGAR 500G","WHITESPOON SUGAR 500G","6.45","8","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009670892001","","");
INSERT INTO product VALUES("772","BAXTON STAR POPS EA","BAXTON STAR POPS EA","0.70","1","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6002553005468","","");
INSERT INTO product VALUES("773","ECLAIRS","ECLAIRS","0.30","0.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","8906066680281","","");
INSERT INTO product VALUES("774","BUBBLE GUM ROLLS","BUBBLE GUM ROLLS","0.63","1","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644999224","","");
INSERT INTO product VALUES("775","Endermint","Endermint","0.27","0.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001065602011","","");
INSERT INTO product VALUES("776","Roll magic","Roll magic","0.83","1.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644996957","","");
INSERT INTO product VALUES("777","LUNCH BAR","LUNCH BAR","2.50","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001065033860","","");
INSERT INTO product VALUES("778","ECLAIRS","ECLAIRS","0.30","0","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","788","","");
INSERT INTO product VALUES("779"," ECLAIRS/TOFFEES"," ECLAIRS/TOFFEES","0.16","0.5","","68","322","1","0","21","","2019-10-28 13:56:29","6","5","6009644921287","","");
INSERT INTO product VALUES("780","BUBBLEGUM ROLLS","BUBBLEGUM ROLLS","0.63","1","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644999194","","");
INSERT INTO product VALUES("781","Roll magic ","Roll magic ","0.83","1.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644999484","","");
INSERT INTO product VALUES("782","RIVONIA WHITE VINEGAR 1LTR","RIVONIA WHITE VINEGAR 1LTR","6.33","9","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009608440021","","");
INSERT INTO product VALUES("783","WHITE VINEGER 375MLS RIV","WHITE VINEGER 375MLS RIV","5.70","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009608440588","","");
INSERT INTO product VALUES("784","MAX WHITE VINEGAR","MAX WHITE VINEGAR","3.00","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001003","","");
INSERT INTO product VALUES("785","DADDIES WHITE VINEGAR750ML","DADDIES WHITE VINEGAR750ML","4.20","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644928057","","");
INSERT INTO product VALUES("786","MAX BROWN VINEGAR","MAX BROWN VINEGAR","3.00","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001002","","");
INSERT INTO product VALUES("787","DADDIES WHITE VINEGAR 1L","DADDIES WHITE VINEGAR 1L","4.67","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644928064","","");
INSERT INTO product VALUES("788","DADDIES SWEET CHILLI 375ML","DADDIES SWEET CHILLI 375ML","8.33","15","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644928170","","");
INSERT INTO product VALUES("789","DADDIES BROWN VINEGAR 750ML ","DADDIES BROWN VINEGAR 750ML ","3.33","5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644928347","","");
INSERT INTO product VALUES("790","RIVONIA BROWN VINEGAR 1L","RIVONIA BROWN VINEGAR 1L","6.23","9","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009608440038","","");
INSERT INTO product VALUES("791","BUTTER CUP MARGERINE 500G","BUTTER CUP MARGERINE 500G","21.25","29","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009603061306","","");
INSERT INTO product VALUES("792","LUCKY STAR IN HOT 400G","LUCKY STAR IN HOT 400G","10.00","15","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6001115001542","","");
INSERT INTO product VALUES("793","C\BELL MILK MAHEU 300G","C\BELL MILK MAHEU 300G","2.91","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672463","","");
INSERT INTO product VALUES("794","SHAKE N SIP B/ FLAV 500ML","SHAKE N SIP B/ FLAV 500ML","3.92","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644999132","","");
INSERT INTO product VALUES("795","MIXED BERRY FLAV 500ML","MIXED BERRY FLAV 500ML","3.92","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644999118","","");
INSERT INTO product VALUES("796","SUPER SHAKE MANGO 500ML","SUPER SHAKE MANGO 500ML","3.83","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009823800501","","");
INSERT INTO product VALUES("797","SUPER SHAKE PINEAPPLE 500ML","SUPER SHAKE PINEAPPLE 500ML","3.83","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644921317","","");
INSERT INTO product VALUES("798","SHAKE N SIP BANA/STRAW  500ML","SHAKE N SIP BANA/STRAW  500ML","3.75","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537015970","","");
INSERT INTO product VALUES("799","SHAKE 'N SIP BANA/STRAW 500ML","SHAKE 'N SIP BANA/STRAW 500ML","5.00","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537015765","","");
INSERT INTO product VALUES("800","S/SHAKE B/STR 500ML","S/SHAKE B/STR 500ML","5.25","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644921492","","");
INSERT INTO product VALUES("801","SHAKE N SIP PINEAPPLE250ML","SHAKE N SIP PINEAPPLE250ML","2.50","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009537019510","","");
INSERT INTO product VALUES("802","SHAKEN SIP PINE/FLAV 250ML","SHAKEN SIP PINE/FLAV 250ML","3.80","5.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009537019503","","");
INSERT INTO product VALUES("803","S/SHAKE BAN/STRAW 300ML","S/SHAKE BAN/STRAW 300ML","3.50","5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644923854","","");
INSERT INTO product VALUES("804","C & M MUNKOYO 500ML","C & M MUNKOYO 500ML","4.00","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","782","","");
INSERT INTO product VALUES("805","SHAKERS STRAW MILK 500ML","SHAKERS STRAW MILK 500ML","5.25","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672630","","");
INSERT INTO product VALUES("806","SHAKERS BAN MILK SHAKE 500ML","SHAKERS BAN MILK SHAKE 500ML","5.25","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672623","","");
INSERT INTO product VALUES("807","C/B M /B/CREAM/400G","C/B M /B/CREAM/400G","2.91","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672821","","");
INSERT INTO product VALUES("808","CREAMBELL MAHEU/BANA/400G","CREAMBELL MAHEU/BANA/400G","2.91","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672838","","");
INSERT INTO product VALUES("809","CREAMBELL MAHEU/STRAW/400G","CREAMBELL MAHEU/STRAW/400G","2.92","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672845","","");
INSERT INTO product VALUES("810","SHAKE N SIP PINE/FLAV 500ML","SHAKE N SIP PINE/FLAV 500ML","3.75","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644999125","","");
INSERT INTO product VALUES("811","AMA SIP SIP BANANA 470ML","AMA SIP SIP BANANA 470ML","3.40","5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009537019435","","");
INSERT INTO product VALUES("812","CHIBWANTU 500ML","CHIBWANTU 500ML","4.00","6","","68","10","1","0","13","","2019-10-05 23:47:32","6","5","6061103197138","","");
INSERT INTO product VALUES("813","SHAKE N SIP BANANA FLAV 250ML","SHAKE N SIP BANANA FLAV 250ML","2.50","4","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009537019497","","");
INSERT INTO product VALUES("814","SHAKE N SIP BAN/STRAW 250ML","SHAKE N SIP BAN/STRAW 250ML","2.50","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009537015758","","");
INSERT INTO product VALUES("815","BLUEBAND MAG 100G","BLUEBAND MAG 100G","4.60","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","61600317","","");
INSERT INTO product VALUES("816","BUTTER CUP MARGARINE 250G","BUTTER CUP MARGARINE 250G","10.42","15","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009603060408","","");
INSERT INTO product VALUES("817","PRESTIGE MA/250G","PRESTIGE MA/250G","9.10","13","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6161101668293","","");
INSERT INTO product VALUES("818","PREST MAG 500G","PREST MAG 500G","16.50","24","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6161101668309","","");
INSERT INTO product VALUES("819","PRESTIGE MARGARINE 100G","PRESTIGE MARGARINE 100G","3.10","2","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6161101661423","","");
INSERT INTO product VALUES("820","PRESTIGE  MARGARINE  500g","PRESTIGE  MARGARINE  500g","16.80","24","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6161101661447","","");
INSERT INTO product VALUES("821","PARMALAT F/CREAM MILKPOWDER 12G","PARMALAT F/CREAM MILKPOWDER 12G","1.20","2","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","4343","","");
INSERT INTO product VALUES("822","DENDAIRY LONG/LIFE MILK 250ML","DENDAIRY LONG/LIFE MILK 250ML","3.97","5.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009657651362","","");
INSERT INTO product VALUES("823","YO jOOS ORANGE FLAV 500ML","YO jOOS ORANGE FLAV 500ML","4.40","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009880507504","","");
INSERT INTO product VALUES("824","MILKIT BANANA FLAV 250ML","MILKIT BANANA FLAV 250ML","3.42","5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644997084","","");
INSERT INTO product VALUES("825","DENDAIRY FULL CREAM MILK 500ML","DENDAIRY FULL CREAM MILK 500ML","6.10","8","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009698811541","","");
INSERT INTO product VALUES("826","PARM MILK SATCHET 500ML","PARM MILK SATCHET 500ML","6.38","6.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683610012","","");
INSERT INTO product VALUES("827","MILKIT VANILA FLAV 250ML","MILKIT VANILA FLAV 250ML","3.80","5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644997114","","");
INSERT INTO product VALUES("828","FINTA SUPA MILK 250ML","FINTA SUPA MILK 250ML","3.20","4.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","60087331","","");
INSERT INTO product VALUES("829","FINTA SUPA MILK 500ML","FINTA SUPA MILK 500ML","4.54","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009601400008","","");
INSERT INTO product VALUES("830","CREAMBEL FRESH MILK 250ML","CREAMBEL FRESH MILK 250ML","0.00","4.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672029","","");
INSERT INTO product VALUES("831","MILKIT SHAKE  VANILA 500ML","MILKIT SHAKE  VANILA 500ML","6.50","10","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644997176","","");
INSERT INTO product VALUES("832","PARM FULL CREAM 1 LITRE","PARM FULL CREAM 1 LITRE","13.57","18","","68","9","1","0","21","","2019-10-18 08:13:14","6","5","6009683610562","","");
INSERT INTO product VALUES("833","FULL CREAM MILK500ML","FULL CREAM MILK500ML","5.40","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672012","","");
INSERT INTO product VALUES("834","CREAMBELL MILK/BANA/FLAV/300ML","CREAMBELL MILK/BANA/FLAV/300ML","4.00","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672081","","");
INSERT INTO product VALUES("835","CREAMBELL MILK/STRAW/FLAV/300ML","CREAMBELL MILK/STRAW/FLAV/300ML","4.00","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672104","","");
INSERT INTO product VALUES("836","DENDAIRY CHOC MILK SHAKE 500ML","DENDAIRY CHOC MILK SHAKE 500ML","9.60","16","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009698811510","","");
INSERT INTO product VALUES("837","CREAMBELL/MILK/CHO/FLAV 300ML","CREAMBELL/MILK/CHO/FLAV 300ML","4.00","6","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672111","","");
INSERT INTO product VALUES("838","PARMALAT  LACTO500ML","PARMALAT  LACTO500ML","7.05","8.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683611507","","");
INSERT INTO product VALUES("839","CAB ORANGE 500ML","CAB ORANGE 500ML","0.00","8.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683612009","","");
INSERT INTO product VALUES("840","CAB PINE 500ML","CAB PINE 500ML","6.05","8.5","","68","3","1","0","19","","2019-10-08 08:13:05","6","5","6009683612016","","");
INSERT INTO product VALUES("841","MILKIT CHOC FLAV 250ML","MILKIT CHOC FLAV 250ML","3.17","5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644997091","","");
INSERT INTO product VALUES("842","YO jOos YOG FRUITY 500ML","YO jOos YOG FRUITY 500ML","4.33","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009880507528","","");
INSERT INTO product VALUES("843","YO  jOOS TROPCAL FLAV 500ML","YO  jOOS TROPCAL FLAV 500ML","5.20","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009880507511","","");
INSERT INTO product VALUES("844","DENDAIRY FULL CREAM MILK 1L","DENDAIRY FULL CREAM MILK 1L","12.20","16","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009698811428","","");
INSERT INTO product VALUES("845","PARM LAT LONG LIFE MILK 250ML","PARM LAT LONG LIFE MILK 250ML","3.00","5.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683610500","","");
INSERT INTO product VALUES("846","BIGTREE INSTANT F/CREAM 400G","BIGTREE INSTANT F/CREAM 400G","32.50","35","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009644995899","","");
INSERT INTO product VALUES("847","CREAMBELL LACTO MABISI 500G","CREAMBELL LACTO MABISI 500G","5.70","7","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672159","","");
INSERT INTO product VALUES("848","MILKIT STRAWBERY 250ML","MILKIT STRAWBERY 250ML","3.42","5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009644997107","","");
INSERT INTO product VALUES("849","PARM FAT FREE 500ML","PARM FAT FREE 500ML","6.99","9.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683610531","","");
INSERT INTO product VALUES("850","PARM FULL CREAM 500ML","PARM FULL CREAM 500ML","6.99","9.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683610517","","");
INSERT INTO product VALUES("851","PARMALAT LOW FAT 1L","PARMALAT LOW FAT 1L","13.57","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683610579","","");
INSERT INTO product VALUES("852","PARM FATFREE MILK 1LTR","PARM FATFREE MILK 1LTR","13.57","18","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683610586","","");
INSERT INTO product VALUES("853","PARM MILK LOW FAT 500ML","PARM MILK LOW FAT 500ML","6.99","9.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683610524","","");
INSERT INTO product VALUES("854","DENDAIRY MANGO & ORANGE JUICE 500ML","DENDAIRY MANGO & ORANGE JUICE 500ML","7.48","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009698812050","","");
INSERT INTO product VALUES("855","EGGS UNIT (10)","EGGS UNIT (10)","8.90","10","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","1313","","");
INSERT INTO product VALUES("856","EGGS  EACH","EGGS  EACH","0.78","1","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","1314","","");
INSERT INTO product VALUES("857","EGGS TRAY","EGGS TRAY","25.00","30","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","1312","","");
INSERT INTO product VALUES("858","PARMA DRINK YOGSTRAW 1LT","PARMA DRINK YOGSTRAW 1LT","0.00","19","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683614775","","");
INSERT INTO product VALUES("859","PARMALAT NUTRAL LOWFAT YOG500G","PARMALAT NUTRAL LOWFAT YOG500G","11.74","16.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683612603","","");
INSERT INTO product VALUES("860","PARM APRICOT LOW FAT/YOG 175G","PARM APRICOT LOW FAT/YOG 175G","5.50","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683612719","","");
INSERT INTO product VALUES("861","PARM FRUIT OF CAPE YOG 175G","PARM FRUIT OF CAPE YOG 175G","5.50","6.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683612726","","");
INSERT INTO product VALUES("862","PARM  FRUIT SLD YOG 500G","PARM  FRUIT SLD YOG 500G","11.74","16.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683612535","","");
INSERT INTO product VALUES("863","PARM FRUIT YOG PEACH & MANGO 175G","PARM FRUIT YOG PEACH & MANGO 175G","5.50","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683612757","","");
INSERT INTO product VALUES("864","PARM L/ FAT YOG BAN/ FLAV 250G","PARM L/ FAT YOG BAN/ FLAV 250G","6.00","8.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683614515","","");
INSERT INTO product VALUES("865","PARM L/F P/F 250G","PARM L/F P/F 250G","4.47","8.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683614522","","");
INSERT INTO product VALUES("866","PARMALAT N/ L/FAT YOG 175G","PARMALAT N/ L/FAT YOG 175G","5.79","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683612610","","");
INSERT INTO product VALUES("867","PARM L/FAT YOG STRAW/FLAV 250G","PARM L/FAT YOG STRAW/FLAV 250G","6.00","8.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683614508","","");
INSERT INTO product VALUES("868","CREAMBELL YOG RAS/BLACK 250G","CREAMBELL YOG RAS/BLACK 250G","5.00","8","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672234","","");
INSERT INTO product VALUES("869","PARM F/S LOW FAT/YOG 175G","PARM F/S LOW FAT/YOG 175G","5.79","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683612436","","");
INSERT INTO product VALUES("870","CREAMBELL YOG PINEAPPLE 500G","CREAMBELL YOG PINEAPPLE 500G","12.50","17","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672340","","");
INSERT INTO product VALUES("871","CREAMBELL YOG D ORAGE 250G","CREAMBELL YOG D ORAGE 250G","5.20","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672258","","");
INSERT INTO product VALUES("872","PARM STRAW L/FAT YOG 175G","PARM STRAW L/FAT YOG 175G","5.50","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683612405","","");
INSERT INTO product VALUES("873","C/B GRAN 175G","C/B GRAN 175G","6.20","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672319","","");
INSERT INTO product VALUES("874","C/B STRAW THICK YOG 175G","C/B STRAW THICK YOG 175G","5.00","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672333","","");
INSERT INTO product VALUES("875","CREAMBELL YOG GRANA 500G","CREAMBELL YOG GRANA 500G","12.50","16.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672302","","");
INSERT INTO product VALUES("876","PARMALAT PINEAPPLE FLAV. 175G","PARMALAT PINEAPPLE FLAV. 175G","5.79","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683612429","","");
INSERT INTO product VALUES("877","PARM DRINK YOG TOFEE 250G","PARM DRINK YOG TOFEE 250G","6.00","8.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683614539","","");
INSERT INTO product VALUES("878","PARMALAT BANA/FLAV 500G","PARMALAT BANA/FLAV 500G","11.74","16.5","","68","10","1","0","15","","2019-10-05 23:47:32","6","5","6009683612511","","");
INSERT INTO product VALUES("879","CREAMBELL YOG STRAWB 500G","CREAMBELL YOG STRAWB 500G","12.50","16.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672326","","");
INSERT INTO product VALUES("880","CREAMBELL F/YOG/CHO/ORANGE175G","CREAMBELL F/YOG/CHO/ORANGE175G","5.50","7.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672418","","");
INSERT INTO product VALUES("881","CREAMBELL F/YOG. MANGO 500G","CREAMBELL F/YOG. MANGO 500G","11.50","15.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672388","","");
INSERT INTO product VALUES("882","CREAMBELL YOG CHOC/ORA 500G","CREAMBELL YOG CHOC/ORA 500G","12.00","16","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672401","","");
INSERT INTO product VALUES("883","CREAMBELL MIXED BERRY YOGHURT 500G","CREAMBELL MIXED BERRY YOGHURT 500G","12.50","17","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","2708","","");
INSERT INTO product VALUES("884","CREAM/B FRUIT THICK YOG 175G","CREAM/B FRUIT THICK YOG 175G","5.50","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672395","","");
INSERT INTO product VALUES("885","C/B MANGO 175G","C/B MANGO 175G","5.07","7","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009803672371","","");
INSERT INTO product VALUES("886","CREAMBELL YOG/D/ORANGE 500G","CREAMBELL YOG/D/ORANGE 500G","9.00","13","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672241","","");
INSERT INTO product VALUES("887","CREAMBELL YOGHURT R&B 500G","CREAMBELL YOGHURT R&B 500G","0.00","13","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672227","","");
INSERT INTO product VALUES("888","CREAMBELL PINE 175G","CREAMBELL PINE 175G","6.20","7.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672357","","");
INSERT INTO product VALUES("889","c/bYOG DRINK BANA500G","c/bYOG DRINK BANA500G","9.50","13","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009803672265","","");
INSERT INTO product VALUES("890","PARMALAT P/L/F/YOGHURT/500G","PARMALAT P/L/F/YOGHURT/500G","11.74","16.5","","68","10","1","0","21","","2019-10-05 23:47:32","6","5","6009683612528","","");
INSERT INTO product VALUES("891","STRAW/LOW FAT YOG 500G","STRAW/LOW FAT YOG 500G","11.74","16.5","","68","10","1","0","19","","2019-10-05 23:47:32","6","5","6009683612504","","");
INSERT INTO product VALUES("892","PARM/BANAN LOW FAT YOG 175G","PARM/BANAN LOW FAT YOG 175G","5.50","7.5","default.gif","68","10","1","0","19","0","2019-10-14 17:51:32","5","5","1992","","");
INSERT INTO product VALUES("893","Classic Choclate","","20.00","35","default.gif","63","100","1","0","11","Non","2019-10-18 08:11:59","1","5","6001056119009","","");
INSERT INTO product VALUES("894","Parmalat Milk","","5.00","150","default.gif","63","500","1","0","11","Non","2019-10-18 08:12:28","1","5","6009683610562","","");



CREATE TABLE `purchase_request` (
  `pr_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `request_date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `purchase_status` varchar(10) NOT NULL,
  PRIMARY KEY (`pr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `reversals_log` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `qty` int(12) NOT NULL,
  `action` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_no` text NOT NULL,
  `user_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO reversals_log VALUES("5","4","1","Deleted","2019-09-18 11:11:09","3531","4");
INSERT INTO reversals_log VALUES("6","29","20","Edited","2019-09-18 11:11:32","3531","4");



CREATE TABLE `sales` (
  `sales_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cash_tendered` decimal(10,2) DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `amount_due` decimal(10,2) NOT NULL,
  `cash_change` decimal(10,2) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `modeofpayment` varchar(15) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `order_no` int(12) NOT NULL,
  PRIMARY KEY (`sales_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

INSERT INTO sales VALUES("1","0","8","120.00","0.00","104.50","15.50","2019-10-05 19:56:40","cash","1","104.50","3430");
INSERT INTO sales VALUES("2","0","8","45.00","0.00","45.00","0.00","2019-10-05 21:46:16","cash","1","45.00","4318");
INSERT INTO sales VALUES("3","0","8","16.00","0.00","15.50","0.50","2019-10-05 22:05:56","cash","1","15.50","930");
INSERT INTO sales VALUES("4","0","8","40.00","0.00","36.00","4.00","2019-10-05 22:06:56","cash","1","36.00","4878");
INSERT INTO sales VALUES("5","0","8","32.00","0.00","30.00","2.00","2019-10-05 22:08:50","cash","1","30.00","1290");
INSERT INTO sales VALUES("6","0","8","207.00","0.00","206.00","1.00","2019-10-05 22:09:30","cash","1","206.00","2178");
INSERT INTO sales VALUES("7","0","8","30.00","0.00","30.00","0.00","2019-10-05 22:28:31","cash","1","30.00","3649");
INSERT INTO sales VALUES("8","0","8","100.00","0.00","96.00","4.00","2019-10-05 22:48:50","cash","1","96.00","792");
INSERT INTO sales VALUES("9","0","8","200.00","0.00","110.50","89.50","2019-10-06 00:02:58","cash","1","110.50","55");
INSERT INTO sales VALUES("10","0","8","37.00","0.00","36.20","0.80","2019-10-06 13:54:39","cash","1","36.20","895");
INSERT INTO sales VALUES("11","0","8","70.00","0.00","66.50","3.50","2019-10-08 08:13:04","cash","1","66.50","1951");
INSERT INTO sales VALUES("12","0","8","73.00","0.00","72.00","1.00","2019-10-08 08:13:18","cash","1","72.00","4915");
INSERT INTO sales VALUES("13","0","8","12.00","0.00","11.85","0.15","2019-10-09 12:13:34","cash","1","11.85","2389");
INSERT INTO sales VALUES("14","0","8","1.00","0.00","0.50","0.50","2019-10-09 12:24:25","cash","1","0.50","1139");
INSERT INTO sales VALUES("15","0","8","40.00","0.00","30.00","10.00","2019-10-09 12:28:46","cash","1","30.00","3487");
INSERT INTO sales VALUES("16","0","8","62.00","0.00","61.00","1.00","2019-10-09 12:30:58","cash","1","61.00","1197");
INSERT INTO sales VALUES("17","0","8","200.00","0.00","160.00","40.00","2019-10-09 12:32:25","cash","1","160.00","1638");
INSERT INTO sales VALUES("18","0","8","250.00","0.00","232.50","17.50","2019-10-09 12:48:24","cash","1","232.50","3879");
INSERT INTO sales VALUES("19","0","8","100.00","0.00","75.00","25.00","2019-10-13 16:03:42","cash","1","75.00","2995");
INSERT INTO sales VALUES("20","0","8","30.00","0.00","29.50","0.50","2019-10-13 16:08:45","cash","1","29.50","4700");
INSERT INTO sales VALUES("21","0","8","40.00","0.00","36.00","4.00","2019-10-13 16:11:22","cash","1","36.00","3839");
INSERT INTO sales VALUES("22","0","8","100.00","0.00","38.00","62.00","2019-10-18 08:13:14","cash","1","38.00","3806");
INSERT INTO sales VALUES("23","0","8","90.00","0.00","90.00","0.00","2019-10-25 19:20:15","cash","1","90.00","1617");
INSERT INTO sales VALUES("24","0","8","30.00","0.00","26.00","4.00","2019-10-25 19:21:22","cash","1","26.00","1457");
INSERT INTO sales VALUES("25","0","8","15.00","0.00","15.00","0.00","2019-10-27 20:02:08","cash","1","15.00","2751");
INSERT INTO sales VALUES("26","0","8","40.00","0.00","30.00","10.00","2019-10-27 20:03:14","cash","1","30.00","869");
INSERT INTO sales VALUES("27","0","8","105.00","0.00","105.00","0.00","2019-10-28 08:05:14","cash","1","105.00","824");
INSERT INTO sales VALUES("28","0","8","20.00","0.00","15.50","4.50","2019-10-28 13:56:29","cash","1","15.50","3565");
INSERT INTO sales VALUES("29","0","8","10.00","0.00","9.00","1.00","2019-10-28 13:57:12","cash","1","9.00","1582");



CREATE TABLE `sales_details` (
  `sales_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `order_no` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  PRIMARY KEY (`sales_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

INSERT INTO sales_details VALUES("1","1","767","30.00","2","3430","8");
INSERT INTO sales_details VALUES("2","1","172","10.50","1","3430","8");
INSERT INTO sales_details VALUES("3","1","372","64.00","1","3430","8");
INSERT INTO sales_details VALUES("4","2","767","15.00","3","4318","8");
INSERT INTO sales_details VALUES("5","3","767","15.00","1","930","8");
INSERT INTO sales_details VALUES("6","3","779","0.50","1","930","8");
INSERT INTO sales_details VALUES("7","4","767","15.00","1","4878","8");
INSERT INTO sales_details VALUES("8","4","172","10.50","2","4878","8");
INSERT INTO sales_details VALUES("9","5","767","15.00","2","1290","8");
INSERT INTO sales_details VALUES("10","6","502","0.50","1","2178","8");
INSERT INTO sales_details VALUES("11","6","372","64.00","1","2178","8");
INSERT INTO sales_details VALUES("12","6","117","2.50","1","2178","8");
INSERT INTO sales_details VALUES("13","6","371","120.00","2","2178","8");
INSERT INTO sales_details VALUES("14","6","246","15.00","1","2178","8");
INSERT INTO sales_details VALUES("15","6","100","4.00","1","2178","8");
INSERT INTO sales_details VALUES("16","7","767","15.00","2","3649","8");
INSERT INTO sales_details VALUES("17","8","471","36.00","1","792","8");
INSERT INTO sales_details VALUES("18","8","371","60.00","1","792","8");
INSERT INTO sales_details VALUES("19","9","471","36.00","1","55","8");
INSERT INTO sales_details VALUES("20","9","371","60.00","1","55","8");
INSERT INTO sales_details VALUES("21","9","172","10.50","1","55","8");
INSERT INTO sales_details VALUES("22","9","133","4.00","1","55","8");
INSERT INTO sales_details VALUES("23","10","24","25.66","1","895","8");
INSERT INTO sales_details VALUES("24","10","32","10.52","0","895","8");
INSERT INTO sales_details VALUES("25","11","840","59.50","7","1951","8");
INSERT INTO sales_details VALUES("26","11","729","7.00","1","1951","8");
INSERT INTO sales_details VALUES("27","12","471","72.00","2","4915","8");
INSERT INTO sales_details VALUES("28","13","33","39.50","0","2389","8");
INSERT INTO sales_details VALUES("29","14","779","0.50","1","1139","0");
INSERT INTO sales_details VALUES("30","15","61","300.00","1","3487","0");
INSERT INTO sales_details VALUES("31","16","2","316.00","2","1197","0");
INSERT INTO sales_details VALUES("32","16","18","420.00","1","1197","0");
INSERT INTO sales_details VALUES("33","17","471","36.00","1","1638","0");
INSERT INTO sales_details VALUES("34","17","371","60.00","1","1638","0");
INSERT INTO sales_details VALUES("35","17","372","64.00","1","1638","0");
INSERT INTO sales_details VALUES("36","18","371","60.00","3","3879","0");
INSERT INTO sales_details VALUES("37","18","663","16.50","1","3879","0");
INSERT INTO sales_details VALUES("38","18","471","36.00","1","3879","0");
INSERT INTO sales_details VALUES("39","19","767","15.00","1","2995","8");
INSERT INTO sales_details VALUES("40","19","371","60.00","1","2995","8");
INSERT INTO sales_details VALUES("41","20","767","15.00","1","4700","8");
INSERT INTO sales_details VALUES("42","20","172","10.50","1","4700","8");
INSERT INTO sales_details VALUES("43","20","133","4.00","1","4700","8");
INSERT INTO sales_details VALUES("44","21","471","36.00","1","3839","8");
INSERT INTO sales_details VALUES("45","22","832","18.00","1","3806","8");
INSERT INTO sales_details VALUES("46","22","420","20.00","1","3806","8");
INSERT INTO sales_details VALUES("47","23","767","30.00","2","1617","8");
INSERT INTO sales_details VALUES("48","23","371","60.00","1","1617","8");
INSERT INTO sales_details VALUES("49","24","767","15.00","1","1457","0");
INSERT INTO sales_details VALUES("50","24","172","10.50","1","1457","0");
INSERT INTO sales_details VALUES("51","24","501","0.50","1","1457","0");
INSERT INTO sales_details VALUES("52","25","767","15.00","1","2751","8");
INSERT INTO sales_details VALUES("53","26","767","15.00","2","869","0");
INSERT INTO sales_details VALUES("54","27","767","45.00","3","824","8");
INSERT INTO sales_details VALUES("55","27","371","60.00","1","824","8");
INSERT INTO sales_details VALUES("56","28","779","0.50","1","3565","8");
INSERT INTO sales_details VALUES("57","28","307","15.00","5","3565","8");
INSERT INTO sales_details VALUES("58","29","307","3.00","3","1582","8");



CREATE TABLE `sales_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `item_sold_id` int(12) NOT NULL,
  `quantity` int(12) NOT NULL,
  `sales_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(12) NOT NULL,
  `price` int(12) NOT NULL,
  `date` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `shop_category_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO shop_category_tb VALUES("1","Bar");
INSERT INTO shop_category_tb VALUES("2","Restruant");



CREATE TABLE `stock_audit_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` text NOT NULL,
  `count` int(12) NOT NULL,
  `added_to` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `action` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO stock_audit_tb VALUES("1","PARM/BANAN LOW FAT YOG 175G","10","New Station Mumbwa","2019-10-14 17:51:32","Edited");
INSERT INTO stock_audit_tb VALUES("2","Classic Choclate","100","New Station Mumbwa","2019-10-18 08:11:59","Added");
INSERT INTO stock_audit_tb VALUES("3","Parmalat Milk","500","New Station Mumbwa","2019-10-18 08:12:28","Added");



CREATE TABLE `stock_damages_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `no_damages` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `stock_purchases_tb` (
  `purchase_id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `qty` text NOT NULL,
  `user_id` int(12) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `supplier_id` int(12) NOT NULL,
  `status` text NOT NULL,
  `invoice` int(12) NOT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

INSERT INTO stock_purchases_tb VALUES("3","779","300","4","2019-10-14 18:28:41","14","purchased","1992");
INSERT INTO stock_purchases_tb VALUES("5","406","580","4","2019-10-14 18:55:22","11","purchased","1993");
INSERT INTO stock_purchases_tb VALUES("7","767","701","4","2019-10-27 08:19:25","11","purchased","0");
INSERT INTO stock_purchases_tb VALUES("8","471","890","4","2019-10-27 08:19:25","11","purchased","0");
INSERT INTO stock_purchases_tb VALUES("9","501","450","4","2019-10-27 08:19:25","11","purchased","0");
INSERT INTO stock_purchases_tb VALUES("10","770","780","4","2019-10-27 08:19:25","11","purchased","0");
INSERT INTO stock_purchases_tb VALUES("11","767","891","4","2019-10-27 08:20:54","11","","0");
INSERT INTO stock_purchases_tb VALUES("12","471","0.5","4","2019-10-27 08:21:43","11","","0");
INSERT INTO stock_purchases_tb VALUES("13","134","10","4","2019-10-30 11:13:03","11","","0");



CREATE TABLE `stock_trasfers_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `qty` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `moved_to` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `stockin` (
  `stockin_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `qty` int(6) NOT NULL,
  `date` datetime NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`stockin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `stores_branch` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `branch_name` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO stores_branch VALUES("5","Kitchen","2019-10-30 11:07:24");
INSERT INTO stores_branch VALUES("6","CAFE","2019-10-30 11:07:15");
INSERT INTO stores_branch VALUES("7","BAR","2019-10-30 11:07:09");



CREATE TABLE `supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_address` varchar(300) NOT NULL,
  `supplier_contact` varchar(50) NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

INSERT INTO supplier VALUES("11","AQUARITE LIMITED","Stand 8643, Chinika Industrial Area
Lusaka","Ashish");
INSERT INTO supplier VALUES("12","BIEN DONNE FARM (LTD)","
Lusaka","Ashish");
INSERT INTO supplier VALUES("13","C & M HOLDSWORTHS LTD","
Lusaka","Ashish");
INSERT INTO supplier VALUES("14","CAPITAL FISHERIES","
Lusaka","Ashish");
INSERT INTO supplier VALUES("15","COMIC GENERAL DEALERS ","
Lusaka","Ashish");
INSERT INTO supplier VALUES("16","CONTINENTAL MILLING CO. LTD","
Lusaka","Ashish");
INSERT INTO supplier VALUES("17","EVERJEMU ENTERPRISES LTD","
Lusaka","Ashish");
INSERT INTO supplier VALUES("18","GRANNYS BAKERY LTD","
Lusaka","Ashish");
INSERT INTO supplier VALUES("19","HAMSGOLD INVESTMENTS LTD","
Lusaka","Ashish");
INSERT INTO supplier VALUES("20","PRODUCTION","
Lusaka","Ashish");
INSERT INTO supplier VALUES("21","OTHERS","
Lusaka","Ashish");



CREATE TABLE `supplier_payments_tb` (
  `payment_id` int(12) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount_paid` text NOT NULL,
  `balance` text NOT NULL,
  `total_amount` text NOT NULL,
  `status` text NOT NULL,
  `invoice_no` int(12) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `temp_trans` (
  `temp_trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` text NOT NULL,
  `branch_id` int(11) NOT NULL,
  `user_id` int(12) NOT NULL,
  PRIMARY KEY (`temp_trans_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO temp_trans VALUES("9","767","15.00","10","1","8");



CREATE TABLE `term` (
  `term_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_id` int(11) DEFAULT NULL,
  `payable_for` varchar(10) NOT NULL,
  `term` varchar(11) NOT NULL,
  `due` decimal(10,2) NOT NULL,
  `payment_start` date NOT NULL,
  `down` decimal(10,2) NOT NULL,
  `due_date` date NOT NULL,
  `interest` decimal(10,2) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`term_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `user_type` text NOT NULL,
  `branch_id_user` int(12) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO user VALUES("4","admin","21232f297a57a5a743894a0e4a801fc3","Mona Lisas","active","1","Admin","3");
INSERT INTO user VALUES("8","user","81dc9bdb52d04dc20036dbd8313ed055","test USer","active","1","User","5");
INSERT INTO user VALUES("10","user","2d579dc29360d8bbfbb4aa541de5afa9","Supervisor","active","1","Supervisor","0");



CREATE TABLE `ware_house_tb` (
  `prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_name` varchar(100) NOT NULL,
  `prod_desc` varchar(500) NOT NULL,
  `prod_price` decimal(10,2) NOT NULL,
  `prod_sell_price` text NOT NULL,
  `prod_pic` varchar(300) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `prod_qty` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `serial` varchar(50) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(12) NOT NULL,
  `barcode` text NOT NULL,
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


