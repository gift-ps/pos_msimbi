<?php
// session_start();

// $id = $_SESSION['id'];

// Get Order no.
if (isset($_POST['get_order'])) {

    $order_no = $_POST['order_no'];

    $query2 = mysqli_query($con, "SELECT * FROM sales WHERE order_no = '$order_no'") or die(mysqli_error($con));
    $count = mysqli_num_rows($query2);

    if ($count < 1) {
        echo "<script type='text/javascript'>alert('Error! Order number does not exist. ');</script>";
        echo "<script>document.location='exchange_items.php'</script>";
    } else {
        echo "<script>document.location='exchange_items.php?order_no=$order_no'</script>";
    }
}




if (isset($_POST['apply_exchange'])) {

    $customer_contact = $_POST['customer_contact'];
    $customer_name = $_POST['customer_name'];
    $total = $_POST['total'];
    $order_no = $_POST['order_no'];
    $sales_id = $_POST['sales_id'];
    $user_id = $_POST['user_id'];
    $branch = $_SESSION['branch'];
    $reason = $_POST['reason'];
    $cost_price = $_POST['cost_price'];
    // Arrays
    $prod_ids = $_POST['prod_id'];
    $qtys = $_POST['qty'];



    // First add data to the sales_exchange
    array_map(function ($prod_id, $qtys) {
        global $con, $sales_id, $order_no, $user_id, $customer_name, $customer_contact, $reason, $branch;

        // return var_dump($qtys);

        $query = mysqli_query($con, "SELECT prod_sell_price FROM product WHERE prod_id = '$prod_id' ") or die(mysqli_error($con));
        while ($row = mysqli_fetch_array($query)) {
            $prod_price = $row['prod_sell_price'];

            // check if order already exists, And delete if so
            $oredre_ck_q = mysqli_query($con, "SELECT id FROM sales_exchange WHERE order_no = '$order_no'") or die(mysqli_error($con));
            if (mysqli_num_rows($oredre_ck_q) > 0) {
                mysqli_query($con, "DELETE FROM sales_exchange WHERE order_no ='$order_no' ") or die('An error occured : ' . mysqli_error($con));
            }
            /** Add to db for log purposes */
            mysqli_query($con, "INSERT INTO sales_exchange ( order_no,sales_id,customer_name,customer_contact,reason, user_id, prod_id, price, qty, branch_id)
                    VALUES('$order_no','$sales_id','$customer_name','$customer_contact','$reason','$user_id', '$prod_id','$prod_price','$qtys', '$branch' )") or die(mysqli_error($con));
        }
    }, $prod_ids, $qtys);

    /** Before We proceed to deleting. do some validation to
     * ensure the price is right
     */
    $priecQuery = mysqli_query($con, "SELECT SUM(price*qty) AS added_total FROM sales_exchange WHERE order_no = '$order_no' ") or die(mysqli_error($con));
    $priceRow = mysqli_fetch_array($priecQuery);
    $added_total = $priceRow['added_total'];

    // return var_dump($added_total, $total, $sales_id);
    function process_exchange($con, $order_no, $cost_price)
    {
        // Then Delete all record with same name from sales_details tbl.
        $delete = mysqli_query($con, "DELETE FROM sales_details WHERE order_no ='$order_no' ") or die('An error occured : ' . mysqli_error($con));

        // Now add new data to it
        $query1 = mysqli_query($con, "SELECT prod_id, price, SUM(qty) AS qty, user_id, sales_id FROM `sales_exchange` 
                WHERE order_no = '$order_no' GROUP BY prod_id") or die(mysqli_error($con));
        while ($row1 = mysqli_fetch_array($query1)) {
            $prod_id = $row1['prod_id'];
            $price = $row1['price'];
            $qty = $row1['qty'];
            $user_id = $row1['user_id'];
            $sales_id = $row1['sales_id'];


            mysqli_query($con, "INSERT INTO sales_details(sales_id, prod_id, price, qty, order_no, user_id, cost_price)
                                        VALUES('$sales_id','$prod_id','$price','$qty', '$order_no','$user_id','$cost_price')") or die(mysqli_error($con));
            // Update sales aswell

        }
        echo "<script type='text/javascript'>alert('Successfully Exchange Items!');</script>";
        echo "<script>document.location='exchange_items.php?success'</script>";
    }

    if ($total > $added_total) {
        $change = $total - $added_total;
?>
        <script>
            $(document).ready(function() {
                $("#change").modal('show');
            });
            alert('Change action required');
        </script>
    <?php
        // return;
    } elseif ($total < $added_total) {
        $toppup = $added_total - $total;
    ?>
        <script>
            $(document).ready(function() {
                $("#toppup").modal('show');
            });
            alert('Topup action required');
        </script>
    <?php
        // return;
    } elseif ($total == $added_total) {
        process_exchange($con, $user_id, $order_no, $sales_id, $cost_price);
    }
    ?>
    <!-- Change Modal -->
    <div id="change" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content" style="height:auto">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body">

                    <h5>Kindly Give the customer a K<?php echo $change ?> change</h5>

                    <div class="modal-footer">
                        <a href="exchange_items.php?proceed=<?php echo $order_no."&change=".$change; ?>" class="btn btn-default">PROCEED</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- Toppup Modal -->
    <div id="toppup" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content" style="height:auto">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title"></h4>
                </div>

                <div class="modal-body">

                    <h4>Collect an additional <b>K<?php echo $toppup ?></b> as a toppup from customer</h4>
                    <?php ?>
                    <div class="modal-footer">
                        <a href="exchange_items.php?proceed=<?php echo $order_no; ?>" class="btn btn-default">PROCEED</a>
                        <a href="exchange_items.php?cancel=<?php echo $order_no; ?>" class="btn btn-danger"> CANCEL </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php

}
// Logic for the proceed btn
if (isset($_GET['proceed'])) {
    // process_exchange($con, $user_id, $order_no, $sales_id, $cost_price);
    // Then Delete all record with same name from sales_details tbl.
    $order_no = $_GET['proceed'];
    $change = $_GET['change'];
    $delete = mysqli_query($con, "DELETE FROM sales_details WHERE order_no ='$order_no' ") or die('An error occured : ' . mysqli_error($con));

    // Now add new data to it
    $query1 = mysqli_query($con, "SELECT prod_id, price, SUM(qty) AS qty, user_id, sales_id FROM `sales_exchange` 
        WHERE order_no = '$order_no' GROUP BY prod_id") or die(mysqli_error($con));
    while ($row1 = mysqli_fetch_array($query1)) {
        $prod_id = $row1['prod_id'];
        $price = $row1['price'];
        $qty = $row1['qty'];
        $user_id = $row1['user_id'];
        $sales_id = $row1['sales_id'];


        mysqli_query($con, "INSERT INTO sales_details(sales_id, prod_id, price, qty, order_no, user_id, cost_price)
                                VALUES('$sales_id','$prod_id','$price','$qty', '$order_no','$user_id','$cost_price')") or die(mysqli_error($con));
        // Update sales aswell... 
        mysqli_query($con, "UPDATE sales SET cash_change = '$change' WHERE sales_id ='$sales_id' ")or die(mysqli_error($con));
        // And also change in sales_exchange
        mysqli_query($con, "UPDATE sales_exchange SET cash_change = '$change' WHERE order_no ='$order_no' ")or die(mysqli_error($con));
    }

    echo "<script type='text/javascript'>alert('Successfully Exchange Items!');</script>";
    echo "<script>document.location='exchange_items.php?success'</script>";
}

// Logic for the cancel btn
if (isset($_GET['cancel'])) {
    $order_no = $_GET['cancel'];
    mysqli_query($con, "DELETE FROM sales_exchange WHERE order_no = '$order_no' ");
    echo "<script>document.location='exchange_items.php?order_no=$order_no'</script>";
}
