<?php
session_start();
if (empty($_SESSION['id'])):
    header('Location:../index.php');
endif;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Product | <?php include('../dist/includes/title.php'); ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
        <script src="../plugins/datatables/table-exporter.js"></script>

        <!-- Export to PDF DATATables -->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css">

        <style>

        </style>
    </head>
    <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
    <body class="hold-transition skin-<?php echo $_SESSION['skin']; ?> layout-top-nav">
        <div class="wrapper">
             <?php
            include('../dist/includes/header_admin.php');
            include ('../Objects/Objects.php');
            include ('DAO.php');
            $Objects = new InvObjects();
            $DAO = new DAO();
            ?>
            <div class="content-wrapper">
                <div class="container">
                    <section class="content-header">
                        <br></br>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">Product</li>
                        </ol>
                    </section>
                    
                    
                          <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title" style=" color: black"><b>TOP 5 STOCK OUTS</b></h3>
                                    </div>
                                    <div class="box-body">  
                                        <br></br>
                                        <form method="post" action="">

                                            <table id="dash" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Product</th>
                                                        <th>SOH</th>
                                                        <th>Value</th>                                                   
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $query_ = $DAO->getTopStockOuts($con);
                                                    while ($row = mysqli_fetch_array($query_)) {
                                                       // $prod_id = $row['prod_id'];
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $row['prod_name']; ?></td>
                                                            <td><?php echo $row['prod_qty']; ?></td>
                                                            <td><?php echo number_format($row['value'],2); ?></td>                                                                                                               
                                                        </tr>                                             
                                                    <?php } ?>					  
                                                </tbody>                                           
                                            </table>

                                        </form>
                                    </div><!-- /.box-body -->
                                </div>
                            </div>
                            
                              <div class="col-xs-12">
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title" style=" color: black"><b>AGED STOCK (3months)</b></h3>
                                    </div>
                                    <div class="box-body">  
                                        <br></br>
                                        <form method="post" action="">

                                            <table id="dash2" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Product</th>
                                                        <th>SOH</th>
                                                        <th>Value</th>                                                   
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $query_1 = $DAO->getAgedStock($con);
                                                    while ($row = mysqli_fetch_array($query_1)) {
                                                       // $prod_id = $row['prod_id'];
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $row['prod_name']; ?></td>
                                                            <td><?php echo $row['prod_qty']; ?></td>
                                                            <td><?php echo number_format($row['value'],2); ?></td>                                                                                                               
                                                        </tr>                                             
                                                    <?php } ?>					  
                                                </tbody>                                           
                                            </table>

                                        </form>
                                    </div><!-- /.box-body -->
                                </div>
                            </div>
                            
                               <div class="col-xs-12">
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title" style=" color: black"><b>DEAD STOCK (Dead Stocks 6-1 year)</b></h3>
                                    </div>
                                    <div class="box-body">  
                                        <br></br>
                                        <form method="post" action="">

                                            <table id="dash3" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Product</th>
                                                        <th>SOH</th>
                                                        <th>Value</th>                                                   
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $query_ = $DAO->getDeadStock($con);
                                                    while ($row = mysqli_fetch_array($query_)) {
                                                       // $prod_id = $row['prod_id'];
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $row['prod_name']; ?></td>
                                                            <td><?php echo $row['prod_qty']; ?></td>
                                                            <td><?php echo number_format($row['value'],2); ?></td>                                                                                                               
                                                        </tr>                                             
                                                    <?php } ?>					  
                                                </tbody>                                           
                                            </table>

                                        </form>
                                    </div><!-- /.box-body -->
                                </div>
                            </div>
                           
                    </section><!-- /.content -->


                    <div class="box-body">
                        <form method="post">                                         

                            <div class="col-lg-4">
                                <label>Select Branch</label>
                                <select class="form-control select2" style="width: 100%;" name="branch_id" required >
                                    <option value="all_branches">All Branches</option>
                                    <?php
                                    $queryc = mysqli_query($con, "select * from stores_branch order by branch_name")or die(mysqli_error($con));
                                    while ($rowc = mysqli_fetch_array($queryc)) {
                                        ?>
                                        <option value="<?php echo $rowc['id']; ?>"><?php echo $rowc['branch_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary" name="stock_count"> Generate Report </button>
                        </form>
                    </div> 


                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title" style=" color: black"><b>Stock Out Report</b></h3>
                                    </div>
                                    <div class="box-body">  
                                        <br></br>
                                        <form method="post" action="">

                                            <table id="example" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Picture</th>
                                                        <th>Product Code</th>
                                                        <th>Product Name</th>  
                                                        <th>Branch Name</th>  
                                                        <th>Supplier</th>
                                                        <th>Qty</th>
                                                        <th>Re Order Level</th>
                                                        <th>Buy Price</th>
                                                        <th>Sell Price</th>
                                                        <th>Item Category</th>                                                 

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                 
                                                    if (isset($_POST['stock_count'])) {
                                                        //$prod_id = $_POST['cat_id'];
                                                        $branch_id = $_POST['branch_id'];

                                                        if($branch_id!="all_branches"){
                                                               $query = mysqli_query($con, "select * from product INNER join supplier on supplier.supplier_id=product.supplier_id  INNER join category

                                                    on category.cat_id=product.cat_id
                                                    INNER JOIn stores_branch on stores_branch.id=product.stock_branch_id
                                                    where prod_qty <= re_order AND product.stock_branch_id='$branch_id'  ")or die(mysqli_error($con));
                                                        
                                                        }                                                      
                                                         else{
                                                                $query = mysqli_query($con, "select * from product INNER join supplier on supplier.supplier_id=product.supplier_id  INNER join category

                                                    on category.cat_id=product.cat_id
                                                    INNER JOIn stores_branch on stores_branch.id=product.stock_branch_id
                                                    where prod_qty <= re_order  ")or die(mysqli_error($con));
                                                        
                                                         }
                                                    } else {
                                                        //$query = mysqli_query($con, "select * from product natural join supplier natural join category INNER JOIN stores_branch ON stores_branch.id=product.stock_branch_id AND stock_branch_id='$branch' order by prod_name")or die(mysqli_error($con));

                                                        $query = mysqli_query($con, "select * from product INNER join supplier on supplier.supplier_id=product.supplier_id  INNER join category

                                                    on category.cat_id=product.cat_id
                                                    INNER JOIn stores_branch on stores_branch.id=product.stock_branch_id
                                                    where prod_qty <= re_order")or die(mysqli_error($con));
                                                    }
                                                        while ($row = mysqli_fetch_array($query)) {
                                                            $prod_id = $row['prod_id'];
                                                            ?>
                                                            <tr>
                                                                <td><img style="width:80px;height:60px" src="../dist/uploads/pos.png"></td>
                                                                <td><?php echo $row['serial']; ?></td>
                                                                <td><?php echo $row['prod_name']; ?></td>
                                                                <td><?php echo $row['branch_name']; ?></td>
                                                                <td><?php echo $row['supplier_name']; ?></td>
                                                                <td><?php
                                                    $productQuantity = $row['prod_qty'];
                                                    if ($productQuantity > 0) {
                                                        echo "<span class='label label-success'>" . $productQuantity . "</span>";
                                                    } else {
                                                        echo "<span class='label label-danger'>" . $productQuantity . "</span>";
                                                    }
                                                            ?>
                                                                </td> 
                                                                <td><?php echo number_format($row['reorder'], 2); ?></td>
                                                                <td><?php echo number_format($row['prod_qty'], 2); ?></td>
                                                                <td><?php echo number_format($row['prod_sell_price'], 2); ?></td>
                                                                <td><?php echo $row['cat_name']; ?></td>                                                       

                                                            </tr>                                             

    <?php } ?>					  
                                                    </tbody>                                           
                                                </table>

                                            </form>
                                        </div><!-- /.box-body -->
                                    </div>
                                </div>
                        </section><!-- /.content -->
                    </div><!-- /.container -->
                </div><!-- /.content-wrapper -->
    <?php include('../dist/includes/footer.php'); ?>
            </div><!-- ./wrapper -->
            <div id="add" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content" style="height:auto">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Add New Product</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" method="post" action="product_add.php" enctype='multipart/form-data'>                           
                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="name">Product Name</label>
                                    <div class="col-lg-9"><input type="hidden" class="form-control" id="id" name="id" required>  
                                        <input type="text" class="form-control" id="name" name="prod_name" placeholder="Product Name" required>  
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="price">Product Description</label>
                                    <div class="col-lg-9">
                                        <textarea class="form-control" id="price" name="prod_desc" placeholder="Product Description"></textarea>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="file">Supplier</label>
                                    <div class="col-lg-9">
                                        <select class="form-control select2" style="width: 100%;" name="supplier" required>
    <?php
    $query2 = mysqli_query($con, "select * from supplier")or die(mysqli_error($con));
    while ($row2 = mysqli_fetch_array($query2)) {
        ?>
                                                <option value="<?php echo $row2['supplier_id']; ?>"><?php echo $row2['supplier_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="price">Price Bought</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" id="price" name="prod_price" placeholder="Price Prodct Bought" required>  
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="price">Quantity Bought</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" id="price" name="prod_qty" placeholder="Quantity Bought" required>  
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="price">Selling Price</label>
                                    <div class="col-lg-9">
                                        <input type="number" class="form-control" id="price" name="prod_sell_price" placeholder="Selling Price Per Item" required>  
                                    </div>
                                </div>

                                <div class="form-group" hidden="">
                                    <label class="control-label col-lg-3" >Item Belongs To:</label>
                                    <div class="col-lg-9">
                                        <select class="form-control select2" style="width: 100%;" name="belongs_to" required>
    <?php
    $queryc = mysqli_query($con, "select * from shop_category_tb")or die(mysqli_error($con));
    while ($rowc = mysqli_fetch_array($queryc)) {
        ?>
                                                <option value="<?php echo $rowc['id']; ?>"><?php echo 'The ' . $rowc['name']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div><!-- /.input group -->
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-lg-3" >Category</label>
                                    <div class="col-lg-9">
                                        <select class="form-control select2" style="width: 100%;" name="category" required>
    <?php
    $queryc = mysqli_query($con, "select * from category order by cat_name")or die(mysqli_error($con));
    while ($rowc = mysqli_fetch_array($queryc)) {
        ?>
                                                <option value="<?php echo $rowc['cat_id']; ?>"><?php echo $rowc['cat_name']; ?></option>
                                            <?php } ?>
                                    </select>
                                </div><!-- /.input group -->
                            </div>                           
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="price">Picture</label>
                                <div class="col-lg-9">
                                    <input type="file" class="form-control" id="price" name="image">  
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    </form>
                </div>
            </div><!--end of modal-dialog-->
        </div>
        <!--end of modal--> 
        <!-- jQuery 2.1.4 -->
        <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <!-- SlimScroll -->
        <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="../plugins/fastclick/fastclick.min.js"></script>
        <!-- AdminLTE App -->
        <script src="../dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="../dist/js/demo.js"></script>
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>



        <!-- Export to PDF - Datatable -->
        <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ]
                });
            });
        </script>
        
         <script>
            $(document).ready(function () {
                $('#dash').DataTable({
                    dom: 'Bfrtip',
                    "ordering": false,
                    "paging": false,
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ]
                });
            });
        </script>
        
         <script>
            $(document).ready(function () {
                $('#dash2').DataTable({
                    dom: 'Bfrtip',
                    "ordering": false,
                    "paging": false,
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ]
                });
            });
        </script>
        
         <script>
            $(document).ready(function () {
                $('#dash3').DataTable({
                    dom: 'Bfrtip',
                    "ordering": false,
                    "paging": false,
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ]
                });
            });
        </script>


        <script>
            $(function () {
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });
        </script>

        <script>
            $("#checkAl").click(function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });
        </script>
    </body>
</html>
