<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
session_start();
?>
<?php
/* Change to the correct path if you copy this example! */
require __DIR__ . '/../../autoload.php';

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

include('../../../../dist/includes/dbcon.php');

try {
    // Enter the share name for your USB printer here
    //$connector = null;

    $connector = new WindowsPrintConnector("chescopos1");

    $id = $_SESSION['id'];
    $queryb = mysqli_query($con, "select * from branch")or die(mysqli_error($con));
    $rowb = mysqli_fetch_array($queryb);
    $reciept_footer_text = $rowb['reciept_footer_text'];

    $branch = $_SESSION['branch'];


    /* Print a "Hello world" receipt" */
    $printer = new Printer($connector);

    // Add image logo for the client..  

    /*
      $tux = EscposImage::load("meat-logo.png");
      $printer->setJustification(Printer::JUSTIFY_CENTER);
      $printer->bitImage($tux);

     */
    $printer->text($rowb['branch_name'] . ", " . "\n");
    $printer->text($rowb['branch_address'] . ", " . "\n");
    $printer->text($rowb['branch_contact'] . "\n\n");

    // invoice info...

    if (isset($_GET['fromdate'])) {
        $enddate = $_GET['enddate'];
        $fromdate = $_GET['fromdate'];

        $printer->text(" Sales Report from  " . $fromdate . " to " . $enddate . "  \n\n");
        $printer->text("............................\n");

        $query = mysqli_query($con, "SELECT sales_details.discount,sales_details.discount_type,SUM(sales_details.qty) as qty, sales_details.price AS prod_sell_price,product.prod_name FROM `sales` INNER JOIN sales_details on sales_details.sales_id=sales.sales_id
        INNER JOIN product on product.prod_id=sales_details.prod_id 
        AND sales.date_added BETWEEN '$fromdate' AND '$enddate' GROUP BY prod_name,sales_details.price")or die(mysqli_error($con));

        $userQuery = mysqli_query($con, "SELECT user.name, SUM(sales_details.qty * sales_details.price) AS totalsold FROM `sales` INNER JOIN sales_details on sales_details.sales_id=sales.sales_id
        INNER JOIN product on product.prod_id=sales_details.prod_id INNER JOIN user on user.user_id=sales_details.user_id
        AND sales.date_added BETWEEN '$fromdate' AND '$enddate' GROUP BY user.name")or die(mysqli_error($con));

        /*
        $queryPayments = mysqli_query($con, "SELECT modes_of_payment_tb.name AS modepayment,SUM(sales.amount_due) AS total  FROM sales_details INNER JOIN sales ON sales.sales_id=sales_details.sales_id                                                       INNER JOIN user ON user.user_id = sales.user_id INNER JOIN product ON product.prod_id = sales_details.prod_id
                                               INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment 
                                               AND sales.date_added BETWEEN '$fromdate' AND '$enddate' 
                                                GROUP BY modes_of_payment_tb.payment_mode_id")or die(mysqli_error($con));
        */
        
          $queryPayments = mysqli_query($con, "SELECT modes_of_payment_tb.name AS modepayment,SUM(sales.amount_due) AS total  FROM sales   
                                                                            INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment 
                                                                            INNER JOIN user on user.user_id=sales.user_id
                                                                            WHERE  sales.date_added BETWEEN '$startDate' AND '$stop_date'                                                     
                                                                            GROUP BY modes_of_payment_tb.payment_mode_id")or die(mysqli_error($con));
        

        $salesTotals = mysqli_query($con, "SELECT SUM(amount_due) as amount_due FROM `sales` WHERE date_added BETWEEN '$fromdate' AND '$enddate'  ")or die(mysqli_error($con));

        $partPaymentsByModes = mysqli_query($con, "SELECT SUM(amount) AS amount,name FROM `part_payments_tb`
         LEFT JOIn modes_of_payment_tb on modes_of_payment_tb.payment_mode_id=part_payments_tb.payment_mode_id
         WHERE part_payments_tb.date_added BETWEEN '$fromdate' AND '$enddate' 
         GROUP BY part_payments_tb.payment_mode_id")or die(mysqli_error($con));

        $partPayments = mysqli_query($con, "SELECT SUM(amount) AS amount FROM `part_payments_tb` WHERE date_added BETWEEN '$fromdate' AND '$enddate'  ")or die(mysqli_error($con));
    } else {
        $currentDateTime = date('Y-m-d H:i:s');

        $printer->text("Todays Sales Report " . $currentDateTime . "  \n\n");
        $printer->text("............................\n");

        $query = mysqli_query($con, "SELECT sales_details.discount,sales_details.discount_type,SUM(sales_details.qty) as qty, sales_details.price AS prod_sell_price,product.prod_name FROM `sales` INNER JOIN sales_details on sales_details.sales_id=sales.sales_id
        INNER JOIN product on product.prod_id=sales_details.prod_id
        AND DATE(sales.date_added)=DATE(NOW()) GROUP BY prod_name,sales_details.price")or die(mysqli_error($con));

        $queryPayments = mysqli_query($con, "SELECT modes_of_payment_tb.name AS modepayment,SUM(sales.amount_due) AS total  FROM sales_details INNER JOIN sales ON sales.sales_id=sales_details.sales_id                                                       INNER JOIN user ON user.user_id = sales.user_id INNER JOIN product ON product.prod_id = sales_details.prod_id
                                                        INNER JOIN  modes_of_payment_tb ON modes_of_payment_tb.payment_mode_id= sales.modeofpayment 
                                                        AND DATE(sales.date_added) = DATE(NOW()) 
                                                         GROUP BY modes_of_payment_tb.payment_mode_id")or die(mysqli_error($con));

        $userQuery = mysqli_query($con, "SELECT user.name, SUM(sales_details.qty * sales_details.price) AS totalsold FROM `sales` INNER JOIN sales_details on sales_details.sales_id=sales.sales_id
        INNER JOIN product on product.prod_id=sales_details.prod_id INNER JOIN user on user.user_id=sales_details.user_id
        AND DATE(sales.date_added)=DATE(NOW()) GROUP BY user.name")or die(mysqli_error($con));

        $partPaymentsByModes = mysqli_query($con, "SELECT SUM(amount) AS amount,name FROM `part_payments_tb`
        LEFT JOIn modes_of_payment_tb on modes_of_payment_tb.payment_mode_id=part_payments_tb.payment_mode_id
        WHERE DATE(part_payments_tb.date_added)=DATE(NOW())
        GROUP BY part_payments_tb.payment_mode_id")or die(mysqli_error($con));

        $partPayments = mysqli_query($con, "SELECT SUM(amount) AS amount FROM `part_payments_tb` WHERE DATE(date_added)= DATE(NOW())")or die(mysqli_error($con));

        $salesTotals = mysqli_query($con, "SELECT SUM(amount_due) as amount_due FROM `sales` WHERE DATE(date_added) = DATE(NOW())")or die(mysqli_error($con));
    }

    $grand = 0;
    $finalTotal = 0;
    $order_no = 0;
    $vatFinalTotal = 0;
    $discountTotal = 0;

    $salesRow = mysqli_fetch_array($salesTotals);
    $tenderedAmount = $salesRow['amount_due'];

    //get part payment amount..

    $partpaymentsRow = mysqli_fetch_array($partPayments);
    $partpaymentAmount = $partpaymentsRow['amount'];

    while ($row = mysqli_fetch_array($query)) {
        $finalTotal += $row['qty'] * $row['prod_sell_price'];

        $discount = $row['discount'];
        $discount_type = $row['discount_type'];

        if ($discount_type == "Percentage") {
            $discountTotal += ($discount / 100);
            // $newPrice = $price - $computedPrice;
        } else {
            $discountTotal += $discount;
        }

        $prodName = $row['prod_name'];
        $price = $row['prod_sell_price'];
        $total = $row['qty'] * $row['prod_sell_price'];
        $grand = $grand + $total;

        $printer->text($row['qty'] . "  " . substr($prodName, 0, 30) . " - @ K " . $price . " =K " . $total . "\n");
    }

    $printer->text("............................\n\n");

    while ($row1 = mysqli_fetch_array($userQuery)) {
        $userSales = $row1['name'];
        $totalsold = $row1['totalsold'];

        $printer->text("User : " . $userSales . ", Amount Collected : " . number_format($totalsold, 2) . "\n");
    }

    $printer->text("............................\n\n");

    while ($row1 = mysqli_fetch_array($queryPayments)) {
        $name = $row1['modepayment'];
        $total = $row1['total'];

        $printer->text($name . " K " . number_format($total, 2) . "\n\n");
    }

    $ttal = $finalTotal - $discountTotal;

    $printer->text("Discounts K :" . number_format($discountTotal, 2) . "\n\n");

    $printer->text("Total Part Payment K : " . number_format($partpaymentAmount, 2) . "\n\n");

    $printer->text("Total Amount Tendered K : " . number_format($tenderedAmount, 2) . "\n\n");

    $printer->text("Total  K : " . number_format($tenderedAmount + $partpaymentAmount, 2) . "\n\n");

    $printer->text("Printed from Chesco POS Ver 3.0 \n\n");

    $printer->text("\n\n");

    $printer->cut();

    /* Close printer */
    $printer->close();

    echo "<script>document.location='../../../sales-report.php'</script>";
} catch (Exception $e) {
    echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
}
