<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
session_start();
?>
<?php
/* Change to the correct path if you copy this example! */
require __DIR__ . '/../../autoload.php';

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

include('../../../../dist/includes/dbcon.php');

try {
    // Enter the share name for your USB printer here
    //$connector = null;

    $connector = new WindowsPrintConnector("chescopos1");

    $id = $_SESSION['id'];
    $queryb = mysqli_query($con, "select * from branch")or die(mysqli_error($con));
    $rowb = mysqli_fetch_array($queryb);
    $reciept_footer_text = $rowb['reciept_footer_text'];

    $branch = $_SESSION['branch'];
    $query = mysqli_query($con, "SELECT * FROM `sales` WHERE sales_id = (SELECT MAX(sales_id) FROM sales WHERE user_id='$id' ) AND user_id='$id'  ")or die(mysqli_error($con));

    $row = mysqli_fetch_array($query);

    $sales_id = $row['sales_id'];
    $sid = $row['sales_id'];
    $due = $row['amount_due'];
    $discount = $row['discount'];
    $grandtotal = $due - $discount;
    $tendered = $row['cash_tendered'];
    $change = $row['cash_change'];

    // update the invoice details.. 
    $invoice = mysqli_query($con, "select * from sales_details natural join product where sales_id='$sid' AND user_id='$id'  ")or die(mysqli_error($con));
    $rows = mysqli_fetch_array($invoice);
    $ordNo = $rows['order_no'];

    mysqli_query($con, "INSERT INTO invoices_tb(order_no) 
                                        VALUES('$ordNo')")or die(mysqli_error($con));

    $invoice2 = mysqli_query($con, "SELECT MAX(id) AS id FROM invoices_tb")or die(mysqli_error($con));
    $rowss = mysqli_fetch_array($invoice2);
    $invoiceNo = $rowss['id'];

    /* Print a "Hello world" receipt" */
    $printer = new Printer($connector);

    // Add image logo for the client..  

    /*
      $tux = EscposImage::load("meat-logo.png");
      $printer->setJustification(Printer::JUSTIFY_CENTER);
      $printer->bitImage($tux);

     */
    $printer->text($rowb['branch_name'] . ", " . "\n");
    $printer->text($rowb['branch_address'] . ", " . "\n");
    $printer->text($rowb['branch_contact'] . "\n\n");

    // invoice info...

    $printer->text("Items Bought for Invoice # 000" . $invoiceNo . "\n\n");
    $printer->text("...........................................\n");

    $query1 = mysqli_query($con, "select * from payment where sales_id='$sales_id'")or die(mysqli_error($con));

    $row1 = mysqli_fetch_array($query1);
    $branch = $_SESSION['branch'];
    $query = mysqli_query($con, "SELECT * FROM `sales` WHERE sales_id = (SELECT MAX(sales_id) FROM sales )")or die(mysqli_error($con));

    $row = mysqli_fetch_array($query);

    $sales_id = $row['sales_id'];
    $sid = $row['sales_id'];
    $due = $row['amount_due'];
    $discount = $row['discount'];
    $grandtotal = $due - $discount;
    $tendered = $row['cash_tendered'];
    $change = $row['cash_change'];

    $query = mysqli_query($con, "select * from sales_details natural join product where sales_id='$sid' ")or die(mysqli_error($con));
    $grand = 0;

    while ($row = mysqli_fetch_array($query)) {
        $order_no = $row['order_no'];
        $prodName = $row['prod_name'];
          $finalTotal += $row['qty'] * $row['prod_sell_price'];

        $price = $row['prod_sell_price'];
        $total = $row['qty'] * $row['prod_sell_price'];
        $grand = $grand + $total;

        $printer->text($row['qty']  . " " . $prodName . " - @ K " . $price . " =K " . $total . "\n");
    }

    $total = number_format($grand - $discount, 2);

    $printer->text("...........................................\n");

    $printer->text("Total K " . number_format($finalTotal, 2) . "\n");
    $printer->text("Cash Tendered " . number_format($tendered, 2) . "\n");
    $printer->text("Change " . number_format($change, 2) . "\n");
    $printer->text("Order No " . $order_no . "\n");

    $query = mysqli_query($con, "select * from user where user_id='$id'")or die(mysqli_error($con));
    $row = mysqli_fetch_array($query);

    $printer->text("...........................................\n");

    $printer->text("Issued By User " . $row['name'] . "\n");

    $printer->text(date("M d, Y") . " " . date("h:i A") . "\n");

    $printer->text($reciept_footer_text . "\n");

    $printer->cut();

    /* Close printer */
    $printer->close();

    echo "<script>document.location='../../../draft-sale.php'</script>";
} catch (Exception $e) {
    echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
}
