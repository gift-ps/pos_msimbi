<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
session_start();
?>
<?php
/* Change to the correct path if you copy this example! */
require __DIR__ . '/../../autoload.php';

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

include('../../../../dist/includes/dbcon.php');

try {
    // Enter the share name for your USB printer here
    //$connector = null;

    $connector = new WindowsPrintConnector("chescopos1");

    $id = $_SESSION['id'];
    $queryb = mysqli_query($con, "select * from branch")or die(mysqli_error($con));
    $rowb = mysqli_fetch_array($queryb);
    $reciept_footer_text = $rowb['reciept_footer_text'];

    $branch = $_SESSION['branch'];


    /* Print a "Hello world" receipt" */
    $printer = new Printer($connector);

    // Add image logo for the client..  

    /*
      $tux = EscposImage::load("meat-logo.png");
      $printer->setJustification(Printer::JUSTIFY_CENTER);
      $printer->bitImage($tux);
     */
    
    $printer->text($rowb['branch_name'] . ", " . "\n");
    $printer->text($rowb['branch_address'] . ", " . "\n");
    $printer->text($rowb['branch_contact'] . "\n\n");
     
    // invoice info...

    $currentDateTime = date('Y-m-d H:i:s');

    if (isset($_GET['fromdate']) && !isset($_GET['cat_id'])) {

        $enddate = $_GET['enddate'];
        $fromdate = $_GET['fromdate'];

        $query = mysqli_query($con, "SELECT invoice,product.prod_name,stock_purchases_tb.date,stock_purchases_tb.qty,user.name FROM `stock_purchases_tb` INNER JOIN product ON product.prod_id= stock_purchases_tb.prod_id 
        INNER JOIN user on user.user_id=stock_purchases_tb.user_id
        AND DATE(stock_purchases_tb.date) BETWEEN '$fromdate' AND '$enddate' AND  stock_purchases_tb.status='purchased'")or die(mysqli_error($con));

        $printer->text("Stock Purchase Report " . $fromdate . " to " . $enddate . "\n");
        $printer->text("............................\n");
    } else if (isset($_GET['fromdate']) && isset($_GET['cat_id'])) {
        
        $enddate = $_GET['enddate'];
        $fromdate = $_GET['fromdate'];
        $cat_id = $_GET['cat_id'];

        $query = mysqli_query($con, "SELECT invoice,product.prod_name,stock_purchases_tb.date,stock_purchases_tb.qty,user.name FROM `stock_purchases_tb` INNER JOIN product ON product.prod_id= stock_purchases_tb.prod_id 
        INNER JOIN user on user.user_id=stock_purchases_tb.user_id
        AND DATE(stock_purchases_tb.date) BETWEEN '$fromdate' AND '$enddate' AND stock_purchases_tb.invoice='$cat_id'"
        . " AND  stock_purchases_tb.status='purchased'")or die(mysqli_error($con));

        $printer->text("Stock Purchase Report " . $fromdate . " to " . $enddate . "\n\n"." for Invoice # ".$cat_id );
        $printer->text("\n............................\n");
        
    } else {
        $printer->text("Todays Stock Purchase Report " . $currentDateTime . "\n");
        $printer->text("............................\n");

        $query = mysqli_query($con, "SELECT invoice,product.prod_name,stock_purchases_tb.date,stock_purchases_tb.qty,user.name FROM `stock_purchases_tb` INNER JOIN product ON product.prod_id= stock_purchases_tb.prod_id 
        INNER JOIN user on user.user_id=stock_purchases_tb.user_id
        AND date(stock_purchases_tb.date)=date(NOW()) AND  stock_purchases_tb.status='purchased'")or die(mysqli_error($con));
    }

    $grand = 0;
    $finalTotal = 0;
    $order_no = 0;
    $vatFinalTotal = 0;

    while ($row = mysqli_fetch_array($query)) {
        $finalTotal += $row['qty'];

        $prodName = $row['prod_name'];

        $printer->text($row['qty'] . "  " . substr($prodName, 0, 30) . "\n");
    }

    $printer->text("\n Total Stock Purchased " . number_format($finalTotal, 2) . "\n\n");

    $printer->text("Printed from Chesco POS Ver 3.0 \n\n");

    $printer->text("\n\n");

    $printer->cut();

    /* Close printer */
    $printer->close();

   echo "<script>document.location='../../../stock-purchases-report.php'</script>";
} catch (Exception $e) {
    echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
}
