<?php 
session_start();
if(empty($_SESSION['id'])):
    header('Location:../index.php');
endif;

include('../dist/includes/dbcon.php');

/**
 * Store Server
 */
if (isset($_POST['add_variant'])) {
    $id = $_POST['prod_id'];
    $color = $_POST['color'];
    $size = $_POST['size'];
    $qty = $_POST['qty'];

    // First, Check total qty of product
    $qty_query = mysqli_query($con, "SELECT prod_qty FROM product WHERE prod_id = '$id' ");
    $qty_row = mysqli_fetch_array($qty_query);
    $total_qty = $qty_row['prod_qty'];

    // Now check the quantity added so far in the DB, if any
    $added_qty_query = mysqli_query($con, "SELECT SUM(qty) AS qty FROM product_variants WHERE prod_id = '$id' ");
    $added_qty_row = mysqli_fetch_array($added_qty_query);
    $added_qty = $added_qty_row['qty'];
    
    // Check if there's enough products that hasn't been assighned to variants.
    $remaining_qty = $total_qty - $added_qty;
    if($remaining_qty <= 0){
        echo "<script type='text/javascript'>alert('Error! The quantity added to this variant is more than the available quantity for this product !!!');</script>";
        echo "<script>document.location='product_variants.php?id=$id'</script>";
    }

    // And now check if those remaining products are enough for the amount selected by user. And save if all is good.
    $validate_qty = $remaining_qty - $qty;
    if($validate_qty >= 0){

        $query2 = mysqli_query($con, "SELECT * FROM product_variants WHERE prod_id = '$id' && color = '$color' && size = '$size' ")or die(mysqli_error($con));
        $count = mysqli_num_rows($query2);
        
        if ($count > 0) {            
            echo "<script type='text/javascript'>alert('Variant Already exist !!!');</script>";
            echo "<script>document.location='product_variants.php?id=$id'</script>";
        } else {
        
            mysqli_query($con, "INSERT INTO product_variants (prod_id, color ,size, qty)
                                            VALUES('$id','$color','$size','$qty' )")or die(mysqli_error($con));
        
            echo "<script type='text/javascript'>alert('Successfully Created Variant !!!');</script>";
            echo "<script>document.location='product_variants.php?id=$id'</script>";
        }
    }else{
        echo "<script type='text/javascript'>alert('Error! The quantity added to this variant is more than the total quantity of the stock available for this product !!!');</script>";
        echo "<script>document.location='product_variants.php?id=$id'</script>";
    }

}

if (isset($_POST['update_variant'])) {
    $prod_id = $_POST['prod_id'];
    $id = $_POST['id'];
    $color = $_POST['color'];
    $size = $_POST['size'];
    $qty = $_POST['qty'];
	
	mysqli_query($con,"UPDATE product_variants SET color = '$color', size = '$size',qty = '$qty' WHERE id = '$id'")or die(mysqli_error($con));
	
	echo "<script type='text/javascript'>alert('Successfully Updated Attribute!');</script>";
	echo "<script>document.location='product_variants.php?id=$prod_id'</script>";  

}

if (isset($_POST['delete_variant'])) {
    $prod_id = $_POST['prod_id'];
    $id = $_POST['id'];
    mysqli_query($con, "DELETE FROM product_variants WHERE id = '$id'")or die(mysqli_error($con));
    
    echo "<script type='text/javascript'>alert('Successfully Deleted Attribute !!');</script>";
    echo "<script>document.location='product_variants.php?id=$prod_id'</script>";
}




/**
 * WAREHOUSE SERVER
 */
if (isset($_POST['add_variant_warehouse'])) {
    $id = $_POST['prod_id'];
    $color = $_POST['color'];
    $size = $_POST['size'];
    $qty = $_POST['qty'];

    // First, Check total qty of ware_house_tb
    $qty_queryWH = mysqli_query($con, "SELECT prod_qty FROM ware_house_tb WHERE prod_id = '$id' ");
    $qty_rowWH = mysqli_fetch_array($qty_queryWH);
    $total_qtyWH = $qty_rowWH['prod_qty'];

    // Now check the quantity added so far in the DB, if any
    $added_qty_queryWH = mysqli_query($con, "SELECT SUM(qty) AS qty FROM product_variants_warehouse WHERE prod_id = '$id' ");
    $added_qty_row = mysqli_fetch_array($added_qty_queryWH);
    $added_qtyWH = $added_qty_row['qty'];
    
    // Check if there's enough products in ware_house_tb that hasn't been assighned to variants.
    $remaining_qtyWH = $total_qtyWH - $added_qtyWH;
    if($remaining_qtyWH <= 0){
        echo "<script type='text/javascript'>alert('Error! The quantity added to this variant is more than the available quantity for this product !!!');</script>";
        echo "<script>document.location='product_variants_warehouse.php?id=$id'</script>";
    }

    // And now check if those remaining products are enough for the amount selected by user. And save if all is good.
    $validate_qtyWH = $remaining_qtyWH - $qty;
    if($validate_qtyWH >= 0){

        $query2 = mysqli_query($con, "SELECT * FROM product_variants_warehouse WHERE prod_id = '$id' && color = '$color' && size = '$size' ")or die(mysqli_error($con));
        $count = mysqli_num_rows($query2);
        
        if ($count > 0) {            
            echo "<script type='text/javascript'>alert('Variant Already exist !!!');</script>";
            echo "<script>document.location='product_variants_warehouse.php?id=$id'</script>";
        } else {
        
            mysqli_query($con, "INSERT INTO product_variants_warehouse (prod_id, color ,size, qty)
                                            VALUES('$id','$color','$size','$qty' )")or die(mysqli_error($con));
        
            echo "<script type='text/javascript'>alert('Successfully Created Variant !!!');</script>";
            echo "<script>document.location='product_variants_warehouse.php?id=$id'</script>";
        }
    }else{
        echo "<script type='text/javascript'>alert('Error! The quantity added to this variant is more than the total quantity of the stock available for this product !!!');</script>";
        echo "<script>document.location='product_variants_warehouse.php?id=$id'</script>";
    }

}



if (isset($_POST['update_variant_variant'])) {
    $prod_id = $_POST['prod_id'];
    $id = $_POST['id'];
    $color = $_POST['color'];
    $size = $_POST['size'];
    $qty = $_POST['qty'];
	
	mysqli_query($con,"UPDATE product_variants_warehouse SET color = '$color', size = '$size',qty = '$qty' WHERE id = '$id'")or die(mysqli_error($con));
	
	echo "<script type='text/javascript'>alert('Successfully Updated Attribute!');</script>";
	echo "<script>document.location='product_variants_warehouse.php?id=$prod_id'</script>";  

}


if (isset($_POST['delete_variant_warehouse'])) {
    $prod_id = $_POST['prod_id'];
    $id = $_POST['id'];
    mysqli_query($con, "DELETE FROM product_variants_warehouse WHERE id = '$id'")or die(mysqli_error($con));
    
    echo "<script type='text/javascript'>alert('Successfully Deleted Attribute !!');</script>";
    echo "<script>document.location='product_variants_warehouse.php?id=$prod_id'</script>";
}