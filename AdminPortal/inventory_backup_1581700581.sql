

CREATE TABLE `advance_payments_tb` (
  `advace_id` int(12) NOT NULL AUTO_INCREMENT,
  `amount` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(12) NOT NULL,
  `prod_id` int(12) NOT NULL,
  `qty` int(12) NOT NULL,
  `branch_id` int(12) NOT NULL,
  `order_no` int(12) NOT NULL,
  `customer_name` text NOT NULL,
  `price` int(12) NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`advace_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO advance_payments_tb VALUES("1","10","2019-10-09 12:40:03","8","371","1","1","2747","USER","60","Sold");
INSERT INTO advance_payments_tb VALUES("2","10","2019-10-09 12:40:03","8","172","1","1","2747","USER","11","Sold");
INSERT INTO advance_payments_tb VALUES("3","60","2019-10-09 12:46:19","8","371","1","1","4679","Choolwe","60","Sold");



CREATE TABLE `branch` (
  `branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(50) NOT NULL,
  `branch_address` varchar(100) NOT NULL,
  `branch_contact` varchar(50) NOT NULL,
  `reciept_footer_text` text NOT NULL,
  `notification_count` int(12) NOT NULL,
  `skin` varchar(15) NOT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO branch VALUES("1","THE AMALGAM ","Lusaka, Zambia.","0977754735","Thank you for shopping with us. ","5","red");



CREATE TABLE `cashout_limits_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `cashoutlimit` text NOT NULL,
  `status` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO cashout_limits_tb VALUES("1","20000","Not Active","2019-10-05 13:03:19");



CREATE TABLE `category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(30) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;

INSERT INTO category VALUES("72","HOME");
INSERT INTO category VALUES("73","FOOD");
INSERT INTO category VALUES("74","MEAT");
INSERT INTO category VALUES("75","FROZEN");
INSERT INTO category VALUES("76","FRESH");
INSERT INTO category VALUES("77","DRINKS");
INSERT INTO category VALUES("78","CONSIGNMENT");



CREATE TABLE `customer` (
  `cust_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_first` varchar(50) NOT NULL,
  `cust_last` varchar(30) NOT NULL,
  `cust_address` varchar(100) NOT NULL,
  `cust_contact` varchar(30) NOT NULL,
  `balance` decimal(10,2) NOT NULL,
  `cust_pic` varchar(300) NOT NULL,
  `bday` date NOT NULL,
  `nickname` varchar(30) NOT NULL,
  `house_status` varchar(30) NOT NULL,
  `years` varchar(20) NOT NULL,
  `rent` varchar(10) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `emp_no` varchar(30) NOT NULL,
  `emp_address` varchar(100) NOT NULL,
  `emp_year` varchar(10) NOT NULL,
  `occupation` varchar(30) NOT NULL,
  `salary` varchar(30) NOT NULL,
  `spouse` varchar(30) NOT NULL,
  `spouse_no` varchar(30) NOT NULL,
  `spouse_emp` varchar(50) NOT NULL,
  `spouse_details` varchar(100) NOT NULL,
  `spouse_income` decimal(10,2) NOT NULL,
  `comaker` varchar(30) NOT NULL,
  `comaker_details` varchar(100) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `credit_status` varchar(10) NOT NULL,
  `ci_remarks` varchar(1000) NOT NULL,
  `ci_name` varchar(50) NOT NULL,
  `ci_date` date NOT NULL,
  `payslip` int(11) NOT NULL,
  `valid_id` int(11) NOT NULL,
  `cert` int(11) NOT NULL,
  `cedula` int(11) NOT NULL,
  `income` int(11) NOT NULL,
  `email` text NOT NULL,
  PRIMARY KEY (`cust_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `damages_log_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(12) NOT NULL,
  `qty_damage` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `demo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(100) NOT NULL,
  `state` varchar(40) NOT NULL,
  `city` varchar(40) NOT NULL,
  `pin` varchar(40) NOT NULL,
  `submission_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO demo VALUES("1","Zambia","21111","Chilenje","212121","2019-10-02");
INSERT INTO demo VALUES("2","Congo","21111","Congo","212121","2019-10-02");
INSERT INTO demo VALUES("3","South Africa","21111","Congo","212121","2019-10-02");



CREATE TABLE `discount_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `discount_price` int(12) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `discount_from` text NOT NULL,
  `discount_to` text NOT NULL,
  `status` text NOT NULL,
  `price_before_disc` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO discount_tb VALUES("1","898","50","2019-11-29 09:57:48","2019-11-28 00:00:00","2019-11-30 00:00:00","notactive","100");



CREATE TABLE `draft_sales_tb` (
  `sales_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `order_no` int(12) NOT NULL,
  `client_name` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`sales_details_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `draft_temp_trans` (
  `temp_trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `order_no` int(12) NOT NULL,
  `customer_name` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(12) NOT NULL,
  `is_printed` int(11) NOT NULL,
  `special_remarks` text NOT NULL,
  PRIMARY KEY (`temp_trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `expenses_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `amount` int(12) NOT NULL,
  `date` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `history_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

INSERT INTO history_log VALUES("1","8","has logged in the system at ","2020-01-28 18:09:53");
INSERT INTO history_log VALUES("2","4","has logged in the system at ","2020-01-28 19:32:52");
INSERT INTO history_log VALUES("3","4","has logged out the system at ","2020-01-28 20:18:08");
INSERT INTO history_log VALUES("4","8","has logged in the system at ","2020-01-28 20:18:14");
INSERT INTO history_log VALUES("5","8","has logged in the system at ","2020-01-28 22:57:43");
INSERT INTO history_log VALUES("6","8","has logged in the system at ","2020-01-29 15:14:32");
INSERT INTO history_log VALUES("7","8","has logged out the system at ","2020-01-29 15:54:35");
INSERT INTO history_log VALUES("8","4","has logged in the system at ","2020-01-29 15:57:52");
INSERT INTO history_log VALUES("9","8","has logged in the system at ","2020-01-30 11:28:29");
INSERT INTO history_log VALUES("10","4","has logged in the system at ","2020-01-30 16:15:23");
INSERT INTO history_log VALUES("11","8","has logged in the system at ","2020-01-30 20:52:58");
INSERT INTO history_log VALUES("12","8","has logged in the system at ","2020-01-30 21:00:48");
INSERT INTO history_log VALUES("13","4","has logged in the system at ","2020-01-30 21:01:16");
INSERT INTO history_log VALUES("14","4","has logged in the system at ","2020-01-30 21:01:58");
INSERT INTO history_log VALUES("15","4","has logged in the system at ","2020-02-04 08:25:36");
INSERT INTO history_log VALUES("16","4","has logged in the system at ","2020-02-05 10:32:22");
INSERT INTO history_log VALUES("17","4","has logged in the system at ","2020-02-05 22:41:27");
INSERT INTO history_log VALUES("18","4","has logged out the system at ","2020-02-05 22:42:20");
INSERT INTO history_log VALUES("19","8","has logged in the system at ","2020-02-05 22:43:16");
INSERT INTO history_log VALUES("20","8","has logged out the system at ","2020-02-05 22:43:22");
INSERT INTO history_log VALUES("21","4","has logged in the system at ","2020-02-05 22:43:27");
INSERT INTO history_log VALUES("22","4","has logged out the system at ","2020-02-05 22:43:56");
INSERT INTO history_log VALUES("23","4","has logged in the system at ","2020-02-05 22:44:01");
INSERT INTO history_log VALUES("24","4","has logged out the system at ","2020-02-05 22:44:04");
INSERT INTO history_log VALUES("25","8","has logged in the system at ","2020-02-05 22:44:10");
INSERT INTO history_log VALUES("26","4","has logged in the system at ","2020-02-05 22:44:43");
INSERT INTO history_log VALUES("27","4","has logged out the system at ","2020-02-05 22:45:48");
INSERT INTO history_log VALUES("28","8","has logged in the system at ","2020-02-05 22:45:56");
INSERT INTO history_log VALUES("29","4","has logged in the system at ","2020-02-05 22:46:35");
INSERT INTO history_log VALUES("30","4","has logged in the system at ","2020-02-06 11:30:48");
INSERT INTO history_log VALUES("31","4","has logged out the system at ","2020-02-06 12:38:44");
INSERT INTO history_log VALUES("32","4","has logged in the system at ","2020-02-06 15:14:15");
INSERT INTO history_log VALUES("33","8","has logged in the system at ","2020-02-06 16:51:50");
INSERT INTO history_log VALUES("34","8","has logged out the system at ","2020-02-06 17:00:52");
INSERT INTO history_log VALUES("35","4","has logged in the system at ","2020-02-06 17:00:57");
INSERT INTO history_log VALUES("36","4","has logged out the system at ","2020-02-06 17:01:04");
INSERT INTO history_log VALUES("37","8","has logged in the system at ","2020-02-06 17:01:14");
INSERT INTO history_log VALUES("38","8","has logged in the system at ","2020-02-07 13:46:05");
INSERT INTO history_log VALUES("39","8","has logged in the system at ","2020-02-09 19:21:42");
INSERT INTO history_log VALUES("40","8","has logged out the system at ","2020-02-09 19:24:40");
INSERT INTO history_log VALUES("41","8","has logged in the system at ","2020-02-10 15:26:07");
INSERT INTO history_log VALUES("42","4","has logged in the system at ","2020-02-14 16:21:47");
INSERT INTO history_log VALUES("43","4","has logged in the system at ","2020-02-14 18:55:17");



CREATE TABLE `inv_damages_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `inv_id` int(12) NOT NULL,
  `quantity` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `inventory_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `quantity` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `invoices_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `order_no` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO invoices_tb VALUES("1","533");
INSERT INTO invoices_tb VALUES("2","533");
INSERT INTO invoices_tb VALUES("3","874");
INSERT INTO invoices_tb VALUES("4","3836");
INSERT INTO invoices_tb VALUES("5","3836");



CREATE TABLE `licience_reg_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `exp_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `licience_key` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

INSERT INTO licience_reg_tb VALUES("1","2020-12-31 00:00:00","");



CREATE TABLE `modes_of_payment_tb` (
  `payment_mode_id` int(12) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`payment_mode_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO modes_of_payment_tb VALUES("1","Cash","2019-11-03 18:29:35");
INSERT INTO modes_of_payment_tb VALUES("3","MTN Mobile Money","2019-11-03 18:40:42");
INSERT INTO modes_of_payment_tb VALUES("4","VISA","2019-11-05 09:22:27");



CREATE TABLE `notifications_settings_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `not_count` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `open_close_cashout_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `login` text NOT NULL,
  `logout` text NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=244 DEFAULT CHARSET=latin1;

INSERT INTO open_close_cashout_tb VALUES("69","9","2019-05-01 16:26:56","2019-05-01 16:38:52","closed");
INSERT INTO open_close_cashout_tb VALUES("70","9","2019-05-01 17:47:22","2019-05-01 17:47:36","closed");
INSERT INTO open_close_cashout_tb VALUES("71","9","2019-05-02 08:05:35","2019-05-02 08:53:45","closed");
INSERT INTO open_close_cashout_tb VALUES("72","9","2019-05-02 09:15:44","2019-05-03 17:21:56","closed");
INSERT INTO open_close_cashout_tb VALUES("73","9","2019-05-04 07:52:51","2019-05-06 08:43:56","closed");
INSERT INTO open_close_cashout_tb VALUES("74","9","2019-05-06 09:31:14","2019-05-06 17:00:53","closed");
INSERT INTO open_close_cashout_tb VALUES("75","8","2019-05-06 13:02:54","2019-05-06 13:03:43","closed");
INSERT INTO open_close_cashout_tb VALUES("76","9","2019-05-06 17:21:32","2019-05-06 17:52:22","closed");
INSERT INTO open_close_cashout_tb VALUES("77","9","2019-05-07 07:42:09","2019-05-07 09:10:06","closed");
INSERT INTO open_close_cashout_tb VALUES("78","9","2019-05-07 09:10:13","2019-05-07 09:10:16","closed");
INSERT INTO open_close_cashout_tb VALUES("79","9","2019-05-07 09:18:20","2019-05-07 11:32:03","closed");
INSERT INTO open_close_cashout_tb VALUES("80","9","2019-05-07 15:07:30","2019-05-08 08:44:27","closed");
INSERT INTO open_close_cashout_tb VALUES("81","9","2019-05-08 08:57:30","2019-05-08 09:03:21","closed");
INSERT INTO open_close_cashout_tb VALUES("82","9","2019-05-08 09:06:08","2019-05-08 09:15:43","closed");
INSERT INTO open_close_cashout_tb VALUES("83","9","2019-05-08 09:30:18","2019-05-08 09:30:37","closed");
INSERT INTO open_close_cashout_tb VALUES("84","9","2019-05-08 10:32:11","2019-05-08 10:50:35","closed");
INSERT INTO open_close_cashout_tb VALUES("85","13","2019-05-08 10:50:24","2019-05-08 10:51:16","closed");
INSERT INTO open_close_cashout_tb VALUES("86","9","2019-05-08 10:52:58","2019-05-08 18:58:58","closed");
INSERT INTO open_close_cashout_tb VALUES("87","9","2019-05-08 19:02:44","2019-05-08 20:54:59","closed");
INSERT INTO open_close_cashout_tb VALUES("88","9","2019-05-09 08:06:22","2019-05-09 18:55:12","closed");
INSERT INTO open_close_cashout_tb VALUES("89","8","2019-05-09 14:35:30","2019-05-09 14:36:34","closed");
INSERT INTO open_close_cashout_tb VALUES("90","9","2019-05-10 08:10:45","2019-05-10 18:32:31","closed");
INSERT INTO open_close_cashout_tb VALUES("91","9","2019-05-11 07:58:20","2019-05-11 21:29:49","closed");
INSERT INTO open_close_cashout_tb VALUES("92","9","2019-05-13 08:07:25","2019-05-13 08:44:14","closed");
INSERT INTO open_close_cashout_tb VALUES("93","9","2019-05-13 08:46:20","2019-05-13 11:22:16","closed");
INSERT INTO open_close_cashout_tb VALUES("94","9","2019-05-13 12:07:54","2019-05-13 17:43:31","closed");
INSERT INTO open_close_cashout_tb VALUES("95","9","2019-05-14 07:45:44","2019-05-14 17:47:48","closed");
INSERT INTO open_close_cashout_tb VALUES("96","9","2019-05-15 08:07:26","2019-05-16 18:47:22","closed");
INSERT INTO open_close_cashout_tb VALUES("97","9","2019-05-17 08:12:21","2019-05-17 17:48:00","closed");
INSERT INTO open_close_cashout_tb VALUES("98","9","2019-05-18 08:17:03","2019-05-18 17:37:41","closed");
INSERT INTO open_close_cashout_tb VALUES("99","9","2019-05-20 08:19:52","2019-05-20 17:35:27","closed");
INSERT INTO open_close_cashout_tb VALUES("100","9","2019-05-21 08:14:43","2019-05-21 17:06:59","closed");
INSERT INTO open_close_cashout_tb VALUES("101","9","2019-05-21 17:35:55","2019-05-21 17:36:29","closed");
INSERT INTO open_close_cashout_tb VALUES("102","9","2019-05-21 18:25:02","2019-05-21 18:28:40","closed");
INSERT INTO open_close_cashout_tb VALUES("103","9","2019-05-22 07:51:03","2019-05-22 09:00:57","closed");
INSERT INTO open_close_cashout_tb VALUES("104","9","2019-05-22 14:28:27","2019-05-22 18:26:45","closed");
INSERT INTO open_close_cashout_tb VALUES("105","8","2019-05-22 16:56:53","2019-05-22 16:57:32","closed");
INSERT INTO open_close_cashout_tb VALUES("106","9","2019-05-23 08:17:22","2019-05-23 19:06:48","closed");
INSERT INTO open_close_cashout_tb VALUES("107","9","2019-05-23 19:26:22","2019-05-23 19:26:44","closed");
INSERT INTO open_close_cashout_tb VALUES("108","9","2019-05-24 08:10:19","2019-05-24 21:23:54","closed");
INSERT INTO open_close_cashout_tb VALUES("109","13","2019-05-24 10:09:54","","");
INSERT INTO open_close_cashout_tb VALUES("110","9","2019-05-25 09:05:30","2019-05-27 17:36:35","closed");
INSERT INTO open_close_cashout_tb VALUES("111","9","2019-05-28 08:09:46","2019-05-28 08:51:46","closed");
INSERT INTO open_close_cashout_tb VALUES("112","9","2019-05-28 09:27:14","2019-05-28 09:31:26","closed");
INSERT INTO open_close_cashout_tb VALUES("113","9","2019-05-28 11:03:32","2019-05-29 21:31:24","closed");
INSERT INTO open_close_cashout_tb VALUES("114","9","2019-05-29 21:31:40","2019-05-29 21:32:10","closed");
INSERT INTO open_close_cashout_tb VALUES("115","9","2019-05-30 08:22:30","2019-05-30 09:24:18","closed");
INSERT INTO open_close_cashout_tb VALUES("116","9","2019-05-30 09:24:24","2019-05-30 09:24:32","closed");
INSERT INTO open_close_cashout_tb VALUES("117","9","2019-05-30 13:20:46","2019-05-30 17:56:44","closed");
INSERT INTO open_close_cashout_tb VALUES("118","9","2019-05-31 09:38:09","2019-05-31 20:10:08","closed");
INSERT INTO open_close_cashout_tb VALUES("119","9","2019-06-01 08:33:59","2019-06-01 08:36:35","closed");
INSERT INTO open_close_cashout_tb VALUES("120","9","2019-06-01 08:38:18","2019-06-01 18:01:10","closed");
INSERT INTO open_close_cashout_tb VALUES("121","9","2019-06-03 08:25:37","2019-06-03 18:36:43","closed");
INSERT INTO open_close_cashout_tb VALUES("122","9","2019-06-03 18:37:39","2019-06-03 18:40:15","closed");
INSERT INTO open_close_cashout_tb VALUES("123","9","2019-06-03 18:47:25","2019-06-03 18:47:49","closed");
INSERT INTO open_close_cashout_tb VALUES("124","9","2019-06-04 08:19:15","2019-06-04 18:35:13","closed");
INSERT INTO open_close_cashout_tb VALUES("125","9","2019-06-04 18:35:26","2019-06-04 18:35:39","closed");
INSERT INTO open_close_cashout_tb VALUES("126","9","2019-06-05 08:25:14","2019-06-05 17:47:52","closed");
INSERT INTO open_close_cashout_tb VALUES("127","9","2019-06-05 17:48:05","2019-06-05 17:48:17","closed");
INSERT INTO open_close_cashout_tb VALUES("128","9","2019-06-06 08:16:25","2019-06-06 19:16:08","closed");
INSERT INTO open_close_cashout_tb VALUES("129","9","2019-06-06 19:16:22","2019-06-06 19:16:40","closed");
INSERT INTO open_close_cashout_tb VALUES("130","9","2019-06-07 08:17:57","2019-06-07 18:06:53","closed");
INSERT INTO open_close_cashout_tb VALUES("131","9","2019-06-07 20:30:38","2019-06-07 20:30:51","closed");
INSERT INTO open_close_cashout_tb VALUES("132","9","2019-06-08 08:54:39","2019-06-08 18:06:07","closed");
INSERT INTO open_close_cashout_tb VALUES("133","9","2019-06-08 18:06:22","2019-06-08 18:06:47","closed");
INSERT INTO open_close_cashout_tb VALUES("134","9","2019-06-10 08:46:29","2019-06-10 08:47:45","closed");
INSERT INTO open_close_cashout_tb VALUES("135","9","2019-06-10 08:47:58","2019-06-10 19:31:14","closed");
INSERT INTO open_close_cashout_tb VALUES("136","9","2019-06-10 19:31:30","2019-06-10 19:32:02","closed");
INSERT INTO open_close_cashout_tb VALUES("137","9","2019-06-11 08:08:14","2019-06-11 19:52:57","closed");
INSERT INTO open_close_cashout_tb VALUES("138","9","2019-06-11 19:53:20","2019-06-11 19:53:28","closed");
INSERT INTO open_close_cashout_tb VALUES("139","9","2019-06-12 07:43:32","2019-06-12 20:20:08","closed");
INSERT INTO open_close_cashout_tb VALUES("140","9","2019-06-13 08:21:09","2019-06-13 17:40:56","closed");
INSERT INTO open_close_cashout_tb VALUES("141","9","2019-06-13 17:41:08","2019-06-13 17:41:30","closed");
INSERT INTO open_close_cashout_tb VALUES("142","9","2019-06-14 08:13:49","2019-06-14 21:49:14","closed");
INSERT INTO open_close_cashout_tb VALUES("143","9","2019-06-14 21:49:25","2019-06-14 21:49:45","closed");
INSERT INTO open_close_cashout_tb VALUES("144","9","2019-06-15 08:20:02","2019-06-15 19:48:56","closed");
INSERT INTO open_close_cashout_tb VALUES("145","9","2019-06-15 19:49:06","2019-06-15 19:49:09","closed");
INSERT INTO open_close_cashout_tb VALUES("146","9","2019-06-15 19:49:20","2019-06-15 19:49:32","closed");
INSERT INTO open_close_cashout_tb VALUES("147","9","2019-06-15 19:52:47","2019-06-15 19:52:50","closed");
INSERT INTO open_close_cashout_tb VALUES("148","9","2019-06-17 08:15:02","2019-06-17 17:48:27","closed");
INSERT INTO open_close_cashout_tb VALUES("149","9","2019-06-17 17:48:39","2019-06-17 19:53:31","closed");
INSERT INTO open_close_cashout_tb VALUES("150","9","2019-06-18 08:28:55","2019-06-18 19:19:22","closed");
INSERT INTO open_close_cashout_tb VALUES("151","9","2019-06-19 08:13:54","2019-06-19 17:38:37","closed");
INSERT INTO open_close_cashout_tb VALUES("152","9","2019-06-20 08:13:36","2019-06-20 17:56:07","closed");
INSERT INTO open_close_cashout_tb VALUES("153","9","2019-06-20 08:13:36","2019-06-20 17:56:07","closed");
INSERT INTO open_close_cashout_tb VALUES("154","9","2019-06-21 07:03:19","2019-06-21 22:00:08","closed");
INSERT INTO open_close_cashout_tb VALUES("155","9","2019-06-22 08:41:47","2019-06-22 20:06:40","closed");
INSERT INTO open_close_cashout_tb VALUES("156","9","2019-06-24 08:58:21","2019-06-24 17:44:16","closed");
INSERT INTO open_close_cashout_tb VALUES("157","9","2019-06-24 17:45:00","2019-06-24 21:30:42","closed");
INSERT INTO open_close_cashout_tb VALUES("158","9","2019-06-24 22:20:19","","");
INSERT INTO open_close_cashout_tb VALUES("159","8","2019-06-25 15:05:54","2019-06-25 15:07:37","closed");
INSERT INTO open_close_cashout_tb VALUES("160","8","2019-06-25 16:40:23","2019-07-16 16:54:17","closed");
INSERT INTO open_close_cashout_tb VALUES("161","8","2019-07-22 20:00:22","2019-07-23 08:53:35","closed");
INSERT INTO open_close_cashout_tb VALUES("162","8","2019-07-31 13:12:01","2019-07-31 13:19:14","closed");
INSERT INTO open_close_cashout_tb VALUES("163","8","2019-07-31 13:19:21","2019-08-04 11:54:18","closed");
INSERT INTO open_close_cashout_tb VALUES("164","8","2019-08-04 11:54:45","2019-08-06 11:58:16","closed");
INSERT INTO open_close_cashout_tb VALUES("165","8","2019-08-06 12:12:03","2019-08-06 12:14:26","closed");
INSERT INTO open_close_cashout_tb VALUES("166","8","2019-08-06 14:15:34","2019-08-06 14:15:39","closed");
INSERT INTO open_close_cashout_tb VALUES("167","8","2019-08-08 14:58:09","2019-08-08 14:58:23","closed");
INSERT INTO open_close_cashout_tb VALUES("168","8","2019-08-08 15:02:09","2019-08-08 15:03:41","closed");
INSERT INTO open_close_cashout_tb VALUES("169","8","2019-08-08 15:09:59","2019-08-08 15:14:24","closed");
INSERT INTO open_close_cashout_tb VALUES("170","8","2019-08-08 17:19:30","2019-08-10 14:16:00","closed");
INSERT INTO open_close_cashout_tb VALUES("171","8","2019-08-10 14:16:12","2019-08-11 15:53:32","closed");
INSERT INTO open_close_cashout_tb VALUES("172","8","2019-08-11 16:23:27","2019-08-11 17:20:59","closed");
INSERT INTO open_close_cashout_tb VALUES("173","8","2019-08-11 17:21:05","2019-08-12 08:19:53","closed");
INSERT INTO open_close_cashout_tb VALUES("174","8","2019-08-12 08:20:01","2019-08-12 08:21:37","closed");
INSERT INTO open_close_cashout_tb VALUES("175","8","2019-08-12 13:59:07","2019-08-12 17:04:27","closed");
INSERT INTO open_close_cashout_tb VALUES("176","8","2019-08-12 17:04:32","2019-08-12 17:04:34","closed");
INSERT INTO open_close_cashout_tb VALUES("177","8","2019-08-13 08:14:56","2019-08-14 09:52:09","closed");
INSERT INTO open_close_cashout_tb VALUES("178","8","2019-08-14 10:28:35","2019-08-14 11:27:21","closed");
INSERT INTO open_close_cashout_tb VALUES("179","8","2019-08-15 13:57:23","2019-08-16 11:06:11","closed");
INSERT INTO open_close_cashout_tb VALUES("180","8","2019-08-17 15:16:30","2019-09-04 19:46:15","closed");
INSERT INTO open_close_cashout_tb VALUES("181","8","2019-09-04 19:51:04","2019-09-04 19:51:50","closed");
INSERT INTO open_close_cashout_tb VALUES("182","8","2019-09-06 17:08:12","2019-09-07 11:16:15","closed");
INSERT INTO open_close_cashout_tb VALUES("183","8","2019-09-18 08:54:36","2019-09-18 08:56:28","closed");
INSERT INTO open_close_cashout_tb VALUES("184","8","2019-09-18 10:38:23","2019-09-18 10:38:53","closed");
INSERT INTO open_close_cashout_tb VALUES("185","8","2019-09-18 11:01:23","2019-09-18 11:10:49","closed");
INSERT INTO open_close_cashout_tb VALUES("186","8","2019-09-18 11:58:36","2019-09-20 15:17:37","closed");
INSERT INTO open_close_cashout_tb VALUES("187","8","2019-09-28 13:08:04","2019-09-28 13:14:03","closed");
INSERT INTO open_close_cashout_tb VALUES("188","8","2019-10-05 19:25:47","2019-10-05 19:25:57","closed");
INSERT INTO open_close_cashout_tb VALUES("189","8","2019-10-05 19:54:42","2019-10-05 23:37:02","closed");
INSERT INTO open_close_cashout_tb VALUES("190","8","2019-10-05 23:37:07","2019-10-05 23:44:36","closed");
INSERT INTO open_close_cashout_tb VALUES("191","8","2019-10-05 23:44:42","2019-10-05 23:46:28","closed");
INSERT INTO open_close_cashout_tb VALUES("192","8","2019-10-05 23:48:46","2019-10-05 23:49:37","closed");
INSERT INTO open_close_cashout_tb VALUES("193","8","2019-10-05 23:49:46","2019-10-05 23:51:12","closed");
INSERT INTO open_close_cashout_tb VALUES("194","8","2019-10-05 23:51:36","2019-10-07 10:48:26","closed");
INSERT INTO open_close_cashout_tb VALUES("195","8","2019-10-08 08:12:56","2019-10-08 08:44:17","closed");
INSERT INTO open_close_cashout_tb VALUES("196","8","2019-10-08 23:45:50","2019-10-10 14:21:42","closed");
INSERT INTO open_close_cashout_tb VALUES("197","8","2019-10-13 16:03:33","2019-10-13 18:29:16","closed");
INSERT INTO open_close_cashout_tb VALUES("198","8","2019-10-18 08:10:01","2019-10-18 08:10:18","closed");
INSERT INTO open_close_cashout_tb VALUES("199","8","2019-10-18 08:12:36","2019-10-18 08:15:10","closed");
INSERT INTO open_close_cashout_tb VALUES("200","8","2019-10-25 19:20:03","2019-10-25 20:48:14","closed");
INSERT INTO open_close_cashout_tb VALUES("201","8","2019-10-27 20:02:02","2019-10-28 08:09:22","closed");
INSERT INTO open_close_cashout_tb VALUES("202","8","2019-10-28 13:54:12","2019-10-28 13:54:28","closed");
INSERT INTO open_close_cashout_tb VALUES("203","8","2019-10-28 13:55:13","2019-10-28 13:57:27","closed");
INSERT INTO open_close_cashout_tb VALUES("204","8","2019-10-28 14:00:19","2019-10-28 14:15:38","closed");
INSERT INTO open_close_cashout_tb VALUES("205","8","2019-11-03 16:32:48","2019-11-03 19:00:25","closed");
INSERT INTO open_close_cashout_tb VALUES("206","8","2019-11-07 19:04:05","2019-11-07 19:07:17","closed");
INSERT INTO open_close_cashout_tb VALUES("207","8","2019-11-07 19:19:12","2019-11-10 17:48:45","closed");
INSERT INTO open_close_cashout_tb VALUES("208","8","2019-11-11 09:19:58","2019-11-11 09:30:25","closed");
INSERT INTO open_close_cashout_tb VALUES("209","8","2019-11-11 09:31:09","2019-11-11 10:19:25","closed");
INSERT INTO open_close_cashout_tb VALUES("210","8","2019-11-11 16:11:27","2019-11-11 16:25:21","closed");
INSERT INTO open_close_cashout_tb VALUES("211","8","2019-11-11 16:32:44","2019-11-11 16:45:30","closed");
INSERT INTO open_close_cashout_tb VALUES("212","8","2019-11-11 16:46:56","2019-11-12 15:55:24","closed");
INSERT INTO open_close_cashout_tb VALUES("213","8","2019-11-16 15:06:46","2019-11-19 12:07:17","closed");
INSERT INTO open_close_cashout_tb VALUES("214","8","2019-11-19 12:26:49","2019-11-19 12:32:14","closed");
INSERT INTO open_close_cashout_tb VALUES("215","8","2019-11-19 18:02:31","2019-11-21 19:11:22","closed");
INSERT INTO open_close_cashout_tb VALUES("216","8","2019-11-21 19:13:47","2019-11-26 16:09:13","closed");
INSERT INTO open_close_cashout_tb VALUES("217","8","2019-11-27 16:54:49","2019-11-29 22:31:17","closed");
INSERT INTO open_close_cashout_tb VALUES("218","11","2019-11-29 22:32:16","","");
INSERT INTO open_close_cashout_tb VALUES("219","8","2019-11-29 23:17:55","2019-12-12 11:30:23","closed");
INSERT INTO open_close_cashout_tb VALUES("220","8","2019-12-12 17:38:34","2019-12-14 17:13:24","closed");
INSERT INTO open_close_cashout_tb VALUES("221","8","2019-12-14 17:13:57","2019-12-14 17:25:56","closed");
INSERT INTO open_close_cashout_tb VALUES("222","8","2019-12-14 17:26:01","2019-12-14 17:26:33","closed");
INSERT INTO open_close_cashout_tb VALUES("223","8","2019-12-15 16:11:25","2019-12-15 16:11:35","closed");
INSERT INTO open_close_cashout_tb VALUES("224","8","2019-12-15 16:14:24","2019-12-15 21:18:23","closed");
INSERT INTO open_close_cashout_tb VALUES("225","8","2019-12-15 21:20:53","2019-12-15 21:24:58","closed");
INSERT INTO open_close_cashout_tb VALUES("226","8","2019-12-16 15:52:22","2019-12-17 14:36:09","closed");
INSERT INTO open_close_cashout_tb VALUES("227","8","2019-12-17 14:36:32","2019-12-17 14:36:54","closed");
INSERT INTO open_close_cashout_tb VALUES("228","8","2019-12-17 15:45:56","2019-12-17 15:46:23","closed");
INSERT INTO open_close_cashout_tb VALUES("229","8","2019-12-17 15:47:12","2019-12-17 15:53:13","closed");
INSERT INTO open_close_cashout_tb VALUES("230","8","2019-12-17 16:42:26","2019-12-19 09:45:02","closed");
INSERT INTO open_close_cashout_tb VALUES("231","8","2019-12-19 09:45:14","2019-12-19 09:49:05","closed");
INSERT INTO open_close_cashout_tb VALUES("232","8","2019-12-19 10:22:05","2019-12-19 10:22:18","closed");
INSERT INTO open_close_cashout_tb VALUES("233","8","2019-12-19 10:30:14","2019-12-19 10:42:08","closed");
INSERT INTO open_close_cashout_tb VALUES("234","8","2019-12-19 10:50:07","2019-12-19 10:50:38","closed");
INSERT INTO open_close_cashout_tb VALUES("235","8","2019-12-19 14:56:32","2019-12-20 11:53:28","closed");
INSERT INTO open_close_cashout_tb VALUES("236","8","2019-12-21 18:05:30","2019-12-22 16:46:39","closed");
INSERT INTO open_close_cashout_tb VALUES("237","8","2019-12-22 16:47:30","2019-12-22 16:48:06","closed");
INSERT INTO open_close_cashout_tb VALUES("238","8","2019-12-23 10:44:19","2019-12-25 12:06:19","closed");
INSERT INTO open_close_cashout_tb VALUES("239","8","2019-12-28 18:26:00","2020-01-29 15:54:35","closed");
INSERT INTO open_close_cashout_tb VALUES("240","8","2020-01-30 11:28:29","2020-02-05 22:43:22","closed");
INSERT INTO open_close_cashout_tb VALUES("241","8","2020-02-05 22:44:10","2020-02-06 17:00:52","closed");
INSERT INTO open_close_cashout_tb VALUES("242","8","2020-02-06 17:01:14","2020-02-09 19:24:40","closed");
INSERT INTO open_close_cashout_tb VALUES("243","8","2020-02-10 15:26:07","","");



CREATE TABLE `open_close_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `open_bal` int(12) NOT NULL,
  `close_bal` int(12) NOT NULL,
  `date` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1297 DEFAULT CHARSET=latin1;

INSERT INTO open_close_tb VALUES("138","458","60","53","2019-05-06");
INSERT INTO open_close_tb VALUES("139","320","27","0","2019-05-06");
INSERT INTO open_close_tb VALUES("140","295","143","0","2019-05-06");
INSERT INTO open_close_tb VALUES("141","327","13","0","2019-05-06");
INSERT INTO open_close_tb VALUES("142","319","82","0","2019-05-06");
INSERT INTO open_close_tb VALUES("143","446","48","0","2019-05-06");
INSERT INTO open_close_tb VALUES("144","27","14","0","2019-05-06");
INSERT INTO open_close_tb VALUES("145","98","31","0","2019-05-06");
INSERT INTO open_close_tb VALUES("146","211","2","0","2019-05-06");
INSERT INTO open_close_tb VALUES("147","159","19","0","2019-05-06");
INSERT INTO open_close_tb VALUES("148","445","86","0","2019-05-06");
INSERT INTO open_close_tb VALUES("149","307","3","0","2019-05-06");
INSERT INTO open_close_tb VALUES("150","436","76","0","2019-05-06");
INSERT INTO open_close_tb VALUES("151","209","19","0","2019-05-06");
INSERT INTO open_close_tb VALUES("152","212","3","0","2019-05-06");
INSERT INTO open_close_tb VALUES("153","397","12","0","2019-05-06");
INSERT INTO open_close_tb VALUES("154","129","21","0","2019-05-06");
INSERT INTO open_close_tb VALUES("155","128","118","0","2019-05-06");
INSERT INTO open_close_tb VALUES("156","373","47","0","2019-05-06");
INSERT INTO open_close_tb VALUES("157","5","54","0","2019-05-06");
INSERT INTO open_close_tb VALUES("158","459","1","0","2019-05-06");
INSERT INTO open_close_tb VALUES("159","130","4","0","2019-05-06");
INSERT INTO open_close_tb VALUES("160","294","29","0","2019-05-06");
INSERT INTO open_close_tb VALUES("161","492","103","92","2019-05-07");
INSERT INTO open_close_tb VALUES("162","237","16","14","2019-05-07");
INSERT INTO open_close_tb VALUES("163","440","108","94","2019-05-07");
INSERT INTO open_close_tb VALUES("164","252","28","0","2019-05-07");
INSERT INTO open_close_tb VALUES("165","174","4","0","2019-05-07");
INSERT INTO open_close_tb VALUES("166","160","43","0","2019-05-07");
INSERT INTO open_close_tb VALUES("167","449","76","0","2019-05-07");
INSERT INTO open_close_tb VALUES("168","303","3","0","2019-05-07");
INSERT INTO open_close_tb VALUES("169","228","4","0","2019-05-07");
INSERT INTO open_close_tb VALUES("170","277","320","0","2019-05-07");
INSERT INTO open_close_tb VALUES("171","448","31","0","2019-05-07");
INSERT INTO open_close_tb VALUES("172","115","27","24","2019-05-07");
INSERT INTO open_close_tb VALUES("173","120","59","0","2019-05-07");
INSERT INTO open_close_tb VALUES("174","247","13","11","2019-05-07");
INSERT INTO open_close_tb VALUES("175","467","17","0","2019-05-07");
INSERT INTO open_close_tb VALUES("176","463","8","0","2019-05-07");
INSERT INTO open_close_tb VALUES("177","295","142","139","2019-05-07");
INSERT INTO open_close_tb VALUES("178","327","11","0","2019-05-07");
INSERT INTO open_close_tb VALUES("179","137","6","0","2019-05-07");
INSERT INTO open_close_tb VALUES("180","460","48","43","2019-05-07");
INSERT INTO open_close_tb VALUES("181","128","117","109","2019-05-07");
INSERT INTO open_close_tb VALUES("182","319","72","0","2019-05-07");
INSERT INTO open_close_tb VALUES("183","458","53","48","2019-05-07");
INSERT INTO open_close_tb VALUES("184","372","17","12","2019-05-07");
INSERT INTO open_close_tb VALUES("185","320","20","0","2019-05-07");
INSERT INTO open_close_tb VALUES("186","301","15","12","2019-05-07");
INSERT INTO open_close_tb VALUES("187","373","38","0","2019-05-07");
INSERT INTO open_close_tb VALUES("188","376","27","25","2019-05-07");
INSERT INTO open_close_tb VALUES("189","289","40","0","2019-05-07");
INSERT INTO open_close_tb VALUES("190","322","80","10","2019-05-07");
INSERT INTO open_close_tb VALUES("191","98","28","22","2019-05-07");
INSERT INTO open_close_tb VALUES("192","447","78","0","2019-05-07");
INSERT INTO open_close_tb VALUES("193","436","75","64","2019-05-07");
INSERT INTO open_close_tb VALUES("194","441","670","659","2019-05-07");
INSERT INTO open_close_tb VALUES("195","24","2","0","2019-05-07");
INSERT INTO open_close_tb VALUES("196","35","436","0","2019-05-07");
INSERT INTO open_close_tb VALUES("197","119","15","9","2019-05-07");
INSERT INTO open_close_tb VALUES("198","22","800","0","2019-05-07");
INSERT INTO open_close_tb VALUES("199","180","265","262","2019-05-07");
INSERT INTO open_close_tb VALUES("200","47","42","0","2019-05-07");
INSERT INTO open_close_tb VALUES("201","45","72","0","2019-05-07");
INSERT INTO open_close_tb VALUES("202","20","398","0","2019-05-07");
INSERT INTO open_close_tb VALUES("203","350","48","0","2019-05-07");
INSERT INTO open_close_tb VALUES("204","27","13","0","2019-05-07");
INSERT INTO open_close_tb VALUES("205","361","7","0","2019-05-07");
INSERT INTO open_close_tb VALUES("206","112","9","0","2019-05-07");
INSERT INTO open_close_tb VALUES("207","342","3","0","2019-05-07");
INSERT INTO open_close_tb VALUES("208","68","4","0","2019-05-07");
INSERT INTO open_close_tb VALUES("209","15","7","0","2019-05-07");
INSERT INTO open_close_tb VALUES("210","402","148","0","2019-05-07");
INSERT INTO open_close_tb VALUES("211","294","28","0","2019-05-07");
INSERT INTO open_close_tb VALUES("212","129","19","0","2019-05-07");
INSERT INTO open_close_tb VALUES("213","305","48","0","2019-05-07");
INSERT INTO open_close_tb VALUES("214","321","132","0","2019-05-07");
INSERT INTO open_close_tb VALUES("215","299","12","0","2019-05-07");
INSERT INTO open_close_tb VALUES("216","96","13","0","2019-05-07");
INSERT INTO open_close_tb VALUES("217","426","100","0","2019-05-07");
INSERT INTO open_close_tb VALUES("218","253","6","0","2019-05-07");
INSERT INTO open_close_tb VALUES("219","362","10","0","2019-05-07");
INSERT INTO open_close_tb VALUES("220","265","15","0","2019-05-07");
INSERT INTO open_close_tb VALUES("221","384","15","0","2019-05-07");
INSERT INTO open_close_tb VALUES("222","446","46","0","2019-05-07");
INSERT INTO open_close_tb VALUES("223","209","18","0","2019-05-07");
INSERT INTO open_close_tb VALUES("224","283","43","0","2019-05-07");
INSERT INTO open_close_tb VALUES("225","469","38","0","2019-05-07");
INSERT INTO open_close_tb VALUES("226","452","21","0","2019-05-07");
INSERT INTO open_close_tb VALUES("227","148","187","0","2019-05-07");
INSERT INTO open_close_tb VALUES("228","116","33","0","2019-05-07");
INSERT INTO open_close_tb VALUES("229","19","1","0","2019-05-07");
INSERT INTO open_close_tb VALUES("230","65","601","541","2019-05-08");
INSERT INTO open_close_tb VALUES("231","295","139","137","2019-05-08");
INSERT INTO open_close_tb VALUES("232","128","109","100","2019-05-08");
INSERT INTO open_close_tb VALUES("233","460","43","39","2019-05-08");
INSERT INTO open_close_tb VALUES("234","458","48","17","2019-05-08");
INSERT INTO open_close_tb VALUES("235","293","22","0","2019-05-08");
INSERT INTO open_close_tb VALUES("236","96","12","0","2019-05-08");
INSERT INTO open_close_tb VALUES("237","277","312","0","2019-05-08");
INSERT INTO open_close_tb VALUES("238","170","4","0","2019-05-08");
INSERT INTO open_close_tb VALUES("239","400","10","0","2019-05-08");
INSERT INTO open_close_tb VALUES("240","254","35","0","2019-05-08");
INSERT INTO open_close_tb VALUES("241","248","35","0","2019-05-08");
INSERT INTO open_close_tb VALUES("242","247","11","0","2019-05-08");
INSERT INTO open_close_tb VALUES("243","246","27","0","2019-05-08");
INSERT INTO open_close_tb VALUES("244","133","24","0","2019-05-08");
INSERT INTO open_close_tb VALUES("245","475","21","0","2019-05-08");
INSERT INTO open_close_tb VALUES("246","129","14","0","2019-05-08");
INSERT INTO open_close_tb VALUES("247","373","36","0","2019-05-08");
INSERT INTO open_close_tb VALUES("248","436","64","0","2019-05-08");
INSERT INTO open_close_tb VALUES("249","440","94","0","2019-05-08");
INSERT INTO open_close_tb VALUES("250","61","79","0","2019-05-08");
INSERT INTO open_close_tb VALUES("251","75","8","0","2019-05-08");
INSERT INTO open_close_tb VALUES("252","256","29","0","2019-05-08");
INSERT INTO open_close_tb VALUES("253","12","1","0","2019-05-08");
INSERT INTO open_close_tb VALUES("254","492","92","0","2019-05-08");
INSERT INTO open_close_tb VALUES("255","148","186","0","2019-05-08");
INSERT INTO open_close_tb VALUES("256","209","17","0","2019-05-08");
INSERT INTO open_close_tb VALUES("257","88","13","0","2019-05-08");
INSERT INTO open_close_tb VALUES("258","390","23","0","2019-05-08");
INSERT INTO open_close_tb VALUES("259","426","99","0","2019-05-08");
INSERT INTO open_close_tb VALUES("260","428","9","0","2019-05-08");
INSERT INTO open_close_tb VALUES("261","98","22","0","2019-05-08");
INSERT INTO open_close_tb VALUES("262","455","7","0","2019-05-08");
INSERT INTO open_close_tb VALUES("263","252","27","0","2019-05-08");
INSERT INTO open_close_tb VALUES("264","68","3","0","2019-05-08");
INSERT INTO open_close_tb VALUES("265","276","338","0","2019-05-09");
INSERT INTO open_close_tb VALUES("266","303","2","0","2019-05-09");
INSERT INTO open_close_tb VALUES("267","80","5","0","2019-05-09");
INSERT INTO open_close_tb VALUES("268","267","12","0","2019-05-09");
INSERT INTO open_close_tb VALUES("269","400","9","0","2019-05-09");
INSERT INTO open_close_tb VALUES("270","205","44","0","2019-05-09");
INSERT INTO open_close_tb VALUES("271","65","541","500","2019-05-09");
INSERT INTO open_close_tb VALUES("272","327","7","1","2019-05-09");
INSERT INTO open_close_tb VALUES("273","128","100","0","2019-05-09");
INSERT INTO open_close_tb VALUES("274","295","137","0","2019-05-09");
INSERT INTO open_close_tb VALUES("275","458","17","14","2019-05-09");
INSERT INTO open_close_tb VALUES("276","436","54","0","2019-05-09");
INSERT INTO open_close_tb VALUES("277","440","84","0","2019-05-09");
INSERT INTO open_close_tb VALUES("278","492","91","0","2019-05-09");
INSERT INTO open_close_tb VALUES("279","441","659","0","2019-05-09");
INSERT INTO open_close_tb VALUES("280","254","34","0","2019-05-09");
INSERT INTO open_close_tb VALUES("281","253","5","0","2019-05-09");
INSERT INTO open_close_tb VALUES("282","460","39","28","2019-05-09");
INSERT INTO open_close_tb VALUES("283","368","12","0","2019-05-09");
INSERT INTO open_close_tb VALUES("284","370","16","0","2019-05-09");
INSERT INTO open_close_tb VALUES("285","467","16","0","2019-05-09");
INSERT INTO open_close_tb VALUES("286","185","25","0","2019-05-09");
INSERT INTO open_close_tb VALUES("287","458","14","0","2019-05-10");
INSERT INTO open_close_tb VALUES("288","289","39","0","2019-05-10");
INSERT INTO open_close_tb VALUES("289","440","78","0","2019-05-10");
INSERT INTO open_close_tb VALUES("290","436","48","0","2019-05-10");
INSERT INTO open_close_tb VALUES("291","441","653","0","2019-05-10");
INSERT INTO open_close_tb VALUES("292","65","500","484","2019-05-10");
INSERT INTO open_close_tb VALUES("293","444","8","0","2019-05-10");
INSERT INTO open_close_tb VALUES("294","208","10","0","2019-05-10");
INSERT INTO open_close_tb VALUES("295","305","43","0","2019-05-10");
INSERT INTO open_close_tb VALUES("296","452","20","0","2019-05-10");
INSERT INTO open_close_tb VALUES("297","35","431","0","2019-05-10");
INSERT INTO open_close_tb VALUES("298","24","1","0","2019-05-10");
INSERT INTO open_close_tb VALUES("299","248","34","0","2019-05-10");
INSERT INTO open_close_tb VALUES("300","22","788","0","2019-05-10");
INSERT INTO open_close_tb VALUES("301","460","28","0","2019-05-10");
INSERT INTO open_close_tb VALUES("302","277","292","0","2019-05-10");
INSERT INTO open_close_tb VALUES("303","475","20","0","2019-05-10");
INSERT INTO open_close_tb VALUES("304","367","6","0","2019-05-10");
INSERT INTO open_close_tb VALUES("305","88","12","0","2019-05-11");
INSERT INTO open_close_tb VALUES("306","65","484","455","2019-05-11");
INSERT INTO open_close_tb VALUES("307","106","555","0","2019-05-11");
INSERT INTO open_close_tb VALUES("308","492","90","0","2019-05-11");
INSERT INTO open_close_tb VALUES("309","450","85","0","2019-05-11");
INSERT INTO open_close_tb VALUES("310","460","27","23","2019-05-11");
INSERT INTO open_close_tb VALUES("311","22","778","0","2019-05-11");
INSERT INTO open_close_tb VALUES("312","444","7","0","2019-05-11");
INSERT INTO open_close_tb VALUES("313","251","41","0","2019-05-11");
INSERT INTO open_close_tb VALUES("314","228","3","0","2019-05-11");
INSERT INTO open_close_tb VALUES("315","313","29","0","2019-05-11");
INSERT INTO open_close_tb VALUES("316","243","8","0","2019-05-11");
INSERT INTO open_close_tb VALUES("317","178","13","0","2019-05-11");
INSERT INTO open_close_tb VALUES("318","283","42","0","2019-05-11");
INSERT INTO open_close_tb VALUES("319","284","50","0","2019-05-11");
INSERT INTO open_close_tb VALUES("320","4","29","0","2019-05-11");
INSERT INTO open_close_tb VALUES("321","375","57","0","2019-05-11");
INSERT INTO open_close_tb VALUES("322","70","1","0","2019-05-11");
INSERT INTO open_close_tb VALUES("323","305","41","0","2019-05-11");
INSERT INTO open_close_tb VALUES("324","237","14","0","2019-05-11");
INSERT INTO open_close_tb VALUES("325","131","11","0","2019-05-11");
INSERT INTO open_close_tb VALUES("326","322","10","0","2019-05-11");
INSERT INTO open_close_tb VALUES("327","15","6","0","2019-05-11");
INSERT INTO open_close_tb VALUES("328","225","7","0","2019-05-11");
INSERT INTO open_close_tb VALUES("329","442","616","0","2019-05-11");
INSERT INTO open_close_tb VALUES("330","127","17","0","2019-05-11");
INSERT INTO open_close_tb VALUES("331","370","15","0","2019-05-13");
INSERT INTO open_close_tb VALUES("332","250","38","0","2019-05-13");
INSERT INTO open_close_tb VALUES("333","148","184","177","2019-05-13");
INSERT INTO open_close_tb VALUES("334","20","397","0","2019-05-13");
INSERT INTO open_close_tb VALUES("335","147","315","300","2019-05-13");
INSERT INTO open_close_tb VALUES("336","65","455","0","2019-05-13");
INSERT INTO open_close_tb VALUES("337","129","11","0","2019-05-13");
INSERT INTO open_close_tb VALUES("338","128","93","89","2019-05-13");
INSERT INTO open_close_tb VALUES("339","299","10","0","2019-05-13");
INSERT INTO open_close_tb VALUES("340","364","2","0","2019-05-13");
INSERT INTO open_close_tb VALUES("341","436","44","0","2019-05-13");
INSERT INTO open_close_tb VALUES("342","441","649","0","2019-05-13");
INSERT INTO open_close_tb VALUES("343","251","39","0","2019-05-13");
INSERT INTO open_close_tb VALUES("344","200","3","0","2019-05-13");
INSERT INTO open_close_tb VALUES("345","192","6","0","2019-05-13");
INSERT INTO open_close_tb VALUES("346","391","15","0","2019-05-13");
INSERT INTO open_close_tb VALUES("347","63","13","0","2019-05-13");
INSERT INTO open_close_tb VALUES("348","236","68","0","2019-05-13");
INSERT INTO open_close_tb VALUES("349","45","67","0","2019-05-14");
INSERT INTO open_close_tb VALUES("350","47","37","0","2019-05-14");
INSERT INTO open_close_tb VALUES("351","20","387","0","2019-05-14");
INSERT INTO open_close_tb VALUES("352","35","424","0","2019-05-14");
INSERT INTO open_close_tb VALUES("353","193","172","0","2019-05-14");
INSERT INTO open_close_tb VALUES("354","128","89","0","2019-05-14");
INSERT INTO open_close_tb VALUES("355","1","1","0","2019-05-14");
INSERT INTO open_close_tb VALUES("356","83","11","0","2019-05-14");
INSERT INTO open_close_tb VALUES("357","205","42","0","2019-05-14");
INSERT INTO open_close_tb VALUES("358","204","61","0","2019-05-14");
INSERT INTO open_close_tb VALUES("359","475","19","0","2019-05-14");
INSERT INTO open_close_tb VALUES("360","252","26","0","2019-05-14");
INSERT INTO open_close_tb VALUES("361","283","39","0","2019-05-14");
INSERT INTO open_close_tb VALUES("362","446","45","0","2019-05-14");
INSERT INTO open_close_tb VALUES("363","492","89","0","2019-05-14");
INSERT INTO open_close_tb VALUES("364","434","5","0","2019-05-14");
INSERT INTO open_close_tb VALUES("365","186","2","0","2019-05-14");
INSERT INTO open_close_tb VALUES("366","428","7","0","2019-05-15");
INSERT INTO open_close_tb VALUES("367","125","29","27","2019-05-15");
INSERT INTO open_close_tb VALUES("368","130","2","0","2019-05-15");
INSERT INTO open_close_tb VALUES("369","128","85","83","2019-05-15");
INSERT INTO open_close_tb VALUES("370","65","440","424","2019-05-15");
INSERT INTO open_close_tb VALUES("371","436","37","0","2019-05-15");
INSERT INTO open_close_tb VALUES("372","440","74","70","2019-05-15");
INSERT INTO open_close_tb VALUES("373","98","18","0","2019-05-15");
INSERT INTO open_close_tb VALUES("374","265","14","0","2019-05-15");
INSERT INTO open_close_tb VALUES("375","156","2","0","2019-05-15");
INSERT INTO open_close_tb VALUES("376","160","42","0","2019-05-15");
INSERT INTO open_close_tb VALUES("377","452","19","0","2019-05-15");
INSERT INTO open_close_tb VALUES("378","106","553","550","2019-05-15");
INSERT INTO open_close_tb VALUES("379","461","5","0","2019-05-15");
INSERT INTO open_close_tb VALUES("380","402","103","0","2019-05-15");
INSERT INTO open_close_tb VALUES("381","390","21","0","2019-05-15");
INSERT INTO open_close_tb VALUES("382","35","429","0","2019-05-15");
INSERT INTO open_close_tb VALUES("383","88","11","0","2019-05-15");
INSERT INTO open_close_tb VALUES("384","310","14","0","2019-05-16");
INSERT INTO open_close_tb VALUES("385","176","2","0","2019-05-16");
INSERT INTO open_close_tb VALUES("386","163","14","0","2019-05-16");
INSERT INTO open_close_tb VALUES("387","305","37","0","2019-05-16");
INSERT INTO open_close_tb VALUES("388","295","136","0","2019-05-16");
INSERT INTO open_close_tb VALUES("389","289","33","0","2019-05-16");
INSERT INTO open_close_tb VALUES("390","299","9","7","2019-05-16");
INSERT INTO open_close_tb VALUES("391","106","550","0","2019-05-16");
INSERT INTO open_close_tb VALUES("392","452","18","0","2019-05-16");
INSERT INTO open_close_tb VALUES("393","22","768","0","2019-05-16");
INSERT INTO open_close_tb VALUES("394","251","38","35","2019-05-16");
INSERT INTO open_close_tb VALUES("395","128","83","79","2019-05-17");
INSERT INTO open_close_tb VALUES("396","65","424","0","2019-05-17");
INSERT INTO open_close_tb VALUES("397","133","19","0","2019-05-17");
INSERT INTO open_close_tb VALUES("398","207","9","0","2019-05-17");
INSERT INTO open_close_tb VALUES("399","148","177","0","2019-05-17");
INSERT INTO open_close_tb VALUES("400","147","300","0","2019-05-17");
INSERT INTO open_close_tb VALUES("401","426","98","78","2019-05-17");
INSERT INTO open_close_tb VALUES("402","446","44","38","2019-05-17");
INSERT INTO open_close_tb VALUES("403","447","77","71","2019-05-17");
INSERT INTO open_close_tb VALUES("404","257","54","48","2019-05-17");
INSERT INTO open_close_tb VALUES("405","30","50","34","2019-05-17");
INSERT INTO open_close_tb VALUES("406","436","34","0","2019-05-17");
INSERT INTO open_close_tb VALUES("407","441","642","0","2019-05-17");
INSERT INTO open_close_tb VALUES("408","460","75","73","2019-05-17");
INSERT INTO open_close_tb VALUES("409","204","60","0","2019-05-17");
INSERT INTO open_close_tb VALUES("410","0","0","0","2019-05-17");
INSERT INTO open_close_tb VALUES("411","402","101","0","2019-05-17");
INSERT INTO open_close_tb VALUES("412","294","27","0","2019-05-17");
INSERT INTO open_close_tb VALUES("413","320","150","0","2019-05-17");
INSERT INTO open_close_tb VALUES("414","2","100","0","2019-05-17");
INSERT INTO open_close_tb VALUES("415","458","94","0","2019-05-17");
INSERT INTO open_close_tb VALUES("416","214","10","0","2019-05-17");
INSERT INTO open_close_tb VALUES("417","118","2","0","2019-05-17");
INSERT INTO open_close_tb VALUES("418","116","32","0","2019-05-17");
INSERT INTO open_close_tb VALUES("419","459","182","0","2019-05-17");
INSERT INTO open_close_tb VALUES("420","283","38","0","2019-05-17");
INSERT INTO open_close_tb VALUES("421","303","11","0","2019-05-18");
INSERT INTO open_close_tb VALUES("422","376","25","0","2019-05-18");
INSERT INTO open_close_tb VALUES("423","98","50","0","2019-05-18");
INSERT INTO open_close_tb VALUES("424","377","16","11","2019-05-18");
INSERT INTO open_close_tb VALUES("425","276","327","0","2019-05-18");
INSERT INTO open_close_tb VALUES("426","21","500","490","2019-05-18");
INSERT INTO open_close_tb VALUES("427","446","38","0","2019-05-18");
INSERT INTO open_close_tb VALUES("428","463","7","0","2019-05-18");
INSERT INTO open_close_tb VALUES("429","375","51","0","2019-05-18");
INSERT INTO open_close_tb VALUES("430","131","10","0","2019-05-18");
INSERT INTO open_close_tb VALUES("431","213","27","0","2019-05-18");
INSERT INTO open_close_tb VALUES("432","283","37","0","2019-05-18");
INSERT INTO open_close_tb VALUES("433","374","120","0","2019-05-18");
INSERT INTO open_close_tb VALUES("434","444","6","0","2019-05-18");
INSERT INTO open_close_tb VALUES("435","30","34","0","2019-05-18");
INSERT INTO open_close_tb VALUES("436","65","409","400","2019-05-18");
INSERT INTO open_close_tb VALUES("437","203","4","0","2019-05-18");
INSERT INTO open_close_tb VALUES("438","460","73","68","2019-05-18");
INSERT INTO open_close_tb VALUES("439","180","262","0","2019-05-18");
INSERT INTO open_close_tb VALUES("440","96","10","0","2019-05-18");
INSERT INTO open_close_tb VALUES("441","466","11","0","2019-05-18");
INSERT INTO open_close_tb VALUES("442","122","26","0","2019-05-18");
INSERT INTO open_close_tb VALUES("443","475","18","0","2019-05-18");
INSERT INTO open_close_tb VALUES("444","128","79","75","2019-05-20");
INSERT INTO open_close_tb VALUES("445","30","32","0","2019-05-20");
INSERT INTO open_close_tb VALUES("446","295","135","0","2019-05-20");
INSERT INTO open_close_tb VALUES("447","458","93","90","2019-05-20");
INSERT INTO open_close_tb VALUES("448","180","261","258","2019-05-20");
INSERT INTO open_close_tb VALUES("449","34","21","0","2019-05-20");
INSERT INTO open_close_tb VALUES("450","249","28","0","2019-05-20");
INSERT INTO open_close_tb VALUES("451","65","400","394","2019-05-20");
INSERT INTO open_close_tb VALUES("452","98","49","0","2019-05-20");
INSERT INTO open_close_tb VALUES("453","283","36","0","2019-05-20");
INSERT INTO open_close_tb VALUES("454","319","150","0","2019-05-20");
INSERT INTO open_close_tb VALUES("455","123","24","0","2019-05-20");
INSERT INTO open_close_tb VALUES("456","4","22","0","2019-05-20");
INSERT INTO open_close_tb VALUES("457","445","85","0","2019-05-20");
INSERT INTO open_close_tb VALUES("458","134","4","0","2019-05-20");
INSERT INTO open_close_tb VALUES("459","402","97","0","2019-05-20");
INSERT INTO open_close_tb VALUES("460","35","428","0","2019-05-20");
INSERT INTO open_close_tb VALUES("461","148","175","0","2019-05-20");
INSERT INTO open_close_tb VALUES("462","30","31","0","2019-05-21");
INSERT INTO open_close_tb VALUES("463","128","75","66","2019-05-21");
INSERT INTO open_close_tb VALUES("464","65","394","0","2019-05-21");
INSERT INTO open_close_tb VALUES("465","123","23","0","2019-05-21");
INSERT INTO open_close_tb VALUES("466","403","42","0","2019-05-21");
INSERT INTO open_close_tb VALUES("467","450","84","0","2019-05-21");
INSERT INTO open_close_tb VALUES("468","13","23","0","2019-05-21");
INSERT INTO open_close_tb VALUES("469","449","75","0","2019-05-21");
INSERT INTO open_close_tb VALUES("470","140","5","0","2019-05-21");
INSERT INTO open_close_tb VALUES("471","5","53","0","2019-05-21");
INSERT INTO open_close_tb VALUES("472","32","7","0","2019-05-21");
INSERT INTO open_close_tb VALUES("473","320","136","131","2019-05-21");
INSERT INTO open_close_tb VALUES("474","213","24","0","2019-05-21");
INSERT INTO open_close_tb VALUES("475","475","17","0","2019-05-21");
INSERT INTO open_close_tb VALUES("476","129","136","0","2019-05-21");
INSERT INTO open_close_tb VALUES("477","299","7","0","2019-05-21");
INSERT INTO open_close_tb VALUES("478","98","48","0","2019-05-21");
INSERT INTO open_close_tb VALUES("479","35","427","0","2019-05-21");
INSERT INTO open_close_tb VALUES("480","180","258","0","2019-05-21");
INSERT INTO open_close_tb VALUES("481","467","15","0","2019-05-21");
INSERT INTO open_close_tb VALUES("482","75","7","0","2019-05-22");
INSERT INTO open_close_tb VALUES("483","302","10","0","2019-05-22");
INSERT INTO open_close_tb VALUES("484","128","66","0","2019-05-22");
INSERT INTO open_close_tb VALUES("485","129","124","0","2019-05-22");
INSERT INTO open_close_tb VALUES("486","440","71","0","2019-05-22");
INSERT INTO open_close_tb VALUES("487","441","640","0","2019-05-22");
INSERT INTO open_close_tb VALUES("488","436","32","0","2019-05-22");
INSERT INTO open_close_tb VALUES("489","185","24","0","2019-05-22");
INSERT INTO open_close_tb VALUES("490","374","100","0","2019-05-22");
INSERT INTO open_close_tb VALUES("491","445","81","0","2019-05-22");
INSERT INTO open_close_tb VALUES("492","295","134","132","2019-05-22");
INSERT INTO open_close_tb VALUES("493","319","145","0","2019-05-22");
INSERT INTO open_close_tb VALUES("494","442","615","0","2019-05-22");
INSERT INTO open_close_tb VALUES("495","132","10","0","2019-05-22");
INSERT INTO open_close_tb VALUES("496","217","8","0","2019-05-22");
INSERT INTO open_close_tb VALUES("497","106","548","0","2019-05-22");
INSERT INTO open_close_tb VALUES("498","262","17","0","2019-05-22");
INSERT INTO open_close_tb VALUES("499","320","131","72","2019-05-22");
INSERT INTO open_close_tb VALUES("500","317","53","0","2019-05-22");
INSERT INTO open_close_tb VALUES("501","448","29","0","2019-05-22");
INSERT INTO open_close_tb VALUES("502","422","60","0","2019-05-22");
INSERT INTO open_close_tb VALUES("503","305","66","0","2019-05-22");
INSERT INTO open_close_tb VALUES("504","255","25","0","2019-05-22");
INSERT INTO open_close_tb VALUES("505","6","20","0","2019-05-22");
INSERT INTO open_close_tb VALUES("506","65","389","382","2019-05-22");
INSERT INTO open_close_tb VALUES("507","2","90","0","2019-05-22");
INSERT INTO open_close_tb VALUES("508","63","12","0","2019-05-22");
INSERT INTO open_close_tb VALUES("509","116","31","0","2019-05-23");
INSERT INTO open_close_tb VALUES("510","249","27","0","2019-05-23");
INSERT INTO open_close_tb VALUES("511","216","20","0","2019-05-23");
INSERT INTO open_close_tb VALUES("512","83","10","0","2019-05-23");
INSERT INTO open_close_tb VALUES("513","305","61","0","2019-05-23");
INSERT INTO open_close_tb VALUES("514","128","64","60","2019-05-23");
INSERT INTO open_close_tb VALUES("515","98","47","0","2019-05-23");
INSERT INTO open_close_tb VALUES("516","436","20","0","2019-05-23");
INSERT INTO open_close_tb VALUES("517","441","628","0","2019-05-23");
INSERT INTO open_close_tb VALUES("518","440","59","0","2019-05-23");
INSERT INTO open_close_tb VALUES("519","301","12","11","2019-05-23");
INSERT INTO open_close_tb VALUES("520","377","11","9","2019-05-23");
INSERT INTO open_close_tb VALUES("521","302","9","0","2019-05-23");
INSERT INTO open_close_tb VALUES("522","106","546","0","2019-05-23");
INSERT INTO open_close_tb VALUES("523","30","21","13","2019-05-23");
INSERT INTO open_close_tb VALUES("524","130","4","0","2019-05-23");
INSERT INTO open_close_tb VALUES("525","327","25","0","2019-05-23");
INSERT INTO open_close_tb VALUES("526","126","28","0","2019-05-23");
INSERT INTO open_close_tb VALUES("527","373","54","0","2019-05-23");
INSERT INTO open_close_tb VALUES("528","61","78","0","2019-05-23");
INSERT INTO open_close_tb VALUES("529","492","88","0","2019-05-23");
INSERT INTO open_close_tb VALUES("530","290","15","0","2019-05-23");
INSERT INTO open_close_tb VALUES("531","449","74","0","2019-05-23");
INSERT INTO open_close_tb VALUES("532","317","48","0","2019-05-23");
INSERT INTO open_close_tb VALUES("533","155","1","0","2019-05-23");
INSERT INTO open_close_tb VALUES("534","185","22","0","2019-05-23");
INSERT INTO open_close_tb VALUES("535","445","80","0","2019-05-23");
INSERT INTO open_close_tb VALUES("536","460","68","0","2019-05-23");
INSERT INTO open_close_tb VALUES("537","98","46","0","2019-05-24");
INSERT INTO open_close_tb VALUES("538","461","2","0","2019-05-24");
INSERT INTO open_close_tb VALUES("539","208","9","0","2019-05-24");
INSERT INTO open_close_tb VALUES("540","80","4","0","2019-05-24");
INSERT INTO open_close_tb VALUES("541","446","37","0","2019-05-24");
INSERT INTO open_close_tb VALUES("542","128","59","57","2019-05-24");
INSERT INTO open_close_tb VALUES("543","301","10","0","2019-05-24");
INSERT INTO open_close_tb VALUES("544","373","52","0","2019-05-24");
INSERT INTO open_close_tb VALUES("545","133","17","0","2019-05-24");
INSERT INTO open_close_tb VALUES("546","322","103","0","2019-05-24");
INSERT INTO open_close_tb VALUES("547","317","46","45","2019-05-24");
INSERT INTO open_close_tb VALUES("548","147","295","0","2019-05-24");
INSERT INTO open_close_tb VALUES("549","148","174","0","2019-05-24");
INSERT INTO open_close_tb VALUES("550","193","170","0","2019-05-24");
INSERT INTO open_close_tb VALUES("551","469","36","0","2019-05-24");
INSERT INTO open_close_tb VALUES("552","188","67","0","2019-05-24");
INSERT INTO open_close_tb VALUES("553","112","8","0","2019-05-24");
INSERT INTO open_close_tb VALUES("554","27","12","0","2019-05-24");
INSERT INTO open_close_tb VALUES("555","15","4","3","2019-05-24");
INSERT INTO open_close_tb VALUES("556","350","45","0","2019-05-24");
INSERT INTO open_close_tb VALUES("557","134","3","2","2019-05-24");
INSERT INTO open_close_tb VALUES("558","65","382","379","2019-05-24");
INSERT INTO open_close_tb VALUES("559","372","22","0","2019-05-24");
INSERT INTO open_close_tb VALUES("560","20","386","0","2019-05-24");
INSERT INTO open_close_tb VALUES("561","63","11","0","2019-05-24");
INSERT INTO open_close_tb VALUES("562","442","614","0","2019-05-24");
INSERT INTO open_close_tb VALUES("563","236","58","0","2019-05-24");
INSERT INTO open_close_tb VALUES("564","87","13","0","2019-05-24");
INSERT INTO open_close_tb VALUES("565","327","23","0","2019-05-24");
INSERT INTO open_close_tb VALUES("566","132","8","0","2019-05-24");
INSERT INTO open_close_tb VALUES("567","65","378","336","2019-05-25");
INSERT INTO open_close_tb VALUES("568","468","11","0","2019-05-25");
INSERT INTO open_close_tb VALUES("569","422","56","0","2019-05-25");
INSERT INTO open_close_tb VALUES("570","374","100","93","2019-05-25");
INSERT INTO open_close_tb VALUES("571","160","41","40","2019-05-25");
INSERT INTO open_close_tb VALUES("572","317","44","37","2019-05-25");
INSERT INTO open_close_tb VALUES("573","305","60","0","2019-05-25");
INSERT INTO open_close_tb VALUES("574","283","34","0","2019-05-25");
INSERT INTO open_close_tb VALUES("575","20","361","0","2019-05-25");
INSERT INTO open_close_tb VALUES("576","209","16","14","2019-05-25");
INSERT INTO open_close_tb VALUES("577","212","25","0","2019-05-25");
INSERT INTO open_close_tb VALUES("578","132","6","0","2019-05-25");
INSERT INTO open_close_tb VALUES("579","123","22","21","2019-05-25");
INSERT INTO open_close_tb VALUES("580","459","181","180","2019-05-25");
INSERT INTO open_close_tb VALUES("581","328","11","0","2019-05-25");
INSERT INTO open_close_tb VALUES("582","322","99","0","2019-05-25");
INSERT INTO open_close_tb VALUES("583","445","79","0","2019-05-25");
INSERT INTO open_close_tb VALUES("584","372","20","19","2019-05-25");
INSERT INTO open_close_tb VALUES("585","5","52","49","2019-05-25");
INSERT INTO open_close_tb VALUES("586","136","10","0","2019-05-25");
INSERT INTO open_close_tb VALUES("587","98","45","44","2019-05-25");
INSERT INTO open_close_tb VALUES("588","370","14","0","2019-05-25");
INSERT INTO open_close_tb VALUES("589","125","27","0","2019-05-25");
INSERT INTO open_close_tb VALUES("590","460","67","0","2019-05-25");
INSERT INTO open_close_tb VALUES("591","13","22","0","2019-05-25");
INSERT INTO open_close_tb VALUES("592","376","22","0","2019-05-25");
INSERT INTO open_close_tb VALUES("593","362","9","0","2019-05-27");
INSERT INTO open_close_tb VALUES("594","160","39","0","2019-05-27");
INSERT INTO open_close_tb VALUES("595","319","127","86","2019-05-27");
INSERT INTO open_close_tb VALUES("596","458","90","0","2019-05-27");
INSERT INTO open_close_tb VALUES("597","65","330","321","2019-05-27");
INSERT INTO open_close_tb VALUES("598","322","96","0","2019-05-27");
INSERT INTO open_close_tb VALUES("599","293","6","5","2019-05-27");
INSERT INTO open_close_tb VALUES("600","301","9","0","2019-05-27");
INSERT INTO open_close_tb VALUES("601","128","55","50","2019-05-27");
INSERT INTO open_close_tb VALUES("602","372","14","8","2019-05-27");
INSERT INTO open_close_tb VALUES("603","35","425","0","2019-05-27");
INSERT INTO open_close_tb VALUES("604","98","43","42","2019-05-27");
INSERT INTO open_close_tb VALUES("605","440","57","0","2019-05-27");
INSERT INTO open_close_tb VALUES("606","436","18","0","2019-05-27");
INSERT INTO open_close_tb VALUES("607","447","71","0","2019-05-27");
INSERT INTO open_close_tb VALUES("608","446","36","0","2019-05-27");
INSERT INTO open_close_tb VALUES("609","212","23","0","2019-05-27");
INSERT INTO open_close_tb VALUES("610","459","176","0","2019-05-27");
INSERT INTO open_close_tb VALUES("611","133","16","0","2019-05-27");
INSERT INTO open_close_tb VALUES("612","251","35","0","2019-05-27");
INSERT INTO open_close_tb VALUES("613","368","11","0","2019-05-27");
INSERT INTO open_close_tb VALUES("614","276","247","0","2019-05-27");
INSERT INTO open_close_tb VALUES("615","300","2","0","2019-05-27");
INSERT INTO open_close_tb VALUES("616","61","76","74","2019-05-27");
INSERT INTO open_close_tb VALUES("617","428","6","0","2019-05-27");
INSERT INTO open_close_tb VALUES("618","446","35","33","2019-05-28");
INSERT INTO open_close_tb VALUES("619","428","5","0","2019-05-28");
INSERT INTO open_close_tb VALUES("620","460","66","0","2019-05-28");
INSERT INTO open_close_tb VALUES("621","461","1","0","2019-05-28");
INSERT INTO open_close_tb VALUES("622","319","76","0","2019-05-28");
INSERT INTO open_close_tb VALUES("623","322","89","0","2019-05-28");
INSERT INTO open_close_tb VALUES("624","422","55","0","2019-05-28");
INSERT INTO open_close_tb VALUES("625","65","320","318","2019-05-28");
INSERT INTO open_close_tb VALUES("626","251","34","0","2019-05-28");
INSERT INTO open_close_tb VALUES("627","368","10","0","2019-05-28");
INSERT INTO open_close_tb VALUES("628","128","49","0","2019-05-28");
INSERT INTO open_close_tb VALUES("629","468","10","9","2019-05-28");
INSERT INTO open_close_tb VALUES("630","123","20","0","2019-05-28");
INSERT INTO open_close_tb VALUES("631","445","78","77","2019-05-28");
INSERT INTO open_close_tb VALUES("632","293","4","0","2019-05-28");
INSERT INTO open_close_tb VALUES("633","374","92","0","2019-05-28");
INSERT INTO open_close_tb VALUES("634","130","3","0","2019-05-28");
INSERT INTO open_close_tb VALUES("635","157","22","0","2019-05-28");
INSERT INTO open_close_tb VALUES("636","143","21","0","2019-05-28");
INSERT INTO open_close_tb VALUES("637","501","40","0","2019-05-28");
INSERT INTO open_close_tb VALUES("638","441","626","0","2019-05-28");
INSERT INTO open_close_tb VALUES("639","500","40","0","2019-05-28");
INSERT INTO open_close_tb VALUES("640","307","2","0","2019-05-28");
INSERT INTO open_close_tb VALUES("641","73","28","0","2019-05-28");
INSERT INTO open_close_tb VALUES("642","34","20","0","2019-05-28");
INSERT INTO open_close_tb VALUES("643","276","197","0","2019-05-28");
INSERT INTO open_close_tb VALUES("644","156","1","0","2019-05-28");
INSERT INTO open_close_tb VALUES("645","430","8","0","2019-05-28");
INSERT INTO open_close_tb VALUES("646","212","22","0","2019-05-28");
INSERT INTO open_close_tb VALUES("647","305","58","0","2019-05-29");
INSERT INTO open_close_tb VALUES("648","319","74","0","2019-05-29");
INSERT INTO open_close_tb VALUES("649","370","12","0","2019-05-29");
INSERT INTO open_close_tb VALUES("650","128","45","40","2019-05-29");
INSERT INTO open_close_tb VALUES("651","221","5","0","2019-05-29");
INSERT INTO open_close_tb VALUES("652","312","30","0","2019-05-29");
INSERT INTO open_close_tb VALUES("653","257","48","0","2019-05-29");
INSERT INTO open_close_tb VALUES("654","444","5","0","2019-05-29");
INSERT INTO open_close_tb VALUES("655","132","4","0","2019-05-29");
INSERT INTO open_close_tb VALUES("656","178","12","0","2019-05-29");
INSERT INTO open_close_tb VALUES("657","239","7","0","2019-05-29");
INSERT INTO open_close_tb VALUES("658","322","86","0","2019-05-29");
INSERT INTO open_close_tb VALUES("659","65","317","0","2019-05-29");
INSERT INTO open_close_tb VALUES("660","2","87","0","2019-05-29");
INSERT INTO open_close_tb VALUES("661","459","171","0","2019-05-29");
INSERT INTO open_close_tb VALUES("662","30","500","487","2019-05-29");
INSERT INTO open_close_tb VALUES("663","374","91","87","2019-05-29");
INSERT INTO open_close_tb VALUES("664","5","45","0","2019-05-29");
INSERT INTO open_close_tb VALUES("665","212","21","0","2019-05-29");
INSERT INTO open_close_tb VALUES("666","320","72","0","2019-05-29");
INSERT INTO open_close_tb VALUES("667","98","41","0","2019-05-29");
INSERT INTO open_close_tb VALUES("668","216","19","0","2019-05-29");
INSERT INTO open_close_tb VALUES("669","251","33","0","2019-05-29");
INSERT INTO open_close_tb VALUES("670","302","8","0","2019-05-29");
INSERT INTO open_close_tb VALUES("671","496","5","0","2019-05-29");
INSERT INTO open_close_tb VALUES("672","492","87","0","2019-05-29");
INSERT INTO open_close_tb VALUES("673","388","8","0","2019-05-29");
INSERT INTO open_close_tb VALUES("674","250","37","0","2019-05-29");
INSERT INTO open_close_tb VALUES("675","277","242","0","2019-05-30");
INSERT INTO open_close_tb VALUES("676","294","7","0","2019-05-30");
INSERT INTO open_close_tb VALUES("677","128","39","31","2019-05-30");
INSERT INTO open_close_tb VALUES("678","458","87","0","2019-05-30");
INSERT INTO open_close_tb VALUES("679","122","25","0","2019-05-30");
INSERT INTO open_close_tb VALUES("680","27","10","0","2019-05-30");
INSERT INTO open_close_tb VALUES("681","212","20","0","2019-05-30");
INSERT INTO open_close_tb VALUES("682","312","29","0","2019-05-30");
INSERT INTO open_close_tb VALUES("683","257","47","0","2019-05-30");
INSERT INTO open_close_tb VALUES("684","320","66","0","2019-05-30");
INSERT INTO open_close_tb VALUES("685","5","44","43","2019-05-30");
INSERT INTO open_close_tb VALUES("686","422","54","0","2019-05-30");
INSERT INTO open_close_tb VALUES("687","252","25","0","2019-05-30");
INSERT INTO open_close_tb VALUES("688","22","766","0","2019-05-30");
INSERT INTO open_close_tb VALUES("689","208","8","0","2019-05-30");
INSERT INTO open_close_tb VALUES("690","295","132","131","2019-05-30");
INSERT INTO open_close_tb VALUES("691","402","87","0","2019-05-30");
INSERT INTO open_close_tb VALUES("692","290","14","0","2019-05-30");
INSERT INTO open_close_tb VALUES("693","250","36","0","2019-05-30");
INSERT INTO open_close_tb VALUES("694","376","21","0","2019-05-30");
INSERT INTO open_close_tb VALUES("695","30","484","478","2019-05-30");
INSERT INTO open_close_tb VALUES("696","21","490","0","2019-05-30");
INSERT INTO open_close_tb VALUES("697","65","302","0","2019-05-30");
INSERT INTO open_close_tb VALUES("698","143","20","0","2019-05-30");
INSERT INTO open_close_tb VALUES("699","62","100","0","2019-05-30");
INSERT INTO open_close_tb VALUES("700","133","15","0","2019-05-31");
INSERT INTO open_close_tb VALUES("701","373","50","0","2019-05-31");
INSERT INTO open_close_tb VALUES("702","372","7","0","2019-05-31");
INSERT INTO open_close_tb VALUES("703","320","40","30","2019-05-31");
INSERT INTO open_close_tb VALUES("704","30","477","476","2019-05-31");
INSERT INTO open_close_tb VALUES("705","449","73","0","2019-05-31");
INSERT INTO open_close_tb VALUES("706","303","10","0","2019-05-31");
INSERT INTO open_close_tb VALUES("707","137","10","0","2019-05-31");
INSERT INTO open_close_tb VALUES("708","61","73","0","2019-05-31");
INSERT INTO open_close_tb VALUES("709","4","21","0","2019-05-31");
INSERT INTO open_close_tb VALUES("710","62","99","98","2019-05-31");
INSERT INTO open_close_tb VALUES("711","440","55","0","2019-05-31");
INSERT INTO open_close_tb VALUES("712","460","65","0","2019-05-31");
INSERT INTO open_close_tb VALUES("713","65","295","0","2019-05-31");
INSERT INTO open_close_tb VALUES("714","73","27","0","2019-05-31");
INSERT INTO open_close_tb VALUES("715","120","58","0","2019-05-31");
INSERT INTO open_close_tb VALUES("716","495","10","0","2019-05-31");
INSERT INTO open_close_tb VALUES("717","20","359","0","2019-05-31");
INSERT INTO open_close_tb VALUES("718","63","10","8","2019-05-31");
INSERT INTO open_close_tb VALUES("719","432","15","0","2019-05-31");
INSERT INTO open_close_tb VALUES("720","299","3","0","2019-05-31");
INSERT INTO open_close_tb VALUES("721","180","257","0","2019-05-31");
INSERT INTO open_close_tb VALUES("722","376","19","0","2019-05-31");
INSERT INTO open_close_tb VALUES("723","277","222","0","2019-05-31");
INSERT INTO open_close_tb VALUES("724","22","756","0","2019-05-31");
INSERT INTO open_close_tb VALUES("725","458","86","0","2019-05-31");
INSERT INTO open_close_tb VALUES("726","128","27","0","2019-05-31");
INSERT INTO open_close_tb VALUES("727","129","123","0","2019-05-31");
INSERT INTO open_close_tb VALUES("728","450","83","82","2019-05-31");
INSERT INTO open_close_tb VALUES("729","192","5","0","2019-05-31");
INSERT INTO open_close_tb VALUES("730","198","5","0","2019-05-31");
INSERT INTO open_close_tb VALUES("731","51","38","0","2019-05-31");
INSERT INTO open_close_tb VALUES("732","50","43","0","2019-05-31");
INSERT INTO open_close_tb VALUES("733","194","83","0","2019-05-31");
INSERT INTO open_close_tb VALUES("734","195","64","0","2019-05-31");
INSERT INTO open_close_tb VALUES("735","202","114","0","2019-05-31");
INSERT INTO open_close_tb VALUES("736","52","6","0","2019-05-31");
INSERT INTO open_close_tb VALUES("737","53","6","0","2019-05-31");
INSERT INTO open_close_tb VALUES("738","54","6","0","2019-05-31");
INSERT INTO open_close_tb VALUES("739","112","7","0","2019-05-31");
INSERT INTO open_close_tb VALUES("740","107","15","0","2019-05-31");
INSERT INTO open_close_tb VALUES("741","390","20","0","2019-05-31");
INSERT INTO open_close_tb VALUES("742","254","30","0","2019-05-31");
INSERT INTO open_close_tb VALUES("743","283","33","0","2019-05-31");
INSERT INTO open_close_tb VALUES("744","249","22","0","2019-05-31");
INSERT INTO open_close_tb VALUES("745","5","42","0","2019-05-31");
INSERT INTO open_close_tb VALUES("746","65","294","291","2019-06-01");
INSERT INTO open_close_tb VALUES("747","434","4","0","2019-06-01");
INSERT INTO open_close_tb VALUES("748","128","26","24","2019-06-01");
INSERT INTO open_close_tb VALUES("749","302","4","0","2019-06-01");
INSERT INTO open_close_tb VALUES("750","338","9","0","2019-06-01");
INSERT INTO open_close_tb VALUES("751","339","13","10","2019-06-01");
INSERT INTO open_close_tb VALUES("752","262","16","0","2019-06-01");
INSERT INTO open_close_tb VALUES("753","261","39","0","2019-06-01");
INSERT INTO open_close_tb VALUES("754","319","69","0","2019-06-01");
INSERT INTO open_close_tb VALUES("755","373","49","0","2019-06-01");
INSERT INTO open_close_tb VALUES("756","448","27","0","2019-06-01");
INSERT INTO open_close_tb VALUES("757","295","130","0","2019-06-01");
INSERT INTO open_close_tb VALUES("758","458","85","0","2019-06-01");
INSERT INTO open_close_tb VALUES("759","123","19","0","2019-06-01");
INSERT INTO open_close_tb VALUES("760","355","16","0","2019-06-01");
INSERT INTO open_close_tb VALUES("761","3","30","0","2019-06-01");
INSERT INTO open_close_tb VALUES("762","4","20","0","2019-06-01");
INSERT INTO open_close_tb VALUES("763","225","6","0","2019-06-01");
INSERT INTO open_close_tb VALUES("764","212","18","0","2019-06-01");
INSERT INTO open_close_tb VALUES("765","320","26","20","2019-06-01");
INSERT INTO open_close_tb VALUES("766","452","17","0","2019-06-01");
INSERT INTO open_close_tb VALUES("767","299","2","0","2019-06-01");
INSERT INTO open_close_tb VALUES("768","460","62","0","2019-06-01");
INSERT INTO open_close_tb VALUES("769","443","65","0","2019-06-01");
INSERT INTO open_close_tb VALUES("770","501","38","0","2019-06-01");
INSERT INTO open_close_tb VALUES("771","374","84","82","2019-06-01");
INSERT INTO open_close_tb VALUES("772","322","66","0","2019-06-01");
INSERT INTO open_close_tb VALUES("773","317","36","0","2019-06-01");
INSERT INTO open_close_tb VALUES("774","442","613","0","2019-06-01");
INSERT INTO open_close_tb VALUES("775","5","40","0","2019-06-01");
INSERT INTO open_close_tb VALUES("776","375","51","0","2019-06-01");
INSERT INTO open_close_tb VALUES("777","137","8","0","2019-06-01");
INSERT INTO open_close_tb VALUES("778","62","97","0","2019-06-03");
INSERT INTO open_close_tb VALUES("779","123","15","0","2019-06-03");
INSERT INTO open_close_tb VALUES("780","128","14","12","2019-06-03");
INSERT INTO open_close_tb VALUES("781","295","128","0","2019-06-03");
INSERT INTO open_close_tb VALUES("782","182","15","0","2019-06-03");
INSERT INTO open_close_tb VALUES("783","137","5","4","2019-06-03");
INSERT INTO open_close_tb VALUES("784","373","48","43","2019-06-03");
INSERT INTO open_close_tb VALUES("785","362","8","0","2019-06-03");
INSERT INTO open_close_tb VALUES("786","312","28","0","2019-06-03");
INSERT INTO open_close_tb VALUES("787","313","28","0","2019-06-03");
INSERT INTO open_close_tb VALUES("788","31","7","6","2019-06-03");
INSERT INTO open_close_tb VALUES("789","492","45","0","2019-06-03");
INSERT INTO open_close_tb VALUES("790","30","474","0","2019-06-03");
INSERT INTO open_close_tb VALUES("791","303","8","0","2019-06-03");
INSERT INTO open_close_tb VALUES("792","372","3","0","2019-06-03");
INSERT INTO open_close_tb VALUES("793","5","38","0","2019-06-03");
INSERT INTO open_close_tb VALUES("794","4","19","0","2019-06-03");
INSERT INTO open_close_tb VALUES("795","163","13","0","2019-06-03");
INSERT INTO open_close_tb VALUES("796","310","13","0","2019-06-03");
INSERT INTO open_close_tb VALUES("797","209","13","0","2019-06-03");
INSERT INTO open_close_tb VALUES("798","374","81","0","2019-06-03");
INSERT INTO open_close_tb VALUES("799","88","10","0","2019-06-03");
INSERT INTO open_close_tb VALUES("800","277","221","0","2019-06-03");
INSERT INTO open_close_tb VALUES("801","322","59","0","2019-06-03");
INSERT INTO open_close_tb VALUES("802","178","11","0","2019-06-03");
INSERT INTO open_close_tb VALUES("803","30","472","452","2019-06-04");
INSERT INTO open_close_tb VALUES("804","320","10","0","2019-06-04");
INSERT INTO open_close_tb VALUES("805","133","14","13","2019-06-04");
INSERT INTO open_close_tb VALUES("806","375","45","0","2019-06-04");
INSERT INTO open_close_tb VALUES("807","21","487","0","2019-06-04");
INSERT INTO open_close_tb VALUES("808","64","57","0","2019-06-04");
INSERT INTO open_close_tb VALUES("809","373","42","0","2019-06-04");
INSERT INTO open_close_tb VALUES("810","443","56","0","2019-06-04");
INSERT INTO open_close_tb VALUES("811","131","7","0","2019-06-04");
INSERT INTO open_close_tb VALUES("812","217","7","0","2019-06-04");
INSERT INTO open_close_tb VALUES("813","182","14","0","2019-06-04");
INSERT INTO open_close_tb VALUES("814","211","1","0","2019-06-04");
INSERT INTO open_close_tb VALUES("815","89","13","12","2019-06-04");
INSERT INTO open_close_tb VALUES("816","98","39","38","2019-06-04");
INSERT INTO open_close_tb VALUES("817","319","66","0","2019-06-04");
INSERT INTO open_close_tb VALUES("818","42","4","0","2019-06-04");
INSERT INTO open_close_tb VALUES("819","492","60","0","2019-06-04");
INSERT INTO open_close_tb VALUES("820","501","37","0","2019-06-04");
INSERT INTO open_close_tb VALUES("821","500","39","0","2019-06-04");
INSERT INTO open_close_tb VALUES("822","317","35","0","2019-06-04");
INSERT INTO open_close_tb VALUES("823","449","71","0","2019-06-04");
INSERT INTO open_close_tb VALUES("824","422","51","0","2019-06-04");
INSERT INTO open_close_tb VALUES("825","65","287","277","2019-06-05");
INSERT INTO open_close_tb VALUES("826","208","7","0","2019-06-05");
INSERT INTO open_close_tb VALUES("827","30","432","0","2019-06-05");
INSERT INTO open_close_tb VALUES("828","98","37","0","2019-06-05");
INSERT INTO open_close_tb VALUES("829","20","344","0","2019-06-05");
INSERT INTO open_close_tb VALUES("830","458","84","0","2019-06-05");
INSERT INTO open_close_tb VALUES("831","214","9","0","2019-06-05");
INSERT INTO open_close_tb VALUES("832","312","27","0","2019-06-05");
INSERT INTO open_close_tb VALUES("833","13","20","0","2019-06-05");
INSERT INTO open_close_tb VALUES("834","255","23","0","2019-06-05");
INSERT INTO open_close_tb VALUES("835","256","28","0","2019-06-05");
INSERT INTO open_close_tb VALUES("836","319","59","57","2019-06-05");
INSERT INTO open_close_tb VALUES("837","317","33","0","2019-06-05");
INSERT INTO open_close_tb VALUES("838","448","24","0","2019-06-05");
INSERT INTO open_close_tb VALUES("839","102","13","0","2019-06-05");
INSERT INTO open_close_tb VALUES("840","374","80","0","2019-06-05");
INSERT INTO open_close_tb VALUES("841","459","168","0","2019-06-05");
INSERT INTO open_close_tb VALUES("842","302","3","0","2019-06-05");
INSERT INTO open_close_tb VALUES("843","123","13","0","2019-06-05");
INSERT INTO open_close_tb VALUES("844","371","16","0","2019-06-05");
INSERT INTO open_close_tb VALUES("845","469","35","0","2019-06-06");
INSERT INTO open_close_tb VALUES("846","133","6","0","2019-06-06");
INSERT INTO open_close_tb VALUES("847","98","36","0","2019-06-06");
INSERT INTO open_close_tb VALUES("848","305","51","0","2019-06-06");
INSERT INTO open_close_tb VALUES("849","283","31","0","2019-06-06");
INSERT INTO open_close_tb VALUES("850","214","8","6","2019-06-06");
INSERT INTO open_close_tb VALUES("851","442","612","0","2019-06-06");
INSERT INTO open_close_tb VALUES("852","400","6","0","2019-06-06");
INSERT INTO open_close_tb VALUES("853","62","95","0","2019-06-06");
INSERT INTO open_close_tb VALUES("854","65","276","270","2019-06-06");
INSERT INTO open_close_tb VALUES("855","460","61","0","2019-06-06");
INSERT INTO open_close_tb VALUES("856","116","29","0","2019-06-06");
INSERT INTO open_close_tb VALUES("857","35","424","0","2019-06-06");
INSERT INTO open_close_tb VALUES("858","270","1","0","2019-06-06");
INSERT INTO open_close_tb VALUES("859","435","15","0","2019-06-06");
INSERT INTO open_close_tb VALUES("860","370","10","0","2019-06-06");
INSERT INTO open_close_tb VALUES("861","254","24","0","2019-06-06");
INSERT INTO open_close_tb VALUES("862","299","1","0","2019-06-07");
INSERT INTO open_close_tb VALUES("863","327","22","0","2019-06-07");
INSERT INTO open_close_tb VALUES("864","295","127","0","2019-06-07");
INSERT INTO open_close_tb VALUES("865","122","23","0","2019-06-07");
INSERT INTO open_close_tb VALUES("866","390","19","0","2019-06-07");
INSERT INTO open_close_tb VALUES("867","6","19","0","2019-06-07");
INSERT INTO open_close_tb VALUES("868","62","65","0","2019-06-07");
INSERT INTO open_close_tb VALUES("869","212","17","0","2019-06-07");
INSERT INTO open_close_tb VALUES("870","96","8","0","2019-06-07");
INSERT INTO open_close_tb VALUES("871","375","44","0","2019-06-07");
INSERT INTO open_close_tb VALUES("872","373","40","0","2019-06-07");
INSERT INTO open_close_tb VALUES("873","460","59","0","2019-06-07");
INSERT INTO open_close_tb VALUES("874","65","268","265","2019-06-07");
INSERT INTO open_close_tb VALUES("875","444","4","0","2019-06-07");
INSERT INTO open_close_tb VALUES("876","450","81","0","2019-06-07");
INSERT INTO open_close_tb VALUES("877","445","76","0","2019-06-07");
INSERT INTO open_close_tb VALUES("878","305","43","0","2019-06-07");
INSERT INTO open_close_tb VALUES("879","317","32","31","2019-06-07");
INSERT INTO open_close_tb VALUES("880","458","83","59","2019-06-07");
INSERT INTO open_close_tb VALUES("881","27","9","0","2019-06-07");
INSERT INTO open_close_tb VALUES("882","384","14","0","2019-06-07");
INSERT INTO open_close_tb VALUES("883","383","20","0","2019-06-07");
INSERT INTO open_close_tb VALUES("884","446","32","0","2019-06-07");
INSERT INTO open_close_tb VALUES("885","447","70","69","2019-06-07");
INSERT INTO open_close_tb VALUES("886","463","6","0","2019-06-07");
INSERT INTO open_close_tb VALUES("887","180","256","0","2019-06-07");
INSERT INTO open_close_tb VALUES("888","422","49","0","2019-06-07");
INSERT INTO open_close_tb VALUES("889","459","167","0","2019-06-07");
INSERT INTO open_close_tb VALUES("890","21","485","0","2019-06-07");
INSERT INTO open_close_tb VALUES("891","4","16","0","2019-06-07");
INSERT INTO open_close_tb VALUES("892","13","17","0","2019-06-08");
INSERT INTO open_close_tb VALUES("893","27","8","0","2019-06-08");
INSERT INTO open_close_tb VALUES("894","129","122","0","2019-06-08");
INSERT INTO open_close_tb VALUES("895","373","39","0","2019-06-08");
INSERT INTO open_close_tb VALUES("896","65","263","213","2019-06-08");
INSERT INTO open_close_tb VALUES("897","193","155","135","2019-06-08");
INSERT INTO open_close_tb VALUES("898","20","341","331","2019-06-08");
INSERT INTO open_close_tb VALUES("899","241","147","0","2019-06-08");
INSERT INTO open_close_tb VALUES("900","305","38","0","2019-06-08");
INSERT INTO open_close_tb VALUES("901","495","9","8","2019-06-08");
INSERT INTO open_close_tb VALUES("902","75","6","0","2019-06-08");
INSERT INTO open_close_tb VALUES("903","319","43","0","2019-06-08");
INSERT INTO open_close_tb VALUES("904","317","29","0","2019-06-08");
INSERT INTO open_close_tb VALUES("905","422","48","0","2019-06-08");
INSERT INTO open_close_tb VALUES("906","446","30","0","2019-06-08");
INSERT INTO open_close_tb VALUES("907","277","206","0","2019-06-08");
INSERT INTO open_close_tb VALUES("908","458","58","0","2019-06-08");
INSERT INTO open_close_tb VALUES("909","255","22","0","2019-06-08");
INSERT INTO open_close_tb VALUES("910","375","42","0","2019-06-08");
INSERT INTO open_close_tb VALUES("911","249","21","0","2019-06-08");
INSERT INTO open_close_tb VALUES("912","35","423","0","2019-06-08");
INSERT INTO open_close_tb VALUES("913","390","18","0","2019-06-08");
INSERT INTO open_close_tb VALUES("914","133","5","0","2019-06-08");
INSERT INTO open_close_tb VALUES("915","30","431","0","2019-06-10");
INSERT INTO open_close_tb VALUES("916","459","166","0","2019-06-10");
INSERT INTO open_close_tb VALUES("917","393","9","0","2019-06-10");
INSERT INTO open_close_tb VALUES("918","322","53","0","2019-06-10");
INSERT INTO open_close_tb VALUES("919","302","2","0","2019-06-10");
INSERT INTO open_close_tb VALUES("920","20","329","0","2019-06-10");
INSERT INTO open_close_tb VALUES("921","436","16","0","2019-06-10");
INSERT INTO open_close_tb VALUES("922","440","42","0","2019-06-10");
INSERT INTO open_close_tb VALUES("923","441","624","0","2019-06-10");
INSERT INTO open_close_tb VALUES("924","319","41","0","2019-06-10");
INSERT INTO open_close_tb VALUES("925","307","1","0","2019-06-10");
INSERT INTO open_close_tb VALUES("926","213","24","0","2019-06-10");
INSERT INTO open_close_tb VALUES("927","65","205","183","2019-06-10");
INSERT INTO open_close_tb VALUES("928","261","29","0","2019-06-10");
INSERT INTO open_close_tb VALUES("929","262","6","0","2019-06-10");
INSERT INTO open_close_tb VALUES("930","246","26","0","2019-06-10");
INSERT INTO open_close_tb VALUES("931","458","57","0","2019-06-10");
INSERT INTO open_close_tb VALUES("932","212","16","0","2019-06-10");
INSERT INTO open_close_tb VALUES("933","295","123","0","2019-06-10");
INSERT INTO open_close_tb VALUES("934","321","129","123","2019-06-11");
INSERT INTO open_close_tb VALUES("935","317","28","0","2019-06-11");
INSERT INTO open_close_tb VALUES("936","98","35","0","2019-06-11");
INSERT INTO open_close_tb VALUES("937","133","4","0","2019-06-11");
INSERT INTO open_close_tb VALUES("938","373","38","0","2019-06-11");
INSERT INTO open_close_tb VALUES("939","295","122","0","2019-06-11");
INSERT INTO open_close_tb VALUES("940","440","37","0","2019-06-11");
INSERT INTO open_close_tb VALUES("941","305","35","0","2019-06-11");
INSERT INTO open_close_tb VALUES("942","65","178","172","2019-06-11");
INSERT INTO open_close_tb VALUES("943","120","57","0","2019-06-11");
INSERT INTO open_close_tb VALUES("944","117","6","0","2019-06-11");
INSERT INTO open_close_tb VALUES("945","35","422","0","2019-06-11");
INSERT INTO open_close_tb VALUES("946","319","33","0","2019-06-11");
INSERT INTO open_close_tb VALUES("947","3","29","0","2019-06-11");
INSERT INTO open_close_tb VALUES("948","65","162","129","2019-06-12");
INSERT INTO open_close_tb VALUES("949","322","100","0","2019-06-12");
INSERT INTO open_close_tb VALUES("950","75","5","0","2019-06-13");
INSERT INTO open_close_tb VALUES("951","34","19","0","2019-06-13");
INSERT INTO open_close_tb VALUES("952","208","6","0","2019-06-13");
INSERT INTO open_close_tb VALUES("953","128","165","0","2019-06-14");
INSERT INTO open_close_tb VALUES("954","295","121","0","2019-06-14");
INSERT INTO open_close_tb VALUES("955","440","36","0","2019-06-14");
INSERT INTO open_close_tb VALUES("956","441","619","0","2019-06-14");
INSERT INTO open_close_tb VALUES("957","106","545","0","2019-06-14");
INSERT INTO open_close_tb VALUES("958","180","254","0","2019-06-14");
INSERT INTO open_close_tb VALUES("959","460","53","0","2019-06-14");
INSERT INTO open_close_tb VALUES("960","214","5","0","2019-06-14");
INSERT INTO open_close_tb VALUES("961","442","611","0","2019-06-14");
INSERT INTO open_close_tb VALUES("962","65","128","0","2019-06-14");
INSERT INTO open_close_tb VALUES("963","56","14","0","2019-06-14");
INSERT INTO open_close_tb VALUES("964","402","84","0","2019-06-14");
INSERT INTO open_close_tb VALUES("965","317","27","0","2019-06-14");
INSERT INTO open_close_tb VALUES("966","221","4","0","2019-06-14");
INSERT INTO open_close_tb VALUES("967","446","29","0","2019-06-14");
INSERT INTO open_close_tb VALUES("968","96","7","0","2019-06-14");
INSERT INTO open_close_tb VALUES("969","5","36","0","2019-06-14");
INSERT INTO open_close_tb VALUES("970","133","3","0","2019-06-14");
INSERT INTO open_close_tb VALUES("971","458","56","0","2019-06-14");
INSERT INTO open_close_tb VALUES("972","57","12","0","2019-06-14");
INSERT INTO open_close_tb VALUES("973","248","33","0","2019-06-14");
INSERT INTO open_close_tb VALUES("974","249","20","0","2019-06-14");
INSERT INTO open_close_tb VALUES("975","260","22","0","2019-06-14");
INSERT INTO open_close_tb VALUES("976","30","428","0","2019-06-14");
INSERT INTO open_close_tb VALUES("977","273","1","0","2019-06-14");
INSERT INTO open_close_tb VALUES("978","151","3","0","2019-06-14");
INSERT INTO open_close_tb VALUES("979","384","13","0","2019-06-15");
INSERT INTO open_close_tb VALUES("980","7","3","0","2019-06-15");
INSERT INTO open_close_tb VALUES("981","321","103","98","2019-06-15");
INSERT INTO open_close_tb VALUES("982","393","8","0","2019-06-15");
INSERT INTO open_close_tb VALUES("983","129","121","0","2019-06-15");
INSERT INTO open_close_tb VALUES("984","128","159","153","2019-06-15");
INSERT INTO open_close_tb VALUES("985","295","119","0","2019-06-15");
INSERT INTO open_close_tb VALUES("986","65","120","89","2019-06-15");
INSERT INTO open_close_tb VALUES("987","73","26","0","2019-06-15");
INSERT INTO open_close_tb VALUES("988","375","41","0","2019-06-15");
INSERT INTO open_close_tb VALUES("989","373","34","0","2019-06-15");
INSERT INTO open_close_tb VALUES("990","5","34","0","2019-06-15");
INSERT INTO open_close_tb VALUES("991","61","71","0","2019-06-15");
INSERT INTO open_close_tb VALUES("992","122","22","0","2019-06-15");
INSERT INTO open_close_tb VALUES("993","317","26","21","2019-06-15");
INSERT INTO open_close_tb VALUES("994","319","17","0","2019-06-15");
INSERT INTO open_close_tb VALUES("995","120","53","0","2019-06-15");
INSERT INTO open_close_tb VALUES("996","62","62","61","2019-06-15");
INSERT INTO open_close_tb VALUES("997","248","32","0","2019-06-15");
INSERT INTO open_close_tb VALUES("998","89","9","0","2019-06-15");
INSERT INTO open_close_tb VALUES("999","283","30","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1000","501","35","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1001","422","47","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1002","30","426","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1003","251","32","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1004","13","16","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1005","14","24","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1006","98","34","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1007","136","9","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1008","163","12","0","2019-06-15");
INSERT INTO open_close_tb VALUES("1009","20","322","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1010","65","69","54","2019-06-17");
INSERT INTO open_close_tb VALUES("1011","26","20","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1012","232","17","16","2019-06-17");
INSERT INTO open_close_tb VALUES("1013","305","32","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1014","375","39","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1015","128","151","145","2019-06-17");
INSERT INTO open_close_tb VALUES("1016","295","116","115","2019-06-17");
INSERT INTO open_close_tb VALUES("1017","373","32","29","2019-06-17");
INSERT INTO open_close_tb VALUES("1018","422","45","43","2019-06-17");
INSERT INTO open_close_tb VALUES("1019","459","165","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1020","319","13","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1021","317","19","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1022","98","33","32","2019-06-17");
INSERT INTO open_close_tb VALUES("1023","30","420","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1024","5","32","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1025","321","88","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1026","460","51","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1027","214","4","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1028","449","70","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1029","246","25","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1030","434","3","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1031","495","7","0","2019-06-17");
INSERT INTO open_close_tb VALUES("1032","65","43","42","2019-06-18");
INSERT INTO open_close_tb VALUES("1033","106","544","541","2019-06-18");
INSERT INTO open_close_tb VALUES("1034","20","312","310","2019-06-18");
INSERT INTO open_close_tb VALUES("1035","236","38","36","2019-06-18");
INSERT INTO open_close_tb VALUES("1036","128","143","138","2019-06-18");
INSERT INTO open_close_tb VALUES("1037","129","115","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1038","373","26","19","2019-06-18");
INSERT INTO open_close_tb VALUES("1039","98","30","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1040","449","69","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1041","328","9","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1042","375","38","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1043","36","94","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1044","320","1","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1045","63","7","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1046","148","172","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1047","147","287","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1048","460","49","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1049","30","416","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1050","283","29","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1051","193","134","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1052","447","68","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1053","445","75","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1054","446","28","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1055","384","12","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1056","369","17","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1057","422","42","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1058","133","2","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1059","303","7","0","2019-06-18");
INSERT INTO open_close_tb VALUES("1060","65","7","4","2019-06-19");
INSERT INTO open_close_tb VALUES("1061","128","135","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1062","295","114","113","2019-06-19");
INSERT INTO open_close_tb VALUES("1063","458","54","52","2019-06-19");
INSERT INTO open_close_tb VALUES("1064","207","8","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1065","30","406","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1066","98","29","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1067","237","13","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1068","3","28","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1069","275","130","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1070","16","8","0","2019-06-19");
INSERT INTO open_close_tb VALUES("1071","458","51","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1072","128","133","128","2019-06-20");
INSERT INTO open_close_tb VALUES("1073","343","56","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1074","301","8","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1075","2","83","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1076","295","112","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1077","321","85","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1078","440","32","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1079","5","27","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1080","333","11","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1081","30","401","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1082","373","17","15","2019-06-20");
INSERT INTO open_close_tb VALUES("1083","176","1","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1084","447","62","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1085","442","607","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1086","203","2","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1087","209","12","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1088","61","69","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1089","214","3","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1090","452","16","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1091","106","540","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1092","98","28","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1093","247","10","0","2019-06-20");
INSERT INTO open_close_tb VALUES("1094","107","13","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1095","390","17","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1096","333","10","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1097","237","12","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1098","373","11","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1099","369","16","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1100","30","395","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1101","458","50","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1102","31","4","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1103","130","1","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1104","136","8","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1105","133","1","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1106","76","3","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1107","452","15","0","2019-06-21");
INSERT INTO open_close_tb VALUES("1108","400","5","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1109","30","394","388","2019-06-22");
INSERT INTO open_close_tb VALUES("1110","369","15","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1111","449","68","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1112","446","22","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1113","317","18","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1114","422","39","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1115","128","127","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1116","98","27","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1117","300","1","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1118","302","1","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1119","226","5","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1120","228","2","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1121","312","26","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1122","6","18","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1123","459","164","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1124","20","300","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1125","366","1","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1126","116","28","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1127","283","27","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1128","390","13","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1129","322","96","0","2019-06-22");
INSERT INTO open_close_tb VALUES("1130","88","9","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1131","336","19","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1132","160","38","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1133","467","14","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1134","442","606","599","2019-06-24");
INSERT INTO open_close_tb VALUES("1135","422","35","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1136","119","9","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1137","321","81","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1138","428","4","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1139","128","126","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1140","295","111","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1141","495","5","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1142","434","2","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1143","124","13","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1144","460","45","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1145","180","252","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1146","319","7","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1147","106","539","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1148","449","65","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1149","21","479","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1150","179","96","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1151","147","286","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1152","214","2","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1153","277","201","0","2019-06-24");
INSERT INTO open_close_tb VALUES("1154","62","58","0","2019-06-25");
INSERT INTO open_close_tb VALUES("1155","64","56","0","2019-06-25");
INSERT INTO open_close_tb VALUES("1156","312","25","0","2019-06-25");
INSERT INTO open_close_tb VALUES("1157","1","1","0","2019-06-25");
INSERT INTO open_close_tb VALUES("1158","3","26","0","2019-07-16");
INSERT INTO open_close_tb VALUES("1159","6","16","0","2019-07-16");
INSERT INTO open_close_tb VALUES("1160","2","81","77","2019-07-22");
INSERT INTO open_close_tb VALUES("1161","2","75","0","2019-07-23");
INSERT INTO open_close_tb VALUES("1162","7","1","0","2019-07-23");
INSERT INTO open_close_tb VALUES("1163","10","3","0","2019-07-23");
INSERT INTO open_close_tb VALUES("1164","6","15","0","2019-07-23");
INSERT INTO open_close_tb VALUES("1165","12","22","20","2019-07-31");
INSERT INTO open_close_tb VALUES("1166","13","20","18","2019-07-31");
INSERT INTO open_close_tb VALUES("1167","23","0","0","2019-08-08");
INSERT INTO open_close_tb VALUES("1168","25","20","14","2019-08-08");
INSERT INTO open_close_tb VALUES("1169","2","144","141","2019-08-11");
INSERT INTO open_close_tb VALUES("1170","18","6","0","2019-08-11");
INSERT INTO open_close_tb VALUES("1171","8","30","0","2019-08-11");
INSERT INTO open_close_tb VALUES("1172","25","36","0","2019-08-11");
INSERT INTO open_close_tb VALUES("1173","29","120","0","2019-08-11");
INSERT INTO open_close_tb VALUES("1174","2","138","124","2019-08-12");
INSERT INTO open_close_tb VALUES("1175","5","72","0","2019-08-12");
INSERT INTO open_close_tb VALUES("1176","4","72","0","2019-08-12");
INSERT INTO open_close_tb VALUES("1177","50","72","0","2019-08-12");
INSERT INTO open_close_tb VALUES("1178","29","119","115","2019-08-12");
INSERT INTO open_close_tb VALUES("1179","2","122","0","2019-08-15");
INSERT INTO open_close_tb VALUES("1180","2","119","111","2019-08-17");
INSERT INTO open_close_tb VALUES("1181","29","114","113","2019-08-17");
INSERT INTO open_close_tb VALUES("1182","56","36","0","2019-08-17");
INSERT INTO open_close_tb VALUES("1183","3","144","0","2019-08-17");
INSERT INTO open_close_tb VALUES("1184","27","36","35","2019-08-17");
INSERT INTO open_close_tb VALUES("1185","2","108","107","2019-08-20");
INSERT INTO open_close_tb VALUES("1186","29","111","110","2019-08-20");
INSERT INTO open_close_tb VALUES("1187","2","116","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1188","5","71","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1189","4","71","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1190","27","32","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1191","29","109","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1192","52","48","0","2019-09-04");
INSERT INTO open_close_tb VALUES("1193","2","114","111","2019-09-18");
INSERT INTO open_close_tb VALUES("1194","5","70","70","2019-09-18");
INSERT INTO open_close_tb VALUES("1195","4","70","0","2019-09-18");
INSERT INTO open_close_tb VALUES("1196","29","108","0","2019-09-18");
INSERT INTO open_close_tb VALUES("1197","2","109","0","2019-09-20");
INSERT INTO open_close_tb VALUES("1198","4","70","0","2019-09-20");
INSERT INTO open_close_tb VALUES("1199","767","10","1","2019-10-05");
INSERT INTO open_close_tb VALUES("1200","172","10","9","2019-10-05");
INSERT INTO open_close_tb VALUES("1201","372","10","9","2019-10-05");
INSERT INTO open_close_tb VALUES("1202","779","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1203","502","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1204","117","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1205","371","10","8","2019-10-05");
INSERT INTO open_close_tb VALUES("1206","246","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1207","100","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1208","471","10","0","2019-10-05");
INSERT INTO open_close_tb VALUES("1209","471","9","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1210","371","7","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1211","172","7","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1212","133","10","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1213","24","10","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1214","32","10","0","2019-10-06");
INSERT INTO open_close_tb VALUES("1215","840","10","0","2019-10-08");
INSERT INTO open_close_tb VALUES("1216","729","10","0","2019-10-08");
INSERT INTO open_close_tb VALUES("1217","471","8","0","2019-10-08");
INSERT INTO open_close_tb VALUES("1218","33","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1219","779","9","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1220","61","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1221","2","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1222","18","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1223","471","6","5","2019-10-09");
INSERT INTO open_close_tb VALUES("1224","371","6","3","2019-10-09");
INSERT INTO open_close_tb VALUES("1225","372","8","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1226","663","10","0","2019-10-09");
INSERT INTO open_close_tb VALUES("1227","767","15","14","2019-10-13");
INSERT INTO open_close_tb VALUES("1228","371","1","0","2019-10-13");
INSERT INTO open_close_tb VALUES("1229","172","30","0","2019-10-13");
INSERT INTO open_close_tb VALUES("1230","133","9","0","2019-10-13");
INSERT INTO open_close_tb VALUES("1231","471","4","0","2019-10-13");
INSERT INTO open_close_tb VALUES("1232","832","10","0","2019-10-18");
INSERT INTO open_close_tb VALUES("1233","420","10","0","2019-10-18");
INSERT INTO open_close_tb VALUES("1234","767","1693","1691","2019-10-25");
INSERT INTO open_close_tb VALUES("1235","371","650","0","2019-10-25");
INSERT INTO open_close_tb VALUES("1236","172","29","0","2019-10-25");
INSERT INTO open_close_tb VALUES("1237","501","11","0","2019-10-25");
INSERT INTO open_close_tb VALUES("1238","767","2391","2390","2019-10-27");
INSERT INTO open_close_tb VALUES("1239","767","2388","0","2019-10-28");
INSERT INTO open_close_tb VALUES("1240","371","649","0","2019-10-28");
INSERT INTO open_close_tb VALUES("1241","779","323","0","2019-10-28");
INSERT INTO open_close_tb VALUES("1242","307","10","5","2019-10-28");
INSERT INTO open_close_tb VALUES("1243","767","2385","2371","2019-11-03");
INSERT INTO open_close_tb VALUES("1244","779","322","321","2019-11-03");
INSERT INTO open_close_tb VALUES("1245","172","28","27","2019-11-03");
INSERT INTO open_close_tb VALUES("1246","767","2369","2363","2019-11-07");
INSERT INTO open_close_tb VALUES("1247","372","7","6","2019-11-07");
INSERT INTO open_close_tb VALUES("1248","172","26","0","2019-11-07");
INSERT INTO open_close_tb VALUES("1249","371","648","0","2019-11-07");
INSERT INTO open_close_tb VALUES("1250","501","460","0","2019-11-07");
INSERT INTO open_close_tb VALUES("1251","471","893","0","2019-11-07");
INSERT INTO open_close_tb VALUES("1252","767","2362","0","2019-11-08");
INSERT INTO open_close_tb VALUES("1253","767","2361","0","2019-11-11");
INSERT INTO open_close_tb VALUES("1254","172","25","0","2019-11-11");
INSERT INTO open_close_tb VALUES("1255","372","5","4","2019-11-11");
INSERT INTO open_close_tb VALUES("1256","897","6","4","2019-11-11");
INSERT INTO open_close_tb VALUES("1257","779","320","0","2019-11-11");
INSERT INTO open_close_tb VALUES("1258","371","647","0","2019-11-11");
INSERT INTO open_close_tb VALUES("1259","502","109","0","2019-11-11");
INSERT INTO open_close_tb VALUES("1260","767","2360","0","2019-11-12");
INSERT INTO open_close_tb VALUES("1261","172","23","0","2019-11-12");
INSERT INTO open_close_tb VALUES("1262","767","2359","2356","2019-11-16");
INSERT INTO open_close_tb VALUES("1263","767","2355","0","2019-11-27");
INSERT INTO open_close_tb VALUES("1264","172","22","0","2019-11-27");
INSERT INTO open_close_tb VALUES("1265","767","2352","2348","2019-11-29");
INSERT INTO open_close_tb VALUES("1266","172","21","19","2019-11-29");
INSERT INTO open_close_tb VALUES("1267","371","646","0","2019-11-29");
INSERT INTO open_close_tb VALUES("1268","372","3","0","2019-11-29");
INSERT INTO open_close_tb VALUES("1269","502","108","0","2019-11-29");
INSERT INTO open_close_tb VALUES("1270","501","459","0","2019-11-29");
INSERT INTO open_close_tb VALUES("1271","542","11","0","2019-11-29");
INSERT INTO open_close_tb VALUES("1272","782","10","0","2019-11-29");
INSERT INTO open_close_tb VALUES("1273","185","10","0","2019-11-29");
INSERT INTO open_close_tb VALUES("1274","900","597","589","2019-12-12");
INSERT INTO open_close_tb VALUES("1275","899","29","25","2019-12-12");
INSERT INTO open_close_tb VALUES("1276","900","0","0","2019-12-14");
INSERT INTO open_close_tb VALUES("1277","682","2","0","2019-12-14");
INSERT INTO open_close_tb VALUES("1278","682","1000","950","2019-12-15");
INSERT INTO open_close_tb VALUES("1279","682","940","938","2019-12-16");
INSERT INTO open_close_tb VALUES("1280","682","934","0","2019-12-17");
INSERT INTO open_close_tb VALUES("1281","683","20","17","2019-12-17");
INSERT INTO open_close_tb VALUES("1282","683","13","1","2019-12-19");
INSERT INTO open_close_tb VALUES("1283","682","930","923","2019-12-19");
INSERT INTO open_close_tb VALUES("1284","682","921","0","2019-12-22");
INSERT INTO open_close_tb VALUES("1285","683","30","0","2019-12-22");
INSERT INTO open_close_tb VALUES("1286","683","0","0","2020-02-05");
INSERT INTO open_close_tb VALUES("1287","84","1","0","2020-02-05");
INSERT INTO open_close_tb VALUES("1288","122","1","0","2020-02-05");
INSERT INTO open_close_tb VALUES("1289","138","1","0","2020-02-05");
INSERT INTO open_close_tb VALUES("1290","134","3","0","2020-02-05");
INSERT INTO open_close_tb VALUES("1291","62","1","0","2020-02-06");
INSERT INTO open_close_tb VALUES("1292","123","1","0","2020-02-06");
INSERT INTO open_close_tb VALUES("1293","126","2","0","2020-02-06");
INSERT INTO open_close_tb VALUES("1294","424","2","0","2020-02-06");
INSERT INTO open_close_tb VALUES("1295","257","2","0","2020-02-06");
INSERT INTO open_close_tb VALUES("1296","312","2","0","2020-02-06");



CREATE TABLE `part_payments_tb` (
  `payment_id` int(12) NOT NULL AUTO_INCREMENT,
  `amount` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_no` int(11) NOT NULL,
  `user_id` int(12) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO part_payments_tb VALUES("1","20","2019-12-10 09:27:55","4160","8");



CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `payment` decimal(10,2) NOT NULL,
  `payment_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `payment_for` date NOT NULL,
  `due` decimal(10,2) NOT NULL,
  `interest` decimal(10,2) NOT NULL,
  `remaining` decimal(10,2) NOT NULL,
  `status` varchar(20) NOT NULL,
  `rebate` decimal(10,2) NOT NULL,
  `or_no` int(11) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

INSERT INTO payment VALUES("1","0","1","104.50","2019-10-05 19:56:40","8","1","2019-10-05","104.50","0.00","0.00","paid","0.00","1901");
INSERT INTO payment VALUES("2","0","2","45.00","2019-10-05 21:46:16","8","1","2019-10-05","45.00","0.00","0.00","paid","0.00","1902");
INSERT INTO payment VALUES("3","0","3","15.50","2019-10-05 22:05:56","8","1","2019-10-05","15.50","0.00","0.00","paid","0.00","1903");
INSERT INTO payment VALUES("4","0","4","36.00","2019-10-05 22:06:56","8","1","2019-10-05","36.00","0.00","0.00","paid","0.00","1904");
INSERT INTO payment VALUES("5","0","5","30.00","2019-10-05 22:08:50","8","1","2019-10-05","30.00","0.00","0.00","paid","0.00","1905");
INSERT INTO payment VALUES("6","0","6","206.00","2019-10-05 22:09:30","8","1","2019-10-05","206.00","0.00","0.00","paid","0.00","1906");
INSERT INTO payment VALUES("7","0","7","30.00","2019-10-05 22:28:31","8","1","2019-10-05","30.00","0.00","0.00","paid","0.00","1907");
INSERT INTO payment VALUES("8","0","8","96.00","2019-10-05 22:48:50","8","1","2019-10-05","96.00","0.00","0.00","paid","0.00","1908");
INSERT INTO payment VALUES("9","0","9","110.50","2019-10-06 00:02:58","8","1","2019-10-06","110.50","0.00","0.00","paid","0.00","1909");
INSERT INTO payment VALUES("10","0","10","36.20","2019-10-06 13:54:39","8","1","2019-10-06","36.20","0.00","0.00","paid","0.00","1910");
INSERT INTO payment VALUES("11","0","11","66.50","2019-10-08 08:13:04","8","1","2019-10-08","66.50","0.00","0.00","paid","0.00","1911");
INSERT INTO payment VALUES("12","0","12","72.00","2019-10-08 08:13:18","8","1","2019-10-08","72.00","0.00","0.00","paid","0.00","1912");
INSERT INTO payment VALUES("13","0","13","11.85","2019-10-09 12:13:34","8","1","2019-10-09","11.85","0.00","0.00","paid","0.00","1913");
INSERT INTO payment VALUES("14","0","14","0.50","2019-10-09 12:24:25","8","1","2019-10-09","0.50","0.00","0.00","paid","0.00","1914");
INSERT INTO payment VALUES("15","0","15","30.00","2019-10-09 12:28:46","8","1","2019-10-09","30.00","0.00","0.00","paid","0.00","1915");
INSERT INTO payment VALUES("16","0","16","61.00","2019-10-09 12:30:58","8","1","2019-10-09","61.00","0.00","0.00","paid","0.00","1916");
INSERT INTO payment VALUES("17","0","17","160.00","2019-10-09 12:32:25","8","1","2019-10-09","160.00","0.00","0.00","paid","0.00","1917");
INSERT INTO payment VALUES("18","0","18","232.50","2019-10-09 12:48:24","8","1","2019-10-09","232.50","0.00","0.00","paid","0.00","1918");
INSERT INTO payment VALUES("19","0","19","75.00","2019-10-13 16:03:42","8","1","2019-10-13","75.00","0.00","0.00","paid","0.00","1919");
INSERT INTO payment VALUES("20","0","20","29.50","2019-10-13 16:08:45","8","1","2019-10-13","29.50","0.00","0.00","paid","0.00","1920");
INSERT INTO payment VALUES("21","0","21","36.00","2019-10-13 16:11:22","8","1","2019-10-13","36.00","0.00","0.00","paid","0.00","1921");
INSERT INTO payment VALUES("22","0","22","38.00","2019-10-18 08:13:14","8","1","2019-10-18","38.00","0.00","0.00","paid","0.00","1922");
INSERT INTO payment VALUES("23","0","23","90.00","2019-10-25 19:20:15","8","1","2019-10-25","90.00","0.00","0.00","paid","0.00","1923");
INSERT INTO payment VALUES("24","0","24","26.00","2019-10-25 19:21:22","8","1","2019-10-25","26.00","0.00","0.00","paid","0.00","1924");
INSERT INTO payment VALUES("25","0","25","15.00","2019-10-27 20:02:08","8","1","2019-10-27","15.00","0.00","0.00","paid","0.00","1925");
INSERT INTO payment VALUES("26","0","26","30.00","2019-10-27 20:03:14","8","1","2019-10-27","30.00","0.00","0.00","paid","0.00","1926");
INSERT INTO payment VALUES("27","0","27","105.00","2019-10-28 08:05:14","8","1","2019-10-28","105.00","0.00","0.00","paid","0.00","1927");
INSERT INTO payment VALUES("28","0","28","15.50","2019-10-28 13:56:29","8","1","2019-10-28","15.50","0.00","0.00","paid","0.00","1928");
INSERT INTO payment VALUES("29","0","29","9.00","2019-10-28 13:57:12","8","1","2019-10-28","9.00","0.00","0.00","paid","0.00","1929");
INSERT INTO payment VALUES("30","0","43","36.00","2019-11-07 19:21:51","8","1","2019-11-07","36.00","0.00","0.00","paid","0.00","1930");
INSERT INTO payment VALUES("31","0","51","30.00","2019-11-16 15:09:50","8","1","2019-11-16","30.00","0.00","0.00","paid","0.00","1931");
INSERT INTO payment VALUES("32","0","53","15.00","2019-11-16 15:15:21","8","1","2019-11-16","15.00","0.00","0.00","paid","0.00","1932");
INSERT INTO payment VALUES("33","0","54","45.00","2019-11-27 16:55:48","8","1","2019-11-27","45.00","0.00","0.00","paid","0.00","1933");
INSERT INTO payment VALUES("34","0","55","10.50","2019-11-27 16:56:37","8","1","2019-11-27","10.50","0.00","0.00","paid","0.00","1934");
INSERT INTO payment VALUES("35","0","3","8.00","2019-12-16 16:45:58","8","1","2019-12-16","8.00","0.00","0.00","paid","0.00","1904");
INSERT INTO payment VALUES("36","0","15","2.00","2019-12-19 14:59:20","8","1","2019-12-19","2.00","0.00","0.00","paid","0.00","1916");
INSERT INTO payment VALUES("37","0","16","4.00","2019-12-19 16:01:13","8","1","2019-12-19","4.00","0.00","0.00","paid","0.00","1917");
INSERT INTO payment VALUES("38","0","17","4.00","2019-12-19 16:10:18","8","1","2019-12-19","4.00","0.00","0.00","paid","0.00","1918");



CREATE TABLE `price_adjustments_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `product` text NOT NULL,
  `type_adjustment` text NOT NULL,
  `instruction` text NOT NULL,
  `amount` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

INSERT INTO price_adjustments_tb VALUES("6","All","Amount","Decrease","9.2","2019-12-01 15:54:12","4");
INSERT INTO price_adjustments_tb VALUES("7","All","Amount","Increase","5","2019-12-01 15:54:47","4");
INSERT INTO price_adjustments_tb VALUES("8","900","Amount","Increase","2","2019-12-01 16:05:58","4");
INSERT INTO price_adjustments_tb VALUES("9","900","Amount","Decrease","2","2019-12-01 16:06:22","4");
INSERT INTO price_adjustments_tb VALUES("10","900","Amount","Decrease","2","2019-12-01 16:07:20","4");
INSERT INTO price_adjustments_tb VALUES("11","900","Amount","Decrease","3","2019-12-01 16:07:45","4");
INSERT INTO price_adjustments_tb VALUES("12","900","Percent","Increase","10","2019-12-01 16:08:04","4");
INSERT INTO price_adjustments_tb VALUES("13","899","Amount","Increase","0.55","2019-12-01 16:11:44","4");



CREATE TABLE `product` (
  `prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_name` varchar(100) NOT NULL,
  `prod_desc` varchar(500) NOT NULL,
  `prod_price` decimal(10,2) NOT NULL,
  `prod_sell_price` text NOT NULL,
  `prod_pic` varchar(300) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `prod_qty` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `reorder` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `serial` varchar(50) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `belongs_to` int(12) NOT NULL,
  `stock_branch_id` int(12) NOT NULL,
  `barcode` text NOT NULL,
  `manufactor_date` text NOT NULL,
  `expire_date` text NOT NULL,
  `vat_status` text NOT NULL,
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=588 DEFAULT CHARSET=latin1;

INSERT INTO product VALUES("2","MILK JUG / CREAMER","MILK JUG / CREAMER","0.00","415","","72","2","1","0","22","","0000-00-00 00:00:00","1","1","2","","","inclusive");
INSERT INTO product VALUES("3","SUGAR BOWL W/LID","SUGAR BOWL W/LID","0.00","480","","72","4","1","0","22","","0000-00-00 00:00:00","1","1","3","","","inclusive");
INSERT INTO product VALUES("4","EGG CUP","EGG CUP","0.00","185","","72","2","1","0","22","","0000-00-00 00:00:00","1","1","4","","","inclusive");
INSERT INTO product VALUES("5","DIP BOWL","DIP BOWL","0.00","340","","72","18","1","0","22","","0000-00-00 00:00:00","1","1","5","","","inclusive");
INSERT INTO product VALUES("6","SALT & PEPPER","SALT & PEPPER","0.00","590","","72","2","1","0","22","","0000-00-00 00:00:00","1","1","6","","","inclusive");
INSERT INTO product VALUES("7","VEGETABLE BOWL (M)","VEGETABLE BOWL (M)","0.00","","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","7","","","inclusive");
INSERT INTO product VALUES("8","VEGETABLE BOWL (L)","VEGETABLE BOWL (L)","0.00","","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","8","","","inclusive");
INSERT INTO product VALUES("9","JAYNE JUG","JAYNE JUG","0.00","620","","72","9","1","0","22","","0000-00-00 00:00:00","1","1","9","","","inclusive");
INSERT INTO product VALUES("10","PITCHER 10" / 30oz","PITCHER 10" / 30oz","0.00","980","","72","4","1","0","22","","0000-00-00 00:00:00","1","1","10","","","inclusive");
INSERT INTO product VALUES("11","SPOONREST","SPOONREST","0.00","","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","11","","","inclusive");
INSERT INTO product VALUES("12","TEABAG HOLDER - SMALL","TEABAG HOLDER - SMALL","0.00","220","","72","2","1","0","22","","0000-00-00 00:00:00","1","1","12","","","inclusive");
INSERT INTO product VALUES("13","TEABAG HOLDER - LARGE","TEABAG HOLDER - LARGE","0.00","290","","72","4","1","0","22","","0000-00-00 00:00:00","1","1","13","","","inclusive");
INSERT INTO product VALUES("14","FRENCH LOAF TRAY","FRENCH LOAF TRAY","0.00","925","","72","2","1","0","22","","0000-00-00 00:00:00","1","1","14","","","inclusive");
INSERT INTO product VALUES("15","TEAPOT TEA FOR TWO","TEAPOT TEA FOR TWO","0.00","860","","72","1","1","0","22","","0000-00-00 00:00:00","1","1","15","","","inclusive");
INSERT INTO product VALUES("16","RICE BOWL (M)","RICE BOWL (M)","0.00","520","","72","1","1","0","22","","0000-00-00 00:00:00","1","1","16","","","inclusive");
INSERT INTO product VALUES("17","RICE BOWL (S)","RICE BOWL (S)","0.00","340","","72","1","1","0","22","","0000-00-00 00:00:00","1","1","17","","","inclusive");
INSERT INTO product VALUES("18","SQUARE CHARGER","SQUARE CHARGER","0.00","340","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","18","","","inclusive");
INSERT INTO product VALUES("19","SQUARE LUNCHEON","SQUARE LUNCHEON","0.00","340","","72","1","1","0","22","","0000-00-00 00:00:00","1","1","19","","","inclusive");
INSERT INTO product VALUES("20","DEVONNE TRAY - (M)","DEVONNE TRAY - (M)","0.00","1535","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","20","","","inclusive");
INSERT INTO product VALUES("21","DEVONNE TRAY - (L)","DEVONNE TRAY - (L)","0.00","","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","21","","","inclusive");
INSERT INTO product VALUES("22","APRIL'S PASTA","APRIL'S PASTA","0.00","410","","72","3","1","0","22","","0000-00-00 00:00:00","1","1","22","","","inclusive");
INSERT INTO product VALUES("23","SOAP DISH","SOAP DISH","0.00","230","","72","1","1","0","22","","0000-00-00 00:00:00","1","1","23","","","inclusive");
INSERT INTO product VALUES("24","TOOTHMUG","TOOTHMUG","0.00","230","","72","1","1","0","22","","0000-00-00 00:00:00","1","1","24","","","inclusive");
INSERT INTO product VALUES("25","CAKE STAND","CAKE STAND","0.00","","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","25","","","inclusive");
INSERT INTO product VALUES("26","CLASSIC COFFEE MUG","CLASSIC COFFEE MUG","0.00","420","","72","5","1","0","22","","0000-00-00 00:00:00","1","1","26","","","inclusive");
INSERT INTO product VALUES("27","BUTTER SLATE","BUTTER SLATE","0.00","","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","27","","","inclusive");
INSERT INTO product VALUES("28","COVERED BUTTER DISH","COVERED BUTTER DISH","0.00","580","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","28","","","inclusive");
INSERT INTO product VALUES("29","SHONA BOWL","SHONA BOWL","0.00","510","","72","1","1","0","22","","0000-00-00 00:00:00","1","1","29","","","inclusive");
INSERT INTO product VALUES("30","ANNA PLATTER","ANNA PLATTER","0.00","1445","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","30","","","inclusive");
INSERT INTO product VALUES("31","NAN'S PLATTER","NAN'S PLATTER","0.00","","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","31","","","inclusive");
INSERT INTO product VALUES("32","OVAL PLATTER - LARGE","OVAL PLATTER - LARGE","0.00","890","","72","3","1","0","22","","0000-00-00 00:00:00","1","1","32","","","inclusive");
INSERT INTO product VALUES("33","FLARED BOWL - MINI","FLARED BOWL - MINI","0.00","210","","72","4","1","0","22","","0000-00-00 00:00:00","1","1","33","","","inclusive");
INSERT INTO product VALUES("34","FLARED BOWL - SMALL","FLARED BOWL - SMALL","0.00","310","","72","5","1","0","22","","0000-00-00 00:00:00","1","1","34","","","inclusive");
INSERT INTO product VALUES("35","FLARED BOWL - MEDIUM","FLARED BOWL - MEDIUM","0.00","","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","35","","","inclusive");
INSERT INTO product VALUES("36","FLARED BOWL - LARGE","FLARED BOWL - LARGE","0.00","1320","","72","2","1","0","22","","0000-00-00 00:00:00","1","1","36","","","inclusive");
INSERT INTO product VALUES("37","VASE (L)","VASE (L)","0.00","","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","37","","","inclusive");
INSERT INTO product VALUES("38","VASE - MEDIUM","VASE - MEDIUM","0.00","","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","38","","","inclusive");
INSERT INTO product VALUES("39","WINE COOLER WITH RIM","WINE COOLER WITH RIM","0.00","1920","","72","1","1","0","22","","0000-00-00 00:00:00","1","1","39","","","inclusive");
INSERT INTO product VALUES("40","LAMP BASE - ALL","LAMP BASE - ALL","0.00","2380","","72","4","1","0","22","","0000-00-00 00:00:00","1","1","40","","","inclusive");
INSERT INTO product VALUES("41","DOG BOWL","DOG BOWL","0.00","645","","72","1","1","0","22","","0000-00-00 00:00:00","1","1","41","","","inclusive");
INSERT INTO product VALUES("42","SALAD DRESSING BOTTLE","SALAD DRESSING BOTTLE","0.00","","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","42","","","inclusive");
INSERT INTO product VALUES("43","PLANT HOLDER","PLANT HOLDER","0.00","1225","","72","2","1","0","22","","0000-00-00 00:00:00","1","1","43","","","inclusive");
INSERT INTO product VALUES("44","CHEESE BOARD","CHEESE BOARD","0.00","","","72","0","1","0","22","","0000-00-00 00:00:00","1","1","44","","","inclusive");
INSERT INTO product VALUES("45","DORIS MARIE BOWL (BAGUETTE)","DORIS MARIE BOWL (BAGUETTE)","0.00","1380","","72","1","1","0","22","","0000-00-00 00:00:00","1","1","45","","","inclusive");
INSERT INTO product VALUES("46","MULTIPURPOSE BOWL - SIZE 5","MULTIPURPOSE BOWL - SIZE 5","0.00","380","","72","4","1","0","23","","0000-00-00 00:00:00","1","1","46","","","inclusive");
INSERT INTO product VALUES("47","MULTIPURPOSE BOWL - SIZE 6","MULTIPURPOSE BOWL - SIZE 6","0.00","435","","72","7","1","0","23","","0000-00-00 00:00:00","1","1","47","","","inclusive");
INSERT INTO product VALUES("48","MUTLIPURPOSE BOWL - SIZE 7 ","MUTLIPURPOSE BOWL - SIZE 7 ","0.00","450","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","48","","","inclusive");
INSERT INTO product VALUES("49","CAKE PLATE","CAKE PLATE","0.00","1830","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","49","","","inclusive");
INSERT INTO product VALUES("50","CAKE STAND","CAKE STAND","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","50","","","inclusive");
INSERT INTO product VALUES("51","CAKE CUTTER","CAKE CUTTER","0.00","125","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","51","","","inclusive");
INSERT INTO product VALUES("52","DOUBLE HANDLE SERVING PLATTER","DOUBLE HANDLE SERVING PLATTER","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","52","","","inclusive");
INSERT INTO product VALUES("53","FLARE BOWL - LARGE","FLARE BOWL - LARGE","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","53","","","inclusive");
INSERT INTO product VALUES("54","FLARE BOWL - MED","FLARE BOWL - MED","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","54","","","inclusive");
INSERT INTO product VALUES("55","FLARE BOWL - SMALL","FLARE BOWL - SMALL","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","55","","","inclusive");
INSERT INTO product VALUES("56","OFF CENTRE FLARE BOWL - LARGE","OFF CENTRE FLARE BOWL - LARGE","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","56","","","inclusive");
INSERT INTO product VALUES("57","BUTTER DISH - WITH KNOB HANDLE","BUTTER DISH - WITH KNOB HANDLE","0.00","710","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","57","","","inclusive");
INSERT INTO product VALUES("58","FRUIT BOWL WITH HANDLE - 18.5X26CM","FRUIT BOWL WITH HANDLE - 18.5X26CM","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","58","","","inclusive");
INSERT INTO product VALUES("59","30'S TEA CUP & SAUCER","30'S TEA CUP & SAUCER","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","59","","","inclusive");
INSERT INTO product VALUES("60","30'S MILK JUG","30'S MILK JUG","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","60","","","inclusive");
INSERT INTO product VALUES("61","30'S SUGAR BOWL","30'S SUGAR BOWL","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","61","","","inclusive");
INSERT INTO product VALUES("62","30'S TEA POT","30'S TEA POT","0.00","585","","72","-2","1","0","23","","2020-02-06 17:01:48","1","1","62","","","inclusive");
INSERT INTO product VALUES("63","SHERLY PLATTER","SHERLY PLATTER","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","63","","","inclusive");
INSERT INTO product VALUES("64","FRENCH OVAL GARLIC PLATTER","FRENCH OVAL GARLIC PLATTER","0.00","720","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","64","","","inclusive");
INSERT INTO product VALUES("65","SPAGHETTI BOWL - STANDARD-23X15CM","SPAGHETTI BOWL - STANDARD-23X15CM","0.00","420","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","65","","","inclusive");
INSERT INTO product VALUES("66","TURKISH BOWL-34X13CM","TURKISH BOWL-34X13CM","0.00","375","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","66","","","inclusive");
INSERT INTO product VALUES("67","MUG - MUG","MUG - MUG","0.00","250","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","67","","","inclusive");
INSERT INTO product VALUES("68","MUG - NN COFFEE   ","MUG - NN COFFEE   ","0.00","250","","72","3","1","0","23","","0000-00-00 00:00:00","1","1","68","","","inclusive");
INSERT INTO product VALUES("69","WAVE SNACK & DIP","WAVE SNACK & DIP","0.00","340","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","69","","","inclusive");
INSERT INTO product VALUES("70","STACK TEA SET","STACK TEA SET","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","70","","","inclusive");
INSERT INTO product VALUES("71","TAPERED VASE","TAPERED VASE","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","71","","","inclusive");
INSERT INTO product VALUES("72","SQUARE VASE","SQUARE VASE","0.00","590","","72","3","1","0","23","","0000-00-00 00:00:00","1","1","72","","","inclusive");
INSERT INTO product VALUES("73","CYCLINDER VASE","CYCLINDER VASE","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","73","","","inclusive");
INSERT INTO product VALUES("74","TEAR DROP BOWL - LARGE","TEAR DROP BOWL - LARGE","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","74","","","inclusive");
INSERT INTO product VALUES("75","GINGER JAR -MED-WITH LID ","GINGER JAR -MED-WITH LID ","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","75","","","inclusive");
INSERT INTO product VALUES("76","GINGER JAR - NO LID","GINGER JAR - NO LID","0.00","660","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","76","","","inclusive");
INSERT INTO product VALUES("77","CRUET SET ","CRUET SET ","0.00","390","","72","7","1","0","23","","0000-00-00 00:00:00","1","1","77","","","inclusive");
INSERT INTO product VALUES("78","BALL VASE ","BALL VASE ","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","78","","","inclusive");
INSERT INTO product VALUES("79","V FLARE BOWL - 9.5CM","V FLARE BOWL - 9.5CM","0.00","325","","72","6","1","0","23","","0000-00-00 00:00:00","1","1","79","","","inclusive");
INSERT INTO product VALUES("80","V FLARE BOWL - 14.5CM","V FLARE BOWL - 14.5CM","0.00","435","","72","8","1","0","23","","0000-00-00 00:00:00","1","1","80","","","inclusive");
INSERT INTO product VALUES("81","SQUARE STORY PLATE - 34.5CM - GALLERY WARE","SQUARE STORY PLATE - 34.5CM - GALLERY WARE","0.00","470","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","81","","","inclusive");
INSERT INTO product VALUES("82","DECO BOWL","DECO BOWL","0.00","860","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","82","","","inclusive");
INSERT INTO product VALUES("83","GEOMETRIC PLATE - SQUARE","GEOMETRIC PLATE - SQUARE","0.00","430","","72","2","1","0","23","","0000-00-00 00:00:00","1","1","83","","","inclusive");
INSERT INTO product VALUES("84","3 DIV SNACK DIP BOWL CENTRE HANDLE - ROUND","3 DIV SNACK DIP BOWL CENTRE HANDLE - ROUND","0.00","340","","72","-1","1","0","23","","2020-02-05 22:44:28","1","1","84","","","inclusive");
INSERT INTO product VALUES("85","COASTER ","COASTER ","0.00","120","","72","2","1","0","23","","0000-00-00 00:00:00","1","1","85","","","inclusive");
INSERT INTO product VALUES("86","SUSHI PLATTER-RECT-35X15.5CM","SUSHI PLATTER-RECT-35X15.5CM","0.00","545","","72","3","1","0","23","","0000-00-00 00:00:00","1","1","86","","","inclusive");
INSERT INTO product VALUES("87","PEANUT PLATE","PEANUT PLATE","0.00","220","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","87","","","inclusive");
INSERT INTO product VALUES("88","NARROW GARLIC PLATTER - 53X22CM","NARROW GARLIC PLATTER - 53X22CM","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","88","","","inclusive");
INSERT INTO product VALUES("89","TEABAG HOLDER","TEABAG HOLDER","0.00","130","","72","3","1","0","23","","0000-00-00 00:00:00","1","1","89","","","inclusive");
INSERT INTO product VALUES("90","SALAD BOWL","SALAD BOWL","0.00","660","","72","3","1","0","23","","0000-00-00 00:00:00","1","1","90","","","inclusive");
INSERT INTO product VALUES("91","CASSEROLE DISH - SMALL","CASSEROLE DISH - SMALL","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","91","","","inclusive");
INSERT INTO product VALUES("92","CASSEROLE DISH - LARGE","CASSEROLE DISH - LARGE","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","92","","","inclusive");
INSERT INTO product VALUES("93","TEAR DROP BOWL ","TEAR DROP BOWL ","0.00","275","","72","3","1","0","23","","0000-00-00 00:00:00","1","1","93","","","inclusive");
INSERT INTO product VALUES("94","DOUBLE SPOON REST","DOUBLE SPOON REST","0.00","320","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","94","","","inclusive");
INSERT INTO product VALUES("95","SINGLE SPOON REST","SINGLE SPOON REST","0.00","150","","72","2","1","0","23","","0000-00-00 00:00:00","1","1","95","","","inclusive");
INSERT INTO product VALUES("96","GRAVY BOAT WITH SAUCER","GRAVY BOAT WITH SAUCER","0.00","650","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","96","","","inclusive");
INSERT INTO product VALUES("97","GRAVY BOAT WITH SPOUT","GRAVY BOAT WITH SPOUT","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","97","","","inclusive");
INSERT INTO product VALUES("98","MEGA ROUND PLATTER","MEGA ROUND PLATTER","0.00","1085","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","98","","","inclusive");
INSERT INTO product VALUES("99","SHALLOW PLATTER ","SHALLOW PLATTER ","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","99","","","inclusive");
INSERT INTO product VALUES("100","WATER JUG STANDARD","WATER JUG STANDARD","0.00","820","","72","2","1","0","23","","0000-00-00 00:00:00","1","1","100","","","inclusive");
INSERT INTO product VALUES("101","POD BOWL","POD BOWL","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","101","","","inclusive");
INSERT INTO product VALUES("102","OVAL GARLIC PLATTER","OVAL GARLIC PLATTER","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","102","","","inclusive");
INSERT INTO product VALUES("103","3 DIVISION SNACK DIP BOWL","3 DIVISION SNACK DIP BOWL","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","103","","","inclusive");
INSERT INTO product VALUES("104","YOGURT BOWL ","YOGURT BOWL ","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","104","","","inclusive");
INSERT INTO product VALUES("105","RECT  SNACK PLATTER STANDARD","RECT  SNACK PLATTER STANDARD","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","105","","","inclusive");
INSERT INTO product VALUES("106","TALL MODEL VASE ","TALL MODEL VASE ","0.00","825","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","106","","","inclusive");
INSERT INTO product VALUES("107","WRAP MUG","WRAP MUG","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","107","","","inclusive");
INSERT INTO product VALUES("108","CEREAL BOWL","CEREAL BOWL","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","108","","","inclusive");
INSERT INTO product VALUES("109","SQUARE PLATE - SIZE 1 (17.5cm)","SQUARE PLATE - SIZE 1 (17.5cm)","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","109","","","inclusive");
INSERT INTO product VALUES("110","SQUARE PLATE - SIZE 2 (23.5cm)","SQUARE PLATE - SIZE 2 (23.5cm)","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","110","","","inclusive");
INSERT INTO product VALUES("111","SQUARE PLATE - SIZE 3 (27.5cm)","SQUARE PLATE - SIZE 3 (27.5cm)","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","111","","","inclusive");
INSERT INTO product VALUES("112","SQUARE PLATE - SIZE 4 (34.5cm)","SQUARE PLATE - SIZE 4 (34.5cm)","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","112","","","inclusive");
INSERT INTO product VALUES("113","CATHY BOWL","CATHY BOWL","0.00","610","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","113","","","inclusive");
INSERT INTO product VALUES("114","KAY BOWL- 40CM","KAY BOWL- 40CM","0.00","1080","","72","3","1","0","23","","0000-00-00 00:00:00","1","1","114","","","inclusive");
INSERT INTO product VALUES("115","ALUMINIUM STAND","ALUMINIUM STAND","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","115","","","inclusive");
INSERT INTO product VALUES("116","ALUMINIUM STAND","ALUMINIUM STAND","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","116","","","inclusive");
INSERT INTO product VALUES("117","MEGA PLATTER - 42 X42 CM SQUARE","MEGA PLATTER - 42 X42 CM SQUARE","0.00","950","","72","2","1","0","23","","0000-00-00 00:00:00","1","1","117","","","inclusive");
INSERT INTO product VALUES("118","GALLERY 3D JUG","GALLERY 3D JUG","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","118","","","inclusive");
INSERT INTO product VALUES("119","WHIRLWIND CANDLE HOLDER  LGE","WHIRLWIND CANDLE HOLDER  LGE","0.00","400","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","119","","","inclusive");
INSERT INTO product VALUES("120","WHIRLWIND CANDLE HOLDER - SM","WHIRLWIND CANDLE HOLDER - SM","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","120","","","inclusive");
INSERT INTO product VALUES("121","FRAGRANCE BURNER","FRAGRANCE BURNER","0.00","410","","72","2","1","0","23","","0000-00-00 00:00:00","1","1","121","","","inclusive");
INSERT INTO product VALUES("122","3D FRAGRANCE BURNER","3D FRAGRANCE BURNER","0.00","510","","72","0","1","0","23","","2020-02-05 22:44:28","1","1","122","","","inclusive");
INSERT INTO product VALUES("123","3D TURKISH BOWL - 34X13CM","3D TURKISH BOWL - 34X13CM","0.00","550","","72","0","1","0","23","","2020-02-06 17:01:48","1","1","123","","","inclusive");
INSERT INTO product VALUES("124","3D THAI BOWL - LARGE","3D THAI BOWL - LARGE","0.00","760","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","124","","","inclusive");
INSERT INTO product VALUES("125","3D CB VASE - STANDARD","3D CB VASE - STANDARD","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","125","","","inclusive");
INSERT INTO product VALUES("126","3D WATER JUG ","3D WATER JUG ","0.00","820","","72","1","1","0","23","","2020-02-06 17:01:48","1","1","126","","","inclusive");
INSERT INTO product VALUES("127","3D EGG RELIEF CRUET SET","3D EGG RELIEF CRUET SET","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","127","","","inclusive");
INSERT INTO product VALUES("128","3D EGG CRUET SET","3D EGG CRUET SET","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","128","","","inclusive");
INSERT INTO product VALUES("129","3D CURVY PLATTER","3D CURVY PLATTER","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","129","","","inclusive");
INSERT INTO product VALUES("130","3D TRINKET BOX ROUND","3D TRINKET BOX ROUND","0.00","350","","72","2","1","0","23","","0000-00-00 00:00:00","1","1","130","","","inclusive");
INSERT INTO product VALUES("131","3D SAFARI DOUBLE SNACK BOWL - ROUND","3D SAFARI DOUBLE SNACK BOWL - ROUND","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","131","","","inclusive");
INSERT INTO product VALUES("132","3D SAFARI SUNDAE BOWL","3D SAFARI SUNDAE BOWL","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","132","","","inclusive");
INSERT INTO product VALUES("133","3D CURVY BOWL","3D CURVY BOWL","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","133","","","inclusive");
INSERT INTO product VALUES("134","3D SPAGHETTI BOWL","3D SPAGHETTI BOWL","0.00","710","","72","2","1","0","23","","2020-02-05 22:46:10","1","1","134","","","inclusive");
INSERT INTO product VALUES("135","3D RELIEF GARLIC PLATTER","3D RELIEF GARLIC PLATTER","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","135","","","inclusive");
INSERT INTO product VALUES("136","3D TOOTHPICK HOLDER","3D TOOTHPICK HOLDER","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","136","","","inclusive");
INSERT INTO product VALUES("137","3D RELIEF SPAGHETTI BOWL","3D RELIEF SPAGHETTI BOWL","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","137","","","inclusive");
INSERT INTO product VALUES("138","3D RELIEF MUG STANDARD","3D RELIEF MUG STANDARD","0.00","250","","72","0","1","0","23","","2020-02-05 22:44:28","1","1","138","","","inclusive");
INSERT INTO product VALUES("139","3D MILK JUG","3D MILK JUG","0.00","435","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","139","","","inclusive");
INSERT INTO product VALUES("140","3D BENDY BOWL","3D BENDY BOWL","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","140","","","inclusive");
INSERT INTO product VALUES("141","BENDY BOWL SIZE 1","BENDY BOWL SIZE 1","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","141","","","inclusive");
INSERT INTO product VALUES("142","BENDY BOWL SIZE 5","BENDY BOWL SIZE 5","0.00","265","","72","5","1","0","23","","0000-00-00 00:00:00","1","1","142","","","inclusive");
INSERT INTO product VALUES("143","BENDY BOWL SIZE 7","BENDY BOWL SIZE 7","0.00","350","","72","4","1","0","23","","0000-00-00 00:00:00","1","1","143","","","inclusive");
INSERT INTO product VALUES("144","MULTIPURPOSE BOWL SIZE 3 - 3D-SANTA","MULTIPURPOSE BOWL SIZE 3 - 3D-SANTA","0.00","250","","72","3","1","0","23","","0000-00-00 00:00:00","1","1","144","","","inclusive");
INSERT INTO product VALUES("145","MULTIPURPOSE BOWL SIZE 7 - 3D-SANTA","MULTIPURPOSE BOWL SIZE 7 - 3D-SANTA","0.00","370","","72","3","1","0","23","","0000-00-00 00:00:00","1","1","145","","","inclusive");
INSERT INTO product VALUES("146","FLARE BOWL-LARGE","FLARE BOWL-LARGE","0.00","560","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","146","","","inclusive");
INSERT INTO product VALUES("147","FLARE BOWL-MEDIUM","FLARE BOWL-MEDIUM","0.00","350","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","147","","","inclusive");
INSERT INTO product VALUES("148","CHRISTMAS TREE DECO","CHRISTMAS TREE DECO","0.00","100","","72","11","1","0","23","","0000-00-00 00:00:00","1","1","148","","","inclusive");
INSERT INTO product VALUES("149","FISH PLATE","FISH PLATE","0.00","410","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","149","","","inclusive");
INSERT INTO product VALUES("150","FLAREBOWL-SMALL","FLAREBOWL-SMALL","0.00","250","","72","1","1","0","23","","0000-00-00 00:00:00","1","1","150","","","inclusive");
INSERT INTO product VALUES("151","TRIO SAUCE BOWL","TRIO SAUCE BOWL","0.00","","","72","0","1","0","23","","0000-00-00 00:00:00","1","1","151","","","inclusive");
INSERT INTO product VALUES("152","Forest Three door Server","Forest Three door Server","0.00","11195","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","152","","","inclusive");
INSERT INTO product VALUES("153","Pantry Cupboard","Pantry Cupboard","0.00","12, 430.00","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","153","","","inclusive");
INSERT INTO product VALUES("154","Sonja Chairs","Sonja Chairs","0.00","4180","","72","2","1","0","24","","0000-00-00 00:00:00","1","1","154","","","inclusive");
INSERT INTO product VALUES("155","Sengwa Coffee Table","Sengwa Coffee Table","0.00","19215","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","155","","","inclusive");
INSERT INTO product VALUES("156","Gallery Headboard Double","Gallery Headboard Double","0.00","8650","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","156","","","inclusive");
INSERT INTO product VALUES("157","Landrey Server","Landrey Server","0.00","10200","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","157","","","inclusive");
INSERT INTO product VALUES("158","French Caf? Pub Table","French Caf? Pub Table","0.00","8210","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","158","","","inclusive");
INSERT INTO product VALUES("159","Forrest Plasma Unit - mahogany top","Forrest Plasma Unit - mahogany top","0.00","8970","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","159","","","inclusive");
INSERT INTO product VALUES("160","Batonga Plasma Unit","Batonga Plasma Unit","0.00","12820","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","160","","","inclusive");
INSERT INTO product VALUES("161","Dressing Table Mirror","Dressing Table Mirror","0.00","4780","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","161","","","inclusive");
INSERT INTO product VALUES("162","Rectangle Chopping Board","Rectangle Chopping Board","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","162","","","inclusive");
INSERT INTO product VALUES("163","Chopping Board Round 300mm","Chopping Board Round 300mm","0.00","510","","72","2","1","0","24","","0000-00-00 00:00:00","1","1","163","","","inclusive");
INSERT INTO product VALUES("164","Chopping Board Round 400mm","Chopping Board Round 400mm","0.00","550","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","164","","","inclusive");
INSERT INTO product VALUES("165","Chopping Board Round - large","Chopping Board Round - large","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","165","","","inclusive");
INSERT INTO product VALUES("166","Chopping Board round - 300mm Leather handle","Chopping Board round - 300mm Leather handle","0.00","510","","72","2","1","0","24","","0000-00-00 00:00:00","1","1","166","","","inclusive");
INSERT INTO product VALUES("167","Chopping Board round - 400mm Leather handle","Chopping Board round - 400mm Leather handle","0.00","550","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","167","","","inclusive");
INSERT INTO product VALUES("168","Recipe stand","Recipe stand","0.00","550","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","168","","","inclusive");
INSERT INTO product VALUES("169","Ilala shade 1000 x 700 - Drum","Ilala shade 1000 x 700 - Drum","0.00","1520","","72","1","1","0","25","","0000-00-00 00:00:00","1","1","169","","","inclusive");
INSERT INTO product VALUES("170","Ilala shade 800 x 600 - Drum","Ilala shade 800 x 600 - Drum","0.00","1150","","72","1","1","0","25","","0000-00-00 00:00:00","1","1","170","","","inclusive");
INSERT INTO product VALUES("171","Ilala shade 700 x 500 - Drum","Ilala shade 700 x 500 - Drum","0.00","915","","72","1","1","0","25","","0000-00-00 00:00:00","1","1","171","","","inclusive");
INSERT INTO product VALUES("172","Ilala shade 600 x 400","Ilala shade 600 x 400","0.00","","","72","0","1","0","25","","0000-00-00 00:00:00","1","1","172","","","inclusive");
INSERT INTO product VALUES("173","Ilala shade 500 x 300","Ilala shade 500 x 300","0.00","","","72","0","1","0","25","","0000-00-00 00:00:00","1","1","173","","","inclusive");
INSERT INTO product VALUES("174","Ilala shade 300 x 250","Ilala shade 300 x 250","0.00","","","72","0","1","0","25","","0000-00-00 00:00:00","1","1","174","","","inclusive");
INSERT INTO product VALUES("175","Ilala shade 220 x 200 - Drum","Ilala shade 220 x 200 - Drum","0.00","290","","72","1","1","0","25","","0000-00-00 00:00:00","1","1","175","","","inclusive");
INSERT INTO product VALUES("176","Ilala shade 450 x 330","Ilala shade 450 x 330","0.00","","","72","0","1","0","25","","0000-00-00 00:00:00","1","1","176","","","inclusive");
INSERT INTO product VALUES("177","Ilala shade 450 x 420 - Bell","Ilala shade 450 x 420 - Bell","0.00","570","","72","1","1","0","25","","0000-00-00 00:00:00","1","1","177","","","inclusive");
INSERT INTO product VALUES("178","Lampshade fabric 55cm - White Inner","Lampshade fabric 55cm - White Inner","0.00","1250","","72","2","1","0","26","","0000-00-00 00:00:00","1","1","178","","","inclusive");
INSERT INTO product VALUES("179","Lampshade fabric 45cm - White Inner","Lampshade fabric 45cm - White Inner","0.00","750","","72","6","1","0","26","","0000-00-00 00:00:00","1","1","179","","","inclusive");
INSERT INTO product VALUES("180","Lampshade fabric Wide 45cm - White Inner","Lampshade fabric Wide 45cm - White Inner","0.00","820","","72","1","1","0","26","","0000-00-00 00:00:00","1","1","180","","","");
INSERT INTO product VALUES("181","Lampshade fabric 45cm - Colour Inner","Lampshade fabric 45cm - Colour Inner","0.00","980","","72","4","1","0","26","","0000-00-00 00:00:00","1","1","181","","","inclusive");
INSERT INTO product VALUES("182","Lampshade fabric 35cm - White Inner","Lampshade fabric 35cm - White Inner","0.00","590","","72","3","1","0","26","","0000-00-00 00:00:00","1","1","182","","","inclusive");
INSERT INTO product VALUES("183","Lampshade fabric 35cm - Colour Inner","Lampshade fabric 35cm - Colour Inner","0.00","760","","72","0","1","0","26","","0000-00-00 00:00:00","1","1","183","","","inclusive");
INSERT INTO product VALUES("184","Lampshade fabric 25cm -White Inner","Lampshade fabric 25cm -White Inner","0.00","490","","72","0","1","0","26","","0000-00-00 00:00:00","1","1","184","","","inclusive");
INSERT INTO product VALUES("185","Lampshade fabric 25cm - Colour Inner","Lampshade fabric 25cm - Colour Inner","0.00","690","","72","2","1","0","26","","0000-00-00 00:00:00","1","1","185","","","inclusive");
INSERT INTO product VALUES("186","Tri pod base","Tri pod base","0.00","","","72","0","1","0","27","","0000-00-00 00:00:00","1","1","186","","","inclusive");
INSERT INTO product VALUES("187","L Lamp base","L Lamp base","0.00","2100","","72","6","1","0","26","","0000-00-00 00:00:00","1","1","187","","","inclusive");
INSERT INTO product VALUES("188","Table Lamp - Glass Clyinder","Table Lamp - Glass Clyinder","0.00","1380","","72","1","1","0","27","","0000-00-00 00:00:00","1","1","188","","","inclusive");
INSERT INTO product VALUES("189","Table Lamp - Glass Pear","Table Lamp - Glass Pear","0.00","1680","","72","1","1","0","28","","0000-00-00 00:00:00","1","1","189","","","inclusive");
INSERT INTO product VALUES("190","Screen / Room Divider","Screen / Room Divider","0.00","5560","","72","1","1","0","26","","0000-00-00 00:00:00","1","1","190","","","inclusive");
INSERT INTO product VALUES("191","Tea Tray","Tea Tray","0.00","480","","72","0","1","0","28","","0000-00-00 00:00:00","1","1","191","","","inclusive");
INSERT INTO product VALUES("192","Door Stop","Door Stop","0.00","450","","72","3","1","0","26","","0000-00-00 00:00:00","1","1","192","","","inclusive");
INSERT INTO product VALUES("193","Door Stop - bird","Door Stop - bird","0.00","80","","72","1","1","0","29","","0000-00-00 00:00:00","1","1","193","","","inclusive");
INSERT INTO product VALUES("194","OVAL BASKET WITH LID","OVAL BASKET WITH LID","0.00","120","","72","1","1","0","29","","0000-00-00 00:00:00","1","1","194","","","inclusive");
INSERT INTO product VALUES("195","Laundry - large","Laundry - large","0.00","300","","72","2","1","0","26","","0000-00-00 00:00:00","1","1","195","","","inclusive");
INSERT INTO product VALUES("196","A Hope Cards","A Hope Cards","0.00","50","","72","9","1","0","30","","0000-00-00 00:00:00","1","1","196","","","inclusive");
INSERT INTO product VALUES("197","VG Single Cards","VG Single Cards","0.00","25","","72","9","1","0","38","","0000-00-00 00:00:00","1","1","197","","","inclusive");
INSERT INTO product VALUES("198","Card Packs","Card Packs","0.00","100","","72","1","1","0","38","","0000-00-00 00:00:00","1","1","198","","","inclusive");
INSERT INTO product VALUES("199","VG Gift Tag Single","VG Gift Tag Single","0.00","35","","72","1","1","0","38","","0000-00-00 00:00:00","1","1","199","","","inclusive");
INSERT INTO product VALUES("200","Birthday Calendar","Birthday Calendar","0.00","110","","72","7","1","0","38","","0000-00-00 00:00:00","1","1","200","","","inclusive");
INSERT INTO product VALUES("201","Year Calendar","Year Calendar","0.00","","","72","0","1","0","38","","0000-00-00 00:00:00","1","1","201","","","inclusive");
INSERT INTO product VALUES("202","Tea Towel","Tea Towel","0.00","150","","72","0","1","0","38","","0000-00-00 00:00:00","1","1","202","","","inclusive");
INSERT INTO product VALUES("203","Magnetic shopping list","Magnetic shopping list","0.00","","","72","0","1","0","38","","0000-00-00 00:00:00","1","1","203","","","inclusive");
INSERT INTO product VALUES("204","Fridge Magnet","Fridge Magnet","0.00","40","","72","7","1","0","38","","0000-00-00 00:00:00","1","1","204","","","inclusive");
INSERT INTO product VALUES("205","Ceramic knob","Ceramic knob","0.00","50","","72","41","1","0","29","","0000-00-00 00:00:00","1","1","205","","","");
INSERT INTO product VALUES("206","Ceramic bottle tops","Ceramic bottle tops","0.00","","","72","0","1","0","29","","0000-00-00 00:00:00","1","1","206","","","");
INSERT INTO product VALUES("207","Ceramic knob - long","Ceramic knob - long","0.00","","","72","0","1","0","29","","0000-00-00 00:00:00","1","1","207","","","");
INSERT INTO product VALUES("208","Thin / Tassels","Thin / Tassels","0.00","150","","72","0","1","0","29","","0000-00-00 00:00:00","1","1","208","","","inclusive");
INSERT INTO product VALUES("209","Poncho","Poncho","0.00","220","","72","9","1","0","29","","0000-00-00 00:00:00","1","1","209","","","inclusive");
INSERT INTO product VALUES("210","Scarves","Scarves","0.00","","","72","0","1","0","29","","0000-00-00 00:00:00","1","1","210","","","inclusive");
INSERT INTO product VALUES("211","Beach Towel","Beach Towel","0.00","350","","72","15","1","0","29","","0000-00-00 00:00:00","1","1","211","","","inclusive");
INSERT INTO product VALUES("212","Hat","Hat","0.00","160","","72","9","1","0","29","","0000-00-00 00:00:00","1","1","212","","","inclusive");
INSERT INTO product VALUES("213","Shoulder bag","Shoulder bag","0.00","120","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","213","","","inclusive");
INSERT INTO product VALUES("214","Round bag","Round bag","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","214","","","inclusive");
INSERT INTO product VALUES("215","Table cloth - Hand Painted","Table cloth - Hand Painted","0.00","630","","72","3","1","0","24","","0000-00-00 00:00:00","1","1","215","","","inclusive");
INSERT INTO product VALUES("216","Tabel Cloth - Animals","Tabel Cloth - Animals","0.00","490","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","216","","","");
INSERT INTO product VALUES("217","Table cloth - large","Table cloth - large","0.00","250","","72","1","1","0","26","","0000-00-00 00:00:00","1","1","217","","","inclusive");
INSERT INTO product VALUES("218","Table runner","Table runner","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","218","","","inclusive");
INSERT INTO product VALUES("219","Pillow case - set","Pillow case - set","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","219","","","inclusive");
INSERT INTO product VALUES("220","Tea cosy","Tea cosy","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","220","","","inclusive");
INSERT INTO product VALUES("221","Christmas stocking","Christmas stocking","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","221","","","inclusive");
INSERT INTO product VALUES("222","Blue Olive - runner","Blue Olive - runner","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","222","","","inclusive");
INSERT INTO product VALUES("223","Food net-Small","Food net-Small","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","223","","","inclusive");
INSERT INTO product VALUES("224","Food net-Large","Food net-Large","0.00","220","","72","3","1","0","24","","0000-00-00 00:00:00","1","1","224","","","inclusive");
INSERT INTO product VALUES("225","Continental","Continental","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","225","","","inclusive");
INSERT INTO product VALUES("226","Square","Square","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","226","","","inclusive");
INSERT INTO product VALUES("227","Rectangle","Rectangle","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","227","","","inclusive");
INSERT INTO product VALUES("228","Inners","Inners","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","228","","","inclusive");
INSERT INTO product VALUES("229","Chitengi pieces","Chitengi pieces","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","229","","","inclusive");
INSERT INTO product VALUES("230","Candles","Candles","0.00","","","72","0","1","0","25","","0000-00-00 00:00:00","1","1","230","","","inclusive");
INSERT INTO product VALUES("231","Candles-Large","Candles-Large","0.00","220","","72","12","1","0","25","","0000-00-00 00:00:00","1","1","231","","","inclusive");
INSERT INTO product VALUES("232","Soap","Soap","0.00","60","","72","16","1","0","35","","0000-00-00 00:00:00","1","1","232","","","inclusive");
INSERT INTO product VALUES("233","Toothpick holder - animal shape","Toothpick holder - animal shape","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","233","","","inclusive");
INSERT INTO product VALUES("234","Soapstone -Tortise","Soapstone -Tortise","0.00","90","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","234","","","inclusive");
INSERT INTO product VALUES("235","Bunting","Bunting","0.00","","","72","0","1","0","24","","0000-00-00 00:00:00","1","1","235","","","inclusive");
INSERT INTO product VALUES("236","Wooden statue","Wooden statue","0.00","3","","72","1120","1","0","24","","0000-00-00 00:00:00","1","1","236","","","inclusive");
INSERT INTO product VALUES("237","Chitengi mirror","Chitengi mirror","0.00","350","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","237","","","inclusive");
INSERT INTO product VALUES("238","Wax wrap","Wax wrap","0.00","180","","72","9","1","0","36","","0000-00-00 00:00:00","1","1","238","","","inclusive");
INSERT INTO product VALUES("239","Bread bag","Bread bag","0.00","80","","72","10","1","0","36","","0000-00-00 00:00:00","1","1","239","","","inclusive");
INSERT INTO product VALUES("240","Vegetable bag","Vegetable bag","0.00","80","","72","3","1","0","36","","0000-00-00 00:00:00","1","1","240","","","inclusive");
INSERT INTO product VALUES("241","Fly Spinner","Fly Spinner","0.00","320","","72","1","1","0","24","","0000-00-00 00:00:00","1","1","241","","","inclusive");
INSERT INTO product VALUES("242","Corkscew set","Corkscew set","0.00","80","","72","3","1","0","29","","0000-00-00 00:00:00","1","1","242","","","inclusive");
INSERT INTO product VALUES("243","Straw set","Straw set","0.00","100","","72","1","1","0","29","","0000-00-00 00:00:00","1","1","243","","","inclusive");
INSERT INTO product VALUES("244","Wet wipes","Wet wipes","0.00","25","","72","10","1","0","29","","0000-00-00 00:00:00","1","1","244","","","inclusive");
INSERT INTO product VALUES("245","Foot File","Foot File","0.00","50","","72","1","1","0","29","","0000-00-00 00:00:00","1","1","245","","","inclusive");
INSERT INTO product VALUES("246","Gift bag-Small","Gift bag-Small","0.00","15","","72","3","1","0","24","","0000-00-00 00:00:00","1","1","246","","","inclusive");
INSERT INTO product VALUES("247","Gift bag-Large","Gift bag-Large","0.00","20","","72","13","1","0","24","","0000-00-00 00:00:00","1","1","247","","","inclusive");
INSERT INTO product VALUES("248","Ribbon","Ribbon","0.00","8","","72","9","1","0","24","","0000-00-00 00:00:00","1","1","248","","","inclusive");
INSERT INTO product VALUES("249","Strong lamp","Strong lamp","0.00","2800","","72","2","1","0","26","","0000-00-00 00:00:00","1","1","249","","","inclusive");
INSERT INTO product VALUES("250","Trunk lamp","Trunk lamp","0.00","3800","","72","2","1","0","26","","0000-00-00 00:00:00","1","1","250","","","inclusive");
INSERT INTO product VALUES("251","Le Pug Popcorn","Le Pug Popcorn","0.00","35","","73","2","1","0","24","","0000-00-00 00:00:00","1","1","251","","","inclusive");
INSERT INTO product VALUES("252","Biltong sticks","Biltong sticks","0.00","20","","73","12","1","0","39","","0000-00-00 00:00:00","1","1","252","","","inclusive");
INSERT INTO product VALUES("253","Biltong sliced","Biltong sliced","0.00","20","","73","20","1","0","39","","0000-00-00 00:00:00","1","1","253","","","inclusive");
INSERT INTO product VALUES("254","Chilli Bites","Chilli Bites","0.00","20","","73","0","1","0","39","","0000-00-00 00:00:00","1","1","254","","","inclusive");
INSERT INTO product VALUES("255","Bread sunflower","Bread sunflower","0.00","45","","73","2","1","0","24","","0000-00-00 00:00:00","1","1","255","","","free");
INSERT INTO product VALUES("256","Bread multigrain","Bread multigrain","0.00","45","","73","2","1","0","24","","0000-00-00 00:00:00","1","1","256","","","free");
INSERT INTO product VALUES("257","Bread nut","Bread nut","0.00","50","","73","1","1","0","24","","2020-02-06 17:01:48","1","1","257","","","free");
INSERT INTO product VALUES("258","Bread rye","Bread rye","0.00","45","","73","0","1","0","24","","0000-00-00 00:00:00","1","1","258","","","free");
INSERT INTO product VALUES("259","Bread zopf","Bread zopf","0.00","45","","73","0","1","0","24","","0000-00-00 00:00:00","1","1","259","","","free");
INSERT INTO product VALUES("260","Biscuits ","Biscuits ","0.00","30","","73","1","1","0","24","","0000-00-00 00:00:00","1","1","260","","","inclusive");
INSERT INTO product VALUES("261","Biscuits ","Biscuits ","0.00","30","","73","11","1","0","24","","0000-00-00 00:00:00","1","1","261","","","inclusive");
INSERT INTO product VALUES("262","Biscuits ","Biscuits ","0.00","30","","73","4","1","0","24","","0000-00-00 00:00:00","1","1","262","","","inclusive");
INSERT INTO product VALUES("263","Biscuits ","Biscuits ","0.00","30","","73","0","1","0","24","","0000-00-00 00:00:00","1","1","263","","","inclusive");
INSERT INTO product VALUES("264","Rusks","Rusks","0.00","55","","73","3","1","0","24","","0000-00-00 00:00:00","1","1","264","","","inclusive");
INSERT INTO product VALUES("265","Marmalade","Marmalade","0.00","","","73","0","1","0","24","","0000-00-00 00:00:00","1","1","265","","","inclusive");
INSERT INTO product VALUES("266","Marmalade","Marmalade","0.00","40","","73","1","1","0","24","","0000-00-00 00:00:00","1","1","266","","","inclusive");
INSERT INTO product VALUES("267","Crackers","Crackers","0.00","28","","73","20","1","0","33","","0000-00-00 00:00:00","1","1","267","","","inclusive");
INSERT INTO product VALUES("268","Microgreen Salad Mix-100G","Microgreen Salad Mix-100G","0.00","47","","73","2","1","0","28","","0000-00-00 00:00:00","1","1","268","","","free");
INSERT INTO product VALUES("269","Microgreen Sunflower-100G","Microgreen Sunflower-100G","0.00","40","","73","0","1","0","28","","0000-00-00 00:00:00","1","1","269","","","free");
INSERT INTO product VALUES("270","Microgreen Sunflower-50G","Microgreen Sunflower-50G","0.00","20","","73","3","1","0","28","","0000-00-00 00:00:00","1","1","270","","","free");
INSERT INTO product VALUES("271","Microgreen Jap radish-50G","Microgreen Jap radish-50G","0.00","25","","73","3","1","0","28","","0000-00-00 00:00:00","1","1","271","","","free");
INSERT INTO product VALUES("272","Microgreen Red Ram radish-50G","Microgreen Red Ram radish-50G","0.00","25","","73","2","1","0","28","","0000-00-00 00:00:00","1","1","272","","","free");
INSERT INTO product VALUES("273","Microgreen Pea shoot-50G","Microgreen Pea shoot-50G","0.00","25","","73","3","1","0","28","","0000-00-00 00:00:00","1","1","273","","","free");
INSERT INTO product VALUES("274","Biscotti","Biscotti","0.00","25","","73","4","1","0","45","","0000-00-00 00:00:00","1","1","274","","","inclusive");
INSERT INTO product VALUES("275","Basil Pesto Hummus","Basil Pesto Hummus","0.00","","","73","0","1","0","40","","0000-00-00 00:00:00","1","1","275","","","inclusive");
INSERT INTO product VALUES("276","Hummus","Hummus","0.00","70","","73","4","1","0","40","","0000-00-00 00:00:00","1","1","276","","","inclusive");
INSERT INTO product VALUES("277","Peanut Ginger Dressing","Peanut Ginger Dressing","0.00","75","","73","3","1","0","40","","0000-00-00 00:00:00","1","1","277","","","inclusive");
INSERT INTO product VALUES("278","Tzatziki","Tzatziki","0.00","75","","73","3","1","0","40","","0000-00-00 00:00:00","1","1","278","","","inclusive");
INSERT INTO product VALUES("279","Sundried Tomato Pesto","Sundried Tomato Pesto","0.00","","","73","0","1","0","40","","0000-00-00 00:00:00","1","1","279","","","inclusive");
INSERT INTO product VALUES("280","Sweet Potato Curry Hummus","Sweet Potato Curry Hummus","0.00","","","73","0","1","0","40","","0000-00-00 00:00:00","1","1","280","","","inclusive");
INSERT INTO product VALUES("281","Ranch Dressing","Ranch Dressing","0.00","75","","73","6","1","0","40","","0000-00-00 00:00:00","1","1","281","","","inclusive");
INSERT INTO product VALUES("282","Herbed Labneh","Herbed Labneh","0.00","","","73","0","1","0","40","","0000-00-00 00:00:00","1","1","282","","","inclusive");
INSERT INTO product VALUES("283","Chicken Liver Pate","Chicken Liver Pate","0.00","75","","73","2","1","0","40","","0000-00-00 00:00:00","1","1","283","","","inclusive");
INSERT INTO product VALUES("284","Babaghanoush","Babaghanoush","0.00","","","73","0","1","0","40","","0000-00-00 00:00:00","1","1","284","","","inclusive");
INSERT INTO product VALUES("285","Olive & Feta Hummus","Olive & Feta Hummus","0.00","","","73","0","1","0","40","","0000-00-00 00:00:00","1","1","285","","","inclusive");
INSERT INTO product VALUES("286","Roasted Pepper Hummus","Roasted Pepper Hummus","0.00","70","","73","3","1","0","40","","0000-00-00 00:00:00","1","1","286","","","inclusive");
INSERT INTO product VALUES("287","Spinach & Basil Pesto","Spinach & Basil Pesto","0.00","77","","73","4","1","0","40","","0000-00-00 00:00:00","1","1","287","","","inclusive");
INSERT INTO product VALUES("288","Cashew nuts","Cashew nuts","0.00","25","","73","37","1","0","29","","0000-00-00 00:00:00","1","1","288","","","inclusive");
INSERT INTO product VALUES("289","Honey","Honey","0.00","67","","73","5","1","0","35","","0000-00-00 00:00:00","1","1","289","","","inclusive");
INSERT INTO product VALUES("290","Tomota Chutney","Tomota Chutney","0.00","35","","73","9","1","0","24","","0000-00-00 00:00:00","1","1","290","","","inclusive");
INSERT INTO product VALUES("291","Cucumber Pickle","Cucumber Pickle","0.00","50","","73","6","1","0","24","","0000-00-00 00:00:00","1","1","291","","","inclusive");
INSERT INTO product VALUES("292","Pork Scratchings","Pork Scratchings","0.00","","","73","0","1","0","24","","0000-00-00 00:00:00","1","1","292","","","inclusive");
INSERT INTO product VALUES("293","Hot Chilli Sauce","Hot Chilli Sauce","0.00","20","","73","3","1","0","24","","0000-00-00 00:00:00","1","1","293","","","inclusive");
INSERT INTO product VALUES("294","Mint Sauce","Mint Sauce","0.00","30","","73","6","1","0","24","","0000-00-00 00:00:00","1","1","294","","","inclusive");
INSERT INTO product VALUES("295","Mayonnaise","Mayonnaise","0.00","","","73","0","1","0","24","","0000-00-00 00:00:00","1","1","295","","","inclusive");
INSERT INTO product VALUES("296","Hickory Ham","Hickory Ham","0.00","","","73","0","1","0","34","","0000-00-00 00:00:00","1","1","296","","","inclusive");
INSERT INTO product VALUES("297","Pressed Tongue","Pressed Tongue","0.00","","","73","0","1","0","34","","0000-00-00 00:00:00","1","1","297","","","inclusive");
INSERT INTO product VALUES("298","Smoked Beef","Smoked Beef","0.00","","","73","0","1","0","34","","0000-00-00 00:00:00","1","1","298","","","inclusive");
INSERT INTO product VALUES("299","Cervelat Salami","Cervelat Salami","0.00","","","73","0","1","0","34","","0000-00-00 00:00:00","1","1","299","","","inclusive");
INSERT INTO product VALUES("300","Dry Wors","Dry Wors","0.00","","","73","0","1","0","34","","0000-00-00 00:00:00","1","1","300","","","inclusive");
INSERT INTO product VALUES("301","Chilli Bite","Chilli Bite","0.00","","","73","0","1","0","34","","0000-00-00 00:00:00","1","1","301","","","inclusive");
INSERT INTO product VALUES("302","Biltong","Biltong","0.00","","","73","0","1","0","34","","0000-00-00 00:00:00","1","1","302","","","inclusive");
INSERT INTO product VALUES("303","Cooked Ham","Cooked Ham","0.00","","","73","0","1","0","34","","0000-00-00 00:00:00","1","1","303","","","inclusive");
INSERT INTO product VALUES("304","Farmers Ham","Farmers Ham","0.00","","","73","0","1","0","34","","0000-00-00 00:00:00","1","1","304","","","inclusive");
INSERT INTO product VALUES("305","Polony","Polony","0.00","","","73","0","1","0","34","","0000-00-00 00:00:00","1","1","305","","","inclusive");
INSERT INTO product VALUES("306","Mushrooms-400g","Mushrooms-400g","0.00","","","73","0","1","0","24","","0000-00-00 00:00:00","1","1","306","","","");
INSERT INTO product VALUES("307","Mushrooms-300g","Mushrooms-300g","0.00","","","73","0","1","0","24","","0000-00-00 00:00:00","1","1","307","","","");
INSERT INTO product VALUES("308","Iced cookie","Iced cookie","0.00","","","73","0","1","0","37","","0000-00-00 00:00:00","1","1","308","","","inclusive");
INSERT INTO product VALUES("309","Onion marmalade","Onion marmalade","0.00","","","73","0","1","0","36","","0000-00-00 00:00:00","1","1","309","","","inclusive");
INSERT INTO product VALUES("310","Strawberry jam","Strawberry jam","0.00","65","","73","4","1","0","36","","0000-00-00 00:00:00","1","1","310","","","inclusive");
INSERT INTO product VALUES("311","Tomato chilli","Tomato chilli","0.00","","","73","0","1","0","36","","0000-00-00 00:00:00","1","1","311","","","inclusive");
INSERT INTO product VALUES("312","Zucchini chutney","Zucchini chutney","0.00","65","","73","1","1","0","36","","2020-02-06 17:01:48","1","1","312","","","inclusive");
INSERT INTO product VALUES("313","Dried tomatoes","Dried tomatoes","0.00","75","","73","2","1","0","36","","0000-00-00 00:00:00","1","1","313","","","inclusive");
INSERT INTO product VALUES("314","Tomato tapenade","Tomato tapenade","0.00","75","","73","4","1","0","36","","0000-00-00 00:00:00","1","1","314","","","inclusive");
INSERT INTO product VALUES("315","Olive tapenade","Olive tapenade","0.00","93","","73","4","1","0","36","","0000-00-00 00:00:00","1","1","315","","","inclusive");
INSERT INTO product VALUES("316","Basil pesto","Basil pesto","0.00","88","","73","3","1","0","36","","0000-00-00 00:00:00","1","1","316","","","inclusive");
INSERT INTO product VALUES("317","Salted caramel","Salted caramel","0.00","80","","73","3","1","0","36","","0000-00-00 00:00:00","1","1","317","","","inclusive");
INSERT INTO product VALUES("318","Passion fruit & lime curd","Passion fruit & lime curd","0.00","","","73","0","1","0","36","","0000-00-00 00:00:00","1","1","318","","","inclusive");
INSERT INTO product VALUES("319","Ginger biscuits","Ginger biscuits","0.00","52","","73","5","1","0","36","","0000-00-00 00:00:00","1","1","319","","","inclusive");
INSERT INTO product VALUES("320","Rusks buttermilk","Rusks buttermilk","0.00","72","","73","4","1","0","36","","0000-00-00 00:00:00","1","1","320","","","inclusive");
INSERT INTO product VALUES("321","Rusks muesli","Rusks muesli","0.00","77","","73","6","1","0","36","","0000-00-00 00:00:00","1","1","321","","","inclusive");
INSERT INTO product VALUES("322","Pre-Cut Noodle Beetroot 350gm","Pre-Cut Noodle Beetroot 350gm","0.00","15","","73","2","1","0","55","","0000-00-00 00:00:00","1","1","322","","","inclusive");
INSERT INTO product VALUES("323","Brocolli Florette 350g","Brocolli Florette 350g","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","323","","","inclusive");
INSERT INTO product VALUES("324","Pre-Cut Broc/Cauli Florette 350g","Pre-Cut Broc/Cauli Florette 350g","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","324","","","inclusive");
INSERT INTO product VALUES("325","Pre-Cut Butternut Cubes/Chunks 500g","Pre-Cut Butternut Cubes/Chunks 500g","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","325","","","inclusive");
INSERT INTO product VALUES("326","Pre-Cut Butternut Noodle 300g","Pre-Cut Butternut Noodle 300g","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","326","","","inclusive");
INSERT INTO product VALUES("327","Pre-Cut But-nut Bases","Pre-Cut But-nut Bases","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","327","","","inclusive");
INSERT INTO product VALUES("328","Pre-Cut Cabbage Cut 300gm","Pre-Cut Cabbage Cut 300gm","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","328","","","inclusive");
INSERT INTO product VALUES("329","Pre-Cut Carrot Cut Pp","Pre-Cut Carrot Cut Pp","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","329","","","inclusive");
INSERT INTO product VALUES("330","Pre-Cut  Carrot balls 300gm","Pre-Cut  Carrot balls 300gm","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","330","","","inclusive");
INSERT INTO product VALUES("331","Pre-Cut Carrot peeled 300g","Pre-Cut Carrot peeled 300g","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","331","","","inclusive");
INSERT INTO product VALUES("332","Cauliflower Florette 350g","Cauliflower Florette 350g","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","332","","","inclusive");
INSERT INTO product VALUES("333","Pre-Cut Coleslaw 350g","Pre-Cut Coleslaw 350g","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","333","","","inclusive");
INSERT INTO product VALUES("334","Prep - Lettuce Mixed Leaf bag 150g","Prep - Lettuce Mixed Leaf bag 150g","0.00","20","","73","6","1","0","55","","0000-00-00 00:00:00","1","1","334","","","free");
INSERT INTO product VALUES("335","Pre-Cut Noodle Marrow 350g","Pre-Cut Noodle Marrow 350g","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","335","","","inclusive");
INSERT INTO product VALUES("336","Prep Fruit Salad Cup 200g","Prep Fruit Salad Cup 200g","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","336","","","inclusive");
INSERT INTO product VALUES("337","Pre-Cut Potato Baby Peeled 500gm","Pre-Cut Potato Baby Peeled 500gm","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","337","","","inclusive");
INSERT INTO product VALUES("338","Pre-Cut Roast Veg Seasonal 500gm","Pre-Cut Roast Veg Seasonal 500gm","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","338","","","inclusive");
INSERT INTO product VALUES("339","Bread and Butter Pickles","Bread and Butter Pickles","0.00","55","","73","2","1","0","24","","0000-00-00 00:00:00","1","1","339","","","inclusive");
INSERT INTO product VALUES("340","Chadaroo  Chunky","Chadaroo  Chunky","0.00","30","","73","5","1","0","31","","0000-00-00 00:00:00","1","1","340","","","inclusive");
INSERT INTO product VALUES("341","Beetroot Pickles","Beetroot Pickles","0.00","30","","73","5","1","0","31","","0000-00-00 00:00:00","1","1","341","","","inclusive");
INSERT INTO product VALUES("342","Pickled Onions Plain","Pickled Onions Plain","0.00","33","","73","5","1","0","31","","0000-00-00 00:00:00","1","1","342","","","inclusive");
INSERT INTO product VALUES("343","Zamor Cucumber Pickle","Zamor Cucumber Pickle","0.00","30","","73","6","1","0","31","","0000-00-00 00:00:00","1","1","343","","","inclusive");
INSERT INTO product VALUES("344","Crunchy Corriander","Crunchy Corriander","0.00","40","","73","6","1","0","31","","0000-00-00 00:00:00","1","1","344","","","inclusive");
INSERT INTO product VALUES("345","Delicous Dill ","Delicous Dill ","0.00","40","","73","6","1","0","31","","0000-00-00 00:00:00","1","1","345","","","inclusive");
INSERT INTO product VALUES("346","Punchy Parsley","Punchy Parsley","0.00","40","","73","6","1","0","31","","0000-00-00 00:00:00","1","1","346","","","inclusive");
INSERT INTO product VALUES("347","Funky Fennel ","Funky Fennel ","0.00","40","","73","6","1","0","31","","0000-00-00 00:00:00","1","1","347","","","inclusive");
INSERT INTO product VALUES("348","Basil Leaves","Basil Leaves","0.00","20","","73","3","1","0","31","","0000-00-00 00:00:00","1","1","348","","","inclusive");
INSERT INTO product VALUES("349","Dried Chilli","Dried Chilli","0.00","20","","73","3","1","0","31","","0000-00-00 00:00:00","1","1","349","","","inclusive");
INSERT INTO product VALUES("350","Dried Mint","Dried Mint","0.00","20","","73","3","1","0","31","","0000-00-00 00:00:00","1","1","350","","","inclusive");
INSERT INTO product VALUES("351","Thyme Leaves","Thyme Leaves","0.00","20","","73","3","1","0","31","","0000-00-00 00:00:00","1","1","351","","","inclusive");
INSERT INTO product VALUES("352","Curry Leaves","Curry Leaves","0.00","30","","73","3","1","0","31","","0000-00-00 00:00:00","1","1","352","","","inclusive");
INSERT INTO product VALUES("353","SALAMI 100G ","SALAMI 100G ","0.00","30","","73","1","1","0","37","","0000-00-00 00:00:00","1","1","353","","","inclusive");
INSERT INTO product VALUES("354","PASTRAMI 200G","PASTRAMI 200G","0.00","30","","73","5","1","0","37","","0000-00-00 00:00:00","1","1","354","","","inclusive");
INSERT INTO product VALUES("355","COOKED HAM 200G","COOKED HAM 200G","0.00","30","","73","3","1","0","37","","0000-00-00 00:00:00","1","1","355","","","inclusive");
INSERT INTO product VALUES("356","PRESSED BEED 200G","PRESSED BEED 200G","0.00","30","","73","5","1","0","37","","0000-00-00 00:00:00","1","1","356","","","inclusive");
INSERT INTO product VALUES("357","BACON POLONY 250G","BACON POLONY 250G","0.00","30","","73","2","1","0","37","","0000-00-00 00:00:00","1","1","357","","","inclusive");
INSERT INTO product VALUES("358","COOKED SALAMI 250G","COOKED SALAMI 250G","0.00","30","","73","3","1","0","37","","0000-00-00 00:00:00","1","1","358","","","inclusive");
INSERT INTO product VALUES("359","SALAMI PEPERONI 250G","SALAMI PEPERONI 250G","0.00","30","","73","1","1","0","37","","0000-00-00 00:00:00","1","1","359","","","inclusive");
INSERT INTO product VALUES("360","POMEGRANITE PUNNET","POMEGRANITE PUNNET","0.00","35","","73","6","1","0","24","","0000-00-00 00:00:00","1","1","360","","","free");
INSERT INTO product VALUES("361","POMEGRANITE FRUIT","POMEGRANITE FRUIT","0.00","25","","73","5","1","0","24","","0000-00-00 00:00:00","1","1","361","","","free");
INSERT INTO product VALUES("362","TOMATO SALSA","TOMATO SALSA","0.00","40","","73","1","1","0","24","","0000-00-00 00:00:00","1","1","362","","","inclusive");
INSERT INTO product VALUES("363","Hunters Biltong-200g","Hunters Biltong-200g","0.00","70","","74","5","1","0","60","","0000-00-00 00:00:00","1","1","363","","","inclusive");
INSERT INTO product VALUES("364","Rangers Biltong-200g","Rangers Biltong-200g","0.00","70","","74","2","1","0","60","","0000-00-00 00:00:00","1","1","364","","","inclusive");
INSERT INTO product VALUES("365","Dry Fire Chili Biltong - 200g","Dry Fire Chili Biltong - 200g","0.00","70","","74","1","1","0","60","","0000-00-00 00:00:00","1","1","365","","","inclusive");
INSERT INTO product VALUES("366","AJS GARLIC","AJS GARLIC","0.00","55","","73","1","1","0","55","","0000-00-00 00:00:00","1","1","366","","","inclusive");
INSERT INTO product VALUES("367","AJS GARLIC AND GINGER","AJS GARLIC AND GINGER","0.00","55","","73","3","1","0","55","","0000-00-00 00:00:00","1","1","367","","","inclusive");
INSERT INTO product VALUES("368","TOMATO VALUE PACK","TOMATO VALUE PACK","0.00","28","","73","2","1","0","55","","0000-00-00 00:00:00","1","1","368","","","free");
INSERT INTO product VALUES("369","BABY CORN","BABY CORN","0.00","20","","73","2","1","0","55","","0000-00-00 00:00:00","1","1","369","","","free");
INSERT INTO product VALUES("370","BABY CORN","BABY CORN","0.00","20","","73","2","1","0","28","","0000-00-00 00:00:00","1","1","370","","","free");
INSERT INTO product VALUES("371","CARROT BALLS","CARROT BALLS","0.00","15","","73","1","1","0","55","","0000-00-00 00:00:00","1","1","371","","","inclusive");
INSERT INTO product VALUES("372","ROAST VEG SEASONAL","ROAST VEG SEASONAL","0.00","30","","73","1","1","0","55","","0000-00-00 00:00:00","1","1","372","","","inclusive");
INSERT INTO product VALUES("373","PEPPER PACK","PEPPER PACK","0.00","30","","73","2","1","0","24","","0000-00-00 00:00:00","1","1","373","","","free");
INSERT INTO product VALUES("374","GREEN BEANS","GREEN BEANS","0.00","15","","73","1","1","0","28","","0000-00-00 00:00:00","1","1","374","","","free");
INSERT INTO product VALUES("375","PATTY PANS","PATTY PANS","0.00","15","","73","3","1","0","28","","0000-00-00 00:00:00","1","1","375","","","free");
INSERT INTO product VALUES("376","Apples Mixed 6s","Apples Mixed 6s","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","376","","","free");
INSERT INTO product VALUES("377","Apple Golden 6's","Apple Golden 6's","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","377","","","free");
INSERT INTO product VALUES("378","Apple Granny 6's","Apple Granny 6's","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","378","","","free");
INSERT INTO product VALUES("379","Apple Pink 6's","Apple Pink 6's","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","379","","","free");
INSERT INTO product VALUES("380","Banana Kg","Banana Kg","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","380","","","free");
INSERT INTO product VALUES("381","Pack - Banana Pp","Pack - Banana Pp","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","381","","","free");
INSERT INTO product VALUES("382","Raspberry fresh pp","Raspberry fresh pp","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","382","","","free");
INSERT INTO product VALUES("383","Blueberry Fresh 500gm pp","Blueberry Fresh 500gm pp","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","383","","","free");
INSERT INTO product VALUES("384","Asparagus PP","Asparagus PP","0.00","78","","73","1","1","0","55","","0000-00-00 00:00:00","1","1","384","","","free");
INSERT INTO product VALUES("385","Blueberry Fresh 125gm Clam shell","Blueberry Fresh 125gm Clam shell","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","385","","","free");
INSERT INTO product VALUES("386","Cherry  pp","Cherry  pp","0.00","27","","73","7","1","0","55","","0000-00-00 00:00:00","1","1","386","","","free");
INSERT INTO product VALUES("387","Coconut Head","Coconut Head","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","387","","","free");
INSERT INTO product VALUES("388","Pears Green/Red 1.5 econo pkt","Pears Green/Red 1.5 econo pkt","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","388","","","free");
INSERT INTO product VALUES("389","Figs Pp","Figs Pp","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","389","","","free");
INSERT INTO product VALUES("390","Grape Green/white 500Gm Pp","Grape Green/white 500Gm Pp","0.00","50","","73","2","1","0","55","","0000-00-00 00:00:00","1","1","390","","","free");
INSERT INTO product VALUES("391","Grapes Red 500Gm Pp","Grapes Red 500Gm Pp","0.00","50","","73","3","1","0","55","","0000-00-00 00:00:00","1","1","391","","","free");
INSERT INTO product VALUES("392","Sock Grapefruit","Sock Grapefruit","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","392","","","free");
INSERT INTO product VALUES("393","Kiwi Pp Small- 2 piece","Kiwi Pp Small- 2 piece","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","393","","","free");
INSERT INTO product VALUES("394","Sock -  Lemon","Sock -  Lemon","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","394","","","free");
INSERT INTO product VALUES("395","Pack - Lemon 1kg","Pack - Lemon 1kg","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","395","","","free");
INSERT INTO product VALUES("396","Sock  - Lime Sock","Sock  - Lime Sock","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","396","","","free");
INSERT INTO product VALUES("397","Lychees 4-500Gm Pp","Lychees 4-500Gm Pp","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","397","","","free");
INSERT INTO product VALUES("398","Lychees 250gm Pp","Lychees 250gm Pp","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","398","","","free");
INSERT INTO product VALUES("399","Mango  Head","Mango  Head","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","399","","","free");
INSERT INTO product VALUES("400","Naartjie Imp Pp","Naartjie Imp Pp","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","400","","","free");
INSERT INTO product VALUES("401","Nectarine Pp","Nectarine Pp","0.00","60","","73","1","1","0","55","","0000-00-00 00:00:00","1","1","401","","","free");
INSERT INTO product VALUES("402","Pack - Citrus Orange 6's","Pack - Citrus Orange 6's","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","402","","","free");
INSERT INTO product VALUES("403","Papino pp","Papino pp","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","403","","","free");
INSERT INTO product VALUES("404","Passion Fruit  Pp","Passion Fruit  Pp","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","404","","","inclusive");
INSERT INTO product VALUES("405","Peach Pp","Peach Pp","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","405","","","free");
INSERT INTO product VALUES("406","Pears Green 1kg","Pears Green 1kg","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","406","","","free");
INSERT INTO product VALUES("407","Pears Red  1kg Pp","Pears Red  1kg Pp","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","407","","","free");
INSERT INTO product VALUES("408","Pineapple Head","Pineapple Head","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","408","","","free");
INSERT INTO product VALUES("409","Plums Pp","Plums Pp","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","409","","","free");
INSERT INTO product VALUES("410","Pommegranite HEAD","Pommegranite HEAD","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","410","","","free");
INSERT INTO product VALUES("411","Spanspek  Head","Spanspek  Head","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","411","","","inclusive");
INSERT INTO product VALUES("412","Strawberry 250Gm Pack","Strawberry 250Gm Pack","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","412","","","free");
INSERT INTO product VALUES("413","Sweetmelon Head","Sweetmelon Head","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","413","","","inclusive");
INSERT INTO product VALUES("414","Watermelon Head","Watermelon Head","0.00","45","","73","2","1","0","55","","0000-00-00 00:00:00","1","1","414","","","free");
INSERT INTO product VALUES("415","Watermelon halves","Watermelon halves","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","415","","","inclusive");
INSERT INTO product VALUES("416","Watermelon Quarters","Watermelon Quarters","0.00","","","73","0","1","0","55","","0000-00-00 00:00:00","1","1","416","","","inclusive");
INSERT INTO product VALUES("417","English Cucumber","English Cucumber","0.00","20","","73","8","1","0","55","","0000-00-00 00:00:00","1","1","417","","","free");
INSERT INTO product VALUES("418","Baby Beetroot PP","Baby Beetroot PP","0.00","22","","73","2","1","0","55","","0000-00-00 00:00:00","1","1","418","","","free");
INSERT INTO product VALUES("419","SWEETCORN 4 PACK","SWEETCORN 4 PACK","0.00","30","","73","2","1","0","55","","0000-00-00 00:00:00","1","1","419","","","free");
INSERT INTO product VALUES("420","Lemon Pound cake","Lemon Pound cake","0.00","160","","73","2","1","0","24","","0000-00-00 00:00:00","1","1","420","","","inclusive");
INSERT INTO product VALUES("421","Beef lasagne","Beef lasagne","0.00","","","73","0","1","0","45","","0000-00-00 00:00:00","1","1","421","","","inclusive");
INSERT INTO product VALUES("422","Spinach Cannelloni","Spinach Cannelloni","0.00","","","73","0","1","0","45","","0000-00-00 00:00:00","1","1","422","","","inclusive");
INSERT INTO product VALUES("423","Chicken Cannelloni","Chicken Cannelloni","0.00","190","","73","1","1","0","45","","0000-00-00 00:00:00","1","1","423","","","inclusive");
INSERT INTO product VALUES("424","Beef Cannelloni","Beef Cannelloni","0.00","114","","73","1","1","0","45","","2020-02-06 17:01:48","1","1","424","","","inclusive");
INSERT INTO product VALUES("425","Bolognaise","Bolognaise","0.00","","","73","0","1","0","45","","0000-00-00 00:00:00","1","1","425","","","inclusive");
INSERT INTO product VALUES("426","Mozarella Pockets","Mozarella Pockets","0.00","121","","73","2","1","0","45","","0000-00-00 00:00:00","1","1","426","","","inclusive");
INSERT INTO product VALUES("427","Parmigiana","Parmigiana","0.00","270","","73","2","1","0","45","","0000-00-00 00:00:00","1","1","427","","","inclusive");
INSERT INTO product VALUES("428","Cinammon rolls","Cinammon rolls","0.00","60","","73","3","1","0","45","","0000-00-00 00:00:00","1","1","428","","","inclusive");
INSERT INTO product VALUES("429","Koeksisters","Koeksisters","0.00","70","","75","16","1","0","24","","0000-00-00 00:00:00","1","1","429","","","inclusive");
INSERT INTO product VALUES("430","Halfmoons","Halfmoons","0.00","50","","75","2","1","0","38","","0000-00-00 00:00:00","1","1","430","","","inclusive");
INSERT INTO product VALUES("431","Spring Rolls","Spring Rolls","0.00","","","75","0","1","0","38","","0000-00-00 00:00:00","1","1","431","","","inclusive");
INSERT INTO product VALUES("432","Samoosas","Samoosas","0.00","50","","75","8","1","0","38","","0000-00-00 00:00:00","1","1","432","","","inclusive");
INSERT INTO product VALUES("433","Crayfish tails","Crayfish tails","0.00","100","","75","17","1","0","30","","0000-00-00 00:00:00","1","1","433","","","free");
INSERT INTO product VALUES("434","Mince Pies","Mince Pies","0.00","70","","75","10","1","0","65","","0000-00-00 00:00:00","1","1","434","","","inclusive");
INSERT INTO product VALUES("435","Quiche","Quiche","0.00","","","75","0","1","0","65","","0000-00-00 00:00:00","1","1","435","","","");
INSERT INTO product VALUES("436","FROZEN FRIES 2.5KG","FROZEN FRIES 2.5KG","0.00","95","","75","3","1","0","55","","0000-00-00 00:00:00","1","1","436","","","inclusive");
INSERT INTO product VALUES("437","GARDEN PEAS","GARDEN PEAS","0.00","80","","75","2","1","0","55","","0000-00-00 00:00:00","1","1","437","","","inclusive");
INSERT INTO product VALUES("438","RASSBERRIES","RASSBERRIES","0.00","120","","75","2","1","0","55","","0000-00-00 00:00:00","1","1","438","","","inclusive");
INSERT INTO product VALUES("439","FROZEN STRAWBERREIS","FROZEN STRAWBERREIS","0.00","90","","75","2","1","0","55","","0000-00-00 00:00:00","1","1","439","","","inclusive");
INSERT INTO product VALUES("440","FROZEN BLUEBERRIES 500G","FROZEN BLUEBERRIES 500G","0.00","104","","75","3","1","0","55","","0000-00-00 00:00:00","1","1","440","","","inclusive");
INSERT INTO product VALUES("441","BUTTER SALTED","BUTTER SALTED","0.00","50","","75","2","1","0","26","","0000-00-00 00:00:00","1","1","441","","","inclusive");
INSERT INTO product VALUES("442","BUTTER UNSALTED","BUTTER UNSALTED","0.00","50","","75","3","1","0","26","","0000-00-00 00:00:00","1","1","442","","","inclusive");
INSERT INTO product VALUES("443","Quiche-LARGE","Quiche-LARGE","0.00","85","","75","4","1","0","24","","0000-00-00 00:00:00","1","1","443","","","inclusive");
INSERT INTO product VALUES("444","Quiche-Small","Quiche-Small","0.00","25","","75","16","1","0","24","","0000-00-00 00:00:00","1","1","444","","","inclusive");
INSERT INTO product VALUES("445","PIES","PIES","0.00","25","","75","10","1","0","31","","0000-00-00 00:00:00","1","1","445","","","inclusive");
INSERT INTO product VALUES("446","PIES","PIES","0.00","25","","75","2","1","0","24","","0000-00-00 00:00:00","1","1","446","","","inclusive");
INSERT INTO product VALUES("447","SOUP","SOUP","0.00","45","","75","2","1","0","31","","0000-00-00 00:00:00","1","1","447","","","inclusive");
INSERT INTO product VALUES("448","CHICKEN CURRY","CHICKEN CURRY","0.00","45","","75","1","1","0","31","","0000-00-00 00:00:00","1","1","448","","","inclusive");
INSERT INTO product VALUES("449","BEEF CURRY","BEEF CURRY","0.00","45","","75","1","1","0","31","","0000-00-00 00:00:00","1","1","449","","","inclusive");
INSERT INTO product VALUES("450","GINGER BREAD","GINGER BREAD","0.00","70","","75","2","1","0","24","","0000-00-00 00:00:00","1","1","450","","","inclusive");
INSERT INTO product VALUES("451","BANANA BREAD","BANANA BREAD","0.00","65","","75","0","1","0","24","","0000-00-00 00:00:00","1","1","451","","","inclusive");
INSERT INTO product VALUES("452","KIDS SPAG BOWL","KIDS SPAG BOWL","0.00","65","","75","6","1","0","69","","0000-00-00 00:00:00","1","1","452","","","inclusive");
INSERT INTO product VALUES("453","KIDS MAC N CHEESE","KIDS MAC N CHEESE","0.00","65","","75","5","1","0","69","","0000-00-00 00:00:00","1","1","453","","","inclusive");
INSERT INTO product VALUES("454","KIDS SAVOURY BEEF MINCE AND MASH","KIDS SAVOURY BEEF MINCE AND MASH","0.00","65","","75","3","1","0","69","","0000-00-00 00:00:00","1","1","454","","","inclusive");
INSERT INTO product VALUES("455","SAVORY MINCE AND MASH","SAVORY MINCE AND MASH","0.00","95","","75","3","1","0","69","","0000-00-00 00:00:00","1","1","455","","","inclusive");
INSERT INTO product VALUES("456","SLOW COOKED BEEF STEW","SLOW COOKED BEEF STEW","0.00","120","","75","3","1","0","69","","0000-00-00 00:00:00","1","1","456","","","inclusive");
INSERT INTO product VALUES("457","Wrap","Wrap","0.00","35","","76","0","1","0","32","","0000-00-00 00:00:00","1","1","457","","","inclusive");
INSERT INTO product VALUES("458","Pie","Pie","0.00","25","","76","0","1","0","32","","0000-00-00 00:00:00","1","1","458","","","inclusive");
INSERT INTO product VALUES("459","Pie","Pie","0.00","","","76","0","1","0","32","","0000-00-00 00:00:00","1","1","459","","","inclusive");
INSERT INTO product VALUES("460","Sandwich","Sandwich","0.00","35","","76","0","1","0","32","","0000-00-00 00:00:00","1","1","460","","","inclusive");
INSERT INTO product VALUES("461","Hot Meal","Hot Meal","0.00","45","","76","0","1","0","32","","0000-00-00 00:00:00","1","1","461","","","inclusive");
INSERT INTO product VALUES("462","Donut","Donut","0.00","15","","76","0","1","0","32","","0000-00-00 00:00:00","1","1","462","","","inclusive");
INSERT INTO product VALUES("463","Cake slice","Cake slice","0.00","20","","76","0","1","0","32","","0000-00-00 00:00:00","1","1","463","","","inclusive");
INSERT INTO product VALUES("464","Cinammon roll","Cinammon roll","0.00","20","","76","0","1","0","32","","0000-00-00 00:00:00","1","1","464","","","inclusive");
INSERT INTO product VALUES("465","Hummus","Hummus","0.00","","","76","0","1","0","32","","0000-00-00 00:00:00","1","1","465","","","inclusive");
INSERT INTO product VALUES("466","Lunch Box","Lunch Box","0.00","","","76","0","1","0","32","","0000-00-00 00:00:00","1","1","466","","","inclusive");
INSERT INTO product VALUES("467","Platter","Platter","0.00","","","76","0","1","0","32","","0000-00-00 00:00:00","1","1","467","","","inclusive");
INSERT INTO product VALUES("468","Wrap","Wrap","0.00","","","76","0","1","0","24","","0000-00-00 00:00:00","1","1","468","","","inclusive");
INSERT INTO product VALUES("469","Baguette","Baguette","0.00","","","76","0","1","0","24","","0000-00-00 00:00:00","1","1","469","","","inclusive");
INSERT INTO product VALUES("470","Eggs","Eggs","0.00","10","","76","3","1","0","37","","0000-00-00 00:00:00","1","1","470","","","free");
INSERT INTO product VALUES("471","AVO-PP","AVO-PP","0.00","20","","73","1","1","0","55","","0000-00-00 00:00:00","1","1","471","","","free");
INSERT INTO product VALUES("472","BUTTERNUT","BUTTERNUT","0.00","10","","73","1","1","0","55","","0000-00-00 00:00:00","1","1","472","","","");
INSERT INTO product VALUES("473","BUTTERNUT","BUTTERNUT","0.00","20","","73","3","1","0","55","","0000-00-00 00:00:00","1","1","473","","","free");
INSERT INTO product VALUES("474","RED ONION-PP","RED ONION-PP","0.00","15","","73","3","1","0","55","","0000-00-00 00:00:00","1","1","474","","","");
INSERT INTO product VALUES("475","GARLIC - PP","GARLIC - PP","0.00","30","","73","3","1","0","55","","0000-00-00 00:00:00","1","1","475","","","");
INSERT INTO product VALUES("476","ORANGES-PP","ORANGES-PP","0.00","60","","73","2","1","0","55","","0000-00-00 00:00:00","1","1","476","","","");
INSERT INTO product VALUES("477","LEMON SOCK ","LEMON SOCK ","0.00","30","","73","4","1","0","55","","0000-00-00 00:00:00","1","1","477","","","");
INSERT INTO product VALUES("478","MINT","MINT","0.00","10","","73","3","1","0","26","","0000-00-00 00:00:00","1","1","478","","","");
INSERT INTO product VALUES("479","CHIVES","CHIVES","0.00","10","","73","2","1","0","26","","0000-00-00 00:00:00","1","1","479","","","");
INSERT INTO product VALUES("480","DILL","DILL","0.00","10","","73","2","1","0","26","","0000-00-00 00:00:00","1","1","480","","","");
INSERT INTO product VALUES("481","SORREL","SORREL","0.00","10","","73","2","1","0","26","","0000-00-00 00:00:00","1","1","481","","","");
INSERT INTO product VALUES("482","ROBOT PEPPER","ROBOT PEPPER","0.00","30","","73","6","1","0","24","","0000-00-00 00:00:00","1","1","482","","","free");
INSERT INTO product VALUES("483","RICOTTA ","RICOTTA ","0.00","52","","73","4","1","0","26","","0000-00-00 00:00:00","1","1","483","","","");
INSERT INTO product VALUES("484","DOUBLE THICK YOGHURT","DOUBLE THICK YOGHURT","0.00","20","","73","1","1","0","26","","0000-00-00 00:00:00","1","1","484","","","");
INSERT INTO product VALUES("485","GREEK YOGHURT","GREEK YOGHURT","0.00","20","","73","3","1","0","26","","0000-00-00 00:00:00","1","1","485","","","");
INSERT INTO product VALUES("486","MILD CHEDDAR","MILD CHEDDAR","0.00","35","","73","5","1","0","26","","0000-00-00 00:00:00","1","1","486","","","");
INSERT INTO product VALUES("487","PANEER","PANEER","0.00","35","","73","2","1","0","26","","0000-00-00 00:00:00","1","1","487","","","");
INSERT INTO product VALUES("488","MOZARELLA","MOZARELLA","0.00","45","","73","2","1","0","26","","0000-00-00 00:00:00","1","1","488","","","");
INSERT INTO product VALUES("489","HALLOUMI","HALLOUMI","0.00","45","","73","1","1","0","26","","0000-00-00 00:00:00","1","1","489","","","");
INSERT INTO product VALUES("490","Bubble juice","Bubble juice","0.00","40","","77","0","1","0","71","","0000-00-00 00:00:00","1","1","490","","","inclusive");
INSERT INTO product VALUES("491","Lemon juice","Lemon juice","0.00","15","","77","1","1","0","33","","0000-00-00 00:00:00","1","1","491","","","free");
INSERT INTO product VALUES("492","Granadilla juice","Granadilla juice","0.00","15","","77","30","1","0","33","","0000-00-00 00:00:00","1","1","492","","","free");
INSERT INTO product VALUES("493","Concentrate juice","Concentrate juice","0.00","130","","77","6","1","0","33","","0000-00-00 00:00:00","1","1","493","","","free");
INSERT INTO product VALUES("494","Passion fruit cordial","Passion fruit cordial","0.00","75","","77","4","1","0","36","","0000-00-00 00:00:00","1","1","494","","","free");
INSERT INTO product VALUES("495","Coke","Coke","0.00","10","","77","20","1","0","29","","0000-00-00 00:00:00","1","1","495","","","inclusive");
INSERT INTO product VALUES("496","Water-500ML","Water-500ML","0.00","5","","77","85","1","0","29","","0000-00-00 00:00:00","1","1","496","","","inclusive");
INSERT INTO product VALUES("497","Water","Water","0.00","","","77","0","1","0","29","","0000-00-00 00:00:00","1","1","497","","","inclusive");
INSERT INTO product VALUES("498","Ice tea","Ice tea","0.00","15","","77","5","1","0","29","","0000-00-00 00:00:00","1","1","498","","","inclusive");
INSERT INTO product VALUES("499","Fruit tree","Fruit tree","0.00","15","","77","21","1","0","29","","0000-00-00 00:00:00","1","1","499","","","inclusive");
INSERT INTO product VALUES("500","MCKANE","MCKANE","0.00","10","","77","20","1","0","29","","0000-00-00 00:00:00","1","1","500","","","inclusive");
INSERT INTO product VALUES("501","Tea loose Leaf","Tea loose Leaf","0.00","138","","77","2","1","0","25","","0000-00-00 00:00:00","1","1","501","","","inclusive");
INSERT INTO product VALUES("502","Tea bags","Tea bags","0.00","172","","77","2","1","0","25","","0000-00-00 00:00:00","1","1","502","","","inclusive");
INSERT INTO product VALUES("503","Tea leaf","Tea leaf","0.00","0","","77","0","1","0","25","","0000-00-00 00:00:00","1","1","503","","","inclusive");
INSERT INTO product VALUES("504","Espresso beans","Espresso beans","0.00","80","","77","4","1","0","25","","0000-00-00 00:00:00","1","1","504","","","inclusive");
INSERT INTO product VALUES("505","Espresso ground","Espresso ground","0.00","85","","77","6","1","0","25","","0000-00-00 00:00:00","1","1","505","","","inclusive");
INSERT INTO product VALUES("506","Medium roast beans","Medium roast beans","0.00","80","","77","4","1","0","25","","0000-00-00 00:00:00","1","1","506","","","inclusive");
INSERT INTO product VALUES("507","Medium roast ground","Medium roast ground","0.00","85","","77","5","1","0","25","","0000-00-00 00:00:00","1","1","507","","","inclusive");
INSERT INTO product VALUES("508","Ethiopia roast","Ethiopia roast","0.00","98","","77","11","1","0","25","","0000-00-00 00:00:00","1","1","508","","","inclusive");
INSERT INTO product VALUES("509","Colombia roast","Colombia roast","0.00","98","","77","5","1","0","25","","0000-00-00 00:00:00","1","1","509","","","inclusive");
INSERT INTO product VALUES("510","Zambia roast","Zambia roast","0.00","110","","77","2","1","0","25","","0000-00-00 00:00:00","1","1","510","","","inclusive");
INSERT INTO product VALUES("511","Uganda roast","Uganda roast","0.00","115","","77","8","1","0","25","","0000-00-00 00:00:00","1","1","511","","","inclusive");
INSERT INTO product VALUES("512","Peg-it-3 Pack","Peg-it-3 Pack","0.00","25","","78","13","1","0","31","","0000-00-00 00:00:00","1","1","512","","","inclusive");
INSERT INTO product VALUES("513","Peg-it Magnetic","Peg-it Magnetic","0.00","10","","78","8","1","0","34","","0000-00-00 00:00:00","1","1","513","","","inclusive");
INSERT INTO product VALUES("514","Peg-it- Animal","Peg-it- Animal","0.00","5","","78","14","1","0","34","","0000-00-00 00:00:00","1","1","514","","","inclusive");
INSERT INTO product VALUES("515","Paint-it-Small","Paint-it-Small","0.00","60","","78","25","1","0","34","","0000-00-00 00:00:00","1","1","515","","","inclusive");
INSERT INTO product VALUES("516","Paint-it-Large","Paint-it-Large","0.00","90","","78","26","1","0","34","","0000-00-00 00:00:00","1","1","516","","","inclusive");
INSERT INTO product VALUES("517","Memory jar","Memory jar","0.00","100","","78","3","1","0","26","","0000-00-00 00:00:00","1","1","517","","","inclusive");
INSERT INTO product VALUES("518","Spa jar","Spa jar","0.00","110","","78","3","1","0","26","","0000-00-00 00:00:00","1","1","518","","","inclusive");
INSERT INTO product VALUES("519","Succulent plant- Glass","Succulent plant- Glass","0.00","60","","78","4","1","0","34","","0000-00-00 00:00:00","1","1","519","","","inclusive");
INSERT INTO product VALUES("520","CUT OUT EARINGS","CUT OUT EARINGS","0.00","40","","78","3","1","0","34","","0000-00-00 00:00:00","1","1","520","","","inclusive");
INSERT INTO product VALUES("521","Christmas Deco -Single","Christmas Deco -Single","0.00","","","78","0","1","0","34","","0000-00-00 00:00:00","1","1","521","","","inclusive");
INSERT INTO product VALUES("522","Christmas Deco -4Pack","Christmas Deco -4Pack","0.00","","","78","0","1","0","34","","0000-00-00 00:00:00","1","1","522","","","inclusive");
INSERT INTO product VALUES("523","Magnetic cookie","Magnetic cookie","0.00","35","","78","36","1","0","34","","0000-00-00 00:00:00","1","1","523","","","inclusive");
INSERT INTO product VALUES("524","Birthday calendar-Wood","Birthday calendar-Wood","0.00","220","","78","3","1","0","34","","0000-00-00 00:00:00","1","1","524","","","inclusive");
INSERT INTO product VALUES("525","Key ring-Wood","Key ring-Wood","0.00","","","78","0","1","0","34","","0000-00-00 00:00:00","1","1","525","","","inclusive");
INSERT INTO product VALUES("526","Cheese slate","Cheese slate","0.00","150","","78","0","1","0","34","","0000-00-00 00:00:00","1","1","526","","","inclusive");
INSERT INTO product VALUES("527","Hammer","Hammer","0.00","120","","78","0","1","0","34","","0000-00-00 00:00:00","1","1","527","","","inclusive");
INSERT INTO product VALUES("528","Tin Mug","Tin Mug","0.00","","","78","0","1","0","34","","0000-00-00 00:00:00","1","1","528","","","inclusive");
INSERT INTO product VALUES("529","Chalk-It Bunting","Chalk-It Bunting","0.00","55","","78","10","1","0","34","","0000-00-00 00:00:00","1","1","529","","","inclusive");
INSERT INTO product VALUES("530","Keyrings","Keyrings","0.00","50","","78","5","1","0","34","","0000-00-00 00:00:00","1","1","530","","","inclusive");
INSERT INTO product VALUES("531","gift tags","gift tags","0.00","25","","78","5","1","0","31","","0000-00-00 00:00:00","1","1","531","","","inclusive");
INSERT INTO product VALUES("532","Serviette Rings","Serviette Rings","0.00","70","","78","3","1","0","34","","0000-00-00 00:00:00","1","1","532","","","inclusive");
INSERT INTO product VALUES("533","Ear Phone Holder","Ear Phone Holder","0.00","80","","78","3","1","0","31","","0000-00-00 00:00:00","1","1","533","","","inclusive");
INSERT INTO product VALUES("534","Word Blocks","Word Blocks","0.00","120","","78","10","1","0","31","","0000-00-00 00:00:00","1","1","534","","","inclusive");
INSERT INTO product VALUES("535","Cheese slate","Cheese slate","0.00","150","","78","1","1","0","34","","0000-00-00 00:00:00","1","1","535","","","inclusive");
INSERT INTO product VALUES("536","Slate - Large","Slate - Large","0.00","300","","78","1","1","0","34","","0000-00-00 00:00:00","1","1","536","","","inclusive");
INSERT INTO product VALUES("537","Happy Birthday","Happy Birthday","0.00","40","","78","6","1","0","31","","0000-00-00 00:00:00","1","1","537","","","inclusive");
INSERT INTO product VALUES("538","Butterfly Welcome","Butterfly Welcome","0.00","150","","78","2","1","0","31","","0000-00-00 00:00:00","1","1","538","","","inclusive");
INSERT INTO product VALUES("539","Bench1.2mx0.4","Bench1.2mx0.4","0.00","1400","","78","1","1","0","29","","0000-00-00 00:00:00","1","1","539","","","inclusive");
INSERT INTO product VALUES("540","Bench1.2mx0.5","Bench1.2mx0.5","0.00","1600","","78","1","1","0","29","","0000-00-00 00:00:00","1","1","540","","","inclusive");
INSERT INTO product VALUES("541","Tractor & trailer-Large","Tractor & trailer-Large","0.00","500","","78","2","1","0","29","","0000-00-00 00:00:00","1","1","541","","","inclusive");
INSERT INTO product VALUES("542","Tractor-Small","Tractor-Small","0.00","","","78","0","1","0","29","","0000-00-00 00:00:00","1","1","542","","","inclusive");
INSERT INTO product VALUES("543","Swing","Swing","0.00","700","","78","2","1","0","29","","0000-00-00 00:00:00","1","1","543","","","inclusive");
INSERT INTO product VALUES("544","Straw set","Straw set","0.00","100","","78","1","1","0","29","","0000-00-00 00:00:00","1","1","544","","","inclusive");
INSERT INTO product VALUES("545","Chitenge bag","Chitenge bag","0.00","100","","78","0","1","0","76","","0000-00-00 00:00:00","1","1","545","","","inclusive");
INSERT INTO product VALUES("546","African stool","African stool","0.00","250","","78","4","1","0","29","","0000-00-00 00:00:00","1","1","546","","","inclusive");
INSERT INTO product VALUES("547","African stool","African stool","0.00","250","","78","2","1","0","29","","0000-00-00 00:00:00","1","1","547","","","inclusive");
INSERT INTO product VALUES("548","Hessian bag","Hessian bag","0.00","290","","78","22","1","0","30","","0000-00-00 00:00:00","1","1","548","","","inclusive");
INSERT INTO product VALUES("549","Earrings","Earrings","0.00","60","","78","14","1","0","39","","0000-00-00 00:00:00","1","1","549","","","inclusive");
INSERT INTO product VALUES("550","Chitenge animals - S","Chitenge animals - S","0.00","250","","78","1","1","0","43","","0000-00-00 00:00:00","1","1","550","","","inclusive");
INSERT INTO product VALUES("551","Chinenge Animals - M","Chinenge Animals - M","0.00","290","","78","2","1","0","43","","0000-00-00 00:00:00","1","1","551","","","inclusive");
INSERT INTO product VALUES("552","Chinenge Animals - L","Chinenge Animals - L","0.00","350","","78","3","1","0","43","","0000-00-00 00:00:00","1","1","552","","","inclusive");
INSERT INTO product VALUES("553","Chitenge tea cosy","Chitenge tea cosy","0.00","270","","78","1","1","0","43","","0000-00-00 00:00:00","1","1","553","","","inclusive");
INSERT INTO product VALUES("554","Card","Card","0.00","","","78","0","1","0","43","","0000-00-00 00:00:00","1","1","554","","","inclusive");
INSERT INTO product VALUES("555","Wash bag","Wash bag","0.00","295","","78","1","1","0","43","","0000-00-00 00:00:00","1","1","555","","","inclusive");
INSERT INTO product VALUES("556","Overnight bag","Overnight bag","0.00","510","","78","2","1","0","43","","0000-00-00 00:00:00","1","1","556","","","inclusive");
INSERT INTO product VALUES("557","Coffee basket","Coffee basket","0.00","","","78","0","1","0","43","","0000-00-00 00:00:00","1","1","557","","","inclusive");
INSERT INTO product VALUES("558","Cooler bag-l","Cooler bag-l","0.00","","","78","0","1","0","43","","0000-00-00 00:00:00","1","1","558","","","inclusive");
INSERT INTO product VALUES("559","Cooler bag-m","Cooler bag-m","0.00","510","","78","3","1","0","43","","0000-00-00 00:00:00","1","1","559","","","inclusive");
INSERT INTO product VALUES("560","Cooler bag-s","Cooler bag-s","0.00","470","","78","1","1","0","43","","0000-00-00 00:00:00","1","1","560","","","inclusive");
INSERT INTO product VALUES("561","Lunch bag","Lunch bag","0.00","","","78","0","1","0","43","","0000-00-00 00:00:00","1","1","561","","","inclusive");
INSERT INTO product VALUES("562","Shopping bag","Shopping bag","0.00","490","","78","5","1","0","43","","0000-00-00 00:00:00","1","1","562","","","inclusive");
INSERT INTO product VALUES("563","Wine cooler","Wine cooler","0.00","","","78","0","1","0","43","","0000-00-00 00:00:00","1","1","563","","","inclusive");
INSERT INTO product VALUES("564","Box bag","Box bag","0.00","595","","78","1","1","0","43","","0000-00-00 00:00:00","1","1","564","","","inclusive");
INSERT INTO product VALUES("565","Pencil case-m","Pencil case-m","0.00","","","78","0","1","0","43","","0000-00-00 00:00:00","1","1","565","","","inclusive");
INSERT INTO product VALUES("566","Pencil case-s","Pencil case-s","0.00","","","78","0","1","0","43","","0000-00-00 00:00:00","1","1","566","","","inclusive");
INSERT INTO product VALUES("567","Chikumbuso Crochet - Medium","Chikumbuso Crochet - Medium","0.00","390","","78","1","1","0","76","","0000-00-00 00:00:00","1","1","567","","","inclusive");
INSERT INTO product VALUES("568","Chikumbuso Plastic Bag - Large","Chikumbuso Plastic Bag - Large","0.00","420","","78","1","1","0","76","","0000-00-00 00:00:00","1","1","568","","","inclusive");
INSERT INTO product VALUES("569","Chikimbuso Plastic Bag - Madium","Chikimbuso Plastic Bag - Madium","0.00","350","","78","3","1","0","76","","0000-00-00 00:00:00","1","1","569","","","inclusive");
INSERT INTO product VALUES("570","Chikimbuso Apron","Chikimbuso Apron","0.00","160","","78","2","1","0","76","","0000-00-00 00:00:00","1","1","570","","","inclusive");
INSERT INTO product VALUES("571","Chikumbuso Chitenge Bag","Chikumbuso Chitenge Bag","0.00","270","","78","1","1","0","76","","0000-00-00 00:00:00","1","1","571","","","inclusive");
INSERT INTO product VALUES("572","Rope bowl-l","Rope bowl-l","0.00","230","","78","3","1","0","76","","0000-00-00 00:00:00","1","1","572","","","inclusive");
INSERT INTO product VALUES("573","Rope bowl-m","Rope bowl-m","0.00","155","","78","6","1","0","76","","0000-00-00 00:00:00","1","1","573","","","inclusive");
INSERT INTO product VALUES("574","Rope bowl-s","Rope bowl-s","0.00","110","","78","2","1","0","76","","0000-00-00 00:00:00","1","1","574","","","inclusive");
INSERT INTO product VALUES("575","Purse changer","Purse changer","0.00","160","","78","1","1","0","76","","0000-00-00 00:00:00","1","1","575","","","inclusive");
INSERT INTO product VALUES("576","Make up bag","Make up bag","0.00","160","","78","1","1","0","76","","0000-00-00 00:00:00","1","1","576","","","inclusive");
INSERT INTO product VALUES("577","Placemats-4set","Placemats-4set","0.00","","","78","0","1","0","76","","0000-00-00 00:00:00","1","1","577","","","inclusive");
INSERT INTO product VALUES("578","ipad cover","ipad cover","0.00","150","","78","2","1","0","76","","0000-00-00 00:00:00","1","1","578","","","inclusive");
INSERT INTO product VALUES("579","ipad cover-mini","ipad cover-mini","0.00","120","","78","2","1","0","76","","0000-00-00 00:00:00","1","1","579","","","inclusive");
INSERT INTO product VALUES("580","Laptop bag-l","Laptop bag-l","0.00","","","78","0","1","0","76","","0000-00-00 00:00:00","1","1","580","","","inclusive");
INSERT INTO product VALUES("581","Laptop bag-m","Laptop bag-m","0.00","","","78","0","1","0","76","","0000-00-00 00:00:00","1","1","581","","","inclusive");
INSERT INTO product VALUES("582","Laptop bag-s","Laptop bag-s","0.00","","","78","0","1","0","76","","0000-00-00 00:00:00","1","1","582","","","inclusive");
INSERT INTO product VALUES("583","Print canvas","Print canvas","0.00","1390","","78","1","1","0","43","","0000-00-00 00:00:00","1","1","583","","","inclusive");
INSERT INTO product VALUES("584","Block Sign","Block Sign","0.00","250","","78","3","1","0","43","","0000-00-00 00:00:00","1","1","584","","","inclusive");
INSERT INTO product VALUES("585","JAR EARINGS","JAR EARINGS","0.00","80","","78","15","1","0","43","","0000-00-00 00:00:00","1","1","585","","","inclusive");
INSERT INTO product VALUES("586","Hanging Sign","Hanging Sign","0.00","200","","78","1","1","0","43","","0000-00-00 00:00:00","1","1","586","","","inclusive");
INSERT INTO product VALUES("587","Hand Carved Mirror","Hand Carved Mirror","0.00","1680","default.gif","77","1","1","0","30","0","2020-01-30 16:23:01","1","1","587","","","inclusive");



CREATE TABLE `purchase_request` (
  `pr_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `request_date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `purchase_status` varchar(10) NOT NULL,
  PRIMARY KEY (`pr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `reversals_log` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `qty` int(12) NOT NULL,
  `action` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `order_no` text NOT NULL,
  `user_id` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `sales` (
  `sales_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cash_tendered` decimal(10,2) DEFAULT NULL,
  `discount` text,
  `amount_due` decimal(10,2) NOT NULL,
  `cash_change` decimal(10,2) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `modeofpayment` varchar(15) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `order_no` int(12) NOT NULL,
  `special_remarks` text NOT NULL,
  PRIMARY KEY (`sales_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

INSERT INTO sales VALUES("11","0","8","80.00","0","2.00","8.00","2019-12-19 10:41:59","1","1","0.00","942","");
INSERT INTO sales VALUES("12","0","8","70.00","0","10.00","6.00","2019-12-19 10:50:34","1","1","0.00","3040","");
INSERT INTO sales VALUES("13","0","8","18.00","0","0.00","0.00","2019-12-19 14:57:47","1","1","0.00","4239","");
INSERT INTO sales VALUES("14","0","8","2.00","0","0.00","0.00","2019-12-19 14:58:46","1","1","0.00","724","");
INSERT INTO sales VALUES("15","0","8","2.00","0","2.00","0.00","2019-12-19 14:59:20","1","1","2.00","4323","");
INSERT INTO sales VALUES("16","0","8","4.00","0","4.00","0.00","2019-12-19 16:01:13","1","1","4.00","3942","");
INSERT INTO sales VALUES("17","0","8","50.00","0","4.00","46.00","2019-12-19 16:10:18","1","1","4.00","2848","Chilli Hot");
INSERT INTO sales VALUES("18","0","8","3.00","0","0.00","1.00","2019-12-22 16:46:32","1","1","0.00","4059","");
INSERT INTO sales VALUES("19","0","8","60.00","0","0.00","6.00","2019-12-22 16:47:58","1","1","0.00","2231","");
INSERT INTO sales VALUES("20","0","8","1500.00","0","0.00","60.00","2020-02-05 22:44:28","1","1","0.00","533","");
INSERT INTO sales VALUES("21","0","8","500.00","0","0.00","-210.00","2020-02-05 22:46:10","4","1","0.00","874","");
INSERT INTO sales VALUES("22","0","8","4000.00","0","0.00","646.00","2020-02-06 17:01:48","1","1","0.00","3836","");



CREATE TABLE `sales_details` (
  `sales_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `order_no` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `discount` text NOT NULL,
  `discount_type` text NOT NULL,
  PRIMARY KEY (`sales_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

INSERT INTO sales_details VALUES("44","11","683","18.00","4","942","8","2","Amount");
INSERT INTO sales_details VALUES("45","11","682","2.00","1","942","8","","");
INSERT INTO sales_details VALUES("46","12","683","18.00","4","3040","8","10","Amount");
INSERT INTO sales_details VALUES("47","12","682","2.00","1","3040","8","","");
INSERT INTO sales_details VALUES("48","13","683","18.00","1","4239","8","","");
INSERT INTO sales_details VALUES("49","14","682","2.00","1","724","8","","");
INSERT INTO sales_details VALUES("50","15","682","2.00","1","4323","8","","");
INSERT INTO sales_details VALUES("51","16","682","2.00","2","3942","8","","");
INSERT INTO sales_details VALUES("52","17","682","2.00","2","2848","8","","");
INSERT INTO sales_details VALUES("53","18","682","2.00","1","4059","8","","");
INSERT INTO sales_details VALUES("54","19","683","18.00","3","2231","8","","");
INSERT INTO sales_details VALUES("55","20","683","18.00","1","533","8","","");
INSERT INTO sales_details VALUES("56","20","84","340.00","2","533","8","","");
INSERT INTO sales_details VALUES("57","20","122","510.00","1","533","8","","");
INSERT INTO sales_details VALUES("58","20","138","250.00","1","533","8","","");
INSERT INTO sales_details VALUES("59","21","134","710.00","1","874","8","","");
INSERT INTO sales_details VALUES("60","22","62","585.00","3","3836","8","","");
INSERT INTO sales_details VALUES("61","22","123","550.00","1","3836","8","","");
INSERT INTO sales_details VALUES("62","22","126","820.00","1","3836","8","","");
INSERT INTO sales_details VALUES("63","22","424","114.00","1","3836","8","","");
INSERT INTO sales_details VALUES("64","22","257","50.00","1","3836","8","","");
INSERT INTO sales_details VALUES("65","22","312","65.00","1","3836","8","","");



CREATE TABLE `sales_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `item_sold_id` int(12) NOT NULL,
  `quantity` int(12) NOT NULL,
  `sales_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(12) NOT NULL,
  `price` int(12) NOT NULL,
  `date` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `shop_category_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO shop_category_tb VALUES("1","Bar");
INSERT INTO shop_category_tb VALUES("2","Restruant");



CREATE TABLE `stock_audit_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` text NOT NULL,
  `count` int(12) NOT NULL,
  `added_to` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `action` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

INSERT INTO stock_audit_tb VALUES("1","PARM/BANAN LOW FAT YOG 175G","10","New Station Mumbwa","2019-10-14 17:51:32","Edited");
INSERT INTO stock_audit_tb VALUES("2","Classic Choclate","100","New Station Mumbwa","2019-10-18 08:11:59","Added");
INSERT INTO stock_audit_tb VALUES("3","Parmalat Milk","500","New Station Mumbwa","2019-10-18 08:12:28","Added");
INSERT INTO stock_audit_tb VALUES("4","Added Item","21","Kitchen","2019-11-11 08:49:43","Added");
INSERT INTO stock_audit_tb VALUES("5","Added Item","21","Kitchen","2019-11-11 09:04:50","Edited");
INSERT INTO stock_audit_tb VALUES("6","Added Item","1","Kitchen","2019-11-11 15:55:24","Deleted");
INSERT INTO stock_audit_tb VALUES("7","Added Item","6","Kitchen","2019-11-11 15:55:38","Added");
INSERT INTO stock_audit_tb VALUES("8","TEST VAT","6","Kitchen","2019-11-11 15:57:09","Added");
INSERT INTO stock_audit_tb VALUES("9","TEST VAT","6","Kitchen","2019-11-11 16:04:32","Edited");
INSERT INTO stock_audit_tb VALUES("10","Parmalat Milk","500","Kitchen","2019-11-11 16:10:55","Edited");
INSERT INTO stock_audit_tb VALUES("11","Coolant","50","Kitchen","2019-11-19 12:11:15","Added");
INSERT INTO stock_audit_tb VALUES("12","Shampoo","30","Kitchen","2019-12-01 15:35:58","Added");
INSERT INTO stock_audit_tb VALUES("13","FANTA ORANGE 500ML","600","Kitchen","2019-12-01 15:36:18","Added");
INSERT INTO stock_audit_tb VALUES("14","test","2","Kitchen","2019-12-14 17:13:47","Added");
INSERT INTO stock_audit_tb VALUES("15","test","1000","Kitchen","2019-12-15 16:12:07","Edited");
INSERT INTO stock_audit_tb VALUES("16","FUNDBAT","20","Kitchen","2019-12-17 15:47:02","Added");
INSERT INTO stock_audit_tb VALUES("17","FUNDBAT","30","Kitchen","2019-12-22 16:47:10","Edited");
INSERT INTO stock_audit_tb VALUES("18","Hand Carved Mirror","1","BAR","2020-01-30 16:15:47","Edited");
INSERT INTO stock_audit_tb VALUES("19","Hand Carved Mirror","1","BAR","2020-01-30 16:17:58","Edited");
INSERT INTO stock_audit_tb VALUES("20","Hand Carved Mirror","1","BAR","2020-01-30 16:18:47","Edited");
INSERT INTO stock_audit_tb VALUES("21","Hand Carved Mirror","1","BAR","2020-01-30 16:19:35","Edited");
INSERT INTO stock_audit_tb VALUES("22","Hand Carved Mirror","1","BAR","2020-01-30 16:21:09","Edited");
INSERT INTO stock_audit_tb VALUES("23","Hand Carved Mirror","1","BAR","2020-01-30 16:21:32","Edited");
INSERT INTO stock_audit_tb VALUES("24","Hand Carved Mirror","1","BAR","2020-01-30 16:23:01","Edited");
INSERT INTO stock_audit_tb VALUES("25","Hand Carved Mirror","1","BAR","2020-01-30 16:23:15","Edited");
INSERT INTO stock_audit_tb VALUES("26","Hand Carved Mirror","1","BAR","2020-01-30 16:23:25","Edited");



CREATE TABLE `stock_damages_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `no_damages` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `stock_purchases_tb` (
  `purchase_id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `qty` text NOT NULL,
  `user_id` int(12) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `supplier_id` int(12) NOT NULL,
  `status` text NOT NULL,
  `invoice` int(12) NOT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO stock_purchases_tb VALUES("3","779","300","4","2019-10-14 18:28:41","14","purchased","1992");
INSERT INTO stock_purchases_tb VALUES("5","406","580","4","2019-10-14 18:55:22","11","purchased","1993");
INSERT INTO stock_purchases_tb VALUES("7","767","701","4","2019-10-27 08:19:25","11","purchased","0");
INSERT INTO stock_purchases_tb VALUES("8","471","890","4","2019-10-27 08:19:25","11","purchased","0");
INSERT INTO stock_purchases_tb VALUES("9","501","450","4","2019-10-27 08:19:25","11","purchased","0");
INSERT INTO stock_purchases_tb VALUES("10","770","780","4","2019-10-27 08:19:25","11","purchased","0");



CREATE TABLE `stock_trasfers_tb` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `prod_id` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `qty` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `moved_to` int(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `stockin` (
  `stockin_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `qty` int(6) NOT NULL,
  `date` datetime NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`stockin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `stores_branch` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `branch_name` text NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO stores_branch VALUES("1","BAR","2020-01-29 16:48:38");



CREATE TABLE `supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_address` varchar(300) NOT NULL,
  `supplier_contact` varchar(50) NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

INSERT INTO supplier VALUES("22","HIPPO ","","");
INSERT INTO supplier VALUES("23","NNINO","","");
INSERT INTO supplier VALUES("24","REVOLTAS","","");
INSERT INTO supplier VALUES("25","ILALA","","");
INSERT INTO supplier VALUES("26","ARTISIAN","","");
INSERT INTO supplier VALUES("27","BASES","","");
INSERT INTO supplier VALUES("28","WOODEN","","");
INSERT INTO supplier VALUES("29","BALI","","");
INSERT INTO supplier VALUES("30","A HOP
","","");
INSERT INTO supplier VALUES("31","VERANDA GAL

","","");
INSERT INTO supplier VALUES("32","Other

","","");
INSERT INTO supplier VALUES("33","MEG

","","");
INSERT INTO supplier VALUES("34","ROOTS & WINGS

","","");
INSERT INTO supplier VALUES("35","ALL KINDS


","","");
INSERT INTO supplier VALUES("36","BEAUTIFUL 



","","");
INSERT INTO supplier VALUES("37","craftz
","","");
INSERT INTO supplier VALUES("38","LE PUG
","","");
INSERT INTO supplier VALUES("39","CAROLINA BUTCHERY","","");
INSERT INTO supplier VALUES("40","RETO
","","");
INSERT INTO supplier VALUES("41","LEMON FRESH

","","");
INSERT INTO supplier VALUES("42","FRINGILLA 


","","");
INSERT INTO supplier VALUES("43","THE MOCHA PUDDLE


","","");
INSERT INTO supplier VALUES("44","SPROUT HOUSE



","","");
INSERT INTO supplier VALUES("45","ALEXS KITCHEN



","","");
INSERT INTO supplier VALUES("46","FOOD FOR THOUGHT




","","");
INSERT INTO supplier VALUES("47","STRAIGHT SHOT





","","");
INSERT INTO supplier VALUES("48","TRAVIUM SUPPLIES



","","");
INSERT INTO supplier VALUES("49","HESTER VAN RENSBURG




","","");
INSERT INTO supplier VALUES("50","JAMEY DEWBERRY





","","");
INSERT INTO supplier VALUES("51","MAJORU






","","");
INSERT INTO supplier VALUES("52","JILLYS ENTERPRISE
","","");
INSERT INTO supplier VALUES("53","UPTOWN CAFÉ
","","");
INSERT INTO supplier VALUES("54","TREATS

","","");
INSERT INTO supplier VALUES("55","DEW FRESH


","","");
INSERT INTO supplier VALUES("56","JUST JANE



","","");
INSERT INTO supplier VALUES("57","ZAMOR



","","");
INSERT INTO supplier VALUES("58","DINGLES




","","");
INSERT INTO supplier VALUES("59","POM&CO




","","");
INSERT INTO supplier VALUES("60","BUTCHER BLOCK





","","");
INSERT INTO supplier VALUES("61","YORK






","","");
INSERT INTO supplier VALUES("62","ITS FRESH







","","");
INSERT INTO supplier VALUES("63","DOMS KITCHEN








","","");
INSERT INTO supplier VALUES("64","ZAMBIA HEALTH









","","");
INSERT INTO supplier VALUES("65","DALE GAMES










","","");
INSERT INTO supplier VALUES("66","ROWAN FARMS











","","");
INSERT INTO supplier VALUES("67","GOGO KITCHEN












","","");
INSERT INTO supplier VALUES("68","SUNFLOWER












","","");
INSERT INTO supplier VALUES("69","COPPER KETTLE












","","");
INSERT INTO supplier VALUES("70","SUNFLOWER KITCHEN













","","");
INSERT INTO supplier VALUES("71","BUBBLE & SHAKE
","","");
INSERT INTO supplier VALUES("72","JUMBO
","","");
INSERT INTO supplier VALUES("73","PEABERRY COFFEE  

","","");
INSERT INTO supplier VALUES("74","k+t

","","");
INSERT INTO supplier VALUES("75","TKZ TRADING

","","");
INSERT INTO supplier VALUES("76","CHIKUMBUSO


","","");
INSERT INTO supplier VALUES("77","SUE ELLEN


","","");
INSERT INTO supplier VALUES("78","FAITH



","","");
INSERT INTO supplier VALUES("79","MATILDA TOYS




","","");
INSERT INTO supplier VALUES("80","PIECE OF CARD





","","");
INSERT INTO supplier VALUES("81","GREAT NORTH






","","");
INSERT INTO supplier VALUES("82","MCFK






","","");
INSERT INTO supplier VALUES("83","ruzawi






","","");



CREATE TABLE `supplier_payments_tb` (
  `payment_id` int(12) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(12) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount_paid` text NOT NULL,
  `balance` text NOT NULL,
  `total_amount` text NOT NULL,
  `status` text NOT NULL,
  `invoice_no` int(12) NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `temp_trans` (
  `temp_trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` text NOT NULL,
  `branch_id` int(11) NOT NULL,
  `user_id` int(12) NOT NULL,
  `discount_type` text NOT NULL,
  `amount` text NOT NULL,
  PRIMARY KEY (`temp_trans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `term` (
  `term_id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_id` int(11) DEFAULT NULL,
  `payable_for` varchar(10) NOT NULL,
  `term` varchar(11) NOT NULL,
  `due` decimal(10,2) NOT NULL,
  `payment_start` date NOT NULL,
  `down` decimal(10,2) NOT NULL,
  `due_date` date NOT NULL,
  `interest` decimal(10,2) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`term_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `user_type` text NOT NULL,
  `branch_id_user` int(12) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO user VALUES("4","admin","21232f297a57a5a743894a0e4a801fc3","Mona Lisas","active","1","Admin","3");
INSERT INTO user VALUES("8","user","81dc9bdb52d04dc20036dbd8313ed055","test USer","active","1","User","1");
INSERT INTO user VALUES("10","user","2eeecd72c567401e6988624b179d0b14","Supervisor","active","1","Supervisor","0");
INSERT INTO user VALUES("11","Monde","d5c186983b52c4551ee00f72316c6eaa","Monde Sichuma","active","1","User","1");



CREATE TABLE `ware_house_tb` (
  `prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_name` varchar(100) NOT NULL,
  `prod_desc` varchar(500) NOT NULL,
  `prod_price` decimal(10,2) NOT NULL,
  `prod_sell_price` text NOT NULL,
  `prod_pic` varchar(300) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `prod_qty` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `serial` varchar(50) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_by` int(12) NOT NULL,
  `barcode` text NOT NULL,
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


