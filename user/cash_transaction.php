<?php
session_start();
if (empty($_SESSION['id'])) :
    header('Location:../index.php');
endif;
if (empty($_SESSION['branch'])) :
    header('Location:../index.php');
endif;

include('../dist/includes/variant_validator.php');

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home | <?php include('../dist/includes/title.php'); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../plugins/select2/select2.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
    <script src="../dist/js/jquery.min.js"></script>
    <script language="JavaScript">
        //  javascript:window.history.forward(1);
    </script>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->

<body class="hold-transition skin-<?php echo $_SESSION['skin']; ?> layout-top-nav" onload="myFunction()">
    <div class="wrapper">
        <?php include('../dist/includes/header.php'); ?>
        <!-- Full Width Column -->
        <div class="content-wrapper">
            <div class="container">
                <section class="content">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title" style="color: black"><b><u><b>Select a Category</b></u></b></h3><br> <br> <a style=" color: red" href="escpos-php-development/example/interface/windows-usb.php"><b>Reprint Previous Receipt | </b></a>

                                    <a style=" color: red" href="input-invoiceno.php"><b>Reprint Receipt By Invoice #</b></a> | <a style=" color: red" href="input-invoiceno.php"><b>( ) on Hold</b></a>

                                    <?php
                                    
                                    $userId = $_SESSION['id'];
                                    $today = date("Y-m-d H:i:s");
                                    $totalAmountCollected = 0;
                                    $openSalesQuery = mysqli_query($con, "SELECT login FROM open_close_cashout_tb WHERE user_id='$userId' AND status=''");
                                    $openrows = mysqli_fetch_array($openSalesQuery);
                                    $loginTime = $openrows['login'];

                                    $query2 = mysqli_query($con, "SELECT SUM(qty) AS qty, prod_sell_price  "
                                        . "FROM sales_details INNER JOIN sales ON sales.sales_id=sales_details.sales_id INNER JOIN user ON user.user_id = sales.user_id "
                                        . "INNER JOIN product ON product.prod_id = sales_details.prod_id"
                                        . " AND sales.date_added >= '$loginTime' AND sales.date_added <= '$today' AND user.user_id='$userId' GROUP BY prod_name,stock_branch_id") or die(mysqli_error($con));

                                    while ($row = mysqli_fetch_array($query2)) {
                                        $totalSold = $row['qty'] * $row['prod_sell_price'];
                                        $totalAmountCollected += $totalSold;
                                    }

                                    $cahsoutLimitQuery = mysqli_query($con, " SELECT * FROM cashout_limits_tb WHERE status ='Active' ") or die(mysqli_error($con));
                                    $row2 = mysqli_fetch_array($cahsoutLimitQuery);
                                    $limit = $row2['cashoutlimit'];
                                    $found = mysqli_num_rows($cahsoutLimitQuery);
                                    ?>
                                    <br>
                                    <?php
                                    if ($found != "0") {
                                        if ($totalAmountCollected > $limit) {
                                            echo ' <h2 class="box-title" style=" color: red"><b>You have exceeded your Selling Limit of : ' . number_format($limit, 2) . ' please cashout !!
                                                        </b></h2>';
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="box-body">
                                    <?php
                                    if (isset($_POST['addtocart'])) {
                                        $barcode = $_POST['barcode'];
                                        //$query = mysql_query(" INSERT INTO barcode VALUES ('','$barcode',now())");
                                        echo $barcode . '<br>';
                                        //header("LOCATION:test-barcode.php");6001068379101
                                    }
                                    ?>
                                    <?php require_once('transaction_add.php'); ?>
                                    <form autocomplete="off" method="post" action="#">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">




                                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                                                    <input class="hideinput" type="text" id="Performer1f" style="display: none; width: 100%;" name='entryname1f[]' placeholder="Performer 1 First Name " />
                                                        <input class="hideinput" type="text" id="<?php echo $row['prod_id']; ?>" style="display: none; width: 100%;" name='entryname1f[]' placeholder="Performer 2 First Name "/>
                                                        <input class="hideinput" type="text" id="Performer3f" style="display: none; width: 100%;" name='entryname1f[] ' placeholder="Performer 3 First Name "/>
                                                        <input class="hideinput" type="text" id="Performer4f" style="display: none; width: 100%;" name='entryname1f[]' placeholder="Performer 4 First Name "/>
                                                        <input class="hideinput" type="text" id="Performer5f" style="display: none; width: 100%;" name='entryname1f[]' placeholder="Performer 5 First Name "/>

                                                        <input class="hideinput" type="text" id="Performer1l" style="display: none; width: 100%;" name='entryname1l[]' placeholder="Performer 1 Last Name " />
                                                        <input class="hideinput" type="text" id="Performer2l" style="display: none; width: 100%;" name='entryname1l[]' placeholder="Performer 2 Last Name "/>
                                                        <input class="hideinput" type="text" id="Performer3l" style="display: none; width: 100%;" name='entryname1l[] ' placeholder="Performer 3 Last Name "/>
                                                        <input class="hideinput" type="text" id="Performer4l" style="display: none; width: 100%;" name='entryname1l[]' placeholder="Performer 4 Last Name "/>
                                                    <input class="hideinput" type="text" id="Performer5l" style="display: none; width: 100%;" name='entryname1l[]' placeholder="Performer 5 Last Name "/>

                                                    <script>
                                                        function perfChange(display_id = 0) {
                                                            var make_id_1 = "#"+display_id;
                                                            var make_id_2 = "#Performer"+display_id+"l";
                                                            $(".hideinput").hide();
                                                            $(make_id_1).show();
                                                            $(make_id_2).show();
                                                        }
                                                    </script>

                                                    <label for="date">Product Name</label>
                                                    <select class="form-control select2" name="prod_name" tabindex="1" class="myselect" id="myselect" onChange="perfChange(this.value);">
                                                        <?php
                                                        $branch = $_SESSION['branch'];
                                                        $cid = $_REQUEST['cid'];
                                                        $branch_id_user = $_SESSION['branch_id_user'];
                                                        // return var_dump($branch_id_user);
                                                        include('../dist/includes/dbcon.php');
                                                        $query2 = mysqli_query($con, "SELECT * from product INNER JOIN exchange_rates_tb ON exchange_rates_tb.exchange_id=product.currency_id 
                                                            AND stock_branch_id='$branch_id_user' AND prod_qty >0 AND (DATE(NOW()) < DATE(expire_date) OR expire_date ='') order by prod_name") or die(mysqli_error($con));
                                                        while ($row = mysqli_fetch_array($query2)) {
                                                            $prod_d = $row['prod_id'];
                                                            if ($row['rate'] == 0) {
                                                                $price = $row['prod_sell_price'];
                                                            } else {
                                                                $price = $row['prod_sell_price'] * $row['rate'];
                                                            }
                                                        ?>
                                                            <option value="<?php echo $row['prod_id']; ?>"><?php echo $row['prod_name'] . " ( K " . number_format($price, 2) . " ) "; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <input type="hidden" class="form-control" name="cid" value="<?php echo $cid; ?>">


                                                </div><!-- /.form group -->
                                            </div>

                                            <div class=" col-md-2">
                                                <div class="form-group">
                                                    <label for="date">Quantity</label>
                                                    <div class="input-group">
                                                        <input type="number" min="1" class="form-control pull-right" id="date" name="qty" placeholder="Quantity" tabindex="2" value="1" required>
                                                    </div><!-- /.input group -->
                                                </div><!-- /.form group -->
                                            </div>

                                            <div class=" col-md-2">
                                                <div class="form-group">
                                                    <label for="date">Barcode</label>
                                                    <div class="input-group">
                                                        <input type="number" class="form-control pull-right" id="date" name="barcode" placeholder="Barcode" tabindex="2" value="" name="barcode" autofocus>
                                                    </div><!-- /.input group -->
                                                </div><!-- /.form group -->
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="date"></label>
                                                    <div class="input-group">
                                                        <input type="submit" class="btn btn-lg btn-primary" name="addtocart" value="+" />
                                                        <!-- <button class="btn btn-lg btn-primary" type="submit" tabindex="3" name="addtocart">+</button> -->
                                                    </div>
                                                </div>
                                    </form>

                                </div>
                                <div class="col-md-12">
                                    <?php
                                    $queryb = mysqli_query($con, "select balance from customer where cust_id='$cid'") or die(mysqli_error($con));
                                    $rowb = mysqli_fetch_array($queryb);
                                    $balance = $rowb['balance'];

                                    if ($balance > 0)
                                        $disabled = "disabled=true";
                                    else {
                                        $disabled = "";
                                    }
                                    ?>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Qty</th>
                                                <th>Variants</th>
                                                <th>Product Name</th>
                                                <th>Unit Price</th>
                                                <th>Total</th>
                                                <th>Action</th>
                                                <th>Discount</th>
                                                <th>composites</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $user_id = $_SESSION['id'];
                                            $query = mysqli_query($con, "SELECT * FROM temp_trans NATURAL JOIN product WHERE branch_id='$branch' AND user_id='$user_id'  ") or die(mysqli_error($con));
                                            $grand = 0;
                                            $totalDiscount = 0;
                                            $newPrice = 0;
                                            while ($row = mysqli_fetch_array($query)) {
                                                $id = $row['temp_trans_id'];
                                                $temp_prod_id = $row['prod_id'];


                                                $price = $row['price'];
                                                $discount_type = $row['discount_type'];
                                                $amount = $row['amount'];

                                                $selectedQty = $row['qty'];
                                                $composites = $row['composites'];

                                                // return var_dump($composite_names);

                                                if ($amount != "") {
                                                    if ($discount_type == "Percentage") {
                                                        $newPrice = ($amount / 100) * ($row['price'] * $row['qty']);
                                                        //$newPrice = $price - $computedPrice;
                                                    } else {
                                                        $newPrice = $amount;
                                                    }
                                                    $total = ($row['price'] * $row['qty']) - $newPrice;
                                                } elseif ($composites != "") {
                                                    $newPrice = ($composites / 100) * ($row['price'] * $row['qty']);
                                                    $total = ($row['price'] * $row['qty']) - $newPrice;
                                                } else {
                                                    $total = ($row['price'] * $row['qty']);
                                                }

                                                $composite_names = $row['composite_names'];

                                                $unitPrice = $row['price'] / $row['qty'];
                                                $grand = $grand + $total;
                                                $totalDiscount += $row['amount'];
                                                // Product ID
                                                $product_id = $row['prod_id'];
                                            ?>
                                                <tr>
                                                    <td><?php echo $row['qty']; ?></td>
                                                    <td>
                                                            <?php
                                                                $mode = "cash";
                                                                display_variants($con, $product_id, $mode);
                                                            ?>
                                                    </td>
                                                       
                                                    <td class="record"><?php echo $row['prod_name']; ?></td>
                                                    <td><?php echo number_format($row['price'], 2); ?></td>
                                                    <td><?php echo number_format($total, 2); ?></td>
                                                    <td>
                                                        <a href="#updateordinance<?php echo $row['temp_trans_id']; ?>" data-target="#updateordinance<?php echo $row['temp_trans_id']; ?>" data-toggle="modal" style="color:#fff;" class="small-box-footer"><i style="font-size:20px;" class="glyphicon glyphicon-edit text-blue"></i></a>
                                                        <a href="#delete<?php echo $row['temp_trans_id']; ?>" data-target="#delete<?php echo $row['temp_trans_id']; ?>" data-toggle="modal" style="color:#fff;" class="small-box-footer"><i style="font-size:20px;" class="glyphicon glyphicon-trash text-red"></i></a>
                                                    </td>
                                                    <td>

                                                        <a href="#discount<?php echo $row['temp_trans_id']; ?>" data-target="#discount<?php echo $row['temp_trans_id']; ?>" data-toggle="modal" style="color:#fff;" class="small-box-footer"><i style="font-size:20px;" class="glyphicon glyphicon-record text-red"></i>

                                                            <?php
                                                            $discountType = $row['discount_type'];
                                                            $amount = $row['amount'];

                                                            if ($discountType == "Percentage" && $amount != "") {
                                                                echo '<h6 style="color:#3c8dbc"><b> ' . $amount . ' % Discount</b></h6>';
                                                            } else if ($amount != "" && $discountType == "Amount") {
                                                                echo '<h6 style="color:#3c8dbc"><b>Discount of K ' . $amount . '</b></h6>';
                                                            } else if ($amount != "" && $discountType == "Promotion"){
                                                                echo '<h6 style="color:#3c8dbc"><b>K'.$amount.' promotion</b></h6>';
                                                            }
                                                            ?>
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <?php
                                                            if (!empty($composites)) {
                                                                echo '<h6 style="color:#3c8dbc"><b> -' . $composites . ' %</b></h6>';
                                                            } else{
                                                                echo 'N/A';
                                                            }
                                                        ?>
                                                    </td>
                                                </tr>



                                                <div id="updateordinance<?php echo $row['temp_trans_id']; ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content" style="height:auto">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title">Update Sales Details</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form autocomplete="off" class="form-horizontal" method="post" action="transaction_update.php" enctype='multipart/form-data'>
                                                                    <input type="hidden" class="form-control" name="cid" value="<?php echo $cid; ?>" required>
                                                                    <input type="hidden" class="form-control" id="price" name="id" value="<?php echo $row['temp_trans_id']; ?>" required>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-lg-3" for="price">Qty</label>
                                                                        <div class="col-lg-9">
                                                                            <input type="text" class="form-control" id="price" name="qty" value="<?php echo $row['qty']; ?>" required>
                                                                        </div>
                                                                    </div>


                                                            </div><br>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                    <!--end of modal-dialog-->
                                                </div>

                                                <div id="variants<?php echo $row['prod_id']; ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content" style="height:auto">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title">Choose Variants For <?php echo $row['prod_name']; echo $hey; ?>.</h4>
                                                            </div>



                                                            <div class="modal-body">
                                                                <form autocomplete="off" class="form-horizontal" method="post" action="#modal" enctype='multipart/form-data'>
                                                                    <?php
                                                                    
                                                                        // First, Check total qty of product
                                                                        $qty_query = mysqli_query($con, "SELECT prod_qty FROM product WHERE prod_id = '$product_id' ");
                                                                        $qty_row = mysqli_fetch_array($qty_query);
                                                                        $total_qty = $qty_row['prod_qty'];
                                                                     
                                                                      $res = mysqli_query($con, "SELECT * FROM product_variants WHERE prod_id = '$product_id' ") or die("Error: " . mysqli_error($con));
                                                                      if (mysqli_num_rows($res) > 0) {
                                                                        while ($rowvarr = mysqli_fetch_array($res)) {
                                                                            $color = $rowvarr['color'];
                                                                            $size = $rowvarr['size'];
                                                                            $total_variant_qty = $rowvarr['qty'];
                                                                            $variant_id = $rowvarr['id'];
                                                                    ?>
                                                                            <div class="bg-dark" style="border-radius: 9px; border: 3px solid #4EC5F1;">
                                                                                <div style="padding-left:20px;">
                                                                                    <div class="row">
                                                                                        <div class="col-md-6"> Color: </div>
                                                                                        <div class="col-md-6"> <?php echo $color ?> </div>
                                                                                        <input type="hidden" name="variant_color[]" />
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-6"> Size: </div>
                                                                                        <div class="col-md-6"> <?php echo $size ?> </div>
                                                                                        <input type="hidden" name="variant_size[]" />
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-md-6"> Quantity: </div>
                                                                                        <div class="col-md-6">
                                                                                        <input type="number" name="variant_qty[]" placeholder="xxx" style="width:75px; border-radius:10px; text-align:center " maxlength="1" />
                                                                                            out of <b> <?php echo $selectedQty; ?></b> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div><br>
                                                                            <input type="hidden" name="variant_id[]" value="<?php echo $variant_id?>" />
                                                                            

                                                                    <?php }
                                                                      } else echo "N/A";
                                                                    ?>

                                                                    <br>
                                                                    <div class="modal-footer">
                                                                        <!-- Get hidden data - $qty and $name(product ID) -->
                                                                        <input type="hidden" name="qty" value="<?php echo $selectedQty; ?>" />
                                                                        <input type="hidden" name="prod_name" value="<?php echo $product_id ?>" />
                                                                        <input type="submit" class="btn btn-primary" name="addtocart_modal" value="Apply" />
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    </div>
                                                            </div>
                                                                </form>
                                                        </div>

                                                    </div>
                                                    <!--end of modal-dialog-->
                                                </div>

                                                <div id="discount<?php echo $row['temp_trans_id']; ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content" style="height:auto">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title">Apply Discount on <?php echo $row['prod_name']; ?>.</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form autocomplete="off" class="form-horizontal" method="post" action="add-discount.php" enctype='multipart/form-data'>
                                                                    <p style=" color: black">Enter Admin / Supervisor password to Authorize </p>
                                                                    <input type="password" class="form-control" id="price" name="password" required autocomplete="off">
                                                                    <input type="hidden" class="form-control" id="price" name="cid" value="<?php echo $row['temp_trans_id']; ?>" required autocomplete="off">
                                                                    <p style=" color: black">Enter Value.</p>
                                                                    <input class="form-control" id="price" name="amount" required autocomplete="off">
                                                                    <label for="date">Discount Type</label>
                                                                    <select class="form-control" name="discount_type" tabindex="1">
                                                                        <option value="Amount">Amount</option>
                                                                        <option value="Percentage">Percentage</option>
                                                                    </select>
                                                            </div><br>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-primary">Apply Discount</button>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                    <!--end of modal-dialog-->
                                                </div>

                                                <div id="delete<?php echo $row['temp_trans_id']; ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content" style="height:auto">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title">Delete Item</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form autocomplete="off" class="form-horizontal" method="post" action="transaction_del.php" enctype='multipart/form-data'>
                                                                    <p>Enter password to Delete </p>
                                                                    <input type="hidden" class="form-control" name="cid" value="<?php echo $cid; ?>" required>
                                                                    <input type="hidden" class="form-control" id="price" name="id" value="<?php echo $row['temp_trans_id']; ?>" required>
                                                                    <input type="password" class="form-control" id="price" name="password" placeholder="Enter Password to Delete" required autocomplete="off">
                                                            </div><br>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-primary">Delete</button>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                </div>
                            </div>
                            <!--end of modal-->
                        <?php   //var_dump($composite_names);
                                composite_hits($temp_prod_id, $composite_names);
                                } 
                        ?>
                        </tbody>
                        </table>
                        </div><!-- /.box-body -->
                    </div>



                    </form>
            </div><!-- /.box-body -->
            <?php   require_once('composite_checker.php'); 
                    // composite_hits($temp_prod_id);
            ?>

            <?php
                // Check for products in composites and show so called hints...
                function composite_hits($prod_d, $composite_names) {
                    global $con, $id;

                    $query_proname1 = mysqli_query($con, "SELECT prod_name FROM product WHERE prod_id = $prod_d ")or die(mysqli_error($con));
                    $row_proname1 = mysqli_fetch_array($query_proname1);
                    $main_prod_name = $row_proname1['prod_name'];
                       
                    $query_comp = mysqli_query($con, "SELECT composite_name FROM product_composites WHERE prod_id = $prod_d GROUP BY composite_name ")or die(mysqli_error($con));
                    while($row_comp = mysqli_fetch_array($query_comp)){
                        $comp_name = $row_comp['composite_name'];


                        if ($composite_names <> $comp_name) {
                            $query_pro11 = mysqli_query($con, "SELECT product_composites.composite_name, product_composites.prod_id, product_composites.discount, product.prod_name
                                        FROM product_composites INNER JOIN product ON product.prod_id=product_composites.prod_id
                                        WHERE product_composites.composite_name = '$comp_name' ")or die("Error 1.0 ".mysqli_error($con));
                            if(mysqli_num_rows($query_pro11) > 0){
                                while($row_pro11 = mysqli_fetch_array($query_pro11)){
                                    $discount = $row_pro11['discount'];
                                    $composite_name = $row_pro11['composite_name'];
                                    $prod_name[] = $row_pro11['prod_name'];
                                    
                                    $prod_names = implode(", 1 ", $prod_name);
                                } 
                                echo "<div class='text-warning'> Buy <b> 1 $prod_names </b> on top <i>$main_prod_name </i> to get <b> $discount % </b> on a $composite_name composite </div> <br>";
                            }
                        }
                    }
                }
            ?>

        </div><!-- /.box -->
    </div><!-- /.col (right) -->

    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-body">
                <!-- Date range -->
                <form autocomplete="off" method="post" name="autoSumForm" action="sales_add.php">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="date">Total</label>
                                <input type="text" style="text-align:right" class="form-control" id="total" name="total" placeholder="Total" value="<?php echo $grand; ?>" onFocus="startCalc();" onBlur="stopCalc();" tabindex="5" readonly>
                            </div><!-- /.form group -->

                            <div class="form-group" hidden="ds">
                                <label for="date">Discount</label>

                                <input type="text" class="form-control text-right" id="discount" name="discount" value="0" tabindex="6" placeholder="Discount (Php)" onFocus="startCalc();" onBlur="stopCalc();">
                                <input type="hidden" class="form-control text-right" id="cid" name="cid" value="<?php echo $totalDiscount; ?>">
                            </div><!-- /.form group -->
                            
                            <div class="form-group">
                                <label for="date">Composites</label>
                                <?php
                                    $get_items_Q = mysqli_query($con, "SELECT composite_names FROM temp_trans 
                                                                WHERE branch_id='$branch' AND user_id='$userId' GROUP BY composite_names ")or die(mysqli_error($con));
                                    while($itemRows = mysqli_fetch_array($get_items_Q)){
                                        $composite_names = $itemRows['composite_names'];
                                ?>
                                    <div style="text-align:right" class="form-control">
                                        <?php echo $composite_names; ?>
                                    </div>
                            <?php } ?>
                            </div><!-- /.form group -->

                            <div class="form-group" hidden="">
                                <label for="date">Amount Due</label>

                                <input type="text" style="text-align:right" class="form-control" id="amount_due" name="amount_due_to_customer" placeholder="Amount Due" value="<?php echo number_format($grand, 2); ?>" readonly>

                            </div><!-- /.form group -->
                            <div class="form-group">
                                <label for="date">Discount</label>

                                <input type="text" style="text-align:right" class="form-control" id="amount_due" name="amount_due" placeholder="Amount Due" value="<?php echo number_format($totalDiscount, 2); ?>" readonly>

                            </div><!-- /.form group -->


                            <div class="form-group" id="tendered">
                                <label for="date">Cash Tendered</label><br>
                                <input type="text" style="text-align:right" class="form-control" onFocus="startCalc();" onBlur="stopCalc();" id="cash" name="tendered" placeholder="Cash Tend                                                    ered" autocomplete="off" required="">
                            </div><!-- /.form group -->
                            <div class="form-group" id="change">
                                <label for="date">Change</label><br>
                                <input type="text" style="text-align:right" class="form-control" id="changed" name="change" autocomplete="off" readonly="" placeholder="Change">
                            </div>

                            <a href="#split" data-target="#split" data-toggle="modal" style="color:#fff;" class="small-box-footer">
                                <p style=" color: #3c8dbc"><b>Split Payment</b></p>
                            </a>

                            <div class="form-group">
                                <label for="date">Mode Payment</label>

                                <select class="form-control select2" name="payment_mode_id" tabindex="1">
                                    <?php
                                    $query2 = mysqli_query($con, "select * from modes_of_payment_tb") or die(mysqli_error($con));
                                    while ($row = mysqli_fetch_array($query2)) {
                                    ?>
                                        <option value="<?php echo $row['payment_mode_id']; ?>"><?php echo $row['name']; ?></option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" class="form-control" name="cid" value="<?php echo $cid; ?>">
                            </div><!-- /.form group -->

                            <div class="form-group" id="tendered">
                                <label for="date">Select Customer</label><br>
                                <select class="form-control select2" name="cust_id" tabindex="1">
                                    <option value="none">-- Select Customer --</option>
                                    <?php
                                    include('../dist/includes/dbcon.php');
                                    $query2 = mysqli_query($con, "select * from customer ") or die(mysqli_error($con));
                                    while ($row = mysqli_fetch_array($query2)) {
                                    ?>
                                        <option value="<?php echo $row['cust_id']; ?>"><?php echo " ( " . $row['cust_first'] . ' - ' . $row['cust_last'] . " ) "; ?></option>
                                    <?php } ?>
                                </select>
                            </div><!-- /.form group -->
                            <a style=" color: blue" href="cust_new.php?type=cash" target="blank_"><b>Add New Customer</b></a><br></br>


                        </div>



                    </div>



                    <button class="btn btn-lg btn-block btn-primary" id="daterange-btn" name="cash" type="submit" tabindex="7">
                        Complete Sales
                    </button>
                    <button class="btn btn-lg btn-block" id="daterange-btn" type="reset" tabindex="8">
                        <a href="#cancel" data-target="#cancel" data-toggle="modal">Cancel Sale</a><br>
                        <a href="#hold" data-target="#hold" data-toggle="modal">Hold Transaction</a>
                    </button>

                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col (right) -->
               


    <div id="cancel" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content" style="height:auto">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Are you sure you want to cancel the Transactions ?</h4>
                </div>
                <div class="modal-body">
                    <form autocomplete="off" class="form-horizontal" method="post" action="cancel" enctype='multipart/form-data'>
                        <p>Enter Admin / Supervisor password to cancel.</p>

                        <input type="password" class="form-control" id="price" name="password" placeholder="Enter Password to Delete" required autocomplete="off">
                </div><br>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div id="hold" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content" style="height:auto">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Are you sure you want to Hold the Transaction ?</h4>
                </div>
                <div class="modal-body">
                    <form autocomplete="off" class="form-horizontal" method="post" action="cancel" enctype='multipart/form-data'>
                        <p>Enter Admin / Supervisor password to cancel.</p>

                        <input type="password" class="form-control" id="price" name="password" placeholder="Enter Password to Delete" required autocomplete="off">
                </div><br>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div id="split" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content" style="height:auto">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title"> Split Payment, Total Bill is <?php echo ' K ' . number_format($grand, 2); ?></h4>
                </div>
                <div class="modal-body">
                    <form autocomplete="off" class="form-horizontal" method="post" action="split-cash-sale.php" enctype='multipart/form-data'>

                        <table>
                            <tr id="service">
                                <td> <span>Mode Payment:</span>
                                </td>
                                <td>
                                    <select class="form-control select4" name="payment_mode_id_1" tabindex="1">
                                        <?php
                                        $query2 = mysqli_query($con, "SELECT * FROM `modes_of_payment_tb`  ORDER BY payment_mode_id ASC") or die(mysqli_error($con));
                                        while ($row = mysqli_fetch_array($query2)) {
                                        ?>
                                            <option value="<?php echo $row['payment_mode_id']; ?>"><?php echo $row['name']; ?></option>

                                        <?php } ?>
                                    </select>
                                </td>
                                <td hidden="">
                                    <input type="text" name="type[]" class="grand_total" value="'.$grand.'" autocomplete="off" readonly />
                                </td>
                                <td> <span>Amount:</span>
                                </td>
                                <td hidden="">
                                    <input type="text" name="grand_total" class="amount" value="<?php echo $grand; ?>" autocomplete="off" />
                                </td>
                                <td>
                                    <input type="text" name="split_bill_amount_1" class="amount" value="0" autocomplete="off" />
                                </td>
                            </tr>

                            <tr id="service">
                                <td> <span>Mode Payment:</span>
                                </td>
                                <td>
                                    <select class="form-control select4" name="payment_mode_id_2" tabindex="1">
                                        <?php
                                        $query2 = mysqli_query($con, "SELECT * FROM `modes_of_payment_tb`  ORDER BY payment_mode_id DESC") or die(mysqli_error($con));
                                        while ($row = mysqli_fetch_array($query2)) {
                                        ?>
                                            <option value="<?php echo $row['payment_mode_id']; ?>"><?php echo $row['name']; ?></option>

                                        <?php } ?>

                                    </select>
                                </td>
                                <td hidden="">
                                    <input type="text" name="type[]" class="grand_total" value="'.$grand.'" autocomplete="off" readonly />
                                </td>
                                <td> <span>Amount:</span>
                                </td>
                                <td>
                                    <input type="text" name="split_bill_amount_2" class="amount" value="0" autocomplete="off" />
                                </td>
                            </tr>


                        </table>


                </div><br>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Complete Sale</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div><!-- /.row -->




    <?php 
    // include('../dist/includes/variant_validator.php');

    // updateVariants("the", "boss"); 
    // echo "Hello"; ?>





    </section><!-- /.content -->
    </div><!-- /.container -->
    </div><!-- /.content-wrapper -->
    <?php include('../dist/includes/footer.php'); ?>
    </div><!-- ./wrapper -->
    <script>
        $(function() {

            $(".btn_delete").click(function() {
                var element = $(this);
                var id = element.attr("id");
                var dataString = 'id=' + id;
                if (confirm("Sure you want to delete this item?")) {
                    $.ajax({
                        type: "GET",
                        url: "temp_trans_del.php",
                        data: dataString,
                        success: function() {

                        }
                    });
                }
                return false;
            });
        });
    </script>

    <script type="text/javascript" src="autosum.js"></script>
    <!-- jQuery 2.1.4 -->
    <script src="../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="../dist/js/jquery.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="../plugins/select2/select2.full.min.js"></script>
    <!-- SlimScroll -->
    <script src="../plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>
    <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script>
        $(function() {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                x,
                "info": true,
                "autoWidth": false
            });
        });
    </script>

    <script>
        $(function() {
            //Initialize Select2 Elements
            $(".select2").select2();

            //Datemask dd/mm/yyyy
            $("#datemask").inputmask("dd/mm/yyyy", {
                "placeholder": "dd/mm/yyyy"
            });
            //Datemask2 mm/dd/yyyy
            $("#datemask2").inputmask("mm/dd/yyyy", {
                "placeholder": "mm/dd/yyyy"
            });
            //Money Euro
            $("[data-mask]").inputmask();

            //Date range picker
            $('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                format: 'MM/DD/YYYY h:mm A'
            });
            //Date range as a button
            $('#daterange-btn').daterangepicker({
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
            );

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();

            //Timepicker
            $(".timepicker").timepicker({
                showInputs: false
            });
        });
    </script>
</body>

</html>