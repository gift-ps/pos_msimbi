<?php

session_start();
$id = $_SESSION['id'];
include('../dist/includes/dbcon.php');

$customer_name = $_POST['customer_name'];
$orderNumber = (rand(50, 5000));
$branch = $_SESSION['branch'];
$waiter_id = $_POST['waiter_id'];
$layby_duration  = $_POST['layby_duration'];

// update the inventory draft sales record ... 

mysqli_query($con, "UPDATE draft_temp_trans SET order_no='$orderNumber',customer_name='$customer_name',layby_duration='$layby_duration',waiter_id='$waiter_id' where order_no='0'") or die(mysqli_error($con));

$query = mysqli_query($con, "select * from draft_temp_trans where order_no='$orderNumber' ")or die(mysqli_error($con));

while ($row = mysqli_fetch_array($query)) {

    $pid = $row['prod_id'];
    $qty = $row['qty'];

    $BatchQuery = mysqli_query($con, "SELECT * FROM `batches_tb` WHERE batch_id = (SELECT MIN(batch_id) from batches_tb WHERE prod_id='$pid' AND qty >0 ) GROUP BY prod_id ")or die(mysqli_error($con));
    $batchRows = mysqli_fetch_array($BatchQuery);
    $batchCostPrice = $batchRows['buy_price'];
    $batchID = $batchRows['batch_id'];

    // update the stock quantities when the draft is saved.. 
    mysqli_query($con, "UPDATE product SET prod_qty=prod_qty-'$qty' where prod_id='$pid' and branch_id='$branch'") or die(mysqli_error($con));

    // update the  batches quantities.. 

    mysqli_query($con, "UPDATE batches_tb SET qty=qty-'$qty' where batch_id='$batchID'") or die(mysqli_error($con));
}

echo "<script type='text/javascript'>alert(' Draft Record Saved Successfully !!..');</script>";
echo "<script>document.location='escpos-php-development/example/interface/incomplete-draft-reciept.php?orderno=$orderNumber'</script>";
?>