<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InvoiceGenerator
 *
 * @author choolwe
 */
class InvoiceGenerator {

    private $invoiceNo;
    public $VatAmount;

    public function setInvoice($invoice_no) {
         $this->invoiceNo = $invoice_no;               
    }

    public function getInvoice() {        
         return $this->invoiceNo;
    }
    
    
    public function setVatAmount($amount) {
         $this->VatAmount = $amount;               
    }

    public function getVATTotal() {        
         return $this->VatAmount;
    }

    public function genetrateInvoice($con) {

        //$query = mysqli_query($con, "select * from sales_details natural join product where sales_id='$sales_id' AND user_id='$user_id' ")or die(mysqli_error($con));

        $query = mysqli_query($con, "select * from sales_details natural join product")or die(mysqli_error($con));

        $AllData = array();
        while ($row = mysqli_fetch_array($query)) {
            $item_id = $row['prod_id'];
            $Description = $row['prod_name'];
            $barcode = $row['barcode'];
            $qty = $row['qty'];
            $price = $row['prod_sell_price'];
            $isTaxInclusive = "";

            // check tax status ..

            $tax_status = $row['vat_status'];
            if ($tax_status == "inclusive") {
                $isTaxInclusive = "true";
            } else {
                $isTaxInclusive = "false";
            }

            $TotalAmount = $qty * $price;

            $named_array = array(
                "ItemId" => $item_id, 'Description' => $Description
                , 'BarCode' => $barcode, 'Quantity' => $qty,
                'UnitPrice' => $price, 'Discount' => '0', 'TaxLabels' => array('A'), 'TotalAmount' => $TotalAmount, 'isTaxInclusive' => $isTaxInclusive, 'RRP' => '0'
            );

            array_push($AllData, $named_array);
        }

        $zraInvoiceData = array('PosVendor' => '',
            'PosSerialNumber' => '', 'IssueTime' => '', 'TransactionType' => '0', 'PaymentMode' => '0',
            'SaleType' => '0', 'LocalPurchaseOrder' => '0', 'Cashier' => 'john',
            'BuyerTPIN' => '0', 'BuyerName' => '', 'BuyerTaxAccountName' => '0',
            'BuyerTaxAccountName' => '', 'BuyerAddress' => '', 'BuyerTel' => '0',
            'OriginalInvoiceCode' => '0', 'BuyerTaxAccountName' => '0', 'OriginalInvoiceNumber' => '0', 'Memo' => '', 'Items' => $AllData);

        //call the getInvoiceData

        return json_encode($zraInvoiceData);
    }

    function getInvoiceData($invoiceDataJSON) {

        // call the DLL to send the invoice DATA...

        $JSONString = '{
	"TPIN":	"1007504387",
	"TaxpayerName":	"CHOOLWE NGANDU",
	"BuyerTPIN":	"0",
	"BuyerName":	"",
	"Addreess":	"",
	"ESDTime":	"20200324071616",
	"TerminalID":	"010300000087",
	"InvoiceCode":	"000200110000",
	"InvoiceNumber":	"00010766",
	"FiscalCode":	"02780224609234530101",
	"TalkTime":	"",
	"Operator":	"",
	"TaxItems":	[{
			"TaxLabel":	"A",
			"CategoryName":	"STANDARD RATED",
			"Rate":	0.16,
			"TaxAmount":	73.38
		}],
	"TotalAmount":	532,
	"VerificationUrl":	"http://efd.zra.org.zm:8097/search/invoiceInspection/invoiceVerification?x=%7B%22INV%22%3A%7B%22IC%22%3A%22000200110000%22%2C%22IN%22%3A%2200010766%22%2C%22TPIN%22%3A%221007504387%22%2C%22MACH%22%3A%22010300000087%22%2C%22DATE%22%3A%2220200324071616%22%2C%22AMT%22%3A%220000000532.00%22%2C%22FSC%22%3A%2202780224609234530101%22%7D%7D",
	"VerificationQRCode":	"R0lGODdhHQEdAbMAAAAAAAAAqgCqAACqqqoAAKoAqqqqAKqqqlVVVVVV/1X/VVX///9VVf9V////Vf///ywAAAAAHQEdAQAE/xDISau99+nNe/UYAIbUmHVo+nwqSrZr2ZJ0CNfYje98T8MmEec13AVZwBQySVQtXb6aLjqNWq9NpmyTjeGO22RQrOmOZ9gcmrpOu9NkrwQ8oZ+4tnixTjbv521vgE5WVYKHPHFPXYl/FnqOinmEfJSIho2WiJOQZ3ibmVpffWFyQp+DSqWena2Lq5GtdqeyZaWgPZKjYq+Vtr5QwKy1uqmmhsSoj8nHjrh+mtCqwL3Gw9fMyLGa2dJ61c/es0/aytjmgdnFtM3czOKk1OFS8fS8t/jC29PW6u7o/4gxMrIvmDFy7ziZCmWQ3bCBagIuzOej3C97F5cpg9gPYEZ/G/8p5kpHMqS8iPzYcRwpkeHKlRbb+XtJUOJDhd7A6ayZcpzInShlNjwnEGehkj2d/YTlceLJikid3skIdKrQm/qS0dyVtKXRoFitXonp8itGoQjvgQUpVaOojh9diVwnK21ck2LN/ihoFwg8rTz9Mr0LCRxduVkJt62qMrBhxFbZLh48NOrhpjEvd6Wql/FjzPXcSp6cuLLN0ERNF6bseenTvU37YqLcLXDmgpqvWkYdtWxQ33BJn23Nta3PeXVZ28bN13FnvbPZxH4z63i45KUZpiZ+8Plv5W7CYqmudJ7s3XhFb3ed93vxvoLEjy3f3bzC3NYdMp/+3nv74PE1Fx7/fY3Zt5ZaAC4HGn/D3QdZgXDst5lw6knYEHygRJdgaamtcx6CEI5noXwKKqahfunNFw14wXUY2odvhaiiib0dddqK2WUYCIYcjvgibbyl6FyPDLI04Yknznghiyge+SCMgr2mJJTcVbhgSjxesiOT+D1pnnhgCljfmCGGWaSZin2W5lwEGkidmGXCiSZabJ4p55125rmmm8/MqSadr/lZ555kChoonoTyuYmhhSIKaKN6PhpnpH9Wyp6iEVI6qKSMTppop6A6WqWSo9VIJI1CFncbjpBypuqW7rVa6qzfbLqqq1CB6OGmpML6n6e0Bhuji1dS+BeqWPJqY4ww+Sjs/7N/3mqsgzcme2iqkfkqWrTPduuotAHq+uO1uMLGLHTOzvQmtuaq0yy7+W0lK3a/hvspnFOuq26sMvpnpHHpRlmvvpziu+y0BxbFb37x+kvsuVISbGnE0pWrXUILt3kpcNsCCeKoQ8oKMr+9ApYxvBr3d7LCAw+oKbm6fewwxmRSaau2E8+b8q/kFUuWx9a23GKQ5eZms8oiM8lxz8gKDOyuJAPtNNNTB7z0tzt3TLHO4vqM86W1bd2kajEHrXXSOVYsKbddN50k2DT3e2rBMq9Mt9nvFh0w1fLKLTSwWT5NINV8o+tVwksiHjhjhSN9tuCRNp5tepKPzfi4kzfdd//lm2Md+eCgs+o35IkefdZ6UYvNcOj3Hr7h30iyXvbaUl9cbOeUZ1056lzrnbvou5uaeeLDBx478KJuXLvSUof9+uhQD81uTsynzXPyr1YLcLfGy348xLk6+bvZoaLcNvFjS9b9+E6bLqL21Nudc7vgUlvq+r6Dn37W8T+/uLIws5348ics/G1Pf9H718/stjrzDbB9ysse3gzHsnbNL3js+1/qitc82dkLgo5zngUBWDMSPu5tYjNgAhuGOBb2rlYhxN71Mli9v6kQc6MTIPryVourvWyGBNSgDbnkpRSWaIcU7GHIAAc3+DWxdRPq3/4cODspCtFK+usblHgHPc//lY6IdQMi+r6HxLm5MIcdvN0T70Y2KSaQbQQknO6K2CAJVs11J7TQ5bTXQM3Z74BllN4EN4jFO0YxiQZ0Iw7hCEg5UvGN/roi52aGPPYZLY1+lB8Yg7g8mJ1xkoQk47HaiMkqInKTgHQfFzlWJUdysoXCm+IDeZjIJC7Qf0tspfc8+MdbypJsGMxkCT0ZSxTCbo6W5CUs8Ugvy53Sel302uceuTdtNVOEqrTmIp85PGmt8ohb1GYnwlnJYYkzi9wspDf1eLCHgVCQwKTjLyW5TU0u84HftCM5yfcgbHYSl3PLl+r6SbRAFnKfYySoybrJznsiDG0M5WNDT2c1EzYz/5s/pB8OudglkOxRmPO0qDwvGcAjihKV61xoNEFK0heS0Zckcmgw4ylRlc7UnRccaUHnN8qbVtSMCn3l81pKuneGtKRGBCoNodnRfQ0Tonl0olK/qDhw3kyqT/0lKIeoNh5yNKgUMqYziYnVlcKQq1kVqBi/Os4IApSDZJ3lGleITIsdzKsT1arBqqrPq8p1oG0l5MjquNa8XrOuBoVpQWMKz60+zj5iLZ85y0nTQ8Z1dpH1oikv68qwyvCobx0rPHNa00FCtX6c3SUVSTtZfpaWjTcMY1RZWsyNfg2pLs1rbC37QoT6kLbMtK3oWItZ3aK0tmqcqmenCVy38c+kOv8NrBitaMKSNbe4w10jZNnSU2Vm9aRJpWhyc5tdwNrVZU5V5zlpB02ettOw1VxtJAnmT+XS8o/3RW9l2QhJ8z5UgXHzbTrpWSTrbpa8pjUrXinbWcnuV8BdxW8NRYvGCOMWtAft5WdR29jn+jefQ/UwYZkYUAk7lLhsXY2F4RpaX34Qxett8IbR49zyHrOstVzUjGX7UuRe152fVC1Ic6yljHZ3qSwuah//+9GzUti9dnSxW/WK5AynVr76PfKQqwtdPOpSuo9Fpfu0mMtSEpiq4m1tKrvs2jhqdskv1jCOuRzlWH5ZxTfmLYbJ7DgIt3ivaTbkmi+MKdzhWb0MFrH/RsGsyOBm9IzbTSaPo5tgHfY3xI6GInsLHWb7yrnNBrV0PTGtZwdzus9m/nRCbQxgRpuY1LCl86nnKtxDX9HAl6awYlnN5/AJmsjvQ2d4qSzUCn9XyEyesqiZu+lWJ3jJdC0wJd08vfmqbdd6xjVilw3qOw86yWo9BFGlfO3bpjXV9hxxkyGtbDbzF9DOTixio43mTh/by+1O92yd7NNJ8/qfJPb1bpvNbkL79de2rHWoAc7YETLVzvmebsLPt2AEmhvRmg43hxUsMe2i+941BnWQKR7xogJ7sPuuOMIZqPCRqxnlKVYiy/Fd4hhWGaeyDjYp23vXj++b3KNd4kwx/1pvAzMyzp6eebYBnl+J1xyqH5xTwRG86pATfNQBTznD4W30gyO9wzffqap/+20lfzbqmv26lYtc6rO/uk8xjjuYp651vrJ956edt6J1OEot413c8BYr30UqbbvDPdGIPzTdoRzarl+44bemL7JlTHIdF77x73001yeMdkmLHKy4gPzE8QlfVxv86Yvu9tinbdRem12aP1b4ui9O70aut+89t7maN37RnOe69rgPJda9TXRBA9m7Zs2sv6mt+hPnfsRNfbDPGb/n1Qt/+bb/t3+b/vq2n8/UrMd26wlfbFaSH7ve3/2Oy7912C8349bOc6yTTnVgN5zymlY+/AXLev8YVz7oD2dktJZp4/d4yGd+Buh+Ukdj8wdy2Zd+BZh3VXZ07VdpQodsjNRvxQZti3VxKld8i+d7PmZUIJZ1fjdmz6drHVhtuveA70ZZZ9ZsflZ9K6aC37d3AyZmTOd1OShTP5UpsTdnWTaA+Xd7EyZ69EeDQLhwQ1iDg3dq4nd8WHZ3Gxdp54VWa1doURhfV2hs8VaCoadoLseEVJh20xd/X2hcyCGGbKh9ZchsM+h63HZgBjKGjsdpW6iA26YjHuVuMReHaMh9hzV3OJiECMWBN8hs+pdsCah4oCd52Nd7Pjhe3SdvLOh0YGd1SBhoPWZ6EfVXVAdnHXdlpEdzvlb/e5L4idJHipbYhYIIVkSlbYmHfiRIe3FzfwxIgZP4bMgnigFYQZGnXJ0ycLqoiovIfR9YSp01h+LXaBCIfxHIccMWc6n4ZKrWjG+Xh8znZIYmgzwojBjohyPojH/XibbWf1MmhUVIgLzXhpoYjh8GceWWiKW4js+YiyWnji44e6aYhq9VfABZgX9HdqgYeGKnX9GHc6yYgaXnigdnjlf3fzqXkD9YiQxJiXZ4hr/ogpuYesY3juf3e+5IiwWpiJuHevUFdecHiPyndDKXZAp5bigZYCKoeZ5obw6XibXIjvKoiofYi0BpkqyWkeA4ixdZdAJZjbhok+fYkoWlUvZ3/5CcyIVV543x2I8ptYF6R4DF6IVPiIXEZ30OGJN1N5VMSYchyHPDFpbOd5VWh4ximXV/yIfNJ3BlNpOeR4tw2ZYSeF1hCIMp2I1AZ4MWd3kY14AmV3LUtZYryXmBmZRUCXw1CYrEFpHqB4d0yZENORqvmJcDN5jR15VqB4KRKXuTuYo6yY+UCYaPiZf1+JJlmXybGYuQiZmWt43UyF3ptIzHJZGkOYvqM3r/VZJNGWgnd4FCCIDGSJOXJYu4OXy9iZQuGYMQGZzTmXmfd5Pk2JFdWZ2Tp5vXOY/ZWZweeZwtWILEyZnC6ZyaKVvbeZJPuWWNuHJ/pof/SITlGJQOWf9nl3iYR/mCWmlTUumfp4lywbeE2pifgFmUb4mfw5hz7Nl598iTYSegvimQTWagcRmhD2mGLkmRhjlu+smIPgmJhRmgL9eYfbWRB4phoemgDAhe8amg+2ecOsiXLrqC9TaI5KmBNIqY7xmiOrpfc7mLJHmAHXqW3lakJeqaHymkhHiTPgp+04iPbkmkFQmasCh3+wlriAiBEtqK70eHonmELTeSrOmkGop6roeealifJ0qGhCmm2IiQtjiF1ticb4eCammjfUqgp3eKd9qFbkqJ1Mmlw2mlZlmjEzmoYxqDh5qX3Gmm9jmeR0qh70iP/DagvMmYf6qPnaqTC1ipT+r/iBIZql5JqZlqqT9pm6Iao0P6oCyKnWWXknPKo2gJnq/qftQ3mIsZiojaqvZ4qZdpqil6pRmKhun5m8cKbtcIj1joq+jolEp4q7CKlY66rMrakzmJk19alWSJq796mKi6pPDprUiKo59Jlao5ptpKrdJ6feynrtGJpdg6lJRWlV+ZqnbKlXcZrY9opEyap7AWqV0KqCoJr1vZrOSqqIlpeIVqgTjakb64qAOpjJtpsAC6jyb6owgIsFH6nJpaprP6qR4YlxS7h/y5dAyaq4Z6o3Cqr44ZqA/rrFWanB75rSAqsigqpslqsgvqp5hHrwJbkXzaW8jZs+Wajg47h8x4/608W6uO+rMx61jESKs6G6vfWaG8CKXyGbP7Kng7WLS8irKzWZr9ebXIOrO7upojGbZnS6p0146eybaVSbcH+63dCbXRGKQ72rTPOrL5uLAbu67uaVUlC5PZeovc2qYjmJvaibgFm42La6Esq26U+7Ir6qUqy7le+6NZuZMHG4xIK54u27VB+LeYaq9xGqYEiaZxS6ixa5mgy7cy2qh4KrYbmaalO7kQm6XMKZNC262667lf+3MZm7kN2rJyuKeZ+a5ta5U227sEm6OWS6x9K451y7yDG7TWKqloO6zVyJ6oKkJj6KMaC6pvJk/kC613e66QWq8tar7hK6cnqLywef++Ixqxpbqpcmu70Hmzo2u3c8u3H2uRX1K/RIm5pnvAcHu42ze2dyesclqxuCu7aVuvFvyfVoi9xeuFBYyvGby7UyucaTmj47utIzy0wlueEoyuZbuG/yulZ8q1JGq9l/u+SoqhSZpeR1u9kzqx3Eq/Z1mn0ZueS0uzTguzDeuvCxmStNKi8eupTeq7y+nE9QfFsyLFqiq9OZu8ZMuo4Qq/o+iA46q3u3myKyu1NmyeE+xxhmeubWy3S6xv/iedS0i1WSjHPUu6/tjHadyPaudYt3vGHvqGdSmN1HvCsdmujuu9vPvHbAzIiavDxwuRZKdyCwzC6TqWIBnGZNrFFjz/pW/LtBMIowLIvUDLqkk7ydmrxHAsf0bcxJSpsZlswqUswGXXrrIavbbsvHbcw4lqmHrMy26ny2wpqDbMyBqntRhZiHaJzKact0b4p5gyxoJLxTqXzJzcn7f7wX+5vMo5p9d8rinrzdXMwnUIvNncwoPcud1MzZCsxtfBzsXqxe+cu4SbvmCcxUL8zOAbsv6LwXj8vVEblTFcxTUrm9dLm8H8u8DZzwj8z34ZuPccrwmruBEN0OK7tcNqzALt0Jjowqvcv3cMro9rkH2YhKO8fjfsyJJr0Kdrv9/ozy1Mpe78r9WrtsYbyjWYoOcsxmSMatYsv0Zd1Oe5vsSsohm9rM3ArNFui7OvK5QFjdP8ysDqjLeoudOtPKqvqaUCvbeCzMNsarbtPNG+jMpgetQ3zalYt7MzzawD3Z5rTcJLraYvLNcDC9dgbayFy8Tza7Rmfc8cjLGfK6IcfbEUrZfuJteFLcPyHNlxrdbcWNPx9thtvcLd6sDreZq2GskIzbDN64Q6nYw0a50zeodoHc8vHYjdW9FXqpRvStI2zdodHG5TzagUjL60itmsHQEAOw=="
}';

        $json = json_decode($JSONString, true);
        $this->setInvoice($json['InvoiceNumber']);
        $this->setVatAmount($json['TotalAmount']);
        
        return $this->getVATTotal();
    }

}
