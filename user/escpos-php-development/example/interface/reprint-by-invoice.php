<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
session_start();
?>
<?php
/* Change to the correct path if you copy this example! */
require __DIR__ . '/../../autoload.php';

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

include('../../../../dist/includes/dbcon.php');

try {
    // Enter the share name for your USB printer here
    //$connector = null;

    $connector = new WindowsPrintConnector("chescopos1");

    $queryb = mysqli_query($con, "select * from branch")or die(mysqli_error($con));
    $rowb = mysqli_fetch_array($queryb);
    $reciept_footer_text = $rowb['reciept_footer_text'];

    $branch = $_SESSION['branch'];
    $invoiceNumber = $_GET['invoice'];
    
    $query = mysqli_query($con, "SELECT * FROM `sales` WHERE invoice_no ='$invoiceNumber'  ")or die(mysqli_error($con));

    $row = mysqli_fetch_array($query);

    $sales_id = $row['sales_id'];
    $sid = $row['sales_id'];
    $due = $row['amount_due'];
    $discount = $row['discount'];
    $grandtotal = $due - $discount;
    $tendered = $row['cash_tendered'];
    $change = $row['cash_change'];
    $mode_payment_id = $row['modeofpayment'];

    // update the invoice details.. 
    $invoice = mysqli_query($con, "select * from sales_details natural join product where sales_id='$sid'")or die(mysqli_error($con));
    $rows = mysqli_fetch_array($invoice);
    $ordNo = $rows['order_no'];

    /* Print a "Hello world" receipt" */
    $printer = new Printer($connector);

    // Add image logo for the client..  

    /*
      $tux = EscposImage::load("meat-logo.png");
      $printer->setJustification(Printer::JUSTIFY_CENTER);
      $printer->bitImage($tux);

     */
    $printer->text($rowb['branch_name'] . ", " . "\n");
    $printer->text($rowb['branch_address'] . ", " . "\n");
    $printer->text($rowb['branch_contact'] . "\n\n");

    // invoice info...

    $printer->text("Items Bought for Invoice # 000" . $invoiceNumber . "\n\n");
    $printer->text("......................................\n");

    $query1 = mysqli_query($con, "select * from modes_of_payment_tb where payment_mode_id='$mode_payment_id'")or die(mysqli_error($con));

    $row1 = mysqli_fetch_array($query1);

    $query = mysqli_query($con, "select * from sales_details natural join product where sales_id='$sid' ")or die(mysqli_error($con));
    $grand = 0;
    $finalTotal = 0;
    $order_no = 0;
    $vatFinalTotal = 0;
    $discountTotal = 0;
    $vatFree = 0;

    while ($row = mysqli_fetch_array($query)) {

        $discount = $row['discount'];
        $discount_type = $row['discount_type'];

        if ($discount_type == "Percentage") {
            $discountTotal += ($discount / 100);
            // $newPrice = $price - $computedPrice;
        } else {
            $discountTotal += $discount;
        }

        if ($row['vat_status'] != "free") {
            $vatFinalTotal += ($row['qty'] * $row['prod_sell_price'] ) - $discountTotal;
        } else if ($row['vat_status'] == "free") {
            $vatFree += ($row['qty'] * $row['prod_sell_price'] );
        }

        $finalTotal += ($row['qty'] * $row['prod_sell_price']) - $discountTotal;
        $order_no = $row['order_no'];
        $prodName = $row['prod_name'];
        $price = $row['prod_sell_price'];
        $total = $row['qty'] * $row['prod_sell_price'];
        $grand = $grand + $total;

        $printer->text($row['qty'] . "  " . substr($prodName, 0, 30) . " - @ K " . $price . " =K " . $total . "\n");
    }

    $total = number_format($grand - $discount, 2);
    $printer->text("......................................\n");

    $amountLessVat = ($vatFinalTotal / 1.16);
    $vatValue = $vatFinalTotal - $amountLessVat;
    $totalExlVAT = $vatFinalTotal - $vatValue;

    if ($discountTotal == "") {
        $printer->text("Discount K " . number_format(0.00, 2) . "\n");
    } else {
        $printer->text("Discount K " . number_format($discountTotal, 2) . "\n");
    }

    $printer->text("Total Excl VAT K " . number_format($totalExlVAT, 2) . "\n");
    $printer->text("VAT @ 16 % : " . number_format($vatValue, 2) . "\n");
    $printer->text("VAT Free : " . number_format($vatFree, 2) . "\n");
    $printer->text("Total Inc Vat K " . number_format($finalTotal, 2) . "\n");

    $printer->text("Cash Tendered " . number_format($tendered, 2) . "\n");
    $printer->text("Change " . number_format($change, 2) . "\n");
    $printer->text("Order No " . $order_no . "\n");

    $query = mysqli_query($con, "select * from user where user_id='$id'")or die(mysqli_error($con));
    $row = mysqli_fetch_array($query);

    $printer->text("......................................\n");

    $printer->text(date("M d, Y") . " " . date("h:i A") . "\n");

    $printer->text($reciept_footer_text . "\n");

    $printer->cut();

    /* Close printer */
    $printer->close();
    
    // errase the invoice after printing the credit note...
    
    

    echo "<script>document.location='../../../input-invoiceno.php'</script>";
} catch (Exception $e) {
    echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
}
