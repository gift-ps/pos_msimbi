<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
session_start();
?>
<?php
/* Change to the correct path if you copy this example! */
require __DIR__ . '/../../autoload.php';

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

include('../../../../dist/includes/dbcon.php');

try {
    // Enter the share name for your USB printer here
    //$connector = null;

    $connector = new WindowsPrintConnector("chescopos1");
    $orderNumber = $_GET['orderno'];
    $id = $_SESSION['id'];
    $queryb = mysqli_query($con, "select * from branch")or die(mysqli_error($con));
    $rowb = mysqli_fetch_array($queryb);
    $reciept_footer_text = $rowb['reciept_footer_text'];

    $branch = $_SESSION['branch'];
    $query = mysqli_query($con, "SELECT * FROM `sales` WHERE sales_id = (SELECT MAX(sales_id) FROM sales WHERE user_id='$id' ) AND user_id='$id'  ")or die(mysqli_error($con));

    $row = mysqli_fetch_array($query);

    $sales_id = $row['sales_id'];
    $sid = $row['sales_id'];
    $due = $row['amount_due'];
    $discount = $row['discount'];
    $grandtotal = $due - $discount;
    $tendered = $row['cash_tendered'];
    $change = $row['cash_change'];

    /* Print a "Hello world" receipt" */
    $printer = new Printer($connector);

    // Add image logo for the client..  

    /*
      $tux = EscposImage::load("meat-logo.png");
      $printer->setJustification(Printer::JUSTIFY_CENTER);
      $printer->bitImage($tux);
     */

    $printer->text($rowb['branch_name'] . ", " . "\n");
    $printer->text($rowb['branch_address'] . ", " . "\n");
    $printer->text($rowb['branch_contact'] . "\n\n");

    $query1 = mysqli_query($con, "select * from payment where sales_id='$sales_id'")or die(mysqli_error($con));

    $row1 = mysqli_fetch_array($query1);

    $query2 = mysqli_query($con, "select DATE_ADD(date, INTERVAL 3 MONTH) as expires_on,customer_name,temp_trans_id,prod_name,price AS prod_sell_price,SUM(qty) AS qty from draft_temp_trans natural join product where order_no='$orderNumber'  group by prod_name ")or die(mysqli_error($con));
    $query3 = mysqli_query($con, "select DATE_ADD(date, INTERVAL 3 MONTH) as expires_on,customer_name,temp_trans_id,prod_name,price AS prod_sell_price,SUM(qty) AS qty from draft_temp_trans natural join product where order_no='$orderNumber'  group by prod_name ")or die(mysqli_error($con));

    $grand = 0;
    $finalTotal = 0;
    $customers_ = mysqli_fetch_array($query3);
    $customerNames = $customers_['customer_name'];
    $exprires = $customers_['expires_on'];

    $printer->text("Transction Statement for : " . $customerNames . "  .\n");
//$row_1 = mysqli_fetch_array($query2);
    while ($customers = mysqli_fetch_array($query2)) {
        $temp_trans_id = $customers['temp_trans_id'];
        $prodName = $customers['prod_name'];
        $finalTotal += $customers['qty'] * $customers['prod_sell_price'];
        $price = number_format($customers['prod_sell_price'], 2);
        $total = $customers['qty'] * $customers['prod_sell_price'];
        $grand = $grand + $total;

        mysqli_query($con, "update draft_temp_trans set is_printed='1' where temp_trans_id='$temp_trans_id'")or die(mysqli_error($con));

        $printer->text($customers['qty'] . " " . $prodName . " - @ K " . $price . " =K " . $total . "\n");
    }

    $total = number_format($grand - $discount, 2);

    $printer->text("...........................................\n");

    $printer->text("Total Amount Due K " . number_format($finalTotal, 2) . "\n");
    $partPaymentsquery = mysqli_query($con, "SELECT SUM(amount) As amount FROM `part_payments_tb` INNER JOIN user ON user.user_id=part_payments_tb.user_id AND order_no='$orderNumber' GROUP BY order_no ")or die(mysqli_error($con));
    if (mysqli_num_rows($partPaymentsquery) > 0) {
        $partRows = mysqli_fetch_array($partPaymentsquery);
        $partpaymentAmount = $partRows['amount'];
    } else {
        $partpaymentAmount = 0;
    }


    $printer->text("Part Payment K " . number_format($partpaymentAmount, 2) . "\n");

    $Baldue = $finalTotal - $partpaymentAmount;

    $printer->text("Balance Due K " . number_format($Baldue, 2) . "\n");

    $query = mysqli_query($con, "select * from user where user_id='$id'")or die(mysqli_error($con));
    $row = mysqli_fetch_array($query);

    $printer->text("...........................................\n");

    $printer->text("Layby Expires On " . $exprires . "\n");

    $printer->text("Issued By User " . $row['name'] . "\n");

    $printer->text(date("M d, Y") . " " . date("h:i A") . "\n");

    $printer->text($reciept_footer_text . "\n");

    $printer->cut();

    /* Close printer */
    $printer->close();

    echo "<script>document.location='../../../draft-sale.php'</script>";
} catch (Exception $e) {
    echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
}
