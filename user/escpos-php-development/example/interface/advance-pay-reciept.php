<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
session_start();
?>
<?php
/* Change to the correct path if you copy this example! */
require __DIR__ . '/../../autoload.php';

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

include('../../../../dist/includes/dbcon.php');

try {
    // Enter the share name for your USB printer here
    //$connector = null;

    $connector = new WindowsPrintConnector("chescopos1");
    $orderNumber = $_GET['orderno'];
    $id = $_SESSION['id'];
    $queryb = mysqli_query($con, "select * from branch")or die(mysqli_error($con));
    $rowb = mysqli_fetch_array($queryb);
    $reciept_footer_text = $rowb['reciept_footer_text'];

    $branch = $_SESSION['branch'];

    // update the invoice details.. 
    $invoice = mysqli_query($con, "select * from sales_details natural join product where sales_id='$sid' AND user_id='$id'  ")or die(mysqli_error($con));
    $rows = mysqli_fetch_array($invoice);
    $ordNo = $rows['order_no'];

    mysqli_query($con, "INSERT INTO invoices_tb(order_no) 
                                        VALUES('$ordNo')")or die(mysqli_error($con));

    $invoice2 = mysqli_query($con, "SELECT MAX(id) AS id FROM invoices_tb")or die(mysqli_error($con));
    $rowss = mysqli_fetch_array($invoice2);
    $invoiceNo = $rowss['id'];

    /* Print a "Hello world" receipt" */
    $printer = new Printer($connector);

    // Add image logo for the client..  

    /*
      $tux = EscposImage::load("meat-logo.png");
      $printer->setJustification(Printer::JUSTIFY_CENTER);
      $printer->bitImage($tux);
     */

    $query = mysqli_query($con, "select prod_name,prod_sell_price,SUM(qty) AS qty,amount,customer_name from advance_payments_tb INNER JOIN product ON product.prod_id=advance_payments_tb.prod_id AND advance_payments_tb.order_no='$orderNumber'  group by prod_name ")or die(mysqli_error($con));
    $grand = 0;

    $query2 = mysqli_query($con, "select prod_name,prod_sell_price,SUM(qty) AS qty,amount,customer_name from advance_payments_tb INNER JOIN product ON product.prod_id=advance_payments_tb.prod_id AND advance_payments_tb.order_no='$orderNumber'  group by prod_name ")or die(mysqli_error($con));

    $row2 = mysqli_fetch_array($query2);
    $customer_name = $row2['customer_name'];

    $printer->text($rowb['branch_name'] . ", " . "\n");
    $printer->text($rowb['branch_address'] . ", " . "\n");
    $printer->text($rowb['branch_contact'] . "\n\n");

    // invoice info...

    $printer->text("Payment Reciept to " . $customer_name . "\n\n");
    $printer->text("...........................................\n");

    $query1 = mysqli_query($con, "select * from payment where sales_id='$sales_id'")or die(mysqli_error($con));

    $row1 = mysqli_fetch_array($query1);

    while ($row = mysqli_fetch_array($query)) {
        $prodName = $row['prod_name'];
        $price = number_format($row['prod_sell_price'], 2);
        $total = $row['qty'] * $row['prod_sell_price'];
        $finalTotal += $row['qty'] * $row['prod_sell_price'];
        $grand = $grand + $total;
        $advanceAmount = $row['amount'];

        $printer->text($row['qty'] . " " . $prodName . " - @ K " . $price . " =K " . $total . "\n");
    }

    $total = number_format($grand - $discount, 2);

    $printer->text("...........................................\n");

    $printer->text("Total Amount Due K " . number_format($finalTotal, 2) . "\n");
    $printer->text("Advance Amount Paid K " . number_format($advanceAmount, 2) . "\n");

    $query = mysqli_query($con, "select * from user where user_id='$id'")or die(mysqli_error($con));
    $row = mysqli_fetch_array($query);

    $printer->text("...........................................\n");

    $printer->text("Issued By User " . $row['name'] . "\n");

    $printer->text(date("M d, Y") . " " . date("h:i A") . "\n");

    $printer->text($reciept_footer_text . "\n");

    $printer->cut();

    /* Close printer */
    $printer->close();

    echo "<script>document.location='../../../advance-payment.php'</script>";
} catch (Exception $e) {
    echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
}
