<?php

session_start();
$id = $_SESSION['id'];
$branch = $_SESSION['branch'];

include('../dist/includes/dbcon.php');




if (isset($_POST['addtocart'])) {
    // Check if product has Variants set
    $prod_d = $_POST['prod_name'];
    $variantChecker = mysqli_query($con, "SELECT prod_id FROM product_variants WHERE prod_id='$prod_d'") or die(mysqli_error($con));
    $variantRows = mysqli_fetch_array($variantChecker);

    // Get name of product
    $pronameQuery = mysqli_query($con, "SELECT prod_name FROM product WHERE prod_id='$prod_d'") or die(mysqli_error($con));
    $pronameRow = mysqli_fetch_array($pronameQuery);

    if (mysqli_num_rows($variantChecker) > 0) {
?>
        <script>
            $(document).ready(function() {
                $("#variant<?php echo $prod_d; ?>").modal('show');
            });
        </script>

        <?php
        $selectedQty = $_POST['qty'];

        // Modal for the add variant form down here..
        ?>
        <div id="variant<?php echo $prod_d ?>" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content" style="height:auto">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Choose Variants For <?php echo $pronameRow['prod_name'];
                                                                    echo $hey; ?>.</h4>
                    </div>

                    <div class="modal-body">
                        <form autocomplete="off" class="form-horizontal" method="post" action="#modal" enctype='multipart/form-data'>
                            <?php

                            // First, Check total qty of product
                            $qty_query = mysqli_query($con, "SELECT prod_qty FROM product WHERE prod_id = '$prod_d' ") or die("Error: " . mysqli_error($con));
                            $qty_row = mysqli_fetch_array($qty_query);
                            $total_qty = $qty_row['prod_qty'];
                            // Now get all variants for particular prod..
                            $res = mysqli_query($con, "SELECT * FROM product_variants WHERE prod_id = '$prod_d' ") or die("Error: " . mysqli_error($con));
                            if (mysqli_num_rows($res) > 0) {
                                while ($rowvarr = mysqli_fetch_array($res)) {
                                    $color = $rowvarr['color'];
                                    $size = $rowvarr['size'];
                                    $total_variant_qty = $rowvarr['qty'];
                                    $variant_id = $rowvarr['id'];


                            ?>
                                    <div class="bg-dark" style="border-radius: 9px; border: 3px solid #4EC5F1;">
                                        <div style="padding-left:20px;">
                                            <div class="row">
                                                <div class="col-md-6"> Color: </div>
                                                <div class="col-md-6"> <?php echo $color ?> </div>
                                                <input type="hidden" name="variant_color[]" />
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6"> Size: </div>
                                                <div class="col-md-6"> <?php echo $size ?> </div>
                                                <input type="hidden" name="variant_size[]" />
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6"> Quantity: </div>
                                                <div class="col-md-6">
                                                    <input type="number" name="variant_qty[]" min="0" max="<?php echo $selectedQty ?>" placeholder="xxx" style="width:75px; border-radius:10px; text-align:center " maxlength="1" />
                                                    out of <b> <?php echo $selectedQty; ?></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>
                                    <input type="hidden" name="variant_id[]" value="<?php echo $variant_id ?>" />

                            <?php
                                }
                            } else echo "N/A";
                            ?>

                            <br>
                            <div class="modal-footer">
                                <!-- Get hidden data - $qty and $name(product ID) -->
                                <input type="hidden" name="qty" value="<?php echo $selectedQty; ?>" />
                                <input type="hidden" name="prod_name" value="<?php echo $prod_d ?>" />
                                <input type="submit" class="btn btn-primary" name="addtocart_modal" value="Apply" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                    </div>
                    </form>
                </div>

            </div>
            <!--end of modal-dialog-->
        </div>
<?php

        return;
    } else {
        save_data();
    }
}


//Get data from the variants modal
if (isset($_POST['addtocart_modal'])) {
    $prod_d = $_POST['prod_name'];
    $variant_qtys = $_POST['variant_qty'];
    $variant_ids = $_POST['variant_id'];
    $qty = $_POST['qty'];

    // $get Product Name
    $res1proName = mysqli_query($con, "SELECT prod_name FROM product WHERE prod_id = '$prod_d' ") or die("Error: " . mysqli_error($con));
    if (mysqli_num_rows($res1proName) > 0) {
        while ($rowvarr = mysqli_fetch_array($res1proName)) {
            $prod_name = $rowvarr['prod_name'];
        }
    }

    array_map(function ($variant_qty, $variant_id) {
        global $con, $cid, $branch, $prod_d, $prod_name;
        // First validate quantiy
        $qty = $_POST['qty'];

        $res = mysqli_query($con, "SELECT * FROM product_variants WHERE prod_id = '$prod_d' ") or die("Error: " . mysqli_error($con));
        if (mysqli_num_rows($res) > 0) {
            while ($rowvarr = mysqli_fetch_array($res)) {
                $color = $rowvarr['color'];
                $size = $rowvarr['size'];
                $total_variant_qty = $rowvarr['qty'];
            }
        }



        // Make sure total variant_qty is greater than what has been selected, not just less that the total products
        // Get the variant qty user selected for this particular add!!
        $queryck1 = mysqli_query($con, "SELECT variant_qty FROM draft_temp_trans_variants WHERE prod_id='$prod_d' AND variant_id = '$variant_id' ") or die("Error 2 " . mysqli_error($con));
        if (mysqli_num_rows($queryck1) > 0) {
            while ($rowk1 = mysqli_fetch_array($queryck1)) {
                $selected_variantQty = $rowk1["variant_qty"];
            }
        }
        // Get the total quantity of items user has selected for the whole transaction... And add it to the newly selected qty that has not yet been uploaded
        $tempDataQuery = mysqli_query($con, "SELECT qty FROM draft_temp_trans WHERE prod_id='$prod_d' AND branch_id='$branch'") or die("Error 1 " . mysqli_error($con));
        if (mysqli_num_rows($tempDataQuery) > 0) {
            while ($tempDataRow = mysqli_fetch_array($tempDataQuery)) {
                $selectedQty = $tempDataRow["qty"] + $qty;
            }
        }
        $total_selectedQty = $selectedQty;
        if (isset($selected_variantQty)) {
            $total_selectedQty = $selectedQty + $selected_variantQty;
        }
        // echo "<script>alert('$selectedQty and total_variant_qty $total_variant_qty and total_selectedQty $total_selectedQty ');</script>";

        if ($total_variant_qty <= $total_selectedQty) {
            echo "<script type='text/javascript'>alert('Error!! Quantity picked for size " . $size . " " . $color . " " . $prod_name . " is more than the total number of (" . $total_variant_qty . ") items remaining.');</script>";
            echo "<script>document.location='draft-order.php?cid=$cid'</script>";
        } else {
            $queryck = mysqli_query($con, "SELECT * FROM draft_temp_trans_variants WHERE prod_id='$prod_d' AND variant_id = '$variant_id' ") or die("Error 2 " . mysqli_error($con));
            $countck = mysqli_num_rows($queryck);
            if ($countck > 0) {
                $sql = "UPDATE draft_temp_trans_variants SET";
                if (!empty($variant_qty)) {
                    $sql .= " variant_qty = variant_qty+'$variant_qty',";
                }
                if (!empty($variant_id)) {
                    $sql .= " variant_id = '$variant_id',";
                }

                $sql = substr($sql, 0, strlen($sql) - 1) . " WHERE prod_id='$prod_d' AND variant_id = '$variant_id' ";

                mysqli_query($con, $sql) or die('An error occured : ' . mysqli_error($con));
            } else {
                mysqli_query($con, "INSERT INTO draft_temp_trans_variants (prod_id, variant_id, variant_qty) VALUES('$prod_d', '$variant_id', '$variant_qty')") or die("Error 4 " . mysqli_error($con));
            }
        }
    }, $variant_qtys, $variant_ids);

    echo "<script>document.location='draft-order.php?cid=$cid'</script>";

    save_data();
}



function save_data()
{

    global $con, $id, $branch;
    $cid = $_POST['cid'];

    $mode = "credit";

    $user_id = $_SESSION['id'];
    $barcode = $_POST['barcode'];


    if ($barcode != "") {
        $qty = "1";

        $checkBarcodeDetails = mysqli_query($con, "select prod_id,prod_qty,prod_name from product WHERE barcode='$barcode'") or die(mysqli_error($con));
        $countRows = mysqli_num_rows($checkBarcodeDetails);
        if ($countRows > 0) {

            $getBarcodeDetails = mysqli_query($con, "select prod_id,prod_qty,prod_name from product WHERE barcode='$barcode'") or die(mysqli_error($con));
            $barcodeRows = mysqli_fetch_array($getBarcodeDetails);
            $name = $barcodeRows['prod_name'];
            $prod_id = $barcodeRows['prod_id'];
            $noQtyInStock = $barcodeRows['prod_qty'];
            $prodName = $barcodeRows['prod_name'];

            if ($qty <= $noQtyInStock) {
                $query = mysqli_query($con, "select prod_sell_price,prod_id,rate from product INNER JOIN exchange_rates_tb ON exchange_rates_tb.exchange_id=product.currency_id AND prod_id='$prod_id'") or die(mysqli_error($con));
                $row = mysqli_fetch_array($query);

                if ($row['rate'] == 0) {
                    $price = $row['prod_sell_price'];
                } else {
                    $price = $row['prod_sell_price'] * $row['rate'];
                }

                //$price = $row['prod_sell_price'];

                $query1 = mysqli_query($con, "select * from draft_temp_trans where prod_id='$prod_id' and branch_id='$branch' AND order_no='0' ") or die(mysqli_error($con));
                $count = mysqli_num_rows($query1);

                $total = $price * $qty;

                if ($count > 0) {
                    mysqli_query($con, "update draft_temp_trans set qty=qty+'$qty',price=price+'$total' where prod_id='$prod_id' and branch_id='$branch' AND order_no='0' ") or die(mysqli_error($con));
                } else {
                    mysqli_query($con, "INSERT INTO draft_temp_trans(prod_id,qty,price,branch_id,user_id) VALUES('$prod_id','$qty','$price','$branch','$user_id')") or die(mysqli_error($con));
                }

                apply_discount_from_admin($con, $prod_id, $qty, $mode);

                echo "<script>document.location='draft-order.php?cid=$cid'</script>";
            } else {
                echo "<script type='text/javascript'>alert('Error !!, you cannot sell " . $qty . " items of " . $prodName . " because it is more than what is in stock !!! ');</script>";
                echo "<script>document.location='draft-order.php?cid=$cid'</script>";
            }
        } else {
            echo "<script type='text/javascript'>alert('Error !!, barcode " . $barcode . " is not found in the System,please search for product !!');</script>";
            echo "<script>document.location='draft-order.php?cid=$cid'</script>";
        }
    } else {
        $name = $_POST['prod_name'];
        $qty = $_POST['qty'];

        $qtyChecker = mysqli_query($con, "select prod_qty,prod_name from product WHERE prod_id='$name'") or die(mysqli_error($con));
        $qtyRows = mysqli_fetch_array($qtyChecker);
        $noQtyInStock = $qtyRows['prod_qty'];
        $prodName = $qtyRows['prod_name'];

        if ($qty <= $noQtyInStock) {
            $query = mysqli_query($con, "select prod_sell_price,prod_id,rate from product INNER JOIN exchange_rates_tb ON exchange_rates_tb.exchange_id=product.currency_id AND prod_id='$name'") or die(mysqli_error($con));
            $row = mysqli_fetch_array($query);

            if ($row['rate'] == 0) {
                $price = $row['prod_sell_price'];
            } else {
                $price = $row['prod_sell_price'] * $row['rate'];
            }

            //$price = $row['prod_sell_price'];

            $query1 = mysqli_query($con, "select * from draft_temp_trans where prod_id='$name' and branch_id='$branch' AND order_no='0' ") or die(mysqli_error($con));
            $count = mysqli_num_rows($query1);

            $total = $price * $qty;

            if ($count > 0) {
                mysqli_query($con, "update draft_temp_trans set qty=qty+'$qty' where prod_id='$name' and branch_id='$branch' AND order_no='0' ") or die(mysqli_error($con));
            } else {
                mysqli_query($con, "INSERT INTO draft_temp_trans(prod_id,qty,price,branch_id,user_id) VALUES('$name','$qty','$price','$branch','$user_id')") or die(mysqli_error($con));
            }

            apply_discount_from_admin($con, $name, $qty, $mode);

            require_once('composite_checker.php');

            // echo "<script>document.location='draft-order.php?cid=$cid'</script>";
        } else {
            echo "<script type='text/javascript'>alert('Error !!, you cannot sell " . $qty . " items of " . $prodName . " because it is more than what is in stock !!! ');</script>";
            echo "<script>document.location='draft-order.php?cid=$cid'</script>";
        }
    }
}
