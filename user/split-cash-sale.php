<?php

session_start();
$id = $_SESSION['id'];
include('../dist/includes/dbcon.php');
//$amount_due = $_POST['amount_due_to_customer'];

date_default_timezone_set("Africa/Lusaka");

$grand_total = $_POST['grand_total'];

//print_r(array_merge($_POST['split_bill_amount'], $_POST['type']));
$index = -1;
$branch = $_SESSION['branch'];
$date = date("Y-m-d H:i:s");

$payment_mode_id_1 = $_POST['payment_mode_id_1'];
$payment_mode_id_2 = $_POST['payment_mode_id_2'];
$split_bill_amount_2 = $_POST['split_bill_amount_2'];
$split_bill_amount_1 = $_POST['split_bill_amount_1'];
$orderNumber = (rand(50, 5000));
$totalPaid = $split_bill_amount_1 + $split_bill_amount_2;

// check if amount split is greater or equal to the total bill ....

if ($totalPaid >= $grand_total) {

    $change = $grand_total - $totalPaid;
    mysqli_query($con, "INSERT INTO invoices_tb(order_no) 
                                        VALUES('$orderNumber')")or die(mysqli_error($con));

    $invoice2 = mysqli_query($con, "SELECT MAX(id) AS id FROM invoices_tb")or die(mysqli_error($con));
    $rowss = mysqli_fetch_array($invoice2);
    $invoiceNo = $rowss['id'];

    mysqli_query($con, "INSERT INTO sales(cust_id,user_id,discount,amount_due,total,date_added,modeofpayment,cash_tendered,cash_change,branch_id,order_no,invoice_no,is_split) 
    VALUES('0','$id','0','$grand_total','$split_bill_amount_1','$date','$payment_mode_id_1','$totalPaid','$change','$branch','$orderNumber','$invoiceNo','Y')")or die(mysqli_error($con));

    $sales_id = mysqli_insert_id($con);

    // insert the split bill details..

    mysqli_query($con, "INSERT INTO split_payments(payment_id,amount,sales_id) 
    VALUES('$payment_mode_id_2','$split_bill_amount_2','$sales_id')")or die(mysqli_error($con));

    // insert the second split..

    mysqli_query($con, "INSERT INTO split_payments(payment_id,amount,sales_id) 
    VALUES('$payment_mode_id_1','$split_bill_amount_1','$sales_id')")or die(mysqli_error($con));

    $_SESSION['sid'] = $sales_id;
    $query = mysqli_query($con, "select * from temp_trans where branch_id='$branch' AND user_id='$id'")or die(mysqli_error($con));
    while ($row = mysqli_fetch_array($query)) {

        $pid = $row['prod_id'];
        $qty = $row['qty'];
        $price = $row['price'];
        $discount = $row['amount'];
        $discount_type = $row['discount_type'];

        // insert the open and close balances... 
        $dateis = date('Y-m-d');
        $openClose = mysqli_query($con, "SELECT * FROM open_close_tb WHERE prod_id='$pid' AND date='$dateis' ")or die(mysqli_error($con));

        // get the cost price for the batch being sold.. 

        $BatchQuery = mysqli_query($con, "SELECT * FROM `batches_tb` WHERE batch_id = (SELECT MIN(batch_id) from batches_tb WHERE prod_id='$pid' AND qty >0 ) GROUP BY prod_id ")or die(mysqli_error($con));
        $batchRows = mysqli_fetch_array($BatchQuery);
        $batchCostPrice = $batchRows['buy_price'];
        $batchID = $batchRows['batch_id'];

        // get the stock count from inventory.. 
        $stockCount = mysqli_query($con, "SELECT * FROM product WHERE prod_id='$pid' ")or die(mysqli_error($con));
        $countRows = mysqli_fetch_array($stockCount);
        $prod_sell_price = $countRows['prod_sell_price'];
        $stockOpenBalance = $countRows['prod_qty'];
        $dateis = date('Y-m-d');

        mysqli_query($con, "INSERT INTO open_close_tb(prod_id,open_bal,date) 
	VALUES('$pid','$stockOpenBalance','$dateis')")or die(mysqli_error($con));

        mysqli_query($con, "INSERT INTO sales_details(prod_id,qty,price,sales_id,order_no,user_id, discount, discount_type,cost_price) VALUES('$pid','$qty','$price','$sales_id','$orderNumber','$id','$discount','$discount_type','$batchCostPrice')")or die(mysqli_error($con));
        mysqli_query($con, "UPDATE product SET prod_qty=prod_qty-'$qty' where prod_id='$pid' and branch_id='$branch'") or die(mysqli_error($con));

        mysqli_query($con, "UPDATE batches_tb SET qty=qty-'$qty' where batch_id='$batchID'") or die(mysqli_error($con));

        // update the inventory counts.. 
    }

    echo "<script>document.location='escpos-php-development/example/interface/windows-usb.php'</script>";

    $result = mysqli_query($con, "DELETE FROM temp_trans where branch_id='$branch' AND user_id='$id'") or die(mysqli_error($con));
} else {
    echo "<script type='text/javascript'>alert(' Error !!, Cash Tendered cannot be less than amount due..');</script>";
    echo "<script>document.location='cash_transaction.php'</script>";
}    