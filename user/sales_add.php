<?php

session_start();
$id = $_SESSION['id'];
include('../dist/includes/dbcon.php');

include('../dist/includes/variant_validator.php');


$amount_due = $_POST['amount_due_to_customer'];

date_default_timezone_set("Africa/Lusaka");
$date = date("Y-m-d H:i:s");
$cid = $_REQUEST['cid'];
$branch = $_SESSION['branch'];

$total = 0;
$cid = $_REQUEST['cid'];

$orderNumber = (rand(50, 5000));
$payment_mode_id = $_POST['payment_mode_id'];
$tendered = $_POST['tendered'];
$change = $_POST['change'];
$cust_id = $_POST['cust_id'];



if ($tendered >= $amount_due) {


    
    $id = $_SESSION['id'];
    
    mysqli_query($con, "INSERT INTO invoices_tb(order_no) 
                                        VALUES('$orderNumber')")or die(mysqli_error($con));

    $invoice2 = mysqli_query($con, "SELECT MAX(id) AS id FROM invoices_tb")or die(mysqli_error($con));
    $rowss = mysqli_fetch_array($invoice2);
    $invoiceNo = $rowss['id'];
    
    mysqli_query($con, "INSERT INTO sales(cust_id,user_id,discount,amount_due,total,date_added,modeofpayment,cash_tendered,cash_change,branch_id,order_no,invoice_no) 
	VALUES('$cust_id','$id','0','$amount_due','$total','$date','$payment_mode_id','$tendered','$change','$branch','$orderNumber','$invoiceNo')")or die(mysqli_error($con));

    $sales_id = mysqli_insert_id($con);

    $_SESSION['sid'] = $sales_id;
    $query = mysqli_query($con, "select * from temp_trans where branch_id='$branch' AND user_id='$id'   ")or die(mysqli_error($con));
    while ($row = mysqli_fetch_array($query)) {
        $pid = $row['prod_id'];
        $qty = $row['qty'];
        $price = $row['price'];
        $discount = $row['amount'];
        $discount_type = $row['discount_type'];
        $composites = $row['composites'];
        $composite_names = $row['composite_names'];

        // insert the open and close balances... 
        $dateis = date('Y-m-d');
        $openClose = mysqli_query($con, "SELECT * FROM open_close_tb WHERE prod_id='$pid' AND date='$dateis' ")or die(mysqli_error($con));

        // get the cost price for the batch being sold.. 

        $BatchQuery = mysqli_query($con, "SELECT * FROM `batches_tb` WHERE batch_id = (SELECT MIN(batch_id) from batches_tb WHERE prod_id='$pid' AND qty >0 ) GROUP BY prod_id ")or die(mysqli_error($con));
        $batchRows = mysqli_fetch_array($BatchQuery);
        $batchCostPrice = $batchRows['buy_price'];
        $batchID = $batchRows['batch_id'];

        /** Add sold variants to sales_variants tbl */
        add_to_sales_variants($con, $pid, $sales_id);

        if (mysqli_num_rows($openClose) > 0) {

            // product already exisits so just update the close balance.. 
            // get the stock count from inventory.. 
            $stockCount = mysqli_query($con, "SELECT * FROM product WHERE prod_id='$pid' ")or die(mysqli_error($con));
            $countRows = mysqli_fetch_array($stockCount);
            $stockOpenBalance = $countRows['prod_qty'];
            $prod_sell_price = $countRows['prod_sell_price'];

            $dateis = date('Y-m-d');

            mysqli_query($con, "INSERT INTO sales_details(prod_id,qty,price,sales_id,order_no, user_id, discount, discount_type,cost_price) 
                            VALUES('$pid','$qty','$price','$sales_id','$orderNumber','$id','$discount','$discount_type','$batchCostPrice')")or die(mysqli_error($con));
            mysqli_query($con, "UPDATE product SET prod_qty=prod_qty-'$qty' where prod_id='$pid' and branch_id='$branch'") or die(mysqli_error($con));

            // update the closing balance for the stock.. 
            mysqli_query($con, "UPDATE open_close_tb SET close_bal='$stockOpenBalance'  WHERE date='$dateis' AND prod_id='$pid' ")or die(mysqli_error($con));

            // update the  batches quantities.. 

            mysqli_query($con, "UPDATE batches_tb SET qty=qty-'$qty' where batch_id='$batchID'") or die(mysqli_error($con));
        } else {
            // get the stock count from inventory.. 
            $stockCount = mysqli_query($con, "SELECT * FROM product WHERE prod_id='$pid' ")or die(mysqli_error($con));
            $countRows = mysqli_fetch_array($stockCount);
            $prod_sell_price = $countRows['prod_sell_price'];
            $stockOpenBalance = $countRows['prod_qty'];
            $dateis = date('Y-m-d');

            mysqli_query($con, "INSERT INTO open_close_tb(prod_id,open_bal,date) 
	                                VALUES('$pid','$stockOpenBalance','$dateis')")or die(mysqli_error($con));

            mysqli_query($con, "INSERT INTO sales_details(prod_id,qty,price,sales_id,order_no,user_id, discount, discount_type,cost_price, composites, composite_names) 
                                    VALUES('$pid','$qty','$price','$sales_id','$orderNumber','$id','$discount','$discount_type','$batchCostPrice', '$composites', '$composite_names' )")or die(mysqli_error($con));
            mysqli_query($con, "UPDATE product SET prod_qty=prod_qty-'$qty' where prod_id='$pid' and branch_id='$branch'") or die(mysqli_error($con));
            
             mysqli_query($con, "UPDATE batches_tb SET qty=qty-'$qty' where batch_id='$batchID'") or die(mysqli_error($con));
        }

        // update the inventory counts.. 
    }

    echo "<script>document.location='escpos-php-development/example/interface/windows-usb.php?cid=$cid'</script>";

    $result = mysqli_query($con, "DELETE FROM temp_trans where branch_id='$branch' AND user_id='$id'") or die(mysqli_error($con));
    // Validate/Update variant quantity and Delete temp variants too

    $querycke1 = mysqli_query($con, "SELECT * FROM temp_trans_variants WHERE prod_id='$pid' ")or die(mysqli_error($con));
    while ($rowck1 = mysqli_fetch_array($querycke1)) {
        $prod_id = $rowck1['prod_id'];
        $variant_qty = $rowck1['variant_qty'];
        $variant_id = $rowck1['variant_id'];

        updateVariants($variant_id, $variant_qty);
    }
    if ($result) {
        mysqli_query($con, "DELETE FROM temp_trans_variants ") or die(mysqli_error($con));
    }
} else {
    echo "<script type='text/javascript'>alert(' Error !!, Cash Tendered cannot be less than amount due..');</script>";
    echo "<script>document.location='cash_transaction.php'</script>";
}
