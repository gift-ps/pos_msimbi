<?php
include('../dist/includes/dbcon.php');

//Func to update Variants
function updateVariants($variant_id, $qty_sold)
{
    // return var_dump("Hello");
    global $con;
    $query1var = mysqli_query($con, "SELECT * FROM product_variants WHERE id = '$variant_id' ") or die(mysqli_error($con));
    if (mysqli_num_rows($query1var) > 0) {
        $query2var = mysqli_query($con, "UPDATE product_variants SET qty=qty-'$qty_sold' WHERE id = '$variant_id' ") or die(mysqli_error($con));
    }
    echo "SELECT * FROM product_variants WHERE id = '$variant_id' <br>
    UPDATE product_variants SET qty=qty-'$qty_sold' WHERE id = '$variant_id' ";
    // var_dump($query1var);
}

// Func to display variants from temp table
function display_variants($con, $product_id, $mode)
{ //return var_dump($product_id);
    // Check if product has variants to detamine what to display
    $res1 = mysqli_query($con, "SELECT * FROM product_variants WHERE prod_id = '$product_id' ") or die("Error: " . mysqli_error($con));
    if (mysqli_num_rows($res1) > 0) { ?>
        <div class="row">
            <div class="">
                <?php
                if ($mode == "cash") {
                    $resvartemp = mysqli_query($con, "SELECT * FROM temp_trans_variants
                            WHERE  prod_id = '$product_id' && variant_qty != '0'") or die("Error: " . mysqli_error($con));
                    while ($rowvartemp = mysqli_fetch_array($resvartemp)) {
                        // Now get details of variant
                        $inputed_qty = $rowvartemp['variant_qty'];
                        $inputed_variant = $rowvartemp['variant_id'];
                        $res1 = mysqli_query($con, "SELECT * FROM product_variants WHERE id = '$inputed_variant'") or die("Error: " . mysqli_error($con));
                        while ($row11 = mysqli_fetch_array($res1)) {
                            $color = $row11['color'];
                            $size = $row11['size'];
                            $qty = $row11['qty'];
                            $variants = "-" . $color . ", " . $size . ", " . $inputed_qty . ".<br/>";
                            echo $variants . "";
                        }
                    }
                }elseif ($mode == "credit") {
                    $resvartemp = mysqli_query($con, "SELECT * FROM draft_temp_trans_variants
                            WHERE  prod_id = '$product_id' && variant_qty != '0'") or die("Error: " . mysqli_error($con));
                    while ($rowvartemp = mysqli_fetch_array($resvartemp)) {
                        // Now get details of variant
                        $inputed_qty = $rowvartemp['variant_qty'];
                        $inputed_variant = $rowvartemp['variant_id'];
                        $res1 = mysqli_query($con, "SELECT * FROM product_variants WHERE id = '$inputed_variant'") or die("Error: " . mysqli_error($con));
                        while ($row11 = mysqli_fetch_array($res1)) {
                            $color = $row11['color'];
                            $size = $row11['size'];
                            $qty = $row11['qty'];
                            $variants = "-" . $color . ", " . $size . ", " . $inputed_qty . ".<br/>";
                            echo $variants . "";
                        }
                    }
                }
                ?>
            </div>
        </div>
    <?php
    } else {
    ?>
        <div class="col-md-9 text-center">
            N/A
        </div>

    <?php
    }
}


// Func to add variants after successfull sale
function add_to_sales_variants($con, $pid, $sales_id)
{
    $querycke1 = mysqli_query($con, "SELECT * FROM temp_trans_variants WHERE prod_id='$pid' ") or die(mysqli_error($con));
    while ($rowck1 = mysqli_fetch_array($querycke1)) {
        $prod_id = $rowck1['prod_id'];
        $variant_qty = $rowck1['variant_qty'];
        $variant_id = $rowck1['variant_id'];

        mysqli_query($con, "INSERT INTO sales_variants (`prod_id`, `variant_id`, `variant_qty`, `sales_id`) 
                                        VALUES ('$prod_id', '$variant_id', '$variant_qty', '$sales_id' ) ");
    }
}

// Func to display variants from sales_variants_table table
function display_sales_variants($con, $product_id, $sales_id)
{
    // Check if product has variants to detamine what to display
    $res1 = mysqli_query($con, "SELECT * FROM product_variants WHERE prod_id = '$product_id' ") or die("Error: " . mysqli_error($con));
    if (mysqli_num_rows($res1) > 0) { ?>
        <div class="row">
            <div class="">
                <?php
                // while ($row11 = mysqli_fetch_array($res1)) {
                // Here we get inputed data (id and quantity of variant)
                // $variant_id = $row['variant_id'];
                $resvartemp = mysqli_query($con, "SELECT * FROM sales_variants
                            WHERE  sales_id = '$sales_id' && prod_id = '$product_id' && variant_qty != '0' ") or die("Error: " . mysqli_error($con));
                while ($rowvartemp = mysqli_fetch_array($resvartemp)) {
                    // Now get details of variant
                    $inputed_qty = $rowvartemp['variant_qty'];
                    $inputed_variant = $rowvartemp['variant_id'];
                    $res1 = mysqli_query($con, "SELECT * FROM product_variants WHERE id = '$inputed_variant'") or die("Error: " . mysqli_error($con));
                    while ($row11 = mysqli_fetch_array($res1)) {
                        $color = $row11['color'];
                        $size = $row11['size'];
                        $qty = $row11['qty'];
                        $variants = "-" . $color . ", " . $size . ", " . $inputed_qty . ".<br/>";
                        echo $variants . "";
                    }
                }
                ?>
            </div>
        </div>
    <?php
    } else {
    ?>
        <div class="col-md-9 text-center">
            N/A
        </div>

    <?php
    }
}



// Func to add variants after successfull transfer
function add_to_transfer_variants($con, $prod_id, $variant_id, $variant_qty, $transfer_id)
{

    mysqli_query($con, "INSERT INTO variants_transfer (`prod_id`, `variant_id`, `variant_qty`, `transfer_id`) 
                                    VALUES ('$prod_id', '$variant_id', '$variant_qty', '$transfer_id' ) ") or die("Error:" . mysqli_error($con));
}

// Func to display variants from sales_variants_table table
function display_transfer_variants($con, $transfer_id)
{
    // Check if product has variants to detamine what to display
    $res1 = mysqli_query($con, "SELECT * FROM variants_transfer WHERE transfer_id = '$transfer_id' ") or die("Error: " . mysqli_error($con));
    if (mysqli_num_rows($res1) > 0) { ?>
        <div class="row">
            <div class="">
                <?php
                // while ($row11 = mysqli_fetch_array($res1)) {
                // Here we get inputed data (id and quantity of variant)
                // $variant_id = $row['variant_id'];
                $resvartemp = mysqli_query($con, "SELECT * FROM variants_transfer
                            WHERE  transfer_id = '$transfer_id' ") or die("Error: " . mysqli_error($con));
                while ($rowvartemp = mysqli_fetch_array($resvartemp)) {
                    // Now get details of variant
                    $inputed_qty = $rowvartemp['variant_qty'];
                    $inputed_variant = $rowvartemp['variant_id'];
                    $res1 = mysqli_query($con, "SELECT * FROM product_variants WHERE id = '$inputed_variant'") or die("Error: " . mysqli_error($con));
                    while ($row11 = mysqli_fetch_array($res1)) {
                        $color = $row11['color'];
                        $size = $row11['size'];
                        $qty = $row11['qty'];
                        $variants = "-" . $color . ", " . $size . ", " . $inputed_qty . ".<br/>";
                        echo $variants . "";
                    }
                }
                ?>
            </div>
        </div>
    <?php
    } else {
    ?>
        <div class="col-md-9 text-center">
            N/A
        </div>

<?php
    }
}
